"""
Utility for converting WiderPerson dataset
(http://www.cbsr.ia.ac.cn/users/sfzhang/WiderPerson/) to JPL format. This
dataset is already part of JPL's standard datasets and can be obtained using
the lwll/dataset_prep repo. This script was created to independently convert
the dataset/annotations to confirm the presence of a bug in the JPL repo.

NOTE: This script does NOT copy/move any images; it only generates the
converted labels.

Download the dataset from one of the links in the aforementioned website or
https://drive.google.com/file/d/1I7OjhaomWqd8Quf7o5suwLloRlY0THbp/view. Place
the downloaded file (WiderPerson.zip) in a directory and unzip it. The
resulting directory structure should be as follows (irrelevant directories and
files are not shown in the tree below):

src_dir
├── Annotations
├── Images
├── train.txt
└── val.txt

Category ids 3 and 4 are excluded by default to match the JPL dataset, but
the --exclude-categories command line option can be used to specify
categories to exclude. Use the --exclude-categories option without any
category ids to include all categories.
"""
import argparse
import pathlib
from typing import Set

import pandas as pd


def _convert(
    ids_path: pathlib.Path, annotations_dir: pathlib.Path,
    exclude_cats: Set[str]
) -> pd.DataFrame:
    """
    Helper function for :func:`convert`.
    """
    image_ids = []
    bboxes = []
    cats = []
    with open(ids_path, "r") as ids_file:
        for image_id in ids_file:
            image_id = image_id.strip()
            image_fname = f"{image_id}.jpg"
            annotations_path = annotations_dir / f"{image_id}.jpg.txt"

            with open(annotations_path, "r") as anns_file:
                lines = anns_file.readlines()
                for line in lines[1:]:
                    cat, *bbox = line.strip().split()

                    if cat not in exclude_cats:
                        image_ids.append(image_fname)
                        bboxes.append(", ".join(bbox))
                        cats.append(cat)

    df = pd.DataFrame({"id": image_ids, "bbox": bboxes, "class": cats})
    return df


def convert(
    src_dir: pathlib.Path, dst_dir: pathlib.Path, exclude_cats: Set[str] = None
):
    """
    Args:
        src_dir: Path to directory containing unzipped dataset and:
            ``train.txt``, ``val.txt``, ``Annotations`` subdirectory,
            ``Images`` subdirectory.
        dst_dir: Path to destination directory.
        exclude_cats: Category ids to exclude from converted dataset.
    """
    if exclude_cats is None:
        exclude_cats = set()

    # Ensure exclude_cat elements are strings.
    exclude_cats = set([str(val) for val in exclude_cats])

    train_path = src_dir / "train.txt"
    test_path = src_dir / "val.txt"
    annotations_dir = src_dir / "Annotations"

    for split, src_path in {"train": train_path, "test": test_path}.items():
        if src_path is not None:
            df = _convert(src_path, annotations_dir, exclude_cats)
            dst_path = dst_dir / "labels_full" / f"labels_{split}.feather"
            df.to_feather(dst_path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "src_dir", type=pathlib.Path,
        help="Path to directory of unzipped dataset contents"
    )
    parser.add_argument(
        "dst_dir", type=pathlib.Path,
        help="Path to destination directory, e.g., "
        "~/lwll_datasets/external/widerperson"
    )
    parser.add_argument(
        "-e", "--exclude-categories", type=str,
        nargs="*", metavar="id", default=["3", "4"],
        help="Class ids to exclude from the converted dataset"
    )
    args = parser.parse_args()

    if args.exclude_categories:
        print(f"Excluding category ids {', '.join(args.exclude_categories)}")
    exclude_cats = set(args.exclude_categories)

    convert(args.src_dir, args.dst_dir, exclude_cats=exclude_cats)
