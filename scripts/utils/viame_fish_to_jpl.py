import argparse
import collections
import concurrent.futures
import logging
import math
import pathlib
import random
import re
import shutil

import cv2
import pandas as pd


logger = logging.getLogger(__name__)


def parse_viame_csv(fpath: pathlib.Path) -> dict:
    """
    Read a VIAME CSV in the format used by :func:`process_video` and
    return the information in a dictionary.

    Args:
        fpath: Path to VIAME CSV. Must be in the format expected by
            :func:`process_video` (*not* the format expected by
            :func:`process_video2`).

    Returns:
        A dictionary containing the annotations file framerate,
        a list of frame ids, a list of bboxes, and a list of class names::

            {
                "fps": fps,
                "frame_ids": frame_ids,
                "bboxes": bboxes,
                "classes": classes,
            }
    """
    frame_ids = []
    bboxes = []
    classes = []
    with open(fpath, "r") as f:
        lines = f.readlines()

    fps = None
    expr = r"(fps: |fps=)(\d+)"

    for line in lines:
        # Look for FPS metadata line.
        if line.startswith("#"):
            if "meta" in line:
                match = re.search(expr, line)
                fps = int(match.groups()[1])
        else:
            line = line.strip().split(",")
            frame_id = int(line[2])
            bbox = [float(val) for val in line[3:7]]

            # At one point, all annotations ended with a list of
            # comma-separated `class name, confidence` pairs. In some cases,
            # this was corrected to only use the class with the highest
            # confidence (and set the confidence value to 1.0). In other cases,
            # it was not.
            class_list = line[9:]
            max_conf = -1
            max_conf_class = None
            for i in range(1, len(class_list), 2):
                conf = float(class_list[i])
                if conf > max_conf:
                    max_conf = conf
                    max_conf_class = class_list[i-1]
            frame_ids.append(frame_id)
            bboxes.append(bbox)
            classes.append(max_conf_class)

    assert fps is not None, "No FPS metadata found in CSV"

    return {
        "fps": fps,
        "frame_ids": frame_ids,
        "bboxes": bboxes,
        "classes": classes,
    }


def process_video(
    frame_dir: pathlib.Path, csv_path: pathlib.Path, dst_dir: pathlib.Path,
    sample_rate: int = None
) -> None:
    """
    Create a JPL-format dataset from a VIAME dataset video and annotations.
    Frames should already have been extracted from the video.

    This is one of two functions to process (frames extracted from) a video.
    This function expects 1) a directory named after the video (e.g.,
    ``761901309_cam2_3.mp4`` containing frames named ``frame000001.png``,
    ``frame000002.png``, etc.) and a CSV file of annotations with the same
    filestem as the video (e.g., ``761901309_cam2_3.csv``). The header of the
    CSV file should contain metadata that includes the FPS at which frames
    were extracted from the video. Frame ids in the CSV file should be integer
    values corresponding to the filenames (1, 2, 3, etc.).

    Args:
        frame_dir: Path to directory of images for the video.
        csv_path: Path to annotations CSV file of the video.
        dst_dir: Path to which output files should be written; cropped images
            will be written to `dst_dir/images` and the corresponding
            JPL-format feather file will be written to `dst_dir/labels`.
        sample_rate: Framerate at which to sample images. If ``None``, use
            all images. This value should be a divisor of the framerate at
            which frames were extracted from the video (e.g., 1, 5, 10).
    """
    frame_id_to_fpath = {}

    for image_fpath in frame_dir.iterdir():
        # NOTE: filename numbering doesn't have to start at 1 (e.g.,
        # frame000001.png); the frame id in the annotations file starts at 0
        # and refers to the first image filename. In the current data, the
        # extracted frames from all videos start at frame000001.png so, for
        # simplicity, simply subtract 1 from filename number to get frame id.
        frame_id = int(image_fpath.stem[len("frame"):]) - 1
        frame_id_to_fpath[frame_id] = image_fpath

    csv_data = parse_viame_csv(csv_path)

    if not csv_data["frame_ids"]:
        logger.warning(f"{csv_path.name} has no annotations; skipping")
        return

    fps = csv_data["fps"]
    if fps != 5:
        max_annotated_frame_id = max(csv_data["frame_ids"])
        max_image_frame_id = max(frame_id_to_fpath)
        if max_annotated_frame_id > max_image_frame_id:
            logger.error(
                f"{csv_path.name} annotated at higher framerate ({fps} FPS) "
                "than extracted frames; skipping"
            )
            return
        else:
            logger.info(
                f"{csv_path.name} annotated at {fps} FPS; manually check "
                "video to ensure correctness"
            )

    frame_id_to_anns = collections.defaultdict(list)
    frame_ids = csv_data["frame_ids"]
    bboxes = csv_data["bboxes"]
    classes = csv_data["classes"]
    for frame_id, bbox, class_ in zip(frame_ids, bboxes, classes):
        frame_id_to_anns[frame_id].append((bbox, class_))

    if sample_rate is not None:
        stride = int(fps / sample_rate)
        frame_id_to_anns = {
            k: v for k, v in frame_id_to_anns.items() if k % stride == 0
        }

    if not frame_id_to_anns:
        logger.info(
            f"{csv_path}: No annotations after sub-sampling, continuing"
        )
        return

    finalized_fnames = []
    finalized_classes = []
    for frame_id, anns in frame_id_to_anns.items():
        if frame_id not in frame_id_to_fpath:
            logger.warning(
                f"File corresponding to frame id {frame_id} missing in "
                f"{frame_dir}; skipping frame"
            )
            continue

        image_fpath = frame_id_to_fpath[frame_id]
        img = cv2.imread(str(image_fpath))
        for i, (bbox, class_) in enumerate(anns):
            x1, y1, x2, y2 = [max(int(val), 0) for val in bbox]
            crop = img[y1:(y2+1), x1:(x2+1)]
            image_fstem = image_fpath.stem
            image_ext = image_fpath.suffix
            dst_image_fname = f"{frame_dir.name}__{image_fstem}__{i}{image_ext}"
            dst_image_fpath = dst_dir / "images" / dst_image_fname
            cv2.imwrite(str(dst_image_fpath), crop)

            finalized_classes.append(class_)
            finalized_fnames.append(dst_image_fname)

    df = pd.DataFrame(
        {"id": finalized_fnames, "class": finalized_classes}
    )
    dst_labels_fname = frame_dir.stem + ".feather"
    dst_labels_fpath = dst_dir / "labels" / dst_labels_fname
    df.to_feather(dst_labels_fpath)


def process_video2(
    frame_dir: pathlib.Path, csv_path: pathlib.Path, dst_dir: pathlib.Path,
    sample_rate: int = 1
) -> None:
    """
    Create a JPL-format dataset from a VIAME dataset video and annotations.
    Frames should already have been extracted from the video.

    This is one of two functions to process (frames extracted from) a video.
    This function expects the annotations CSV file to contain the actual
    filenames for frame ids and does not expect any header with metadata on
    the FPS at which frames were extracted from the original video. It is
    assumed that the video was extracted at 5 FPS.

    Args:
        frame_dir: Path to directory of images for the video.
        csv_path: Path to annotations CSV file of the video.
        dst_dir: Path to which output files should be written; cropped images
            will be written to `dst_dir/images` and the corresponding
            JPL-format feather file will be written to `dst_dir/labels`.
        sample_rate: Framerate at which to sample images. If ``None``, use
            all images. This value should be a divisor of the framerate at
            which frames were originally extracted from the video (e.g., 1, 5).
    """
    frame_ids = []
    bboxes = []
    classes = []

    frame_id_to_fpath = {}
    with open(csv_path, "r") as f:
        for line in f:
            line = line.strip().split(",")

            fname = line[1]
            frame_id = int(line[2])
            bbox = [float(val) for val in line[3:7]]
            class_ = line[9]

            if frame_id not in frame_id_to_fpath:
                image_fpath = frame_dir / fname
                assert image_fpath.exists(), "Image does not exist"
                frame_id_to_fpath[frame_id] = image_fpath

            frame_ids.append(frame_id)
            bboxes.append(bbox)
            classes.append(class_)

    frame_id_to_anns = collections.defaultdict(list)
    for frame_id, bbox, class_ in zip(frame_ids, bboxes, classes):
        frame_id_to_anns[frame_id].append((bbox, class_))

    if sample_rate is not None:
        # TODO: check that all of these videos are actually 5 Hz. This info
        # isn't available in these files and will have to be checked via the
        # viame.kitware.com:
        # https://viame.kitware.com/#/folder/5e4c2817a0fc86aa0315b1a1
        fps = 5
        stride = int(fps / sample_rate)
        frame_id_to_anns = {
            k: v for k, v in frame_id_to_anns.items() if k % stride == 0
        }

    if not frame_id_to_anns:
        logger.info(
            f"{csv_path}: No annotations after sub-sampling; continuing"
        )
        return

    finalized_fnames = []
    finalized_classes = []
    for frame_id, anns in frame_id_to_anns.items():
        image_fpath = frame_id_to_fpath[frame_id]
        img = cv2.imread(str(image_fpath))

        for i, (bbox, class_) in enumerate(anns):
            x1, y1, x2, y2 = [max(int(val), 0) for val in bbox]
            crop = img[y1:(y2+1), x1:(x2+1)]
            image_fstem = image_fpath.stem
            ext = image_fpath.suffix
            dst_image_fname = f"{frame_dir.name}__{image_fstem}__{i}{ext}"
            dst_image_fpath = dst_dir / "images" / dst_image_fname
            cv2.imwrite(str(dst_image_fpath), crop)

            finalized_classes.append(class_)
            finalized_fnames.append(dst_image_fname)

    df = pd.DataFrame(
        {"id": finalized_fnames, "class": finalized_classes}
    )
    dst_labels_fname = frame_dir.stem + ".feather"
    dst_labels_fpath = dst_dir / "labels" / dst_labels_fname
    df.to_feather(dst_labels_fpath)


def process_videos(
    src_dir: pathlib.Path, dst_dir: pathlib.Path, sample_rate: int = None,
    num_workers: int = None
) -> None:
    """
    Currently, the dataset directory (US_SE_2017_SEFSC_QUADCAM_SAMPLER)
    contains videos/annotations in two formats:

    - a directory of frames, where the directory name is the original filename
      (e.g., 761901325_cam2_2.mp4) and the annotations are found at the SAME
      level as the directory in a CSV file with the same filestem (e.g.,
      761901325_cam2_2.csv). The directory contains frames numbered in the
      format ``frame000000.png``. The CSV files' third column corresponds
      to the frame number in the filename.
      NOTE: These CSV files contain a metadata line in the header indicating
      the framerate for the detections (e.g., ``fps: 10``) as output by
      kwiver's downsampler. In some cases, this doesn't match the fps at which
      frames were extracted by ffmpeg (e.g., a CSV with frames that go up to 60
      but the corresponding directory only contains 30 images)

    - a directory of both frames and its corresponding annotations file, where
      the frames are not named sequentially. The CSV file frame ids correspond
      to the image filenames, and there is no metadata header with information
      on the detection framerate or framerate at which frames were extracted.
      These values are assumed to be 5 Hz.

    Args:
        src_dir: Path to dataset directory containing all (extracted) videos
            and annotations.
        dst_dir: Path to which output files should be written. Images are
            written to `dst_dir/images` and are named
            `{frame_dir}__{image_fstem}__{i}.{ext}`, where `i` is an
            incrementing id corresponding to the bboxes from the image. For
            example, the first bbox (crop) extracted from
            `src_dir/762101003_cam4.avi/frame003735.png` would be written to
            `dst_dir/images/762101003_cam4.avi__frame003735_0.png`.
            Feather files are written to `dst_dir/labels`; the feather file
            for the aforementioned example would be written to
            `dst_dir/labels/762101003_cam4.feather`.
        sample_rate: Framerate at which to sub-sample extracted frames. For
            example, if the detector was run at 5 Hz and ``sample_rate=1``
            is passed, only every fifth frame is used to produce the output
            dataset. The underlying functions take into account the fact that
            the detector and ffmpeg extraction may have been performed at
            different framerates.
        num_workers: Number of processes to spawn.
    """
    dst_image_dir = dst_dir / "images"
    dst_label_dir = dst_dir / "labels"

    dst_dir.mkdir(exist_ok=True)
    dst_image_dir.mkdir(exist_ok=True)
    dst_label_dir.mkdir(exist_ok=True)

    futures = []
    pool = concurrent.futures.ProcessPoolExecutor(max_workers=num_workers)
    for fpath in src_dir.iterdir():
        if fpath.is_dir() and fpath.suffix:
            csv_path = fpath.with_suffix(".csv")
            func = process_video
        elif fpath.is_dir() and fpath.name.startswith("J"):
            csv_path = fpath / (fpath.name + ".csv")
            func = process_video2

        assert csv_path.exists(), f"{csv_path} does not exist"
        future = pool.submit(
            func, frame_dir=fpath, csv_path=csv_path,
            dst_dir=dst_dir, sample_rate=sample_rate
        )
        futures.append(future)

    pool.shutdown()


def make_splits(
    src_dir: pathlib.Path, dst_dir: pathlib.Path, ratio_train: float = 0.7
) -> None:
    """
    Split data into training and test sets using a greedy approach that
    ensures all annotations from a given video are put into either train OR
    test while attempting to maintain the desired class balance. This ensures
    that any video in the train set is not also seen in the test set.

    Args:
        src_dir: Directory containing `labels` and `images` subdirectories,
            i.e., the `dst_dir` from :func:`process_videos`.
        dst_dir: Path to which final JPL-format dataset should be
            written.
        ratio_train: Fraction of data (based on classes) that should be
            placed in the train set.
    """
    labels_dir = src_dir / "labels"

    dataframes = {}
    for labels_fpath in labels_dir.iterdir():
        video_name = labels_fpath.stem
        df = pd.read_feather(labels_fpath)
        dataframes[video_name] = df

    combined = pd.concat(dataframes.values()).reset_index(drop=True)

    class_counts = dict(combined["class"].value_counts())

    # Map of class name to list of videos containing said class, sorted by
    # number of instances in those classes.
    class_to_videos = collections.defaultdict(list)

    for class_ in class_counts:
        for video, df in dataframes.items():
            num_instances = len(df.loc[df["class"] == class_])
            if num_instances > 0:
                class_to_videos[class_].append((video, num_instances))

    for video_list in class_to_videos.values():
        video_list.sort(key=lambda x: x[1])

    # We use a greedy approach to assign videos containing each class (in
    # order of ascending count) to the train set until the desired train/test
    # ratio is achieved. We do not split images from a single video into
    # multiple sets; a video is either assigned to the train or the test set.
    assigned_videos = set()
    sorted_classes = sorted(
        [(class_, count) for class_, count in class_counts.items()],
        key=lambda x: x[1],
        reverse=True
    )

    train_df = pd.DataFrame(columns=("id", "class"))
    test_df = pd.DataFrame(columns=("id", "class"))

    while sorted_classes:
        curr_class, num_instances = sorted_classes.pop()
        num_train = math.ceil(ratio_train * num_instances)

        num_assigned_to_train = len(
            train_df.loc[train_df["class"] == curr_class]
        )
        while num_assigned_to_train < num_train:
            if not class_to_videos[curr_class]:
                break

            video_name, _ = class_to_videos[curr_class].pop()

            if video_name in assigned_videos:
                continue
            video_df = dataframes[video_name]
            train_df = pd.concat([train_df, video_df])
            assigned_videos.add(video_name)
            num_assigned_to_train = len(
                train_df.loc[train_df["class"] == curr_class]
            )

        while class_to_videos[curr_class]:
            video_name, _ = class_to_videos[curr_class].pop()
            if video_name in assigned_videos:
                continue
            video_df = dataframes[video_name]
            test_df = pd.concat([test_df, video_df])
            assigned_videos.add(video_name)

    train_df = train_df.reset_index(drop=True)
    test_df = test_df.reset_index(drop=True)

    dst_labels_dir = dst_dir / "labels_full"
    dst_labels_dir.mkdir(exist_ok=True)

    for df, split in [(train_df, "train"), (test_df, "test")]:
        dst_image_dir = dst_dir / "viame_fish_full" / split
        dst_image_dir.mkdir(exist_ok=True, parents=True)

        for fname in df["id"].tolist():
            src_image_fpath = src_dir / "images" / fname
            dst_image_fpath = dst_image_dir / fname
            shutil.copyfile(src_image_fpath, dst_image_fpath)

        dst_labels_fpath = dst_labels_dir / f"labels_{split}.feather"
        df.to_feather(dst_labels_fpath)


def make_splits_simple(
    src_dir: pathlib.Path, dst_dir: pathlib.Path, ratio_train: float = 0.7
) -> None:
    """
    Split data into training and test sets without regard for which video
    annotations came from. In other words, annotations from the same video
    may end up both train and test.

    Args:
        src_dir: Directory containing `labels` and `images` subdirectories,
            i.e., the `dst_dir` from :func:`process_videos`.
        dst_dir: Path to which final JPL-format dataset should be
            written.
        ratio_train: Fraction of data (based on classes) that should be
            placed in the train set.
    """
    labels_dir = src_dir / "labels"

    dataframes = {}
    for labels_fpath in labels_dir.iterdir():
        video_name = labels_fpath.stem
        df = pd.read_feather(labels_fpath)
        dataframes[video_name] = df

    combined = pd.concat(dataframes.values()).reset_index(drop=True)

    train_df = pd.DataFrame(columns=("id", "class"))
    test_df = pd.DataFrame(columns=("id", "class"))

    random.seed(0)
    for class_, count in dict(combined["class"].value_counts()).items():
        num_train = math.ceil(ratio_train * count)
        num_test = count - num_train

        if count > 1 and num_test == 0:
            num_train -= 1
            num_test = 1

        class_df = combined.loc[combined["class"] == class_]

        if num_test > 0:
            idxs = set(class_df.index)
            class_train_idxs = set(random.sample(idxs, k=num_train))
            class_test_idxs = idxs - class_train_idxs

            class_train_df = combined.iloc[list(class_train_idxs)]
            class_test_df = combined.iloc[list(class_test_idxs)]

            train_df = pd.concat([train_df, class_train_df])
            test_df = pd.concat([test_df, class_test_df])
        else:
            train_df = pd.concat([train_df, class_df])

    train_df = train_df.reset_index(drop=True)
    test_df = test_df.reset_index(drop=True)

    dst_labels_dir = dst_dir / "labels_full"
    dst_labels_dir.mkdir(exist_ok=True)

    for df, split in [(train_df, "train"), (test_df, "test")]:
        dst_image_dir = dst_dir / "viame_fish_full" / split
        dst_image_dir.mkdir(exist_ok=True, parents=True)

        for fname in df["id"].tolist():
            src_image_fpath = src_dir / "images" / fname
            dst_image_fpath = dst_image_dir / fname
            shutil.copyfile(src_image_fpath, dst_image_fpath)

        dst_labels_fpath = dst_labels_dir / f"labels_{split}.feather"
        df.to_feather(dst_labels_fpath)


if __name__ == "__main__":
    logging.basicConfig(
        format="[%(levelname)s] %(message)s", level=logging.DEBUG
    )
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "src_dir", type=pathlib.Path, help="Path to dataset directory"
    )
    parser.add_argument(
        "dst_dir", type=pathlib.Path, help="Path to destination directory"
    )
    parser.add_argument(
        "-f", "--framerate", type=int,
        help="Framerate at which to sample annotations"
    )
    parser.add_argument(
        "-n", "--num-workers", type=int, default=None,
        help="Number of processes to spawn for parallel conversion"
    )
    parser.add_argument(
        "-s", "--splits-only", action="store_true",
        help="Generate splits from labels/images that have already been "
        "extracted from the original dataset; in this case, src_dir should "
        "point to the directory containing 'images' and 'labels' "
        "subdirectories generated from the script, and dst_dir should "
        "point to the final destination directory"
    )
    parser.add_argument(
        "-S", "--split-simple", action="store_true",
        help="Assign detections to train/test without regard for which video "
        "they came from. Otherwise, default is to use a greedy approach to "
        "assign entire videos to either train or test while attempting to "
        "maintain class balance"
    )
    args = parser.parse_args()

    if not args.dst_dir.exists():
        args.dst_dir.mkdir(parents=True)

    split_func = make_splits_simple if args.split_simple else make_splits

    if args.splits_only:
        split_func(args.src_dir, args.dst_dir)
    else:
        process_videos(
            args.src_dir, args.dst_dir, sample_rate=args.framerate,
            num_workers=args.num_workers
        )
    # TODO: both process original data and make splits.
