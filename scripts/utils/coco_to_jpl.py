import pandas as pd
import json
import os
import tqdm
import argparse
import warnings
import pathlib

def create_df(js, id_to_cat,
            ignore_cats=[],
            df=None):
    if df is None:
        df = pd.DataFrame(columns=['id','bbox','class'])
    empty_anns = [x['file_name'] for x in js['images']]
    img_to_id = {}
    for im in js['images']:
        idx = im['id']
        assert idx not in img_to_id
        img_to_id[idx] = im['file_name']
    for ann in tqdm.tqdm(js['annotations']):
        bbox = [int(x) for x in ann['bbox']]
        bbox[2] = bbox[0] + bbox[2]
        bbox[3] = bbox[1] + bbox[3]
        bbox = ",".join(str(x) for x in bbox).strip()
        filename = img_to_id[ann['image_id']]
        clas = id_to_cat[ann['category_id']]
        if clas in ignore_cats:#['test','ignore','negative']:
            continue
        else:
            try:
                empty_anns.remove(filename)
            except ValueError:
                pass
        df.loc[len(df)] = [filename, bbox, clas]
    return df, empty_anns

def main(coco_json, feather_dst, ignore_cats):
    with open(coco_json) as f:
        js = json.load(f)

    id_to_cat = {}
    for c in js['categories']:
        idx = c['id']
        assert idx not in id_to_cat
        id_to_cat[idx] = c['name']

    if os.path.isfile(feather_dst):
        df = pd.read_feather(feather_dst)
    else:
        df = None
    output_df, empty_anns = create_df(
        js,
        id_to_cat,
        ignore_cats,
        df
    )
    if len(empty_anns) > 0:
        warnings.warn(f"There are {len(empty_anns)} images with no annotations")

    output_df.to_feather(feather_dst)
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "coco_json", type=pathlib.Path,
        help="Path to coco json file"
    )
    parser.add_argument(
        "feather_dst", type=pathlib.Path,
        help="Path to destination where feather file should be placed. " 
        "If there is already a file there, will append dataframe to it"
    )
    parser.add_argument('-i','--ignore_cats', 
            nargs='*', 
            help='Categories to ignore (pass as list)', 
            required=False)
    args = parser.parse_args()
    main(args.coco_json, args.feather_dst, args.ignore_cats)
