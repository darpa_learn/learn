import argparse
import json
import pathlib
from typing import Dict, Set, Tuple
from pprint import pprint


def filter_by_category(
    train_dataset: dict, test_dataset: dict
) -> Dict[str, set]:
    """
    Return train and test annotation ids corresponding to categories that are
    exhaustively annotated across all images, as well as the set of
    exhaustively annotated category ids.
    """
    non_exhaustive_cat_ids = set()
    for dataset in (train_dataset, test_dataset):
        for image in dataset["images"]:
            for cat_id in image["not_exhaustive_category_ids"]:
                non_exhaustive_cat_ids.add(cat_id)

    all_cat_ids = set([cat["id"] for cat in train_dataset["categories"]])
    exhaustive_cat_ids = all_cat_ids - non_exhaustive_cat_ids

    train_annotation_ids = set()
    for ann in train_dataset["annotations"]:
        if ann["category_id"] in exhaustive_cat_ids:
            train_annotation_ids.add(ann["id"])

    test_annotation_ids = set()
    for ann in test_dataset["annotations"]:
        if ann["category_id"] in exhaustive_cat_ids:
            test_annotation_ids.add(ann["id"])

    return {
        "train_annotation_ids": train_annotation_ids,
        "test_annotation_ids": test_annotation_ids,
        "exhaustive_cat_ids": exhaustive_cat_ids,
    }


def _filter_by_image(dataset: dict) -> Tuple[Set[int], Set[int]]:
    """
    Return the ids of images that are exhaustively annotated (i.e., images with
    an empty `not_exhaustive_category_ids` field) and the ids of annotations
    corresponding to those images, for a given dataset.

    Helper function for :func:`filter_by_image`.
    """
    image_ids = set()
    for image in dataset["images"]:
        if not image["not_exhaustive_category_ids"]:
            image_ids.add(image["id"])

    annotation_ids = set()
    for ann in dataset["annotations"]:
        if ann["image_id"] in image_ids:
            annotation_ids.add(ann["id"])

    return image_ids, annotation_ids


def filter_by_image(
    train_dataset: dict, test_dataset: dict
) -> Dict[str, set]:
    """
    Return the ids of images that are exhaustively annotated (i.e., images with
    an empty `not_exhaustive_category_ids` field) and the ids of annotations
    corresponding to those images for both train and test datasets.
    """
    train_image_ids, train_annotation_ids = _filter_by_image(train_dataset)
    test_image_ids, test_annotation_ids = _filter_by_image(test_dataset)
    return {
        "train_image_ids": train_image_ids,
        "train_annotation_ids": train_annotation_ids,
        "test_image_ids": test_image_ids,
        "test_annotation_ids": test_annotation_ids,
    }


def get_exhaustive_ids(train_dataset: dict, test_dataset: dict) -> dict:
    """
    Args:
        train_dataset: LVIS train dataset JSON as a Python dict.
        test_dataset: LVIS val dataset JSON as a Python dict.

    Returns:
        A dict containing statistics on the original dataset, the dataset if
        filtered to retain only annotations with categories exhaustively
        annotated across all images, and the dataset if filtered to retain
        only annotations for images that are exhaustively annotated (as well
        as the relevant image, annotation, and/or category ids for the latter).
    """
    by_cat = filter_by_category(train_dataset, test_dataset)
    by_image = filter_by_image(train_dataset, test_dataset)

    ret = {
        "original": {
            "stats": {
                "categories": len(train_dataset["categories"]),
                "train_images": len(train_dataset["images"]),
                "test_images": len(test_dataset["images"]),
                "train_anns": len(train_dataset["annotations"]),
                "test_anns": len(test_dataset["annotations"]),
            },
        },
        "by_category": {
            "stats": {
                "categories": len(by_cat["exhaustive_cat_ids"]),
                "train_anns": len(by_cat["train_annotation_ids"]),
                "test_anns": len(by_cat["test_annotation_ids"]),
            },
            "train_annotation_ids": by_cat["train_annotation_ids"],
            "test_annotation_ids": by_cat["test_annotation_ids"],
        },
        "by_image": {
            "stats": {
                "train_images": len(by_image["train_image_ids"]),
                "train_anns": len(by_image["train_annotation_ids"]),
                "test_images": len(by_image["test_image_ids"]),
                "test_anns": len(by_image["test_annotation_ids"]),
            },
            "train_image_ids": by_image["train_image_ids"],
            "train_annotation_ids": by_image["train_annotation_ids"],
            "test_image_ids": by_image["test_image_ids"],
            "test_annotation_ids": by_image["test_annotation_ids"],
        },
    }
    return ret


def filter_and_write_dataset(
    dataset: dict, annotation_ids: Set[int], dst_path: pathlib.Path
):
    """
    Args:
        dataset: LVIS dataset split JSON file as a Python dict.
        annotation_ids: Set of annotation ids to keep.
        dst_path: Path to the resulting feather file.
    """
    dataset["annotations"] = [
        ann for ann in dataset["annotations"] if ann["id"] in annotation_ids
    ]

    df = lvis_to_jpl.convert(dataset)
    df.to_feather(dst_path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="If output arguments are not provided, statistics on "
        "the original train/test dataset splits (categories, images, "
        "annotations) are printed to the terminal alongside statistics on the "
        "number of categories, images, and/or annotations when the dataset "
        "splits are filtered to exclude images or categories that are not "
        "exhaustively annotated.\n"
        "If output arguments are provided, convert the filtered versions of "
        "the dataset splits to JPL format and write to a feather file."
    )
    dataset_args = parser.add_argument_group("Dataset arguments (required)")
    dataset_args.add_argument(
        "-t", "--train", type=pathlib.Path, metavar="<path>", required=True,
        help="Path to LVIS train JSON file"
    )
    dataset_args.add_argument(
        "-v", "--val", type=pathlib.Path, metavar="<path>", required=True,
        help="Path to LVIS val JSON file"
    )

    output_args = parser.add_argument_group("Output arguments (optional)")
    output_args.add_argument(
        "-o", "--output", type=pathlib.Path, metavar="<path>",
        help="Path to output directory for resulting feather files"
    )
    output_args.add_argument(
        "-f", "--filter-by", type=str, choices=("category", "image"),
        help="Whether to filter by category (only retain annotations for "
        "categories that are exhaustively annotated across all images) or "
        "by image (only retain annotations that do not contain any "
        "non-exhaustively annotated categories"
    )
    output_args.add_argument(
        "-m", "--match-categories", action="store_true",
        help="Only retain categories present in both train and val datasets"
    )
    args = parser.parse_args()

    if not (bool(args.output) == bool(args.filter_by)):
        raise RuntimeError(
            "Both --output and --filter-by required to write output files"
        )

    train = json.load(open(args.train, "r"))
    test = json.load(open(args.val, "r"))

    ret = get_exhaustive_ids(train, test)

    if args.output:
        import lvis_to_jpl

        if not args.output.exists():
            args.output.mkdir(parents=True)

        key = "by_category" if args.filter_by == "category" else "by_image"
        train_annotation_ids = ret[key]["train_annotation_ids"]
        test_annotation_ids = ret[key]["test_annotation_ids"]

        if args.match_categories:
            # Only retain annotations corresponding to categories present in
            # both train and test sets.
            train_cats = set(
                [ann["category_id"] for ann in train["annotations"]]
            )
            test_cats = set(
                [ann["category_id"] for ann in test["annotations"]]
            )
            common_cats = train_cats.intersection(test_cats)

            train_ids_to_remove = set()
            for ann in train["annotations"]:
                if ann["category_id"] not in common_cats:
                    train_ids_to_remove.add(ann["id"])
            train_annotation_ids -= train_ids_to_remove

            test_ids_to_remove = set()
            for ann in test["annotations"]:
                if ann["category_id"] not in common_cats:
                    test_ids_to_remove.add(ann["id"])
            test_annotation_ids -= test_ids_to_remove

        filter_and_write_dataset(
            train, train_annotation_ids, args.output / "labels_train.feather"
        )

        filter_and_write_dataset(
            test, test_annotation_ids, args.output / "labels_test.feather"
        )
    else:
        # Print stats (numbers of images, annotations, categories) in the
        # original dataset, the dataset if filtered by category, and the
        # dataset if filtered by image.
        stats = {k: v["stats"] for k, v in ret.items()}
        pprint(stats)
