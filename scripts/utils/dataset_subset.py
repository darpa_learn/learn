import argparse
import concurrent.futures
import pathlib
import random
import shutil
from typing import List, Tuple

import pandas as pd


def copy_files(
    src_dir: pathlib.Path, dst_dir: pathlib.Path, fnames: List[str]
):
    """
    Copy the filenames in `fnames` from `src_dir` to `dst_dir`. The filenames
    references in `fnames` must exist in `src_dir`.
    """
    pool = concurrent.futures.ThreadPoolExecutor()
    for fname in fnames:
        src_path = src_dir / fname
        dst_path = dst_dir / fname
        pool.submit(shutil.copy, src_path, dst_path)
    pool.shutdown()


def sample_dataframe(
    df: pd.DataFrame, num_images: int
) -> Tuple[pd.DataFrame, List[str]]:
    """
    Sample `num_images` images from the JPL-formatted DataFrame `df` and
    return the resulting DataFrame and a list of the selected image filenames.
    """
    image_fnames = df["id"].unique().tolist()
    sample_fnames = random.sample(image_fnames, num_images)
    df_sample = df.loc[df["id"].isin(sample_fnames)]

    # Calling reset_index is required for writing to a feather file.
    # Passing `drop=True` prevents a new "Index" column from being added.
    df_sample = df_sample.reset_index(drop=True)

    return df_sample, sample_fnames


def sample_dataset(
    src: pathlib.Path, dst: pathlib.Path, num_train: int, num_test: int
):
    """
    Sample `num_train` train images and `num_test` test images from the
    JPL-formatted LwLL dataset at `src`, and write the result to a parallel
    directory/file structure at `dst`.
    """
    src_dataset_name = src.stem
    dst_dataset_name = dst.stem

    src_paths = {
        "dataset_name": src_dataset_name,
        "root_dir": src,
    }
    dst_paths = {
        "dataset_name": dst_dataset_name,
        "root_dir": dst,
    }

    for paths_dict in (src_paths, dst_paths):
        root_dir = paths_dict["root_dir"]

        labels_dir = root_dir / "labels_full"
        train_labels_path = labels_dir / "labels_train.feather"
        test_labels_path = labels_dir / "labels_test.feather"

        dataset_name = paths_dict["dataset_name"]
        image_dir = root_dir / f"{dataset_name}_full"
        train_image_dir = image_dir / "train"
        test_image_dir = image_dir / "test"

        paths_dict.update(
            {
                "labels_dir": labels_dir,
                "train_labels_path": train_labels_path,
                "test_labels_path": test_labels_path,
                "image_dir": image_dir,
                "train_image_dir": train_image_dir,
                "test_image_dir": test_image_dir
            }
        )

    # Create the dst directories if they don't exist.
    for key, path in dst_paths.items():
        if key.endswith("_dir") and not path.exists():
            path.mkdir(parents=True)

    # Sample train subset, save the sampled feather file, and copy images.
    src_train_df = pd.read_feather(src_paths["train_labels_path"])
    dst_train_df, train_sample_fnames = sample_dataframe(
        src_train_df, num_train
    )
    dst_train_df.to_feather(dst_paths["train_labels_path"])
    copy_files(
        src_paths["train_image_dir"],
        dst_paths["train_image_dir"],
        train_sample_fnames
    )

    # Sample test subset, save to feather, and copy images.
    src_test_df = pd.read_feather(src_paths["test_labels_path"])
    dst_test_df, test_sample_fnames = sample_dataframe(src_test_df, num_test)
    dst_test_df.to_feather(dst_paths["test_labels_path"])
    copy_files(
        src_paths["test_image_dir"],
        dst_paths["test_image_dir"],
        test_sample_fnames
    )


if __name__ == "__main__":
    """
    Example:

    Given a dataset /path/to/lvis with the following directory structure:

    lvis
    ├── labels_full
    └── lvis_full
        ├── test
        └── train

    where `labels_full` contains `labels_train.feather` and
    `labels_test.feather`, and `lvis_full/train` and `lvis_full/test`
    contain images, we wish to create a parallel dataset /path/to/lvis_subset
    with the structure:

    lvis_subset
    ├── labels_full
    └── lvis_subset_full
        ├── test
        └── train

    which contains 500 train images from the original dataset and 100 test
    images from the original dataset by running the following command:

    ```
    python dataset_subset.py /path/to/lvis /path/to/lvis_subset 500 100 -s 0
    ```

    The random seed value `-s 0` is optional but guarantees reproducibility.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "src", type=pathlib.Path, help="Path to source dataset root"
    )
    parser.add_argument(
        "dst", type=pathlib.Path, help="Path to destination dataset root"
    )
    parser.add_argument(
        "num_train", type=int, help="Number of training images to sample"
    )
    parser.add_argument(
        "num_test", type=int, help="Number of test images to sample"
    )
    parser.add_argument("-s", "--seed", type=int, help="Random seed")
    args = parser.parse_args()

    if args.seed is not None:
        random.seed(args.seed)

    sample_dataset(args.src, args.dst, args.num_train, args.num_test)
