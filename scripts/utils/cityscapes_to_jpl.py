"""
Utility for converting Cityscapes dataset (https://www.cityscapes-dataset.com)
to JPL format. From https://www.cityscapes-dataset.com/downloads/, download:

- gtCoarse.zip
- leftImg8bit_trainvaltest.zip
- leftImg8bit_trainextra.zip

and unzip all of them in a single directory. Download requires registration.
The directory structure of the unzipped files should be as follows:

src_dir
├── gtCoarse
│   ├── train
│   ├── train_extra
│   └── val
└── leftImg8bit
    ├── test
    ├── train
    ├── train_extra
    └── val

where each of the above leaf directories contains subdirectories for each
city (and each city directory contains annotations/images).
"""
import argparse
import concurrent.futures
import glob
import json
import pathlib
import re
import shutil
from typing import List, Tuple

import pandas as pd


def chunk_list(list_, num_chunks):
    """
    From https://gist.github.com/nrsyed/3ccdd9dc25e4114ddaed7fd8a73e57a4
    """
    div, rem = divmod(len(list_), num_chunks)
    if div == 0:
        chunk_size = 1
    elif rem == 0:
        chunk_size = div
    else:
        chunk_size = div + 1

    for i in range(0, len(list_), chunk_size):
        yield list_[i:i+chunk_size]


def convert_image(
    json_fpath: pathlib.Path,
    img_fpath: pathlib.Path,
    img_dst_dir: pathlib.Path = None,
    num_workers: int = None
) -> Tuple[List[str], List[str], List[str]]:
    """
    Args:
        img_dst_dir: where to copy the image

    Returns:
    """
    img_fname = str(img_fpath.name)

    with open(json_fpath, "r") as f:
        annotations = json.load(f)

    jpl_ids = []
    jpl_bboxes = []
    jpl_classes = []

    for obj in annotations["objects"]:
        jpl_ids.append(img_fname)
        jpl_classes.append(obj["label"])

        x_pts = [pt[0] for pt in obj["polygon"]]
        y_pts = [pt[1] for pt in obj["polygon"]]

        tl_x = min(x_pts)
        tl_y = min(y_pts)
        br_x = max(x_pts)
        br_y = max(y_pts)

        bbox = [tl_x, tl_y, br_x, br_y]
        jpl_bbox = ", ".join([str(coord) for coord in bbox])
        jpl_bboxes.append(jpl_bbox)

    if img_dst_dir is not None:
        new_img_fpath = img_dst_dir / img_fname

        if not new_img_fpath.exists():
            shutil.copy(img_fpath, new_img_fpath)

    return jpl_ids, jpl_bboxes, jpl_classes


def _convert_dataset(images: List[dict]):
    """
    Helper function for func:`convert_dataset`. Processes a batch of images.

    Args:
        images: A list of dicts, where each dict contains the keyword args
            to be passed to func:`convert_image`.
    """
    jpl_ids = []
    jpl_bboxes = []
    jpl_classes = []

    for image in images:
        _jpl_ids, _jpl_bboxes, _jpl_classes = convert_image(**image)
        jpl_ids.extend(_jpl_ids)
        jpl_bboxes.extend(_jpl_bboxes)
        jpl_classes.extend(_jpl_classes)

    return jpl_ids, jpl_bboxes, jpl_classes


def convert_dataset(
    root_dir: pathlib.Path, dst_dir: pathlib.Path, num_workers: int = 12
):
    """
    Args:
        root_dir: Cityscapes dataset root directory containing coarse GT
            annotations (``gtCoarse``) and ``leftImg8bit`` subdirectories.
        dst_dir: Destination directory for converted files.
    """
    json_glob = root_dir / "gtCoarse/*/*/*.json"
    json_fpaths = glob.glob(str(json_glob))

    annotation_fname_expr = r"([\w-]+_\d+_\d+)_gtCoarse_polygons.json"

    # Mapping of JSON annotations files to their corresponding image filepaths.
    json_img_fpath_map = {}

    for _json_fpath in json_fpaths:
        json_fpath = pathlib.Path(_json_fpath)
        relpath = json_fpath.relative_to(root_dir)

        # From the annotation file relative path, construct the filepath of
        # the corresponding image file.
        subset = relpath.parents[1].name
        if subset not in ("train", "train_extra", "val"):
            raise RuntimeError(
                "Unknown subset (must be train, train_extra, or val)"
            )

        city = relpath.parent.name
        match = re.match(annotation_fname_expr, relpath.name)
        img_id = match.groups()[0]
        img_fname = f"{img_id}_leftImg8bit.png"
        img_fpath = root_dir / "leftImg8bit" / subset / city / img_fname

        if not img_fpath.exists():
            raise FileNotFoundError(f"{img_fpath} not found")

        json_img_fpath_map[json_fpath] = img_fpath

    img_dir = dst_dir / "cityscapes_full"
    train_img_dir = img_dir / "train"
    test_img_dir = img_dir / "test"

    img_dir.mkdir(exist_ok=True, parents=True)
    train_img_dir.mkdir(exist_ok=True)
    test_img_dir.mkdir(exist_ok=True)

    train_images = []
    test_images = []

    for json_fpath, img_fpath in json_img_fpath_map.items():
        if "train" in str(img_fpath):
            list_ = train_images
            img_dst_dir = train_img_dir
        elif "val" in str(img_fpath):
            list_ = test_images
            img_dst_dir = test_img_dir
        else:
            raise RuntimeError(f"Invalid image filepath: {img_fpath}")

        image = {
            "json_fpath": json_fpath,
            "img_fpath": img_fpath,
            "img_dst_dir": img_dst_dir,
        }
        list_.append(image)

    # Use multiprocessing and submit batches of training set images to the
    # process pool. We use a single "batch" for the test set because it only
    # contains 500 images (compared to 22973 training images).
    pool = concurrent.futures.ProcessPoolExecutor(max_workers=num_workers)

    train_futures = []
    for train_image_subset in chunk_list(train_images, num_workers):
        future = pool.submit(_convert_dataset, train_image_subset)
        train_futures.append(future)

    test_future = pool.submit(_convert_dataset, test_images)
    pool.shutdown()

    train_jpl_ids = []
    train_jpl_bboxes = []
    train_jpl_classes = []

    for future in train_futures:
        _jpl_ids, _jpl_bboxes, _jpl_classes = future.result()
        train_jpl_ids.extend(_jpl_ids)
        train_jpl_bboxes.extend(_jpl_bboxes)
        train_jpl_classes.extend(_jpl_classes)

    train_df = pd.DataFrame(
        {
            "id": train_jpl_ids,
            "bbox": train_jpl_bboxes,
            "class": train_jpl_classes,
        }
    )

    test_jpl_ids, test_jpl_bboxes, test_jpl_classes = test_future.result()
    test_df = pd.DataFrame(
        {
            "id": test_jpl_ids,
            "bbox": test_jpl_bboxes,
            "class": test_jpl_classes
        }
    )

    labels_dir = dst_dir / "labels_full"
    labels_dir.mkdir(exist_ok=True)

    train_labels_fpath = labels_dir / "labels_train.feather"
    test_labels_fpath = labels_dir / "labels_test.feather"

    train_df.to_feather(train_labels_fpath)
    test_df.to_feather(test_labels_fpath)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "src_dir", type=pathlib.Path,
        help="Path to directory containing unzipped cityscapes data "
        "directories (gtCoarse, leftImg8bit)"
    )
    parser.add_argument(
        "dst_dir", type=pathlib.Path,
        help="Path to destination root directory "
        "(e.g., ~/lwll_datasets/external/cityscapes)"
    )
    parser.add_argument(
        "-n", "--num-workers", type=int, default=None,
        help="Number of processes to spawn for parallel conversion"
    )
    args = parser.parse_args()
    convert_dataset(args.src_dir, args.dst_dir, num_workers=args.num_workers)
