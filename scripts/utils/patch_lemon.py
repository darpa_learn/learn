import json
from osgeo import gdal
from tqdm import tqdm
import os
import cv2
from math import ceil
import numpy as np

base_dir = '/data/projects/vigilant/lemon'
files = {
    'train' : 'lemon_v0.6.6.mscoco_train.json',
    'val' : 'lemon_v0.6.6.mscoco_vali.json',
    'test' : 'lemon_v0.6.6.mscoco_test.json'
}

height = 512
width = 512
overlap_perc = 0.0
imgdir = 'output_images'
if not os.path.exists(imgdir):
    os.mkdir(imgdir)

class Box:
    def __init__(self, x1=0, y1=0, x2=0, y2=0, data={}):
        if x1 > x2 or y1 > y2:
            raise ValueError("Wrong rectangle coords")
        self.x1, self.y1, self.x2, self.y2 = x1, y1, x2, y2
        self.data = data

    def area(self):
        return (self.x2 - self.x1) * (self.y2 - self.y1)

    def shape(self):
        return int(self.y2 - self.y1), int(self.x2 - self.x1)

    def move(self, dx, dy):
        self.x1, self.x2 = self.x1 + dx, self.x2 + dx
        self.y1, self.y2 = self.y1 + dy, self.y2 + dy

    def to_ints(self):
        self.x1, self.y1, self.x2, self.y2 = (
            int(self.x1),
            int(self.y1),
            int(self.x2),
            int(self.y2),
        )

    def collapse(self, dc=1):
        self.x1, self.x2 = self.x1 + dc, self.x2 - dc
        self.y1, self.y2 = self.y1 + dc, self.y2 - dc

    def as_tuple(self):
        return self.x1, self.y1, self.x2, self.y2

    def is_inside(self, frame):
        return (
            self.x1 >= frame.x1
            and self.y1 >= frame.y1
            and self.x2 <= frame.x2
            and self.y2 <= frame.y2
        )

    def intersection(self, other, data={}):
        a, b = self, other
        x1 = max(min(a.x1, a.x2), min(b.x1, b.x2))
        y1 = max(min(a.y1, a.y2), min(b.y1, b.y2))
        x2 = min(max(a.x1, a.x2), max(b.x1, b.x2))
        y2 = min(max(a.y1, a.y2), max(b.y1, b.y2))
        if x1 < x2 and y1 < y2:
            res = Box(x1, y1, x2, y2, data=data)
            return res
        else:
            return None

    @staticmethod
    def _split(beg, end, side, overlap):
        length = end - beg
        step = side - overlap
        fl_n = float(length) / step
        n = int(fl_n)
        if n <= 1 or (length - side) <= 2.0:
            return [(int(beg), int(end))]
        fl_step = float(length - side) / (n - 1)
        return [
            (int(x), int(x) + side) for x in np.arange(beg, end - side + 0.01, fl_step)
        ]

    def split(self, size=(512, 512), overlap=(100, 100), **kwargs):
        x_side, y_side = size
        x_overlap, y_overlap = overlap

        return [
            Box(x1, y1, x2, y2, data=self.data)
            for x1, x2 in self._split(self.x1, self.x2, x_side, x_overlap)
            for y1, y2 in self._split(self.y1, self.y2, y_side, y_overlap)
        ]


class Tile:
    def __init__(self, box, tile_boxes=[]):
        self.box = box
        self.tile_boxes = tile_boxes

    def check_nested(self):
        for b in self.tile_boxes:
            if not b.is_inside(self.box):
                print(self.box.as_tuple())
                print(b.as_tuple())
                raise ValueError("Box is outside of tile")

    def move_to_origin(self):
        dx = -self.box.x1
        dy = -self.box.y1
        self.box.move(dx, dy)
        for b in self.tile_boxes:
            b.move(dx, dy)

    def to_ints(self):
        self.box.to_ints()
        for x in self.tile_boxes:
            x.to_ints()

    def collapse(self, dc_box=0, dc_nested=1):
        self.box.collapse(dc_box)
        for x in self.tile_boxes:
            x.collapse(dc_nested)

    def intersection(self, other, box_crop_threshold=0.7, greater=True, **kwargs):
        box_common = self.box.intersection(other)
        if box_common is None:
            return None

        nested_intersections = []
        for x in self.tile_boxes:
            isect = box_common.intersection(x, data=x.data)
            if isect is None:
                continue
            isect_area = isect.area()
            x_area = x.area()
            is_area_greater_th = isect_area >= x_area * box_crop_threshold
            if greater == is_area_greater_th:
                nested_intersections.append(isect)

        res = Tile(box_common, nested_intersections)
        res.check_nested()
        return res

    def split(self, **kwargs):
        parts = self.box.split(**kwargs)
        return [self.intersection(r, greater=True, **kwargs) for r in parts]

def imsave(filepath, img):
    os.makedirs(os.path.dirname(filepath), exist_ok=True)
    cv2.imwrite(filepath, img)

def add_tile_num(path, suffix):
    root, ext = os.path.splitext(path)
    return root + suffix + ext


def xywh2xyxy(xywhs):
    xyxys = []
    for bbox in xywhs:
        x1, y1, w, h = bbox
        xyxy = [x1, y1, x1 + w, y1 + h]
        xyxys.append(xyxy)
    return xyxys

def split_image(img, bboxes, **kwargs):
    tiles = Tile(Box(0, 0, img.shape[1], img.shape[0]), bboxes).split(**kwargs)

    img_tiles = []
    for tile in tiles:
        tile.to_ints()
        tile.collapse(dc_box=0, dc_nested=1)
        img_tile = get_crop(img, tile.box)
        tile.move_to_origin()
        img_tiles.append(img_tile)

    return zip(img_tiles, tiles)

def split_on_tiles(img, bboxes, **kwargs):

    h_img, w_img = img.shape[:2]
    h_step = height
    w_step = width
    h_overlap = int(h_step * overlap_perc) 
    w_overlap = int(w_step * overlap_perc)
    kwargs["overlap"] = (w_overlap, h_overlap)
    kwargs["size"] = (w_step + w_overlap, h_step + h_overlap)

    return split_image(img, bboxes, **kwargs)

def get_crop(image, box):
    return image[box.y1:box.y2, box.x1:box.x2, :].copy()

def get_annotation(image_id, annotation_id, coordinates, cat_id):
    # Count area
    x1, y1, x2, y2 = coordinates
    w = x2 - x1
    h = y2 - y1
    area = w * h
    # bbox to x,y,w,h coco format
    bbox = [x1, y1, w, h]
    # check boxes
    if (min(bbox) < 0) or (area <= 0):
        return
    return dict(
        area=area,
        bbox=','.join(str(x) for x in bbox).strip(),
        category_id=cat_id,
        id=annotation_id,
        image_id=image_id,
        iscrowd=0,
        segmentation=[],
    )

for phase, filename in files.items():
    with open(os.path.join(base_dir, filename)) as f:
        ann_json = json.load(f)

    if not os.path.exists(os.path.join(imgdir,phase)):
        os.mkdir(os.path.join(imgdir,phase))
    # create empty coco-like dataset
    last_img_idx = 0
    last_ann_idx = 0
    coco_dataset_dict = {
        'type': 'type',
        'categories': ann_json['categories'],
        'images': [],
        'annotations': []
    }

    # split original images and anns on tiles
    for image in tqdm(ann_json['images']):
        img = gdal.Open(os.path.join(base_dir,'images', image['file_name'])).ReadAsArray().transpose(1,2,0)
        img_anns = [
            x for x in ann_json['annotations'] if x['image_id'] == image['id']
        ]
        bboxes_xyxy = xywh2xyxy([x['bbox'] for x in img_anns])
        bboxes = []
        for ann_i in range(len(img_anns)):
            bboxes.append(Box(
                *bboxes_xyxy[ann_i],
                data=dict(category_id=img_anns[ann_i]['category_id'])
            ))

        # get tiles
        tiles = split_on_tiles(img, bboxes)
        tiles_counter = 0
        for tile_img, tile_ann in tiles:
            dst_img_path = os.path.join(os.path.join(imgdir, phase) , image['file_name'])
            dst_img_full_path = add_tile_num(dst_img_path,
                                             "_p" + str(tiles_counter))
            file_name = add_tile_num(image['file_name'],
                                     "_p" + str(tiles_counter))

            if len(tile_ann.tile_boxes) == 0:
            #    print('skip')
                continue

            # append to images
            coco_dataset_dict["images"].append(
                dict(
                    license=1,
                    file_name=file_name.replace('.ptif','.png'),
                    height=tile_img.shape[0],
                    width=tile_img.shape[1],
                    id=last_img_idx,
                ))

            # append annotations
            for tile_box in tile_ann.tile_boxes:
                box_annotation = get_annotation(last_img_idx, last_ann_idx,
                                        tile_box.as_tuple(), 
                                        tile_box.data['category_id'])
                if not box_annotation:
                    print("No boxes")
                    continue
                coco_dataset_dict["annotations"].append(box_annotation)
                last_ann_idx += 1

            last_img_idx += 1
            tiles_counter += 1
            imsave(dst_img_full_path.replace('.ptif','.png'), tile_img)
    with open(f'tiled_images_withanns.{phase}_mscoco.json', 'w') as f:
        json.dump(coco_dataset_dict, f, sort_keys=True, indent=4)
