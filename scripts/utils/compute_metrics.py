"""
This module contains refactored and slightly modified versions of functions
from the JPL metrics code in ``learn.utils.metrics``.
"""
import argparse
import json
import logging
import pathlib
from pprint import pprint
import sys
from typing import Dict, Tuple, Union

import numpy as np
import pandas as pd

import learn.utils.metrics

import nms


logger = logging.getLogger(__name__)

if __name__ == "__main__":
    log_fmt = "[%(levelname)s][%(module)s.%(funcName)s] %(msg)s"
    logging.basicConfig(level=logging.DEBUG, format=log_fmt)

try:
    import kwcoco
    from kwcoco.coco_evaluator import CocoEvaluator
except ModuleNotFoundError:
    logger.warning("Module kwcoco not found")


def voc_ap(rec: list, prec: list) -> Tuple[float, list, list]:
    """
    This function has been copied verbatim from the JPL code.
    """
    rec.insert(0, 0.0)  # insert 0.0 at begining of list
    rec.append(1.0)  # insert 1.0 at end of list
    mrec = rec[:]

    prec.insert(0, 0.0)  # insert 0.0 at begining of list
    prec.append(0.0)  # insert 0.0 at end of list
    mpre = prec[:]
    # This part makes the precision monotonically decreasing

    for i in range(len(mpre)-2, -1, -1):
        mpre[i] = max(mpre[i], mpre[i+1])
    """
     This part creates a list of indexes where the recall changes
        matlab: i=find(mrec(2:end)~=mrec(1:end-1))+1;
    """

    i_list = []
    for i in range(1, len(mrec)):
        if mrec[i] != mrec[i-1]:
            i_list.append(i)  # if it was matlab would be i + 1
    """
     The Average Precision (AP) is the area under the curve
        (numerical integration)
        matlab: ap=sum((mrec(i)-mrec(i-1)).*mpre(i));
    """
    ap = 0.0
    for i in i_list:
        ap += ((mrec[i]-mrec[i-1])*mpre[i])
    return ap, mrec, mpre


def mAP(
    df: pd.DataFrame, actuals: pd.DataFrame, min_overlap: float = 0.5
) -> Tuple[float, dict]:
    """
    This function combines:
    `learn.utils.metrics.mAP`, `learn.utils.metrics.mean_average_precision`,
    and `learn.utils.metrics.calculate_mAP`, which are arbitrarily,
    confusingly, and redundantly structured/named in the original JPL code.

    Args:
        df: Predictions DataFrame.
        actuals: Ground truth DataFrame.
        min_overlap: IOU threshold for prediction/GT matching.
    """

    ### This portion is from learn.utils.metrics.mAP
    learn.utils.metrics._validate_input_ids_many_to_many(df, actuals)
    _df = learn.utils.metrics.format_obj_detection_data(df)
    _actuals = learn.utils.metrics.format_obj_detection_data(actuals)

    ### This portion is from learn.utils.metrics.mean_average_precision
    gt_stats = learn.utils.metrics.populate_gt_stats(_actuals)

    # NOTE: out_dict maps image ids (filename) to lists of GT objects,
    # where each GT object contains class_name and bbox.
    out_dict = gt_stats[0]
    gt_counter_per_class = gt_stats[1]
    counter_images_per_class = gt_stats[2]
    gt_classes = gt_stats[3]
    n_classes = gt_stats[4]

    out_preds = learn.utils.metrics.prep_preds(_df, gt_classes)

    ### The remainder is from learn.utils.metrics.calculate_mAP

    # Add "used" key/bool to out_dict GT instances.
    for obj_list in out_dict.values():
        for obj in obj_list:
            obj["used"] = False
            obj["bbox"] = [float(x) for x in obj["bbox"].split()]

            bbox = obj["bbox"]
            obj["area"] = (bbox[2] - bbox[0] + 1) * (bbox[3] - bbox[1] + 1)

    sum_AP = 0.0
    ap_dictionary = {}
    #lamr_dictionary = {}
    count_true_positives = {}
    for class_index, class_name in enumerate(gt_classes):
        count_true_positives[class_name] = 0

        # out_preds is a dict mapping class name to list of detections for
        # that class, where each detection is a dict containing confidence,
        # image id (filename), and bbox.
        dr_data = out_preds[class_name]
        tp = np.zeros(len(dr_data))
        fp = np.zeros(len(dr_data))

        # Iterate over all detections. For each detection, iterate over all
        # GT objects (in the same image) of the same class and compute IOU
        # w/each GT object. Choose the GT obj with which it has the greatest
        # overlap. If that GT obj has not already been assigned to a detection,
        # update `tp` and increment class TP count. Else (if no GT match was
        # found or the GT match IOU was below the overlap threshold, or if the
        # GT match exceeded the threshold but was previously used/assigned to
        # a different detection, i.e., it represents a duplicate detection),
        # count it as a FP.
        for idx, detection in enumerate(dr_data):
            file_id = detection["file_id"]
            bb = [float(x) for x in detection["bbox"].split()]

            ovmax = -1
            gt_match = None

            for gt_obj in out_dict[file_id]:
                if gt_obj["class_name"] == class_name:
                    bbgt = gt_obj["bbox"]
                    bi = [max(bb[0], bbgt[0]), max(bb[1], bbgt[1]),
                          min(bb[2], bbgt[2]), min(bb[3], bbgt[3])]
                    iw = bi[2] - bi[0] + 1
                    ih = bi[3] - bi[1] + 1
                    if iw > 0 and ih > 0:
                        # compute overlap (IoU)
                        det_area = (bb[2] - bb[0] + 1) * (bb[3] - bb[1] + 1)
                        gt_area = gt_obj["area"]
                        union = det_area + gt_area - (iw * ih)
                        iou = iw * ih / union

                        if iou > ovmax:
                            ovmax = iou  # type: ignore
                            gt_match = gt_obj

            if ovmax >= min_overlap and not gt_match["used"]:
                tp[idx] = 1
                gt_match["used"] = True
                count_true_positives[class_name] += 1
            else:
                fp[idx] = 1

        # compute precision/recall
        fp = np.cumsum(fp)
        tp = np.cumsum(tp)

        gt_count = gt_counter_per_class[class_name]
        rec = tp / gt_count
        prec = tp / (tp + fp)

        ap, mrec, mprec = voc_ap(rec.tolist(), prec.tolist())
        sum_AP += ap
        ap_dictionary[class_name] = ap

        #n_images = counter_images_per_class[class_name]
        #lamr, mr, fppi = learn.utils.metrics.log_average_miss_rate(
        #    rec, fp, n_images)
        #lamr_dictionary[class_name] = lamr

    mAP_ = round(sum_AP / n_classes, 2)
    return mAP_, ap_dictionary


def load_annotations(path: pathlib.Path) -> pd.DataFrame:
    """
    Args:
        path: Path to ground truth feather file OR detections (predictions)
            JSON file output by the learn framework local interface.
    """
    if path.suffix == ".json":
        anns_json = json.load(open(path, "r"))
        df = pd.DataFrame.from_dict(anns_json)
    elif path.suffix == ".feather":
        df = pd.read_feather(path)
    return df


def to_kwcoco(df: pd.DataFrame) -> kwcoco.CocoDataset:
    coco = kwcoco.CocoDataset()

    for class_name in sorted(df["class"].unique()):
        cid = coco.add_category(class_name)

    for image_fname in sorted(df["id"].unique()):
        gid = coco.add_image(image_fname)

    for _, row in df.iterrows():
        image_fname = row["id"]
        class_name = row["class"]

        gid = coco.index.file_name_to_img[image_fname]["id"]
        cid = coco.index.name_to_cat[class_name]["id"]

        x1, y1, x2, y2 = [float(val) for val in row["bbox"].split(",")]
        w = x2 - x1
        h = y2 - y1
        bbox = [x1, y1, w, h]

        annotation_kwargs = {
            "image_id": gid,
            "category_id": cid,
            "bbox": bbox,
        }

        if "confidence" in row:
            #annotation_kwargs["score"] = row["confidence"]
            annotation_kwargs["score"] = 1.0

        coco.add_annotation(**annotation_kwargs)

    return coco


def filter_bboxes_by_size(df: pd.DataFrame):
    class_to_areas = {class_: [] for class_ in df["class"].unique()}
    areas = []

    for _, row in df.iterrows():
        x1, y1, x2, y2 = [float(coord) for coord in row["bbox"].split(",")]
        class_ = row["class"]
        area = (x2 - x1 + 1) * (y2 - y1 + 1)
        areas.append(area)
        class_to_areas[class_].append(area)
    df["area"] = areas

    filtered_dfs = []
    for class_, areas_ in class_to_areas.items():
        q25, q75 = [float(val) for val in np.percentile(areas_, [25, 75])]
        iqr = q75 - q25
        outlier_thresh = 1.2 * iqr
        outlier_min = q25 - outlier_thresh
        outlier_max = q75 + outlier_thresh
        cond = (
            (df["class"] == class_)
            & (df["area"] >= outlier_min)
            & (df["area"] <= outlier_max)
        )
        df_ = df.loc[cond].drop(columns="area")
        filtered_dfs.append(df_)
    df = pd.concat(filtered_dfs)
    return df


def compute_metrics(
    gt: pd.DataFrame, dets: pd.DataFrame, backend: str = "default"
) -> Union[float, dict]:
    """
    Compute mAP (and per-class AP, depending on the backend).

    Args:
        gt: Ground truth labels DataFrame.
        dets: Detections DataFrame.
        backend: "default" (uses the mAP function defined in this script),
            "jpl" (uses the original JPL LEARN metrics code), or "kwcoco".
            "default" and "jpl" are functionally identical under the hood
            but "default" (:func:`mAP`) returns per-class AP in addition to
            mAP. "kwcoco" calls the kwcoco backend with sklearn-style
            metrics evaluation and returns both mAP and per-class AP.
    """
    ret = None
    if backend == "default":
        mAP_, per_class_AP = mAP(dets, gt)
        ret = {
            "mAP": mAP_,
            "per_class": per_class_AP,
        }
    elif backend == "jpl":
        mAP_ = learn.utils.metrics.mAP(dets, gt)
        ret = mAP_
    elif backend == "kwcoco":
        gt = to_kwcoco(gt)
        dets = to_kwcoco(dets)
        eval_config = {
            "true_dataset": gt,
            "pred_dataset": dets,
            "ap_method": "sklearn",
        }
        coco_eval = CocoEvaluator(eval_config)
        coco_eval._verbose = 0
        results = coco_eval.evaluate()
        result = list(results.values())[0]
        class_measures = result.ovr_measures.to_dict()

        per_class_AP = {}
        for class_name, measures in class_measures.items():
            per_class_AP[class_name] = measures["ap"]
        mAP_ = sum(per_class_AP.values()) / len(per_class_AP)
        ret = {
            "mAP": mAP_,
            "per_class": per_class_AP,
        }
    else:
        raise ValueError(f"Invalid backend: {backend}")
    return ret


def compute_metrics_per_image(
    gt: pd.DataFrame, dets: pd.DataFrame, backend: str = "default",
    use_tqdm: bool = True
) -> dict:
    """
    Compute mAP (and per-class AP, depending on the backend) separately for
    each image in the dataset.

    Args:
        gt: See :func:`compute_metrics`.
        dets: See :func:`compute_metrics`.
        backend: See :func:`compute_metrics`.
        use_tqdm: Display progress bar with tqdm.

    Returns: A dict mapping image filenames to the metrics for each image.
    """
    metrics = {}
    image_fnames = dets["id"].unique()

    if use_tqdm:
        from tqdm import tqdm
        sys.stdout.flush()
        image_fnames = tqdm(image_fnames, unit="image")

    for image_fname in image_fnames:
        gt_ = gt.loc[gt["id"] == image_fname]
        dets_ = dets.loc[dets["id"] == image_fname]
        metrics_ = compute_metrics(
            gt_, dets_, backend=backend
        )
        metrics[image_fname] = metrics_

    return metrics


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-b", "--backend", type=str, choices=["default", "jpl", "kwcoco"],
        default="default",
        help="The metrics backend to use: "
        "default (a version of the JPL code modified to return per-class AP "
        "in addition to mAP), jpl (the original JPL code, which only returns "
        "mAP), kwcoco (kwcoco evaluator with scikit-learn AP computation; "
        "returns both mAP and per-class AP)"
    )
    parser.add_argument(
        "-d", "--det", type=pathlib.Path, help="Path to detections JSON file"
    )
    parser.add_argument(
        "-g", "--gt", type=pathlib.Path,
        help="Path to ground truth feather or JSON file"
    )
    parser.add_argument(
        "-i", "--per-image", type=pathlib.Path, metavar="<path>",
        help="Compute per-image metrics and write the result to <path>"
    )
    parser.add_argument(
        "-n", "--nms-iou-thresh", type=float, nargs="?", const=0.3,
        default=None,
        help="Perform NMS on detections with the specified IOU threshold "
        "(default 0.3)"
    )
    parser.add_argument(
        "-D", "--duplicate-detections", type=int, default=None,
        metavar="<N>", help="Duplicate detections N times before computing mAP"
    )
    args = parser.parse_args()

    gt = load_annotations(args.gt)
    dets = load_annotations(args.det)

    if args.duplicate_detections:
        dets_ = pd.DataFrame(
            np.repeat(dets.values, args.duplicate_detections + 1, axis=0)
        )
        dets_.columns = dets.columns
        dets = dets_

    if args.nms_iou_thresh:
        logger.info(
            f"Performing NMS with IOU threshold = {args.nms_iou_thresh}"
        )
        num_orig_dets = len(dets)
        dets = nms.non_max_suppression(
            dets, iou_thresh=args.nms_iou_thresh, use_tqdm=True,
            per_class=True
        )
        num_removed = num_orig_dets - len(dets)
        logger.info(
            f"Eliminated {num_removed} of {num_orig_dets} original predictions"
        )

    overall_metrics = compute_metrics(gt, dets, backend=args.backend)

    if args.per_image:
        logger.info("Computing per image metrics")
        per_image_metrics = compute_metrics_per_image(
            gt, dets, backend=args.backend
        )
        metrics = {
            "overall": overall_metrics,
            "per_image": per_image_metrics,
        }
        with open(args.per_image, "w") as f:
            json.dump(metrics, f, indent=2)
        logger.info(f"Wrote metrics to {args.per_image}")
    else:
        pprint(overall_metrics)
