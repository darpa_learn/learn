"""
JPL baselines are run using the repo at
https://gitlab.lollllz.com/bjohnson/lwll_baselines

Image classification baselines are obtained via two steps:

1) Within that repo, the script api/ic_get_labels.py retrieves data on IC tasks
from the JPL server and generates JSON files containing all filenames for
each checkpoint of each task/dataset. It is not designed to work on local
files.

2) After these JSON files have been generated, image_classification/run.sh
is used to run the baseline on the desired tasks.

This script replaces step 1; it takes a local task config and generates a
JSON file (for the task's base dataset). That JSON file can be used for step 2.
"""
import argparse
import collections
import json
import pathlib
import random
from typing import Dict, List

import numpy as np
import pandas as pd


rand_seed = 123

np.random.seed(rand_seed)
random.seed(rand_seed)


def make_checkpoints(
    config: pathlib.Path, datasets_dir: pathlib.Path
) -> Dict[int, List[str]]:
    """
    Args:
        config: Path to task config (e.g.,
            `learn/configs/local_task_configs/{task}.json`) whose base dataset
            we wish to generate baselines repo–compatible checkpoints for.
            This function expects the config to contain 4 seed budgets and 4
            "non-deterministic" label budgets.
        datasets_dir: Path to LWLL datasets root (e.g.,
            `/data/lwll_datasets/external`).

    Returns:
        A dict where each key is an integer checkpoint and each value is a
        list of filenames for that checkpoint.
    """
    task_config = json.load(open(config, "r"))
    base_stage = None
    for stage in task_config["stages"]:
        if stage["name"] == "base":
            base_stage = stage
    dataset = base_stage["dataset"]

    labels_dir = datasets_dir / dataset / "labels_full"
    train_labels_path = labels_dir / "labels_train.feather"
    train_labels = pd.read_feather(train_labels_path)
    num_classes = len(train_labels["class"].unique())

    # Construct the list of budgets for each checkpoint, i.e., total number
    # of labels for each checkpoint.
    base_budgets = []
    for seed_budget in base_stage["seed_budgets"]:
        base_budgets.append(seed_budget * num_classes)
    base_budgets.extend(base_stage["label_budget"])

    # Generate seed labels for first 4 checkpoints ("deterministic"
    # checkpoints).
    class_to_fnames = collections.defaultdict(list)
    for _, row in train_labels.iterrows():
        class_ = row["class"]
        fname = row["id"]
        class_to_fnames[class_].append(fname)

    for fnames in class_to_fnames.values():
        random.shuffle(fnames)

    # Deterministic seeds (variable name taken from original baselines script).
    dseeds = []
    last_seed_budget = 0
    for seed_budget in base_stage["seed_budgets"]:
        labels_per_class = seed_budget - last_seed_budget
        dseed = []

        for class_, fnames in class_to_fnames.items():
            # Grab the first N items from the list of fnames and append them
            # to dseed. Update the list of fnames to exclude those items. This
            # will work with empty lists.
            dseed.extend(fnames[:labels_per_class])
            class_to_fnames[class_] = fnames[labels_per_class:]

        dseeds.append(dseed)
        last_seed_budget = seed_budget

    # Fill out dseed checkpoints that don't have enough labels; this will occur
    # if there are classes with fewer instances than the specified seed
    # budget(s), i.e., if the task's seed budgets are [1, 2, 4, 8] and there
    # are any classes with fewer than 2, 4, or 8 instances in the train set.
    used_fnames = set()
    for dseed in dseeds:
        used_fnames.update(dseed)

    # Shuffle list of remaining (unused) filenames and pad each dseed
    # checkpoint containing insufficient labels.
    fnames = list(set(train_labels["id"]) - used_fnames)

    last_budget = 0
    for i, dseed in enumerate(dseeds):
        budget = base_budgets[i]
        num_labels = budget - last_budget

        num_labels_to_add = num_labels - len(dseed)
        if num_labels_to_add > 0:
            dseed.extend(fnames[:num_labels_to_add])
            fnames = fnames[num_labels_to_add:]
        last_budget = budget

    # Follow the same procedure as api_helpers.sample_checkpoints from the
    # lwll_baselines repo to generate the remaining checkpoints, even if some
    # of the steps are redundant.
    filenames = np.sort(train_labels.id.values)
    for dseed in dseeds:
        filenames = filenames[~np.in1d(filenames, dseed)]

    filenames = np.hstack(dseeds + [np.random.permutation(filenames)])
    checkpoints = {
        idx: filenames[:budget].tolist()
        for idx, budget in enumerate(base_budgets)
    }

    return checkpoints


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--config", type=pathlib.Path,
        help="Path to local task config file",
        default="../../configs/local_task_configs/viame_fish_v2.json"
    )
    parser.add_argument(
        "-d", "--datasets-dir", type=pathlib.Path,
        help="Path to datasets directory",
        default="/data/datasets/lwll_datasets/external"
    )
    parser.add_argument(
        "-o", "--output", type=pathlib.Path, help="Path to output JSON file",
        required=True
    )
    args = parser.parse_args()

    checkpoints = make_checkpoints(args.config, args.datasets_dir)
    json.dump(checkpoints, open(args.output, "w"), indent=2)
