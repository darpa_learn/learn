"""
This module contains functions for performing non-maximum suppression on
object detection results AFTER inference. It is unrelated to any NMS settings
for detectron.
"""
import collections
import json
import logging
import pathlib
import sys
from typing import Union

import numpy as np
import pandas as pd


logger = logging.getLogger(__name__)


def non_max_suppression(
    dets: Union[pd.DataFrame, dict], iou_thresh: float = 0.3,
    per_class: bool = True, sort_by: str = "confidence",
    use_tqdm: bool = False
) -> pd.DataFrame:
    """
    Perform non-max suppression on all images in a DataFrame of detections,
    either on a per-class basis or neglecting class.

    Args:
        dets: Either a dict or DataFrame of JPL-format detections.
        iou_thresh: NMS IOU threshold.
        per_class: Perform per-class NMS; if ``False``, class is ignored.
        sort_by: See :func:`_non_max_suppression`.
        use_tqdm: Display progress with ``tqdm``.

    Returns: DataFrame after NMS has been performed.
    """
    if isinstance(dets, dict):
        df = pd.DataFrame.from_dict(dets)
    elif isinstance(dets, pd.DataFrame):
        df = dets

    class_names = df["class"].unique()

    if per_class:
        logger.info(
            f"Performing per-class NMS with {len(class_names)} classes"
        )
    else:
        logger.info("Performing NMS (ignoring class)")

    dataframes = []

    image_fnames = df["id"].unique()
    if use_tqdm:
        from tqdm import tqdm
        # flush stdout to ensure log messages are printed correctly.
        sys.stdout.flush()
        image_fnames = tqdm(image_fnames, unit="image")

    for fname in image_fnames:
        df_ = df.loc[(df["id"] == fname)]

        if per_class:
            for class_ in class_names:
                class_df = df_.loc[(df["class"] == class_)]
                if not class_df.empty:
                    class_df = _non_max_suppression(class_df, iou_thresh, sort_by)
                    dataframes.append(class_df)
        elif not df_.empty:
            df_ = _non_max_suppression(df_, iou_thresh, sort_by)
            dataframes.append(df_)

    updated_df = pd.concat(dataframes)
    return updated_df


def _non_max_suppression(
    df: pd.DataFrame, iou_thresh: float, sort_by: str = "confidence"
) -> pd.DataFrame:
    """
    Helper function that performs non-max suppression on a DataFrame
    representing detections for a single image.

    Args:
        df: Detections DataFrame for a single image.
        iou_thresh: NMS IOU threshold, i.e., the amount of overlap necessary
            for two detection bboxes to be considered duplicates.
        sort_by: "area", "confidence", or "left" (position, bbox tl_x).

    Returns: DataFrame after NMS has been performed.
    """
    bboxes = [
        [float(coord) for coord in bbox.split(",")]
        for bbox in df["bbox"].tolist()
    ]
    bboxes = np.array(bboxes)

    area = (
        ((bboxes[:, 2] - bboxes[:, 0]) + 1)
        * ((bboxes[:, 3] - bboxes[:, 1]) + 1)
    )

    if sort_by == "confidence":
        conf = np.array(df["confidence"])
        idxs = collections.deque(np.argsort(conf)[::-1])
    elif sort_by == "area":
        idxs = collections.deque(np.argsort(area)[::-1])
    elif sort_by == "left":
        idxs = collections.deque(np.argsort(bboxes[:, 0]))

    max_ious = []

    idxs_to_keep = []
    while idxs:
        # Grab current index (index corresponding to the detection with the
        # greatest probability currently in the list of indices).
        curr_idx = idxs.popleft()
        idxs_to_keep.append(curr_idx)

        # Find the coordinates of the regions of overlap between the current
        # detection and all other detections.
        overlaps_tl_x = np.maximum(bboxes[curr_idx, 0], bboxes[idxs, 0])
        overlaps_tl_y = np.maximum(bboxes[curr_idx, 1], bboxes[idxs, 1])
        overlaps_br_x = np.minimum(bboxes[curr_idx, 2], bboxes[idxs, 2])
        overlaps_br_y = np.minimum(bboxes[curr_idx, 3], bboxes[idxs, 3])

        # Compute width and height of overlapping regions.
        overlap_w = np.maximum(0, (overlaps_br_x - overlaps_tl_x) + 1)
        overlap_h = np.maximum(0, (overlaps_br_y - overlaps_tl_y) + 1)

        # Compute amount of overlap (intersection).
        inter = overlap_w * overlap_h
        union = area[curr_idx] + area[idxs] - inter
        iou = inter / union

        idxs_to_remove = [idxs[i] for i in np.where(iou >= iou_thresh)[0]]
        for idx in idxs_to_remove:
            idxs.remove(idx)

    return df.iloc[idxs_to_keep]


def main(
    src_fpath: pathlib.Path, dst_fpath: pathlib.Path,
    iou_thresh: float, sort_by: str
):
    """
    Read an object detection results JSON file, perform NMS, and write the
    output to another JSON file.

    Args:
        src_fpath: Path to JSON file of detection results.
        dst_fpath: Path to which the resulting detections should be written
            after NMS has been performed.
        iou_thresh: NMS IOU threshold.
        sort_by: See :func:`_non_max_suppression`.
    """
    dets = json.load(open(src_fpath, "r"))
    orig_len = len(dets["bbox"])

    dets = non_max_suppression(
        dets, iou_thresh=iou_thresh, sort_by=sort_by, use_tqdm=True
    )
    new_len = len(dets["bbox"])
    logger.info(f"Reduced {orig_len} predictions to {new_len} predictions")

    json.dump(dets, open(dst_fpath, "w"), indent=2)
    logger.info(f"Saved to {dst_fpath}")


if __name__ == "__main__":
    """
    src_dir = pathlib.Path("/data/najam.syed/learn/debug_stanford_campus")
    src_fpath = src_dir / "top_108_base_7.json"

    iou_thresh = 0.05
    sort_by = "confidence"
    dst_fname = f"{src_fpath.stem}_nms_{iou_thresh}_{sort_by}.json"
    dst_fpath = src_dir / dst_fname
    main(src_fpath, dst_fpath, iou_thresh, sort_by)
    """
    pass
