import argparse
import json
import os
import pathlib

import pandas as pd


def convert(dataset: dict) -> pd.DataFrame:
    """
    Args:
        dataset: LVIS train or val JSON dataset as a Python dict.

    Returns:
        A JPL-formatted pandas DataFrame.
    """
    cat_id_to_name = {}
    for cat in dataset["categories"]:
        cat_id_to_name[cat["id"]] = cat["name"]

    image_id_to_fname = {}

    for image in dataset["images"]:
        image_id = image["id"]
        coco_url = image["coco_url"]
        fname = os.path.split(coco_url)[1]
        image_id_to_fname[image_id] = fname

    # Lists that will be used to create the JPL formatted pandas dataframe.
    # Note that the "id" for a given annotation refers, in the JPL format, to
    # the image filename.
    jpl_ids = []
    jpl_bboxes = []
    jpl_classes = []

    for ann in dataset["annotations"]:
        image_id = ann["image_id"]
        image_fname = image_id_to_fname[image_id]

        # COCO/LVIS bboxes are xywh (top left x, top left y, width height).
        # JPL schema requires a comma-separated string in tlbr (top left x,
        # top left y, bottom right x, bottom right y) coordinates.
        x1, y1, w, h = ann["bbox"]
        x2 = x1 + w
        y2 = y1 + h
        bbox = [x1, y1, x2, y2]
        bbox = map(int, bbox)
        bbox = map(str, bbox)
        bbox_str = ", ".join(bbox)

        cat_id = ann["category_id"]

        #cat_name = cat_id
        cat_name = cat_id_to_name[cat_id]

        jpl_ids.append(image_fname)
        jpl_bboxes.append(bbox_str)
        jpl_classes.append(cat_name)

    df = pd.DataFrame(
        {"id": jpl_ids, "bbox": jpl_bboxes, "class": jpl_classes}
    )
    return df


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-t", "--train", type=pathlib.Path, help="Path to LVIS train JSON file"
    )
    parser.add_argument(
        "-v", "--val", type=pathlib.Path, help="Path to LVIS val JSON file"
    )
    parser.add_argument(
        "-o", "--output", type=pathlib.Path, required=True,
        help="Path to output directory for resulting feather file(s)"
    )
    args = parser.parse_args()

    if not args.train and not args.val:
        raise RuntimeError("Train and/or val dataset path(s) required")

    if args.train:
        train_json = json.load(open(args.train, "r"))
        train_df = convert(train_json)
        train_dst_fpath = args.output / "labels_train.feather"
        train_df.to_feather(train_dst_fpath)

    if args.val:
        test_json = json.load(open(args.val, "r"))
        test_df = convert(test_json)
        test_dst_fpath = args.output / "labels_test.feather"
        test_df.to_feather(test_dst_fpath)
