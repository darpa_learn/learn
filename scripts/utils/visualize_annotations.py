"""
Command line tool for drawing ground truth and/or prediction bboxes on images
from a dataset and writing the resulting images to disk. For detailed
instructions, refer to argparse parser and function documentation.

Example:
python visualize_annotations.py \
  -i ~/lwll_datasets/external/lvis/lvis_full/test \
  -g ~/lwll_datasets/external/lvis/labels_full/labels_test.feather \
  -d ~/learn/outputs/2021-08-14/10-25-45/lvis_results_base_1.json \
  -o ~/visualizations/lvis_test
"""
import argparse
import collections
import json
import pathlib
from typing import Dict, List, Tuple

import cv2
import numpy as np
import pandas as pd
from tqdm import tqdm


def load_annotations(
    path: pathlib.Path,
    det_conf_thresh: float = None
) -> Dict[str, List[dict]]:
    """
    Load ground truth or detection annotations. If detections, optionally
    filter by confidence.

    Args:
        path: Path to JSON file (output by learn framework), JPL format feather
            file, or JPL format DataFrame saved in a pickle file.
        det_conf_thresh: Confidence threshold if ``path`` corresponds to a
            detections file. If ``None``, detections will not be filtered.

    Returns: A dict mapping each filename to a list of its annotations,
        where each element in the list is a dict corresponding to an
        annotation, e.g.:

        {
            "image_filename1.jpg": [
                {
                    "bbox": [20, 0, 112, 36],
                    "class": "class1",
                    "confidence": 0.25
                },
                ...
            ],
            ...
        }

    .. note::
        JSON file must have a ".json" extension, feather file must have
        ".feather", and pickle file must have ".pkl".

    .. note::
        Detections are assumed to contain a "confidence" column/field, whereas
        ground truth annotations do not.
    """
    ext = path.suffix
    if ext == ".pkl":
        df = pd.read_pickle(path)
    elif ext == ".feather":
        df = pd.read_feather(path)
    elif ext == ".json":
        json_ = json.load(open(path, "r"))
        df = pd.DataFrame.from_dict(json_)
    else:
        raise ValueError("Extension must be one of pkl, feather, or json")

    if "confidence" in df and det_conf_thresh:
        df = df.loc[df["confidence"] >= det_conf_thresh]

    image_to_anns = collections.defaultdict(list)
    for row in df.itertuples():
        fname = row.id
        ann = {
            "bbox": [float(coord) for coord in row.bbox.split(",")],
            "class": row[-1],
        }

        if hasattr(row, "confidence"):
            ann["confidence"] = row.confidence

        image_to_anns[fname].append(ann)

    return image_to_anns


def draw_bboxes(
    img: np.array,
    annotations: List[dict],
    color: Tuple[int, int, int],
    categories: Dict[int, str] = None
) -> None:
    """
    Draw bboxes on an image array (in-place).

    Args:
        img: OpenCV (BGR) image array.
        annotations: List of annotations for the specified image/file, in
            the format returned by :func:`load_annotations`.
        color: BGR color of the bbox/text.
        categories: Optional (if the class names in ``annotations`` are
            integers instead of string names); mapping of integer class id
            to string class name.
    """
    for ann in annotations:
        bbox = [int(coord) for coord in ann["bbox"]]
        cv2.rectangle(img, bbox[:2], bbox[2:4], color, 2)

        class_name_text = str(ann["class"])
        if categories is not None:
            class_name_text = categories[int(ann["class"])]

        cv2.putText(
            img, class_name_text, bbox[:2], cv2.FONT_HERSHEY_SIMPLEX,
            0.75, color, thickness=1
        )


def write_visualization(
    image_dir: pathlib.Path,
    output_dir: pathlib.Path,
    det_path: pathlib.Path = None,
    gt_path: pathlib.Path = None,
    det_conf_thresh: float = 0.05,
    categories: Dict[int, str] = None
):
    """
    Plot ground truth or detections bboxes on images and write images to disk.

    Args:
        image_dir: Path to dataset image directory.
        output_dir: Path to directory where visualizations will be written.
        det_path: Path to detections JSON or pandas DataFrame feather/pickle
            file.
        gt_path: Path to ground truth JSON or pandas DataFrame feather/pickle
            file.
        det_conf_thresh: Detection confidence threshold (if ``det_path``
            provided).
        categories: If the annotations files contain integer class names
            instead of strings, bboxes will be displayed with the integer
            class ids UNLESS ``categories`` is provided. ``categories`` should
            be a dict mapping integer id to name.

    .. note::
        At least one of ``det_path`` or ``gt_path`` must be provided;
        otherwise, there's nothing to visualize.
    """
    if not det_path and not gt_path:
        raise ValueError("At least one of det_path or gt_path required")

    if not output_dir.exists():
        output_dir.mkdir(parents=True)

    det_fnames = set()
    if det_path:
        det_anns = load_annotations(det_path, det_conf_thresh=det_conf_thresh)
        det_fnames = set(det_anns.keys())

    gt_fnames = set()
    if gt_path:
        gt_anns = load_annotations(gt_path)
        gt_fnames = set(gt_anns.keys())

    image_fnames = det_fnames.union(gt_fnames)

    gt_color = (0, 255, 0)
    det_color = (0, 0, 255)

    for image_fname in tqdm(image_fnames, unit="image"):
        image_fpath = image_dir / image_fname
        img = cv2.imread(str(image_fpath))

        # Draw ground truth bboxes (if any) for this image.
        if gt_path and image_fname in gt_anns:
            draw_bboxes(img, gt_anns[image_fname], gt_color, categories)

        # Draw detection bboxes (if any) for this image.
        if det_path and image_fname in det_anns:
            draw_bboxes(img, det_anns[image_fname], det_color, categories)

        dst_path = output_dir / image_fname
        cv2.imwrite(str(dst_path), img)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i", "--image-dir", type=pathlib.Path, required=True,
        help="Path to dataset image directory"
    )
    parser.add_argument(
        "-g", "--gt", type=pathlib.Path,
        help="Path to ground truth annotations "
        "(.json, .feather, or pandas DataFrame .pkl)"
    )
    parser.add_argument(
        "-d", "--det", type=pathlib.Path,
        help="Path to model detections (.json, .feather, or pandas "
        "DataFrame .pkl)"
    )
    parser.add_argument(
        "-o", "--output-dir", type=pathlib.Path,
        help="Directory to which output images will be written"
    )
    parser.add_argument(
        "-c", "--conf-thresh", type=float, default=0.05,
        help="Confidence threshold for detections"
    )
    parser.add_argument(
        "-C", "--categories", type=pathlib.Path,
        help="(optional) JSON file mapping integer class ids to class names"
    )
    args = parser.parse_args()

    categories = None
    if args.categories:
        categories = {
            int(k): v
            for k, v in json.load(open(args.categories, "r")).items()
        }

    write_visualization(
        args.image_dir, args.output_dir, det_path=args.det, gt_path=args.gt,
        det_conf_thresh=args.conf_thresh, categories=categories
    )
