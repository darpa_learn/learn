import os
import torch
import time
import socket
import subprocess
import logging
import argparse
from yaml import load, dump, Loader
from learn.protocol.jplinterface import JPLInterface
from typing import List


def get_task_ids():
    """
    Get task ids for all the tasks associated with a problem after checking
    if the task id for a particular task has been provided

    Args:

    Return:
       List of task ids for a particular problem

    Example:
         >>> from scripts.run_tasks import get_task_ids
         >>> get_task_ids()
            ['6d5e1f85-5d8f-4cc9-8184-299db03713f4',
             'b01a6738-0b85-46c2-9318-16c3e2ef0f6d',
             'bbfadb2c-c7c3-4596-b548-3dd01a6d1d2c',
             'problem_test_image_classification']
    """
    secret = os.environ.get("LWLL_TA1_TEAM_SECRET")
    gov_secret = os.environ.get("LWLL_TA1_GOVTEAM_SECRET")
    url = os.environ.get("LWLL_TA1_API_ENDPOINT")
    problem_type = os.environ.get("LWLL_TA1_PROB_TYPE")
    problem_task = os.environ.get("LWLL_TA1_PROB_TASK")
    if problem_task is not None and problem_task != "all":
        return [problem_task]
    task_ids = JPLInterface.get_task_subset_by_type(secret, gov_secret, url,
                                                    problem_type)
    return task_ids


def create_temp_config(task_id: str, device_id: int,
                       default_config_path: str, num_workers: int):
    """
    Create a temporary configuration with task and device id based on the
    default configuration

    Args:
        task_id (str): Task id associated with a configuration
        device_id (int): GPU device used for the task
        default_config_path (str): Default configuration for the task
        num_workers (int): Number of workers used by dataloader

    Return:
        Path to a new configuration file based on the default configuration
        where task id and device has been modified

    Example:
         >>> from scripts.run_tasks import create_temp_config
         >>> update_cfg = create_temp_config("6d5e1f85-5d8f-4cc9-8184-299db03713f4", 1,
                                            "configs/domain_net_auto.yaml")
         >>> update_cfg
            '6d5e1f85-5d8f-4cc9-8184-299db03713f4_1_domain_net_auto.yaml'
    """
    with open(default_config_path, "r") as cfg_file:
        default_config = load(cfg_file, Loader)
        default_config["device"] = device_id
        default_config["tasks_to_run"] = [task_id]
        default_config["num_workers"] = num_workers
    default_config_fname = os.path.basename(default_config_path)
    updated_config_fname = f"{task_id}_{device_id}_{default_config_fname}"
    with open(updated_config_fname, "w") as cfg_file:
        dump(default_config, cfg_file, default_flow_style=False)
    return updated_config_fname


def create_temp_config_multiple_devices(task_id: str, device_ids: List[int],
                                        default_config_path: str, num_workers: int):
    """
    Create a temporary configuration with task and multiple devices based on the
    default configuration

    Args:
        task_id (str): Task id associated with a configuration
        device_ids (list): GPU device used for the task
        default_config_path (str): Default configuration for the task
        num_workers (int): Number of workes used by the dataloader

    Return:
        Path to a new configuration file based on the default configuration
        where task id and devices has been modified

    Example:
         >>> from scripts.run_tasks import create_temp_config_multiple_devices
         >>> update_cfg = create_temp_config("06023f86-a66b-4b2c-8b8b-951f5edd0f22", [0, 1],
                                             "configs/mt_test_feyza.yam")
         >>> update_cfg
            '06023f86-a66b-4b2c-8b8b-951f5edd0f22_1_domain_net_auto.yaml'
    """
    with open(default_config_path, "r") as cfg_file:
        default_config = load(cfg_file, Loader)
        default_config["device"] = device_ids
        default_config["tasks_to_run"] = [task_id]
        default_config["num_workers"] = num_workers
    default_config_fname = os.path.basename(default_config_path)
    updated_config_fname = f"{task_id}_{default_config_fname}"
    with open(updated_config_fname, "w") as cfg_file:
        dump(default_config, cfg_file, default_flow_style=False)
    return updated_config_fname


def get_gpu_devices():
    """
    Get gpus from either environment or from via device count

    Args:

    Returns:
        A list of gpus allocated for the tasks
    """
    gpu_env_val = os.environ.get("LWLL_TA1_GPUS")
    if gpu_env_val is not None:
        if gpu_env_val == "all":
            return list(range(torch.cuda.device_count()))
        else:
            return list(map(int, os.environ.get("LWLL_TA1_GPUS").split(" ")))
    else:
        return list(range(torch.cuda.device_count()))

def get_default_config(problem_type: str):
    """
    Given a problem type, return the relative path to the default base config.

    Args:
        problem_type: one of "image_classification, machine_translation,
            object_detection, or video_classification"
    """
    assert problem_type in ['image_classification', 'object_detection', 
        'machine_translation', 'video_classification']
    return "configs/dev_configs/quick_test/{}.yaml".format(problem_type)

def run_tasks(base_config: str, num_workers: int, interface: str):
    """
    Run tasks as child processes based on the number of gpus available

    Args:
        base_config: relative path to config file that would be to solve all the tasks
        num_workers: num workers to use in total
        interface: either 'jpl' or 'local' for which interface you want to use for learn task
    """
    available_gpus = get_gpu_devices()
    num_gpus = len(available_gpus)
    task_ids = get_task_ids()

    problem_type = os.environ.get("LWLL_TA1_PROB_TYPE")
    if base_config is None:
        base_config = get_default_config(problem_type)

    if interface == 'local':
        if problem_type in ['machine_translation', 'video_classification']:
            raise NotImplementedError(
                'No local interface for {}'.format(problem_type))
        interface_name = 'LocalInterface'
    elif interface == 'jpl':
        interface_name = 'JPLInterface'
    else:
        raise ValueError('Unknown problem type: {}'.format(problem_type))

    if problem_type == "machine_translation":
        if len(task_ids) > 1:
            raise NotImplementedError("Multiple tasks ids are not supported for machine translation")
        mt_temp_config = create_temp_config_multiple_devices(task_ids[0], available_gpus,
                                                             base_config, num_workers)
        framework_cmd = ["framework", "-a", "learn/algorithms/", "-i", interface_name,
                         "-p", f"{mt_temp_config}", "learn/protocol/learn_protocol.py",
                         "-r", f"{task_ids[0]}_{socket.gethostname()}_{time.asctime().replace(' ', '_')}.log"]
        updated_env = os.environ.copy()
        updated_env["CUDA_VISIBLE_DEVICES"] = ",".join(map(str, available_gpus))
        updated_env["CUDA_LAUNCH_BLOCKING"] = "1"
        
        mt_cmd = subprocess.Popen(framework_cmd, env=updated_env)
        ret_code = mt_cmd.wait()
        if ret_code ==  0:
            logging.info(f"Process terminated with {ret_code}")
        else:
            logging.warning(f"Process terminated with {ret_code}")
        os.remove(mt_temp_config)
    else:
        # Assign gpu id to different the tasks
        task_assignment = {task_id: available_gpus[i%num_gpus] \
                           for i, task_id in enumerate(task_ids)}

        # If the number of tasks are more than the number of gpus then the task_assignment
        # would be used as a queue to schedule tasks on all gpus and wait for the tasks to
        # complete. The task on all gpus are complete new tasks would be scheduled on all
        # the gpus
        for task_lb in range(0, len(task_ids), num_gpus):
            temp_configs = []
            framework_runs = []
            for task_id in task_ids[task_lb: task_lb+num_gpus]:
                device_id = task_assignment[task_id]
                temp_config = create_temp_config(task_id, device_id, base_config,
                                                 num_workers)
                temp_configs.append(temp_config)
                framework_cmd = ["framework", "-a", "learn/algorithms/", "-i", interface_name,
                                 "-p", f"{temp_config}", "learn/protocol/learn_protocol.py",
                                 "-r", f"{task_id}_{device_id}_{socket.gethostname()}_"
                                       f"{time.asctime().replace(' ', '_')}.log"]
                framework_runs.append(subprocess.Popen(framework_cmd))
            for idx, framework_run in enumerate(framework_runs):
                ret_code = framework_run.wait()
                if ret_code == 0:
                    logging.info(f"Process associated with {task_ids[task_lb+idx]} "
                                 f"returned terminated with {ret_code}")
                else:
                    logging.warning(f"Process associated with {task_ids[task_lb+idx]} "
                                    f"returned terminated with {ret_code}")
            for temp_config in temp_configs:
                os.remove(temp_config)


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Run script for learn")
    parser.add_argument("--base-config", "-b", type=str,
                        default=None,
                        help="Base configuration used for generating " + 
                        "temporary config for all the tasks. If not set " + 
                        "will use default for given envirnoment variable")
    parser.add_argument("--num-workers", "-n", type=int,
                        default=9, help="Number of workers used by dataloader")
    parser.add_argument("--interface", "-i", type=str,
                        default='jpl', help="Which interface to use, 'jpl' or 'local'")
    args = parser.parse_args()
    raise NotImplementedError("No longer supported - use hydra_launcher instead")
    run_tasks(args.base_config, args.num_workers, args.interface)
