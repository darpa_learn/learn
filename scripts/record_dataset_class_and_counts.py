from pathlib import Path
import pandas as pd
import numpy as np
from learn.utils.wnid_to_class_names import IMAGENET_WNID_TO_CLASS_NAMES


p = Path('/home/chris/lwll_datasets/external/')
train_files = list(p.glob('*/labels_full/labels_train.feather'))
test_files = list(p.glob('*/labels_full/labels_test.feather'))


for split in ['train', 'test']:
    files = list(p.glob(f'*/labels_full/labels_{split}.feather'))
    _class = []
    _count = []
    _dataset_names = []
    _problem_type = []

    for fn in train_files:
        dataset_labels = pd.read_feather(fn)
        dataset_name = fn.parts[5]
        if 'class' in dataset_labels.columns: # exlcude mt labels
            problem_type = 'image_classification'
            if 'bbox' in dataset_labels.columns:
                problem_type = 'object_detection'
            if 'imagenet' in dataset_name:
                print(dataset_name)
                dataset_labels = dataset_labels['class'].map(
                    lambda l: IMAGENET_WNID_TO_CLASS_NAMES[l].lower())
            else:
                dataset_labels = dataset_labels['class'].map(lambda l: str(l).lower().replace('_', ' '))
            classes, counts = np.unique(dataset_labels.tolist(), return_counts=True)
            _class += classes.tolist()
            _count += counts.tolist()
            _dataset_names += [dataset_name] * len(counts)
            _problem_type += [problem_type] * len(counts)

    labels = pd.DataFrame(
        {'class': _class,
         'count': _count,
         'dataset': _dataset_names,
         'problem_type': _problem_type}
        )
    labels.to_csv(f'{split}_labels.csv')


## Proto visualizations but didn't look good so switching to matlab
#
# import matplotlib.pyplot as plt
# import seaborn as sns
# sns.set()
#
# bar_kws = {'xticklabels': []}
# h = sns.barplot(data=train_labels, x='count', y='class', hue='dataset')
# h.get_xaxis().set_ticks([])
# # train_labels.set_index('dataset').plot(kind='bar', stacked=True)
# h.get_x
#
