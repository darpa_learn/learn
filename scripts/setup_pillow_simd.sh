#!/bin/bash

# Make script fail on first error:
set -e


# Need this on some systems to enable conda from bash:
CONDA_ENV="learn"
eval "$(conda shell.bash hook)"
conda activate "${CONDA_ENV}"

##
## Fastai Pillow-SIMD jpeg and tiff optimization support:
USE_OPTIMIZED_PIL=true
if [[ $USE_OPTIMIZED_PIL = true ]]; then
    echo "Using optimized Pillow install (attempt with jpeg-turb and tiff support..."
    conda uninstall -y --force pillow pil jpeg libtiff libjpeg-turbo
    pip   uninstall -y         pillow pil jpeg libtiff libjpeg-turbo
    conda install -yc conda-forge libjpeg-turbo
    conda install -y -c zegami libtiff-libjpeg-turbo
    # Limit to versions below 7.0, because 7.0 needs a newer torchvision version to avoid some errors related to PIL.Image.PILLOW_VERSION -> PIL.Image.__version__
    # CFLAGS="${CFLAGS} -mavx2" pip install --upgrade --no-cache-dir --force-reinstall --no-binary :all: --compile "pillow-simd<7.0"
    CFLAGS="${CFLAGS} -march=native -msse3" pip install --upgrade --no-cache-dir --force-reinstall --no-binary :all: --compile "pillow-simd<7.0"

    # Disable flags (this ends up making everything slower)
    # pip install --upgrade --no-cache-dir --force-reinstall --no-binary :all: --compile "pillow-simd<7.0"
else
    echo "NOT using optimized Pillow install!"
fi
echo "Checking to see which version of PIL installed. If there is a .post section it means we are running Pillow-SIMD:"
python -c "from PIL import Image; print(Image.PILLOW_VERSION)"
# echo "Performing fastai optimization check..."
# python -m fastai.utils.check_perf
# python -m fastai.utils.show_install