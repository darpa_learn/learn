from setuptools import setup, find_packages

setup(
    name='learn',
    version='0.0.1',
    packages=find_packages(),
    url='https://gitlab.kitware.com/darpa_learn/learn',
    license='',
    author='Christopher Funk',
    author_email='christopher.funk@kitware.com',
    description='Learn Framework',
    entry_points={
        "smqtk_plugins": [
            "detectron2 = learn.smqtk_plugins.detect_image_objects.detectron2_generalizedrcnn",
        ]
    }
)
