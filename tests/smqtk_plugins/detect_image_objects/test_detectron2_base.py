import gc
import logging
from typing import Any
from typing import Callable
from typing import Type
import unittest.mock as mock

import detectron2.data.transforms as T
from smqtk_image_io import AxisAlignedBoundingBox
import numpy as np
import numpy.testing
import pytest
import torch
from torch.utils.data import Dataset

# noinspection PyProtectedMember
from learn.smqtk_plugins.detect_image_objects.detectron2_base import (
    _trivial_batch_collator,
    _to_tensor,
    _aug_one_image,
    make_d2_dataloader,
    AugmentImageSequence,
    AugmentImageIterable,
    Detectron2Base,
)

from tests import REPO_DIR


LOG = logging.getLogger(__name__)

PARAM_np_dtype = pytest.mark.parametrize(
    "np_dtype",
    [np.uint8, np.float32, np.float64],
    ids=lambda v: f"dtype={v.__name__}"
)

TEST_CONFIG_PATH = REPO_DIR / "configs/detectron_config/coco2014_retinanet_R_50_FPN_3x.yaml"


def test_trivial_batch_collector() -> None:
    """
    Should literally do nothing and return what was input.
    """
    new_type = type("rando_new_type")
    new_object = new_type()
    val = _trivial_batch_collator(new_object)
    assert val is new_object


@PARAM_np_dtype
def test_to_tensor_nochan(np_dtype: np.dtype) -> None:
    """
    Test that a tensor is appropriately output for the given ndarray with NO
    channel dimension.
    """
    n = np.empty((15, 16), dtype=np_dtype)
    t = _to_tensor(n)
    assert t.shape == (15, 16)
    assert t.dtype == torch.float32


@PARAM_np_dtype
def test_to_tensor_1chan(np_dtype: np.dtype) -> None:
    """
    Test that a tensor is appropriately output for the given ndarray with ONE
    channel dimension.
    """
    n = np.empty((17, 18, 1), dtype=np_dtype)
    t = _to_tensor(n)
    assert t.shape == (1, 17, 18)
    assert t.dtype == torch.float32


@PARAM_np_dtype
def test_to_tensor_3chan(np_dtype: np.dtype) -> None:
    """
    Test that a tensor is appropriately output for the given ndarray with THREE
    channel dimension.
    """
    n = np.empty((19, 20, 3), dtype=np_dtype)
    t = _to_tensor(n)
    assert t.shape == (3, 19, 20)
    assert t.dtype == torch.float32


def test_aug_one_image() -> None:
    """
    Test single-image augmentation pass with mock augmentation.
    """
    m_aug = mock.MagicMock(spec=T.Augmentation)
    test_image = np.zeros((128, 224, 3), dtype=np.uint8)

    ret = _aug_one_image(test_image, m_aug)

    # The mock augmentation does nothing to the `AugInput` given to it, so
    # can treat it as a "NoOp" augmentation where the `aug_input.image` is the
    # same image as was input to the parent function.
    assert isinstance(ret, dict)
    assert len(ret) == 3
    assert 'image' in ret
    assert isinstance(ret['image'], torch.Tensor)
    assert ret['image'].shape == (3, 128, 224)
    assert 'height' in ret
    assert ret['height'] == 128
    assert 'width' in ret
    assert ret['width'] == 224


class TestAugmentImageSequence:

    def test_len(self) -> None:
        """
        Dataset length should reflect the input sequence.
        """
        a = mock.Mock(spec=T.Augmentation)

        inst = AugmentImageSequence([], a)
        assert len(inst) == 0

        empty_mat = np.empty((0,))
        inst = AugmentImageSequence([empty_mat] * 173, a)
        assert len(inst) == 173

    @pytest.mark.parametrize("seq_type", [tuple, list, np.asarray],
                             ids=lambda v: f"seq_type={v.__name__}")
    def test_getitem(self, seq_type: Callable) -> None:
        """
        Test that the expected return occurs for the given input. Also checking
        that augmentation input is appropriately called.
        """
        m_aug = mock.MagicMock(spec=T.Augmentation)

        # [N x H x W] matrix.
        test_images = seq_type(np.random.rand(4, 5, 6))

        # A sequence with no content.
        inst = AugmentImageSequence(test_images, m_aug)

        r = inst[0]
        assert m_aug.call_count == 1
        assert 'image' in r
        np.testing.assert_allclose(test_images[0], r['image'])
        assert 'height' in r
        assert r['height'] == 5
        assert 'width' in r
        assert r['width'] == 6

        r = inst[1]
        assert m_aug.call_count == 2
        assert 'image' in r
        np.testing.assert_allclose(test_images[1], r['image'])
        assert 'height' in r
        assert r['height'] == 5
        assert 'width' in r
        assert r['width'] == 6

        r = inst[2]
        assert m_aug.call_count == 3
        assert 'image' in r
        np.testing.assert_allclose(test_images[2], r['image'])
        assert 'height' in r
        assert r['height'] == 5
        assert 'width' in r
        assert r['width'] == 6

        r = inst[3]
        assert m_aug.call_count == 4
        assert 'image' in r
        np.testing.assert_allclose(test_images[3], r['image'])
        assert 'height' in r
        assert r['height'] == 5
        assert 'width' in r
        assert r['width'] == 6

        with pytest.raises(IndexError):
            _ = inst[4]

    @pytest.mark.parametrize("n_workers", [0, 1, 2, 4],
                             ids=lambda v: f"n_workers={v}")
    def test_with_dataloader(self, n_workers: int) -> None:
        """
        Test batching returns with dataloader.
        We parameterize n-workers to ensure the correct batching occurs in
        parallel worker scenarios.

        Results from batches should maintain input order. Since we are
        "ordering" the values of the input images and are using a no-op
        augmentation (mock), the single scalar valued "images" should be
        monotonically increasing for easy evaluation.
        """
        m_aug = mock.MagicMock(spec=T.Augmentation)
        # [N x H x W] matrix, ascending values to be able to "know" order.
        test_images = np.arange(18).reshape((18, 1, 1))
        dataset = AugmentImageSequence(test_images, m_aug)

        # An evenly divisible batch size.
        batch_size = 3
        batch_list = list(make_d2_dataloader(dataset, batch_size=batch_size, num_workers=n_workers))
        assert len(batch_list) == 6
        assert {len(b) for b in batch_list} == {batch_size}
        # Flatten batched "images" and check for monotonic value increase to
        # assert correct yield ordering
        for i, e in enumerate([e for b in batch_list for e in b]):
            assert e['image'][0, 0] == i

        # A NOT evenly divisible batch size, to ensure that the last batch
        # correctly contains the tail elements.
        batch_size = 5
        tail_size = 3
        batch_list = list(make_d2_dataloader(dataset, batch_size=batch_size, num_workers=n_workers))
        assert len(batch_list) == 4
        assert {len(b) for b in batch_list} == {batch_size, tail_size}
        # Flatten batched "images" and check for monotonic value increase to
        # assert correct yield ordering
        for i, e in enumerate([e for b in batch_list for e in b]):
            assert e['image'][0, 0] == i


class TestAugmentImageIterable:

    @pytest.mark.parametrize("n_workers", [0, 1, 2, 4],
                             ids=lambda v: f"n_workers={v}")
    def test_with_dataloader(self, n_workers: int) -> None:
        """
        Test batching returns with dataloader.
        We parameterize n-workers to ensure the correct batching occurs in
        parallel worker scenarios, even though n_workers>1 is discouraged when
        using iterable datasets.

        Results from batches should maintain input order. Since we are
        "ordering" the values of the input images and are using a no-op
        augmentation (mock), the single scalar valued "images" should be
        monotonically increasing for easy evaluation.
        """
        m_aug = mock.MagicMock(spec=T.Augmentation)
        # [N x H x W] matrix, ascending values to be able to "know" order.
        test_images = np.arange(18).reshape((18, 1, 1))

        # batch size of 3, evenly divisible
        batch_size = 3
        dataset = AugmentImageIterable(iter(test_images), m_aug, batch_size)
        batch_list = list(make_d2_dataloader(dataset, batch_size=batch_size, num_workers=n_workers))
        assert len(batch_list) == 6
        assert {len(b) for b in batch_list} == {batch_size}
        # Flatten batched "images" and check for monotonic value increase to
        # assert correct yield ordering
        for i, e in enumerate([e for b in batch_list for e in b]):
            assert e['image'][0, 0] == i

        # batch size of 5, NOT evenly divisible
        batch_size = 5
        tail_size = 3
        dataset = AugmentImageIterable(iter(test_images), m_aug, batch_size)
        batch_list = list(make_d2_dataloader(dataset, batch_size=batch_size, num_workers=n_workers))
        assert len(batch_list) == 4
        assert {len(b) for b in batch_list} == {batch_size, tail_size}
        # Flatten batched "images" and check for monotonic value increase to
        # assert correct yield ordering
        for i, e in enumerate([e for b in batch_list for e in b]):
            assert e['image'][0, 0] == i


class TestDetectron2Base:

    class StubPlugin(Detectron2Base):
        """ Stub implementation of abstract base class used to test base class
        provided functionality. """
        def _get_augmentation(self) -> T.Augmentation: ...

    @classmethod
    def teardown_class(cls) -> None:
        del cls.StubPlugin
        # Clean-up locally defined pluggable implementation.
        gc.collect()

    @mock.patch.object(Detectron2Base, "_lazy_load_model")
    def test_init_lazy_load(self, _: Any) -> None:
        """
        Test that with lazy load on, construction does NOT attempt to load the
        model.
        """
        inst = self.StubPlugin(TEST_CONFIG_PATH.as_posix(), model_lazy_load=True)

        inst._lazy_load_model.assert_not_called()  # type: ignore

    @mock.patch.object(Detectron2Base, "_lazy_load_model")
    def test_init_eager_load(self, _: mock.Mock) -> None:
        """
        Test that with lazy load off, the model attempts to initialize
        immediately.
        """
        inst = self.StubPlugin(TEST_CONFIG_PATH.as_posix(), model_lazy_load=False)

        inst._lazy_load_model.assert_called_once()  # type: ignore

    @pytest.mark.parametrize("initially_lazy", [False, True], ids=lambda v: f"initially_lazy={v}")
    def test_lazy_load_model_idempotent(self, initially_lazy: bool) -> None:
        """
        Test that the lazy loading function returns the same object on
        successive calls.
        """
        inst = self.StubPlugin(TEST_CONFIG_PATH.as_posix(), model_lazy_load=initially_lazy)
        inst_model1 = inst._lazy_load_model()
        inst_model2 = inst._lazy_load_model()
        assert inst_model1 is not None
        assert inst_model1 is inst_model2

    @pytest.mark.parametrize("input_container_type,dataset_type",
                             [(list, AugmentImageSequence),
                              (iter, AugmentImageIterable)])
    @mock.patch("learn.smqtk_plugins.detect_image_objects.detectron2_base.make_d2_dataloader",
                side_effect=make_d2_dataloader)
    def test_detect_objects(self, m_make_loader: mock.Mock,
                            input_container_type: Callable,
                            dataset_type: Type[Dataset]) -> None:
        """
        Test calling detect_objects method with a sequence-like input type,
        checking that an ``AugmentImageSequence`` is appropriately created.
        Mocking model creation and forward as their side effects are not the
        focus.
        """
        # Input images for this test, 24 small 3-channel images
        test_image_sequence = np.random.randint(0, 255, (24, 16, 16, 3), dtype=np.uint8)
        # Very simple detection output per-image for this test. Following
        # expected output API.
        test_output_one_image = (
            (AxisAlignedBoundingBox((0, 0), (1, 1)), {'null': 1.}),
        )

        # Test values for batch / workers
        test_batch_size = 5
        test_num_workers = 1

        inst = self.StubPlugin(
            TEST_CONFIG_PATH.as_posix(),
            batch_size=test_batch_size,
            num_workers=test_num_workers,
            model_lazy_load=True,  # don't load anything on construction.
        )
        # We don't want to actually create/use a model, we're just testing the
        # surrounding process.
        inst._lazy_load_model = mock.Mock(  # type: ignore
            return_value=mock.Mock(spec=torch.nn.Module)
        )
        # At least output a size-bounded sequence to simulate actually doing
        # something based on inputs. Also, because this output is looped over.
        inst._forward = mock.Mock(  # type: ignore
            side_effect=lambda m, bi: [{} for _ in bi]
        )
        # Mock translation of network output into API format by just always
        # returning the same simple output.
        inst._iterate_output = mock.Mock(  # type: ignore
            return_value=test_output_one_image
        )

        # mock out augmentation, expect to be invoked, but because it's a mock
        # will of course do nothing to the input.
        m_aug = mock.MagicMock(spec=T.Augmentation,
                               return_value=mock.MagicMock(name="MockTransform"))
        inst._get_augmentation = mock.MagicMock(return_value=m_aug)  # type: ignore

        test_input = input_container_type(test_image_sequence)
        actual_img_det_list = [
            list(img_dets)
            for img_dets in inst.detect_objects(test_input)
        ]

        # Loader should be called once with the image sequence dataset, and
        # with known batch-size/num-workers param values.
        m_make_loader.assert_called_once()
        assert isinstance(m_make_loader.call_args.args[0], dataset_type)
        assert m_make_loader.call_args.args[1:] == (test_batch_size, test_num_workers)

        # Forward call should be called once per batch. Given 24 images and
        # batch size of 5, we expect 5 batches.
        assert inst._forward.call_count == 5

        # Output interpret function should be called for each output, so once
        # per image in this case.
        assert inst._iterate_output.call_count == 24

        expected_output = [list(test_output_one_image) for _ in range(24)]
        assert actual_img_det_list == expected_output
