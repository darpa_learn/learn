import numpy as np

from learn.smqtk_plugins.detect_image_objects.detectron2_generalizedrcnn import Detectron2GeneralizedRCNN

from tests import REPO_DIR


class TestDetectron2GeneralizedRCNN:

    def test_smoke_random(self) -> None:
        """
        Smoke-test running a model on a random RGB image.
        """
        cfg_fpath = REPO_DIR / "configs/detectron_config/coco2014_retinanet_R_50_FPN_3x.yaml"
        inst = Detectron2GeneralizedRCNN(
            cfg_fpath.as_posix(),
            batch_size=1, model_lazy_load=False, num_workers=0
        )

        random_image = (np.random.rand(244, 244, 3) * 255).astype(np.uint8)
        results = list(list(inst.detect_objects([random_image]))[0])
