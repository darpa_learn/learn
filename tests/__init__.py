from pathlib import Path

REPO_DIR = Path(__file__).parent.parent
DATA_DIR = Path(__file__).parent / "data"
