# Description of Approach

## Algorithms

Our method has 3 major steps:
- Domain and Network Selection
- Adaptive Learning
- Domain Adaption/Few-shot Learning 

### Domain Adaptation and Network Selection
We use proxy-a distance to select the source domain [1].   This trains a linear classifier to predict the data as 
coming from the source or target domains.  We use a network pretrained on imagenet to compute the features 
for the classification. We run the classification 5 times and average the accuracies. 
We do this for each potential source domain.  We then select the source
domain which ahs the worst classification accuracy.  

To select the pretrained network to use as the initial weights for image classification, we compute the label 
cross-entropy loss using the labeled data as prototypes for the classes.   For object detection, we use the 
feature entropy of the backbone network.  The minimum cross entropy loss or entropy is chosen as the network to use.  

The code for this algorithm is contained in `learn/algorithm/templateDomainNetworkSelection/domainNetworkSelection.py`    

### Adaptive Learning
We have two adaptive learning algorithm which are used for both image classification and object detection. 

- VAAL 
This code is based on the VAAL paper [2].  At each budget level, a VAE is trained on the data.  At the same time, 
a discriminator is adversarially trained on the VAE's latent space to predict if a piece of data is coming from the 
labeled or unlabeled pool. The probability associated with the discriminator's predictions is used as a score to send 
the samples predicted as "unlabeled" with the lowest confidence for labeling. VAAL's sampling strategy does not 
require training on the mainstream task or receiving any label from the oracle. We have altered the loss function the 
VAE and tuned the algorithm to work on more diverse data.      
The code for this algorithm is contained in `learn/algorithm/VAAL/query_algo.py`    

- Clue
This is a function which has been submitted for publication as part of entire process for ADA-CLUE.  
This approach does a weighted k-mean clustering based on the uncertainty (measured by entropy) and selects the 
closest points to the centroid for labeling. The k is set to the budget level for each stage. The code for this 
algorithm is contained in  `learn/algorithm/CLUE/query_algo.py`

- NLP is covered under NLP

### Image Classification
The approach is based on MME (Min-Max Entropy) [3].  The method adapts to a target domain by minimizing 
the entropy of unlabeled target domain. To calculate the entropy, we aim to get domain-invariant prototype by 
maximizing entropy with respect to the prototype for target samples.  The original approach of MME was designed for 
the situation where source and target contain exactly the same classes. We modified MME to consider class difference 
between source and target. While we use all source samples in training, we applied min-max entropy only for classes 
present in the target.
The code for this algorithm is contained in `learn/algorithm/MME/image_classification_novelcat.py` 

### Object Detection
The approach is based on FSNet [4] to detect objects from a few examples. We focus on the training schedule and the 
instance-level feature normalization of the object detectors in model design and training based on fine-tuning. We 
find it crucial to freeze the backbone of the network and only train the last layer of the bbox detector and the 
classification for the few-shot object detection task. We design a two-stage training scheme for fine-tuning: first 
train the entire object detector, such as Faster R-CNN, on the data abundant base classes, and then only fine-tune 
the last layers of the detector on a small balanced training set consisting of both base and novel classes while 
freezing the other parameters of the model. During the fine-tuning stage, we introduce instance-level feature 
normalization to the box classifier. Such approach outperforms the existing meta-learning methods.
The code for this algorithm is contained in `learn/algorithm/FsDet/object_detection.py`

### NLP  
Active Learning - For the Neural Machine translation active learning, we're selecting the sentences we want to get 
labels for using a Coverage sampling approach, following [6]. This method attempts to tackle the phenomena of over 
and under-translation of a Neural Machine translation system, which may not translate all words from the given 
source sentences. For this purpose, we are computing translation coverage asa measure of the uncertainty, by 
calculating a coverage-based score, which takes into account the attention probabilities assigned to a sentence's 
words, when decoding a source sentence with the XLM encoder-decoder transformer architecture. Since the decoder 
architecture is a multi-layer multi-head transformer, consisting of 6 layers and 8 heads, we take into account 
only the last layer weights and average the calculated scores over the transformer heads, to obtain a final 
score per sentence. In addition to this, we keep a maximum sentence length to 250, and observe that sentences 
with a length of 100-250 improve our system's performance, hence, we boost these sentences' scores, to make them 
more likely to be selected for labeling.
The code for this algorithm is contained in the folder `learn/algorithm/BU_NLP/`


Our zero-shot and low-shot machine translation solution is based on a cross-lingual language model called XLM [7]. 
XLM is a transformer encoder-decoder based architecture. We first train XLM on a large corpora of monolingual 
Arabic [8,9] and English news data with masked language model objective. We then use back-translation [10] objective 
to leverage the pre-trained bilingual language model for unsupervised neural machine translation task. Finally, we 
continuously fine-tune the model with the streaming parallel data.


## Code
The main of the code is located in: `learn/protocol/learn_protocol.py`.  Each step here is an algorithm in the 
algorithms folder.  In addition, some minor steps such as algorithm selection (which could choose which algorithms
depending on the data though we opted for a simpler solution of just having one algorithms) and all the pretraining
code for the datasets.   


# References
[1] Ganin, Yaroslav, Evgeniya Ustinova, Hana Ajakan, Pascal Germain, Hugo Larochelle, François Laviolette, 
Mario Marchand, and Victor Lempitsky. "Domain-adversarial training of neural networks." 
The Journal of Machine Learning Research 17, no. 1 (2016): 2096-2030.

[2] Sinha, Samarth, Sayna Ebrahimi, and Trevor Darrell. "Variational adversarial active learning." 
In Proceedings of the IEEE International Conference on Computer Vision, pp. 5972-5981. 2019.

[3] Saito, Kuniaki, Donghyun Kim, Stan Sclaroff, Trevor Darrell, and Kate Saenko. "Semi-supervised domain adaptation 
via minimax entropy." In Proceedings of the IEEE International Conference on Computer Vision, pp. 8050-8058. 2019.

[4] Wang, Xin, Thomas E. Huang, Trevor Darrell, Joseph E. Gonzalez, and Fisher Yu. 
"Frustratingly Simple Few-Shot Object Detection." arXiv preprint arXiv:2003.06957 (2020).

[6] https://www.aclweb.org/anthology/K18-1015.pdf

[7] https://arxiv.org/pdf/1901.07291.pdf

[8]https://data.mendeley.com/datasets/57zpx667y9/2

[9] https://data.mendeley.com/datasets/hhrb7phdyx/2

[10] https://arxiv.org/pdf/1711.00043.pdf

[11] Chen, Yinbo, Xiaolong Wang, Zhuang Liu, Huijuan Xu, and Trevor Darrell. "A new meta-baseline for few-shot 
learning." arXiv preprint arXiv:2003.04390 (2020).
