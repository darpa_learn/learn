from dataclasses import dataclass, field
from typing import Any, List
from enum import Enum

import hydra
from hydra.core.config_store import ConfigStore
from omegaconf import DictConfig, OmegaConf, MISSING
from learn.launcher.launcher import LaunchProtocol
import logging

log = logging.getLogger(__name__)

# Figure this out after having a working example.
#
# @dataclass
# class Problem:
#     problem_type: str = 'ImageClassification'
#     algoSelection_algo: str = 'templateAlgoSelection/algoSelection.py'
#     apikey: str = 'adab5090-39a9-47f3-9fc2-3d65e0fee9a2'
#     dataset_dir: str = 'lwll_datasets'
#     tasks_to_run: List[str] = field(default_factory=lambda: ['bbfadb2c-c7c3-4596-b548-3dd01a6d1d2c'])
#     url: str = 'https://api-staging.lollllz.com/'
#     add_dataset_to_whitelist: List[str] = field(default_factory=lambda: ['domain_net-painting'])
#
# @dataclass
# class DomainNetworkSelector:
#     name: str = 'templateDomainNetworkSelection'
#
# @dataclass
# class Config:
#     defaults: List[Any] = field(default_factory=lambda: [{"problem": "quick_test"}])
#     problem: Problem = MISSING
#     domain_network_selector: DomainNetworkSelector = MISSING
#     self_supervision: str = MISSING
#     active_learner: str = 'templateQueryAlgo'
#     classifier: str = MISSING
#
#
# cs = ConfigStore.instance()
# cs.store(name="config", node=Config)
# cs.store(group="problem", name="quick_test", node=Problem)
# cs.store(group="domain_network_selector", name="domain_network_selector", node=DomainNetworkSelector)


@hydra.main(config_path="configs/hydra_config", config_name="config")
def hydra_launcher(cfg: DictConfig) -> None:
    """ Load the protocol and then run it.  Check README.md for how to run.

    """
    import os
    print("hydralaucnher ", os.getcwd())
    log.info(f'\n{OmegaConf.to_yaml(cfg, resolve=True)}')
    launch_protocol = LaunchProtocol()
    launch_protocol.run_protocol(cfg)


if __name__ == "__main__":
    hydra_launcher()
