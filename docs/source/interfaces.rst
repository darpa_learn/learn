==========
Interfaces
==========
Interfaces are protocol backend functions.  This is only useful for the protocol designers (not for researchers).
Harness is a general interface fo

.. automodule:: framework.harness
   :special-members:
   :members:
   :private-members:


.. automodule:: framework.jplinterface
   :special-members:
   :members:
   :private-members:

.. automodule:: framework.localinterface
   :special-members:
   :members:
   :private-members:
