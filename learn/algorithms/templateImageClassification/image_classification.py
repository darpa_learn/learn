import sys
import torch
from learn.algorithms.templateImageClassification.imageClassificationAdapter import ImageClassifierAdapter
import torch.utils.data as data
import logging
log = logging.getLogger(__name__)


class ImageClassifierAlgorithm(ImageClassifierAdapter):

    def __init__(self, toolset):
        # def __init__(self, problem, base_dataset, adapt_dataset, arguments):
        ImageClassifierAdapter.__init__(self, toolset)
        self.config = toolset['protocol_config']['image_classifier']['params']

    def initialize(self):
        """  Initialize your algorithm and save any parameters or objects you want
        to be persistent between checkpoints.

        """

        # pre-trained network on source task (the closest task)
        #  You are not required to use it but strongly suggested to use it
        #  These are in the style of torchvision models
        self.model = self.toolset["source_network"]

    def domain_adapt_training(self):
        """ This is where you will fine-tune/train your few-shot or domain adaption
        algorithm.  You may use the dataloaders below and feel free to override the
        transformers in the datasets to use whatever pretraining you desire.
        Please do not modify the datasets though.
        """

        # ###################  Creating the Labeled DataLoader ###############
        # Create Dataloaders for labeled data by getting the labeled indices and
        # creating a random sub-sampler.  Then create the dataloader using this
        # sampler
        #
        #  Note: To change the transformer in the JPLDataset dataset, edit
        #      current_dataset.transform.  You can also edit the labels during
        #      loading by editing the  current_dataset.target_transform
        #
        #  Note: Another approach could be to inherit the JPLDataset class and
        #      override the __get_item__ function
        #
        #  Check out dataset.py for more information
        #     Notes: There could be NO unlabeled data and the algorithm has to be
        #         able to handle that case!
        #
        #         The min batch size here is so that the batch size isn't
        #         larger than the labeled/unlabeled dataset which causes the
        #         dataloader to hang

        # labeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
        #     self.toolset["target_dataset"].get_labeled_indices()
        # )
        #
        # labeled_dataloader = torch.utils.data.DataLoader(
        #     self.toolset["target_dataset"],
        #     sampler=labeled_sampler,
        #     batch_size=min(self.toolset["target_dataset"].labeled_size,
        #                    int(self.config["batch_size"])
        #                    ),
        #     num_workers=int(self.config['num_workers']),
        #     collate_fn=self.toolset["target_dataset"].collate_batch,
        #     drop_last=True,
        # )
        #
        # # ###################  Creating the Unlabeled DataLoader ###############
        # #  Same as labeled dataaset but for unlabeled indices
        # #   Except skip if no unlabeled
        # if self.toolset["target_dataset"].unlabeled_size > 0:
        #     unlabeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
        #         self.toolset["target_dataset"].get_unlabeled_indices()
        #     )
        #     unlabeled_dataloader = torch.utils.data.DataLoader(
        #         self.toolset["target_dataset"],
        #         sampler=unlabeled_sampler,
        #         batch_size=min(self.toolset["target_dataset"].unlabeled_size,
        #                        int(self.config["batch_size"])
        #                        ),
        #         num_workers=int(self.config['num_workers']),
        #         drop_last=False,
        #     )

        # ##################### fine-tune/train your model ######################
        #  Here is where you will actual finetune the model
        #  You can use the pretrained model with self.model
        #  Make sure to set any persistent tensors to cpu with `tensor.cpu` command

    def inference(self):
        """
        Inference is during the evaluation stage.  For this example, the
        task_network is trained in the train and adapt stage's code and
        is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to
        JPL

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        log.info('Doing Random Classification')
        # self.task_model.eval()
        # eval_dataloader = data.DataLoader(
        #     toolset["Dataset"],
        #     batch_size=int(self.arguments["batch_size"]),
        #     drop_last=False
        # )
        # preds = []
        # indices = []
        #
        # for imgs, inds in eval_dataloader:
        #     if self.cuda:
        #         imgs = imgs.cuda()
        #
        #     with torch.no_grad():
        #         preds_ = self.task_model(imgs)
        #
        #     preds += torch.argmax(preds_, dim=1).cpu().numpy().tolist()
        #     indices += inds.numpy().tolist()

        # preds, indices = self.toolset["eval_dataset"].dummy_data(
        #     'image_classification')

        preds = self.toolset["eval_dataset"].dummy_data('image_classification')
        return preds
