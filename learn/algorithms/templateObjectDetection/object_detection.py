import sys
from learn.algorithms.templateObjectDetection.objectDetectionAdapter import ObjectDetectionAdapter

import torch
import torch.utils.data as data
import numpy as np

class ObjectDetectionAlgorithm(ObjectDetectionAdapter):
    """
    """
    def __init__(self, toolset):
        """

        """
        ObjectDetectionAdapter.__init__(self, toolset)
        self.config = toolset['protocol_config']['object_detector']['params']

    def initialize(self):

        # pre-trained network on source task (the closest task)
        #  You are not required to use it but strongly suggested to use it
        #  These are in the style of torchvision models
        self.model = self.toolset["source_network"]


    def domain_adapt_training(self):
        """ Method for the training in the train stage of the problem.
        Also known as the base stage.

        Here you will train your algorithm and query for new labels until the
        labeling budget is used up. Once the labeling budget is used up and
        your training is finished, return and it will run the inference function.

        You may modify any part of this function however there are some things
        you will need to do here.

        1.  You will need to get the labels from the JPLdataset.  The examples
            are one way to create the pytorch dataloaders (``labeled_dataloader``
            and ``unlabeled_dataloader`` in the code).
        2.  You will need to query for labels.  You can do this by specifying
            the indices of the labels in the :class:`dataset.JPLDataset`.

        In the example in the code, `self.current_dataset` is either train and adapt
        dataset depending on the stage.  For this example, the VAAL approach doesn't
        have a way to adapt from the trianing stage so we can use
        self.current_dataset here.  VAAL is only an active training approach.

        """
        # ###################  Creating the Labeled DataLoader ###############
        # Create Dataloaders for labeled data by getting the labeled indices and
        # creating a random sub-sampler.  Then create the dataloader using this
        # sampler
        #
        #  Note: To change the transformer in the JPLDataset dataset, edit
        #      current_dataset.transform.  You can also edit the labels during
        #      loading by editing the  current_dataset.target_transform
        #
        #  Note: Another approach could be to inherit the JPLDataset class and
        #      override the __get_item__ function
        #
        #  Check out dataset.py for more information
        #     Notes: There could be NO unlabeled data and the algorithm has to be
        #         able to handle that case!
        #
        #         The min batch size here is so that the batch size isn't
        #         larger than the labeled/unlabeled dataset which causes the
        #         dataloader to hang

        labeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
            self.toolset["target_dataset"].get_labeled_indices()
        )

        labeled_dataloader = torch.utils.data.DataLoader(
            self.toolset["target_dataset"],
            sampler=labeled_sampler,
            batch_size=min(self.toolset["target_dataset"].labeled_size,
                           int(self.config["batch_size"])
                           ),
            num_workers=int(self.config['num_workers']),
            collate_fn=self.toolset["target_dataset"].collate_batch,
            drop_last=True,
        )

        # ###################  Creating the Unlabeled DataLoader ###############
        #  Same as labeled dataaset but for unlabeled indices

        if self.toolset["target_dataset"].unlabeled_size > 0:
            unlabeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
                self.toolset["target_dataset"].get_unlabeled_indices()
            )
            unlabeled_dataloader = torch.utils.data.DataLoader(
                self.toolset["target_dataset"],
                sampler=unlabeled_sampler,
                batch_size=min(self.toolset["target_dataset"].unlabeled_size,
                               int(self.config["batch_size"])
                               ),
                num_workers=int(self.config['num_workers']),
                drop_last=False,
            )
        #
        # ###################  End of the Creating DataLoaders ###############

    def inference(self):
        """
        Inference is during the evaluation stage.  For this example, the
        task_network is trained in the train and adapt stage's code and
        is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to
        JPL

        Args:
            eval_dataset (JPLEvalDataset): Dataset for the labels used
                for evaluation

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        # self.task_model.eval()
        # eval_dataloader = data.DataLoader(
        #     eval_dataset,
        #     batch_size=int(self.arguments["batch_size"]),
        #     drop_last=False
        # )
        # preds = []
        # indices = []
        #
        # for imgs, inds in eval_dataloader:
        #     if self.cuda:
        #         imgs = imgs.cuda()
        #
        #     with torch.no_grad():
        #         preds_ = self.task_model(imgs)
        #
        #     preds += torch.argmax(preds_, dim=1).cpu().numpy().tolist()
        #     indices += inds.numpy().tolist()

        preds, indices = self.toolset["eval_dataset"].dummy_data('object_detection')
        return preds, indices
