""" Template for using dectectron2 for object detection in the learn task.  When
adding a new algorithm to the framework, make a copy of this folder, rename it,
and then you may add your code in the new folder.  You may also add new files to this
newly created folder (such as dropping in all the code from your repository).  Also,
update the imports below.

Look at the ObjectDetectionAlgorithm for more details on what you need to do to this
file.

"""

import os
import logging
logger = logging.getLogger(__name__)

import time
import datetime

from learn.algorithms.FsDet.objectDetectionAdapter import ObjectDetectionAdapter
from learn.algorithms.FsDet.distributed import trainer_func

import torch
import numpy as np
from contextlib import contextmanager
import torch
import gc
import ubelt as ub
import copy
from learn.utils.metrics import mean_average_precision, mAP
import pandas as pd
import yaml
import json
from hydra.utils import get_original_cwd


import torch.multiprocessing as mp
import torch.distributed as dist
from torch.nn.parallel import DistributedDataParallel as DDP

from detectron2.config import CfgNode
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.engine import DefaultTrainer, launch
from detectron2.config import get_cfg
from detectron2 import model_zoo
from detectron2.utils.comm import get_world_size
from detectron2.utils.logger import log_every_n_seconds
from detectron2.checkpoint import DetectionCheckpointer, Checkpointer
from detectron2.model_zoo import get_checkpoint_url
from tempfile import TemporaryDirectory
from learn.utils.wandb_utils import WANDB_INFO

# import torch.multiprocessing as mp
# if mp.get_start_method(allow_none=True) is None:
#     mp.set_start_method('spawn')

# import cv2
# cv2.setNumThreads(0)


# torch.set_num_threads(1)
# # torch.multiprocessing.set_sharing_strategy('file_system')
# # os.environ["OMP_NUM_THREADS"]="1"



def get_config(orig_cfg, train_dataset_name, test_dataset_name, num_classes, model_directory, config):
    """ Configuration for detectron.  This takes the configuration parameters
    from the config.py and sets them to the appropriate values in the detectron
    config.

    More info: https://detectron2.readthedocs.io/modules/config.html

    Args:
        cfg (CfgNode): DetectronConfig obtained from source network
        train_dataset_name (str): name of the train dataset (used for identifying
            it later in the code
        test_dataset_name (str): name of the test dataset (used for identifying
            it later in the code
        num_classes (int): number of classes for the problem you are solving
        model_directory (TemporaryDirectory): TemporaryDirectory for saving/loading models
        config (scriptconfig.Config): list of configuration values for the
            detectron system.  Defined in config.py

    Returns:
         detectron2 CfgNode instance

    """
    cfg = orig_cfg.clone() # This is done to use a copy of config that is not frozen
    cfg.defrost()
    cfg.DATASETS.TRAIN = (train_dataset_name,)
    cfg.DATALOADER.NUM_WORKERS = config['num_workers']
    cfg.SOLVER.IMS_PER_BATCH = config['batch_size']
    cfg.SOLVER.BASE_LR = config['learning_rate']  # pick a good LR
    cfg.SOLVER.STEPS = tuple(config["steps"])
    cfg.SOLVER.MAX_ITER = config['fsnet_maximum_iter']
    cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = config[
        'batch_size_per_image']  # faster, and good enough for this toy dataset (default: 512)
    cfg.MODEL.ROI_HEADS.NUM_CLASSES = num_classes
    cfg.MODEL.RETINANET.NUM_CLASSES = num_classes

    # specify the trained model weights, here use the default weights from
    # detectron2 model zoo
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = config[
        'score_thresh_test']  # set the testing threshold for this model
    cfg.DATASETS.TEST = (test_dataset_name,)
    cfg.OUTPUT_DIR = model_directory
    return cfg


@contextmanager
def inference_context(model):
    """ A context where the model is temporarily changed to eval mode,
    and restored to previous mode afterwards.

    From: https://detectron2.readthedocs.io/_modules/detectron2/evaluation/evaluator.html#DatasetEvaluator

    Args:
        model: a detectron2 torch Module

    """
    training_mode = model.training
    model.eval()
    yield
    model.train(training_mode)
    model = model.cpu()


def inference_on_dataset(model, data_loader):
    """ Run model on the data_loader and returns results.

    Modified from: https://detectron2.readthedocs.io/_modules/detectron2/evaluation/evaluator.html#DatasetEvaluator

    Args:
        model (nn.Module): a module which accepts an object from
            `data_loader` and returns some outputs. It will be temporarily set to `eval` mode.

            If you wish to evaluate a model in `training` mode instead, you can
            wrap the given model and override its behavior of `.eval()` and `.train()`.
        data_loader: an iterable object with a length.
            The elements it generates will be the inputs to the model.

    Returns:
        The outputs from the network on the data in data_loader
    """
    num_devices = get_world_size()
    logger.info("Start inference on {} images".format(len(data_loader)))

    total = len(data_loader)  # inference data loader must have a fixed length

    num_warmup = min(5, total - 1)
    start_time = time.perf_counter()
    total_compute_time = 0
    output = []
    with inference_context(model), torch.no_grad():
        for idx, inputs in enumerate(data_loader):
            if idx == num_warmup:
                start_time = time.perf_counter()
                total_compute_time = 0

            start_compute_time = time.perf_counter()
            outputs = model(inputs)
            if torch.cuda.is_available():
                torch.cuda.synchronize()
            total_compute_time += time.perf_counter() - start_compute_time

            iters_after_start = idx + 1 - num_warmup * int(idx >= num_warmup)
            seconds_per_img = total_compute_time / iters_after_start
            if idx >= num_warmup * 2 or seconds_per_img > 5:
                total_seconds_per_img = (
                                                time.perf_counter() - start_time) / iters_after_start
                eta = datetime.timedelta(
                    seconds=int(total_seconds_per_img * (total - idx - 1)))
                log_every_n_seconds(
                    logging.INFO,
                    "Inference done {}/{}. {:.4f} s / img. ETA={}".format(
                        idx + 1, total, seconds_per_img, str(eta)
                    ),
                    n=5,
                )
            for i, _input in enumerate(inputs):
                outputs[i]['image_id'] = _input['image_id']
                outputs[i]['instances'] = outputs[i]['instances'].to('cpu')
                output.append(outputs[i])

    # TODO: Remove unnecessary logging info
    image_id_str = '\n'.join(list(map(str, map(lambda x: x['image_id'], output[:100]))))
    logger.info(f"Obtained output on the following images:\n {image_id_str}")
    # Measure the time only for this worker (before the synchronization barrier)
    total_time = time.perf_counter() - start_time
    total_time_str = str(datetime.timedelta(seconds=total_time))
    # NOTE this format is parsed by grep
    logger.info(
        "Total inference time: {} ({:.6f} s / img per device, on {} devices)".format(
            total_time_str, total_time / (total - num_warmup), num_devices
        )
    )
    total_compute_time_str = str(datetime.timedelta(seconds=int(total_compute_time)))
    logger.info(
        "Total inference pure compute time: {} ({:.6f} s / img per device, on {} devices)".format(
            total_compute_time_str, total_compute_time / (total - num_warmup),
            num_devices
        )
    )
    return output


class ObjectDetectionAlgorithm(ObjectDetectionAdapter):
    """  Object detection algorithm class.  you need to fill out the initialization
    domain_adapt_training, and inference functions.  Each function has a description
    on what you need to do in that function.

    Please remember to free GPU memory at the end of each function

    """

    def __init__(self, toolset):
        """
            First initialization of the function.  This sets the toolset and
            loads the config.  The algorithm initialization is separate so that
            the protocol can change the defaults before your algorithm loads.
            Most likely only Kitware will be editing this.

            Args:
                toolset (dict): dictionary of functions, datasets, and configs
        """
        ObjectDetectionAdapter.__init__(self, toolset)
        self.config = toolset['protocol_config']['object_detector']['params']
        self.hyperparam_config = toolset['protocol_config']['object_detector']['hyperparams']

        if WANDB_INFO.run_wandb:
            self.wandb_name = toolset["protocol_config"]["wandb_params"]["name"]

        device = self.config['device']
        if ub.iterable(device):
            self.device = device
        else:
            if device == -1:
                self.device = list(range(torch.cuda.device_count()))
            else:
                self.device = [device]

    def initialize(self):
        """  Here is where you initialize your algorithm.  It is seperate from on
        above so that the protocol calling this can override the default config
        when necessary.  Here you have `self.toolset[]` which contains the classes
        and values you will need to run the algorithm.  Some useful tools are:

            target_dataset: pytorch dataset that contains the semi-supervised
                training data you will need to train/adapt.  The dataset is defined
                in framework.dataset
            eval_dataset: Unlabeled dataset for final evaluation.  You may use it
                for transductive learning if you want to
            source_dataset: A Dataset from the most similar task we can find in the
                external datasets.
            source_network: Pre-trained network from source dataset.  You can use
                this as a dataset for the backbone.


        Returns:

        """

        # pre-trained network on source task (the closest task)
        #  You are not required to use it but strongly suggested to use it
        #  These are in the style of torchvision models
        # TODO figure out how to work this into detectron

        train_dataset = self.toolset['target_dataset']
        test_dataset = self.toolset['eval_dataset']

        # Register the dataset in the backend
        self.target_dataset_name = f'{train_dataset.name}_train'
        self.eval_dataset_name = f'{test_dataset.name}_test'
        if self.target_dataset_name in MetadataCatalog.list():
            self.target_dataset_name = f'{train_dataset.name}_train_adapt'
            self.eval_dataset_name = f'{test_dataset.name}_test_adapt'
        print("The name being used is ", self.target_dataset_name)
        print("here is the meta before adding the dataset", MetadataCatalog.list())
        DatasetCatalog.register(self.target_dataset_name, lambda d=train_dataset: self.transform_dataset(d))
        train_cats = [str(cat) for cat in train_dataset.categories.tolist()]
        MetadataCatalog.get(self.target_dataset_name).thing_classes = train_cats
        print("here is the meta after", MetadataCatalog.list())

        DatasetCatalog.register(self.eval_dataset_name, lambda d=test_dataset: self.transform_dataset(d))
        test_cats = [str(cat) for cat in test_dataset.categories.tolist()]
        MetadataCatalog.get(self.eval_dataset_name).thing_classes = test_cats

        num_cats = len(test_dataset.categories)

        # Get the config
        self.cfg = get_config(self.toolset["source_network"].model_cfg,
                              self.target_dataset_name,
                              self.eval_dataset_name,
                              num_cats,
                              self.toolset["temp_model_directory"],
                              self.config)


    def domain_adapt_training(self):
        """ Train your algorithm to adapt to the target_dataset.  This will be called
        multiple times, each with more labels in the target dataset labeled.
        Make sure to set your model to cpu() before the end of the function.  Also,
        clear the cuda cache (as shown below).

        You may save your network weight between the calls (but keep it in CPU)
        """

        # Transform our dataset to their type of dataset
        #  Need to do this each budget level time to make sure all labels from
        #  active learning


        self.ddp_num_gpus = len(self.device)
        if self.config["use_ddp"] and torch.cuda.device_count() > 1 and self.ddp_num_gpus > 1:

            os.makedirs(self.cfg.OUTPUT_DIR, exist_ok=True)
            self.trainer = DefaultTrainer(self.cfg)
            self.trainer.resume_or_load(resume=False)
            # load base model weights and set the
            # requires_grad to False for the base model.
            self.load_model()

            work_dir = str(os.path.join(os.getcwd(), 'FsDet_ddp/'))
            os.makedirs(work_dir, exist_ok=True)

            # dump model to file
            chkptr = DetectionCheckpointer(self.trainer.model, work_dir, 
                optimizer=self.trainer.optimizer, scheduler=self.trainer.scheduler)
            chkptr.save(name='initial_model')

            # dump config to file
            config_str = self.cfg.dump()
            config_dct = yaml.safe_load(config_str)
            with open(os.path.join(work_dir, "original_config.yaml"), 'w') as yamlfile:
                yaml.dump(config_dct, yamlfile)

            
            # save dataset stuff for rebuilding dataset
            ddp_config = {}
            ddp_config["config"] = self.config

            target_dataset = self.toolset['target_dataset']
            test_dataset = self.toolset['eval_dataset']

            target_image_fnames = []
            for img_fname in target_dataset.image_fnames:
                target_image_fnames.append(str(img_fname))

            test_image_fnames = []
            for img_fname in test_dataset.image_fnames:
                test_image_fnames.append(str(img_fname))

            target_targets = []
            for target in target_dataset.targets:
                if target is None:
                    target_targets.append(target)
                else:
                    new_target = []
                    for dic in target:
                        new_dic = {}
                        new_dic["category"] = int(dic["category"])
                        new_dic["bbox"] = dic["bbox"].cpu().numpy().tolist()
                        new_target.append(new_dic)
                    target_targets.append(new_target)

            test_targets = []
            for target in target_dataset.targets:
                if target is None:
                    test_targets.append(target)
                else:
                    new_target = []
                    for dic in target:
                        new_dic = {}
                        new_dic["category"] = int(dic["category"])
                        new_dic["bbox"] = dic["bbox"].cpu().numpy().tolist()
                        new_target.append(new_dic)
                    test_targets.append(new_target)


            target_lab_inds = []
            for ind in target_dataset.labeled_indices:
                target_lab_inds.append(int(ind))

            target_unlab_inds = []
            for ind in target_dataset.unlabeled_indices:
                target_unlab_inds.append(int(ind))

            test_lab_inds = []
            for ind in test_dataset.labeled_indices:
                test_lab_inds.append(int(ind))

            test_unlab_inds = []
            for ind in test_dataset.unlabeled_indices:
                test_unlab_inds.append(int(ind))



            ddp_config["target_dataset"] = {"name": target_dataset.name,
                "root": str(target_dataset.root),
                "categories": target_dataset.categories.tolist(),
                "image_fnames": target_image_fnames,
                "num_images": int(target_dataset.num_images),
                "labeled_size": int(target_dataset.labeled_size),
                "unlabeled_size": int(target_dataset.unlabeled_size),
                "targets": target_targets,
                "labeled_indices": target_lab_inds,
                "unlabeled_indices": target_unlab_inds
            }


            ddp_config["test_dataset"] = {"name": test_dataset.name,
                "root": str(test_dataset.root),
                "categories": test_dataset.categories.tolist(),
                "image_fnames": test_image_fnames,
                "num_images": int(test_dataset.num_images),
                "labeled_size": int(test_dataset.labeled_size),
                "unlabeled_size": int(test_dataset.unlabeled_size),
                "targets": test_targets,
                "labeled_indices": test_lab_inds,
                "unlabeled_indices": test_unlab_inds
            }

            if WANDB_INFO.run_wandb: 
                ddp_config["wandb_name"] = self.wandb_name 


            with open(os.path.join(work_dir, 'ddp_config.json'), 'w') as fp:
                json.dump(ddp_config, fp)


            num_gpus = int(self.ddp_num_gpus)
            if num_gpus > torch.cuda.device_count():
                num_gpus = torch.cuda.device_count()


            ## launch with torch.distributed.launch
            # training_args = ['--work_dir', work_dir, '--seed', '0', '--gpus', str(num_gpus)]
            # training_script = os.path.join(get_original_cwd(), './learn/algorithms/FsDet/distributed.py')
            # from learn.algorithms.BU_NLP.distributed_launch import torch_distributed_launch
            # torch_distributed_launch(nproc_per_node=num_gpus, training_script=training_script, training_script_args=training_args)


            # launch with detectron's distributed launch function
            os.environ["MASTER_ADDR"] = "127.0.0.2"
            os.environ["MASTER_PORT"] = str(self.config["port"])
            os.environ['CUDA_VISIBLE_DEVICES'] = ','.join([str(x) for x in self.device])
            launch(trainer_func,
                num_gpus_per_machine=num_gpus,
                num_machines=1,
                dist_url='env://',
                args=(work_dir,)
            )

            print("Re-entered main process, loading trained model")
            chkptr.load(os.path.join(work_dir, "final_model.pth"))
            print("Finished loading trained model")


            # make sure the set your model to GPU if you want to save it since it is
            # taking up a lot of gpu memory that other approaches need to run
            # self.trainer.model = self.trainer.model.cpu()
            if os.path.exists(os.path.join(self.cfg.OUTPUT_DIR, "model_final.pth")):
                self.cfg.MODEL.WEIGHTS = os.path.join(self.cfg.OUTPUT_DIR, "model_final.pth")
            gc.collect()  # Make sure all object have ben deallocated if not used
            torch.cuda.empty_cache()


        else:
            os.makedirs(self.cfg.OUTPUT_DIR, exist_ok=True)
            self.trainer = DefaultTrainer(self.cfg)
            self.trainer.resume_or_load(resume=False)
            # load base model weights and set the
            # requires_grad to False for the base model.
            self.load_model()
            self.trainer.train()

            # make sure the set your model to GPU if you want to save it since it is
            # taking up a lot of gpu memory that other approaches need to run
            # self.trainer.model = self.trainer.model.cpu()
            if os.path.exists(os.path.join(self.cfg.OUTPUT_DIR, "model_final.pth")):
                self.cfg.MODEL.WEIGHTS = os.path.join(self.cfg.OUTPUT_DIR, "model_final.pth")
            gc.collect()  # Make sure all object have ben deallocated if not used
            torch.cuda.empty_cache()


    
    def hyperparameter_search(self):

        logger.info('Hyperparameter tuning for FsDet')

        lrs = self.hyperparam_config["learning_rate"]
        freeze_backbone = self.hyperparam_config["fsnet_freeze_backbone"]

        # original_labeled_indices = copy.deepcopy(self.toolset['target_dataset'].get_labeled_indices())
        target_labeled_inds = self.toolset['target_dataset'].get_labeled_indices()

        print('number of labeled indices: ', len(target_labeled_inds))

        # ******* rather than changing the target indices themselves, it would be better
        # to just pass them as an argument into the lambda function for the data catalogs


        # just take random percentage of labeled indices
        num_train = int((1 - self.hyperparam_config["val_ratio"]) * len(target_labeled_inds))
        target_labeled_inds = (np.array(target_labeled_inds)[torch.randperm(len(target_labeled_inds))]).tolist()
        train_inds = target_labeled_inds[:num_train]
        val_inds = target_labeled_inds[num_train:]
        # self.toolset['target_dataset'].labeled_indices = set(train_inds)

        # gt = np.array(self.toolset['target_dataset'].targets)[val_inds].tolist()

        print('number of validation inds: ', len(val_inds))

        # what to do with val_inds?


        # clone the config and edit fields with new target and eval=val_set
        # create a trainer on the config and train it
        # do evaluation on the eval set (which is now the validation set)


        print(self.toolset["target_dataset"].targets[0:2])
        print(self.toolset["target_dataset"].targets[0])
        print()

        # for i in range(len(self.toolset["target_dataset"].targets)):
        #     if self.toolset["target_dataset"].targets[i] is not None:
        #         print(self.toolset["target_dataset"].targets[i])

        train_dataset = self.toolset['target_dataset']
        test_dataset = self.toolset['target_dataset']


        self.target_dataset_name_hyperparam = f'{train_dataset.name}_train_hyperparam'
        self.eval_dataset_name_hyperparam = f'{test_dataset.name}_test_hyperparam'
        # if self.target_dataset_name_hyperparam in MetadataCatalog.list():
        #     self.target_dataset_name_hyperparam = f'{train_dataset.name}_train_adapt_hyperparam'
        #     self.eval_dataset_name_hyperparam = f'{test_dataset.name}_test_adapt_hyperparam'

        print('train name: ', self.target_dataset_name_hyperparam)
        print('test name: ', self.eval_dataset_name_hyperparam)


        # training data for hyperparameter tuning
        DatasetCatalog.register(self.target_dataset_name_hyperparam, lambda d=train_dataset: self.transform_dataset_hyperparam(d, train_inds))
        train_cats = [str(cat) for cat in train_dataset.categories.tolist()]
        MetadataCatalog.get(self.target_dataset_name_hyperparam).thing_classes = train_cats  # have to change categories?

        # testing data for hyperparameter tuning
        DatasetCatalog.register(self.eval_dataset_name_hyperparam, lambda d=test_dataset: self.transform_dataset_hyperparam(d, val_inds))
        test_cats = [str(cat) for cat in test_dataset.categories.tolist()]
        MetadataCatalog.get(self.eval_dataset_name_hyperparam).thing_classes = test_cats  # have to change categories?

        num_cats = len(test_dataset.categories)

        hyperparam_search_config = self.cfg.clone()
        hyperparam_search_config.defrost()

        hyperparam_search_config.DATASETS.TRAIN = (self.target_dataset_name_hyperparam,)
        hyperparam_search_config.DATASETS.TEST = (self.eval_dataset_name_hyperparam,)
        hyperparam_search_config.SOLVER.MAX_ITER = self.hyperparam_config['val_step']
        


        # put these datasets in the config, then 


        # need to get the ground truth annotations from eval
        # gt needs to be a dataframe with cols inp[['id', 'class', 'confidence', 'xmin', 'ymin', 'xmax', 'ymax']]
        # confidence is optional

        gt_df = pd.DataFrame(columns=['id', 'class', 'xmin', 'ymin', 'xmax', 'ymax'])


        target_targets = copy.deepcopy(self.toolset["target_dataset"].targets)
        targs = [target_targets[x] for x in range(0,len(target_targets)) if x in val_inds]

        print('len targs: ', len(targs))
        print('len val inds: ', len(val_inds))

        for targ, ind in zip(targs, val_inds):

            for det in targ:
                class_ = det['category']
                bbox = det['bbox'].cpu().numpy().tolist()

                x_min = float(bbox[0])
                y_min = float(bbox[1])
                x_max = float(bbox[2])
                y_max = float(bbox[3])

                bbox_str = str(x_min)+','+str(y_min)+','+str()

                ind_ = self.toolset["target_dataset"]._indices_to_fnames([ind])[0]

                # add = {'id': ind_, 'class': class_, 'xmin': x_min, 'ymin': y_min, 'xmax': x_max, 'ymax': y_max}

                add = {'id': ind_, 'class': class_, 'bbox': [x_min, y_min, x_max, y_max]}
                
                
                
                gt_df = gt_df.append(add, ignore_index=True)

        gt_df["class"] = gt_df["class"].astype(str)
        # gt_df["id"] = gt_df["id"].astype(int)
        gt_df = gt_df.sort_values("id")



        # loop over lrs and freeze
        for freeze in self.hyperparam_config['fsnet_freeze_backbone']:
            for lr in self.hyperparam_config['learning_rate']:
            
                hyperparam_search_config.SOLVER.BASE_LR = lr

                # print(hyperparam_search_config)

                self.trainer = DefaultTrainer(hyperparam_search_config)
                self.trainer.resume_or_load(resume=False)

                print('loading model')
                self.load_model_hyperparam(freeze)  # edit freeze parameter here
                
                print('training for hyperparameter tuning')
                self.trainer.train()



                eval_data_loader = self.trainer.build_test_loader(hyperparam_search_config, self.eval_dataset_name_hyperparam)
                outputs = inference_on_dataset(self.trainer.model, eval_data_loader)
                indices = []
                bbox = []
                conf = []
                classes = []

                # overall_list = []

                # for item in eval_data_loader:
                #     print(item)

                print('len outputs: ', len(outputs))
                print(outputs)


                for o in outputs:
                    if len(o['instances']) > 0:
                        for i in range(len(o['instances'])):
                            indices.append(o['image_id'])
                            bbox_loc = map(str,
                                        o['instances'].pred_boxes.tensor[
                                            i].int().tolist())
                            # bbox_loc = o['instances'].pred_boxes.tensor[i].int().tolist()
                            # print('bbox loc: ', bbox_loc)
                            bbox.append(', '.join(list(bbox_loc)))
                            # print(bbox)
                            # exit()
                            conf.append(o['instances'].scores[i].item())
                            classes.append(o['instances'].pred_classes[i].item())

                            # overall_list.append([o['image_id'], o['instances'].pred_classes[i].item(), o['instances'].scores[i].item(), *bbox_loc])

                print('indices: ', indices)
                print('conf: ', conf)
                print('classes: ', classes)


                # print(np.max(indices))
                # print(np.min(indices))
                # print(np.max(val_inds))
                # print(np.min(val_inds))
                print(len(val_inds))
                print(len(conf))
                print(len(bbox))
                print(len(classes))
                print()
                print('val inds: ', val_inds)
                print('indices: ', indices)
                print('set gt_df: ', set(gt_df['id']))
                print('set indices: ', set(indices))
                print('set val inds: ', set(val_inds))
                print()
                print('number of labeled indices: ', len(target_labeled_inds))
                print('len train inds: ', len(train_inds))
                print('len val inds: ', len(val_inds))
                # exit()


                preds = (bbox, conf, classes)
                preds_formatted = self.toolset["target_dataset"].format_predictions(preds, indices)
                pred = pd.DataFrame(preds_formatted).sort_values("id")

                print()
                print('length and set')
                print(set(gt_df['id'].unique().tolist()))
                print(set(pred['id'].unique().tolist()))
                print(len(set(gt_df['id'].unique().tolist()).difference(set(pred['id'].unique().tolist()))))
                print(set(gt_df['id'].unique().tolist()).difference(set(pred['id'].unique().tolist())))

                map_ = mAP(pred, gt_df)

                print(map_)
                exit()

        for img, lab, _ in self.toolset['target_dataset']:
            print('img shape: ', img.shape)
            print("ind: ", _)
            print(len(lab))  # [{'category': -1, 'bbox': tensor([-1, -1, -1, -1])}]
            print(lab[:2])
            print(lab[0])
            exit()


        # for lr in lrs:
        #     for freeze in freeze_backbone:



        DatasetCatalog.remove(self.target_dataset_name_hyperparam)
        DatasetCatalog.remove(self.eval_dataset_name_hyperparam)
        MetadataCatalog.remove(self.target_dataset_name_hyperparam)
        MetadataCatalog.remove(self.eval_dataset_name_hyperparam)

        # self.toolset['target_dataset'].labeled_indices = original_labeled_indices
        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()
    
    
    
    
    def inference(self):
        """
        Inference is during the evaluation stage.  Predict all images on the eval
        dataset (as the example below is doing).

        Returns:
            tuple(list[tuple],list[int]): preds and indices
                predicted category indices and image indices

        """
        model = self.trainer.model.cuda()
        data_loader = self.trainer.build_test_loader(self.cfg, self.eval_dataset_name)

        outputs = inference_on_dataset(model, data_loader)
        indices = []
        bbox = []
        conf = []
        classes = []

        # Return only the top N results (based on confidence) if "top_n"
        # in config, else return all.
        top_n = self.config.get("top_n", None)

        for o in outputs:
            if len(o['instances']) > 0:
                if top_n:
                    idxs = torch.topk(
                        o['instances'].scores, min(top_n, len(o['instances']))
                    ).indices.tolist()
                else:
                    idxs = range(len(o['instances']))

                for i in idxs:
                    indices.append(o['image_id'])
                    bbox_loc = map(str,
                                   o['instances'].pred_boxes.tensor[
                                       i].int().tolist())
                    bbox.append(', '.join(list(bbox_loc)))
                    conf.append(o['instances'].scores[i].item())
                    classes.append(o['instances'].pred_classes[i].item())

        preds = (bbox, conf, classes)

        # Random response useful to debugging to make sure you have the correct
        #    format.
        # preds2, indices2 = self.toolset["eval_dataset"].dummy_data('object_detection')
        logger.info(f'Predicted {len(indices)} objects on {len(data_loader)} images.')
        logger.info(f"Unique indices in the datasets {len(np.unique(indices))}")
        # self.trainer.model = self.trainer.model.cpu()
        del self.trainer
        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()
        return preds, indices

    @staticmethod
    def transform_dataset(dataset):
        """ Transform our datasets into the coco format which can be then be registered
        into the dectectron system.  For now we are only training it but we might
        want to create a validation dataset to test on as well

        Args:
            dataset (framework.dataset):  Dataset from framework

        Returns
            dict in coco format.
        """

        # ######################### Get the code ready for detectron
        # Convert our dataset
        logger.info('Converting target_dataset to coco dataset')
        from detectron2.structures import BoxMode
        from detectron2.data.detection_utils import read_image
        from PIL import Image

        num_images = dataset.num_images
        dataset_dict = []
        num_records = 0
        for index in range(num_images):
            record = dict()
            record['file_name'] = dataset.root + '/' + dataset.image_fnames[index]
            img = Image.fromarray(read_image(record['file_name']))
            record['width'], record['height'] = img.size
            record['image_id'] = index
            record['annotations'] = list()
            targets = dataset.targets[index]
            if targets is not None:
                for target in targets:
                    record['annotations'].append(dict({
                        'bbox': target['bbox'].tolist(),
                        'bbox_mode': BoxMode.XYXY_ABS,
                        'category_id': target['category']
                    }))
                num_records += 1
            dataset_dict.append(record)

        logger.info(f"Transformed the dataset into COCO style. "
                     f"Num Images {len(dataset_dict)} and Num Annotations: {num_records}")

        return dataset_dict


    @staticmethod
    def transform_dataset_hyperparam(dataset, inds):
        """ Transform our datasets into the coco format which can be then be registered
        into the dectectron system.  For now we are only training it but we might
        want to create a validation dataset to test on as well

        Args:
            dataset (framework.dataset):  Dataset from framework
            eval (boolean): whether or not we should do eval portion of dataset

        Returns
            dict in coco format.
        """

        # ######################### Get the code ready for detectron
        # Convert our dataset
        logger.info('Converting target_dataset to coco dataset')
        from detectron2.structures import BoxMode
        from detectron2.data.detection_utils import read_image
        from PIL import Image

        # if eval:
        #     # eval inds
        #     union_lab_unlab = np.union1d(dataset.get_unlabeled_indices(), dataset.get_labeled_indices())
        #     diff = np.setdiff1d(np.arange(len(dataset)), union_lab_unlab)
        #     inds = diff.tolist()
        # else:
        #     # train inds
        #     inds = dataset.get_labeled_indices()

        # print('len inds in transform_dataset_hyperparam: ', len(inds))
        # print(inds)


        num_images = dataset.num_images
        dataset_dict = []
        num_records = 0
        for index in range(num_images):
            if index not in inds:
                continue

            record = dict()
            record['file_name'] = dataset.root + '/' + dataset.image_fnames[index]
            img = Image.fromarray(read_image(record['file_name']))
            record['width'], record['height'] = img.size
            record['image_id'] = index
            record['annotations'] = list()
            targets = dataset.targets[index]
            if targets is not None and index in inds:
                for target in targets:
                    # print(target)
                    # print(target['category'])
                    # print(type(target['category']))
                    # print(type(inds[0]))
                    # exit()
                    record['annotations'].append(dict({
                        'bbox': target['bbox'].tolist(),
                        'bbox_mode': BoxMode.XYXY_ABS,
                        'category_id': target['category']
                    }))
                num_records += 1
            dataset_dict.append(record)

        logger.info(f"Transformed the dataset into COCO style. "
                     f"Num Images {len(dataset_dict)} and Num Annotations: {num_records}")

        return dataset_dict



    def load_model(self):
        # assume the trained base model saved in cfg.MODEL.WEIGHTS and
        # the last layer of the model is randomly initialized and matches the
        # number of novel class.
        output_dir = self.cfg.OUTPUT_DIR
        if self.config['warm_start']:
            model_weights = self.cfg.MODEL.WEIGHTS
        else:
            model_weights = self.toolset["source_network"].model_cfg.MODEL.WEIGHTS

        DetectionCheckpointer(self.trainer.model, save_dir=output_dir)._load_file(model_weights)
        # Freeze the base model except for the box predictor.
        # Following the naming of the Faster R-CNN
        if self.config['fsnet_freeze_backbone']:
            for k, p in self.trainer.model.named_parameters():
                if not ('box_predictor' in k
                        or 'bbox_pred' in k
                        or 'cls_score' in k):
                    p.requires_grad = False

            logger.info('freezing the backbone!')
        else:
            logger.info('NOT freezing the backbone!')


    def load_model_hyperparam(self, freeze):
        # assume the trained base model saved in cfg.MODEL.WEIGHTS and
        # the last layer of the model is randomly initialized and matches the
        # number of novel class.
        output_dir = self.cfg.OUTPUT_DIR
        model_weights = copy.deepcopy(self.cfg.MODEL.WEIGHTS)
        model = copy.deepcopy(self.trainer.model)
        DetectionCheckpointer(model, save_dir=output_dir)._load_file(model_weights)
        # Freeze the base model except for the box predictor.
        # Following the naming of the Faster R-CNN
        if freeze:
            for k, p in model.named_parameters():
                if not ('box_predictor' in k
                        or 'bbox_pred' in k
                        or 'cls_score' in k):
                    p.requires_grad = False

            logger.info('freezing the backbone!')
        else:
            logger.info('NOT freezing the backbone!')
