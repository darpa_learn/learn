import abc
from learn.deprecated_tinker.basealgorithm import BaseAlgorithm


class ObjectDetectionAdapter(BaseAlgorithm):

    def __init__(self, toolset):
        BaseAlgorithm.__init__(self, toolset)

    def execute(self, toolset, step_descriptor):

        self.toolset = toolset
        if step_descriptor == 'Initialize':
            return self.initialize()
        elif step_descriptor == "HyperparameterSearch":
            print("reached hyperparameter search in adapter!!!")
            return self.hyperparameter_search()
        elif step_descriptor == 'DomainAdaptTraining':
            return self.domain_adapt_training()
        elif step_descriptor == 'EvaluateOnTestDataSet':
            return self.inference()
        else:
            raise NotImplementedError(f'Step {step_descriptor} not implemented')

    @abc.abstractmethod
    def initialize(self):
        raise NotImplementedError

    @abc.abstractmethod
    def domain_adapt_training(self):
        raise NotImplementedError

    @abc.abstractmethod
    def inference(self):
        raise NotImplementedError

    @abc.abstractmethod
    def hyperparameter_search(self):
        raise NotImplementedError
