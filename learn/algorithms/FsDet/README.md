# Few-Shot Object Detection (FsDet)

FsDet contains the few-shot object detection implementation used in the paper 
[Frustratingly Simple Few-Shot Object Detection](https://arxiv.org/abs/2003.06957).