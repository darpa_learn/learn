import torch
from learn.algorithms.CoMix.videoClassificationAdapter import VideoClassifierAdapter
import logging

import os
import torch.optim as optim
from torch import nn
import torch.nn.functional as F
import math
import numpy as np
from learn.algorithms.CoMix.utils import *
from learn.algorithms.CoMix.models import *
from learn.algorithms.CoMix.models.graph_model import *
import time
import datetime
import copy
from torch.autograd import Function
from learn.algorithms.CoMix.core.losses import * 
from learn.algorithms.CoMix.i3d.pytorch_i3d import InceptionI3d
import random
import warnings
warnings.filterwarnings("ignore")
from hydra.utils import get_original_cwd, to_absolute_path
from learn.utils.wandb_utils import WANDB_INFO
import logging

from learn.algorithms.TimeSformer.models.vit import vit_base_patch16_224
from learn.algorithms.TimeSformer.models.swin import swin_s

log = logging.getLogger(__name__)


class VideoClassifierAlgorithm(VideoClassifierAdapter):
    # Implementation of Contrast and Mix (CoMix) for Video Domain Adaptation.

    def __init__(self, toolset):
        # def __init__(self, problem, base_dataset, adapt_dataset, arguments):
        VideoClassifierAdapter.__init__(self, toolset)
        self.config = toolset['protocol_config']['video_classifier']['late']['params']

    def initialize(self):
        """  Initialize your algorithm and save any parameters or objects you want
        to be persistent between checkpoints.

        """

        # pre-trained network on source task (the closest task)
        #  You are not required to use it but strongly suggested to use it
        #  These are in the style of torchvision models

        #self.graph_model = Graph_Model()

        self.model_backbone = None#InceptionI3d(400, in_channels=3)
        #model_path = os.path.join(get_original_cwd(),
        #        'learn/algorithms/CoMix/models/rgb_imagenet.pt')
        #self.model_backbone.load_state_dict(torch.load(model_path))
        if self.toolset['stage'] == 'adapt':
            self.toolset['source_dataset'].do_background_extract(self.config['bg_output_dir'])
            self.toolset['target_dataset'].do_background_extract(self.config['bg_output_dir'])
            self.use_bg_extract = True
            # make mu be 1 to make sure background shapes line up
            self.config['mu'] = 1
        else:
            self.use_bg_extract = False

    @staticmethod
    def get_weights_for_sampler(dataset, useful_cats):
        """ get weight the dataset to give to pytorch's weightedrandomsampler.
        This is instead of subset selector so that we can weight each element.

        Args:
            dataset (ImageClassificationDataset): dataset which you are weighting
            useful_cats (list[str]): categories to weight, usually for removing
                categories from source dataset

        Returns:
            list[float]: weights for random sampler
        """
        # get counts for each class
        targets = np.array(dataset.targets)
        n = len(targets)
        cls, counts = np.unique(targets[targets!=None], return_counts=True)

        count_dict = dict(zip(cls, counts))

        # Set counts to 0 if unused
        cats = dataset.categories
        cats_to_drop = list(set(cats) - set(useful_cats))
        drop_idxs = dataset._category_name_to_category_index(cats_to_drop)

        weight_dict = dict({None: 0})
        for k, v in count_dict.items():
            if k in drop_idxs:
                weight_dict[k] = 0
            else:
                weight_dict[k] = float(n-v)/n

        # Set weights
        return [weight_dict[t] for t in targets]

    def get_dataloaders(self, source_dataset, target_dataset):
        """ Get dataloaders 

        Args:
            source_dataset (VideoClassificationDataset): source dataset
            target_dataset (VideoClassificationDataset): target dataset

        Returns:
            dataloaders for source and target

        """
        class_names = target_dataset.categories
        logging.info('Creating dataloaders')
        if self.config['backbone'] == 'timesformer':
            if source_dataset is not None:
                source_dataset.num_slow_frames = 48
                source_dataset.num_fast_frames = 96
            target_dataset.num_slow_frames = 48
            target_dataset.num_fast_frames = 96
        elif self.config['backbone'] in ['swin_t', 'swin_s', 'swin_b']: 
            if source_dataset is not None:
                source_dataset.num_slow_frames = 16
                source_dataset.num_fast_frames = 32
            target_dataset.num_slow_frames = 16
            target_dataset.num_fast_frames = 32

        source_labeled_dataloader = None
        if source_dataset is not None:
            source_labeled_dataloader = torch.utils.data.DataLoader(
                source_dataset,
                sampler=torch.utils.data.sampler.SubsetRandomSampler(
                    source_dataset.get_labeled_indices()),
                batch_size=min(source_dataset.labeled_size,
                    int(self.config["batch_size"])),
                num_workers=int(self.config['num_workers']),
                collate_fn=source_dataset.collate_batch,
                drop_last=True
            )

        target_labeled_dataloader = None
        if target_dataset.labeled_size > 0:
            target_labeled_dataloader = torch.utils.data.DataLoader(
                target_dataset,
                sampler=torch.utils.data.sampler.SubsetRandomSampler(
                    target_dataset.get_labeled_indices()),
                batch_size=min(target_dataset.labeled_size,
                    int(self.config["batch_size"])),
                num_workers=int(self.config['num_workers']),
                collate_fn=target_dataset.collate_batch,
                drop_last=True
            )

        # here the unlabeled dataloader is actually the labeled and unlabeled samples
        target_unlabeled_dataloader = torch.utils.data.DataLoader(
            target_dataset,
            sampler=torch.utils.data.sampler.RandomSampler(target_dataset, replacement = True, num_samples = 10000000),
            batch_size=int(self.config["batch_size"]),# * int(self.config["mu"])),
            num_workers=int(self.config['num_workers']),
            collate_fn=target_dataset.collate_batch
        )

        return source_labeled_dataloader, target_labeled_dataloader, target_unlabeled_dataloader, class_names

    def get_overlap_dataloader(self, source_dataset, target_dataset):
        """ Get dataloader for source overlapped classes (that are in target) 

        Args:
            source_dataset (VideoClassificationDataset): source dataset
            target_dataset (VideoClassificationDataset): target dataset

        Returns:
            dataloaders for source which overlaps with target classes
        """
        source_cats = set(source_dataset.categories.tolist())
        target_cats = set(target_dataset.categories.tolist())
        common_categories = source_cats & target_cats

        if len(common_categories) == 0:
            return None # No overlap

        common_cat_int = source_dataset._category_name_to_category_index(common_categories)
        target_cat_int = target_dataset._category_name_to_category_index(common_categories)

        source_label_mapping ={} 
        for i in range(len(common_cat_int)):
            source_label_mapping[common_cat_int[i]] = target_cat_int[i]

        overlap_indices = []
        for i,x in enumerate(source_dataset.targets):
            if x in common_cat_int:
                overlap_indices.append(i)

        source_overlap_dataloader = torch.utils.data.DataLoader(
                source_dataset,
                sampler=torch.utils.data.sampler.SubsetRandomSampler(
                    overlap_indices),
                batch_size=min(len(overlap_indices),
                    int(self.config["batch_size"])),
                num_workers=int(self.config['num_workers']),
                collate_fn=source_dataset.collate_batch,
                drop_last=False
            )

        return source_overlap_dataloader, source_label_mapping

    def get_backbone(self, num_classes):
        if self.config['backbone'] == 'slowfast':
            model_backbone_pretrained = torch.hub.load('facebookresearch/pytorchvideo:main', 'slow_r50', pretrained=True)
            return nn.Sequential(model_backbone_pretrained, nn.Linear(400, num_classes))
        elif self.config['backbone'] == 'timesformer':
            model_config = {
                'crop_size': 224,
                'num_classes': num_classes,
                'num_frames': 96,
            }
            #model_path = os.path.join(get_original_cwd(), 'learn/algorithms/TimeSformer/models/TimeSformer_divST_96x4_224_K400.pyth')
            model_path = os.path.join(self.toolset['protocol_config']['domain_network_selector']['params']['pretrained_network_dir'], 
                    'TimeSformer_divST_96x4_224_K400.pyth')
            state_dict = torch.load(model_path, map_location="cpu")['model_state']
            state_dict = {k: v for k, v in state_dict.items() if "head" not in k}
            model = vit_base_patch16_224(model_config)
            model.load_state_dict(state_dict, strict=False)
            return model
        elif self.config['backbone'] == 'swin_s':
            #model_path = os.path.join(
            #    get_original_cwd(), 
            #    'learn/algorithms/TimeSformer/models/swin_small_patch244_window877_kinetics400_1k.pth')
            model_path = os.path.join(
                self.toolset['protocol_config']['domain_network_selector']['params']['pretrained_network_dir'],
                'swin_small_patch244_window877_kinetics400_1k.pth'
            )
            return swin_s(model_path, num_classes)
        else:
            raise NotImplementedError("Backbone {} not implemented".format(self.config['backbone']))


    def freeze_backbone(self):
        if self.config['backbone'] == 'slowfast':
            for p in self.model_backbone.parameters():
                p.requires_grad = False
            for p in self.model_backbone[0].blocks[4].parameters():
                p.requires_grad = True
            for p in self.model_backbone[0].blocks[5].parameters():
                p.requires_grad = True
            for p in self.model_backbone[1].parameters():
                p.requires_grad = True
        elif self.config['backbone'] == 'timesformer':
            for n, p in self.model_backbone.named_parameters():
                if "model.head" in n:
                    p.requires_grad = True
                else:
                    p.requires_grad = False
        elif self.config['backbone'] in ['swin_t', 'swin_s', 'swin_b']:
            for n, p in self.model_backbone.named_parameters():
                if n.startswith("head"):
                    p.requires_grad = True
                else:
                    p.requires_grad = False
        else:
            raise NotImplementedError("Backbone {} not implemented".format(self.config['backbone']))


    def domain_adapt_training(self):
        """ This is where you will fine-tune/train your few-shot or domain adaption
        algorithm.  You may use the dataloaders below and feel free to override the
        transformers in the datasets to use whatever pretraining you desire.
        Please do not modify the datasets though.
        """

        # ###################  Creating the Labeled DataLoader ###############
        # Create Dataloaders for labeled data by getting the labeled indices and
        # creating a random sub-sampler.  Then create the dataloader using this
        # sampler
        #
        #  Note: To change the transformer in the JPLDataset dataset, edit
        #      current_dataset.transform.  You can also edit the labels during
        #      loading by editing the  current_dataset.target_transform
        #
        #  Note: Another approach could be to inherit the JPLDataset class and
        #      override the __get_item__ function
        #
        #  Check out dataset.py for more information
        #     Notes: There could be NO unlabeled data and the algorithm has to be
        #         able to handle that case!
        #
        #         The min batch size here is so that the batch size isn't
        #         larger than the labeled/unlabeled dataset which causes the
        #         dataloader to hang

        

        # ##################### fine-tune/train your model ######################
        #  Here is where you will actual finetune the model
        #  You can use the pretrained model with self.model
        #  Make sure to set any persistent tensors to cpu with `tensor.cpu` command


        #########################################################################
        #
        # Dataloaders come here ... labeled_dataloader, unlabeled_dataloader, data_loader_eval...
        #
        #########################################################################
 
        if self.toolset['source_dataset'] is not None:
            self.toolset['source_dataset'].use_orginal_dataset = False
        self.toolset['target_dataset'].use_orginal_dataset = False

        source_labeled_dataloader, target_labeled_dataloader, target_unlabeled_dataloader, class_names = self.get_dataloaders(
            self.toolset['source_dataset'], self.toolset['target_dataset']
        )

        if self.use_bg_extract:
            assert source_labeled_dataloader is not None

        num_classes = len(target_unlabeled_dataloader.dataset.categories.tolist()) 

        self.model_backbone = self.get_backbone(num_classes)
        #self.model_backbone = self.model_backbone.cuda()
        #self.model_backbone = nn.DataParallel(self.model_backbone)

        if self.config['use_uda_net'] and self.toolset['ckpt'] > -1 and self.toolset['stage'] == 'adapt':
            # load weights
            log.info('Loading UDA weights from comix_uda_model.pth')
            self.model_backbone.load_state_dict(torch.load('comix_uda_model.pth'))


        optimizer = optim.SGD(self.model_backbone.parameters(),
                            lr=self.config['learning_rate'],
                            weight_decay=self.config['weight_decay'],
                            momentum=self.config['momentum'] )

        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, float(self.config['num_iter_adapt']))

        if target_labeled_dataloader is not None:
            len_labeled_dataloader = len(target_labeled_dataloader)
            log.info('len_labeled_dataloader = '+ str(len_labeled_dataloader))

        if source_labeled_dataloader is not None:
            len_source_dataloader = len(source_labeled_dataloader)
        #    source_labeled_dataloader.dataset.spatial_transform
        len_unlabeled_dataloader = len(target_unlabeled_dataloader) 

        if (self.config['src_overlap_loss_adapt'] or self.toolset['ckpt'] == -1) and self.toolset['stage'] == 'adapt':
            # is UDA
            source_overlap_dataloader, source_label_mapping_overlap = self.get_overlap_dataloader(
                self.toolset["source_dataset"],
                self.toolset["target_dataset"]
            )
            len_source_overlap_dataloader = len(source_overlap_dataloader)
            original_targets = self.toolset['source_dataset'].targets
            self.toolset['source_dataset'].targets = [source_label_mapping_overlap[i] if i in source_label_mapping_overlap 
                    else i for i in self.toolset['source_dataset'].targets]
        else:
            source_overlap_dataloader = None

        print_line()
        log.info('len_unlabeled_dataloader = '+ str(len_unlabeled_dataloader))

        best_accuracy_yet = 0.0
        best_itrn = 0
        best_model_backbone_wts = copy.deepcopy(self.model_backbone.state_dict())

        print_line()
        print_line()
        print_line()

        start_time = time.process_time()
        running_lr = self.config['learning_rate']
        epoch_number = 0
        start_iter = 0 

        if not os.path.exists(self.config['model_root']):
            os.makedirs(self.config['model_root'])

        self.model_backbone = self.model_backbone.train()
        self.freeze_backbone()

        criterion = nn.CrossEntropyLoss().cuda()
        temperature = 0.5
        simclr_loss_criterion = SupConLoss(temperature=temperature)

        self.model_backbone = self.model_backbone.cuda()
        self.model_backbone = nn.DataParallel(self.model_backbone)
        # for loop for training iterations ...
        start_iter = 0
        num_iterations = self.config['num_iter_adapt']

        cls_loss_func = CrossEntropyLabelSmooth(num_classes=num_classes, epsilon=0.1,
                        size_average=False)

        for itrn in range(start_iter, num_iterations):
            print("\rRunning Iteration: {}/{}".format(itrn, num_iterations),
                end='', flush=True)
            if itrn%100 == 0:
                print(f'Itrn: (T) {itrn+1} LR: {scheduler.get_lr()}')

            if itrn==start_iter:
                if target_labeled_dataloader is not None:
                    iter_labeled_data = iter(target_labeled_dataloader)
                iter_unlabeled_data = iter(target_unlabeled_dataloader)

            if target_labeled_dataloader is not None:
                if itrn % len_labeled_dataloader == 0:
                    iter_labeled_data = iter(target_labeled_dataloader)
                    epoch_number = epoch_number + 1

            if itrn % len_unlabeled_dataloader == 0:            
                iter_unlabeled_data = iter(target_unlabeled_dataloader)

            if target_labeled_dataloader is not None:
                if self.use_bg_extract:
                    feat_labeled_fast, feat_labeled_slow, feat_labeled_fast_spaug, labels, _, bg_tgt_lab = iter_labeled_data.next()
                else:
                    feat_labeled_fast, feat_labeled_slow, feat_labeled_fast_spaug, labels, _ = iter_labeled_data.next()
            #feat_labeled_fast = LAB_DATA[0]
            #feat_labeled_slow = LAB_DATA[1]

            if self.use_bg_extract:
                feat_unlabeled_fast, feat_unlabeled_slow, feat_unlabeled_fast_spaug, _, _, bg_tgt_unl = iter_unlabeled_data.next()
            else:
                feat_unlabeled_fast, feat_unlabeled_slow, feat_unlabeled_fast_spaug, _, _ = iter_unlabeled_data.next()

            if source_labeled_dataloader is not None and self.use_bg_extract:
                if itrn==start_iter or itrn % len_source_dataloader == 0:
                    iter_src_dataloader = iter(source_labeled_dataloader)
                feat_source_fast, feat_source_slow, _, _, _, bg_src = iter_src_dataloader.next()

            #feat_unlabeled_fast = UNLAB_DATA[0]
            #feat_unlabeled_slow = UNLAB_DATA[1]
            #print(feat_unlabeled_fast.shape)

            if self.config['backbone'] == 'timesformer' or self.config['backbone'] in ['swin_t', 'swin_s', 'swin_b']:
                # Repeat slow to fit into the model
                if target_labeled_dataloader is not None:
                    feat_labeled_slow = feat_labeled_slow.repeat_interleave(2, dim=2)
                feat_unlabeled_slow = feat_unlabeled_slow.repeat_interleave(2, dim=2)

            if target_labeled_dataloader is not None:
                feat_labeled_fast = make_variable(feat_labeled_fast)
                feat_labeled_slow = make_variable(feat_labeled_slow)
                feat_labeled_fast_spaug = make_variable(feat_labeled_fast_spaug)

                labels = make_variable(labels)

            feat_unlabeled_fast = make_variable(feat_unlabeled_fast)
            feat_unlabeled_slow = make_variable(feat_unlabeled_slow)
            feat_unlabeled_fast_spaug = make_variable(feat_unlabeled_fast_spaug)

            optimizer.zero_grad()

            cls_loss = None
            if target_labeled_dataloader is not None:
                preds_labeled_fast = self.model_backbone(feat_labeled_fast)

                cls_loss = cls_loss_func(preds_labeled_fast, labels).mean()
            
            if source_overlap_dataloader is not None:
                if itrn == start_iter or itrn % len_source_overlap_dataloader == 0:
                    iter_src_overlap_dataloader = iter(source_overlap_dataloader)
                feat_source_overlap_fast, _, _, overlap_labels, _, _ = iter_src_overlap_dataloader.next() 
                #overlap_labels = [source_label_mapping_overlap[i] for i in overlap_labels]
                overlap_labels = make_variable(overlap_labels)
                feat_source_overlap_fast = make_variable(feat_source_overlap_fast) 
                preds_overlap_fast = self.model_backbone(feat_source_overlap_fast)
                cls_loss_overlap = cls_loss_func(preds_overlap_fast, overlap_labels).mean()

            preds_unlabeled_fast = self.model_backbone(feat_unlabeled_fast)
            preds_unlabeled_slow = self.model_backbone(feat_unlabeled_slow)
            preds_unlabeled_fast_spaug = self.model_backbone(feat_unlabeled_fast_spaug)


            target_logits_fused = (preds_unlabeled_fast.data + preds_unlabeled_slow.data + preds_unlabeled_fast_spaug.data) / 3
            target_logits_softmax = torch.softmax(target_logits_fused, dim=-1)

            max_probs, target_pseudo_labels = torch.max(target_logits_softmax, dim=-1)
            pseudo_mask = max_probs.ge(self.config['pseudo_threshold']).float()

            pseudo_cls_loss = (F.cross_entropy(preds_unlabeled_fast, target_pseudo_labels, reduction='none') * pseudo_mask).mean()

            #if pseudo_mask is None : 
            #    supcon_loss = simclr_loss(torch.softmax(preds_unlabeled_fast, dim=-1), torch.softmax(preds_unlabeled_slow, dim=-1), simclr_loss_criterion)
            #else :
            #    pseudo_mask_new = pseudo_mask > 0
            #    preds_unlabeled_fast_masked = torch.masked_select(preds_unlabeled_fast, pseudo_mask_new.unsqueeze(dim=1).repeat(1, preds_unlabeled_fast.shape[1]))
            #    preds_unlabeled_slow_masked = torch.masked_select(preds_unlabeled_slow, pseudo_mask_new.unsqueeze(dim=1).repeat(1, preds_unlabeled_slow.shape[1]))
            #    target_pseudo_labels_masked = torch.masked_select(target_pseudo_labels, pseudo_mask_new)
            #    preds_unlabeled_fast_masked = preds_unlabeled_fast_masked.reshape(-1, num_classes)
            #    preds_unlabeled_slow_masked = preds_unlabeled_slow_masked.reshape(-1, num_classes)

            #    if preds_unlabeled_slow_masked.shape[0] > 0 :
            #        supcon_loss = simclr_loss(torch.softmax(preds_unlabeled_fast_masked, dim=-1), torch.softmax(preds_unlabeled_slow_masked, dim=-1), simclr_loss_criterion, target_pseudo_labels_masked)
            #    else :
            supcon_loss = torch.tensor(0.0).cuda()

            if self.use_bg_extract:
                tcl_loss_temporal = 0
            else:
                tcl_loss_temporal = simclr_loss(torch.softmax(preds_unlabeled_fast, dim=-1), 
                        torch.softmax(preds_unlabeled_slow, dim=-1), simclr_loss_criterion)
            
            tcl_loss_spatial = simclr_loss(torch.softmax(preds_unlabeled_fast, dim=-1), 
                    torch.softmax(preds_unlabeled_fast_spaug, dim=-1), simclr_loss_criterion)

            tcl_loss = tcl_loss_spatial + tcl_loss_temporal

            annealing_coeff = get_annealing_coefficient((itrn+1)/num_iterations)

            loss = (self.config['lambda_bgm'] * (tcl_loss)) + (self.config['lambda_tpl'] * annealing_coeff * (pseudo_cls_loss))
            
            if source_overlap_dataloader is not None:
                loss += cls_loss_overlap

            if target_labeled_dataloader is not None:
                loss += cls_loss

            if self.use_bg_extract and bg_tgt_unl is not None and bg_src is not None:
                mix_ratio = np.random.uniform(0, self.config['max_gamma'])
                preds_src_mix_slow, preds_tgt_mix_slow = self.bg_mixin(feat_unlabeled_slow.cpu(), feat_source_slow, bg_tgt_unl, bg_src, mix_ratio)#preds_unlabeled_slow)
                preds_src_mix_fast, preds_tgt_mix_fast = self.bg_mixin(feat_unlabeled_fast.cpu(), feat_source_fast, bg_tgt_unl, bg_src, mix_ratio)#preds_unlabeled_slow)
                bg_mix_loss_src = simclr_loss(torch.softmax(preds_src_mix_fast, dim=-1), torch.softmax(preds_src_mix_slow, dim=-1), simclr_loss_criterion)
                
                virtual_label = torch.arange(0, preds_unlabeled_fast.size(0))
                virtual_label = torch.cat((virtual_label, virtual_label), dim=0)
                virtual_label = make_variable(virtual_label)
                concatenate_tgt_unlabeled_fast = torch.cat((preds_unlabeled_fast, preds_tgt_mix_fast), dim=0)
                concatenate_tgt_unlabeled_slow = torch.cat((preds_unlabeled_slow, preds_tgt_mix_slow), dim=0)
                bg_mix_loss_tgt = simclr_loss(torch.softmax(concatenate_tgt_unlabeled_fast, dim=-1), 
                        torch.softmax(concatenate_tgt_unlabeled_slow, dim=-1), 
                        simclr_loss_criterion, labels=virtual_label)
                
                #bg_mix_loss_tgt = simclr_loss(torch.softmax(preds_tgt_mix_fast, dim=-1), torch.softmax(preds_tgt_mix_slow, dim=-1), simclr_loss_criterion)
                bg_mix_loss = bg_mix_loss_tgt + bg_mix_loss_src
                loss += self.config['lambda_bgm'] * bg_mix_loss
            else:
                bg_mix_loss = None
            
            loss.backward()
         
            optimizer.step()
            
            scheduler.step()

            # Log updates.
            if ((itrn + 1) % self.config['log_in_steps'] == 0):
                print_line()
                log.info("Iteration [{}/{}]: ce_loss={} ce_loss_overlap={} tcl_loss={} tpl_loss={} bg_mix_loss={} total_loss={}"
                      .format(itrn + 1,
                              num_iterations,
                              cls_loss.item() if cls_loss is not None else -1,
                              cls_loss_overlap.item() if source_overlap_dataloader is not None else -1,
                              tcl_loss.item() if tcl_loss is not None else -1,
                              pseudo_cls_loss.item() if pseudo_cls_loss is not None else -1,
                              bg_mix_loss.item() if bg_mix_loss is not None else -1,
                              loss.item()
                              ))
                print_line()
                print_line()
                if WANDB_INFO.run_wandb:
                    metric_info = {
                        'iter': itrn+1,
                        'cls_loss' : cls_loss.item(),
                        'tcl_loss' : tcl_loss.item(),
                        'supcon_loss' : supcon_loss.item(),
                        'tpl_loss' : pseudo_cls_loss.item(),
                        'total_loss' : loss.item(),
                        'learning_rate' : scheduler.get_lr()
                    }

                    if self.use_bg_extract and bg_tgt_unl is not None and bg_src is not None:
                        metric_info['bg_mixing_loss'] = bg_mix_loss.item()

                    if source_overlap_dataloader is not None:
                        metric_info['cls_loss_overlap'] = cls_loss_overlap.item()

                    WANDB_INFO.log_metrics(metric_info)

            # Evaluate model on the validation set.
            if ((itrn + 1) % self.config['eval_in_steps'] == 0):
                
                # Call the unsupervised validation function if required.

                #avg_loss_val = loss_val / len(data_loader_eval)
                #avg_acc_val = float(acc_val.cpu().numpy()) / len(data_loader_eval.dataset)

                checkpoint_path_current = os.path.join(self.config['model_root'], "Current-Checkpoint.pt")
                torch.save({
                            'iter':itrn+1,
                            'epoch_number':epoch_number,
                            'backbone':self.model_backbone.state_dict(),
                            'optimizer':optimizer.state_dict(),
                            'scheduler':scheduler.state_dict(),
                            'best_accuracy_yet':best_accuracy_yet,
                            'best_itrn':best_itrn
                            },checkpoint_path_current)

                '''if(best_accuracy_yet <= avg_acc_val):
                    best_accuracy_yet = avg_acc_val
                    best_model_backbone_wts = copy.deepcopy(self.model_backbone.state_dict())

                    save_model(self.model_backbone, "ModelBackbone-CoMix-Model-Best.pth", self.config["model_root"])

                    best_itrn = itrn + 1
                    checkpoint_path_best = os.path.join(self.config['model_root'], "Best-Checkpoint.pt")
                    torch.save({
                            'iter':itrn+1,
                            'epoch_number':epoch_number,
                            'backbone':self.model_backbone.state_dict(),
                            'optimizer':optimizer.state_dict(),
                            'scheduler':scheduler.state_dict(),
                            'best_accuracy_yet':best_accuracy_yet,
                            'best_itrn':best_itrn
                            },checkpoint_path_best)

                print('best_acc_yet: ', best_accuracy_yet, ' ( in itrn:', best_itrn, ')...')
                self.model_backbone.train()
                print_line()

                end_time_eval = time.process_time()
                print("Avg Loss = {}, Avg Acc = {}".format(avg_loss_val, str(avg_acc_val)))
                print_line()'''

        self.model_backbone = self.model_backbone.cpu()
        if self.config['use_uda_net'] and self.toolset['ckpt'] == -1:
            # save weights
            log.info('Saving UDA weights to comix_uda_model.pth')
            torch.save(self.model_backbone.state_dict(), 'comix_uda_model.pth')

        import gc
        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()

    def bg_mixin(self, feat_tgt, feat_src, bg_tgt, bg_src, mix_ratio):
        src_mix_tgt_bg = (feat_src*(1-mix_ratio)) + (bg_tgt.repeat(1,1, feat_src.shape[2],1,1)*mix_ratio)
        tgt_mix_src_bg = (feat_tgt*(1-mix_ratio)) + (bg_src.repeat(1,1, feat_tgt.shape[2],1,1)*mix_ratio)

        src_mix_tgt_bg = src_mix_tgt_bg.float()
        src_mix_tgt_bg = make_variable(src_mix_tgt_bg)
        
        tgt_mix_src_bg = tgt_mix_src_bg.float()
        tgt_mix_src_bg = make_variable(tgt_mix_src_bg)

        preds_src_mix = self.model_backbone(src_mix_tgt_bg)

        preds_tgt_mix = self.model_backbone(tgt_mix_src_bg)

        return preds_src_mix, preds_tgt_mix
        

    def inference(self, eval_dataset):
        """
        Inference is during the evaluation stage.  For this example, the
        task_network is trained in the train and adapt stage's code and
        is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to
        JPL

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """

        eval_dataset.use_orginal_dataset = False
        
        logging.info('Performing inference ...')
        config = self.config

        # Random predictions
        #preds, indices = self.toolset["eval_dataset"].dummy_data(
        #    'video_classification')
        #return preds,indices



        # override transforms

        eval_dataloader = torch.utils.data.DataLoader(eval_dataset,
            batch_size=int(self.config["batch_size"]),
            drop_last=False  ##### remove num_workers here
        )

        model_backbone = self.model_backbone.eval().cuda()
        #model_backbone = nn.DataParallel(model_backbone)

        softmax = nn.Softmax(dim=1).cuda()

        config['probabilistic_predictions'] = False
        
        preds = []
        indices = []
        with torch.no_grad():
            for feats_fast, _, _, _, inds in eval_dataloader:
                #print("\rInference {}/{}".format(inds, len(eval_dataloader)), end='', flush=True)
                feats_fast = make_variable(feats_fast)
                preds_ = model_backbone(feats_fast)
                if config['probabilistic_predictions']:
                    preds += softmax(preds_).cpu().numpy().tolist()
                else:
                    preds += torch.argmax(preds_, dim=1).cpu().numpy().tolist()

                indices += inds.numpy().tolist()

        model_backbone = model_backbone.cpu()

        del eval_dataloader
        import gc#garbage_collection_cuda
        gc.collect()
        torch.cuda.empty_cache()

        if config['probabilistic_predictions']:
            predictions = pd.DataFrame(preds, columns=np.arange(len(preds[0])))
            predictions['id'] = indices
            return predictions
        else:
            return preds, indices

    def compute_neighborhood_density(self, eval_dataset):
        """
        Inference is during the evaluation stage.
        Given a network and unlabeled samples, the code outputs the criterion of
        how well the samples are clusterd.

        Args:
            eval_dataset (framework.datasets.ImageClassificationDataset): unlabeled evaluation framework.
                We need to predict the classes for each item in the dataset.

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        config = self.config

        # override transforms
        crop_size = 227
        if config['net'] != 'alexnet':
            crop_size = 224

        trans = transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(crop_size),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])
        eval_dataset.transform = trans
        G = self.G.eval().to(self.device)
        F1 = self.F1.eval().to(self.device)
        eval_dataloader = data.DataLoader(eval_dataset,
                                          batch_size=int(self.config["batch_size"]),
                                          drop_last=False
                                          )
        preds = []
        indices = []
        with torch.no_grad():
            for i, (imgs, _, inds) in enumerate(eval_dataloader):
                imgs = imgs.to(self.device)
                output = G(imgs)
                preds_ = F1(output)
                preds_ = F.softmax(preds_)
                if i == 0:
                    pred_all = preds_
                else:
                    pred_all = torch.cat([pred_all, preds_], 0)

        self.G = G.cpu()
        self.F1 = F1.cpu()
        normed_pred = F.normalize(pred_all)
        mat = torch.matmul(normed_pred, normed_pred.t()) / 0.05
        mask = torch.eye(mat.size(0), mat.size(0)).bool()
        mat.masked_fill_(mask, -1 / 0.05)
        density = torch.mean(torch.sum(mat * (torch.log(mat + 1e-5)), 1))
        
        if source_overlap_dataloader is not None:
            self.toolset['source_dataset'].targets = original_targets
        import gc
        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()

        return density


