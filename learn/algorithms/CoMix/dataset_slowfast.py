from learn.utils.dataset import VideoClassificationDataset
import pytorchvideo
from pytorchvideo.transforms import RandAugment
import numpy as np
from learn.utils.dataset import VideoClassificationDataset, pil_loader
import ubelt as ub
from typing import Any, Callable, Dict, Iterator, List, Optional, Set, Tuple, Union
import torchvision
import glob
import torch
import os
import logging

log = logging.getLogger(__name__)

def median_calc(input_frames):
  c, t, h, w = input_frames.shape
  bg_image = np.zeros((3, 1, h, w), np.uint8)
  bg_image[0, 0, :, :] = np.median(input_frames[0, :, :, :], axis=0)
  bg_image[1, 0, :, :] = np.median(input_frames[1, :, :, :], axis=0)
  bg_image[2, 0, :, :] = np.median(input_frames[2, :, :, :], axis=0)
  return bg_image

class VideoClassificationDataset_SlowFast(VideoClassificationDataset):
    
    def __init__(
        self,
        problem: Any,
        dataset_name: str,
        dataset_root: str,
        dataset_split: str,
        data_type: str,
        transform: Any = ub.NoParam,
        target_transform: Optional[Callable] = None,
        categories: Optional[List[str]] = None,
        seed_labels: Optional[List[str]] = None,
    ) -> None:
        """Initialize."""


        '''

        Inherited the VideoClassificationDataset class to mould it into the SlowFast dataset.

        returns: 

        '''

        super(VideoClassificationDataset_SlowFast, self).__init__(
            problem=problem, 
            dataset_name=dataset_name, 
            dataset_root=dataset_root, 
            dataset_split=dataset_split,
            data_type=data_type,
            transform=transform, 
            target_transform=target_transform, 
            categories=categories, 
            seed_labels=seed_labels
        )

        #################################################
        self.num_slow_frames = 8
        self.num_fast_frames = 16 # I have not added these to the arguments of __init__ yet, to avoid errors. Please do that.
        self.transform = torchvision.transforms.Compose([torchvision.transforms.Resize([224, 224]), torchvision.transforms.ToTensor()])
        self.spatial_transform = RandAugment(magnitude=9, num_layers=2, prob=0.5, transform_hparas=None, sampling_type='gaussian', sampling_hparas=None)
        self.use_orginal_dataset = False  # This is a hack flag to use original dataset rather than SlowFast
        #################################################
        self.using_background_extract = False

    def do_background_extract(self, output_dir):
        """
        Runs background extract of dataset and puts background tensor in output_dir
        """
        self.using_background_extract = True
        self.bg_output_dir = os.path.join(output_dir, self.name)
        
        if not os.path.exists(self.bg_output_dir):
            os.makedirs(self.bg_output_dir)
        
        log.info(f"Extracting background images in {self.bg_output_dir}")
        for index,row in self.meta_pd.iterrows():
            #if os.path.exists(os.path.join(self.bg_output_dir, str(clip_id) + '_bg.pt')):
            #    continue
            
            clip_id, video_id, start_frame, end_frame = row

            # Assumes to be in jpg format
            action_frames = [os.path.join(self.root, video_id, str(i) +'.jpg') for i in range(start_frame, end_frame)]
            
            imgs = []
            for img_fname in action_frames:
                img = pil_loader(img_fname)
                img = self.transform(img)
                imgs.append(img)
            imgs = torch.stack(imgs) # TODO: maybe more efficient to do this in for-loop above
            imgs = torch.transpose(imgs, 0, 1) # converting the imgs from t, c, h, w to c, t, h, w
            bg_image = median_calc(imgs)
            
            torch.save(bg_image, os.path.join(self.bg_output_dir, str(clip_id) + '_bg.pt'))

    def __getitem__(self, index: int) -> Tuple[Any, int, int]:
        """
        Return an item by index.

        Args:
            index (int): Index

        Returns:
            tuple([PIL Image], int, int): (image, target, index)
                Image is array of CHW, target is the category index of the target class, and
                index is the index of the image in the dataset
        """
        if self.use_orginal_dataset:
            return super().__getitem__(index)
        
        if isinstance(index, (np.floating, float)):
            index = np.int64(index)

        #clip_id = self.clip_ids[index]
        row = self.meta_pd.loc[index]
        clip_id, video_id, start_frame, end_frame = row
        
        # Assumes to be in jpg format
        action_frames = [os.path.join(self.root, video_id, str(i) +'.jpg') for i in range(start_frame, end_frame)]
        #action_frames = glob.glob(os.path.join(self.root, clip_id, '*'))
        
        target = self.targets[index]

        imgs = []
        for img_fname in action_frames:
            img = pil_loader(img_fname)
            # TODO: right now this assumes the transforms are by image rather
            # than by video. Add two classes of transforms to account for this
            img = self.transform(img)
            imgs.append(img)
        imgs = torch.stack(imgs) # TODO: maybe more efficient to do this in for-loop above
        target_val = target

        #################################################
        # obtaining the slow-fast versions of the frames here ...
        imgs = torch.transpose(imgs, 0, 1) # converting the imgs from t, c, h, w to c, t, h, w
        if self.num_fast_frames <= imgs.shape[1]:
            allRange = np.arange(imgs.shape[1]) # total number of frames in the video
        else:
            # Extend evenly if the frames in video are not enough
            allRange = np.linspace(0, imgs.shape[1] - 1, self.num_fast_frames).astype(np.int64)
        splitRange_fast = np.array_split(allRange, self.num_fast_frames) # divide allRange into 16 equal parts
        splitRange_slow = np.array_split(allRange, self.num_slow_frames) # divide allRange into 8 equal parts

        fast_ids = [np.random.choice(a) for a in splitRange_fast] # from each of the 16 parts, choose an index, which gives us 16 indices
        slow_ids = [np.random.choice(a) for a in splitRange_slow] # from each of the 8 parts, choose an index, which gives us 8 indices

        imgs_fast = imgs[:, fast_ids, :, :]
        imgs_slow = imgs[:, slow_ids, :, :]

        imgs_fast = imgs_fast.contiguous()
        imgs_fast_spaug = torch.transpose(self.spatial_transform(torch.transpose(imgs_fast, 0, 1)), 0, 1)
        #################################################



        if target is None:
            # if nothing, make it -1 for the pytorch collate function
            target_val = self.null_target
        elif self.target_transform is not None:
            target_val = self.target_transform(target)
        elif isinstance(target_val, list) and len(target_val) > 1:
            target_val = [target_val[0]]

        if self.using_background_extract:
            bg_image = torch.load(os.path.join(self.bg_output_dir, str(clip_id) + '_bg.pt'))
            return imgs_fast, imgs_slow, imgs_fast_spaug, target_val, index, bg_image
        else:
            return imgs_fast, imgs_slow, imgs_fast_spaug, target_val, index, 
