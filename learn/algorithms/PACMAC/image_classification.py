import numpy as np
import torch
torch.multiprocessing.set_sharing_strategy("file_system")
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.utils.data as data
import copy
import ubelt as ub
from torchvision import transforms
import pandas as pd
from timm.data.constants import IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD
from RandAugment import RandAugment
import os
from learn.algorithms.PACMAC.imageClassificationAdapter import ImageClassifierAdapter
from learn.algorithms.PACMAC.dataset_wrapper import DatasetWrapper

from learn.utils.dataset import ImageClassificationDataset
import logging
from learn.utils.wandb_utils import WANDB_INFO
import util.misc as misc
from util.misc import NativeScalerWithGradNormCount as NativeScaler
import util.lr_decay as lrd
import util.lr_sched as lr_sched
import models_vit
from timm.loss import LabelSmoothingCrossEntropy
import math
import sys
from typing import Iterable
import tqdm
from sklearn.metrics import confusion_matrix


log = logging.getLogger(__name__)


class ImageClassifierAlgorithm(ImageClassifierAdapter):
    """
    This is the implementation of PACMAC.

    """

    def __init__(self, toolset):
        # def __init__(self, problem, base_dataset, adapt_dataset, arguments):
        ImageClassifierAdapter.__init__(self, toolset)

        self.config = toolset["protocol_config"]["image_classifier"][
            "params"
        ]  # configs/hydra_config/image_classifier/PACMAC.yaml

        device = self.config["device"]
        if ub.iterable(device):
            self.device = device[0]
        else:
            self.device = device

        if self.config["work_dir"] is not None:
            self.work_dir = self.config["work_dir"]
        else: 
            self.work_dir = self.toolset["temp_model_directory"]
        os.makedirs(self.work_dir, exist_ok=True)


    def initialize(
        self,
        source_network,
        source_dataset,
        whitelist_datasets,
        target_dataset,
        networks,
    ):
        """Initialize your algorithm and save any parameters or objects you want to be persistent between
        budget checkpoints.  DomainNetworkSelection will choose a potential source datasets `source_dataset`
        and provide a trained network for that dataset `source_dataset.` Please do not modify the datasets
        beyond overriding the transforms (self.transform and self.transform_target are fine to change).

        Args:
            source_network (pytorch.nn.Module):  Pretrained network on source task
            source_dataset (ImageClassificationDataset): Pytorch dataset for source domain/task
            whitelist_datasets (dict[framework.dataset]): Dictionary of all possible external datasets which
                can be used on this task.  You may use this if you want to select a different dataset than
                the one selected as source.
            target_dataset: (ImageClassificationDataset): Pytorch dataset for target domain/task
            networks: (Dict[str, Dict[str, nn.module]]): Two leveled dict with the dataset as the first key
                and the network structure name as the second key.  e.g. networks['imagenet_1k']['densenet169']
                Note: empty for now for memory savings
        """

        log.info("Initializing PACMAC algorithm")
        # Note: whitelist is contained here in case you want the change the source dataset
        log.debug(f"{whitelist_datasets.keys()}")
        self.source_dataset = source_dataset
        torch.cuda.manual_seed(self.config["seed"])
        self.original_source_network = copy.deepcopy(source_network)
        self.source_network = source_network

        self.target_categories = target_dataset.categories.tolist()
        self.target_categories = list(
            map(
                lambda l: str(l).lower().replace("_", " ").replace("-", " "),
                self.target_categories,
            )
        )
        self.source_categories = source_dataset.categories.tolist()
        self.source_categories = list(
            map(
                lambda l: str(l).lower().replace("_", " ").replace("-", " "),
                self.source_categories,
            )
        )
        config = self.config

        source_cats = set(self.source_categories)
        target_cats = set(self.target_categories)
        common_categories = source_cats & target_cats
        target_only_cats = target_cats - source_cats
        self.total_categories = list(source_cats | target_cats)

        # get mapping list to consider class overlap.
        self.map_source2total = [
            self.total_categories.index(i) for i in self.source_categories
        ]
        self.map_target2total = [
            self.total_categories.index(i) for i in self.target_categories
        ]

        logging.info(f"Number of categories in common: {len(common_categories)}")
        logging.info(f"Number of Categories only in target: {len(target_only_cats)}")

        # Load network and weights
        self.source_network = models_vit.vit_base_patch16(
            num_classes=len(self.source_categories),
            drop_path_rate=config["drop_path"],
            global_pool=config["global_pool"],
        )
        pretrained_path = config["pretrained_path"]
        checkpoint = torch.load(pretrained_path, map_location="cpu")
        if "model" in checkpoint:
            checkpoint = checkpoint["model"]
        self.source_network.load_state_dict(checkpoint, strict=False)

        self.source_network.to(self.device)

        self.trained_on_source = False

    @staticmethod
    def get_weights_for_sampler(dataset):
        """get weight the dataset to give to pytorch's weightedrandomsampler.
        This is instead of subset selector so that we can weight each element.

        Args:
            dataset (ImageClassificationDataset): dataset which you are weighting
            useful_cats (list[str]): categories to weight, usually for removing
                categories from source dataset

        Returns:
            list[float]: weights for random sampler
        """
        # get counts for each class
        targets = np.array(dataset.targets)
        n = len(targets)
        cls, counts = np.unique(targets[targets != None], return_counts=True)

        ## set weights to zero for those indices that are not in labeled_indices and not in unlabeled_indices
        # get difference between union of labeled and unlabeled and all indices
        union_lab_unlab = np.union1d(
            dataset.get_unlabeled_indices(), dataset.get_labeled_indices()
        )
        diff = np.setdiff1d(np.arange(len(dataset)), union_lab_unlab)
        print("len lab in weights: ", len(dataset.get_labeled_indices()))
        print("len unlab in weights: ", len(dataset.get_unlabeled_indices()))
        print("len total dataset in weights: ", len(dataset))
        print("len diff: ", len(diff))  # 92
        print(diff)

        count_dict = dict(zip(cls, counts))

        # # Set counts to 0 if unused
        # cats = dataset.categories
        # cats_to_drop = list(set(cats) - set(useful_cats))
        # drop_idxs = dataset._category_name_to_category_index(cats_to_drop)

        weight_dict = dict({None: 0})
        for k, v in count_dict.items():
            weight_dict[k] = float(n / v)
        # Set weights

        weights = [weight_dict[t] for t in targets]
        weights = np.array(weights)
        weights[diff] = 0.0
        weights = weights.tolist()

        return weights
        # return [weight_dict[t] for t in targets]

    def get_dataloaders(self, source_dataset, target_dataset):
        """Get dataloaders for source labeled for only target classes, target labeled,
        and target unlabeled

        Args:
            source_dataset (ImageClassificationDataset): source dataset
            target_dataset (ImageClassificationDataset): target dataset

        Returns:
            dataloaders for source labeled for only target classes, target labeled,
                and target unlabeled

        """
        class_names = target_dataset.categories

        a_lot = int(1e6)
        logging.info("Creating dataloaders")

        print(
            "len target lab inds in get_dataloaders: ",
            len(target_dataset.get_labeled_indices()),
        )

        # Source
        if self.config['weighted_sampling_source']:
            source_weights = self.get_weights_for_sampler(source_dataset)
            sampler = torch.utils.data.sampler.WeightedRandomSampler(
                source_weights, a_lot
            )
        else:
            sampler = None
        source_labeled_dataloader = torch.utils.data.DataLoader(
            source_dataset,
            sampler=sampler,
            batch_size=min(source_dataset.labeled_size, int(self.config["batch_size"])),
            num_workers=int(self.config["num_workers"] / 3),
            collate_fn=source_dataset.collate_batch,
            drop_last=False,
        )

        target_labeled_dataloader = None
        if target_dataset.labeled_size > 0:
            target_weights = self.get_weights_for_sampler(target_dataset)
            # Source
            labeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
                target_dataset.get_labeled_indices()
            )
            target_labeled_dataloader = torch.utils.data.DataLoader(
                target_dataset,
                sampler=labeled_sampler,
                batch_size=min(
                    target_dataset.labeled_size, int(self.config["batch_size"])
                ),
                num_workers=int(self.config["num_workers"] / 3),
                collate_fn=target_dataset.collate_batch,
                drop_last=False,
            )

        target_unlabeled_dataloader = None
        if target_dataset.unlabeled_size > 0:
            unlabeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
                target_dataset.get_unlabeled_indices()
            )
            target_unlabeled_dataloader = torch.utils.data.DataLoader(
                target_dataset,
                sampler=unlabeled_sampler,
                batch_size=min(
                    target_dataset.unlabeled_size, int(self.config["batch_size"])
                ),
                num_workers=int(self.config["num_workers"] / 3),
                drop_last=False,
            )

        return (
            source_labeled_dataloader,
            target_labeled_dataloader,
            target_unlabeled_dataloader,
            class_names,
        )

    def mapping_to_total(self, gt_labels, map_list):
        return torch.LongTensor([map_list[i] for i in gt_labels.squeeze().tolist()]).to(
            self.device
        )

    def evaluate_and_save_checkpoint(self, loader, model, path, device, epoch):
        with torch.no_grad():
            preds = []
            labels = []
            for  (_, data_s_aug, label_s, _) in tqdm.tqdm(loader):
                label_s = label_s.to(device, non_blocking=True)
                data_s_aug = data_s_aug.to(device, non_blocking=True)
                with torch.cuda.amp.autocast():
                    logits = model(data_s_aug)
                    preds += torch.argmax(logits, dim=1).cpu().numpy().tolist()
                    labels += label_s.cpu().numpy().tolist()
        os.makedirs(os.path.dirname(path), exist_ok=True)
        torch.save(model.state_dict(), path)
        acc = np.sum(np.array(preds) == np.array(labels)) / len(labels)
        print(f'Epoch {epoch}: Accuracy: {acc}')

    def domain_adapt_training(self, target_dataset, eval_dataset):
        """This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
        the dataloaders below and feel free to override the transformers in the datasets to use whatever
        pretraining you desire. Please do not modify the datasets though.

        Args:
            target_dataset (ImageClassificationDataset): pytorch dataset for the target dataset used for
                training
            eval_dataset (ImageClassificationDataset): eval dataset used for evaluation.  Included here in
                case you want to do transductive learning.
        """
        log.info("Few-shot Adaption with PACMAC Algorithm")
        config = self.config

        trans = transforms.Compose([])

        self.source_dataset.transform = trans
        target_dataset.transform = trans

        normalize_transform = transforms.Normalize(
            IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD
        )
        transform_randaug = transforms.Compose(
            [
                transforms.Resize((256, 256)),
                transforms.RandomCrop((224, 224)),
                transforms.RandomHorizontalFlip(),
                RandAugment(self.config["rand_augs"], self.config["rand_aug_severity"]),
                transforms.ToTensor(),
                normalize_transform,
            ]
        )

        transform_simple = transforms.Compose(
            [transforms.Resize((224, 224)), transforms.ToTensor(), normalize_transform]
        )
        # Get dataloaders for source dataset, target dataset, and target unlabelled dataset
        (
            source_loader,
            target_loader,
            target_loader_unl,
            class_list,
        ) = self.get_dataloaders(
            DatasetWrapper(self.source_dataset, transform_simple, transform_randaug),
            DatasetWrapper(target_dataset, transform_simple, transform_randaug),
        )


        model = self.source_network
        eff_batch_size = config["batch_size"] * config["accum_iter"]
        if config["lr"] == "None":
            config["lr"] = config["blr"] * eff_batch_size / 256

        param_groups = lrd.param_groups_lrd(
            model,
            config["weight_decay"],
            no_weight_decay_list=model.no_weight_decay(),
            layer_decay=config["layer_decay"],
        )
        optimizer = torch.optim.AdamW(param_groups, lr=config["lr"])

        loss_scaler = NativeScaler()

        if config["smoothing"] > 0.0:
            criterion = LabelSmoothingCrossEntropy(smoothing=config["smoothing"])
        else:
            criterion = torch.nn.CrossEntropyLoss()

        data_loader = zip(source_loader, target_loader)
        length = min(len(source_loader), len(target_loader))

        # only train on the source data once per stage
        if not self.trained_on_source:
            print("Training on source")
            for epoch in range(config["finetune_epochs"]):
                train_stats = self.finetune_one_epoch(
                    model,
                    criterion,
                    source_loader,
                    optimizer,
                    self.device,
                    epoch,
                    loss_scaler,
                    config=config,
                )
            self.trained_on_source = True
            state_dict = model.state_dict()
            torch.save(state_dict, os.path.join(self.work_dir, "source_pretrained.pth"))

        else:
            # load the weights from training on source
            print("Loading source weights")
            state_dict = torch.load(os.path.join(self.work_dir, "source_pretrained.pth"))
            mesg = model.load_state_dict(state_dict)
            print(mesg)

        print("Training on source+target")
        optimizer = torch.optim.AdamW(param_groups, lr=config["lr"])
        for epoch in range(config["adapt_epochs"]):
            train_stats = self.adapt_one_epoch(
                model,
                criterion,
                criterion,
                source_loader,
                target_loader_unl,
                target_loader,
                optimizer,
                self.device,
                epoch,
                loss_scaler,
                config=config,
            )
        self.source_network = model
        del source_loader, target_loader, target_loader_unl

        import gc

        gc.collect()  # Make sure all object have been deallocated if not used
        torch.cuda.empty_cache()

    def inference(self, eval_dataset):
        """
        Inference is during the evaluation stage.  For this example, the task_network is trained in the
        train and adapt stage's code and is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to JPL

        Args:
            eval_dataset (framework.datasets.ImageClassificationDataset): unlabeled evaluation framework.
                We need to predict the classes for each item in the dataset.

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        config = self.config

        log.info("Starting inference for PACMAC Algorithm")

        # override transforms
        crop_size = 224

        trans = transforms.Compose(
            [
                transforms.Resize(256),
                transforms.CenterCrop(crop_size),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
            ]
        )

        eval_dataset.transform = trans

        eval_dataloader = data.DataLoader(
            eval_dataset,
            batch_size=int(self.config["batch_size"]),
            drop_last=False,  ##### remove num_workers here
        )

        log.info(f"Starting inference on {len(eval_dataloader)} batches")

        softmax = nn.Softmax(dim=1).to(self.device)

        network = self.source_network.to(self.device)
        ##### use this loop for inference
        preds = []
        indices = []
        with torch.no_grad():
            for imgs, _, inds in eval_dataloader:
                imgs = imgs.to(self.device)
                preds_ = network(imgs)
                if config["probabilistic_predictions"]:
                    preds += softmax(preds_).cpu().numpy().tolist()
                else:
                    preds += torch.argmax(preds_, dim=1).cpu().numpy().tolist()

                indices += inds.numpy().tolist()
        #####

        del eval_dataloader
        import gc

        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()

        if config["probabilistic_predictions"]:
            predictions = pd.DataFrame(preds, columns=np.arange(len(preds[0])))
            predictions["id"] = indices
            return predictions
        else:
            return preds, indices

    def compute_prf1(self, true_mask, pred_mask):
        """
        Compute precision, recall, and F1 metrics for predicted mask against ground truth
        """
        conf_mat = confusion_matrix(true_mask, pred_mask, labels=[False, True])
        p = conf_mat[1, 1] / (conf_mat[0, 1] + conf_mat[1, 1] + 1e-8)
        r = conf_mat[1, 1] / (conf_mat[1, 0] + conf_mat[1, 1] + 1e-8)
        f1 = (2 * p * r) / (p + r + 1e-8)
        return conf_mat, p, r, f1

    # This always zips the two data loaders
    def finetune_one_epoch(
        self,
        model: torch.nn.Module,
        criterion: torch.nn.Module,
        source_data_loader: Iterable,
        optimizer: torch.optim.Optimizer,
        device: torch.device,
        epoch: int,
        loss_scaler,
        config=None,
    ):
        model.train(True)
        metric_logger = misc.MetricLogger(delimiter="  ")
        metric_logger.add_meter(
            "lr", misc.SmoothedValue(window_size=1, fmt="{value:.6f}")
        )
        header = "Epoch: [{}]".format(epoch)
        print_freq = 20

        accum_iter = config["accum_iter"]

        optimizer.zero_grad()
        length = len(source_data_loader)
        for data_iter_step, (_, data_s_aug, label_s, _) in enumerate(
            metric_logger.log_every(
                source_data_loader, print_freq, header, length=length
            )
        ):

            # we use a per iteration (instead of per epoch) lr scheduler
            if data_iter_step % accum_iter == 0:
                lr_sched.adjust_learning_rate(
                    optimizer, data_iter_step / length + epoch, config, config['finetune_epochs']
                )

            label_s = label_s.to(device, non_blocking=True)
            data_s_aug = data_s_aug.to(device, non_blocking=True)

            with torch.cuda.amp.autocast():
                logits_s = model(data_s_aug)
                loss = criterion(logits_s, label_s)

            loss_value = loss.item()
            if not math.isfinite(loss_value):
                print("Loss is {}, stopping training".format(loss_value))
                sys.exit(1)

            loss_avg = loss / accum_iter

            loss_scaler(
                loss_avg,
                optimizer,
                parameters=model.parameters(),
                update_grad=(data_iter_step + 1) % accum_iter == 0,
            )

            if (data_iter_step + 1) % accum_iter == 0:
                optimizer.zero_grad()

            torch.cuda.synchronize()

            metric_logger.update(total_loss=loss_value)

            min_lr = 10.0
            max_lr = 0.0
            for group in optimizer.param_groups:
                min_lr = min(min_lr, group["lr"])
                max_lr = max(max_lr, group["lr"])

            metric_logger.update(lr=max_lr)
            loss_value_reduce = misc.all_reduce_mean(loss_value)
        # gather the stats from all processes
        metric_logger.synchronize_between_processes()
        print("Averaged stats:", metric_logger)
        return {k: meter.global_avg for k, meter in metric_logger.meters.items()}

    # This always zips the two data loaders
    def adapt_one_epoch(
        self,
        model: torch.nn.Module,
        criterion: torch.nn.Module,
        target_criterion: torch.nn.Module,
        source_data_loader: Iterable,
        target_data_loader: Iterable,
        target_data_loader_labeled: Iterable,
        optimizer: torch.optim.Optimizer,
        device: torch.device,
        epoch: int,
        loss_scaler,
        config=None,
    ):
        model.train(True)
        metric_logger = misc.MetricLogger(delimiter="  ")
        metric_logger.add_meter(
            "lr", misc.SmoothedValue(window_size=1, fmt="{value:.6f}")
        )
        header = "Epoch: [{}]".format(epoch)
        print_freq = 20

        accum_iter = config["accum_iter"]

        optimizer.zero_grad()
        data_loader = zip(source_data_loader, target_data_loader)
        length = min(len(source_data_loader), len(target_data_loader))
        print(f'Length of zipped loader: {length}')

        if config['use_labeled_target']:
            data_iter_t_lab = iter(target_data_loader_labeled)
            len_target_lab = len(target_data_loader_labeled)
        for data_iter_step, (
            (data_s, data_s_aug, label_s, _),
            (data_t, data_t_aug, label_t, _),
        ) in enumerate(
            metric_logger.log_every(data_loader, print_freq, header, length=length)
        ):
            # we use a per iteration (instead of per epoch) lr scheduler
            if data_iter_step % accum_iter == 0:
                lr_sched.adjust_learning_rate(
                    optimizer, data_iter_step / length + epoch, config, config['adapt_epochs']
                )

            if config['use_labeled_target']:
                if data_iter_step % len_target_lab == 0:
                    data_iter_t_lab = iter(target_data_loader_labeled)

                (_, data_t_lab_aug, label_t_lab, _) = next(data_iter_t_lab)

            data_s = data_s.to(device, non_blocking=True)
            label_s = label_s.to(device, non_blocking=True)

            data_t = data_t.to(device, non_blocking=True)

            data_s_aug = data_s_aug.to(device, non_blocking=True)
            data_t_aug = data_t_aug.to(device, non_blocking=True)

            label_t = label_t.to(device, non_blocking=True)

            if config['use_labeled_target']:
                data_t_lab_aug = data_t_lab_aug.to(device, non_blocking=True)
                label_t_lab = label_t_lab.to(device, non_blocking=True)

            with torch.cuda.amp.autocast():
                data_s_t = torch.cat([data_s_aug, data_t], dim=0)
                logits_s_t = model(data_s_t)
                logits_s = logits_s_t[: len(data_s)]
                classifier_loss_s = criterion(logits_s, label_s)
                loss = torch.zeros_like(classifier_loss_s)
                loss += classifier_loss_s
                if config['use_labeled_target']:
                    logits_t_lab = model(data_t_lab_aug)
                    classifier_loss_t_lab = criterion(logits_t_lab, label_t_lab)
                    loss += classifier_loss_t_lab

            # select target samples to use for self-training
            with torch.cuda.amp.autocast():
                logits_t = logits_s_t[len(data_s) :]
                scores_t = F.softmax(logits_t, dim=1)
                top1_t, preds_t = scores_t.max(dim=1)

                if config["attention_seeding"]:
                    attention_stride = config["committee_size"]
                    attentions = model.get_last_selfattention(data_t_aug)
                    nh = attentions.shape[1]  # number of heads
                    attentions = attentions[:, :, 0, 1:].reshape(
                        len(data_t_aug), nh, -1
                    )
                    attention = attentions.mean(dim=1)  # Average across heads
                else:
                    attention_stride = 1
                    attention = None

                sel_mask = torch.zeros_like(preds_t.detach()).long()
                exclude_mask = torch.zeros_like(attention).long()
                logits_t_correct = torch.zeros_like(logits_t)

                for _ in range(config["committee_size"]):
                    mask_t, enc_feats_masked_t = model.forward_encoder(
                        data_t_aug,
                        config["mask_ratio"],
                        exclude_mask=exclude_mask,
                        attention=attention,
                        attention_stride=attention_stride,
                    )
                    logits_masked_t = model.head(enc_feats_masked_t)
                    preds_masked_t = logits_masked_t.argmax(
                        dim=1, keepdim=True
                    ).squeeze()

                    cur_sel_mask = (preds_masked_t == preds_t).detach()
                    sel_mask += cur_sel_mask.type(torch.uint8)
                    logits_t_correct[cur_sel_mask, :] = logits_masked_t[cur_sel_mask, :]
                    exclude_mask = exclude_mask + (1 - mask_t.long())
                    if config["attention_seeding"]:
                        attention_stride -= 1

                votes_req = config["committee_size"]  # unanimous voting
                sel_mask_cons = sel_mask >= votes_req

                threshold = config["conf_threshold"]
                sel_mask_conf = (top1_t > threshold).detach()  # confidence thresholding
                sel_mask = torch.logical_or(sel_mask_cons, sel_mask_conf)

                correct_mask = (preds_t.detach() == label_t).cpu()
                _, correct_precision, _, _ = self.compute_prf1(
                    correct_mask.cpu().numpy(), sel_mask.cpu().numpy()
                )

                if sel_mask.sum() > 0:
                    sel_ratio = sel_mask.sum().item() / len(sel_mask)
                    sst_loss = (
                        config["lambda_unsup"]
                        * sel_ratio
                        * target_criterion(
                            logits_t_correct[sel_mask], preds_t[sel_mask]
                        )
                    )
                    loss += sst_loss

            loss_value = loss.item()
            if not math.isfinite(loss_value):
                print("Loss is {}, stopping training".format(loss_value))
                sys.exit(1)

            loss_avg = loss / accum_iter

            loss_scaler(
                loss_avg,
                optimizer,
                parameters=model.parameters(),
                update_grad=(data_iter_step + 1) % accum_iter == 0,
            )

            if (data_iter_step + 1) % accum_iter == 0:
                optimizer.zero_grad()

            torch.cuda.synchronize()

            metric_logger.update(total_loss=loss_value)
            metric_logger.update(classifier_loss_s=classifier_loss_s.item())
            if config['use_labeled_target']:
                metric_logger.update(classifier_loss_t_lab=classifier_loss_t_lab.item())
            if sel_mask.sum() > 0:
                metric_logger.update(sst_loss=sst_loss.item())
                metric_logger.update(sel_ratio=sel_ratio)
                metric_logger.update(correct_precision=correct_precision)

            min_lr = 10.0
            max_lr = 0.0
            for group in optimizer.param_groups:
                min_lr = min(min_lr, group["lr"])
                max_lr = max(max_lr, group["lr"])

            metric_logger.update(lr=max_lr)

            loss_value_reduce = misc.all_reduce_mean(loss_value)
            classifier_loss_s_reduce = misc.all_reduce_mean(classifier_loss_s.item())
            if config['use_labeled_target']:
                classifier_loss_t_lab_reduce = misc.all_reduce_mean(
                    classifier_loss_t_lab.item()
                )
            if sel_mask.sum() > 0:
                sst_loss_reduce = misc.all_reduce_mean(sst_loss.item())
                sel_ratio_reduce = misc.all_reduce_mean(sel_ratio)
                correct_precision_reduce = misc.all_reduce_mean(correct_precision)
        # gather the stats from all processes
        metric_logger.synchronize_between_processes()
        print("Averaged stats:", metric_logger)
        return {k: meter.global_avg for k, meter in metric_logger.meters.items()}
