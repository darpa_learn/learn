import torch.utils.data as data
import torchvision
from typing import List

from learn.utils.dataset import ImageClassificationDataset


class DatasetWrapper(ImageClassificationDataset):
    def __init__(self, dataset: ImageClassificationDataset, transform1, transform2):
        self.transform1 = transform1
        self.transform2 = transform2
        for k in dataset.__dict__.keys():
            setattr(self, k, getattr(dataset, k))

    def __getitem__(self, index):
        img, lab, idx = super().__getitem__(index)
        return self.transform1(img), self.transform2(img), lab, idx
