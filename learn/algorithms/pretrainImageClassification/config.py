import scriptconfig as scfg


class Config(scfg.Config):
    """
    Default configuration for your algorithm

    Please add all of your default configuration parameters for your algorithm below
    with a description (help) on
        - what your parameter does
        - what changes when you use your parameter

    This is so that someone else can come and alter there parameters and know
    how this will affect the run of the algorithm

    """
    default = {
        'name': scfg.Value('cool_name',
                           help='name of the proposed method'),

        'batch_size': scfg.Value(128,
                                 help='batch size of network while running.  '
                                      'Increasing will do X and decrease will do Y'),

        'num_workers': scfg.Value(16,
                                 help='number of workers in the dataloader'),

        'seed': scfg.Value(8675309,
                                 help='Random seed used for reproducibility'),

        'net': scfg.Value('resnet34',
                                 help='Name of network used for evaluation'),

        'output_dir': scfg.Path('./output/pretraining',
                                 help='Name of network used for evaluation'),

        'learning_rate': scfg.Value(0.001,
                                 help='Initial Learning Rate'),

        'device': scfg.Value([0],
                             help='Device to use during training'),

        'max_epochs': scfg.Value(5000,
                                 help='maximum number of epochs to train the network for'),

        'patience': scfg.Value(30,
                               help='Early stopping patience (how many epochs of no imporvement to stop '
                               'after'),

        'mixed_precision': scfg.Value(False,
                               help='Train with Mixed precision?')





    }