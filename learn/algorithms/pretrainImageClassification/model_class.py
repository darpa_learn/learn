import torch
from torch.nn import functional as F
from torch.utils.data.sampler import SubsetRandomSampler
import copy

import pytorch_lightning as pl
import pytorch_lightning.metrics.functional as plmf
import logging


class Pretrainer(pl.LightningModule):
    def __init__(self,
                 hparams,
                 backbone,
                 train_dataset,
                 test_dataset):
        super(Pretrainer, self).__init__()
        # not the best model...
        self.hparams = hparams
        self.lr = self.hparams['learning_rate']
        self.batch_size = self.hparams['batch_size']
        backbone.set_as_classifier()
        self.backbone = backbone
        self.class_list = train_dataset.categories

        self.train_idx = None
        self.val_idx = None
        self.test_idx = None
        self.dataloaders = None
        self.train_dataset = train_dataset
        self.test_dataset = test_dataset
        self.set_dataloaders()

    def forward(self, x):
        return self.backbone.forward(x)

    def training_step(self, batch, batch_idx):
        # REQUIRED
        x, y, _ = batch
        y_hat = self.forward(x)
        loss = F.cross_entropy(y_hat, y)
        acc = plmf.accuracy(pred=y_hat, target=y)
        tensorboard_logs = {'train/loss': loss,
                            'train/acc': acc}

        return {'loss': loss,
                'train_acc': acc,
                'log': tensorboard_logs}

    def validation_step(self, batch, batch_idx):
        # OPTIONAL
        x, y, _ = batch
        y_hat = self.forward(x)
        return {'y_hat': y_hat, 'y': y}

    def validation_epoch_end(self, outputs):
        # OPTIONAL
        y_hat, y = self.collate_batches(outputs)
        metrics = self.calculate_metrics(y_hat.detach(), y.detach(), 'val')
        metrics['log'] = copy.deepcopy(metrics)
        return metrics

    def test_step(self, batch, batch_idx):
        # OPTIONAL
        x, y, _ = batch
        y_hat = self.forward(x)
        return {'y_hat': y_hat, 'y': y}

    def test_epoch_end(self, outputs):
        y_hat, y = self.collate_batches(outputs)
        metrics = self.calculate_metrics(y_hat.detach(), y.detach(), 'test')
        metrics['log'] = copy.deepcopy(metrics)
        return metrics

    def collate_batches(self, outputs):
        y_hat = torch.Tensor().type_as(outputs[0]['y_hat']).to(self.device)
        y = torch.Tensor().type_as(outputs[0]['y']).to(self.device)

        for x in outputs:
            y_hat = torch.cat((y_hat, x['y_hat']))
            y = torch.cat((y, x['y']))

        return y_hat.cpu(), y.cpu()

    def calculate_metrics(self, y_hat, y, prefix=""):
        num_classes = y_hat.shape[1]
        y_hat_idx = y_hat.argmax(dim=1)

        metrics = dict()
        to_graph = dict()

        metrics[f'{prefix}/loss'] = F.cross_entropy(y_hat, y)
        metrics[f'{prefix}/acc'] = acc = plmf.accuracy(pred=y_hat, target=y, num_classes=num_classes)
        metrics[f'{prefix}/f1_score'] = plmf.f1_score(pred=y_hat_idx, target=y,
                                                      num_classes=num_classes).item()
        # to_graph['confusion_matrix'] = plmf.confusion_matrix(pred=y_hat_idx, target=y)
        to_graph['precision_recall'] = plmf.precision_recall_curve(pred=y_hat, target=y)

        prt = to_graph['precision_recall'][:2]
        import seaborn as sns
        import matplotlib.pyplot as plt
        sns.set_style("darkgrid")
        plt.plot(prt[1], prt[0])
        plt.xlabel('Recall')
        plt.xlim([-.01, 1.01])
        plt.ylabel('Precision')
        plt.ylim([-.01, 1.01])
        tag = f'{prefix}_precision_recall'
        self.logger.experiment.add_figure('precision_recall', plt.gcf())

        return metrics

    def configure_optimizers(self):
        # REQUIRED
        # can return multiple optimizers and learning_rate schedulers
        optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer,
                                                               factor=0.5,
                                                               patience=10)
        lr_dict = {
            'scheduler': scheduler,  # The LR schduler
            'interval': 'epoch',  # The unit of the scheduler's step size
            'frequency': 1,  # The frequency of the scheduler
            'reduce_on_plateau': True,  # For ReduceLROnPlateau scheduler
            'monitor': 'val/acc'  # Metric to monitor
        }
        return [optimizer], [lr_dict]

    def train_dataloader(self):
        # REQUIRED
        return self.dataloaders['train']

    def val_dataloader(self):
        # OPTIONAL
        return self.dataloaders['val']

    def test_dataloader(self):
        # OPTIONAL
        return self.dataloaders['test']

    def on_fit_start(self):
        """ Update dataloaders to new batch size

        Returns:

        """
        self.set_dataloaders()

    def set_dataloaders(self):
        """ Sets dataloaders for source labeled for only target classes, target labeled,
        and target unlabeled

        """
        train_dataset = self.train_dataset
        test_dataset = self.test_dataset
        class_names = train_dataset.categories
        a_lot = int(1e6)
        if self.train_idx is None:
            print('Creating dataset train/val split')
            train_idx = train_dataset.get_labeled_indices()
            # y = np.array(train_dataset.targets)[x]
            # train_idx, val_idx, = train_test_split(x, stratify=y, test_size=0.1)
            test_idx = test_dataset.get_labeled_indices()
            logging.info(f'Train/Val/Test Sizes: {len(train_idx)} / {len(test_idx)} / {len(test_idx)}')
            self.train_idx = train_idx
            self.test_idx = test_idx

        logging.info('Creating dataloaders')

        train = torch.utils.data.DataLoader(
            train_dataset,
            sampler=SubsetRandomSampler(self.train_idx),
            batch_size=self.batch_size,
            num_workers=self.hparams['num_workers'],
            collate_fn=train_dataset.collate_batch,
            drop_last=True,
        )

        val = torch.utils.data.DataLoader(
            test_dataset,
            sampler=SubsetRandomSampler(self.test_idx),
            batch_size=self.batch_size,
            num_workers=self.hparams['num_workers'],
            collate_fn=train_dataset.collate_batch,
            drop_last=False,
        )

        test = torch.utils.data.DataLoader(
            test_dataset,
            sampler=SubsetRandomSampler(self.test_idx),
            batch_size=self.batch_size,
            num_workers=int(self.hparams['num_workers']),
            collate_fn=test_dataset.collate_batch,
            drop_last=False,
        )

        self.dataloaders = {'train': train, 'val': val, 'test': test}
