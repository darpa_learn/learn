import torch
import torch.utils.data as data
import torchvision.transforms as transforms
from pathlib import Path

from learn.algorithms.pretrainImageClassification.imageClassificationAdapter import ImageClassifierAdapter
from learn.algorithms.pretrainImageClassification.config import Config
from learn.algorithms.pretrainImageClassification.model_class import Pretrainer
from learn.utils.memory import garbage_collection_cuda

import pytorch_lightning as pl
from pytorch_lightning import Trainer, seed_everything
from learn.algorithms.pretrainImageClassification.early_stop import EarlyStopping
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.callbacks import ModelCheckpoint


class ImageClassifierAlgorithm(ImageClassifierAdapter):

    def __init__(self, toolset):
        # def __init__(self, problem, base_dataset, adapt_dataset, arguments):
        ImageClassifierAdapter.__init__(self, toolset)

        # Load the default configuration from config.py for your algorithm
        self.config = Config()
        self.update_parameters(toolset['protocol_config'])

        # This sends the configuration back to the toolset
        self.toolset['classification_config'] = self.config

    def update_parameters(self, protocol_config):
        """  Update parameters for algorithm based on protocol config

        Args:
            protocol_config (dict):  Dictionary of parameters.

        Returns:
            None
        """
        common_keyword = set(protocol_config.keys()) & set(self.config.keys())
        for ck in common_keyword:
            self.config[ck] = protocol_config[ck]

    def initialize(self, source_network, source_dataset, whitelist_datasets):
        """  Initialize your algorithm and save any parameters or objects you want to be persistent between
        budget checkpoints.  DomainNetworkSelection will choose a potential source datasets `source_dataset`
        and provide a trained network for that dataset `source_dataset.` Please do not modify the datasets
        beyond overriding the transforms (self.transform and self.transform_target are fine to change).

        Args:
            source_network (torch.nn.Module):  Pretrained network on source task
            source_dataset (framework.datasets.ImageClassificationDataset): Pytorch dataset for source
                domain/task
            whitelist_datasets (dict[str, framework.datasets.ImageClassificationDataset]): Dictionary of all
                possible external datasets which can be used on this task.  You may use this if you want to
                select a different dataset than the one selected as source.
        """
        train_dataset = source_dataset
        # pre-trained network on source task (the closest task)
        #  You are not required to use it but strongly suggested to use it
        #  These are in the style of torchvision models
        self.model = source_network
        seed_everything(self.config['seed'])

        G = source_network  # Note: source network only imagenet for now
        G.set_as_feature_extractor()
        self.config['net'] = G.model_name
        inc = G.num_feature_dims
        # TODO: automate device from config
        self.device = self.config['device']

        test_dataset = whitelist_datasets[f'{train_dataset.name}_test']
        self.set_augmentations(train_dataset, 'train')
        self.set_augmentations(test_dataset, 'test')

        hparams = self.config.to_dict()
        model = Pretrainer(hparams, G, train_dataset, test_dataset)

        early_stop = EarlyStopping(
            monitor='val/acc',
            patience=self.config['patience'],
            strict=False,
            verbose=True,
            mode='max'
        )

        logger = TensorBoardLogger(
            save_dir=self.config['output_dir'],
            version=7,
            name=f'{source_dataset.name}_{G.model_name}'
        )

        checkpoint_callback = ModelCheckpoint(
            filepath=logger.log_dir,
            save_top_k=5,
            verbose=True,
            monitor='val/acc',
            mode='max',
            prefix='v7'
        )

        # trainer = Trainer(gpus=self.device,
        #                   auto_lr_find=False,
        #                   auto_scale_batch_size=True,
        #                   use_amp=True,
        #                   early_stop_callback=early_stop,
        #                   logger=logger)
        #
        # trainer.fit(model)

        trainer = Trainer(gpus=self.device,
                          max_epochs=self.config['max_epochs'],
                          auto_lr_find=False,
                          auto_scale_batch_size=False,
                          use_amp=self.config['mixed_precision'],
                          early_stop_callback=early_stop,
                          logger=logger,
                          check_val_every_n_epoch=1,
                          checkpoint_callback=checkpoint_callback)

        trainer.fit(model)

        trainer.test(model)

        # import ipdb
        # ipdb.set_trace()
        #
        # # trainer = Trainer(gpus=self.device,
        # #                   distributed_backend='ddp',
        # #                   auto_lr_find=False,
        # #                   auto_scale_batch_size=False,
        # #                   use_amp=True,
        # #                   early_stop_callback=early_stop,
        # #                   logger=logger)
        #
        # trainer.fit(model)
        # trainer.test(model)
        exit(0)

    def set_augmentations(self, dataset, augmentation_style):
        """ Set the augmentations for a dataset

        Args:
            dataset (framework.datasets.ImageClassificationDataset):
            augmentation_style (str): either test or train

        Returns:

        """
        crop_size = 224
        if augmentation_style == 'test':
            dataset.transform = transforms.Compose([
                transforms.Resize(256),
                transforms.CenterCrop(crop_size),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        elif augmentation_style == 'train':
            dataset.transform = transforms.Compose([
                transforms.Resize(256),
                transforms.RandomHorizontalFlip(),
                transforms.RandomCrop(crop_size),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        else:
            raise NotImplementedError(f'"{augmentation_style}" augmentation style not implemented')

    def domain_adapt_training(self, target_dataset, eval_dataset):
        """ This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
        the dataloaders below and feel free to override the transformers in the datasets to use whatever
        pretraining you desire. Please do not modify the datasets though.

        Args:
            target_dataset (object): pytorch dataset for the target dataset used for training
            eval_dataset (object): eval dataset used for evaluation.  Included here in case you want to do
                transductive learning.
        """
        pass

    def inference(self, eval_dataset):
        """
        Inference is during the evaluation stage.  For this example, the task_network is trained in the
        train and adapt stage's code and is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to JPL

        Args:
            eval_dataset (framework.datasets.ImageClassificationDataset): unlabeled evaluation framework.
                We need to predict the classes for each item in the dataset.

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        config = self.config

        # override transforms
        crop_size = 227
        if self.config['net'] != 'alexnet':
            crop_size = 224

        trans = transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(crop_size),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])

        eval_dataset.transform = trans
        device = 'cpu'
        if config['cuda']:
            device = 'cuda'
        G = self.G.eval().to(device)
        F1 = self.F1.eval().to(device)

        eval_dataloader = data.DataLoader(eval_dataset,
            batch_size=int(self.config["batch_size"]),
            drop_last=False
        )
        preds = []
        indices = []
        with torch.no_grad():
            for imgs, _, inds in eval_dataloader:
                imgs = imgs.to(device)
                output = G(imgs)
                preds_ = F1(output)

                preds += torch.argmax(preds_, dim=1).cpu().numpy().tolist()
                indices += inds.numpy().tolist()

        self.G = G.cpu()
        self.F1 = F1.cpu()

        import gc
        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()
        # preds, indices = self.tools
        # et["eval_dataset"].dummy_data(
        #     'image_classification')
        return preds, indices

