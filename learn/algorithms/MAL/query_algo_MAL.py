from torchvision.transforms.transforms import RandomHorizontalFlip
from easydict import EasyDict
from learn.algorithms.MAL.query_adapter import QueryAdapter
from learn.algorithms.MAL import sampler, mal, models
import torch
import ubelt as ub
import logging
from torchvision import transforms
from learn.utils.dataset import ImageClassificationDataset
import gc
import copy

class QueryAlgo(QueryAdapter):
    def __init__(self, toolset):
        QueryAdapter.__init__(self, toolset)
        # can be both image classifier and active learner
        if 'MAL' in toolset['protocol_config']['active_learner']['name']:
            self.is_active_learner = True
            active_learner_config = toolset["protocol_config"]["active_learner"]["params"]
        else:
            self.is_active_learner = False
            active_learner_config = None

        if 'MAL' in toolset['protocol_config']['image_classifier']['name']:
            self.is_classifier = True
            classifier_config = toolset["protocol_config"]["image_classifier"]["params"]
        else:
            self.is_classifier = False
            classifier_config = None
        
        if classifier_config is not None and active_learner_config is not None:
            # update active learner's parameters with classifier
            logging.info("Using MAL as Classifier and Active Learner - Will default to Classifier Params")
            active_learner_config.update(classifier_config)
            self.config = active_learner_config
        elif classifier_config is not None:
            logging.info("Using MAL just as Image Classifier")
            self.config = classifier_config
        elif active_learner_config is not None:
            logging.info("Using MAL just as Active Learner")
            self.config = active_learner_config
        else:
            raise ValueError("Not using MAL as classifier or active learner")

        self.config = EasyDict(self.config)
        
        self.device = self.config.device
        if ub.iterable(self.device):
            self.device = self.device[0]
            self.config.device = self.device

    def initialize(self, source_network, source_dataset, target_dataset):
        """
        Initialize your algorithm and save any parameters or objects you want
        to be persistent between budget checkpoints.

        Args:
            source_dataset (ImageClassificationDataset): Pytorch dataset for source domain/task
            target_dataset: (ImageClassificationDataset): Pytorch dataset for target domain/task
        """
        logging.info("Initializing MAL algorithm")
        self.original_source_network = copy.deepcopy(source_network)
        self.mal = mal.MAL(device=self.device, args = self.config,
                is_active_learner=self.is_active_learner,
                is_classifier=self.is_classifier)
        self.original_source_network.set_as_feature_extractor()
        
        self.mal.set_model(source_network, source_dataset, target_dataset)
        if self.is_classifier:
            self.original_source_network.classifier =self.mal.C_IC
            self.mal.F.classifier = self.mal.C_IC

        self.source_dataset = source_dataset
        self.target_dataset = target_dataset
        self.set_augmentations(self.source_dataset)
        self.set_augmentations(self.target_dataset)
        torch.cuda.manual_seed(self.config["seed"])
        self.train_accuracies = []

    @staticmethod
    def set_augmentations(dataset):
        """Set the augmentations for a dataset

        Args:
            dataset (ImageClassificationDataset): dataset

        Returns:
            None

        """
        crop_size = 224
        dataset.transform = transforms.Compose(
            [
                transforms.Resize((256, 256)),
                transforms.RandomCrop((crop_size, crop_size)),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
            ]
        )

    def get_dataloaders(self, source_dataset, target_dataset, add_source_to_labeled=False):
        """
        Create Dataloaders for labeled data by getting the labeled indices and
        creating a random sub-sampler.  Then create the dataloader using this
        sampler

        Note: To change the transformer in the JPLDataset dataset, edit
             current_dataset.transform.  You can also edit the labels during
             loading by editing the  current_dataset.target_transform

        Note: Another approach could be to inherit the JPLDataset class and
             override the __get_item__ function

        Check out dataset.py for more information
        Notes: There could be NO unlabeled data and the algorithm has to be
            able to handle that case!

            The min batch size here is so that the batch size isn't
            larger than the labeled/unlabeled dataset which causes the
            dataloader to hang


        Args:
            source_dataset (framework.dataset): Pytorch dataset for source domain/task
            target_dataset: (framework.dataset): Pytorch dataset for target domain/task

        Returns:
            A tuple of dataloaders associated with labelled and unlabelled data
        """
        if add_source_to_labeled:
            labeled_dset = torch.utils.data.ConcatDataset([source_dataset, target_dataset])
            labeled_indices = list(range(len(source_dataset))) + [len(source_dataset) + i for i in target_dataset.get_labeled_indices()]
        else:
            labeled_dset = target_dataset
            labeled_indices = target_dataset.get_labeled_indices()
        
        labeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
            labeled_indices
        )
        labeled_dataloader = torch.utils.data.DataLoader(
            labeled_dset,
            sampler=labeled_sampler,
            batch_size= int(self.config["batch_size"]),#min(target_dataset.labeled_size, int(self.config["batch_size"])),
            num_workers=int(self.config["num_workers"]) // 2,
            collate_fn=target_dataset.collate_batch,
            drop_last=False,
            pin_memory=True
        )

        # ###################  Creating the Unlabeled DataLoader ###############
        #  Same as labeled dataaset but for unlabeled indices
        unlabeled_dataloader = None
        if target_dataset.unlabeled_size > 0:
            unlabeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
                target_dataset.get_unlabeled_indices()
            )
            unlabeled_dataloader = torch.utils.data.DataLoader(
                target_dataset,
                sampler=unlabeled_sampler,
                batch_size=min(
                    target_dataset.unlabeled_size, int(self.config["batch_size"])
                ),
                num_workers=int(self.config["num_workers"]) // 2,
                drop_last=False,
            )

        return {"labeled": labeled_dataloader, "unlabeled": unlabeled_dataloader}

    def select_and_label_data(self, budget, do_selection=True, add_source_to_labeled=False):
        """
        Select the data to be labelled by the active learning algorithm
        """
        
        self.mal.running_active_learner = True
        self.mal.get_optimizers()
        self.mal.start_epoch = 0

        logging.info("Running MAL as active learner")

        if self.target_dataset.unlabeled_size > 0:
            
            self.mal.move_to_gpu()
            # get dataloaders for the current dataset
            train_loaders = self.get_dataloaders(
                self.source_dataset, self.target_dataset, add_source_to_labeled
            )

            # run MAL to get model trained if didn't already do domain adaptation
            self.mal.train(train_loaders, 
                save_path = self.config['model_save_path'],
                stage = self.toolset['stage'],
                ckpt = self.toolset['ckpt'])
            if do_selection:
                # use the trained model to select indices
                self.config.budget = budget
                mal_sampler = sampler.MalSampler(self.config)
                sampled_indices = mal_sampler.sample(
                    al=self.mal, dataloader=train_loaders["unlabeled"], device=self.device
                )

                #  ########### Query for labels -- Kitware managed ################
                #  This function is handled by Kitware and takes the indices from the
                #  algorithm and queries for new labels. The new labels are added to
                #  the dataset and the labeled/unlabeled indices are updated

                self.target_dataset.get_more_labels(sampled_indices)

            del train_loaders
            self.mal.move_to_cpu()
        #        #  Note: you don't have to request the entire budget, but
        #        #      you shouldn't end the function until the budget is exhausted
        #        #      since the budget is lost after evaluation.

        #
        # ##################  End of ACTIVE LEARNING #####################

    def domain_adapt_training(self, target_dataset, eval_dataset):
        """ This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
        the dataloaders below and feel free to override the transformers in the datasets to use whatever
        pretraining you desire. Please do not modify the datasets though.

        Args:
            target_dataset (ImageClassificationDataset): pytorch dataset for the target dataset used for
                training
            eval_dataset (ImageClassificationDataset): eval dataset used for evaluation.  Included here in
                case you want to do transductive learning.
        """
        
        if not self.config['ic']['warm_start']:
            logging.info('Using Pre-training Parameters rather than using last budget level')
            self.mal.F = copy.deepcopy(self.original_source_network)
            self.mal.F.set_as_feature_extractor()
            self.mal.C_IC = self.mal.F.classifier
            self.toolset['source_network'] = self.mal.F
        else:
            logging.info('Using last budget level if available')
        
        # if its not an active learner, run the training anyways to train the model, just dont get the labels
        if not self.is_active_learner:
            logging.info("Running MAL as active learner just to train for IC")
            # calls select_and_label_data just to train, but wont do label selection
            self.select_and_label_data(budget=-1, do_selection=False, 
                    add_source_to_labeled = self.config['add_source_to_labeled'])

        logging.info("Running MAL as image classifier")

        self.mal.running_active_learner = False
        self.mal.get_optimizers()
        self.mal.start_epoch = 0
        
            
        self.mal.move_to_gpu()
        self.set_augmentations(target_dataset)
        train_loaders = self.get_dataloaders(
            self.source_dataset, target_dataset, add_source_to_labeled = False
        )
        self.mal.train(train_loaders,
                save_path = self.config['model_save_path'],
                stage = self.toolset['stage'],
                ckpt = self.toolset['ckpt'])
        del train_loaders
        self.mal.move_to_cpu()
        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()
        
    def inference(self, eval_dataset):
        """
        Inference is during the evaluation stage.  For this example, the task_network is trained in the
        train and adapt stage's code and is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to JPL

        Args:
            eval_dataset (framework.datasets.ImageClassificationDataset): unlabeled evaluation framework.
                We need to predict the classes for each item in the dataset.

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        config = self.config

        self.mal.move_to_gpu()
        # override transforms
        crop_size = 224

        trans = transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(crop_size),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])

        eval_dataset.transform = trans
        eval_dataloader = torch.utils.data.DataLoader(eval_dataset,
            batch_size=int(self.config["batch_size"]),
            drop_last=False
        )
        preds = []
        indices = []
        with torch.no_grad():
            for imgs, _, inds in eval_dataloader:
                imgs = imgs.to(self.device)
                output = self.mal.F(imgs)
                preds_ = self.mal.C_IC(output)
                preds += torch.argmax(preds_, dim=1).cpu().numpy().tolist()
                indices += inds.numpy().tolist()

        self.mal.move_to_cpu()
        del eval_dataloader

        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()

        return preds, indices
