import scriptconfig as scfg


class Config(scfg.Config):
    """
    Default configuration for your algorithm

    Please add all of your default configuration parameters for your algorithm below
    with a description (help) on
        - what your parameter does
        - what changes when you use your parameter

    This is so that someone else can come and alter there parameters and know
    how this will affect the run of the algorithm

    """
    default = {
        'name': scfg.Value('MAL',
            help='Name of the Active Learning Algorithm'),

        # Device parameters
        "cuda": scfg.Value(True,
            help='Flag to determine if GPU is used by the active learner'),
        "device": scfg.Value([0,1,2],
            help='CUDA Device used during training'),
        "num_workers": scfg.Value(16,
            help='Number of workers needed for the dataloader'),
        "resume": scfg.Value(16,
            help='Resume from a checkpoint'),

        # Entropy loss values
        "lamda": scfg.Value(0.1,
            help='Adversarial entropy loss regularizer'),
        "temperature": scfg.Value(0.05,
            help='Entropy temperature'),

        # Ablations
        "query_strategy": scfg.Value('mal',
            help='Query strategy choices: ["uncertainty", "diversity", "mal"]'),
        "entropy": scfg.Value(True,
            help="Use adversarial entropy on unlabeled data"),
        "dis": scfg.Value(True,
            help="Use discriminator"),
        "latent_dim": scfg.Value(128,
            help='Latent dimension for discriminator'),

        # Optimizer
        "lr_f": scfg.Value(1e-3,
            help='Learning rate for feature extractor of the backbone (F)'),
        "lr_c": scfg.Value(1e-3,
            help='Learning rate for cosine-based classifier (C)'),
        "lr_d": scfg.Value(1e-3,
            help='Learning rate for discriminator (D)'),
        "wd": scfg.Value(1e-5,
            help='Weight decay'),
        "lr_scheduler": scfg.Value("ReduceLROnPlateau",
            help='Learning rate scheduler'),
        "optimizer": scfg.Value("adam",
            help='Which optimizer to use'),
        "patience": scfg.Value(15,
            help='Number of epochs to plateau before lr reduction'),
        "factor": scfg.Value(0.5,
            help='Learning rate reduction factor'),

        # Training parameters
        "batch_size": scfg.Value(256,
            help='Batch size of the network'),
        "nepochs": scfg.Value(90,
            help='Number of epochs'),

        'seed': scfg.Value(1,
            help='random seed (default: 1)'),

    }
