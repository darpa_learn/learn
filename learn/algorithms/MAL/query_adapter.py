from learn.deprecated_tinker.basealgorithm import BaseAlgorithm
import abc


class QueryAdapter(BaseAlgorithm):
    def __init__(self, toolset):
        BaseAlgorithm.__init__(self, toolset)

    def execute(self, toolset, step_descriptor):
        """
        stage is a string that is passed in according to the protocol. It
        identifies which stage of the task is being requested (e.g. "adapt",
        "Evaluate" ) execution does not return anything, it is purely for the sake
        of altering the internal model. Available resources for training can be
        retrieved using the BaseAlgorithm functions.

        Args:
            toolset:
            step_descriptor:

        Returns:

        """

        self.toolset = toolset
        if step_descriptor == 'Initialize':
            return self.initialize(
                    source_network=self.toolset["source_network"],
                    source_dataset=self.toolset["source_dataset"],
                    target_dataset=self.toolset["target_dataset"]
                    )
        elif step_descriptor == 'SelectAndLabelData':
            return self.select_and_label_data(self.toolset['budget'])
        # also works for image classification
        elif step_descriptor == 'DomainAdaptTraining':
            return self.domain_adapt_training(
                target_dataset=self.toolset["target_dataset"],
                eval_dataset=self.toolset["eval_dataset"]
            )
        elif step_descriptor == 'EvaluateOnTestDataSet':
            return self.inference(eval_dataset=self.toolset["eval_dataset"])
        else:
            raise NotImplementedError(f'Step {step_descriptor} not implemented')

    @abc.abstractmethod
    def initialize(self, source_dataset, target_dataset):
        raise NotImplementedError

    @abc.abstractmethod
    def select_and_label_data(self, budget):
        raise NotImplementedError

    @abc.abstractmethod
    def domain_adapt_training(self, target_dataset, eval_dataset):
        raise NotImplementedError

    @abc.abstractmethod
    def inference(self, eval_dataset):
        raise NotImplementedError
