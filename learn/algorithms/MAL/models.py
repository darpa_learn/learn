import math
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import logging

class FeatureExtractor(nn.Module):
    def __init__(self, args):
        super(FeatureExtractor, self).__init__()

        logging.info("Initializing feature encoder with {}".format(args.al.train_from))
        pretrained = True if args.al.train_from == "imagenet" else False

        encoder = torchvision.models.__dict__[args.backbone](pretrained=pretrained)
        modules = list(encoder.children())[:-1]  # delete the last fc layer.
        self.backbone = args.backbone
        self.AL_approach = args.AL_approach
        if self.backbone == "resnet18" and self.AL_approach == "bald":
            self.avg_pool = modules[-1]
            self.encoder = nn.Sequential(*modules[:-1])
            self.dropout = nn.Dropout2d(p=0.2)
        else:
            self.encoder = nn.Sequential(*modules)
        if args.backbone.startswith("resnet"):
            self.encoder_out_features = encoder.fc.in_features
        elif args.backbone == "vgg16_bn":
            self.encoder_out_features = 512 * 7 * 7
        else:
            raise NotImplementedError

    def forward(self, x):
        x = self.encoder(x)
        if self.backbone == "resnet18" and self.AL_approach == "bald":
            x = self.dropout(x)
            x = self.avg_pool(x)
        x = x.view(x.size(0), -1)
        return x


#######################################################################################
#######################################################################################
#######################################################################################
#######################################################################################


class Discriminator(nn.Module):
    def __init__(self, encoder_out_features, args):
        super(Discriminator, self).__init__()
        '''if args.backbone == "resnet18":
            z1 = 512
            z2 = 512
        elif args.backbone == "vgg16_bn":
            z1 = 512 * 7 * 7
            z2 = 512
        elif args.backbone == "resnet50":
            z1 = 256 * 8 * 8
            z2 = 64
        else:
            raise NotImplementedError'''

        self.D = nn.Sequential(
            nn.Linear(encoder_out_features, 512),
            nn.ReLU(True),
            nn.Linear(512, 512),
            nn.ReLU(True),
            nn.Linear(512, 1),
        )
        self.weight_init()

    def weight_init(self):
        for block in self._modules:
            for m in self._modules[block]:
                kaiming_init(m)

    def forward(self, z):
        return self.D(z)


def kaiming_init(m):
    if isinstance(m, (nn.Linear, nn.Conv2d)):
        nn.init.kaiming_normal_(m.weight)
        if m.bias is not None:
            m.bias.data.fill_(0)
    elif isinstance(m, (nn.BatchNorm1d, nn.BatchNorm2d)):
        m.weight.data.fill_(1)
        if m.bias is not None:
            m.bias.data.fill_(0)


#######################################################################################
#######################################################################################
#######################################################################################
#######################################################################################


class ALClassifier(nn.Module):
    def __init__(self, args, encoder_out_features, num_classes, h_dim=512):
        super(ALClassifier, self).__init__()
        self.encoder_out_features = encoder_out_features
        #if args.backbone == "resnet18":
        #    h_dim = self.encoder_out_features // 2
        #elif args.backbone == "vgg16_bn":
        #    h_dim = self.encoder_out_features // 49
        #else:
        #    raise NotImplementedError

        self.fc1 = nn.Linear(self.encoder_out_features, h_dim)
        self.fc2 = nn.Linear(h_dim, num_classes, bias=False)
        self.temperature = args.appr.temperature

    def forward(self, x, reverse=False, eta=0.1):
        x = self.fc1(x)
        if reverse:
            x = grad_reverse(x, eta)
        x = F.normalize(x)
        x_out = self.fc2(x) / self.temperature
        return x_out


def grad_reverse(x, lambd=1.0):
    return GradReverse.apply(x, lambd)


class GradReverse(torch.autograd.Function):
    @staticmethod
    def forward(ctx, x, lambd):
        ctx.lambd = lambd
        return x

    @staticmethod
    def backward(ctx, grad_output):
        return grad_output * -ctx.lambd, None


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find("Conv") != -1:
        m.weight.data.normal_(0.0, 0.1)
    elif classname.find("Linear") != -1:
        nn.init.xavier_normal_(m.weight)
        nn.init.zeros_(m.bias)
    elif classname.find("BatchNorm") != -1:
        m.weight.data.normal_(1.0, 0.1)
        m.bias.data.fill_(0)


class Flatten(nn.Module):
    def forward(self, input):
        return input.view(input.size(0), -1)


class Predictor(nn.Module):
    def __init__(self, num_class, inc, temp=0.05):
        super(Predictor, self).__init__()
        self.fc = nn.Linear(inc, num_class)#, bias=False)
        self.num_class = num_class
        self.temp = temp

    def forward(self, x, reverse=False, eta=0.1):
        if reverse:
            x = grad_reverse(x, eta)
        x = F.normalize(x)
        x_out = self.fc(x) / self.temp
        return x_out

#######################################################################################
#######################################################################################
#######################################################################################
#######################################################################################


class TaskNet(nn.Module):
    def __init__(self, args, encoder=None):
        super(TaskNet, self).__init__()
        self.backbone = args.backbone
        self.AL_approach = args.AL_approach
        if not encoder:
            logging.info("Initializing TaskNet with {}".format(args.task.train_from))
            pretrained = True if args.task.train_from == "imagenet" else False
            logging.info(
                "Not using a pretrained encoder - loading backbone from torchvision pretrained imagenet=",
                pretrained,
            )
            encoder = torchvision.models.__dict__[args.backbone](pretrained=pretrained)
            modules = list(encoder.children())[:-1]  # delete the last fc layer.
            if self.backbone == "resnet18" and self.AL_approach == "bald":
                self.avg_pool = modules[-1]
                self.encoder = nn.Sequential(*modules[:-1])
                self.dropout = nn.Dropout2d(p=0.2)
            else:
                self.encoder = nn.Sequential(*modules)

            if args.backbone.startswith("resnet"):
                self.classifier = torch.nn.Linear(
                    encoder.fc.in_features, args.num_classes
                )

            elif args.backbone.startswith("vgg"):
                self.classifier = torch.nn.Linear(512 * 7 * 7, args.num_classes)

        else:
            logging.info("Initializing task learner with encoder - you must be doing MAL")
            self.encoder = encoder
            if args.backbone.startswith("resnet"):
                tmp = torchvision.models.__dict__[args.backbone](pretrained=False)
                self.classifier = nn.Linear(tmp.fc.in_features, args.num_classes)

            elif args.backbone.startswith("vgg"):
                self.classifier = torch.nn.Linear(512 * 7 * 7, args.num_classes)

    def forward(self, x, embedding=False):
        x = self.encoder(x)
        if self.backbone == "resnet18" and self.AL_approach == "bald":
            x = self.dropout(x)
            x = self.avg_pool(x)
        x = x.view(x.size(0), -1)
        feat = x
        x = self.classifier(x)
        if embedding:
            return x, feat
        return x

    def get_embedding_dim(self):
        return 512
