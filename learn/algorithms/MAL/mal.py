import numpy as np
import os
import shutil
import time

import torch
import torch.nn as nn

from itertools import cycle
from learn.algorithms.MAL.utils import (
    AverageMeter,
    _CustomDataParallel,
    binary_accuracy,
    EntropyLoss,
    accuracy,
)
from learn.algorithms.MAL.models import ALClassifier, FeatureExtractor, Discriminator, Predictor
import logging
from learn.utils.wandb_utils import WANDB_INFO


class MAL:
    def __init__(self, device, args,
            is_active_learner=True, is_classifier=True):
        self.args = args
        self.device = device

        self.is_active_learner = is_active_learner
        self.is_classifier = is_classifier

        if not (is_active_learner or is_classifier):
            raise ValueError("MAL needs to be either active learner or classifier or both")

        # can only be running AL or classifier at any given point in time
        if is_active_learner:
           self.running_active_learner = True 
        else: 
           self.running_active_learner = False

        # Losses
        self.bce_loss = nn.BCEWithLogitsLoss().to(self.device)
        self.mse_loss = nn.MSELoss().to(self.device)
        self.ce_loss = nn.CrossEntropyLoss().to(self.device)
        self.advent_loss = EntropyLoss(args).to(self.device)

    def get_discriminator(self, num_encoder_features):
        D = Discriminator(num_encoder_features, self.args)
        D = _CustomDataParallel(D).to(self.device)
        self.print_size(D, "Discriminator")
        return D

    def get_num_classes(self, source_dataset, target_dataset):
        self.target_categories = target_dataset.categories.tolist()
        self.target_categories = list(map(lambda l: str(l).lower().replace('_', ' ').replace('-', ' '),
                 self.target_categories))
        if self.args.add_source_to_labeled:
            self.source_categories = source_dataset.categories.tolist()
            self.source_categories = list(map(lambda l: str(l).lower().replace('_', ' ').replace('-', ' '), self.source_categories))

            source_cats = set(self.source_categories)
            target_cats = set(self.target_categories)
            common_categories = source_cats & target_cats
            target_only_cats = target_cats - source_cats
            self.total_categories = list(source_cats | target_cats)
            # get mapping list to consider class overlap.
            self.map_source2total = [self.total_categories.index(i) for i in self.source_categories]
            self.map_target2total = [self.total_categories.index(i) for i in self.target_categories]

            return len(self.total_categories)
        else:
            return len(self.target_categories)

    def mapping_to_total(self, gt_labels, map_list):
        return torch.LongTensor([map_list[i] for i in gt_labels.squeeze().tolist()]).to(self.device)

    def move_to_cpu(self):
        if True:#self.is_active_learner:
            self.C_AL = self.C_AL.cpu()
        self.F = self.F.cpu()
        if self.is_classifier:
            self.C_IC = self.C_IC.cpu()
        if self.args.dis:
            self.D = self.D.cpu()

    def move_to_gpu(self):
        if self.running_active_learner:
            self.C_AL = self.C_AL.to(self.device)
            if self.args.dis:
                self.D = self.D.to(self.device)
        else:
            self.C_IC = self.C_IC.to(self.device)
        self.F = self.F.to(self.device)
    
    def set_model(self, feature_extractor, source_dataset, target_dataset):
        if feature_extractor is None:
            self.F = FeatureExtractor(self.args)
        else:
            self.F = feature_extractor

        num_classes_al  = self.get_num_classes(source_dataset, target_dataset)
        num_classes_ic = len(target_dataset.categories)
        
        self.C_AL = ALClassifier(self.args, self.F.num_feature_dims, num_classes=num_classes_al)
        self.C_AL = _CustomDataParallel(self.C_AL).to(self.device)

        if self.is_classifier:
            self.C_IC = Predictor(num_class = num_classes_ic, inc =  self.F.num_feature_dims)
            self.C_IC = _CustomDataParallel(self.C_IC).to(self.device)

        self.F = _CustomDataParallel(self.F).to(self.device)

        #self.print_size(self.F, "Active Learner (F)")
        #self.print_size(self.C_AL, "Classifier (C)")

        if self.args.dis:
            self.D = self.get_discriminator(self.F.num_feature_dims)

        # these are now called at start of each run
        #self.get_optimizers()
        #self.start_epoch = 0

        # if self.args.resume:
        #     self.resume(run_id)

    def get_optimizers(self):
        lr_f = self.args.al.lr_f
        lr_d = self.args.al.lr_d
        lr_c = self.args.al.lr_c

        if self.args.optimizer == "adam":
            self.optimizer_F = torch.optim.Adam(
                self.F.parameters(), lr=lr_f, weight_decay=self.args.al.wd
            )
            
            if self.running_active_learner:
                self.optimizer_C_AL = torch.optim.Adam(
                    self.C_AL.parameters(), lr=lr_c, weight_decay=self.args.al.wd
                )

                if self.args.dis:
                    self.optimizer_D = torch.optim.Adam(
                        self.D.parameters(),
                        lr=lr_d,
                        weight_decay=self.args.al.wd,
                    )
            else:
                # TODO: revisit parameters here
                self.optimizer_C_IC = torch.optim.SGD(
                    self.C_IC.parameters(), lr=1.0, momentum=0.9, weight_decay=0.0005,
                    nesterov=True
                )

        else:
            raise ValueError(
                "Optimizer {} is not supported. Use Adam".format(self.args.optimizer)
            )

        logging.info("Adam optimizers are set for F, C, and D...")
        self.scheduler_F = self.set_scheduler(self.optimizer_F)
        
        if self.running_active_learner:
            self.scheduler_C_AL = self.set_scheduler(self.optimizer_C_AL)
            if self.args.dis:
                self.scheduler_D = self.set_scheduler(self.optimizer_D)
        else:
            self.scheduler_C_IC = self.set_scheduler(self.optimizer_C_IC)

    def set_scheduler(self, optimizer):
        return torch.optim.lr_scheduler.ReduceLROnPlateau(
            optimizer,
            factor=self.args.al.lr_factor,
            verbose=True,
            patience=self.args.al.patience,
            min_lr=self.args.al.min_lr,
        )

    def train(self, dataloaders, save_path=None, stage=None, ckpt=None):
        logging.info("Beginning of MAL training")
        best_loss, best_acc, at_epoch = np.inf, 0.0, 0

        for epoch in range(self.start_epoch, self.args.nepochs):

            # Train
            tstart = time.time()
            
            if self.running_active_learner:
                Tloss = self.train_epoch_al(dataloaders, epoch)
            else:
                Tloss = self.train_epoch_ic(dataloaders, epoch)

            logging.info(
                "|| [EpTime= {:.1f} min]".format((time.time() - tstart) / (60)),
            )

            # loss_d, loss_t, acc_t = self.test()

            # remember best acc1 and save checkpoint
            # is_best = acc_t > best_acc
            # best_acc = max(acc_t, best_acc)
            # logging.info(
            #     "|| Test:  Task Acc: {:.2f}% | Best acc:{:.2f}%".format(acc_t, best_acc)
            # )

            # use training loss as an alternative to step the scheduler
            self.scheduler_F.step(Tloss.avg)

            if self.running_active_learner:
                self.scheduler_C_AL.step(Tloss.avg)
                if self.args.dis:
                    self.scheduler_D.step(Tloss.avg)
            else:
                self.scheduler_C_IC.step(Tloss.avg)

            # if is_best:
            #     self.save_model(
            #         is_best=is_best,
            #         split_id=split_id,
            #         epoch=epoch,
            #         best_acc=best_acc,
            #         run_id=run_id,
            #     )
        if save_path is not None:
            if not os.path.exists(save_path):
                os.mkdir(save_path)
            save_path = os.path.join(save_path, stage)
            if not os.path.exists(save_path):
                os.mkdir(save_path)
            if self.running_active_learner:
                tag = 'AL'
            else:
                tag = 'IC'
            torch.save(self.F.state_dict(), os.path.join(
                save_path, f"F_{tag}_{ckpt}_ep{epoch}.pth"
            ))
            
            if self.running_active_learner:
                torch.save(self.C_AL.state_dict(), os.path.join(
                    save_path, f"C_AL_{ckpt}_ep{epoch}.pth"
                ))
            else:
                torch.save(self.C_IC.state_dict(), os.path.join(
                    save_path, f"C_IC_{ckpt}_ep{epoch}.pth"
                ))
        return best_acc

    def train_epoch_ic(self, target_loaders, epoch):
        """Training for just image classification"""
        Tloss = AverageMeter("|Task loss=", ":.2f")
        top1_t = AverageMeter("|Task acc=", ":.2f")

        self.F.train()
        self.C_IC.train()

        for batch_idx, target_batch_labeled in enumerate(target_loaders["labeled"]):
            labeled_imgs, labels, _ = target_batch_labeled

            labeled_imgs = labeled_imgs.to(self.device)
            labels = labels.to(self.device, dtype=torch.long)
            # ================================================================== #
            #                Entropy minimization on Labeled Data                #
            # ================================================================== #
            
            self.optimizer_C_IC.zero_grad()
            self.optimizer_F.zero_grad()
            labeled_output = self.F(labeled_imgs)
            lab_ent_out = self.C_IC(labeled_output)
            ce_loss = self.ce_loss(lab_ent_out, labels)
            ce_loss.backward()
            
            self.optimizer_C_IC.step()    
            self.optimizer_F.step()

            # measure accuracy and record loss
            acc_t = accuracy(lab_ent_out, labels)
            Tloss.update(ce_loss.item(), labels.size(0))
            top1_t.update(acc_t.item(), labels.size(0))
        
        logging.info(
            "Epoch {}/{}: TLoss: {:.2f} | TrainAcc: {:.2f}% | ".format(
                epoch,
                self.args.nepochs,
                Tloss.avg,
                top1_t.avg
            )
        )
        if WANDB_INFO.run_wandb:
            wb_tag = "IC"
            WANDB_INFO.log_metrics({
                f"MAL_train_loss_{wb_tag}" : Tloss.avg,
                f"MAL_top1t_{wb_tag}" : top1_t.avg
            })
        return Tloss

    def train_epoch_al(self, target_loaders, epoch):

        Tloss = AverageMeter("|Task loss=", ":.2f")
        top1_t = AverageMeter("|Task acc=", ":.2f")
        Dloss = AverageMeter("|DLoss=", ":.2f")
        Eloss = AverageMeter("|EntLoss=", ":.2f")
        top1_d_unlabeled = AverageMeter("|D-UnL=", ":.2f")
        top1_d_labeled = AverageMeter("|D-Lab=", ":.2f")

        self.F.train()

        self.C_AL.train()
        if self.args.dis:
            self.D.train()
        
        # will error out at checkpont 7 when there is no unlabeled data, but it shouldn't get to that point
        for batch_idx, (target_batch_unlabeled, target_batch_labeled,) in enumerate(
            zip(target_loaders["unlabeled"], cycle(target_loaders["labeled"]))
        ):
            labeled_imgs, labels, _ = target_batch_labeled
            unlabeled_imgs, _, _ = target_batch_unlabeled
            unlabeled_imgs = unlabeled_imgs.to(self.device)

            labeled_imgs = labeled_imgs.to(self.device)
            labels = labels.to(self.device, dtype=torch.long)
            # ================================================================== #
            #                Entropy minimization on Labeled Data                #
            # ================================================================== #
            
            self.optimizer_C_AL.zero_grad()

            self.optimizer_F.zero_grad()
            labeled_output = self.F(labeled_imgs)
            lab_ent_out = self.C_AL(labeled_output)
            ce_loss = self.ce_loss(lab_ent_out, labels)
            ce_loss.backward(retain_graph=True)
            self.optimizer_C_AL.step()
                
            self.optimizer_F.step()

            # measure accuracy and record loss
            acc_t = accuracy(lab_ent_out, labels)
            Tloss.update(ce_loss.item(), labels.size(0))
            top1_t.update(acc_t.item(), labels.size(0))

            # ================================================================== #
            #               Entropy maximization on unLabeled Data               #
            # ================================================================== #
            if self.args.entropy:
                self.optimizer_C_AL.zero_grad()
                self.optimizer_F.zero_grad()
                unlabeled_output = self.F(unlabeled_imgs)
                unlab_ent_out = self.C_AL(x=unlabeled_output, reverse=True, eta=1.0)

                entropy_loss = self.advent_loss(unlab_ent_out)

                entropy_loss.backward(retain_graph=True)
                self.optimizer_C_AL.step()
                self.optimizer_F.step()

                Eloss.update(entropy_loss.item(), unlabeled_output.size(0))

            # ================================================================== #
            #                       Pool Discriminator                           #
            # ================================================================== #
            if self.args.dis:
                if True:#unlabeled_output is None:
                    with torch.no_grad():
                        unlabeled_output = self.F(unlabeled_imgs)
                        labeled_output = self.F(labeled_imgs)

                dis_labeled_out = self.D(labeled_output).view(-1)
                dis_unlabeled_out = self.D(unlabeled_output).view(-1)

                unlab_D = torch.zeros(unlabeled_imgs.size(0)).to(self.device)
                lab_D = torch.ones(labeled_imgs.size(0)).to(self.device)

                dis_loss = self.bce_loss(dis_labeled_out, lab_D) + self.bce_loss(
                    dis_unlabeled_out, unlab_D
                )

                self.optimizer_D.zero_grad()
                self.D.zero_grad()
                dis_loss.backward(retain_graph=True)
                self.optimizer_D.step()

                # measure discriminator's performance
                Dloss.update(
                    dis_loss.item(),
                    labeled_imgs.size(0) + unlabeled_imgs.size(0),
                )
                acc_d_labeled = binary_accuracy(dis_labeled_out, lab_D)
                acc_d_unlabeled = binary_accuracy(dis_unlabeled_out, unlab_D)
                top1_d_labeled.update(acc_d_labeled, labeled_imgs.size(0))
                top1_d_unlabeled.update(acc_d_unlabeled, unlabeled_imgs.size(0))

        logging.info(
            "Epoch {}/{}: TLoss: {:.2f} | LossD {:.2f} | DAcc-L {:.2f} | DAcc-U {:.2f} | LossE {:.2f} | "
            "TrainAcc: {:.2f}% | ".format(
                epoch,
                self.args.nepochs,
                Tloss.avg,
                Dloss.avg,
                top1_d_labeled.avg,
                top1_d_unlabeled.avg,
                Eloss.avg,
                top1_t.avg,
            )
        )
        if WANDB_INFO.run_wandb:
            wb_tag = "AL"
            metric_dict = {
                f"MAL_discrim_loss_{wb_tag}" : Dloss.avg,
                f"MAL_discrim_acc_l_{wb_tag}" : top1_d_labeled.avg,
                f"MAL_discrim_acc_unl_{wb_tag}" : top1_d_unlabeled.avg,
                f"MAL_eloss_{wb_tag}" : Eloss.avg,
                f"MAL_top1t_{wb_tag}" : top1_t.avg,
                f"MAL_train_loss_{wb_tag}" : Tloss.avg,
            }
            
            WANDB_INFO.log_metrics(metric_dict)
        return Tloss

    def test(self):
        Tloss = AverageMeter("Task loss=", ":.2f")
        top1_t = AverageMeter("Task acc=", ":.2f")
        Dloss = AverageMeter("D-Loss=", ":.2f")

        self.F.eval()
        self.C_AL.eval()
        if self.args.dis:
            self.D.eval()

        with torch.no_grad():

            for batch_idx, (imgs, labels, _) in enumerate(self.test_dataloader):
                imgs = imgs.to(self.device)
                labels = labels.to(self.device, dtype=torch.long)

                output = self.F(imgs)
                ent_out = self.C_AL(output)

                if self.args.dis:
                    # Discriminator (test set can be assumed as unlabeled for performance calculations)
                    dis_unlabeled_out = self.D(output).view(-1)
                    lab_real_D = torch.zeros(imgs.size(0)).to(self.device)
                    dis_loss = self.bce_loss(dis_unlabeled_out, lab_real_D)
                    Dloss.update(dis_loss.item(), imgs.size(0))

                ce_loss = self.ce_loss(ent_out, labels)
                acc = accuracy(ent_out, labels)
                Tloss.update(ce_loss.item(), imgs.size(0))
                top1_t.update(acc.item(), imgs.size(0))

        return Dloss.avg, Tloss.avg, top1_t.avg

    def print_size(self, model, name):
        def pretty_print(num):
            magnitude = 0
            while abs(num) >= 1000:
                magnitude += 1
                num /= 1000.0
            return "%.2f%s" % (num, ["", "K", "M", "G", "T", "P"][magnitude])

        count = sum(p.numel() for p in model.parameters() if p.requires_grad)
        logging.info("<<<< {} Model >>>>".format(name))
        logging.info("Num parameters in %s model  = %s " % (name, pretty_print(count)))

    def save_model(self, is_best, split_id, epoch, best_acc, run_id):

        F_path = os.path.join(
            self.args.path.checkpoint,
            "F_split_id_{}_run_id_{}.pth.tar".format(split_id, run_id),
        )
        F_path_best = os.path.join(
            self.args.path.checkpoint,
            "Best_F_split_id_{}_run_id_{}.pth.tar".format(split_id, run_id),
        )
        state_F = {
            "epoch": epoch + 1,
            "state_dict": self.F.state_dict(),
            "best_acc": best_acc,
            "optimizer": self.optimizer_F.state_dict(),
        }
        torch.save(state_F, F_path)
        if is_best:
            shutil.copyfile(F_path, F_path_best)

        C_path = os.path.join(
            self.args.path.checkpoint,
            "C_split_id_{}_run_id_{}.pth.tar".format(split_id, run_id),
        )
        C_path_best = os.path.join(
            self.args.path.checkpoint,
            "Best_C_split_id_{}_run_id_{}.pth.tar".format(split_id, run_id),
        )
        state_C = {
            "state_dict": self.C_AL.state_dict(),
            "best_acc": best_acc,
            "optimizer": self.optimizer_C_AL.state_dict(),
        }
        torch.save(state_C, C_path)
        if is_best:
            shutil.copyfile(C_path, C_path_best)

        if self.args.dis:
            state_D = {
                "state_dict": self.D.state_dict(),
                "best_acc": best_acc,
                "optimizer": self.optimizer_D.state_dict(),
            }
            D_path = os.path.join(
                self.args.path.checkpoint,
                "D_split_id_{}_run_id_{}.pth.tar".format(split_id, run_id),
            )
            D_path_best = os.path.join(
                self.args.path.checkpoint,
                "Best_D_split_id_{}_run_id_{}.pth.tar".format(split_id, run_id),
            )
            torch.save(state_D, D_path)
            if is_best:
                shutil.copyfile(D_path, D_path_best)

    def load_checkpoint(self, model, optimizer, path, get_epoch=False):
        checkpoint = torch.load(path)
        model.load_state_dict(checkpoint["state_dict"])
        model = model.to(self.device)

        optimizer.load_state_dict(checkpoint["optimizer"])

        best_acc = checkpoint["best_acc"]

        logging.info(
            "=> loaded checkpoint '{}' with best model acc of {}".format(path, best_acc)
        )
        if get_epoch:
            start_epoch = checkpoint["epoch"]
            return model, optimizer, start_epoch

        return model, optimizer

    def resume(self, run_id, split_id=None):
        if split_id is None:
            split_id = self.args.start_split

        F_path = os.path.join(
            self.args.path.checkpoint,
            "F_split_id_{}_run_id_{}.pth.tar".format(split_id, run_id),
        )
        self.F, self.optimizer_F, self.start_epoch = self.load_checkpoint(
            model=self.F, optimizer=self.optimizer_F, path=F_path, get_epoch=True
        )

        C_path = os.path.join(
            self.args.path.checkpoint,
            "C_split_id_{}_run_id_{}.pth.tar".format(split_id, run_id),
        )
        self.C_AL, self.optimizer_C_AL = self.load_checkpoint(
            model=self.C_AL,
            optimizer=self.optimizer_C_AL,
            path=C_path,
        )

        if self.args.dis:
            D_path = os.path.join(
                self.args.path.checkpoint,
                "Best_D_split_id_{}_run_id_{}.pth.tar".format(split_id, run_id),
            )
            self.D, self.optimizer_D = self.load_checkpoint(
                model=self.D,
                optimizer=self.optimizer_D,
                path=D_path,
            )
        if self.start_epoch < self.args.nepochs - 1:
            logging.info("Starting training from epoch ", self.start_epoch)
        else:
            logging.info("Active learner finished training for split id ", split_id)
