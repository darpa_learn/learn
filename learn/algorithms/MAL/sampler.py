import torch
import torch.nn as nn
from typing import List, Tuple

# https://github.com/pytorch/pytorch/issues/973
torch.multiprocessing.set_sharing_strategy("file_system")
import ubelt as ub
import numpy as np
import resource
import time
import os


def compute_entropy(C, feat, lamda, eta=1.0):
    C_out = C(x=feat, reverse=True, eta=-eta)
    C_out = torch.nn.functional.softmax(C_out, dim=1)
    loss_ent = -lamda * torch.sum(C_out * (torch.log(C_out + 1e-5)), 1)
    return loss_ent


class MalSampler:
    def __init__(self, args):
        self.args = args
        self.budget = args.budget
        self.device = args.device
        self.query_strategy = args.query_strategy

    def sample(self, al, dataloader, device):

        D = al.D
        F, C = al.F, al.C_AL
        all_indices = []
        all_dis_predictions = []
        entropies = []

        print("Collecting all the predictions for unlabeled data...")
        for images, _, indices in dataloader:
            images = images.to(device)

            with torch.no_grad():
                output_F = F(images)
                entropy = compute_entropy(C, feat=output_F, lamda=self.args.lamda)
                if self.args.dis:
                    dis_predictions = D(output_F)

            entropies.extend(entropy)

            if self.args.dis:
                all_dis_predictions.extend(dis_predictions)

            all_indices.extend(indices)

        entropies = torch.stack(entropies)
        entropies = entropies.view(-1)

        if self.args.dis:
            all_dis_predictions = torch.stack(all_dis_predictions)
            all_dis_predictions = all_dis_predictions.view(-1)

            print("Sorting discriminator predictions...")
            # need to multiply by -1 to be able to use torch.topk
            all_dis_predictions *= -1

            # select the points which the discriminator thinks are the most likely to be unlabeled
            _, query_indices_diversity = torch.topk(
                all_dis_predictions, k=len(all_dis_predictions)
            )

        print("Sorting entropies...")
        # select the points which the task learner is most uncertain about
        _, query_indices_uncertainty = torch.topk(
            entropies, largest=True, k=len(entropies)
        )

        if self.query_strategy == "mal":
            print("Choosing samples based on both uncertainty and divrersity ...")
            tstart = time.time()
            intersection = self.get_intersection(
                query_indices_diversity, query_indices_uncertainty
            )
            print("[Sampling Time = {:.1f} min]".format((time.time() - tstart) / (60)))
            query_pool_indices = np.asarray(all_indices)[intersection]

        elif self.query_strategy == "uncertainty":
            print("Choosing samples based on uncertainty ...")
            query_indices_uncertainty = (
                query_indices_uncertainty[: int(self.budget)].cpu().numpy()
            )
            query_pool_indices = np.asarray(all_indices)[query_indices_uncertainty]

        elif self.query_strategy == "diversity":
            if self.args.dis:
                print("Choosing samples based on diversity ...")
                query_indices_diversity = (
                    query_indices_diversity[: int(self.budget)].cpu().numpy()
                )
                query_pool_indices = np.asarray(all_indices)[query_indices_diversity]
        else:
            raise NotImplementedError

        # save_indices(query_pool_indices, args=self.args)
        print(
            "{} samples are selected using {}-based sampling for labeling.".format(
                len(query_pool_indices), self.query_strategy
            )
        )
        return query_pool_indices

    def get_intersection(self, a, b):
        # Assumes a and b have the same indices but in different orders.
        a = a.cpu().numpy()
        b = b.cpu().numpy()
        a_keys = {idx: i for i, idx in enumerate(a)}
        b_keys = {idx: i for i, idx in enumerate(b)}
        max_f = lambda x: max(a_keys[x], b_keys[x])
        sorted_keys = sorted(a, key=max_f)
        return sorted_keys[: self.budget]


# def save_indices(indices, split, args):
#     des = os.path.join(args.path.checkpoint, 'indices')
#     if not os.path.exists(des):
#         os.mkdir(des)
#     f_path = generate_indices_path(args, split)
#
#     with open(f_path, 'wb') as f:
#         np.save(f, indices)
#     print("Indices for split {} are saved at {}.".format(split, f_path))


def generate_indices_path(args, split):
    des = os.path.join(args.path.checkpoint, "indices")
    f_path = os.path.join(
        des,
        "indices_approach_{}_dataset_{}_query_strategy_{}_split_{}_seed_{}_run_id_{}.npy".format(
            args.AL_approach,
            args.dataset,
            args.query_strategy,
            split,
            args.seed,
            args.run_id,
        ),
    )
    return f_path
