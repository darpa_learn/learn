import datetime
import os
import random
import time
import torch
import numpy as np
from copy import deepcopy
import torch.nn.functional as F
import torch.nn as nn

class EntropyLoss(nn.Module):
    def __init__(self, args):
        # adversarial entropy loss
        super(EntropyLoss, self).__init__()
        self.lamda = args.appr.lamda

    def forward(self, C_out):
        b = F.softmax(C_out, dim=1) * F.log_softmax(C_out, dim=1)
        b *= self.lamda
        b = torch.mean(torch.sum(b, 1))
        return b


def datasets_statistics(dataset_name):
    means = {}
    means['imagenet'] = [0.485, 0.456, 0.406]
    means['cifar100'] = [0.5071, 0.4867, 0.4408]
    means['CIFAR100Imbalance'] = [0.5071, 0.4867, 0.4408]
    means['inaturalist'] = means['imagenet']
    means['bdd100k'] = [0.2789, 0.2929, 0.2902]
    means['cityscapes'] = [0.290101, 0.328081, 0.286964]

    stds = {}
    stds['imagenet'] = [0.229, 0.224, 0.225]
    stds['cifar100'] = [0.2675, 0.2565, 0.2761]
    stds['CIFAR100Imbalance'] = [0.2675, 0.2565, 0.2761]
    stds['inaturalist'] = stds['imagenet']
    stds['bdd100k'] = [0.2474, 0.2653, 0.2761]
    stds['cityscapes'] = [0.182954, 0.186566, 0.184475]

    return means[dataset_name], stds[dataset_name]


###############################################################

def generate_splits(args, num_train_samples):
    if args.full_dataset is False:
        initial_budget = args.initial_budget * num_train_samples // 100
        final_budget = args.final_budget * num_train_samples // 100
        args.budget = args.split_budget * num_train_samples // 100
        num_splits = ((final_budget - initial_budget) // args.budget) + 1
        splits = []
        splits.append(initial_budget)
        splits = [int(split) for split in np.linspace(initial_budget, final_budget, num=num_splits)]

    else:
        splits = [num_train_samples]

    return splits



def make_directories(args):
    name = '{}_{}'.format(args.dataset, args.AL_approach)

    if args.wandb.notes == '':
        args.wandb.notes = name

    folder_name = args.wandb.notes + '_' + name
    if not os.path.exists(args.path.checkpoint):
        os.makedirs(args.path.checkpoint)

    args.path.checkpoint = os.path.join(args.path.checkpoint, folder_name)
    if not os.path.exists(args.path.checkpoint):
        os.makedirs(args.path.checkpoint)

    return args.path.checkpoint, args.wandb.notes



def generate_indices_path(args, split):
    des = os.path.join(args.path.checkpoint, 'indices')
    f_path = os.path.join(
        des, 'indices_approach_{}_dataset_{}_query_strategy_{}_split_{}_seed_{}_run_id_{}.npy'.format(
            args.AL_approach, args.dataset, args.query_strategy, split, args.seed, args.run_id))
    return f_path


def save_indices(indices, split, args):
    des = os.path.join(args.path.checkpoint, 'indices')
    if not os.path.exists(des):
        os.mkdir(des)
    f_path = generate_indices_path(args, split)
    # f_path = os.path.join(
    #     des, 'indices_approach_{}_dataset_{}_query_strategy_{}_split_{}_seed_{}_run_id_{}.npy'.format(
    #         args.AL_approach, args.dataset, args.query_strategy, split, args.seed, args.run_id))
    with open(f_path, 'wb') as f:
        np.save(f, indices)
    print("Indices for split {} are saved at {}.".format(split, f_path))




class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self, name, fmt=':f'):
        self.name = name
        self.fmt = fmt
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def __str__(self):
        fmtstr = '{name} {val' + self.fmt + '}'
        return fmtstr.format(**self.__dict__)


class ProgressMeter(object):
    def __init__(self, num_batches, meters, prefix=""):
        self.batch_fmtstr = self._get_batch_fmtstr(num_batches)
        self.meters = meters
        self.prefix = prefix

    def display(self, batch):
        entries = [self.prefix + self.batch_fmtstr.format(batch)]
        entries += [str(meter) for meter in self.meters]
        print('\t'.join(entries))

    def _get_batch_fmtstr(self, num_batches):
        num_digits = len(str(num_batches // 1))
        fmt = '{:' + str(num_digits) + 'd}'
        return '[' + fmt + '/' + fmt.format(num_batches) + ']'


def set_seed(seed=None):
    """
    Set the random seed for the RNG in pytorch, numpy and python.

    Args:
        seed (int): if None, will use a strong random seed.
    """
    if seed is None:
        seed = (
                os.getpid()
                + int(datetime.now().strftime("%S%f"))
                + int.from_bytes(os.urandom(2), "big")
        )
        print(f"Using a auto-generated random seed: {seed}")
    else:
        print(f"Using random seed: {seed}")

    print("Setting seed: ", seed)
    np.random.seed(seed)
    torch.set_rng_state(torch.manual_seed(seed).get_state())
    random.seed(seed)

    if torch.cuda.is_available():
        torch.cuda.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)  # Seeds all GPUs in multi-GPU model
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False





def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))

    return res[0]


def binary_accuracy(output, target):
    assert target.ndim == 1 and target.size() == target.size()
    output = output > 0.5
    acc = (target == output).sum().item() / target.size(0)

    return 100.0 * acc




class _CustomDataParallel(torch.nn.DataParallel):
    def __init__(self, model):
        super(_CustomDataParallel, self).__init__(model)

    def __getattr__(self, name):
        try:
            return super(_CustomDataParallel, self).__getattr__(name)
        except AttributeError:
            return getattr(self.module, name)


