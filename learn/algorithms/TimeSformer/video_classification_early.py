from openselfsup.datasets.pipelines import transforms
import torch
import torch.nn as nn
import torchvision
from torchvision.transforms.transforms import Normalize

import os
import numpy as np
import copy
from learn.algorithms.TimeSformer.videoClassificationAdapter import (
    VideoClassifierAdapter,
)
import logging

from models.vit import vit_base_patch16_224
from models.swin import swin_t, swin_s, swin_b
import models.optimizer as optim
import models.losses as losses
from models.utils import topks_correct, CollateFn

from hydra.utils import get_original_cwd


class VideoClassifierAlgorithm(VideoClassifierAdapter):
    def __init__(self, toolset):
        # def __init__(self, problem, base_dataset, adapt_dataset, arguments):
        VideoClassifierAdapter.__init__(self, toolset)
        self.config = toolset['protocol_config']['video_classifier']['early']['params']

    def initialize(self):
        """Initialize your algorithm and save any parameters or objects you want
        to be persistent between checkpoints.

        """

        # pre-trained network on source task (the closest task)
        #  You are not required to use it but strongly suggested to use it
        #  These are in the style of torchvision models
        self.model = self.get_model()
        self.model.cuda()
        self.model = nn.DataParallel(self.model)
        if self.config["work_dir"] is not None:
            os.makedirs(self.config["work_dir"], exist_ok=True)

    def get_model(self):
        num_classes = len(self.toolset['target_dataset'].categories)
        unfreezed_layers = []
        
        model_path = self.toolset['protocol_config']['domain_network_selector']['params']['pretrained_network_dir']
        if self.config['backbone'] == 'timesformer':
            self.config['num_classes'] = num_classes
            model = vit_base_patch16_224(self.config)
            state_dict = torch.load(
                os.path.join(
                    model_path,
                    'TimeSformer_divST_96x4_224_K400.pyth'
                    #get_original_cwd(),
                    #'learn/algorithms/TimeSformer/models/TimeSformer_divST_96x4_224_K400.pyth',
                ),
                map_location="cpu",
            )['model_state']
            state_dict = {k: v for k, v in state_dict.items() if "model.head" not in k}
            model.load_state_dict(state_dict, strict=False)
            # Unfreeze head
            for n, p in model.named_parameters():
                if "model.head" in n:
                    p.requires_grad = True
                else:
                    p.requires_grad = False
            model_depth = 12
            if self.config['unfreeze'] > 0:
                for idx in range(self.config['unfreeze']):
                    prefix = "blocks.{}".format(model_depth - idx - 1)
                    for name, p in model.named_parameters():
                        if prefix in name:
                            p.requires_grad = True
                            unfreezed_layers.append(name)
        elif self.config['backbone'] == 'swin_b':
            model = swin_b(
                os.path.join(
                    model_path,
                    'swin_base_patch244_window877_kinetics400_1k.pth'
                    #get_original_cwd(),
                    #"learn/algorithms/TimeSformer/models/swin_base_patch244_window877_kinetics400_1k.pth",
                ),
                num_classes,
            )
            # Unfreeze head
            for n, p in model.named_parameters():
                if n.startswith("head"):
                    p.requires_grad = True
                else:
                    p.requires_grad = False
            if self.config['unfreeze'] > 0:
                for idx in range(self.config['unfreeze']):
                    if idx < 2:
                        prefix = "layers.3.blocks.{}".format(1 - idx)
                    elif idx < 20:
                        prefix = "layers.2.blocks.{}".format(19 - idx)
                    else:
                        prefix = "layers.1.blocks.{}".format(21 - idx)
                    for name, p in model.named_parameters():
                        if prefix in name:
                            p.requires_grad = True
                            unfreezed_layers.append(name)
        else:
            raise NotImplementedError(
                "Backbone {} not implementated".format(self.config['backbone'])
            )

        logging.info("Unfreezed: {}".format(unfreezed_layers))

        return model

    def get_dataloaders(self, dataset, collate_fn=None):
        """Get dataloaders

        Args:
            source_dataset (VideoClassificationDataset): source dataset
            target_dataset (VideoClassificationDataset): target dataset

        Returns:
            dataloaders for source and target

        """
        # Set dataset flag
        dataset.use_orginal_dataset = True
        class_names = dataset.categories
        logging.info('Creating dataloaders')

        labeled_dataloader = None
        if dataset.labeled_size > 0:
            labeled_dataloader = torch.utils.data.DataLoader(
                dataset,
                sampler=torch.utils.data.sampler.SubsetRandomSampler(
                    dataset.get_labeled_indices()
                ),
                batch_size=min(dataset.labeled_size, int(self.config["batch_size"])),
                num_workers=int(self.config['num_workers']),
                collate_fn=collate_fn,
                drop_last=False,
            )
        unlabeled_dataloader = None
        if dataset.unlabeled_size > 0:
            unlabeled_dataloader = torch.utils.data.DataLoader(
                dataset,
                sampler=torch.utils.data.sampler.SubsetRandomSampler(
                    dataset.get_unlabeled_indices()
                ),
                batch_size=min(dataset.unlabeled_size, int(self.config["batch_size"])),
                num_workers=int(self.config['num_workers']),
                collate_fn=collate_fn,
                drop_last=False,
            )

        return labeled_dataloader, unlabeled_dataloader, class_names

    def domain_adapt_training(self):
        """This is where you will fine-tune/train your few-shot or domain adaption
        algorithm.  You may use the dataloaders below and feel free to override the
        transformers in the datasets to use whatever pretraining you desire.
        Please do not modify the datasets though.
        """

        # ###################  Creating the Labeled DataLoader ###############
        # Create Dataloaders for labeled data by getting the labeled indices and
        # creating a random sub-sampler.  Then create the dataloader using this
        # sampler
        #
        #  Note: To change the transformer in the JPLDataset dataset, edit
        #      current_dataset.transform.  You can also edit the labels during
        #      loading by editing the  current_dataset.target_transform
        #
        #  Note: Another approach could be to inherit the JPLDataset class and
        #      override the __get_item__ function
        #
        #  Check out dataset.py for more information
        #     Notes: There could be NO unlabeled data and the algorithm has to be
        #         able to handle that case!
        #
        #         The min batch size here is so that the batch size isn't
        #         larger than the labeled/unlabeled dataset which causes the
        #         dataloader to hang

        self.model.train()
        original_target_transform = copy.deepcopy(self.toolset["target_dataset"].transform)
        self.toolset["target_dataset"].transform = torchvision.transforms.Compose(
            [
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(
                    [0.45, 0.45, 0.45], [0.225, 0.225, 0.225]
                ),
            ]
        )
        labeled_dataloader, _, class_names = self.get_dataloaders(
            self.toolset["target_dataset"],
            collate_fn=CollateFn(
                sampling_rate=self.config["sampling_rate"],
                num_frames=self.config["num_frames"],
            ),
        )
        assert labeled_dataloader is not None, "Zero-shot is currently not supported"

        # ##################### fine-tune/train your model ######################
        #  Here is where you will actual finetune the model
        #  You can use the pretrained model with self.model
        #  Make sure to set any persistent tensors to cpu with `tensor.cpu` command
        optimizer = optim.construct_optimizer(self.model, self.config)
        loss_fun = losses.get_loss_func(self.config['loss_func'])(reduction="mean")
        data_size = len(labeled_dataloader)

        # In case the number of samples is less than global_batch_size
        self.config["global_batch_size"] = min(self.config["global_batch_size"], len(labeled_dataloader))
        num_iters = self.config["global_batch_size"] // self.config["batch_size"]

        for cur_epoch in range(self.config['max_epoch']):

            for cur_iter, (inputs, labels, _) in enumerate(labeled_dataloader):
                if self.model.cuda:
                    inputs, labels = inputs.cuda(), labels.cuda()
                # Set lr
                lr = optim.get_epoch_lr(
                    cur_epoch + float(cur_iter) / data_size, self.config
                )
                optim.set_lr(optimizer, lr)

                # Predict
                preds = self.model(inputs)
                loss = loss_fun(preds, labels)

                # # Optimize
                # optimizer.zero_grad()
                # loss.backward()
                # optimizer.step()

                if self.config["batch_size"] >= self.config["global_batch_size"]:
                    # Perform the backward pass.
                    optimizer.zero_grad()
                    loss.backward()
                    # Update the parameters.
                    optimizer.step()
                else:
                    assert (
                        self.config["global_batch_size"] % self.config["batch_size"]
                        == 0
                    )
                    if cur_iter == 0:
                        optimizer.zero_grad()
                    loss.backward()
                    if (cur_iter + 1) % num_iters == 0:
                        for p in self.model.parameters():
                            if p.requires_grad:
                                p.grad /= num_iters
                        optimizer.step()
                        optimizer.zero_grad()

                # Summarize
                num_topks_correct = topks_correct(preds, labels, (1, 5))
                top1_err, top5_err = [
                    (1.0 - x / preds.size(0)) * 100.0 for x in num_topks_correct
                ]

                print(
                    "Epoch: {}/{}\tIteration {}/{}\tloss: {}\ttop1_err: {}".format(
                        cur_epoch,
                        self.config['max_epoch'],
                        cur_iter,
                        data_size,
                        loss.item(),
                        top1_err.item(),
                    )
                )
        self.toolset["target_dataset"].transform = original_target_transform

        if self.config["work_dir"] is not None and self.config["save_model_every_ckpt"]:
            save_filepath = os.path.join(self.config["work_dir"], f"model_stage_{self.toolset['stage']}_ckpt_{self.toolset['ckpt']}.pth")
            torch.save(self.model.state_dict(), save_filepath)

    def inference(self, eval_dataset):
        """
        Inference is during the evaluation stage.  For this example, the
        task_network is trained in the train and adapt stage's code and
        is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to
        JPL

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        # TODO: Multi-view ensemble
        self.model.eval()

        original_eval_transform = copy.deepcopy(eval_dataset.transform)
        eval_dataset.transform = torchvision.transforms.Compose(
            [
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(
                    [0.45, 0.45, 0.45], [0.225, 0.225, 0.225]
                ),
            ]
        )
        preds = {}

        for clip_idx in range(self.config["num_test_views"]):
            for spatial_idx in range(self.config["num_test_crops"]):
                if self.config["num_test_crops"] == 1:  # Sampling the middle one
                    spatial_idx = 1
                _, eval_dataloader, _ = self.get_dataloaders(
                    eval_dataset,
                    collate_fn=CollateFn(
                        sampling_rate=self.config["sampling_rate"],
                        clip_idx=clip_idx,
                        num_clips=self.config["num_test_views"],
                        num_frames=self.config["num_frames"],
                        spatial_idx=spatial_idx,
                    ),
                )
                for inputs, _, inds in eval_dataloader:
                    inputs = inputs.cuda()

                    with torch.no_grad():
                        preds_ = self.model(inputs)

                    for indice, pred_ in zip(inds.numpy().tolist(), preds_.cpu()):
                        if indice not in preds:
                            preds[indice] = pred_
                            continue

                        if self.config["ensemble_method"] == "sum":
                            preds[indice] += pred_
                        elif self.config["ensemble_method"] == "max":
                            preds[indice] = torch.maximum(preds[indice], pred_)
                        else:
                            raise NotImplementedError

        indices, preds = list(zip(*preds.items()))
        preds = torch.stack(preds).argmax(1).numpy().tolist()
        eval_dataset.transform = original_eval_transform
        return preds, list(indices)
