# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved.

"""Loss functions."""

import torch.nn as nn
import torch.nn.functional as F


class SupContrastiveLoss(nn.Module):
    def __init__(self, reduction="mean"):
        super().__init__()
        self.ce = nn.CrossEntropyLoss(reduction=reduction)
        assert reduction == "mean", "Reduction {} not supported".format(reduction)

    def forward(self, preds, sup_labels):
        sup_logits, unsup_logits, unsup_labels = preds
        loss = self.ce(sup_logits, sup_labels)
        if unsup_labels.ndim == 1:
            loss += self.ce(unsup_logits, unsup_labels)
        elif unsup_labels.ndim == 2:
            assert (unsup_labels.sum(-1) > 0).all(), "Invalid unsup labels"
            loss -= (
                F.log_softmax(unsup_logits, dim=-1)
                * unsup_labels
                / unsup_labels.sum(-1, keepdim=True)
            ).sum()
        else:
            raise NotImplementedError(
                "Unsupported unsup labels dim: {}".format(unsup_labels.ndim)
            )

        return loss


_LOSSES = {
    "cross_entropy": nn.CrossEntropyLoss,
    "bce": nn.BCELoss,
    "bce_logit": nn.BCEWithLogitsLoss,
    "sup_contrastive": SupContrastiveLoss,
}


def get_loss_func(loss_name):
    """
    Retrieve the loss given the loss name.
    Args (int):
        loss_name: the name of the loss to use.
    """
    if loss_name not in _LOSSES.keys():
        raise NotImplementedError("Loss {} is not supported".format(loss_name))
    return _LOSSES[loss_name]
