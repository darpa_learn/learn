# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved.

"""Optimizer."""

import torch

from .utils import get_lr_at_epoch


def construct_optimizer(model, cfg):
    """
    Construct a stochastic gradient descent or ADAM optimizer with momentum.
    Details can be found in:
    Herbert Robbins, and Sutton Monro. "A stochastic approximation method."
    and
    Diederik P.Kingma, and Jimmy Ba.
    "Adam: A Method for Stochastic Optimization."

    Args:
        model (model): model to perform stochastic gradient descent
        optimization or ADAM optimization.
        cfg (config): configs of hyper-parameters of SGD or ADAM, includes base
        learning rate,  momentum, weight_decay, dampening, and etc.
    """
    if cfg['optimizer'] == "sgd":
        return torch.optim.SGD(
            model.parameters(),
            lr=cfg['lr'],
            momentum=cfg['momentum'],
            weight_decay=cfg['weight_decay'],
            nesterov=cfg['nesterov'],
        )
    # elif cfg['optimizer'] == "adam":
    #     return torch.optim.Adam(
    #         model.parameters(),
    #         lr=cfg['lr'],
    #         betas=(0.9, 0.999),
    #         eps=1e-08,
    #         weight_decay=cfg['weight_decay'],
    #     )
    # elif cfg.SOLVER.OPTIMIZING_METHOD == "adamw":
    #     return torch.optim.AdamW(
    #         model.parameters(),
    #         lr=cfg.SOLVER.BASE_LR,
    #         betas=(0.9, 0.999),
    #         eps=1e-08,
    #         weight_decay=cfg.SOLVER.WEIGHT_DECAY,
    #     )
    else:
        raise NotImplementedError(
            "Does not support {} optimizer".format(cfg['optimizer'])
        )


def get_epoch_lr(cur_epoch, cfg):
    """
    Retrieves the lr for the given epoch (as specified by the lr policy).
    Args:
        cfg (config): configs of hyper-parameters of ADAM, includes base
        learning rate, betas, and weight decays.
        cur_epoch (float): the number of epoch of the current training stage.
    """
    return get_lr_at_epoch(cfg, cur_epoch)


def set_lr(optimizer, new_lr):
    """
    Sets the optimizer lr to the specified value.
    Args:
        optimizer (optim): the optimizer using to optimize the current network.
        new_lr (float): the new learning rate to set.
    """
    for param_group in optimizer.param_groups:
        param_group["lr"] = new_lr
