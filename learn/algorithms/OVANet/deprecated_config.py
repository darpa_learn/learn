import scriptconfig as scfg


class Config(scfg.Config):
    """
    Default configuration for your algorithm

    Please add all of your default configuration parameters for your algorithm below
    with a description (help) on
        - what your parameter does
        - what changes when you use your parameter

    This is so that someone else can come and alter there parameters and know
    how this will affect the run of the algorithm

    """
    default = {
        'name': scfg.Value('MME',
                           help='name of the proposed method'),

        'batch_size': scfg.Value(64,
                                 help='batch size of network while running.  '
                                      'Increasing will do X and decrease will do Y'),

        'num_workers': scfg.Value(16,
                                  help='number of workers in the dataloader'),

        'steps': scfg.Value(10000,
                            help='maximum number of iterations '
                                 'to train (default: 50000)'),

        'method': scfg.Value('MME',
                             help='Either source and target labeled example training: "S+T", '
                                  'entropy minimization only training "ENT" or MME Training "MME"'),

        'lr': scfg.Value(0.01,
                         help='learning rate multiplication'),

        'multi': scfg.Value(0.1,
                            help='learning rate decrease factor for lower layer (default: 0.1)'),

        'T': scfg.Value(0.05,
                        help='temperature (default: 0.05).  1/T * protosimilarity (Section 3.1)'),

        'lambda': scfg.Value(0.1,
                             help='value of lambda which is the ratio of the loss when adding the supervised '
                                  'and unsupervised losses together'),

        'checkpath': scfg.Value('./save_model_ssda',
                                help='sdir to save checkpoint'),

        'seed': scfg.Value(1,
                           help='random seed (default: 1)'),

        'log_interval': scfg.Value(100,
                                   help='how many batches to wait before logging '
                                        'training status'),

        'save_interval': scfg.Value(500,
                                    help='how many batches to wait before saving a model'),

        'net': scfg.Value('alexnet',
                          help='which network to use'),

        'patience': scfg.Value(5,
                               help='early stopping to wait for improvement '
                                    'before terminating. (default: 5 (5*save_interval iterations))'),

        'lr_patience': scfg.Value(5,
                                  help='Reduce lr on plateau after this many save_interval'),

        'early': scfg.Value(True,
                            help='early stopping on validation or not'),

        'device': scfg.Value(0,
                             help='what device to use (eg. 0 means gpu 0'),

        'rate_threshold': scfg.Value(0.1,
                                     help='Rate at ratio between |unlabeled data| / |labeled data| for MME'),

        'warm_start': scfg.Value(True,
                                 help='Whether to save start at the last budget level or start for '
                                      'pretrained weights'),

        'only_target_mme': scfg.Value(False,
                                 help='Only use target data for MME algorithm'),
        'mme_freeze_backbone': scfg.Value(False,
                                      help='freeze the backbone network if true'),
        'mme_baseline_classifier': scfg.Value(False,
                                      help='Use a classifier with no normalization (standard linear classifier)'),
        'mme_baseline_plusplus': scfg.Value(False,
                                          help='Use a classifier of baseline_plusplus'),
        'mme_auto_off': scfg.Value(False,
                                          help='Automatically turn off MME if not all target classes are in '
                                               'the source datasets'),

    }
