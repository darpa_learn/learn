import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data
import ubelt as ub
from torchvision import transforms

from learn.algorithms.OVANet.imageClassificationAdapter import ImageClassifierAdapter
from learn.algorithms.OVANet.model.basenet import Predictor
from learn.algorithms.OVANet.utils.loss import ova_sup, ova_entropy
from learn.algorithms.OVANet.utils.lr_schedule import inv_lr_scheduler
from learn.utils.dataset import ImageClassificationDataset
import logging


class ImageClassifierAlgorithm(ImageClassifierAdapter):
    """
    This implementation follows MME (https://arxiv.org/pdf/1904.06487.pdf) except this allows for
    non-perfect overlap between the source and target domain labels.

    """

    def __init__(self, toolset):
        # def __init__(self, problem, base_dataset, adapt_dataset, arguments):
        ImageClassifierAdapter.__init__(self, toolset)
        self.config = toolset['protocol_config']['image_classifier']['params']

        device = self.config['device']
        if ub.iterable(device):
            self.device = device[0]
        else:
            self.device = device

    def initialize(self, source_network, source_dataset, whitelist_datasets, target_dataset, networks):
        """  Initialize your algorithm and save any parameters or objects you want to be persistent between
        budget checkpoints.  DomainNetworkSelection will choose a potential source datasets `source_dataset`
        and provide a trained network for that dataset `source_dataset.` Please do not modify the datasets
        beyond overriding the transforms (self.transform and self.transform_target are fine to change).

        Args:
            source_network (pytorch.nn.Module):  Pretrained network on source task
            source_dataset (ImageClassificationDataset): Pytorch dataset for source domain/task
            whitelist_datasets (dict[framework.dataset]): Dictionary of all possible external datasets which
                can be used on this task.  You may use this if you want to select a different dataset than
                the one selected as source.
            target_dataset: (ImageClassificationDataset): Pytorch dataset for target domain/task
            networks: (Dict[str, Dict[str, nn.module]]): Two leveled dict with the dataset as the first key
                and the network structure name as the second key.  e.g. networks['imagenet_1k']['densenet169']



        """
        logging.info('Initializing MME algorithm')
        # Note: whitelist is contained here in case you want the change the source dataset
        logging.debug(f"{whitelist_datasets.keys()}")
        self.source_dataset = source_dataset
        torch.cuda.manual_seed(self.config['seed'])

        G = source_network  # Note: source network only imagenet for now
        G.set_as_feature_extractor()
        self.config['net'] = G.model_name
        inc = G.num_feature_dims

        class_list = target_dataset.categories
        params = []
        for key, value in dict(G.named_parameters()).items():
            if value.requires_grad:
                if 'classifier' not in key:
                    params += [{'params': [value], 'lr': self.config['multi'],
                                'weight_decay': 0.0005}]
                else:
                    params += [{'params': [value], 'lr': self.config['multi'] * 10,
                                'weight_decay': 0.0005}]
        self.params = params
        #if "resnet" in self.config['net']:
        F = Predictor(num_class=len(class_list), inc=inc)
        F_ova = Predictor(num_class=len(class_list)*2, inc=inc)
        self.G = G
        self.F = F
        self.F_ova = F_ova

        # if os.path.exists(self.config['checkpath']) == False:
        #     os.mkdir(self.config['checkpath'])
        source_cats = set(source_dataset.categories.tolist())
        target_cats = set(target_dataset.categories.tolist())
        common_categories = source_cats & target_cats
        target_only_cats = target_cats - source_cats
        logging.info(f'Number of categories in common: {len(common_categories)}')
        logging.info(f'Number of Categories only in target: {len(target_only_cats)}')
        if len(common_categories) < 1:
            raise NotImplementedError("Need at least 1 common class between source and target!")

    @staticmethod
    def get_weights_for_sampler(dataset, useful_cats):
        """ get weight the dataset to give to pytorch's weightedrandomsampler.
        This is instead of subset selector so that we can weight each element.

        Args:
            dataset (ImageClassificationDataset): dataset which you are weighting
            useful_cats (list[str]): categories to weight, usually for removing
                categories from source dataset

        Returns:
            list[float]: weights for random sampler
        """
        # get counts for each class
        targets = np.array(dataset.targets)
        n = len(targets)
        cls, counts = np.unique(targets[targets!=None], return_counts=True)

        count_dict = dict(zip(cls, counts))

        # Set counts to 0 if unused
        cats = dataset.categories
        cats_to_drop = list(set(cats) - set(useful_cats))
        drop_idxs = dataset._category_name_to_category_index(cats_to_drop)

        weight_dict = dict({None: 0})
        for k, v in count_dict.items():
            if k in drop_idxs:
                weight_dict[k] = 0
            else:
                weight_dict[k] = float(n-v)/n

        # Set weights
        return [weight_dict[t] for t in targets]

    def get_dataloaders(self, source_dataset, target_dataset):
        """ Get dataloaders for source labeled for only target classes, target labeled,
        and target unlabeled

        Args:
            source_dataset (ImageClassificationDataset): source dataset
            target_dataset (ImageClassificationDataset): target dataset

        Returns:
            dataloaders for source labeled for only target classes, target labeled,
                and target unlabeled

        """
        class_names = target_dataset.categories
        a_lot = int(1e6)
        logging.info('Creating dataloaders')
        # Source
        source_weights = self.get_weights_for_sampler(source_dataset, class_names)
        source_labeled_dataloader = torch.utils.data.DataLoader(
            source_dataset,
            sampler=torch.utils.data.sampler.WeightedRandomSampler(
                source_weights,
                a_lot),
            batch_size=min(source_dataset.labeled_size,
                           int(self.config[
                                   "batch_size"])),
            num_workers=int(self.config['num_workers']),
            collate_fn=source_dataset.collate_batch,
            drop_last=False,
        )

        target_weights = self.get_weights_for_sampler(target_dataset, class_names)
        # Source
        target_labeled_dataloader = torch.utils.data.DataLoader(
            target_dataset,
            sampler=torch.utils.data.sampler.WeightedRandomSampler(
                target_weights,
                a_lot),
            batch_size=min(target_dataset.labeled_size,
                           int(self.config[
                                   "batch_size"])),
            num_workers=int(self.config['num_workers']),
            collate_fn=target_dataset.collate_batch,
            drop_last=False,
        )

        target_unlabeled_dataloader = None
        if target_dataset.unlabeled_size > 0:
            unlabeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
                target_dataset.get_unlabeled_indices()
            )
            target_unlabeled_dataloader = torch.utils.data.DataLoader(
                target_dataset,
                sampler=unlabeled_sampler,
                batch_size=min(target_dataset.unlabeled_size,
                               int(self.config["batch_size"])
                               ),
                num_workers=int(self.config['num_workers']),
                drop_last=False,
            )

        return source_labeled_dataloader, target_labeled_dataloader, target_unlabeled_dataloader, class_names

    def domain_adapt_training(self, target_dataset, eval_dataset):
        """ This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
        the dataloaders below and feel free to override the transformers in the datasets to use whatever
        pretraining you desire. Please do not modify the datasets though.

        Args:
            target_dataset (ImageClassificationDataset): pytorch dataset for the target dataset used for training
            eval_dataset (ImageClassificationDataset): eval dataset used for evaluation.  Included here in case you want to do
                transductive learning.
        """
        logging.info('OVANet Training Algorithm')
        config = self.config

        # override transforms
        crop_size = 227
        if config['net'] != 'alexnet':
            crop_size = 224

        trans = transforms.Compose([
            transforms.Resize(256),
            transforms.RandomHorizontalFlip(),
            transforms.RandomCrop(crop_size),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])

        self.source_dataset.transform = trans
        target_dataset.transform = trans

        # Get dataloaders for source and target
        source_loader, target_loader, target_loader_unl, class_list = self.get_dataloaders(
            self.source_dataset, target_dataset
        )
        lr = config['lr']
        params = self.params
        method = config['method']

        G = self.G.train().to(self.device)
        F = self.F.train().to(self.device)
        F_ova = self.F_ova.train().to(self.device)
        # ##################### fine-tune/train your model ######################
        #  Here is where you will actual finetune the model
        #  You can use the pretrained model with self.model
        #  Make sure to set any persistent tensors to cpu with `tensor.cpu` command

        optimizer_g = optim.SGD(params, momentum=0.9,
                                weight_decay=0.0005, nesterov=True)
        optimizer_f = optim.SGD(list(F.parameters())+list(F_ova.parameters()),
                                lr=1.0, momentum=0.9,
                                weight_decay=0.0005, nesterov=True)

        def zero_grad_all():
            optimizer_g.zero_grad()
            optimizer_f.zero_grad()

        param_lr_g = []
        for param_group in optimizer_g.param_groups:
            param_lr_g.append(param_group["lr"])
        param_lr_f = []
        for param_group in optimizer_f.param_groups:
            param_lr_f.append(param_group["lr"])
        criterion = nn.CrossEntropyLoss()

        all_step = config['steps']
        data_iter_s = iter(source_loader)
        data_iter_t = iter(target_loader)
        data_iter_t_unl = iter(target_loader_unl)
        len_train_source = len(source_loader)
        len_train_target = len(target_loader)
        len_train_target_semi = len(target_loader_unl)
        best_loss = 1e10
        counter = 0
        loss_avg = 0
        loss_ova_avg = 0
        loss_t_avg = 0
        prog = ub.ProgIter(range(all_step), desc="Training OVANet")
        rate_labeled = float(len_train_target_semi / len_train_target)
        flag_unl_training = rate_labeled < self.config["rate_threshold"]
        if flag_unl_training:
            print("disable MME training")
        else:
            print("unlabeled / labeled %s"%(rate_labeled))

        for step in prog:
            optimizer_g = inv_lr_scheduler(param_lr_g, optimizer_g, step,
                                           init_lr=config['lr'])

            optimizer_f = inv_lr_scheduler(param_lr_f, optimizer_f, step,
                                           init_lr=config['lr'])
            lr = optimizer_f.param_groups[0]['lr']
            if step % len_train_target == 0:
                data_iter_t = iter(target_loader)
            if step % len_train_target_semi == 0:
                data_iter_t_unl = iter(target_loader_unl)
            if step % len_train_source == 0:
                data_iter_s = iter(source_loader)

            im_data_s, gt_labels_s, _ = next(data_iter_s)
            im_data_tu, _, _ = next(data_iter_t_unl)
            im_data_t, gt_labels_t, _ = next(data_iter_t)
            zero_grad_all()
            data = torch.cat((im_data_s, im_data_t), 0).to(self.device)
            target = torch.cat((gt_labels_s, gt_labels_t), 0).to(self.device)
            F_ova.weight_norm()
            output = G(data)
            out1 = F(output)
            out_ova = F_ova(output)
            loss = criterion(out1, target)
            loss_ova = ova_sup(out_ova, target)
            loss_all = loss + loss_ova
            loss_avg += loss.cpu().item()
            loss_ova_avg += loss_ova.cpu().item()
            loss_all.backward()
            if not method == 'S+T' and not flag_unl_training:
                output = G(im_data_tu.to(self.device))
                steps_here = max(step % config['log_interval'], 1)
                output_ova = F_ova(output)
                loss_t = ova_entropy(output_ova)
                loss_t_avg += loss_t.cpu().item()
                loss_t.backward()
                optimizer_f.step()
                optimizer_g.step()
                zero_grad_all()
                prog.set_extra(
                    f'lr{lr:.02g} Loss CE:{loss_avg/steps_here:.04f} E:{loss_t_avg/steps_here:.04f}')
            if (step % config['log_interval'] == 0 or step % config['save_interval'] == 0) and step > 0:
                loss_avg /= config['log_interval']
                loss_t_avg /= config['log_interval']

                if not method == 'S+T':
                    log_train = (f'\nTrain Ep: {step} lr: {lr} \t '
                                 f'Closed Loss: {loss_avg:.6f} OVA Loss: {loss_ova_avg:.6f} Loss T {loss_t_avg:.6f} '
                                 f'Method {method}\n')
                else:
                    log_train = (f'\nTrain Ep: {step} lr: {lr} \t '
                                 f'Closed Loss: {loss_avg:.6f} OVA Loss: {loss_ova_avg:.6f} Method {method}\n')
                logging.debug(f"{log_train}")

            if step % config['save_interval'] == 0 and step > 0:
                if loss_avg < best_loss:
                    logging.debug(f'\nNew best loss: {best_loss:04f}->{loss_avg:04f}')
                    best_loss = loss_avg
                    counter = 0
                else:
                    counter += 1
                    if counter >= config['patience']:
                        break

        self.G = G.cpu()
        self.F = F.cpu()
        self.F_ova = F_ova.cpu()

        import gc
        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()

    def inference(self, eval_dataset):
        """
        Inference is during the evaluation stage.  For this example, the task_network is trained in the
        train and adapt stage's code and is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to JPL

        Args:
            eval_dataset (framework.datasets.ImageClassificationDataset): unlabeled evaluation framework.
                We need to predict the classes for each item in the dataset.

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        config = self.config

        # override transforms
        crop_size = 227
        if config['net'] != 'alexnet':
            crop_size = 224

        trans = transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(crop_size),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])

        eval_dataset.transform = trans
        G = self.G.eval().to(self.device)
        F = self.F.eval().to(self.device)
        F_ova = self.F_ova.eval().to(self.device)

        eval_dataloader = data.DataLoader(eval_dataset,
            batch_size=int(self.config["batch_size"]),
            drop_last=False
        )
        preds = []
        indices = []
        with torch.no_grad():
            for imgs, _, inds in eval_dataloader:
                imgs = imgs.to(self.device)
                output = G(imgs)
                preds_ = F(output)
                preds_ova = F_ova(output)
                open_class = int(preds_.size(1)) - 1
                preds_closed = torch.argmax(preds_, dim=1)

                ## Open-set Evaluation
                out_open = torch.nn.functional.softmax(preds_ova.view(preds_ova.size(0), 2, -1), 1)
                tmp_range = torch.range(0, preds_ova.size(0) - 1).long().cuda()
                pred_unk = out_open[tmp_range, 0, preds_closed]
                ## if the score is larger than 0.5, the sample is supposed to be an unknown class
                ind_unk = np.where(pred_unk.data.cpu().numpy() > 0.5)[0]

                ## The index of unknown class is the size of preds_.size(1)
                preds_closed[ind_unk] = open_class

                preds += preds_closed.cpu().numpy().tolist()
                indices += inds.numpy().tolist()

        self.G = G.cpu()
        self.F = F.cpu()
        self.F_ova = F_ova.cpu()

        import gc
        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()

        return preds, indices
