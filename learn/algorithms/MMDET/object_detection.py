""" Template for using mmdetection (https://github.com/open-mmlab/mmdetection) 
for object detection in the learn task.  When
adding a new algorithm to the framework, make a copy of this folder, rename it,
and then you may add your code in the new folder.  You may also add new files to this
newly created folder (such as dropping in all the code from your repository).  Also,
update the imports below.

Look at the ObjectDetectionAlgorithm for more details on what you need to do to this
file.

"""

import os
import sys
import logging
import random
logger = logging.getLogger(__name__)

import time
import datetime
import shutil

from learn.algorithms.MMDET.objectDetectionAdapter import ObjectDetectionAdapter
from learn.algorithms.FsDet.distributed import trainer_func
from learn.utils.wandb_utils import WANDB_INFO
from learn.utils.metrics import mean_average_precision, mAP

import learn.algorithms.MMDET.register_modules

from functools import partial

import torch
import torch.nn.functional as F
import cv2
import numpy as np
from contextlib import contextmanager
import gc
import ubelt as ub
import copy
import pandas as pd
import yaml
import json
from hydra.utils import get_original_cwd
# from IPython import embed
from tempfile import TemporaryDirectory
import subprocess

import mmcv
from mmcv.runner import load_checkpoint
from mmcv import Config
import mmdet
from mmdet.datasets import build_dataset, build_dataloader
from mmdet.models import build_detector
from mmdet.apis import train_detector, inference_detector # , init_random_seed, set_random_seed
from mmdet.models.builder import LOSSES
from mmdet.datasets.builder import DATASETS
from mmdet.datasets.custom import CustomDataset
from mmdet.utils import collect_env


# os.environ['OMP_NUM_THREADS'] = "1"


class ObjectDetectionAlgorithm(ObjectDetectionAdapter):
    """  Object detection algorithm class.  you need to fill out the initialization
    domain_adapt_training, and inference functions.  Each function has a description
    on what you need to do in that function.

    Please remember to free GPU memory at the end of each function

    """

    def __init__(self, toolset):
        """
            First initialization of the function.  This sets the toolset and
            loads the config.  The algorithm initialization is separate so that
            the protocol can change the defaults before your algorithm loads.
            Most likely only Kitware will be editing this.

            Args:
                toolset (dict): dictionary of functions, datasets, and configs
        """
        ObjectDetectionAdapter.__init__(self, toolset)
        self.config = toolset['protocol_config']['object_detector']['params']

        if WANDB_INFO.run_wandb:
            self.wandb_name = toolset["protocol_config"]["wandb_params"]["name"]

        device = self.config['device']
        if ub.iterable(device):
            self.device = device
        else:
            if device == -1:
                self.device = list(range(torch.cuda.device_count()))
            else:
                self.device = [device]
        if len(self.device) > torch.cuda.device_count():
            self.device = self.device[:torch.cuda.device_count()]

        self.ddp_num_gpus = len(self.device)

        if self.config["work_dir"] is not None:
            self.work_dir = self.config["work_dir"]
        else: 
            self.work_dir = self.toolset["temp_model_directory"]
        os.makedirs(self.work_dir, exist_ok=True)

    def initialize(self):
        """  Here is where you initialize your algorithm.  It is seperate from on
        above so that the protocol calling this can override the default config
        when necessary.  Here you have `self.toolset[]` which contains the classes
        and values you will need to run the algorithm.  Some useful tools are:

            target_dataset: pytorch dataset that contains the semi-supervised
                training data you will need to train/adapt.  The dataset is defined
                in framework.dataset
            eval_dataset: Unlabeled dataset for final evaluation.  You may use it
                for transductive learning if you want to
            source_dataset: A Dataset from the most similar task we can find in the
                external datasets.
            source_network: Pre-trained network from source dataset.  You can use
                this as a dataset for the backbone.


        Returns:

        """

        train_dataset = self.toolset['target_dataset']
        test_dataset = self.toolset['eval_dataset']
        self.target_dataset_name = f'{train_dataset.name}_train'
        self.eval_dataset_name = f'{test_dataset.name}_test'

        
    def domain_adapt_training(self):
        """ Train your algorithm to adapt to the target_dataset.  This will be called
        multiple times, each with more labels in the target dataset labeled.
        Make sure to set your model to cpu() before the end of the function.  Also,
        clear the cuda cache (as shown below).

        You may save your network weight between the calls (but keep it in CPU)
        """

        logger = logging.getLogger(__name__)

        # if self.toolset["stage"] != 'base':
        #     print("finished base stage, exiting")
        #     exit()

        # put the datasets in mmdet format
        train_dataset = self.toolset['target_dataset']
        self.transform_dataset(train_dataset, train=True)

        self.register_new_losses()

        # override config entirely with new config and make no changes
        if self.config["new_config"] is not None:
            cfg_pth = os.path.join(get_original_cwd(), f'learn/algorithms/MMDET/configs/{self.config["new_config"]}')
            self.mmdet_config = Config.fromfile(cfg_pth)
        
        # load a config (potentially mmdet config) from outside learn folder
        # and make necessary changes to it
        elif self.config["config_override"] is not None:
            self.mmdet_config = self.set_config(self.config["config_override"])
        
        # load specified config from learn folder and make changes to it
        else:
            mmdet_config_path = os.path.join(get_original_cwd(), './learn/algorithms/MMDET/configs/'+self.config["mmdet_model_config_file"])
            self.mmdet_config = self.set_config(mmdet_config_path)

        self.mmdet_config.work_dir = self.work_dir

        # loop over config, and if there are any num_classes, replace it
        # there might be problems with this (i.e. won't work with nested lists)
        # but I think it's fine for mmdet's config structure
        def replace(conf, depth):
            if depth <= 0:
                return
            try:
                for k,v in conf.items():
                    if isinstance(v, dict):
                        replace(v, depth-1)
                    elif isinstance(v, list):
                        for element in v:
                            replace(element, depth-1)
                    else:
                        # print(k,v)
                        if k == 'num_classes':
                            conf[k] = len(self.toolset["target_dataset"].categories)
                        if k == 'CLASSES':
                            conf[k] = self.toolset['target_dataset'].categories
            except:
                pass

        replace(self.mmdet_config, 500)
        print(f'Config:\n{self.mmdet_config.pretty_text}')

        if self.config["checkpoint_override"] is not None:
            original_chkpt_file = self.config["checkpoint_override"]
        else:
            original_chkpt_file = str(os.path.join(self.toolset['protocol_config']['domain_network_selector']['params']["pretrained_network_dir"],
                self.config["model_checkpoint_file"]))

        if self.toolset["protocol_config"]["self_supervision_pretrain"] is not None:
            if self.toolset["protocol_config"]["self_supervision_pretrain"]["name"] == "CutLER/pretrain_algo.py":
                if os.path.exists(str(os.path.join(self.toolset["protocol_config"]["self_supervision_pretrain"]["params"]["work_dir"], "pretrain.pth"))):
                    original_chkpt_file = os.path.join(self.toolset["protocol_config"]["self_supervision_pretrain"]["params"]["work_dir"], "pretrain.pth")
                    logger.info(f"Found CutLER weights at {original_chkpt_file}")

        # seed = np.random.randint(2**31)
        seed = self.config["seed"]
        random.seed(seed)
        np.random.seed(seed)
        torch.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)
        self.mmdet_config.seed = seed


        if self.config["use_ddp"] and torch.cuda.device_count() > 1 and self.ddp_num_gpus > 1:

            logger.info("Using DDP")
            self.mmdet_config.dump(os.path.join(self.work_dir, 'mmdet_config.py'))
            training_args = ['--work_dir', self.work_dir, '--seed', str(seed), '--gpus', str(len(self.device)), \
                '--model_checkpoint_file', original_chkpt_file]
            training_script = os.path.join(get_original_cwd(), './learn/algorithms/MMDET/distributed.py')
            self.torch_distributed_launch(nproc_per_node=len(self.device), training_script=training_script, training_script_args=training_args)
            logger.info("Returned from distributed training")

            # distributed model should be saved in the work dir
            model = build_detector(
                self.mmdet_config.model, train_cfg=self.mmdet_config.get('train_cfg'), test_cfg=self.mmdet_config.get('test_cfg')
            )
            checkpoint_file = os.path.join(self.work_dir, 'latest.pth')
            chkpt = load_checkpoint(model, checkpoint_file)
            self.model = model

        else:

            meta = dict()
            env_info_dict = collect_env()
            env_info = '\n'.join([(f'{k}: {v}') for k, v in env_info_dict.items()])
            dash_line = '-' * 60 + '\n'
            logger.info('Environment info:\n' + dash_line + env_info + '\n' +
                        dash_line)
            meta['env_info'] = env_info
            meta['config'] = self.mmdet_config.pretty_text
            meta['seed'] = seed

            logger.info("building dataset")
            datasets = [build_dataset(self.mmdet_config.data.train)]

            logger.info("building detector")
            model = build_detector(
                self.mmdet_config.model, train_cfg=self.mmdet_config.get('train_cfg'), test_cfg=self.mmdet_config.get('test_cfg')
            )

            checkpoint_file = original_chkpt_file
            chkpt = load_checkpoint(model, checkpoint_file)

            logger.info("training model")

            # the fix below will log multiple mmdet things for training so set this to error
            # for now to avoid that
            log = logging.getLogger()
            log.handlers[0].level = logging.ERROR

            model.train()
            train_detector(model, datasets, self.mmdet_config, distributed=False, validate=False, meta=meta)

            # somehow the root logger's stream handler is getting set to ERROR, so set it back to INFO manually
            # this will then set __name__'s log level to INFO again
            log = logging.getLogger()
            log.handlers[0].level = logging.INFO
            logger = logging.getLogger(__name__)

            logger.info("finished training")

            self.model = model

        if self.config["save_model_every_ckpt"]:
            if os.path.exists(os.path.join(self.work_dir, "latest.pth")):
                fname = str(self.toolset["stage"]) + "_" + str(self.toolset['ckpt']) + "_model.pth"
                shutil.copy(os.path.join(self.work_dir, "latest.pth"), os.path.join(self.work_dir, fname))

        if self.config["eval_train_set"]:
            if os.path.exists(os.path.join(self.work_dir, "latest.pth")):
                fname = str(self.toolset["stage"]) + "_" + str(self.toolset['ckpt']) + "_model.pth"
                shutil.copy(os.path.join(self.work_dir, "latest.pth"), os.path.join(self.work_dir, fname))
            
            fname = str(self.toolset["stage"]) + "_" + str(self.toolset['ckpt']) + "_train_data_coco.json"
            if os.path.exists(os.path.join(self.work_dir, "train_data_coco.json")):
                shutil.copy(os.path.join(self.work_dir, "train_data_coco.json"), os.path.join(self.work_dir, fname))

        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()
        return
        
    
    def inference(self):
        """
        Inference is during the evaluation stage.  Predict all images on the eval
        dataset (as the example below is doing).

        Returns:
            tuple(list[tuple],list[int]): preds and indices
                predicted category indices and image indices

        """
        
        torch.cuda.set_device(self.device[0]) # inference after ddp is slow without this
        self.model.cpu()
        self.model.cuda()

        self.mmdet_config.data.test.test_mode = True
        # test_dataset = build_dataset(self.mmdet_config.data.test)
        # test_loader = build_dataloader(test_dataset,
        #     samples_per_gpu=1, # batch size of each gpu
        #     workers_per_gpu=1,
        #     num_gpus=1,
        #     dist=False,
        #     shuffle=False)
        self.model.cfg = self.mmdet_config


        if self.config["eval_train_set"]:
            train_loader = torch.utils.data.DataLoader(self.toolset["target_dataset"], batch_size=1)
            self.model.eval()
            items = []

            for inference_itr, (img, _, inds) in enumerate(train_loader):

                item = {}
                item["index"] = inds[0].detach().cpu().item()
                item["labels"] = []
                item["boxes"] = []
                item["confidences"] = []
                
                if inds.detach().cpu().item() not in self.toolset["target_dataset"].get_labeled_indices():
                    continue

                filename = [os.path.join(self.toolset["target_dataset"].root, self.toolset["target_dataset"].index_to_image_fname[ind.cpu().item()]) for ind in inds]
                results = inference_detector(self.model, filename)

                for result_idx, result in enumerate(results):
                    labels = [np.full(bbox_.shape[0], i, dtype=np.int32) for i,bbox_ in enumerate(result)]
                    labels = np.concatenate(labels)
                    bboxes = np.vstack(result)

                    for i in range(len(labels)):
                        item["confidences"].append(float(bboxes[i][-1]))
                        bboxes_ = list(map(int, bboxes[i][:-1]))
                        bbox_formatted = list(map(str, bboxes_))
                        item["boxes"].append(', '.join(bbox_formatted))
                        item["labels"].append(str(labels[i]))

                items.append(item)

            with open(os.path.join(self.work_dir, str(self.toolset["stage"]) + "_" + str(self.toolset['ckpt']) + '_train_preds.json'), 'w') as fp:
                json.dump({"results": items}, fp)


        test_loader_og = torch.utils.data.DataLoader(self.toolset["eval_dataset"], batch_size=1) # self.config["batch_size"])

        bbox = []
        conf = []
        classes = []
        indices = []
        widths = []
        heights = []

        self.model.eval()

        if self.config["inference_write_dir"] is not None:
            import pandas as pd
            img_write_dir = self.config["inference_write_dir"]
            os.makedirs(img_write_dir, exist_ok=True)

            gt_info = {}
            gt_labels = pd.read_feather(os.path.join(self.mmdet_config.data_root, '../../labels_full/labels_test.feather'))
            for (id_, bbox_, class_) in zip(gt_labels["id"], gt_labels["bbox"], gt_labels["class"]):
                if id_ not in gt_info:
                    gt_info[id_] = {'bbox': [[int(i) for i in bbox_.split(',')]], 'label': [class_]}
                else:
                    gt_info[id_]['bbox'].append([int(i) for i in bbox_.split(',')])
                    gt_info[id_]['label'].append(class_)


        logger.info(f"Starting inference for {len(test_loader_og)} batches")
        start_time = time.time()
        last_log_time = time.time()

        for inference_itr, (img, _, inds) in enumerate(test_loader_og):
            if time.time() - last_log_time > 30:
                print(f"Completed {inference_itr}/{len(test_loader_og)} batches, {(time.time() - start_time):.2f} seconds elapsed")
                last_log_time = time.time()

            filename = [os.path.join(self.toolset["eval_dataset"].root, self.toolset["eval_dataset"].index_to_image_fname[ind.cpu().item()]) for ind in inds]
            results = inference_detector(self.model, filename)

            if self.config["inference_write_dir"] is not None:
                img = cv2.imread(str(filename[0])) 
                gt_dic = gt_info[self.toolset["eval_dataset"].index_to_image_fname[inds.cpu().item()]]

            for result_idx, result in enumerate(results):
                # if not doing semantic segmentation, result is a list of length
                # num_classes, with bounding boxes in each result[i] for <= 0 i <= num_classes
                labels = [np.full(bbox_.shape[0], i, dtype=np.int32) for i,bbox_ in enumerate(result)]
                labels = np.concatenate(labels)
                bboxes = np.vstack(result)

                for i in range(len(labels)):

                    indices.append(inds[result_idx].cpu().item())
                    conf.append(bboxes[i][-1])
                    bboxes_ = list(map(int, bboxes[i][:-1]))
                    bbox_formatted = list(map(str, bboxes_))
                    bbox.append(', '.join(bbox_formatted))
                    classes.append(labels[i])

                    widths.append(bboxes_[2] - bboxes_[0])
                    heights.append(bboxes_[3] - bboxes_[1])

                    if self.config["inference_write_dir"] is not None:
                        img = cv2.rectangle(img, (bboxes_[0], bboxes_[1]), (bboxes_[2], bboxes_[3]), (0,0,255), 2)
                        cat_label = self.toolset["eval_dataset"].category_index_to_category[labels[i]]
                        img = cv2.putText(img, cat_label, (bboxes_[0], bboxes_[3]-10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255),1)

            if self.config["inference_write_dir"] is not None:
                for gt_bbox, gt_label in zip(gt_dic['bbox'], gt_dic['label']):
                    img = cv2.rectangle(img, (gt_bbox[0], gt_bbox[1]), (gt_bbox[2], gt_bbox[3]), (0,255,0), 2)
                    cat_label = gt_label
                    img = cv2.putText(img, cat_label, (gt_bbox[0], gt_bbox[3]-10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0),1)

                cv2.imwrite(os.path.join(img_write_dir, str(self.toolset["eval_dataset"].index_to_image_fname[inds.cpu().item()])), img)


        try:
            print()
            print('bbox ', len(bbox), len(conf), len(classes), len(indices))
            print('confidence ', np.max(conf), np.min(conf))
            print('indcies ', np.max(indices), np.min(indices))
            print('classes ', len(classes), np.max(classes), np.min(classes), np.sum(classes))
            print()
            print('widths ', np.min(widths), np.max(widths), np.mean(widths))
            print('heights ', np.min(heights), np.max(heights), np.mean(heights))
            print()
            for x in range(20):
                print(bbox[x], conf[x], classes[x], indices[x])
        except:
            pass

        self.bbox = bbox
        self.conf = conf
        self.classes = classes
        self.indices = indices


        preds = (self.bbox, self.conf, self.classes)

        # Random response useful to debugging to make sure you have the correct
        #    format.
        # preds2, indices2 = self.toolset["eval_dataset"].dummy_data('object_detection')
        logger.info(f'Predicted {len(indices)} objects on {len(test_loader_og)} images.')
        logger.info(f"Unique indices in the datasets {len(np.unique(indices))}")

        print(f'Predicted {len(indices)} objects on {len(test_loader_og)} images.')
        print(f"Unique indices in the datasets {len(np.unique(indices))}")

        del self.model

        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()
        print("returning from inference")
        
        return preds, self.indices

    
    def set_config(self, path):
        """
        Load an MMDET config for training. There is the MMDET config which
        contains the low-level model/training details. 
        """

        train_dataset =  self.toolset['target_dataset']
        test_dataset = self.toolset['eval_dataset']
    
        mmdet_config = Config.fromfile(path)
        mmdet_config.dataset_type = 'CocoDataset'
        mmdet_config.data_root = train_dataset.root
        mmdet_config.data.ann_file = str(os.path.join(self.work_dir, 'train_data_coco.json'))
        train_pipeline = mmdet_config.train_pipeline
        test_pipeline = mmdet_config.test_pipeline
        mmdet_config.classes = tuple(train_dataset.categories)

        # print(type(mmdet_config)) # <class 'mmcv.utils.config.Config'>

        # if using RepeatDataset
        if mmdet_config.data.train.type == "RepeatDataset":
            if self.config["use_class_balanced"] and mmdet_config.data.train.dataset.type != 'ClassBalancedDataset':
                mmdet_config.data.train.dataset.type = 'CocoDataset'
                mmdet_config.data.train.dataset.data_root = train_dataset.root
                mmdet_config.data.train.dataset.ann_file = str(os.path.join(self.work_dir, 'train_data_coco.json'))
                mmdet_config.data.train.dataset.img_prefix = train_dataset.root
                mmdet_config.data.train.dataset.classes = tuple(train_dataset.categories)

                data = copy.deepcopy(mmdet_config.data.train.dataset)
                mmdet_config.data.train.dataset = dict(
                    type='ClassBalancedDataset',
                    oversample_thr=self.config["oversample_thr"],
                    dataset=data)
            elif self.config["use_class_balanced"]:
                mmdet_config.data.train.dataset.dataset.type = 'CocoDataset'
                mmdet_config.data.train.dataset.dataset.data_root = train_dataset.root
                mmdet_config.data.train.dataset.dataset.ann_file = str(os.path.join(self.work_dir, 'train_data_coco.json'))
                mmdet_config.data.train.dataset.dataset.img_prefix = train_dataset.root
                mmdet_config.data.train.dataset.dataset.classes = tuple(train_dataset.categories)
            else:
                mmdet_config.data.train.dataset.type = 'CocoDataset'
                mmdet_config.data.train.dataset.data_root = train_dataset.root
                mmdet_config.data.train.dataset.ann_file = str(os.path.join(self.work_dir, 'train_data_coco.json'))
                mmdet_config.data.train.dataset.img_prefix = train_dataset.root
                mmdet_config.data.train.dataset.classes = tuple(train_dataset.categories)

        elif mmdet_config.data.train.type == 'ClassBalancedDataset' and self.config["use_class_balanced"]:
            mmdet_config.data.train.oversample_thr = self.config["oversample_thr"]
            mmdet_config.data.train.dataset.type = 'CocoDataset'
            mmdet_config.data.train.dataset.data_root = train_dataset.root
            mmdet_config.data.train.dataset.ann_file = str(os.path.join(self.work_dir, 'train_data_coco.json'))
            mmdet_config.data.train.dataset.img_prefix = train_dataset.root
            mmdet_config.data.train.dataset.classes = tuple(train_dataset.categories)
        
        else:
            mmdet_config.data.train.type = 'CocoDataset'
            mmdet_config.data.train.data_root = train_dataset.root
            mmdet_config.data.train.ann_file = str(os.path.join(self.work_dir, 'train_data_coco.json'))
            mmdet_config.data.train.img_prefix = train_dataset.root
            mmdet_config.data.train.classes = tuple(train_dataset.categories) # tuple(train_dataset.category_to_category_index.values())

            if self.config["use_class_balanced"]:
                data = copy.deepcopy(mmdet_config.data.train)
                mmdet_config.data.train = dict(
                    type='ClassBalancedDataset',
                    oversample_thr=self.config["oversample_thr"],
                    dataset=data)

        mmdet_config.data.val.type = 'CocoDataset'
        mmdet_config.data.val.data_root = train_dataset.root
        mmdet_config.data.val.ann_file = str(os.path.join(self.work_dir, 'train_data_coco.json'))
        mmdet_config.data.val.img_prefix = train_dataset.root
        mmdet_config.data.val.classes = tuple(train_dataset.categories) # tuple(train_dataset.category_to_category_index.values())

        mmdet_config.data.test.type = 'CocoDataset'
        mmdet_config.data.test.data_root = train_dataset.root  # change back to test??
        mmdet_config.data.test.ann_file = str(os.path.join(self.work_dir, 'train_data_coco.json'))
        mmdet_config.data.test.img_prefix = train_dataset.root
        mmdet_config.data.test.classes = tuple(train_dataset.categories) # tuple(train_dataset.category_to_category_index.values()) # is this issue in model?

        mmdet_config.log_config.interval = self.config["log_interval"]
        mmdet_config.checkpoint_config.interval = self.config["checkpoint_interval"]
        mmdet_config.data.samples_per_gpu = self.config["batch_size"]  # Batch size
        mmdet_config.gpu_ids = self.device
        mmdet_config.device = 'cuda'
        mmdet_config.work_dir = self.config["work_dir"]

        ckpt = int(self.toolset["ckpt"])
        if self.toolset["stage"] == "adapt":
            if len(self.config["iters_per_ckpt_adapt"]) > 0 and ckpt < len(self.config["iters_per_ckpt_adapt"]):
                num_iter = self.config["iters_per_ckpt_adapt"][ckpt]
            else:
                num_iter = self.config["max_iters"]
        else:    
            if len(self.config["iters_per_ckpt"]) > 0 and ckpt < len(self.config["iters_per_ckpt"]):
                num_iter = self.config["iters_per_ckpt"][ckpt]
            else:
                num_iter = self.config["max_iters"]

            
        mmdet_config.lr_config.warmup_iters = 1 if self.config["warmup_iters"] >= num_iter else self.config["warmup_iters"]
        mmdet_config.lr_config.step = [step for step in self.config["lr_steps"] if step < num_iter]
        mmdet_config.runner = {'type': 'IterBasedRunner', 'max_iters': num_iter}
        mmdet_config.optimizer.lr = self.config["lr"]


        # print(f'Config:\n{mmdet_config.pretty_text}')

        # mmdet_config.dump(os.path.join(get_original_cwd(), './learn/algorithms/MMDET/configs/convnext_extra_large_config.py'))
        # exit()

        return mmdet_config


    def transform_dataset(self, dataset, train=False):
        """ Transform our datasets into the coco format which can be then be registered
        into the dectectron system.  For now we are only training it but we might
        want to create a validation dataset to test on as well

        Args:
            dataset (framework.dataset):  Dataset from framework

        Returns
            dict in coco format.
        """
        logger.info('Converting target_dataset to coco dataset')

        bbox_heights = []
        bbox_widths = []

        all_labels = []

        images = []
        annotations = []
        categories = []
        annotation_id = 0

        for cat_name, cat_id in dataset.category_to_category_index.items():
            categories.append({'name': cat_name, 'id': int(cat_id)})
        
        num_images = dataset.num_images

        for index in range(num_images):
            filename = dataset.root + '/' + dataset.image_fnames[index]
            img = mmcv.image.imread(filename)
            height, width = img.shape[:2]
            targets = dataset.targets[index]
            
            image_dct = {'file_name': dataset.image_fnames[index], # dataset.root + '/' + dataset.image_fnames[index],
                'height': height,
                'width': width,
                'id': int(index)
            }

            image_anno_ctr = 0

            if targets is not None:

                for target in targets:
                    bbox = target['bbox'].cpu().numpy().tolist()
                    bbox = [float(x) for x in bbox]

                    # skip bboxes with 0 width or height
                    if (bbox[2] - bbox[0]) <= 0 or (bbox[3] - bbox[1]) <= 0:
                        continue

                    all_labels.append(target['category'])
                    bbox_widths.append(bbox[2] - bbox[0])
                    bbox_heights.append(bbox[3] - bbox[1])

                    annotation_dct = {'bbox': [bbox[0], bbox[1], (bbox[2]-bbox[0]), (bbox[3]-bbox[1])],  # coco annotations in file need x, y, width, height
                        'image_id': int(index),
                        'area': (bbox[2]-bbox[0]) * (bbox[3]-bbox[1]),
                        'category_id': int(target['category']),
                        'id': annotation_id,
                        'iscrowd': 0
                    }
                    annotations.append(annotation_dct)
                    annotation_id += 1
                    image_anno_ctr += 1
            
            if not train or (train and image_anno_ctr > 0):
                images.append(image_dct)

        all_labels = set(all_labels)
        print(f"set of all potential labels: {all_labels} \n")

        if train and len(images) > 0:
            print(f'min width: {np.min(bbox_widths)}')
            print(f'max width: {np.max(bbox_widths)}')
            print(f'min height: {np.min(bbox_heights)}')
            print(f'max height: {np.max(bbox_heights)}')
            print(f'avg width: {np.mean(bbox_widths)}')
            print(f'avg height: {np.mean(bbox_heights)}')

        logger.info(f"Transformed the dataset into COCO style. "
                     f"Num Images {len(images)} and Num Annotations: {annotation_id}")

        coco_format_json = dict(
            images=images,
            annotations=annotations,
            categories=categories)
        mmcv.dump(coco_format_json, os.path.join(self.work_dir, 'train_data_coco.json'))


    def register_new_losses(self):
        train_dataset = self.toolset["target_dataset"]

        LOSSES._module_dict.pop('ReducedFocalLoss', None)
        LOSSES._module_dict.pop('EQLv2', None)

        # https://github.com/facebookresearch/fvcore/blob/main/fvcore/nn/focal_loss.py
        def reduced_focal_loss(inputs, targets, threshold=0.5, alpha=0.25, gamma=2.0):
            
            targets = F.one_hot(targets, num_classes=inputs.shape[-1]).type_as(inputs)
            p = torch.sigmoid(inputs)
            ce_loss = F.binary_cross_entropy_with_logits(
                inputs, targets, reduction="none"
            )
            p_t = p * targets + (1 - p) * (1 - targets)
            ones_mask = p_t < threshold
            other_mask = p_t >= threshold
            other_mask = other_mask * (((1 - p_t) ** gamma) / threshold ** gamma)
            f_x = ones_mask + other_mask
            
            loss = ce_loss * f_x

            if alpha >= 0:
                alpha_t = alpha * targets + (1 - alpha) * (1 - targets)
                loss = alpha_t * loss

            return loss

        @LOSSES.register_module()
        class ReducedFocalLoss(torch.nn.Module):
            def __init__(self,
                        threshold=0.5,
                        gamma=2.0,
                        alpha=0.25,
                        reduction='mean',
                        loss_weight=1.0):
                super(ReducedFocalLoss, self).__init__()
                self.alpha = alpha
                self.gamma = gamma
                self.reduction = reduction
                self.loss_weight = loss_weight
                self.threshold = threshold


            def forward(self,
                        pred,
                        target,
                        weight=None,
                        avg_factor=None,
                        reduction_override=None):
                """Forward function.
                Args:
                    pred (torch.Tensor): The prediction.
                    target (torch.Tensor): The learning target of the prediction
                        in gaussian distribution.
                    weight (torch.Tensor, optional): The weight of loss for each
                        prediction. Defaults to None.
                    avg_factor (int, optional): Average factor that is used to average
                        the loss. Defaults to None.
                    reduction_override (str, optional): The reduction method used to
                        override the original reduction method of the loss.
                        Defaults to None.
                """
                assert reduction_override in (None, 'none', 'mean', 'sum')
                reduction = (
                    reduction_override if reduction_override else self.reduction)
                loss_reg = self.loss_weight * reduced_focal_loss(
                    pred,
                    target,
                    threshold=self.threshold,
                    alpha=self.alpha,
                    gamma=self.gamma)
                
                if reduction == 'mean':
                    return loss_reg.mean()
                if reduction == 'sum':
                    return loss_reg.sum()

                return loss_reg


        @LOSSES.register_module()
        class EQLv2(torch.nn.Module):
            def __init__(self,
                    use_sigmoid=True,
                    reduction='mean',
                    class_weight=None,
                    loss_weight=1.0,
                    num_classes=len(train_dataset.categories),  # 1203 for lvis v1.0, 1230 for lvis v0.5
                    gamma=12,
                    mu=0.8,
                    alpha=4.0,
                    vis_grad=False):
                super().__init__()
                self.use_sigmoid = True
                self.reduction = reduction
                self.loss_weight = loss_weight
                self.class_weight = class_weight
                self.num_classes = num_classes
                self.group = True

                # cfg for eqlv2
                self.vis_grad = vis_grad
                self.gamma = gamma
                self.mu = mu
                self.alpha = alpha

                # initial variables
                self._pos_grad = None
                self._neg_grad = None
                self.pos_neg = None

                def _func(x, gamma, mu):
                    return 1 / (1 + torch.exp(-gamma * (x - mu)))
                self.map_func = partial(_func, gamma=self.gamma, mu=self.mu)
                # logger = get_root_logger()
                # logger.info(f"build EQL v2, gamma: {gamma}, mu: {mu}, alpha: {alpha}")

            def forward(self,
                        cls_score,
                        label,
                        weight=None,
                        avg_factor=None,
                        reduction_override=None,
                        **kwargs):
                self.n_i, self.n_c = cls_score.size()

                self.gt_classes = label
                self.pred_class_logits = cls_score

                def expand_label(pred, gt_classes):
                    target = pred.new_zeros(self.n_i, self.n_c)
                    target[torch.arange(self.n_i), gt_classes] = 1
                    return target

                target = expand_label(cls_score, label)
                pos_w, neg_w = self.get_weight(cls_score)
                weight = pos_w * target + neg_w * (1 - target)
                cls_loss = F.binary_cross_entropy_with_logits(cls_score, target,
                                                            reduction='none')
                cls_loss = torch.sum(cls_loss * weight) / self.n_i
                self.collect_grad(cls_score.detach(), target.detach(), weight.detach())
                return self.loss_weight * cls_loss

            def get_channel_num(self, num_classes):
                num_channel = num_classes + 1
                return num_channel

            def get_activation(self, cls_score):
                cls_score = torch.sigmoid(cls_score)
                n_i, n_c = cls_score.size()
                bg_score = cls_score[:, -1].view(n_i, 1)
                cls_score[:, :-1] *= (1 - bg_score)
                return cls_score

            def collect_grad(self, cls_score, target, weight):
                prob = torch.sigmoid(cls_score)
                grad = target * (prob - 1) + (1 - target) * prob
                grad = torch.abs(grad)

                # do not collect grad for objectiveness branch [:-1]
                pos_grad = torch.sum(grad * target * weight, dim=0)[:-1]
                neg_grad = torch.sum(grad * (1 - target) * weight, dim=0)[:-1]

                # dist.all_reduce(pos_grad)
                # dist.all_reduce(neg_grad)

                self._pos_grad += pos_grad
                self._neg_grad += neg_grad
                self.pos_neg = self._pos_grad / (self._neg_grad + 1e-10)

            def get_weight(self, cls_score):
                # we do not have information about pos grad and neg grad at beginning
                if self._pos_grad is None:
                    self._pos_grad = cls_score.new_zeros(self.num_classes)
                    self._neg_grad = cls_score.new_zeros(self.num_classes)
                    neg_w = cls_score.new_ones((self.n_i, self.n_c))
                    pos_w = cls_score.new_ones((self.n_i, self.n_c))
                else:
                    # the negative weight for objectiveness is always 1
                    neg_w = torch.cat([self.map_func(self.pos_neg), cls_score.new_ones(1)])
                    pos_w = 1 + self.alpha * (1 - neg_w)
                    neg_w = neg_w.view(1, -1).expand(self.n_i, self.n_c)
                    pos_w = pos_w.view(1, -1).expand(self.n_i, self.n_c)
                return pos_w, neg_w


    # based on learn.algorithms.BU_NLP.distributed_launch.torch_distributed_launch
    def torch_distributed_launch(self, nproc_per_node, training_script, training_script_args):
        args = {}

        # os.environ['CUDA_VISIBLE_DEVICES'] = ','.join([str(x) for x in self.device])
    
        os.environ['OMP_NUM_THREADS'] = "1"

        args["nnodes"] =  1
        args["node_rank"] = 0
        args["nproc_per_node"] = nproc_per_node
        args["master_addr"] = "127.0.0.2"
        args["master_port"] = self.config["port"]
        args["use_env"] = False
        args["module"] = False
        args["no_python"] = False
        args["training_script"] = training_script
        args["training_script_args"] = training_script_args
        
        # world size in terms of number of processes
        dist_world_size = args["nproc_per_node"] * args["nnodes"]

        # set PyTorch distributed related environmental variables
        current_env = os.environ.copy()
        current_env["MASTER_ADDR"] = args["master_addr"]
        current_env["MASTER_PORT"] = str(args["master_port"])
        current_env["WORLD_SIZE"] = str(dist_world_size)
        current_env["NGPU"] = str(args["nproc_per_node"])
        processes = []

        if 'OMP_NUM_THREADS' not in os.environ and args["nproc_per_node"] > 1:
            current_env["OMP_NUM_THREADS"] = str(1)
            print("*****************************************\n"
                "Setting OMP_NUM_THREADS environment variable for each process "
                "to be {} in default, to avoid your system being overloaded, "
                "please further tune the variable for optimal performance in "
                "your application as needed. \n"
                "*****************************************".format(current_env["OMP_NUM_THREADS"]))

        for local_rank in range(0, args["nproc_per_node"]):
            # each process's rank
            dist_rank = args["nproc_per_node"] * args["node_rank"] + local_rank
            current_env["RANK"] = str(dist_rank)
            current_env["LOCAL_RANK"] = str(local_rank)

            # spawn the processes
            with_python = not args["no_python"]
            cmd = []
            if with_python:
                cmd = [sys.executable, "-u"]
                if args["module"]:
                    cmd.append("-m")
            else:
                if not args["use_env"]:
                    raise ValueError("When using the '--no_python' flag, you must also set the '--use_env' flag.")
                if args["module"]:
                    raise ValueError("Don't use both the '--no_python' flag and the '--module' flag at the same time.")

            cmd.append(args["training_script"])

            if not args["use_env"]:
                cmd.append("--local_rank={}".format(local_rank))

            cmd.extend(args["training_script_args"])

            process = subprocess.Popen(cmd, env=current_env)
            processes.append(process)

            
        for process in processes:
            process.wait()
            if process.returncode != 0:
                raise subprocess.CalledProcessError(returncode=process.returncode,
                                                    cmd=[cmd])


