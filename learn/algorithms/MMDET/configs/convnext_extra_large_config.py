model = dict(
    type='CascadeRCNN',
    pretrained=None,
    backbone=dict(
        type='ConvNeXt',
        in_chans=3,
        depths=[3, 3, 27, 3],
        dims=[256, 512, 1024, 2048],
        drop_path_rate=0.8,
        layer_scale_init_value=1.0,
        out_indices=[0, 1, 2, 3]),
    neck=dict(
        type='FPN',
        in_channels=[256, 512, 1024, 2048],
        out_channels=256,
        num_outs=5),
    rpn_head=dict(
        type='RPNHead',
        in_channels=256,
        feat_channels=256,
        anchor_generator=dict(
            type='AnchorGenerator',
            scales=[8],
            ratios=[0.5, 1.0, 2.0],
            strides=[4, 8, 16, 32, 64]),
        bbox_coder=dict(
            type='DeltaXYWHBBoxCoder',
            target_means=[0.0, 0.0, 0.0, 0.0],
            target_stds=[1.0, 1.0, 1.0, 1.0]),
        loss_cls=dict(
            type='CrossEntropyLoss', use_sigmoid=True, loss_weight=1.0),
        loss_bbox=dict(
            type='SmoothL1Loss', beta=0.1111111111111111, loss_weight=1.0)),
    roi_head=dict(
        type='CascadeRoIHead',
        num_stages=3,
        stage_loss_weights=[1, 0.5, 0.25],
        bbox_roi_extractor=dict(
            type='SingleRoIExtractor',
            roi_layer=dict(type='RoIAlign', output_size=7, sampling_ratio=0),
            out_channels=256,
            featmap_strides=[4, 8, 16, 32]),
        bbox_head=[
            dict(
                type='ConvFCBBoxHead',
                num_shared_convs=4,
                num_shared_fcs=1,
                in_channels=256,
                conv_out_channels=256,
                fc_out_channels=1024,
                roi_feat_size=7,
                num_classes=80,
                bbox_coder=dict(
                    type='DeltaXYWHBBoxCoder',
                    target_means=[0.0, 0.0, 0.0, 0.0],
                    target_stds=[0.1, 0.1, 0.2, 0.2]),
                reg_class_agnostic=False,
                reg_decoded_bbox=True,
                norm_cfg=dict(type='SyncBN', requires_grad=True),
                # loss_cls=dict(
                #     type='CrossEntropyLoss',
                #     use_sigmoid=False,
                #     loss_weight=1.0),
                loss_cls=dict(
                    type='EQLv2', 
                    use_sigmoid=True,
                    reduction='mean',
                    class_weight=None,
                    loss_weight=2.0,
                    num_classes=62,  # 1203 for lvis v1.0, 1230 for lvis v0.5
                    gamma=12,
                    mu=0.8,
                    alpha=4.0,
                    vis_grad=False),
                loss_bbox=dict(type='GIoULoss', loss_weight=1.0)),
            dict(
                type='ConvFCBBoxHead',
                num_shared_convs=4,
                num_shared_fcs=1,
                in_channels=256,
                conv_out_channels=256,
                fc_out_channels=1024,
                roi_feat_size=7,
                num_classes=80,
                bbox_coder=dict(
                    type='DeltaXYWHBBoxCoder',
                    target_means=[0.0, 0.0, 0.0, 0.0],
                    target_stds=[0.05, 0.05, 0.1, 0.1]),
                reg_class_agnostic=False,
                reg_decoded_bbox=True,
                norm_cfg=dict(type='SyncBN', requires_grad=True),
                # loss_cls=dict(
                #     type='CrossEntropyLoss',
                #     use_sigmoid=False,
                #     loss_weight=1.0),
                loss_cls=dict(
                    type='EQLv2', 
                    use_sigmoid=True,
                    reduction='mean',
                    class_weight=None,
                    loss_weight=2.0,
                    num_classes=62,  # 1203 for lvis v1.0, 1230 for lvis v0.5
                    gamma=12,
                    mu=0.8,
                    alpha=4.0,
                    vis_grad=False),
                loss_bbox=dict(type='GIoULoss', loss_weight=1.0)),
            dict(
                type='ConvFCBBoxHead',
                num_shared_convs=4,
                num_shared_fcs=1,
                in_channels=256,
                conv_out_channels=256,
                fc_out_channels=1024,
                roi_feat_size=7,
                num_classes=80,
                bbox_coder=dict(
                    type='DeltaXYWHBBoxCoder',
                    target_means=[0.0, 0.0, 0.0, 0.0],
                    target_stds=[0.033, 0.033, 0.067, 0.067]),
                reg_class_agnostic=False,
                reg_decoded_bbox=True,
                norm_cfg=dict(type='SyncBN', requires_grad=True),
                # loss_cls=dict(
                #     type='CrossEntropyLoss',
                #     use_sigmoid=False,
                #     loss_weight=1.0),
                loss_cls=dict(
                    type='EQLv2', 
                    use_sigmoid=True,
                    reduction='mean',
                    class_weight=None,
                    loss_weight=2.0,
                    num_classes=62,  # 1203 for lvis v1.0, 1230 for lvis v0.5
                    gamma=12,
                    mu=0.8,
                    alpha=4.0,
                    vis_grad=False),
                loss_bbox=dict(type='GIoULoss', loss_weight=1.0))
        ]),
    train_cfg=dict(
        rpn=dict(
            assigner=dict(
                type='MaxIoUAssigner',
                pos_iou_thr=0.7,
                neg_iou_thr=0.3,
                min_pos_iou=0.3,
                match_low_quality=True,
                ignore_iof_thr=-1,
                gpu_assign_thr=200),
            sampler=dict(
                type='RandomSampler',
                num=256,
                pos_fraction=0.5,
                neg_pos_ub=-1,
                add_gt_as_proposals=False),
            allowed_border=0,
            pos_weight=-1,
            debug=False),
        rpn_proposal=dict(
            nms_across_levels=False,
            nms_pre=2000,
            nms_post=2000,
            max_per_img=2000,
            nms=dict(type='nms', iou_threshold=0.7),
            min_bbox_size=0),
        rcnn=[
            dict(
                assigner=dict(
                    type='MaxIoUAssigner',
                    pos_iou_thr=0.5,
                    neg_iou_thr=0.5,
                    min_pos_iou=0.5,
                    match_low_quality=False,
                    ignore_iof_thr=-1,
                    gpu_assign_thr=200),
                sampler=dict(
                    type='RandomSampler',
                    num=512,
                    pos_fraction=0.25,
                    neg_pos_ub=-1,
                    add_gt_as_proposals=True),
                pos_weight=-1,
                debug=False),
            dict(
                assigner=dict(
                    type='MaxIoUAssigner',
                    pos_iou_thr=0.6,
                    neg_iou_thr=0.6,
                    min_pos_iou=0.6,
                    match_low_quality=False,
                    ignore_iof_thr=-1,
                    gpu_assign_thr=200),
                sampler=dict(
                    type='RandomSampler',
                    num=512,
                    pos_fraction=0.25,
                    neg_pos_ub=-1,
                    add_gt_as_proposals=True),
                pos_weight=-1,
                debug=False),
            dict(
                assigner=dict(
                    type='MaxIoUAssigner',
                    pos_iou_thr=0.7,
                    neg_iou_thr=0.7,
                    min_pos_iou=0.7,
                    match_low_quality=False,
                    ignore_iof_thr=-1,
                    gpu_assign_thr=200),
                sampler=dict(
                    type='RandomSampler',
                    num=512,
                    pos_fraction=0.25,
                    neg_pos_ub=-1,
                    add_gt_as_proposals=True),
                pos_weight=-1,
                debug=False)
        ]),
    test_cfg=dict(
        rpn=dict(
            nms_across_levels=False,
            nms_pre=1000,
            nms_post=1000,
            max_per_img=1000,
            nms=dict(type='nms', iou_threshold=0.7),
            min_bbox_size=0),
        rcnn=dict(
            score_thr=0.05,
            nms=dict(type='nms', iou_threshold=0.5),
            max_per_img=100)))
dataset_type = 'CocoDataset'
data_root = '/data/users/sarah.brockman/lwll_datasets/external/xview_chipped_transition/xview_chipped_transition_full/train'
img_norm_cfg = dict(
    mean=[123.675, 116.28, 103.53], std=[58.395, 57.12, 57.375], to_rgb=True)
train_pipeline = [
    dict(type='LoadImageFromFile', to_float32=True),
    dict(type='LoadAnnotations', with_bbox=True, with_mask=False),
    dict(type='RandomFlip', flip_ratio=0.5),
    dict(type='PhotoMetricDistortion'),
    dict(
        type='AutoAugment',
        policies=[[{
            'type':
            'Resize',
            'img_scale': [(480, 1333), (512, 1333), (544, 1333), (576, 1333),
                          (608, 1333), (640, 1333), (672, 1333), (704, 1333),
                          (736, 1333), (768, 1333), (800, 1333)],
            'multiscale_mode':
            'value',
            'keep_ratio':
            True
        }],
                  [{
                      'type': 'Resize',
                      'img_scale': [(400, 1333), (500, 1333), (600, 1333)],
                      'multiscale_mode': 'value',
                      'keep_ratio': True
                  }, {
                      'type': 'RandomCrop',
                      'crop_type': 'absolute_range',
                      'crop_size': (384, 600),
                      'allow_negative_crop': True
                  }, {
                      'type':
                      'Resize',
                      'img_scale': [(480, 1333), (512, 1333), (544, 1333),
                                    (576, 1333), (608, 1333), (640, 1333),
                                    (672, 1333), (704, 1333), (736, 1333),
                                    (768, 1333), (800, 1333)],
                      'multiscale_mode':
                      'value',
                      'override':
                      True,
                      'keep_ratio':
                      True
                  }],
                  [
                    dict(
                        type='Rotate',
                        prob=0.5,
                        level=10)
                    ]
                  ]),
    dict(
        type='Normalize',
        mean=[123.675, 116.28, 103.53],
        std=[58.395, 57.12, 57.375],
        to_rgb=True),
    dict(type='Pad', size_divisor=32),
    dict(type='DefaultFormatBundle'),
    dict(type='Collect', keys=['img', 'gt_bboxes', 'gt_labels'])
]
test_pipeline = [
    dict(type='LoadImageFromFile', to_float32=True),
    dict(
        type='MultiScaleFlipAug',
        img_scale=(1333, 800),
        flip=False,
        transforms=[
            dict(type='Resize', keep_ratio=True),
            dict(type='RandomFlip'),
            dict(
                type='Normalize',
                mean=[123.675, 116.28, 103.53],
                std=[58.395, 57.12, 57.375],
                to_rgb=True),
            dict(type='Pad', size_divisor=32),
            dict(type='ImageToTensor', keys=['img']),
            dict(type='Collect', keys=['img'])
        ])
]
data = dict(
    samples_per_gpu=1,
    workers_per_gpu=2,
    train=dict(
        type='ClassBalancedDataset',
        oversample_thr=0.1,
        dataset=dict(
            type='CocoDataset',
            ann_file=
            '/data/users/sarah.brockman/convnext_extra_large_test/train_data_coco.json',
            img_prefix=
            '/data/users/sarah.brockman/lwll_datasets/external/xview_chipped_transition/xview_chipped_transition_full/train',
            pipeline=[
                dict(type='LoadImageFromFile', to_float32=True),
                dict(type='LoadAnnotations', with_bbox=True, with_mask=False),
                dict(type='RandomFlip', flip_ratio=0.5),
                dict(type='PhotoMetricDistortion'),
                dict(
                    type='AutoAugment',
                    policies=[[{
                        'type':
                        'Resize',
                        'img_scale': [(480, 1333), (512, 1333), (544, 1333),
                                      (576, 1333), (608, 1333), (640, 1333),
                                      (672, 1333), (704, 1333), (736, 1333),
                                      (768, 1333), (800, 1333)],
                        'multiscale_mode':
                        'value',
                        'keep_ratio':
                        True
                    }],
                              [{
                                  'type':
                                  'Resize',
                                  'img_scale': [(400, 1333), (500, 1333),
                                                (600, 1333)],
                                  'multiscale_mode':
                                  'value',
                                  'keep_ratio':
                                  True
                              }, {
                                  'type': 'RandomCrop',
                                  'crop_type': 'absolute_range',
                                  'crop_size': (384, 600),
                                  'allow_negative_crop': True
                              }, {
                                  'type':
                                  'Resize',
                                  'img_scale': [(480, 1333), (512, 1333),
                                                (544, 1333), (576, 1333),
                                                (608, 1333), (640, 1333),
                                                (672, 1333), (704, 1333),
                                                (736, 1333), (768, 1333),
                                                (800, 1333)],
                                  'multiscale_mode':
                                  'value',
                                  'override':
                                  True,
                                  'keep_ratio':
                                  True
                              }],
                              [
                                dict(
                                    type='Rotate',
                                    prob=0.5,
                                    level=10)
                                ]
                              ]),
                dict(
                    type='Normalize',
                    mean=[123.675, 116.28, 103.53],
                    std=[58.395, 57.12, 57.375],
                    to_rgb=True),
                dict(type='Pad', size_divisor=32),
                dict(type='DefaultFormatBundle'),
                dict(type='Collect', keys=['img', 'gt_bboxes', 'gt_labels'])
            ],
            data_root=
            '/data/users/sarah.brockman/lwll_datasets/external/xview_chipped_transition/xview_chipped_transition_full/train',
            classes=('Barge', 'Bus', 'Cargo Car', 'Cargo Plane', 'Cargo Truck',
                     'Cement Mixer', 'Container Crane', 'Container Ship',
                     'Crane Truck', 'Damaged Building', 'Dump Truck',
                     'Engineering Vehicle', 'Excavator', 'Ferry',
                     'Fishing Vessel', 'Fixed-wing Aircraft', 'Flat Car',
                     'Front loader/Bulldozer', 'Ground Grader', 'Haul Truck',
                     'Helicopter', 'Helipad', 'Hut/Tent', 'Locomotive',
                     'Maritime Vessel', 'Mobile Crane', 'Motorboat',
                     'Oil Tanker', 'Passenger Car', 'Passenger Vehicle',
                     'Pickup Truck', 'Pylon', 'Railway Vehicle',
                     'Reach Stacker', 'Sailboat', 'Scraper/Tractor', 'Shed',
                     'Shipping Container', 'Small Aircraft', 'Small Car',
                     'Storage Tank', 'Straddle Carrier', 'Tank car', 'Tower',
                     'Tower crane', 'Trailer', 'Truck', 'Truck Tractor',
                     'Truck w/Box', 'Truck w/Flatbed', 'Truck w/Liquid',
                     'Tugboat', 'Utility Truck', 'Yacht'))),
    val=dict(
        type='CocoDataset',
        ann_file=
        '/data/users/sarah.brockman/convnext_extra_large_test/train_data_coco.json',
        img_prefix=
        '/data/users/sarah.brockman/lwll_datasets/external/xview_chipped_transition/xview_chipped_transition_full/train',
        pipeline=[
            dict(type='LoadImageFromFile', to_float32=True),
            dict(
                type='MultiScaleFlipAug',
                img_scale=(1333, 800),
                flip=False,
                transforms=[
                    dict(type='Resize', keep_ratio=True),
                    dict(type='RandomFlip'),
                    dict(
                        type='Normalize',
                        mean=[123.675, 116.28, 103.53],
                        std=[58.395, 57.12, 57.375],
                        to_rgb=True),
                    dict(type='Pad', size_divisor=32),
                    dict(type='ImageToTensor', keys=['img']),
                    dict(type='Collect', keys=['img'])
                ])
        ],
        data_root=
        '/data/users/sarah.brockman/lwll_datasets/external/xview_chipped_transition/xview_chipped_transition_full/train',
        classes=('Barge', 'Bus', 'Cargo Car', 'Cargo Plane', 'Cargo Truck',
                 'Cement Mixer', 'Container Crane', 'Container Ship',
                 'Crane Truck', 'Damaged Building', 'Dump Truck',
                 'Engineering Vehicle', 'Excavator', 'Ferry', 'Fishing Vessel',
                 'Fixed-wing Aircraft', 'Flat Car', 'Front loader/Bulldozer',
                 'Ground Grader', 'Haul Truck', 'Helicopter', 'Helipad',
                 'Hut/Tent', 'Locomotive', 'Maritime Vessel', 'Mobile Crane',
                 'Motorboat', 'Oil Tanker', 'Passenger Car',
                 'Passenger Vehicle', 'Pickup Truck', 'Pylon',
                 'Railway Vehicle', 'Reach Stacker', 'Sailboat',
                 'Scraper/Tractor', 'Shed', 'Shipping Container',
                 'Small Aircraft', 'Small Car', 'Storage Tank',
                 'Straddle Carrier', 'Tank car', 'Tower', 'Tower crane',
                 'Trailer', 'Truck', 'Truck Tractor', 'Truck w/Box',
                 'Truck w/Flatbed', 'Truck w/Liquid', 'Tugboat',
                 'Utility Truck', 'Yacht')),
    test=dict(
        type='CocoDataset',
        ann_file=
        '/data/users/sarah.brockman/convnext_extra_large_test/train_data_coco.json',
        img_prefix=
        '/data/users/sarah.brockman/lwll_datasets/external/xview_chipped_transition/xview_chipped_transition_full/train',
        pipeline=[
            dict(type='LoadImageFromFile', to_float32=True),
            dict(
                type='MultiScaleFlipAug',
                img_scale=(1333, 800),
                flip=False,
                transforms=[
                    dict(type='Resize', keep_ratio=True),
                    dict(type='RandomFlip'),
                    dict(
                        type='Normalize',
                        mean=[123.675, 116.28, 103.53],
                        std=[58.395, 57.12, 57.375],
                        to_rgb=True),
                    dict(type='Pad', size_divisor=32),
                    dict(type='ImageToTensor', keys=['img']),
                    dict(type='Collect', keys=['img'])
                ])
        ],
        data_root=
        '/data/users/sarah.brockman/lwll_datasets/external/xview_chipped_transition/xview_chipped_transition_full/train',
        classes=('Barge', 'Bus', 'Cargo Car', 'Cargo Plane', 'Cargo Truck',
                 'Cement Mixer', 'Container Crane', 'Container Ship',
                 'Crane Truck', 'Damaged Building', 'Dump Truck',
                 'Engineering Vehicle', 'Excavator', 'Ferry', 'Fishing Vessel',
                 'Fixed-wing Aircraft', 'Flat Car', 'Front loader/Bulldozer',
                 'Ground Grader', 'Haul Truck', 'Helicopter', 'Helipad',
                 'Hut/Tent', 'Locomotive', 'Maritime Vessel', 'Mobile Crane',
                 'Motorboat', 'Oil Tanker', 'Passenger Car',
                 'Passenger Vehicle', 'Pickup Truck', 'Pylon',
                 'Railway Vehicle', 'Reach Stacker', 'Sailboat',
                 'Scraper/Tractor', 'Shed', 'Shipping Container',
                 'Small Aircraft', 'Small Car', 'Storage Tank',
                 'Straddle Carrier', 'Tank car', 'Tower', 'Tower crane',
                 'Trailer', 'Truck', 'Truck Tractor', 'Truck w/Box',
                 'Truck w/Flatbed', 'Truck w/Liquid', 'Tugboat',
                 'Utility Truck', 'Yacht')),
    ann_file=
    '/data/users/sarah.brockman/convnext_extra_large_test/train_data_coco.json'
)
evaluation = dict(metric=['bbox', 'segm'])
optimizer = dict(
    constructor='LearningRateDecayOptimizerConstructor',
    type='AdamW',
    lr=0.00025,
    betas=(0.9, 0.999),
    weight_decay=0.05,
    paramwise_cfg=dict(decay_rate=0.7, decay_type='layer_wise', num_layers=12))
optimizer_config = dict(grad_clip=None)
lr_config = dict(
    policy='step',
    warmup='linear',
    warmup_iters=1000,
    warmup_ratio=0.001,
    step=[15000])
runner = dict(type='IterBasedRunner', max_iters=20000)
checkpoint_config = dict(interval=30000)
log_config = dict(interval=50, hooks=[dict(type='TextLoggerHook')])
custom_hooks = [dict(type='NumClassCheckHook')]
dist_params = dict(backend='nccl')
log_level = 'INFO'
load_from = None
resume_from = None
workflow = [('train', 1)]
opencv_num_threads = 0
mp_start_method = 'fork'
auto_scale_lr = dict(enable=False, base_batch_size=16)
fp16 = dict(loss_scale=512.0)
classes = ('Barge', 'Bus', 'Cargo Car', 'Cargo Plane', 'Cargo Truck',
           'Cement Mixer', 'Container Crane', 'Container Ship', 'Crane Truck',
           'Damaged Building', 'Dump Truck', 'Engineering Vehicle',
           'Excavator', 'Ferry', 'Fishing Vessel', 'Fixed-wing Aircraft',
           'Flat Car', 'Front loader/Bulldozer', 'Ground Grader', 'Haul Truck',
           'Helicopter', 'Helipad', 'Hut/Tent', 'Locomotive',
           'Maritime Vessel', 'Mobile Crane', 'Motorboat', 'Oil Tanker',
           'Passenger Car', 'Passenger Vehicle', 'Pickup Truck', 'Pylon',
           'Railway Vehicle', 'Reach Stacker', 'Sailboat', 'Scraper/Tractor',
           'Shed', 'Shipping Container', 'Small Aircraft', 'Small Car',
           'Storage Tank', 'Straddle Carrier', 'Tank car', 'Tower',
           'Tower crane', 'Trailer', 'Truck', 'Truck Tractor', 'Truck w/Box',
           'Truck w/Flatbed', 'Truck w/Liquid', 'Tugboat', 'Utility Truck',
           'Yacht')
gpu_ids = [0, 1, 2, 3]
work_dir = '/data/users/sarah.brockman/convnext_extra_large_test'
