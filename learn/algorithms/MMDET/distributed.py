""" 
File to be executed by MMDET's object_detection.py for distributed training
"""

import os
import sys
import logging
logger = logging.getLogger(__name__)

import time
import datetime
import random

from learn.algorithms.MMDET.objectDetectionAdapter import ObjectDetectionAdapter
from learn.algorithms.FsDet.distributed import trainer_func
from learn.utils.wandb_utils import WANDB_INFO
from learn.utils.metrics import mean_average_precision, mAP

import learn.algorithms.MMDET.register_modules

import torch
import torch.nn.functional as F
import torch.distributed as dist
import numpy as np
from contextlib import contextmanager
import gc
import ubelt as ub
import copy
import pandas as pd
import yaml
import json
from hydra.utils import get_original_cwd
# from IPython import embed
from tempfile import TemporaryDirectory
import argparse
import psutil

import mmcv
from mmcv.runner import load_checkpoint, get_dist_info, init_dist
from mmcv import Config
import mmdet
from mmdet.datasets import build_dataset
from mmdet.models import build_detector
from mmdet.apis import train_detector
from mmdet.models.builder import LOSSES
from mmdet.datasets.builder import DATASETS
from mmdet.utils import collect_env

from functools import partial


torch.multiprocessing.set_start_method('spawn', force=True)
os.environ['OMP_NUM_THREADS'] = "1"


def parse_args():
    """
    Parse arguments for torch.distributed.launch function
    """
    parser = argparse.ArgumentParser(description='Train MMDET with DDP')
    # add more arguments here

    parser.add_argument(
        '--work_dir',
        type=str,
        default=None,
        help='the dir to save logs and models')
    parser.add_argument('--seed', type=int, default=None, help='random seed')
    parser.add_argument(
        '--gpus',
        type=int,
        default=1,
        help='number of gpus to use')
    parser.add_argument(
        '--model_checkpoint_file',
        type=str,
        default=None,
        help='file containing models checkpoint to load')
    parser.add_argument('--local_rank', type=int, default=0)
    parser.add_argument('--freeze', type=bool, default=False)
    args = parser.parse_args()
    if 'LOCAL_RANK' not in os.environ:
        os.environ['LOCAL_RANK'] = str(args.local_rank)

    return args


def trainer_func():
    args = parse_args()
    torch.cuda.set_device(args.local_rank)
    dist.init_process_group(backend="nccl", init_method='env://', rank=int(os.getenv("LOCAL_RANK")), world_size=args.gpus)
    filepath = args.work_dir

    # set random seed so both processes have the same model
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    torch.cuda.manual_seed_all(args.seed)

    print(f"reached trainer func with world size {dist.get_world_size()} and rank {dist.get_rank()}")

    cfg_pth = os.path.join(filepath, 'mmdet_config.py')
    mmdet_config = Config.fromfile(cfg_pth)

    meta = dict()
    env_info_dict = collect_env()
    env_info = '\n'.join([(f'{k}: {v}') for k, v in env_info_dict.items()])
    dash_line = '-' * 60 + '\n'
    logger.info('Environment info:\n' + dash_line + env_info + '\n' +
                dash_line)
    meta['env_info'] = env_info
    meta['config'] = mmdet_config.pretty_text
    meta['seed'] = args.seed

    num_classes = len(mmdet_config.classes)
    register_new_losses(num_classes)

    mmdet_config.seed = int(args.seed)
    mmdet_config.work_dir = filepath

    # print(torch.cuda.device_count(), os.getenv('CUDA_VISIBLE_DEVICES'), args.local_rank, type(args.local_rank))
    # torch.cuda.set_device(int(mmdet_config.gpu_ids[args.local_rank]))

    mmdet_config.gpu_ids = list([int(x) for x in range(dist.get_world_size())])

    datasets = [build_dataset(mmdet_config.data.train)]

    model = build_detector(
        mmdet_config.model, train_cfg=mmdet_config.get('train_cfg'), test_cfg=mmdet_config.get('test_cfg')
    )
    # call model.init_weights() here???

    # try:
    #     model.cuda()
    #     model.cpu()
    #     print("success rank ", dist.get_rank())
    # except:
    #     print("failed to load model onto gpu rank ", dist.get_rank())
    # exit()

    if args.model_checkpoint_file is not None:
        chkpt = load_checkpoint(model, args.model_checkpoint_file)

    model.CLASSES = datasets[0].CLASSES
    model.train()

    print("mmdet config in distributed:")
    print(f'Config:\n{mmdet_config.pretty_text}')

    if args.freeze:
        for n,p in model.named_parameters():
            if 'backbone' in n:
                p.requires_grad = False

    train_detector(model, datasets, mmdet_config, distributed=True, validate=False, meta=meta)

    print(f'finished training rank {dist.get_rank()}')
    gc.collect()
    torch.cuda.empty_cache()
    return 0



def register_new_losses(num_classes):
    # train_dataset = self.toolset["target_dataset"]

    LOSSES._module_dict.pop('ReducedFocalLoss', None)
    LOSSES._module_dict.pop('EQLv2', None)

    # https://github.com/facebookresearch/fvcore/blob/main/fvcore/nn/focal_loss.py
    def reduced_focal_loss(inputs, targets, threshold=0.5, alpha=0.25, gamma=2.0):
        
        targets = F.one_hot(targets, num_classes=inputs.shape[-1]).type_as(inputs)
        p = torch.sigmoid(inputs)
        ce_loss = F.binary_cross_entropy_with_logits(
            inputs, targets, reduction="none"
        )
        p_t = p * targets + (1 - p) * (1 - targets)
        ones_mask = p_t < threshold
        other_mask = p_t >= threshold
        other_mask = other_mask * (((1 - p_t) ** gamma) / threshold ** gamma)
        f_x = ones_mask + other_mask
        
        loss = ce_loss * f_x

        if alpha >= 0:
            alpha_t = alpha * targets + (1 - alpha) * (1 - targets)
            loss = alpha_t * loss

        return loss

    @LOSSES.register_module()
    class ReducedFocalLoss(torch.nn.Module):
        def __init__(self,
                    threshold=0.5,
                    gamma=2.0,
                    alpha=0.25,
                    reduction='mean',
                    loss_weight=1.0):
            super(ReducedFocalLoss, self).__init__()
            self.alpha = alpha
            self.gamma = gamma
            self.reduction = reduction
            self.loss_weight = loss_weight
            self.threshold = threshold


        def forward(self,
                    pred,
                    target,
                    weight=None,
                    avg_factor=None,
                    reduction_override=None):
            """Forward function.
            Args:
                pred (torch.Tensor): The prediction.
                target (torch.Tensor): The learning target of the prediction
                    in gaussian distribution.
                weight (torch.Tensor, optional): The weight of loss for each
                    prediction. Defaults to None.
                avg_factor (int, optional): Average factor that is used to average
                    the loss. Defaults to None.
                reduction_override (str, optional): The reduction method used to
                    override the original reduction method of the loss.
                    Defaults to None.
            """
            assert reduction_override in (None, 'none', 'mean', 'sum')
            reduction = (
                reduction_override if reduction_override else self.reduction)
            loss_reg = self.loss_weight * reduced_focal_loss(
                pred,
                target,
                threshold=self.threshold,
                alpha=self.alpha,
                gamma=self.gamma)
            
            if reduction == 'mean':
                return loss_reg.mean()
            if reduction == 'sum':
                return loss_reg.sum()

            return loss_reg


    @LOSSES.register_module()
    class EQLv2(torch.nn.Module):
        def __init__(self,
                use_sigmoid=True,
                reduction='mean',
                class_weight=None,
                loss_weight=1.0,
                num_classes=num_classes,  # 1203 for lvis v1.0, 1230 for lvis v0.5
                gamma=12,
                mu=0.8,
                alpha=4.0,
                vis_grad=False):
            super().__init__()
            self.use_sigmoid = True
            self.reduction = reduction
            self.loss_weight = loss_weight
            self.class_weight = class_weight
            self.num_classes = num_classes
            self.group = True

            # cfg for eqlv2
            self.vis_grad = vis_grad
            self.gamma = gamma
            self.mu = mu
            self.alpha = alpha

            # initial variables
            self._pos_grad = None
            self._neg_grad = None
            self.pos_neg = None

            def _func(x, gamma, mu):
                return 1 / (1 + torch.exp(-gamma * (x - mu)))
            self.map_func = partial(_func, gamma=self.gamma, mu=self.mu)
            # logger = get_root_logger()
            # logger.info(f"build EQL v2, gamma: {gamma}, mu: {mu}, alpha: {alpha}")

        def forward(self,
                    cls_score,
                    label,
                    weight=None,
                    avg_factor=None,
                    reduction_override=None,
                    **kwargs):
            self.n_i, self.n_c = cls_score.size()

            self.gt_classes = label
            self.pred_class_logits = cls_score

            def expand_label(pred, gt_classes):
                target = pred.new_zeros(self.n_i, self.n_c)
                target[torch.arange(self.n_i), gt_classes] = 1
                return target

            target = expand_label(cls_score, label)
            pos_w, neg_w = self.get_weight(cls_score)
            weight = pos_w * target + neg_w * (1 - target)
            cls_loss = F.binary_cross_entropy_with_logits(cls_score, target,
                                                        reduction='none')
            cls_loss = torch.sum(cls_loss * weight) / self.n_i
            self.collect_grad(cls_score.detach(), target.detach(), weight.detach())
            return self.loss_weight * cls_loss

        def get_channel_num(self, num_classes):
            num_channel = num_classes + 1
            return num_channel

        def get_activation(self, cls_score):
            cls_score = torch.sigmoid(cls_score)
            n_i, n_c = cls_score.size()
            bg_score = cls_score[:, -1].view(n_i, 1)
            cls_score[:, :-1] *= (1 - bg_score)
            return cls_score

        def collect_grad(self, cls_score, target, weight):
            prob = torch.sigmoid(cls_score)
            grad = target * (prob - 1) + (1 - target) * prob
            grad = torch.abs(grad)

            # do not collect grad for objectiveness branch [:-1]
            pos_grad = torch.sum(grad * target * weight, dim=0)[:-1]
            neg_grad = torch.sum(grad * (1 - target) * weight, dim=0)[:-1]

            # dist.all_reduce(pos_grad)
            # dist.all_reduce(neg_grad)

            self._pos_grad += pos_grad
            self._neg_grad += neg_grad
            self.pos_neg = self._pos_grad / (self._neg_grad + 1e-10)

        def get_weight(self, cls_score):
            # we do not have information about pos grad and neg grad at beginning
            if self._pos_grad is None:
                self._pos_grad = cls_score.new_zeros(self.num_classes)
                self._neg_grad = cls_score.new_zeros(self.num_classes)
                neg_w = cls_score.new_ones((self.n_i, self.n_c))
                pos_w = cls_score.new_ones((self.n_i, self.n_c))
            else:
                # the negative weight for objectiveness is always 1
                neg_w = torch.cat([self.map_func(self.pos_neg), cls_score.new_ones(1)])
                pos_w = 1 + self.alpha * (1 - neg_w)
                neg_w = neg_w.view(1, -1).expand(self.n_i, self.n_c)
                pos_w = pos_w.view(1, -1).expand(self.n_i, self.n_c)
            return pos_w, neg_w


if __name__=="__main__":
    print("reached main in distributed.py")
    trainer_func()
    print("finished trainer_func in main")
