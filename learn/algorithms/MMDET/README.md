# MMDET
This folder contains code for running [mmdetection](https://github.com/open-mmlab/mmdetection) within the learn framework.  

## Installation
MMdet and its requirements should be installed through the requirements.txt file in the top-level directory of the repo. Please follow the instructions there for setting up the learn conda environment. If the installation instructions there do not include mmdet and mmcv-full, they can be installed by the following:

```bash
MMCV_WITH_OPS=1 FORCE_CUDA=1 pip install mmcv-full==1.5.3 -f https://download.openmmlab.com/mmcv/dist/cu111/torch1.10.0/index.html
```

and 

```bash
pip install mmdet==2.25.0
```

## Using existing configuration files and checkpoints

There are some configuration files located in ```learn/algorithms/MMDET/configs```. To use one of these configs, set the ```mmdet_model_config_file``` parameter in the MMDET yaml config (```configs/hydra_config/object_detector/MMDET.yaml```) to the name of this file (just the filename, not the path, but be sure to include the .py extension). The appropriate dataset- and problem-specific changes will be made to the config file automatically.

There should be some mmdet models in your ```domain_network_selector.params.pretrained_network_dir``` directory. If not, see the next section for instructions on how to use a new checkpoint file. If you are using a checkpoint in ```domain_network_selector.params.pretrained_network_dir```, simply set the ```model_checkpoint_file``` parameter in the ```MMDET.yaml``` file to the name of the checkpoint file (exlude the full path, but be sure to include the .pth extension). 


## Using new configuration files and checkpoints

There are many model configurations available on the [MMDET github page](https://github.com/open-mmlab/mmdetection/tree/master/configs). To use new config files, it is easiest to clone the main mmdetection repo on your machine. Then, you can set the ```config_override``` parameter in ```MMDET.yaml``` to the *full path* to this config file. The necessary dataset- and problem-specific changes will be made.

When you use a new config, you should use the corresponding coco-pretrained backbone. These pretrained backbones can be found on the same page as the config file in the main mmdetection repo. In the readme, there should be a table under "pretrained models", with links to the configs and models. Download the model, and set the ```checkpoint_override``` parameter in ```MMDET.yaml``` to the *full path* to this file.


## Training

Example usage of running MMDET with hydra on poolcar dataset (make sure you are in top-level of learn repo):

```bash
python hydra_launcher.py \
    domain_network_selector=od_auto \
    "+wandb_params.name=mmdet poolcar" \
    object_detector=MMDET \
    problem.tasks_to_run=poolcar \
    problem.dataset_dir=/path/to/top_level/dataset/directory \
    domain_network_selector.params.find_source_data_method=coco2014 \
    problem.add_dataset_to_whitelist=[coco2014] \
    domain_network_selector.params.find_pretrained_method=retinanet_R_101_FPN_3x \
    domain_network_selector.params.pretrained_network_dir=/path/to/pretrained_networks/directory \
    self_supervision_pretrain=none \
    wandb_params.run_wandb=False \
    harness=local
```

MMDET is fastest when run in a distributed fashion. To prevent different MMDET runs on the same machine from sharing any GPUs, use the ```LWLL_TA1_GPUS``` environment variable. This environment variable works with and without slurm (*see note below).

Example usage:

```bash
LWLL_TA1_GPUS="2 3" python hydra_launcher.py ... object_detector.params.device=[0,1] 
```

In the above example, all processes will be placed on GPUs 2 and 3. The ```object_detector.params.device``` ordinals are "within" the devices listed by the ```LWLL_TA1_GPUS``` variable. Since we only have two GPUs here, a device ordinal in ```object_detector.params.device``` higher than 1 will cause an error.

*This environment variable overrides whatever GPUs (if any) are placed in ```CUDA_VISIBLE_DEVICES``` by slurm. For example, even if you request one GPU through slurm and set this variable to use three GPUs, the code will use three GPUs. Although the job is technically placed through slurm, this will cause additional load on the GPUs as more jobs will be able to be allocated on the same GPUs, perhaps unkowingly by other users.

Using ```CUDA_VISIBLE_DEVICES``` in the same manner as ```LWLL_TA1_GPUS``` does not have the same effect, and processes may be placed on the same GPU. In particular, the sub-processes launched for distributed training will always be launched on the first x GPUs.
