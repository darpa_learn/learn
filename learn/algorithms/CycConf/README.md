# Cycle-Inconsistency Learning for Robust Object Detection

## Environment
- CUDA 10.2

## Dependencies

Create virtual envs 
- `python3 -m pip install --user virtualenv`
- `python3 -m venv ttt`

Activate virtualenv.
- `source ttt/bin/activate`


Install dependencies.

- `pip install -r requirements.txt`

- Install Pytorch 1.6

`pip install torch torchvision`
- Install Detectron2

`python -m pip install detectron2 -f \
  https://dl.fbaipublicfiles.com/detectron2/wheels/cu102/torch1.6/index.html`

