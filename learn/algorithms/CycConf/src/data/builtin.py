import json
import os
import os.path as osp
from collections import defaultdict

from detectron2.data import DatasetCatalog
from .coco import register_coco_instances  # use customized data register

__all__ = [
    "register_all_bdd_tracking",
    "register_all_bdd100k",
    "get_sequences",
]


def load_json(filename):
    with open(filename, "r") as fp:
        reg_file = json.load(fp)
    return reg_file


# ==== Predefined datasets and splits for BDD100K ==========
_PREDEFINED_SPLITS_BDD = {
    "bdd100k": {
        "bdd100k_train": (
            "bdd100k/images/100k/train",  # path to training images
            "bdd100k/labels/bdd100k_labels_images_train_coco.json",  # path to training labels
        ),
        "bdd100k_val": (
            "bdd100k/images/100k/val",  # path to validation images
            "bdd100k/labels/bdd100k_labels_images_det_coco_val.json",  # path to validation labels
        ),
        "bdd100k_train_daytime": (
            "bdd100k/images/100k/train",  # path to training images
            "bdd100k/labels/domain_splits/bdd100k_ann_train_timeofday_daytime_coco.json",
        ),
        "bdd100k_train_night": (
            "bdd100k/images/100k/train",  # path to training images
            "bdd100k/labels/domain_splits/bdd100k_ann_train_timeofday_night_coco.json",
        ),
    },
}

# BDD100K MOT set domain splits.
_PREDEFINED_SPLITS_BDDT = {
    "bdd_tracking_2k": {
        "bdd_tracking_2k_train": (
            "bdd-tracking-2k/images/train",
            "bdd-tracking-2k/labels/bdd100k_mot_train_coco.json",
        ),
        "bdd_tracking_2k_val": (
            "bdd-tracking-2k/images/val",
            "bdd-tracking-2k/labels/bdd100k_mot_val_coco.json",
        ),
    },
    "bdd_tracking_2k_per_seq": {},
}

# Register data for different domains as well as different sequence.
domain_path = "bdd-tracking-2k/labels/domain_splits/"
train_splits = load_json(
    osp.join("datasets", domain_path, "bdd100k_mot_domain_splits_train.json")
)
val_splits = load_json(
    osp.join("datasets", domain_path, "bdd100k_mot_domain_splits_val.json")
)


# per_seq_{split}_{key}_{_attr}: [dataset_names]
per_seq_maps = defaultdict(list)

# register the per domain sets
for split, result in [("train", train_splits), ("val", val_splits)]:
    for key, values in result.items():
        # key is ["timeofday", "scene", "weather"]
        for attr, seqs in values.items():
            # attr is the actual attribute under each category like
            # `daytime`, `night`, etc. Values are list of sequence names.
            if "/" in attr or " " in attr:
                if "/" in attr:
                    _attr = attr.replace("/", "-")
                if " " in attr:
                    _attr = attr.replace(" ", "-")
            else:
                _attr = attr

            # register per domain values.
            _PREDEFINED_SPLITS_BDDT["bdd_tracking_2k"][
                "bdd_tracking_2k_{}_{}".format(split, _attr)
            ] = (
                "bdd-tracking-2k/images/{}".format(split),
                osp.join(
                    domain_path,
                    "labels",
                    split,
                    "{}_{}_{}_coco.json".format(split, key, _attr),
                ),
            )

            for seq in seqs:
                # register each sequence.
                _PREDEFINED_SPLITS_BDDT["bdd_tracking_2k_per_seq"][
                    "per_seq_{}_{}_{}".format(split, _attr, seq)
                ] = (
                    "bdd-tracking-2k/images/{}".format(split),
                    "bdd-tracking-2k/labels/per_seq/{}/{}_{}_coco/{}.json".format(
                        split, key, _attr, seq
                    ),
                )
                per_seq_maps["per_seq_{}_{}".format(split, _attr)] += [
                    "per_seq_{}_{}_{}".format(split, _attr, seq)
                ]


def register_all_bdd_tracking(root="datasets"):
    # bdd_tracking meta data
    # fmt: off
    thing_classes = ['pedestrian', 'rider', 'car', 'truck', 'bus', 
    'train', 'motorcycle', 'bicycle']
    thing_classes_3cls = ["vehicle", "pedestrian", "cyclist"]
    # fmt: on
    for DATASETS in [_PREDEFINED_SPLITS_BDDT]:
        for key, value in DATASETS.items():
            metadata = {
                "thing_classes": thing_classes_3cls
                if "3cls" in key
                else thing_classes
            }
            for name, (img_dir, label_file) in value.items():
                register_coco_instances(
                    name,
                    metadata,
                    os.path.join(root, label_file),
                    os.path.join(root, img_dir),
                )


def register_all_bdd100k(root="datasets"):
    # bdd100k meta data
    # fmt: off
    thing_classes = ['pedestrian', 'rider', 'car', 'truck', 'bus', 
    'train', 'motorcycle', 'bicycle', "traffic light", "traffic sign"]
    # fmt: on
    metadata = {"thing_classes": thing_classes}
    for _, value in _PREDEFINED_SPLITS_BDD.items():
        for name, (img_dir, label_file) in value.items():
            register_coco_instances(
                name,
                metadata,
                os.path.join(root, label_file),
                os.path.join(root, img_dir),
            )


def get_sequences(cfg):
    datasets = []
    for dataset_name in cfg.DATASETS.TEST:
        if dataset_name in per_seq_maps:
            datasets.extend(per_seq_maps[dataset_name])
        else:
            datasets.append(dataset_name)
    return datasets