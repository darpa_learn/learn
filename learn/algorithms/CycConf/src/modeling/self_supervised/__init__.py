from .build import SSHEAD_REGISTRY, build_ss_head
# import all the ss head, so they will be registered
from .cycle import CycleHead

#from .jigsaw import JigsawHead
#from .leftright import LeftRightHead
from .rotation import RotationHead
