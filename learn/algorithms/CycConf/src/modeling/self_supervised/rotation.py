import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from typing import Any, List, Sequence, Tuple, Union
from detectron2.structures import ImageList

from .build import SSHEAD_REGISTRY
from .ss_layers import Bottleneck, conv1x1, conv3x3


class RotationHead(nn.Module):
    def __init__(self, cfg, cin):
        super(RotationHead, self).__init__()

        # resnet config
        self.name = 'rot'
        self.input = 'images'
        self.device = torch.device(cfg.MODEL.DEVICE)
        norm_layer = nn.BatchNorm2d
        self._norm_layer = norm_layer
        self.dilation = 1
        self.groups = 1
        self.base_width = 64

        # hard code the task specific parameters in order to
        # support multi-tasking
        # self.crop_size = cfg.MODEL.SS.CROP_SIZE
        self.crop_size = 224
        # self.ratio = 2
        self.ratio = cfg.MODEL.SS.RATIO  # crop image ratio
        self.enable_batch = cfg.TTT.ENABLE_BATCH
        self.batch_size = cfg.TTT.BATCH_SIZE

        depth = cfg.MODEL.RESNETS.DEPTH
        stage_ids = {"res2": 0, "res3": 1, "res4": 2, "res5": 3}
        num_blocks_per_stage = {50: [3, 4, 6, 3], 101: [3, 4, 23, 3],
                                152: [3, 8, 36, 3]}[depth]
        self.start_stage = min(stage_ids[cfg.MODEL.SS.FEAT_LEVEL]+1, 3)
        self.inplanes = cin
        self.scale = cfg.MODEL.SS.LOSS_SCALE

        out_channels = self.inplanes

        for i in range(self.start_stage, 4):
            out_channels *= 2
            setattr(self, "layer{}".format(i),
                    self._make_layer(Bottleneck, out_channels//4,
                                     num_blocks_per_stage[i], stride=2))

        # num_classes = cfg.MODEL.SS.NUM_CLASSES
        num_classes = 4
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Linear(out_channels, num_classes)
        self.criterion = nn.CrossEntropyLoss()

        assert len(cfg.MODEL.PIXEL_MEAN) == len(cfg.MODEL.PIXEL_STD)
        num_channels = len(cfg.MODEL.PIXEL_MEAN)
        pixel_mean = torch.Tensor(cfg.MODEL.PIXEL_MEAN).to(self.device).view(num_channels, 1, 1)
        pixel_std = torch.Tensor(cfg.MODEL.PIXEL_STD).to(self.device).view(num_channels, 1, 1)
        self.normalizer = lambda x: (x - pixel_mean) / pixel_std

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out',
                                        nonlinearity='relu')
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def _make_layer(self, block, planes, blocks, stride=1, dilate=False):
        norm_layer = self._norm_layer
        downsample = None
        previous_dilation = self.dilation
        if dilate:
            self.dilation *= stride
            stride = 1
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                conv1x1(self.inplanes, planes * block.expansion, stride),
                norm_layer(planes * block.expansion),
            )

        layers = []
        layers.append(
            block(self.inplanes, planes, stride, downsample, self.groups,
                  self.base_width, previous_dilation, norm_layer))
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.inplanes, planes, groups=self.groups,
                                base_width=self.base_width,
                                dilation=self.dilation,
                                norm_layer=norm_layer))

        return nn.Sequential(*layers)

    def forward(self, batched_inputs, feat_base, feat_level):
        # print('input size: ', x.size())
        if self.enable_batch:
            count = self.batch_size // len(batched_inputs)
            xs, ys = [], []
            for _ in range(count):
                x, y = self.gen_ss_inputs(batched_inputs)
                xs.append(x)
                ys.append(y)
            x = torch.cat(xs, dim=0)
            y = torch.cat(ys, dim=0)
        else:
            x, y = self.gen_ss_inputs(batched_inputs)

        x = feat_base(x)[feat_level]
        for i in range(self.start_stage, 4):
            x = getattr(self, "layer{}".format(i))(x)

        x = self.avgpool(x)
        # print('feat size after avg pool: ', x.size())
        bs = x.size(0)
        x = x.squeeze()
        if bs == 1:
            x = x.unsqueeze(0)
        x = self.fc(x)
        # print('x: ', type(x), x.size())
        # print('y: ', type(y), y.size())
        loss = self.criterion(x, y.long())
        losses = {'loss_rot_cls': loss * self.scale}

        # print('ss pred accuracy: ',
        #       (x.argmax(axis=1)==y).float().mean().item()*100)
        return x, y, losses

    # add the data processing for each task
    def preprocess_image_ss(self, batched_inputs):
        """resize and random crop the images"""
        images = [x["image"].to(self.device) for x in batched_inputs]
        images = [self.normalizer(x) for x in images]
        images = MyImageList.from_tensors_crop(images, self.crop_size, self.ratio)
        return images

    def gen_ss_inputs(self, batched_inputs):
        """produce rotation targets"""
        images = self.preprocess_image_ss(batched_inputs=batched_inputs)
        tensors = images.tensor.clone().to(self.device)
        targets = torch.zeros(len(tensors)).long().to(self.device)
        for i in range(len(tensors)):
            tar = np.random.choice(4)
            targets[i] = tar
            t = images.tensor[i]
            rot = t.rot90(tar, (1, 2))
            tensors[i] = rot
        images.tensor = tensors
        return tensors, targets


@SSHEAD_REGISTRY.register()
def build_rotation_head(cfg, input_shape):
    in_channels = input_shape[cfg.MODEL.SS.FEAT_LEVEL].channels
    # print('in_channels: ', in_channels)
    rot_head = RotationHead(cfg, in_channels)
    return rot_head

class MyImageList(ImageList):
    """
    Structure that holds a list of images (of possibly
    varying sizes) as a single tensor.
    This works by padding the images to the same size,
    and storing in a field the original sizes of each image
    Attributes:
        image_sizes (list[tuple[int, int]]): each tuple is (h, w)
    """

    def __init__(self, tensor: torch.Tensor, image_sizes: List[Tuple[int, int]]):
        """
        Arguments:
            tensor (Tensor): of shape (N, H, W) or (N, C_1, ..., C_K, H, W) where K >= 1
            image_sizes (list[tuple[int, int]]): Each tuple is (h, w).
        """
        self.tensor = tensor
        self.image_sizes = image_sizes

    def __len__(self) -> int:
        return len(self.image_sizes)

    def __getitem__(self, idx: Union[int, slice]) -> torch.Tensor:
        """
        Access the individual image in its original size.
        Returns:
            Tensor: an image of shape (H, W) or (C_1, ..., C_K, H, W) where K >= 1
        """
        size = self.image_sizes[idx]
        return self.tensor[idx, ..., : size[0], : size[1]]  # type: ignore

    def to(self, *args: Any, **kwargs: Any) -> "ImageList":
        cast_tensor = self.tensor.to(*args, **kwargs)
        return ImageList(cast_tensor, self.image_sizes)

    @staticmethod
    def from_tensors(
        tensors: Sequence[torch.Tensor], size_divisibility: int = 0, pad_value: float = 0.0
    ) -> "ImageList":
        """
        Args:
            tensors: a tuple or list of `torch.Tensors`, each of shape (Hi, Wi) or
                (C_1, ..., C_K, Hi, Wi) where K >= 1. The Tensors will be padded with `pad_value`
                so that they will have the same shape.
            size_divisibility (int): If `size_divisibility > 0`, also adds padding to ensure
                the common height and width is divisible by `size_divisibility`
            pad_value (float): value to pad
        Returns:
            an `ImageList`.
        """
        assert len(tensors) > 0
        assert isinstance(tensors, (tuple, list))
        for t in tensors:
            assert isinstance(t, torch.Tensor), type(t)
            assert t.shape[1:-2] == tensors[0].shape[1:-2], t.shape
        # per dimension maximum (H, W) or (C_1, ..., C_K, H, W) where K >= 1 among all tensors
        max_size = tuple(max(s) for s in zip(*[img.shape for img in tensors]))

        if size_divisibility > 0:
            import math

            stride = size_divisibility
            max_size = list(max_size)  # type: ignore
            max_size[-2] = int(math.ceil(max_size[-2] / stride) * stride)  # type: ignore
            max_size[-1] = int(math.ceil(max_size[-1] / stride) * stride)  # type: ignore
            max_size = tuple(max_size)

        image_sizes = [im.shape[-2:] for im in tensors]

        if len(tensors) == 1:
            # This seems slightly (2%) faster.
            # TODO: check whether it's faster for multiple images as well
            image_size = image_sizes[0]
            padded = F.pad(
                tensors[0],
                [0, max_size[-1] - image_size[1], 0, max_size[-2] - image_size[0]],
                value=pad_value,
            )
            batched_imgs = padded.unsqueeze_(0)
        else:
            batch_shape = (len(tensors),) + max_size
            batched_imgs = tensors[0].new_full(batch_shape, pad_value)
            for img, pad_img in zip(tensors, batched_imgs):
                pad_img[..., : img.shape[-2], : img.shape[-1]].copy_(img)

        return ImageList(batched_imgs.contiguous(), image_sizes)

    @staticmethod
    def from_tensors_crop(
        tensors: Sequence[torch.Tensor], crop_size: int = 224, ratio: int=1
    ) -> "ImageList":
        """
        Args:
            tensors: a tuple or list of `torch.Tensors`, each of shape (Hi, Wi) or
                (C_1, ..., C_K, Hi, Wi) where K >= 1. The Tensors will be padded with `pad_value`
                so that they will have the same shape.
            size_divisibility (int): If `size_divisibility > 0`, also adds padding to ensure
                the common height and width is divisible by `size_divisibility`
            pad_value (float): value to pad
        Returns:
            an `ImageList`.
        """
        assert len(tensors) > 0
        assert isinstance(tensors, (tuple, list))
        for t in tensors:
            assert isinstance(t, torch.Tensor), type(t)
            assert t.shape[1:-2] == tensors[0].shape[1:-2], t.shape
        # per dimension maximum (H, W) or (C_1, ..., C_K, H, W) where
        # K >= 1 among all tensors
        max_size = tuple(max(s) for s in zip(*[img.shape for img in tensors]))

        image_sizes = [im.shape[-2:] for im in tensors]

        # resize the images to half size of the original size
        croped_tensors = torch.rand(len(tensors), tensors[0].size(0),
                                    crop_size, crop_size)

        new_image_sizes = []
        for i, tensor in enumerate(tensors):
            image_size = image_sizes[i]
            tensor = tensor.unsqueeze(1) # add the channel dimension here
            resized_image = F.interpolate(tensor, scale_factor=ratio).squeeze()
            crop_image = crop_tensor(resized_image, (crop_size, crop_size))
            croped_tensors[i] = crop_image
            new_image_sizes.append(crop_image.shape[-2:])

        return ImageList(croped_tensors.contiguous(), new_image_sizes)


def crop_tensor(image, crop_sizes):
    image = image.clone()
    indx = image.size(-2) - crop_sizes[0]
    indy = image.size(-1) - crop_sizes[1]
    if indx == 0:
        startx = 0
    else:
        startx = np.random.choice(indx)
    if indy == 0:
        starty = 0
    else:
        starty = np.random.choice(indy)
    return image[:, startx:startx+crop_sizes[0],
           starty:starty+crop_sizes[1]]
