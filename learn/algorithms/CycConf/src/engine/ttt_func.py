"""TTT Updater and Evaluator"""
import numpy as np
import torch

from detectron2.checkpoint import DetectionCheckpointer
from detectron2.engine import DefaultTrainer, SimpleTrainer
from detectron2.evaluation import DatasetEvaluator, print_csv_format
from detectron2.evaluation.evaluator import DatasetEvaluators, inference_context
from detectron2.utils import comm
from detectron2.utils.comm import get_world_size, is_main_process
from detectron2.utils.events import EventStorage
from detectron2.utils.logger import log_every_n_seconds, setup_logger

import contextlib
import datetime
import json
import logging
import os
import os.path as osp
import time
from collections import OrderedDict

try:
    _nullcontext = contextlib.nullcontext  # python 3.7+
except AttributeError:

    @contextlib.contextmanager
    def _nullcontext(enter_result=None):
        yield enter_result

__all__ = ['TTTUpdater', 'ttt_on_dataset']


class TTTUpdater():

    """Define a new trainer for ttt. The optimizer, training
    schedule will be different. 

    Since we will re-use the building functions such
    as build_optimizer and build_lr, etc, we will need to 
    update the cfg.SOLVER at for TTT if the hyper-parameters
    are different from training. 
    """

    def __init__(self, cfg, model, dataname):
        """ We directly pass the trained model to the updater,
        and reset it for each sequence
        """
        super().__init__(cfg)
        logger = logging.getLogger("TTT")
        # setup_logger is not called for d2
        if not logger.isEnabledFor(logging.INFO):
            setup_logger()

        self.model = model
        # build optimizer
        self.optimizer = DefaultTrainer.build_optimizer(cfg, model)
        # build scheduler
        self.scheduler = DefaultTrainer.build_lr_scheduler(cfg, self.optimizer)
        # save checkpoints
        self.checkpoint = DetectionCheckpointer(
            model,
            os.path.join(cfg.OUTPUT_DIR, dataname),
            optimizer=self.optimizer,
            scheduler=self.scheduler
        )
        self.start_iter = 0
        self.iter = self.start_iter
        self.max_iter = cfg.SOLVER.MAX_ITER
        self.cfg = cfg
        self.storage = EventStorage(self.start_iter)

    def run_step(self, data):
        """gradient step"""
        self.model.train()
        # use a different inference in the model design
        loss_dict = self.model.ss_forward(**data)
        losses = sum(loss_dict.values())
        self.optimizer.zero_grad()
        losses.backward()

        # use a new stream so the ops don't wait for DDP
        with torch.cuda.stream(
            torch.cuda.Stream()
        ) if losses.device.type == "cuda" else _nullcontext():
            metrics_dict = loss_dict
            self._write_metrics(metrics_dict)
            self._detect_anomaly(losses, loss_dict)

        self.optimizer.step()
        self.iter += 1
        self.model.eval()

    def get_model(self):
        return self.model

    def _write_metrics(self, metrics_dict: dict):
        """
        Args:
            metrics_dict (dict): dict of scalar metrics
        """
        metrics_dict = {
            k: v.detach().cpu().item() if isinstance(v, torch.Tensor) else float(v)
            for k, v in metrics_dict.items()
        }
        # gather metrics among all workers for logging
        # This assumes we do DDP-style training, which is currently the only
        # supported method in detectron2.
        all_metrics_dict = comm.gather(metrics_dict)

        if comm.is_main_process():
            if "data_time" in all_metrics_dict[0]:
                # data_time among workers can have high variance. The actual latency
                # caused by data_time is the maximum among workers.
                data_time = np.max([x.pop("data_time")
                                    for x in all_metrics_dict])
                self.storage.put_scalar("data_time", data_time)

            # average the rest metrics
            metrics_dict = {
                k: np.mean([x[k] for x in all_metrics_dict]) for k in all_metrics_dict[0].keys()
            }
            total_losses_reduced = sum(loss for loss in metrics_dict.values())

            self.storage.put_scalar("total_loss", total_losses_reduced)
            if len(metrics_dict) > 1:
                self.storage.put_scalars(**metrics_dict)

    def _detect_anomaly(self, losses, loss_dict):
        if not torch.isfinite(losses).all():
            raise FloatingPointError(
                "Loss became infinite or NaN at iteration={}!\nloss_dict = {}".format(
                    self.iter, loss_dict
                )
            )

    def select_pair(self, boxes):
        return boxes[-1]


def ttt_on_dataset(cfg, model, data_loader, evaluator, dataset_name):
    """
    test time training on each sequence.

    Args: 
        - add `cfg` to indicate configurations.
        - add `dataset_name` here to indicate evaluation directory
    """
    num_devices = get_world_size()
    logger = logging.getLogger(__name__)
    logger.info("Start test time training on {} images".format(len(data_loader)))

    total = len(data_loader)  # inference data loader must have a fixed length
    if evaluator is None:
        # create a no-op evaluator
        evaluator = DatasetEvaluators([])
    evaluator.reset()

    num_warmup = min(5, total - 1)
    start_time = time.perf_counter()
    total_compute_time = 0

    updater = TTTUpdater(cfg, model, dataset_name)

    # first inference to obtain proposals.

    boxes = []
    buffer = cfg.TTT.BUFFER
    for idx, inputs in enumerate(data_loader):
        if idx == num_warmup:
            start_time = time.perf_counter()
            total_compute_time = 0

        start_compute_time = time.perf_counter()

        if idx > 1 and idx % cfg.TTT.INTERVAL == 0:
            #print(inputs)
            #print(curr_box[0].size(), curr_box[1], len(curr_box[2]))
            model.train()
            for i in range(cfg.TTT.STEPS):
                outputs = model.get_proposals(inputs)
                assert isinstance(outputs, tuple), 'need to return box features'
                if torch.cuda.is_available():
                    torch.cuda.synchronize()
                _, curr_box = outputs
                prev_box = updater.select_pair(boxes)
                updater.run_step(inputs, curr_box, prev_box)
            model = updater.get_model()

        # re-evaluate the detection performance using the updated model.
        if model.training:
            model.eval()
        with inference_context(model), torch.no_grad():
            outputs = model.det_inference(inputs)
            if torch.cuda.is_available():
                torch.cuda.synchronize()
            assert isinstance(outputs, tuple), 'need to return box features'
            outputs, curr_box = outputs
            boxes.append(curr_box)
            if len(boxes) > buffer:
                boxes = boxes[-buffer:]

            total_compute_time += time.perf_counter() - start_compute_time
            #print(outputs[0]['instances'].pred_boxes.tensor.size())
            #print(inputs)
            evaluator.process(inputs, outputs)

            iters_after_start = idx + 1 - num_warmup * int(idx >= num_warmup)
            seconds_per_img = total_compute_time / iters_after_start
            if idx >= num_warmup * 2 or seconds_per_img > 5:
                total_seconds_per_img = (
                    time.perf_counter() - start_time) / iters_after_start
                eta = datetime.timedelta(seconds=int(
                    total_seconds_per_img * (total - idx - 1)))
                log_every_n_seconds(
                    logging.INFO,
                    "Inference done {}/{}. {:.4f} s / img. ETA={}".format(
                        idx + 1, total, seconds_per_img, str(eta)
                    ),
                    n=5,
                )


    # Measure the time only for this worker (before the synchronization barrier)
    total_time = time.perf_counter() - start_time
    total_time_str = str(datetime.timedelta(seconds=total_time))
    # NOTE this format is parsed by grep
    logger.info(
        "Total inference time: {} ({:.6f} s / img per device, on {} devices)".format(
            total_time_str, total_time / (total - num_warmup), num_devices
        )
    )
    total_compute_time_str = str(
        datetime.timedelta(seconds=int(total_compute_time)))
    logger.info(
        "Total inference pure compute time: {} ({:.6f} s / img per device, on {} devices)".format(
            total_compute_time_str, total_compute_time /
            (total - num_warmup), num_devices
        )
    )

    results = evaluator.evaluate()
    # An evaluator may return None when not in main process.
    # Replace it by an empty dict instead to make it easier for downstream code to handle
    if results is None:
        results = {}
    return results
