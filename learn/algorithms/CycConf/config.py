import scriptconfig as scfg


class Config(scfg.Config):
    """
    Default configuration for your algorithm

    Please add all of your default configuration parameters for your algorithm below
    with a description (help) on
        - what your parameter does
        - what changes when you use your parameter

    This is so that someone else can come and alter there parameters and know
    how this will affect the run of the algorithm

    """
    default = {
        'name': scfg.Value('CycConf',
                           help='name of the proposed method'),

        'batch_size': scfg.Value(8,
                                 help='batch size of network while running.  '
                                      'Increasing will do X and decrease will do Y'),

        'batch_size_per_image': scfg.Value(512,
                                 help='batch size in the ROI head for the number of'
                                      'detections'),

        'learning_rate': scfg.Value(0.00025,
                                 help=''),

        'num_workers': scfg.Value(8,
                                 help='number of workers in the dataloader'),

        'score_thresh_test': scfg.Value(1e-3,
                                 help='set the testing threshold for this model'
                                      'higher means more FP, lower means more FN'),

        'fsnet_maximum_iter': scfg.Value(10000,
                                  help='maximum number of iterations for training '
                                       'Higher will take longer but might be more '
                                       'accurate'),

        'cuda': scfg.Value(True,
                                  help='use cuda (if available'),


        'model_config_file': scfg.Value(
            "COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x.yaml",
                help='Default model for training and parameters.  This defines the '
                     'pretrained model parameter and setting'),

        'fsnet_freeze_backbone':  scfg.Value(False,
                help='Whether or not to freeze the backbone'),

        'device': scfg.Value(0,
                             help='what device to use (eg. 0 means gpu 0'),

    }