import scriptconfig as scfg


class Config(scfg.Config):
    """
    Default configuration for your algorithm

    Please add all of your default configuration parameters for your algorithm below
    with a description (help) on
        - what your parameter does
        - what changes when you use your parameter

    This is so that someone else can come and alter there parameters and know
    how this will affect the run of the algorithm
    """
    default = {
        # multi-gpu / multi-node
        'pretrain_on_source_target_train_steps': scfg.Value(50000, help='Number of training steps'),
        'checkpoint_config': scfg.Value(dict(
                # interval=100, # SET automatically to be `total_epochs` 'pretrain_on_source_target_train_steps'
                save_optimizer=False,
                max_keep_ckpts=1,
            ),
            help='When to save a checkpoint. must be divisible by total_epochs'),
        'total_epochs': scfg.Value(-1, help='number of "pre"training epochs'), # SET automatically using 'pretrain_on_source_target_train_steps'

        "train_script": scfg.Value("./learn/algorithms/HPT/OpenSelfSup/tools/train.py",
                            help="Script for distributed training."),
        "local_rank": scfg.Value(-1,
                            help="Multi-GPU - Local rank"),
        "master_port": scfg.Value(-1,
                            help="Master port (for multi-node SLURM jobs)"),
        'device': scfg.Value(-1,
                            help='what device to use (eg. 0 means gpu 0 and -1 means all available'),
        "dump_path": scfg.Value("./learn/algorithms/HPT/dumped/",
                            help="Experiment dump path"),
        "exp_name": scfg.Value("lwll_exp",
                            help="Experiment name"),
        "exp_id": scfg.Value("lwll_exp_id",
                            help="Experiment ID"),

        'model': scfg.Value(dict(
            type='MOCO',
            queue_len=65536,
            feat_dim=128,
            momentum=0.999,
            backbone=dict(
                type='ResNet',
                depth=50,
                in_channels=3,
                out_indices=[4],  # 0: conv-1, x: stage-x
                norm_cfg=dict(type='BN')
            ),
            neck=dict(
                type='NonLinearNeckV1',
                auto_channels=True,
                out_channels=128,
                with_avg_pool=True
            ),
            head=dict(type='ContrastiveHead', temperature=0.2)),
            help='Define the pretrain model'),
        'data': scfg.Value(dict(
            imgs_per_gpu=64,  # expected, e.g. 64*4=256
            workers_per_gpu=2,
            drop_last=True,
            train=dict(
                data_source=dict(
                    type="ImageList",
                    memcached=False,
                    mclient_path=""
                ),
                type='ContrastiveDataset',
                pipeline=[
                    dict(type='RandomResizedCrop', size=224, scale=(0.2, 1.)),
                    dict(
                        type='RandomAppliedTrans',
                        transforms=[
                            dict(
                                type='ColorJitter',
                                brightness=0.4,
                                contrast=0.4,
                                saturation=0.4,
                                hue=0.4)
                        ],
                        p=0.8),
                    dict(type='RandomGrayscale', p=0.2),
                    dict(
                        type='RandomAppliedTrans',
                        transforms=[
                            dict(
                                type='GaussianBlur',
                                sigma_min=0.1,
                                sigma_max=2.0)
                        ],
                        p=0.5
                    ),
                    dict(type='RandomHorizontalFlip'),
                    dict(type='ToTensor'),
                    # TODO(cjrd) should we normalize using the dataset values?
                    dict(type='Normalize', mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
                ]
            )
        )),
        # optimizer
        'optimizer': scfg.Value(dict(type='SGD', lr=0.03, weight_decay=0.0001, momentum=0.9)),
        'optimizer_config': scfg.Value(dict(), help="needed for OpenSelfSup initialization: this can be ignored"),
        'train_cfg': scfg.Value(dict(), help="needed for OpenSelfSup initialization: this can be ignored"),
        'test_cfg': scfg.Value(dict(), help="needed for OpenSelfSup initialization: this can be ignored"),
        'cudnn_benchmark': scfg.Value(True, help="whether to train in cudnn benchmark mode (faster but not reproducible)"),
        'load_from': scfg.Value(None, help='pretrained model for the pretraining: this is set dynamically in the code, so best to ignore here'),
        'resume_from': scfg.Value(None, help='useful to set if resuming from a previous pretraining state'),

        # learning policy
        'lr_config': scfg.Value(dict(policy='CosineAnnealing', min_lr=0.)),
        "seed": scfg.Value(0, help='Random seed'),
        "batch_size": scfg.Value(256, help='Batch size of the network'),
        "num_workers": scfg.Value(2, help='Number of workers needed for the dataloader'),
        "dist_params": scfg.Value(dict(backend='nccl'), help='Distributed backend'),
        "work_dir": scfg.Value('', help='Where the results will be stored; if blank, assume dump_path subdirectory'),
        'log_config': scfg.Value(dict(
            interval=20,
            hooks=[
                dict(type='TextLoggerHook'),
            ]
        ), help='The logging config for HPT subprocess (leave as TextLogger unless you know what you\'re doing)'),
        'log_level': scfg.Value('INFO', help='Log level of the HPT subprocess'),
        'workflow': scfg.Value([('train', 1),], help="HPT workflow, always set to default"),
    }
