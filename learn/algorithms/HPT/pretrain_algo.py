import json
import logging
import os
import shutil

import mmcv
import numpy as np
import torch
import torch.distributed as dist
import torch.nn as nn
import ubelt as ub
from learn.algorithms.BU_NLP.distributed_launch import torch_distributed_launch
from learn.algorithms.HPT.pretrain_adapter import PretrainAdapter
from learn.utils.dataset import ImageClassificationDataset
from learn.utils.memory import garbage_collection_cuda
from learn.utils.pretrained_networks import ImageClassificationNetwork
from mmcv.parallel import MMDistributedDataParallel
from mmcv.runner import DistSamplerSeedHook, EpochBasedRunner, obj_from_dict
from torch.utils.data import DataLoader, SequentialSampler, Subset
from torchvision import transforms

from OpenSelfSup.openselfsup.apis import train_model
from OpenSelfSup.openselfsup.apis.train import build_optimizer
from OpenSelfSup.openselfsup.hooks import DistOptimizerHook, build_hook
from OpenSelfSup.openselfsup.models import build_model

from hydra.utils import get_original_cwd

SUPPORTED_RESNET_DEPTHS = [18, 34, 50, 101, 152]


class PretrainAlgo(PretrainAdapter):
    def __init__(self, toolset):
        PretrainAdapter.__init__(self, toolset)
        self.config = toolset['protocol_config']['self_supervision_pretrain']['params']

    def initialize(self, initial_model, datasets, stage):
        """
        Initialize your algorithm and save any parameters or objects you want
        to be persistent between budget checkpoints.

        Args:
            initial_model: Pytorch network used as the initial model that the pretraining takes place on top of.
            datasets [ImageClassificationDataset]: Array of Pytorch dataset for the pretraining.
        """
        logging.info('Initializing HPT algorithm')
        self.datasets = datasets

        self.cached_path = None
        # used cached model path and don't bother doing HPT pretraining
        if self.config['cached_model_path']:
            cached_model_file_list = self.config['cached_model_path']
            if isinstance(self.config['cached_model_path'], str):
                cached_model_file_list = [self.config['cached_model_path']]
            if stage == 'base':
                cached_model_file = cached_model_file_list[0]
            elif stage == 'adapt' and len(cached_model_file_list) == 2:
                cached_model_file = cached_model_file_list[1]
            else:
                logging.info("Couldn't use cached HPT")
                cached_model_file = None

            if cached_model_file is not None and os.path.isfile(cached_model_file): 
                self.cached_path = self.config['cached_model_path']    
                logging.info(f"Using cached HPT: {self.cached_path}")

        if self.cached_path is None:
            # output the pretrain config here
            self.outdir = os.path.abspath(os.path.join(self.config['dump_path'], self.config['exp_name'], self.config['exp_id']))
            # TODO should this be a config option?
            # shutil.rmtree(self.outdir, ignore_errors=True)
            os.makedirs(self.outdir, exist_ok=True)

            # TODO should these filenames be customizable?
            self.output_config_fp = os.path.join(self.outdir, "config.json")
            self.output_data_list_fp = os.path.join(self.outdir, "data_list.txt")

            # output the data list file
            dct = 0
            with open(self.output_data_list_fp, "w") as ofile:
                for d in datasets:
                    for l in d.image_fnames:
                        dct += 1
                        ofile.write(str(d.root) + "/" + l + "\n")

            self.source_network_fp = os.path.join(self.outdir, "source_network.pth")
            logging.info(f'Saving current source model for additional pretraining')
            torch.save({'state_dict': self.toolset["source_network"].conv_params.state_dict()}, self.source_network_fp)

            #######################################################
            # update the config based on the incoming architecture
            #######################################################
            # TODO: make this dependent on LWLL_TA1_GPUS
            if isinstance(self.config['device'], int):
                if self.config['device'] == -1:
                    self.config['device'] = list(range(torch.cuda.device_count()))
                    n_gpus = torch.cuda.device_count()
                else:
                    n_gpus = 1
            else:
                n_gpus = len(self.config['device'])
            # update the number of pretraining epochs based on the number of steps
            batch_size = self.config['data']['imgs_per_gpu'] * n_gpus
            steps_per_epoch = dct // batch_size
            n_epochs = self.config['pretrain_on_source_target_train_steps'] // steps_per_epoch
            self.config['total_epochs'] = n_epochs
            self.config['checkpoint_config']['interval'] = n_epochs
            # set the LR for the given number of gpus
            lr = self.config['optimizer']['lr'] * n_gpus / 8.0
            self.config['optimizer']['lr'] = lr

            logging.info(f'HPT pretraining for {n_epochs} epochs on {dct} samples across {n_gpus} gpus with bsz {batch_size} and lr {lr}')

            self.config['model']['pretrained'] = self.source_network_fp
            self.config['data']['train']['data_source']["list_file"] = self.output_data_list_fp
            self.config['data']['train']['data_source']["root"] = ""

            classification_network_name = self.toolset['source_network'].model_name
            if classification_network_name[:6] == "resnet":
                network_type = "ResNet"
                network_depth = int(classification_network_name[6:])
                if network_depth not in SUPPORTED_RESNET_DEPTHS:
                    raise Exception(f"HPT currently only supports resnet depth: {SUPPORTED_RESNET_DEPTHS}, not {network_depth}")
            elif classification_network_name[:7] == "resnext":
                network_type = "ResNeXt"
                network_depth = int(classification_network_name[7:])
                if network_depth not in SUPPORTED_RESNET_DEPTHS:
                    raise Exception(f"HPT currently only supports resnext depth: {SUPPORTED_RESNET_DEPTHS}, not {network_depth}")
            else:
                network_type=classification_network_name
                network_depth=-1

            self.config['model']['backbone']['type'] = network_type
            self.config['model']['backbone']['depth'] = network_depth

            if not self.config["work_dir"]:
                self.config["work_dir"] = os.path.join(self.outdir, "work_dir")

            # This is a hack to get this to work. TODO: revisit this
            self.config['prefetch'] = False
            self.config['img_norm_cfg'] = None
            # output the config file
            with open(self.output_config_fp, "w") as ofile:
                json.dump(self.config, ofile, indent=4, sort_keys=True)

    def pretrain(self):
        """
        Perform HPT pretraining
        """
        if self.cached_path is not None:
            new_weights = torch.load(self.cached_path)
        else:
            logging.info(f'Running HPT pretrain algorithm')
            n_gpus = len(self.config['device'])
            torch_distributed_launch(n_gpus, 
                os.path.join(get_original_cwd(), self.config['train_script']),
                [self.output_config_fp, "--launcher", "pytorch"])

            logging.info(f'Loading new model weights and reapplying to source')
            new_weights = torch.load(os.path.join(self.config["work_dir"], "latest.pth"))
        new_weights = extract_backbone_from_OSS_model(new_weights['state_dict'])
        self.toolset['source_network'].conv_params.load_state_dict(new_weights)

        garbage_collection_cuda()
        logging.info(f'Done running HPT pretrain algorithm')

def extract_backbone_from_OSS_model(state_dict):
    output_dict = dict()
    for key, value in state_dict.items():
        if key.startswith('backbone'):
            output_dict[key[9:]] = value
    return output_dict
