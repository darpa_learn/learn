## Managing OpenSelfSup

OpenSelfSup library was originally added via the following commands:

```
git remote add openselfsup git@github.com:open-mmlab/OpenSelfSup.git
git fetch openselfsup
# change `master` as desired
git subtree add --prefix=learn/algorithms/HPT/OpenSelfSup openselfsup/master --squash
```

You can update OpenSelfSup with upstream changes as follows:

```
# change `master` as desired
git subtree pull --prefix=learn/algorithms/HPT/OpenSelfSup git@github.com:open-mmlab/OpenSelfSup.git master --squash
```

