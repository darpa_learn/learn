import torch
from learn.algorithms.VidWrapper.videoClassificationAdapter import VideoClassifierAdapter
import logging
from learn.deprecated_tinker.basealgorithm import BaseAlgorithm
from learn.deprecated_tinker.harness import Harness
from typing import Any, Dict
import learn.algorithms as algorithms_folder
from pathlib import Path
import os
log = logging.getLogger(__name__)
import sys
import inspect

def get_algorithm(algotype: str, toolset: Dict[str, Any]) -> BaseAlgorithm:
    """
    Load a single algorithm file and instantiate the relevant object therefrom.

    Arguments:
        algotype: (option 1) This is a string that contains either an absolute or relative path to the
                    desired algorithm file. If the path is relative, then the location of
                    the file is determined using the self.algorithmsbase directory and
                    appending the algotype string to it to generate the absolute file path.

        algotype: (option 2) This is a string that names a plugin that has been pip installed. This
                    function will attempt to load using the plugin if a file with a matching name to
                    algotype can't be found.

        toolset: This is a dict() containing the initialization information for the algorithm.

    Returns:
        a single instantiated object of the desired algorithm class.
    """
    # load the algorithm from the algorithmsbase/algotype

    # TODO: implement the mechanism to override the normal behavior of this
    # function and use it to create template algorithm files and adapters
    # instead.

    # Validate the toolset is a dictionary or None
    if toolset and not isinstance(toolset, dict):
        log.error("toolset must be a dictionary")
        raise TypeError("toolset must be a dictionary")

    algorithmsbase = str(Path(algorithms_folder.__file__).parent)
    # if the file exists, then load the algo from the file. If not, then
    # load the algo from plugin
    algofile = os.path.join(algorithmsbase, algotype)
    if os.path.exists(algofile) and not os.path.isdir(algofile):
        log.info(f"{algotype} found in algorithms path, loading file")
        return load_from_file(algofile, toolset)
    else: # plugin
        raise NotImplemnetedError

def load_from_file(algofile: str, toolset: Dict[str, Any]) -> BaseAlgorithm:
    """Load a protocol from a Python file."""

    # get the path to the algo file so that we can append it to the system path
    # this makes the import easier
    argpath, argfile = os.path.split(algofile)
    if argpath:
        sys.path.append(argpath)

    # load the file as a module and create an object of the class type in the file
    # the name of the class doesn't matter, as long as there is only one class in
    # the file.
    argbase, argext = os.path.splitext(argfile)
    if argext == ".py":
        algoimport = __import__(argbase, globals(), locals(), [], 0)
        for _name, obj in inspect.getmembers(algoimport):
            if inspect.isclass(obj):
                foo = inspect.getmodule(obj)
                if foo == algoimport:
                    # construct the algorithm object
                    algorithm = obj(toolset)
    else:
        log.error("Given algorithm is not a python file, other types not supported")
        raise AttributeError("Given algorithm is not a python file, other types not supported")

    return algorithm



class VideoClassifierAlgorithm(VideoClassifierAdapter):

    def __init__(self, toolset):
        # def __init__(self, problem, base_dataset, adapt_dataset, arguments):
        VideoClassifierAdapter.__init__(self, toolset)
        self.config = toolset['protocol_config']['video_classifier']['params']
        self.early_config = toolset['protocol_config']['video_classifier']['early']['params']
        self.late_config = toolset['protocol_config']['video_classifier']['late']['params']
        self.early_alg = get_algorithm(toolset['protocol_config']['video_classifier']['early']['name'], toolset)
        self.late_alg = get_algorithm(toolset['protocol_config']['video_classifier']['late']['name'], toolset)
        

    def initialize(self):
        """  Initialize your algorithm and save any parameters or objects you want
        to be persistent between checkpoints.

        """
        self.early_alg.execute(self.toolset, "Initialize")
        self.late_alg.execute(self.toolset, "Initialize")

    def domain_adapt_training(self):
        """ This is where you will fine-tune/train your few-shot or domain adaption
        algorithm.  You may use the dataloaders below and feel free to override the
        transformers in the datasets to use whatever pretraining you desire.
        Please do not modify the datasets though.
        """

        ckpt = self.toolset['ckpt']
        if ckpt == -1: # is UDA
            if self.config['uda'] == 'early':
                self.executing_early = True
            else:
                self.executing_early = False
        elif ckpt < self.config['early_ckpt_max']:
            self.executing_early = True
        else:
            self.executing_early = False
        
        
        if self.executing_early:
            self.early_alg.execute(self.toolset, "DomainAdaptTraining")
        else:
            self.late_alg.execute(self.toolset, "DomainAdaptTraining")

    def inference(self):
        """
        Inference is during the evaluation stage.  For this example, the
        task_network is trained in the train and adapt stage's code and
        is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to
        JPL

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        
        if self.executing_early:
            return self.early_alg.execute(self.toolset, "EvaluateOnTestDataSet")
        else:
            return self.late_alg.execute(self.toolset, "EvaluateOnTestDataSet") 
