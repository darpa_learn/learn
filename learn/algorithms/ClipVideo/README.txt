# these were the build steps I (Brandon) took get the environment correct.

conda create -n learn python=3.7 pytorch==1.9.1 torchvision==0.10.1 torchaudio==0.9.1 cudatoolkit=11.3 -c pytorch -c conda-forge
conda activate learn
pip install -r requirements.txt
git lfs install
cd learn/algorithms/
cd CycConf/
pip install -r requirements.txt && cd ..
cd HPT
pip install -r requirements.txt && cd ..
cd MAL
pip install -r requirements.txt && cd ..
cd SENTRY/
pip install -r requirements.txt && cd ..
cd TimeSformer/
pip install -r requirements.txt && cd ..
MMCV_WITH_OPS=1 FORCE_CUDA=1 pip install mmcv-full==1.5.3 -f https://download.openmmlab.com/mmcv/dist/cu111/torch1.9/index.html
pip install mmdet==2.25.0
cd ClipVideo/
pip install -r requirements.txt
