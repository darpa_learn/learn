import torch
import torch.nn as nn
import torchvision
from torchvision.transforms.transforms import Normalize

import os
import clip
import time
import datetime
import numpy as np
import copy
from learn.algorithms.ClipVideo.videoClassificationAdapter import (
    VideoClassifierAdapter,
)
import logging
from hydra.utils import get_original_cwd
import ubelt as ub
from models import xclip
from utils.logger import create_logger
from utils.optimizer import build_optimizer, build_scheduler
from utils.tools import AverageMeter, reduce_tensor, epoch_saving, auto_resume_helper, load_checkpoint, generate_text
import torch.distributed as dist
from timm.loss import LabelSmoothingCrossEntropy, SoftTargetCrossEntropy
from datasets.blending import CutmixMixupBlending
import pandas as pd
from utils.config import _C
from utils.sampling import CollateFn
from einops import rearrange
from learn.utils.wandb_utils import WANDB_INFO

def get_rank():
    if not dist.is_available():
        return 0
    if not dist.is_initialized():
        return 0
    return dist.get_rank()

def generate_text(categories):
    text_aug = f"{{}}"
    classes = torch.cat([clip.tokenize(text_aug.format(c), context_length=77) for c in categories])

    return classes

class VideoClassifierAlgorithm(VideoClassifierAdapter):
    def __init__(self, toolset):
        # def __init__(self, problem, base_dataset, adapt_dataset, arguments):
        VideoClassifierAdapter.__init__(self, toolset)
        self.config = toolset['protocol_config']['video_classifier']['params'] # /configs/hydra_config/video_classififer/ClipVideo.yaml

        self.xclip_cfg = _C.clone()
        param_list = []
        for k, v in self.config["xclip"].items():
            k = k.upper()
            if isinstance(v, dict):
                for sub_k, sub_v in v.items():
                    sub_k = sub_k.upper()
                    param_list.append(f"{k}.{sub_k}")
                    param_list.append(sub_v)
            else:
                param_list.append(k)
                param_list.append(v)
        self.xclip_cfg.merge_from_list(param_list)

        self.config["crop_size"] = self.xclip_cfg.DATA.INPUT_SIZE
        self.config["num_frames"] = self.xclip_cfg.DATA.NUM_FRAMES

    def initialize(self):
        """Initialize your algorithm and save any parameters or objects you want
        to be persistent between checkpoints.

        """
        config = self.xclip_cfg
        logger = create_logger(output_dir=config.OUTPUT, dist_rank=get_rank(), name=f"{config.MODEL.ARCH}")
        logger.info(f"working dir: {config.OUTPUT}")
        model, _ = xclip.load(config.MODEL.PRETRAINED, config.MODEL.ARCH, 
                        device="cpu", jit=False, 
                        T=config.DATA.NUM_FRAMES, 
                        droppath=config.MODEL.DROP_PATH_RATE, 
                        use_checkpoint=config.TRAIN.USE_CHECKPOINT, 
                        use_cache=config.MODEL.FIX_TEXT,
                        logger=logger,
                    )
        if self.config["pretrained_checkpoint"] is not None:
            state_dict = torch.hub.load_state_dict_from_url(self.config["pretrained_checkpoint"], map_location="cpu")
            msg = model.load_state_dict(state_dict["model"], strict=False)
            logger.info(msg)
        model = model.cuda()

        self.model = model
        self.logger = logger


    def get_dataloaders(self, dataset, collate_fn=None):
        """Get dataloaders

        VideoClassificationDataset is found in learn/utils/dataset.py

        Args:
            source_dataset (VideoClassificationDataset): source dataset
            target_dataset (VideoClassificationDataset): target dataset

        Returns:
            dataloaders for source and target

        """
        # Set dataset flag
        dataset.use_orginal_dataset = True
        class_names = dataset.categories
        logging.info('Creating dataloaders')

        labeled_dataloader = None
        if dataset.labeled_size > 0:
            labeled_dataloader = torch.utils.data.DataLoader(
                dataset,
                sampler=torch.utils.data.sampler.SubsetRandomSampler(
                    dataset.get_labeled_indices()
                ),
                batch_size=min(dataset.labeled_size, int(self.config["batch_size"])),
                num_workers=int(self.config['num_workers']),
                collate_fn=collate_fn,
                drop_last=False,
            )
        unlabeled_dataloader = None
        if dataset.unlabeled_size > 0:
            unlabeled_dataloader = torch.utils.data.DataLoader(
                dataset,
                sampler=torch.utils.data.sampler.SubsetRandomSampler(
                    dataset.get_unlabeled_indices()
                ),
                batch_size=min(dataset.unlabeled_size, int(self.config["batch_size"])),
                num_workers=int(self.config['num_workers']),
                collate_fn=collate_fn,
                drop_last=False,
            )

        return labeled_dataloader, unlabeled_dataloader, class_names

    def domain_adapt_training(self):
        """This is where you will fine-tune/train your few-shot or domain adaption
        algorithm.  You may use the dataloaders below and feel free to override the
        transformers in the datasets to use whatever pretraining you desire.
        Please do not modify the datasets though.
        """

        # ###################  Creating the Labeled DataLoader ###############
        # Create Dataloaders for labeled data by getting the labeled indices and
        # creating a random sub-sampler.  Then create the dataloader using this
        # sampler
        #
        #  Note: To change the transformer in the JPLDataset dataset, edit
        #      current_dataset.transform.  You can also edit the labels during
        #      loading by editing the  current_dataset.target_transform
        #
        #  Note: Another approach could be to inherit the JPLDataset class and
        #      override the __get_item__ function
        #
        #  Check out dataset.py for more information
        #     Notes: There could be NO unlabeled data and the algorithm has to be
        #         able to handle that case!
        #
        #         The min batch size here is so that the batch size isn't
        #         larger than the labeled/unlabeled dataset which causes the
        #         dataloader to hang

        original_target_transform = copy.deepcopy(self.toolset["target_dataset"].transform)
        self.toolset["target_dataset"].transform = torchvision.transforms.Compose(
            [
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(
                    [0.485, 0.456, 0.406], [0.229, 0.224, 0.225]
                ),
            ]
        )
        labeled_dataloader, _, class_names = self.get_dataloaders(
            self.toolset["target_dataset"],
            collate_fn=CollateFn(
                sampling_rate=self.config["sampling_rate"],
                num_frames=self.config["num_frames"],
                crop_size=self.config["crop_size"],
            ),
        )
        assert labeled_dataloader is not None, "Zero-shot is currently not supported"


        # ##################### fine-tune/train your model ######################
        #  Here is where you will actual finetune the model
        #  Make sure to set any persistent tensors to cpu with `tensor.cpu` command
        #  Save the model to a class variable (e.g. self.model) so we can use it for inference

        config = self.xclip_cfg
        logger = self.logger
        model = self.model

        mixup_fn = None
        if config.AUG.MIXUP > 0:
            criterion = SoftTargetCrossEntropy()
            mixup_fn = CutmixMixupBlending(num_classes=len(class_names), 
                                        smoothing=config.AUG.LABEL_SMOOTH, 
                                        mixup_alpha=config.AUG.MIXUP, 
                                        cutmix_alpha=config.AUG.CUTMIX, 
                                        switch_prob=config.AUG.MIXUP_SWITCH_PROB)
        elif config.AUG.LABEL_SMOOTH > 0:
            criterion = LabelSmoothingCrossEntropy(smoothing=config.AUG.LABEL_SMOOTH)
        else:
            criterion = nn.CrossEntropyLoss()
        
        optimizer = build_optimizer(config, model)
        lr_scheduler = build_scheduler(config, optimizer, len(labeled_dataloader))
        # model = torch.nn.parallel.DistributedDataParallel(model, device_ids=[config.LOCAL_RANK], broadcast_buffers=False, find_unused_parameters=False)
        model = nn.DataParallel(self.model, device_ids=self.config['device_ids'])

        start_epoch, max_accuracy = 0, 0.0

        if config.TRAIN.AUTO_RESUME:
            resume_file = auto_resume_helper(config.OUTPUT)
            if resume_file:
                config.defrost()
                config.MODEL.RESUME = resume_file
                config.freeze()
                logger.info(f'auto resuming from {resume_file}')
            else:
                logger.info(f'no checkpoint found in {config.OUTPUT}, ignoring auto resume')

        if config.MODEL.RESUME:
            start_epoch, max_accuracy = load_checkpoint(config, model.module, optimizer, lr_scheduler, logger)


        self.logger.info(f"Generating text labels for {len(class_names)} classes, {class_names}")
        text_labels = generate_text(class_names)

        for epoch in range(start_epoch, config.TRAIN.EPOCHS):
            self.train_one_epoch(epoch, model, criterion, optimizer, lr_scheduler, labeled_dataloader, text_labels, config, mixup_fn)
            if get_rank() == 0 and (epoch % config.SAVE_FREQ == 0 or epoch == (config.TRAIN.EPOCHS - 1)):
                epoch_saving(config, epoch, model.module, max_accuracy, optimizer, lr_scheduler, logger, config.OUTPUT, is_best=True)

        self.toolset["target_dataset"].transform = original_target_transform



    def inference(self, eval_dataset):
        """
        Inference is during the evaluation stage.  For this example, the
        task_network is trained in the train and adapt stage's code and
        is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to
        JPL

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """

        # use self.model for inference
        # model = self.model
        model = nn.DataParallel(self.model, device_ids=self.config['device_ids'])

        model.eval()

        original_eval_transform = copy.deepcopy(eval_dataset.transform)
        eval_dataset.transform = torchvision.transforms.Compose(
            [
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(
                    [0.45, 0.45, 0.45], [0.225, 0.225, 0.225]
                ),
            ]
        )
        preds = {}

        eval_start_time = time.time()
        for clip_idx in range(self.config["num_test_views"]):
            for spatial_idx in range(self.config["num_test_crops"]):
                if self.config["num_test_crops"] == 1:  # Sampling the middle one
                    spatial_idx = 1
                _, eval_dataloader, class_names = self.get_dataloaders(
                    eval_dataset,
                    collate_fn=CollateFn(
                        sampling_rate=self.config["sampling_rate"],
                        clip_idx=clip_idx,
                        num_clips=self.config["num_test_views"],
                        num_frames=self.config["num_frames"],
                        crop_size=self.config["crop_size"],
                        spatial_idx=spatial_idx,
                    ),
                )
                self.logger.info(f"Generating text labels for {len(class_names)} classes, {class_names}")
                text_labels = generate_text(class_names)
                text_inputs = text_labels.cuda()

                if text_inputs.shape[0] == 1:
                    text_inputs = text_inputs.view(1, -1)

                text_inputs = text_inputs.unsqueeze(0).expand(len(model.device_ids), -1, -1)

                for inputs, _, inds in eval_dataloader:
                    inputs = inputs.cuda()
                    inputs = rearrange(inputs, 'b c t h w -> b t c h w')

                    with torch.no_grad():
                        preds_ = model(inputs, text_inputs)

                    for indice, pred_ in zip(inds.numpy().tolist(), preds_.cpu()):
                        if indice not in preds:
                            preds[indice] = pred_
                            continue

                        if self.config["ensemble_method"] == "sum":
                            preds[indice] += pred_
                        elif self.config["ensemble_method"] == "max":
                            preds[indice] = torch.maximum(preds[indice], pred_)
                        else:
                            raise NotImplementedError

        indices, preds = list(zip(*preds.items()))
        preds = torch.stack(preds).argmax(1).numpy().tolist()
        eval_dataset.transform = original_eval_transform
        return preds, list(indices)

    def train_one_epoch(self, epoch, model, criterion, optimizer, lr_scheduler, train_loader, text_labels, config, mixup_fn):
        logger = self.logger

        model.train()
        optimizer.zero_grad()
        
        num_steps = len(train_loader)
        batch_time = AverageMeter()
        tot_loss_meter = AverageMeter()
        
        start = time.time()
        end = time.time()
        
        texts = text_labels.cuda(non_blocking=True)
        
        if texts.shape[0] == 1:
            texts = texts.view(1, -1)
        
        texts = texts.unsqueeze(0).expand(len(model.device_ids), -1, -1)
            
        for idx, (inputs, labels, _) in enumerate(train_loader):

            batch_data = dict(
                imgs=inputs,
                label=labels
            )

            images = batch_data["imgs"].cuda(non_blocking=True)
            label_id = batch_data["label"].cuda(non_blocking=True)
            label_id = label_id.reshape(-1)
            # reshape to [B, T, C, H, W]
            images = rearrange(images, 'b c t h w -> b t c h w')
            # images = images.view((-1,config.DATA.NUM_FRAMES,3)+images.size()[-2:])
            
            if mixup_fn is not None:
                images, label_id = mixup_fn(images, label_id)

            output = model(images, texts)

            total_loss = criterion(output, label_id)
            total_loss = total_loss / config.TRAIN.ACCUMULATION_STEPS

            if config.TRAIN.ACCUMULATION_STEPS == 1:
                optimizer.zero_grad()
            total_loss.backward()
            if config.TRAIN.ACCUMULATION_STEPS > 1:
                if (idx + 1) % config.TRAIN.ACCUMULATION_STEPS == 0:
                    optimizer.step()
                    optimizer.zero_grad()
                    lr_scheduler.step_update(epoch * num_steps + idx)
            else:
                optimizer.step()
                lr_scheduler.step_update(epoch * num_steps + idx)

            torch.cuda.synchronize()
            
            tot_loss_meter.update(total_loss.item(), len(label_id))
            batch_time.update(time.time() - end)
            end = time.time()

            if idx % config.PRINT_FREQ == 0:
                lr = optimizer.param_groups[0]['lr']
                memory_used = torch.cuda.max_memory_allocated() / (1024.0 * 1024.0)
                etas = batch_time.avg * (num_steps - idx)
                logger.info(
                    f'Train: [{epoch}/{config.TRAIN.EPOCHS}][{idx}/{num_steps}]\t'
                    f'eta {datetime.timedelta(seconds=int(etas))} lr {lr:.9f}\t'
                    f'time {batch_time.val:.4f} ({batch_time.avg:.4f})\t'
                    f'tot_loss {tot_loss_meter.val:.4f} ({tot_loss_meter.avg:.4f})\t'
                    f'mem {memory_used:.0f}MB')
                if WANDB_INFO.run_wandb:
                    metric_info = {
                        'lr': lr,
                        'num_steps': num_steps,
                        'batch_time.val': batch_time.val,
                        'batch_time.avg': batch_time.avg,
                        'tot_loss_meter.val': tot_loss_meter.val,
                        'tot_loss_meter.avg': tot_loss_meter.avg,
                        'mem': memory_used
                    }
                    WANDB_INFO.log_metrics(metric_info)
        epoch_time = time.time() - start
        logger.info(f"EPOCH {epoch} training takes {datetime.timedelta(seconds=int(epoch_time))}")
