import torch
from learn.utils.pretrained_networks import ImageClassificationNetwork
from collections import OrderedDict
import copy

pretraining_dataset = 'domain_net-real'
final_checkpoint_num = 115
model_name = 'densenet169'
final_checkpoint = f'output/pretraining/{pretraining_dataset}_{model_name}/' \
                   f'v7/v7_ckpt_epoch_{final_checkpoint_num}.ckpt'

final_checkpoint = f'output/pretraining/{pretraining_dataset}_{model_name}/' \
                   f'version_7/v7epoch={final_checkpoint_num}.ckpt'

final = torch.load(final_checkpoint, map_location='cpu')

best_fn = final['checkpoint_callback_best_model_path']
names = best_fn.split('/')
for idx, s in enumerate(names):
    if s == 'output':
        break
best_fn = '/'.join(names[idx:])
print(best_fn)

best = torch.load(best_fn, map_location='cpu')

del final, final_checkpoint

hp = best['hyper_parameters']
assert(hp['net']==model_name)
new_state_list = list()
for k, v in best['state_dict'].items():
    new_k = k.replace('backbone.','')
    new_state_list.append(
        (new_k, v)
    )

new_state_dict = OrderedDict(new_state_list)
model = ImageClassificationNetwork(pretraining_dataset, '',  model_name, False)
print(model.load_state_dict(new_state_dict))

fn = f'data/pretrained_networks/{pretraining_dataset}_{model_name}.pth'
torch.save(model.state_dict(), fn)

# m = torch.load('data/pretrained_networks/domain_net-quickdraw_resnet34.pth')

