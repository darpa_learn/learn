from copy import deepcopy

import torch
import torch.jit
import torch.nn as nn
import torch.utils.data as data
import torchvision.transforms as transforms
from pathlib import Path

from learn.algorithms.TENT.imageClassificationAdapter import ImageClassifierAdapter
#from learn.algorithms.pretrainImageClassification.config import Config
#from learn.algorithms.TENT.model_class import Pretrainer
from learn.utils.memory import garbage_collection_cuda

#import pytorch_lightning as pl
from pytorch_lightning import Trainer, seed_everything
#from learn.algorithms.pretrainImageClassification.early_stop import EarlyStopping
#from pytorch_lightning.loggers import TensorBoardLogger
#from pytorch_lightning.callbacks import ModelCheckpoint
import ubelt as ub
from learn.utils.wandb_utils import WANDB_INFO

@torch.jit.script
def softmax_entropy(x: torch.Tensor) -> torch.Tensor:
    """Entropy of softmax distribution from logits."""
    return -(x.softmax(1) * x.log_softmax(1)).sum(1)


@torch.enable_grad()  # ensure grads in possible no grad context for testing
def forward_and_adapt(x, pred_network, optimizer):
    """Forward and adapt model on batch of data.
    Measure entropy of the model prediction, take gradients, and update params.
    """
    # forward
    #output = G(x)
    #preds_ = F1(output)
    preds_ = pred_network(x)
    # adapt
    loss = softmax_entropy(preds_).mean(0) 
    if WANDB_INFO.run_wandb:
        WANDB_INFO.log_metrics({"tent/loss" : loss})
    loss.backward()
    optimizer.step()
    optimizer.zero_grad()
    return preds_


def collect_params(model):
    """Collect the affine scale + shift parameters from batch norms.
    Walk the model's modules and collect all batch normalization parameters.
    Return the parameters and their names.
    Note: other choices of parameterization are possible!
    """
    params = []
    # names = []
    for nm, m in model.named_modules():
        if isinstance(m, nn.BatchNorm2d):
            for np, p in m.named_parameters():
                if np in ['weight', 'bias']:  # weight is scale, bias is shift
                    params.append(p)
                    # names.append(f"{nm}.{np}")
    return params#, names


def check_model(model):
    """Check model for compatability with tent."""
    is_training = model.training
    assert is_training, "tent needs train mode: call model.train()"
    param_grads = [p.requires_grad for p in model.parameters()]
    has_any_params = any(param_grads)
    has_all_params = all(param_grads)
    assert has_any_params, "tent needs params to update: " \
                           "check which require grad"
    assert not has_all_params, "tent should not update all params: " \
                               "check which require grad"
    has_bn = any([isinstance(m, nn.BatchNorm2d) for m in model.modules()])
    assert has_bn, "tent needs normalization for its optimization"


class ImageClassifierAlgorithm(ImageClassifierAdapter):

    def __init__(self, toolset):
        # def __init__(self, problem, base_dataset, adapt_dataset, arguments):
        ImageClassifierAdapter.__init__(self, toolset)

        # Load the default configuration from config.py for your algorithm
        self.config = toolset['protocol_config']['test_time_adaptation']['params']

        # This sends the configuration back to the toolset
        self.toolset['classification_config'] = self.config

    def initialize(self, source_network, source_dataset, whitelist_datasets):
        """  Initialize your algorithm and save any parameters or objects you want to be persistent between
        budget checkpoints.  DomainNetworkSelection will choose a potential source datasets `source_dataset`
        and provide a trained network for that dataset `source_dataset.` Please do not modify the datasets
        beyond overriding the transforms (self.transform and self.transform_target are fine to change).

        Args:
            source_network (torch.nn.Module):  Pretrained network on source task
            source_dataset (framework.datasets.ImageClassificationDataset): Pytorch dataset for source
                domain/task
            whitelist_datasets (dict[str, framework.datasets.ImageClassificationDataset]): Dictionary of all
                possible external datasets which can be used on this task.  You may use this if you want to
                select a different dataset than the one selected as source.
        """
        seed_everything(self.config['seed'])

        device = self.config['device']
        if ub.iterable(device):
            self.device = device[0]
        else:
            self.device = device

    def set_augmentations(self, dataset, augmentation_style):
        """ Set the augmentations for a dataset

        Args:
            dataset (framework.datasets.ImageClassificationDataset):
            augmentation_style (str): either test or train

        Returns:

        """
        crop_size = 224
        if augmentation_style == 'test':
            dataset.transform = transforms.Compose([
                transforms.Resize(256),
                transforms.CenterCrop(crop_size),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        elif augmentation_style == 'train':
            dataset.transform = transforms.Compose([
                transforms.Resize(256),
                transforms.RandomHorizontalFlip(),
                transforms.RandomCrop(crop_size),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        else:
            raise NotImplementedError(f'"{augmentation_style}" augmentation style not implemented')

    def domain_adapt_training(self, target_dataset, eval_dataset):
        """ This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
        the dataloaders below and feel free to override the transformers in the datasets to use whatever
        pretraining you desire. Please do not modify the datasets though.

        Args:
            target_dataset (object): pytorch dataset for the target dataset used for training
            eval_dataset (object): eval dataset used for evaluation.  Included here in case you want to do
                transductive learning.
        """
        self.set_augmentations(target_dataset, 'train')
        self.set_augmentations(eval_dataset, 'test')
        '''
        hparams = self.config.to_dict()
        self.model = Pretrainer(hparams, G, train_dataset, test_dataset)

        early_stop = EarlyStopping(
            monitor='val/acc',
            patience=self.config['patience'],
            strict=False,
            verbose=True,
            mode='max'
        )

        logger = TensorBoardLogger(
            save_dir=self.config['output_dir'],
            version=7,
            name=f'{source_dataset.name}_{G.model_name}'
        )

        checkpoint_callback = ModelCheckpoint(
            filepath=logger.log_dir,
            save_top_k=5,
            verbose=True,
            monitor='val/acc',
            mode='max',
            prefix='v7'
        )

        # trainer = Trainer(gpus=self.device,
        #                   auto_lr_find=False,
        #                   auto_scale_batch_size=True,
        #                   use_amp=True,
        #                   early_stop_callback=early_stop,
        #                   logger=logger)
        #
        # trainer.fit(model)

        trainer = Trainer(gpus=self.device,
                          max_epochs=self.config['max_epochs'],
                          auto_lr_find=False,
                          auto_scale_batch_size=False,
                          use_amp=self.config['mixed_precision'],
                          early_stop_callback=early_stop,
                          logger=None,#logger,
                          check_val_every_n_epoch=1,
                          checkpoint_callback=None)#checkpoint_callback)

        trainer.fit(model)

        trainer.test(model)'''

    def inference(self, eval_dataset, pred_network):
        """
        Inference is during the evaluation stage.  For this example, the task_network is trained in the
        train and adapt stage's code and is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to JPL

        Args:
            eval_dataset (framework.datasets.ImageClassificationDataset): unlabeled evaluation framework.
                We need to predict the classes for each item in the dataset.

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        config = self.config

        # override transforms
        crop_size = 227
        if self.config['net'] != 'alexnet':
            crop_size = 224

        trans = transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(crop_size),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])

        eval_dataset.transform = trans
        device = self.device
        # G = self.G.eval().to(device)
        # train mode, because tent optimizes the model to minimize entropy
        pred_network = pred_network.train().to(device)
        # disable grad, to (re-)enable only what tent updates
        pred_network.requires_grad_(False)
        # configure norm for tent updates: enable grad + force batch statisics
        for m in pred_network.modules():
            if isinstance(m, nn.BatchNorm2d):
                m.requires_grad_(True)
                # force use of batch stats in train and eval modes
                m.track_running_stats = False
                m.running_mean = None
                m.running_var = None

        params = collect_params(pred_network)
        optimizer = torch.optim.Adam(params,
                   lr=self.config['tent_lr'],
                   weight_decay=self.config['tent_wd'])
        check_model(pred_network)

        eval_dataloader = data.DataLoader(eval_dataset,
            batch_size=int(self.config["batch_size"]),
            drop_last=False
        )
        preds = []
        indices = []
            
        #with torch.no_grad():
        for imgs, _, inds in eval_dataloader:
            imgs = imgs.to(device)

            for _ in range(self.config['tent_step']):
                preds_ = forward_and_adapt(imgs, pred_network, optimizer)

            if not config['bn_adapt_before']:  # adapt and predict at the same time
                preds += torch.argmax(preds_, dim=1).cpu().numpy().tolist()
                indices += inds.numpy().tolist()

        if config['bn_adapt_before']: # now predict after adapting
            for imgs, _, inds in eval_dataloader:
                imgs = imgs.to(device)
                preds_ = pred_network(imgs)
                preds += torch.argmax(preds_, dim=1).cpu().numpy().tolist()
                indices += inds.numpy().tolist()

        pred_network = pred_network.cpu()
        
        import gc
        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()
        # preds, indices = self.tools
        # et["eval_dataset"].dummy_data(
        #     'image_classification')
        return preds, indices

