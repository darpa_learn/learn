import abc
from learn.deprecated_tinker.basealgorithm import BaseAlgorithm


class ObjectDetectionAdapter(BaseAlgorithm):

    def __init__(self, toolset):
        BaseAlgorithm.__init__(self, toolset)

    def execute(self, toolset: dict, step_descriptor: str):
        """
        Execute function associated with the adapter

        Args:
            toolset (dict): A dictionary with experiment parameters
            step_descriptor (str): The name of different steps supported by adapter

        Returns:

        """
        self.toolset = toolset
        if step_descriptor == 'Initialize':
            return self.initialize(
                source_network=self.toolset["source_network"],
                source_dataset=self.toolset["source_dataset"],
                whitelist_datasets=self.toolset["whitelist_datasets"]
            )
        elif step_descriptor == 'DomainAdaptTraining':
            return self.domain_adapt_training(
                target_dataset=self.toolset["target_dataset"],
                eval_dataset=self.toolset["eval_dataset"]
            )
        elif step_descriptor == 'EvaluateOnTestDataSet':
            return self.inference(
                eval_dataset=self.toolset["eval_dataset"]
            )
        else:
            raise NotImplementedError(f'Step {step_descriptor} not implemented')

    @abc.abstractmethod
    def initialize(self, source_network, source_dataset, whitelist_datsets):
        raise NotImplementedError

    @abc.abstractmethod
    def domain_adapt_training(self, target_dataset, eval_dataset):
        raise NotImplementedError

    @abc.abstractmethod
    def inference(self, eval_dataset):
        raise NotImplementedError
