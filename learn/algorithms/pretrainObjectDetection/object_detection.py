""" Pretraining using dectectron2 for object detection in the learn task.

Look at the ObjectDetectionAlgorithm for more details on what you need to do to this
file.

"""

import os
import logging
import time
import datetime
import logging

from learn.algorithms.pretrainObjectDetection.objectDetectionAdapter import ObjectDetectionAdapter
from learn.algorithms.pretrainObjectDetection.config import Config
from learn.algorithms.pretrainObjectDetection.trainer import Trainer

import torch
import gc
import ubelt as ub
from contextlib import contextmanager
from pathlib import Path

from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.config import get_cfg
from detectron2 import model_zoo
from detectron2.utils.comm import get_world_size
from detectron2.utils.logger import log_every_n_seconds
from detectron2.engine import DefaultTrainer

def get_config(train_dataset_name, test_dataset_name, num_classes, config):
    """ Configuration for detectron.  This takes the configuration parameters
    from the config.py and sets them to the appropriate values in the detectron
    config.

    More info: https://detectron2.readthedocs.io/modules/config.html

    Args:
        train_dataset_name (str): name of the train dataset (used for identifying
            it later in the code
        test_dataset_name (str): name of the test dataset (used for identifying
            it later in the code
        num_classes (int): number of classes for the problem you are solving
        config (scriptconfig.Config): list of configuration values for the
            detectron system.  Defined in config.py

    Returns:
         detectron2 CfgNode instance

    """
    cfg = get_cfg()
    cfg.merge_from_file(model_zoo.get_config_file(config['model_config_file']))
    cfg.DATASETS.TRAIN = (train_dataset_name,)
    cfg.DATALOADER.NUM_WORKERS = config['num_workers']
    cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(
        config['model_config_file'])  # Let training initialize from model zoo
    cfg.SOLVER.IMS_PER_BATCH = config['batch_size']
    cfg.SOLVER.BASE_LR = config['learning_rate']  # pick a good LR
    cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = config[
        'batch_size_per_image']  # faster, and good enough for this toy dataset (default: 512)
    cfg.MODEL.ROI_HEADS.NUM_CLASSES = num_classes
    cfg.MODEL.RETINANET.NUM_CLASSES = num_classes
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = config[
        'score_thresh_test']  # set the testing threshold for this model
    cfg.DATASETS.TEST = (test_dataset_name,)
    # This does not work properly. The model is split between devices if the config
    # changes the device to another gpu
    # TODO figure out why the model is split
    cfg.MODEL.DEVICE = config['device']
    cfg.TEST.EVAL_PERIOD = config['eval_period']
    cfg.SOLVER.CHECKPOINT_PERIOD = config['checkpoint_period']
    return cfg


@contextmanager
def inference_context(model):
    """ A context where the model is temporarily changed to eval mode,
    and restored to previous mode afterwards.

    From: https://detectron2.readthedocs.io/_modules/detectron2/evaluation/evaluator.html#DatasetEvaluator

    Args:
        model: a detectron2 torch Module

    """
    training_mode = model.training
    model.eval()
    yield
    model.train(training_mode)
    model = model.cpu()


def inference_on_dataset(model, data_loader):
    """ Run model on the data_loader and returns results.

    Modified from: https://detectron2.readthedocs.io/_modules/detectron2/evaluation/evaluator.html#DatasetEvaluator

    Args:
        model (nn.Module): a module which accepts an object from
            `data_loader` and returns some outputs. It will be temporarily set to `eval` mode.

            If you wish to evaluate a model in `training` mode instead, you can
            wrap the given model and override its behavior of `.eval()` and `.train()`.
        data_loader: an iterable object with a length.
            The elements it generates will be the inputs to the model.

    Returns:
        The outputs from the network on the data in data_loader
    """
    num_devices = get_world_size()
    logger = logging.getLogger(__name__)
    logger.info("Start inference on {} images".format(len(data_loader)))

    total = len(data_loader)  # inference data loader must have a fixed length

    num_warmup = min(5, total - 1)
    start_time = time.perf_counter()
    total_compute_time = 0
    output = []
    with inference_context(model), torch.no_grad():
        for idx, inputs in enumerate(data_loader):
            if idx == num_warmup:
                start_time = time.perf_counter()
                total_compute_time = 0

            start_compute_time = time.perf_counter()
            outputs = model(inputs)
            if torch.cuda.is_available():
                torch.cuda.synchronize()
            total_compute_time += time.perf_counter() - start_compute_time

            iters_after_start = idx + 1 - num_warmup * int(idx >= num_warmup)
            seconds_per_img = total_compute_time / iters_after_start
            if idx >= num_warmup * 2 or seconds_per_img > 5:
                total_seconds_per_img = (
                                                time.perf_counter() - start_time) / iters_after_start
                eta = datetime.timedelta(
                    seconds=int(total_seconds_per_img * (total - idx - 1)))
                log_every_n_seconds(
                    logging.INFO,
                    "Inference done {}/{}. {:.4f} s / img. ETA={}".format(
                        idx + 1, total, seconds_per_img, str(eta)
                    ),
                    n=5,
                )
            for i, _input in enumerate(inputs):
                outputs[i]['image_id'] = _input['image_id']
                outputs[i]['instances'] = outputs[i]['instances'].to('cpu')
                output.append(outputs[i])

    # Measure the time only for this worker (before the synchronization barrier)
    total_time = time.perf_counter() - start_time
    total_time_str = str(datetime.timedelta(seconds=total_time))
    # NOTE this format is parsed by grep
    logger.info(
        "Total inference time: {} ({:.6f} s / img per device, on {} devices)".format(
            total_time_str, total_time / (total - num_warmup), num_devices
        )
    )
    total_compute_time_str = str(datetime.timedelta(seconds=int(total_compute_time)))
    logger.info(
        "Total inference pure compute time: {} ({:.6f} s / img per device, on {} devices)".format(
            total_compute_time_str, total_compute_time / (total - num_warmup),
            num_devices
        )
    )
    return output


class ObjectDetectionAlgorithm(ObjectDetectionAdapter):
    """  Object detection algorithm class.  you need to fill out the initialization
    domain_adapt_training, and inference functions.  Each function has a description
    on what you need to do in that function.

    Please remember to free GPU memory at the end of each function

    """

    def __init__(self, toolset):
        """
            First initialization of the function.  This sets the toolset and
            loads the config.  The algorithm initialization is separate so that
            the protocol can change the defaults before your algorithm loads.
            Most likely only Kitware will be editing this.

            Args:
                toolset (dict): dictionary of functions, datasets, and configs
        """
        ObjectDetectionAdapter.__init__(self, toolset)
        # Load the default configuration from config.py for your algorithm
        self.config = Config()
        self.update_parameters(toolset['protocol_config'])

        # This sends the configuration back to the toolset
        self.toolset['object_detector_config'] = self.config

    def update_parameters(self, protocol_config):
        """  Update parameters for algorithm based on protocol config

        Args:
            protocol_config (dict):  Dictionary of parameters.

        Returns:
            None
        """
        common_keyword = set(protocol_config.keys()) & set(self.config.keys())
        for ck in common_keyword:
            self.config[ck] = protocol_config[ck]


    def initialize(self, source_network, source_dataset, whitelist_datasets):
        """
        Initialize your algorithm and save any parameters or objects you want to be persistent between
        budget checkpoints.  DomainNetworkSelection will choose a potential source datasets `source_dataset`
        and provide a trained network for that dataset `source_dataset.` Please do not modify the datasets
        beyond overriding the transforms (self.transform and self.transform_target are fine to change).
        Args:
            source_network (torch.nn.Module):  Pretrained network on source task
            source_dataset (framework.datasets.ImageClassificationDataset): Pytorch dataset for source
                domain/task
            whitelist_datasets (dict[str, framework.datasets.ImageClassificationDataset]): Dictionary of all
                possible external datasets which can be used on this task.  You may use this if you want to
                select a different dataset than the one selected as source.

        """
        # pre-trained network on source task (the closest task)
        #  You are not required to use it but strongly suggested to use it
        #  These are in the style of torchvision models
        # TODO figure out how to work this into detectron
        self.model = source_network

        # Register the dataset in the backend
        train_dataset = source_dataset
        test_dataset = whitelist_datasets[f'{train_dataset.name}_test']

        if train_dataset.name == "coco2014":
            train_dataset_name = "coco_2017_train"
            test_dataset_name = "coco_2017_val"
        else:
            train_dataset_name = f'{train_dataset.name}_train'
            test_dataset_name = f'{test_dataset.name}_test'

        self.train_dataset_name = train_dataset_name
        self.test_dataset_name = test_dataset_name
        self.toolset["target_dataset"] = train_dataset
        self.toolset["eval_dataset"] = test_dataset
        current_detectron_dataset = DatasetCatalog.list()
        # make sure datasets aren't already registered
        if train_dataset_name not in current_detectron_dataset:
            DatasetCatalog.register(train_dataset_name,
                                    lambda d=train_dataset: self.transform_dataset(d))
            # Even if they have the same name, best to make sure they have the right number of categories
            MetadataCatalog.get(train_dataset_name).thing_classes = \
                    train_dataset.categories.tolist()
        if test_dataset_name not in current_detectron_dataset:
            DatasetCatalog.register(test_dataset_name,
                                    lambda d=test_dataset: self.transform_dataset(d))
            # Even if they have the same name, best to make sure they have the right number of categories
            MetadataCatalog.get(test_dataset_name).thing_classes = \
                    test_dataset.categories.tolist()

        if len(MetadataCatalog.get(train_dataset_name).thing_classes) != \
                len(train_dataset.categories):
            raise RuntimeError("Mismatch in number of training categories")
        if len(MetadataCatalog.get(test_dataset_name).thing_classes) != \
                len(test_dataset.categories):
            raise RuntimeError("Mismatch in number of testing categories")
        num_cats = len(train_dataset.categories)
        # Get the config
        self.cfg = get_config(train_dataset_name,
                              test_dataset_name,
                              num_cats,
                              self.config)
        # Transform our dataset to their type of dataset
        #  Need to do this each budget level time to make sure all labels from
        #  active learning
        self.cfg.OUTPUT_DIR = str(Path(f'./{train_dataset_name}_{self.config["name"]}'))
        os.makedirs(self.cfg.OUTPUT_DIR, exist_ok=True)
        if "imagenet_bbox" in train_dataset_name:
            self.trainer = DefaultTrainer(self.cfg)
        else:
            self.trainer = Trainer(self.cfg)
        self.trainer.resume_or_load(resume=False)
        self.trainer.train()

        # make sure the set your model to GPU if you want to save it since it is
        # taking up a lot of gpu memory that other approaches need to run
        self.trainer.model = self.trainer.model.cpu()
        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()
        exit(0)

    def domain_adapt_training(self, target_dataset, eval_dataset):
        """ Train your algorithm to adapt to the target_dataset.  This will be called
        multiple times, each with more labels in the target dataset labeled.
        Make sure to set your model to cpu() before the end of the function.  Also,
        clear the cuda cache (as shown below).

        You may save your network weight between the calls (but keep it in CPU)
        """
        pass


    def inference(self, eval_dataset):
        """
        Inference is during the evaluation stage.  Predict all images on the eval
        dataset (as the example below is doing).

        Returns:
            tuple(list[tuple],list[int]): preds and indices
                predicted category indices and image indices

        """
        model = self.trainer.model.cuda()
        data_loader = self.trainer.build_test_loader(self.cfg,
                self.test_dataset_name)

        # predictor = DefaultPredictor(self.cfg)
        outputs = inference_on_dataset(model, data_loader)
        count = 0
        indices = []
        bbox = []
        conf = []
        classes = []
        for o in outputs:
            count += len(o['instances'])
            if len(o['instances']) > 0:
                for i in range(len(o['instances'])):
                    indices.append(o['image_id'])
                    bbox_loc = map(str,
                                   o['instances'].pred_boxes.tensor[
                                       i].int().tolist())
                    bbox.append(', '.join(list(bbox_loc)))
                    conf.append(o['instances'].scores[i].item())
                    classes.append(o['instances'].pred_classes[i].item())
        preds = (bbox, conf, classes)

        # Random response useful to debugging to make sure you have the correct
        #    format.
        # preds2, indices2 = self.toolset["eval_dataset"].dummy_data(
        #    'object_detection')

        self.trainer.model = self.trainer.model.cpu()
        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()
        return preds, indices

    @staticmethod
    def transform_dataset(dataset):
        """ Transform our datasets into the coco format which can be then be registered
        into the dectectron system.  For now we are only training it but we might
        want to create a validation dataset to test on as well

        Args:
            dataset (framework.dataset):  Dataset from framework

        Returns
            dict in coco format.
        """

        # ######################### Get the code ready for detectron
        # Convert our dataset
        logging.info(f'Converting {dataset.name} to coco dataset')
        from detectron2.structures import BoxMode
        from detectron2.data.detection_utils import read_image
        from PIL import Image

        num_images = dataset.num_images
        dataset_dict = []
        for index in ub.ProgIter(range(num_images)):
            record = dict()
            record['file_name'] = str(dataset.root) + '/' + dataset.image_fnames[index]
            img = Image.fromarray(read_image(record['file_name']))
            record['width'], record['height'] = img.size
            record['image_id'] = index
            record['annotations'] = list()
            targets = dataset.targets[index]
            if targets is not None:
                for target in targets:
                    record['annotations'].append(dict({
                        'bbox': target['bbox'].tolist(),
                        'bbox_mode': BoxMode.XYXY_ABS,
                        'category_id': int(target['category'])
                    }))
            dataset_dict.append(record)

        return dataset_dict
