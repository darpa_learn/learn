import ubelt as ub
import logging
import torch
import pandas as pd
from typing import Any, Tuple, List, Dict

from learn.utils.memory import garbage_collection_cuda
from learn.algorithms.templateNLP.machineTranslationAdapter import MachineTranslationAdapter

import scriptconfig as scfg
from pathlib import Path


class MachineTranslationAlgorithm(MachineTranslationAdapter):
    """

    """

    def __init__(self, toolset: Dict[str, Any]) -> None:
        MachineTranslationAdapter.__init__(self, toolset)
        self.config = toolset['protocol_config']['machine_translation']['params']

        device = self.config['device']
        if ub.iterable(device):
            self.device = device[0]
        else:
            self.device = device

    def initialize(self, source_network: torch.nn.Module,
                   target_dataset: pd.DataFrame,
                   eval_dataset: pd.DataFrame,
                   query_fn: Any) -> None:
        """  Initialize your algorithm and save any parameters or objects you want to be persistent between
        budget checkpoints.  DomainNetworkSelection will choose a potential source datasets `source_dataset`
        and provide a trained network for that dataset `source_dataset.` Please do not modify the datasets
        beyond overriding the transforms (self.transform and self.transform_target are fine to change).



        Args:
            source_network (pytorch.nn.Module):  Pretrained model from previous stage or None
            target_dataset: (pandas.DataFrame): The target dataset for training with indices
            eval_dataset (pandas.DataFrame): The evaluation dataset for testing with indices
            query_fn (function): python function for querying labels.  Given indices, returns answers

        """
        logging.info('Initializing NLP algorithm')

        self.network = source_network
        if source_network is None:
            self.network # = load_pretrained_network
            self.toolset['source_network'] = self.network  # Save source network for future use

        self.target_dataset = target_dataset
        self.target_labels = None
        self.eval_dataset = eval_dataset
        self.query_fn = query_fn

    def select_and_label_data(self, budget: int) -> None:
        """ This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
        the dataloaders below and feel free to override the transformers in the datasets to use whatever
        pretraining you desire. Please do not modify the datasets though.

        You can call multiple times query_fn with any number of labels up until the budget has run out and
        then it will return nothing.  However, if you already have something labeled, it will relabel it and
        count against you.

        Args:
            budget (int): the number of sentences you may request

        """
        config = self.config
        logging.info(f'Querying for New Labels ')
        if self.target_labels is None:
            query = self.target_dataset['id'].sample(budget).tolist()
        else:
            # Get Queries for unlabeled samples.
            unlabeled = pd.Series(
                pd.concat((self.target_labels['id'], self.target_dataset['id'])).unique()
            )
            budget = min(len(unlabeled), budget)

            logging.info(f'Number unlabeled sentences is {len(unlabeled)}')
            query = unlabeled.sample(budget).tolist()

        logging.info(f'Asking for {len(query)} possible sentences (will only get {budget} characters)')

        # This requests new labels, can be called as many times as you want and will just stop giving
        # more labels when the budget is up.
        new_labels = self._chuck_request(query)

        logging.info(f'Adding {len(new_labels)} new sentences')
        if self.target_labels is None:
            self.target_labels = new_labels
        else:
            self.target_labels = pd.concat((self.target_labels, new_labels))

        garbage_collection_cuda()

    def _chuck_request(self, query: List[int]):
        """ This function will chunk the request into multiple different calls to prevent timeout of the
        JPL network.  Since not sure how many characters

        Args:
            query (list[int]): The list of indices for labeling

        Returns:

        """
        chuck_size = 500
        n = len(query)
        output = []
        for i in range(0, n, chuck_size):
            new_labels = self.query_fn(query[i:min(i+chuck_size, n)])
            if len(new_labels) > 0:
                output += new_labels
            else:
                break
        return pd.DataFrame(output)

    def domain_adapt_training(self) -> None:
        """ This is where you will fine-tune/train your algorithm.  You should make sure your network is saved
        in the self.network to be able to use it in the second stage.

        """
        config = self.config
        logging.info(f'Domain Adaption / Learning Step')

        garbage_collection_cuda()

    def inference(self) -> Dict[str, List[int]]:
        """
        Inference is during the evaluation stage.  Here we are just returning the prediction in `pred` below
        for every item in the test set.  Hopefully your approach will do better.  This is from the ipython
        notebook given out by JPL.

        Returns:
            Dict[str, List[int]]: the return from creating the pandas with the id list and the text
                predictions

        """
        config = self.config
        logging.info(f'Evaluation Step')

        pred = 'The quick brown fox jumps over the lazy dog'
        pred_list = [pred for _ in range(len(self.eval_dataset))]
        ret = pd.DataFrame({'id': self.eval_dataset.df['id'].tolist(), 'text': pred_list}).to_dict()

        garbage_collection_cuda()
        return ret


