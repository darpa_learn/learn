import numpy as np
import pandas as pd
import ubelt as ub
import os

from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import LinearSVC

import torch

# https://github.com/pytorch/pytorch/issues/973
torch.multiprocessing.set_sharing_strategy('file_system')
from torch.utils.data import DataLoader
from torchvision import transforms

from learn.utils.pretrained_networks import get_all_pretrained_models
from learn.utils.pretrained_networks import (
    ImageClassificationNetwork, 
    DetectronObjectDetector,
    VideoClassificationNetwork
)
from learn.algorithms.CoMix.dataset_slowfast import VideoClassificationDataset_SlowFast as VideoClassificationDataset
from learn.utils.memory import garbage_collection_cuda

from learn.algorithms.templateDomainNetworkSelection.domainNetworkSelectionAdapter import \
    DomainNetworkSelectionAdapter
import logging
log = logging.getLogger(__name__)

from pathlib import Path

from learn.utils.dataset import (
  ImageClassificationDataset, ObjectDetectionDataset#, VideoClassificationDataset
)

from hydra import utils as hydra_utils
from learn.utils.wandb_utils import WANDB_INFO
import os


class DomainNetworkSelection(DomainNetworkSelectionAdapter):
    def __init__(self, toolset):
        """ Initialize algorithm, update config with protocol parameters, save config.

        Args:
            toolset:  The toolset from the protocol
        """
        DomainNetworkSelectionAdapter.__init__(self, toolset)
        self.config = toolset['protocol_config']['domain_network_selector']['params']

        device = self.config['device']
        if ub.iterable(device):
            self.device = device[0]
        else:
            self.device = device

        pretrained_network_path = self.config['pretrained_network_dir']
        if not os.path.isabs(pretrained_network_path):
            pretrained_network_path = os.path.join(
                hydra_utils.get_original_cwd(), pretrained_network_path
            )
        self.pretrained_network_path = pretrained_network_path

    def select_network_and_dataset(self, whitelist, whitelist_datasets, target_dataset, problem_type,
                                   last_stage_source_network):
        """ Select the Closest Network and Dataset for the task
        Args:
            whitelist (list[str]): list of dataset names
            whitelist_datasets (dict[str,Union(ImageClassificationDataset, ObjectDetectionDataset))]:
                List of datasets in the whitelist
            target_dataset (dict[str,Union(ImageClassificationDataset, ObjectDetectionDataset))]:
                Target dataset for problem
            problem_type (str): Problem type for current task
            last_stage_source_network (ImageClassificationNetwork): pretrained
                network from the last stage

        Returns:
            Nearest Network, Nearest Dataset

        TODO:
            Make a config item to skip if people want to
            Extract object backbone network for different object detection pretrained networks

        """

        if problem_type == 'machine_translation':# or problem_type == 'video_classification':
            return last_stage_source_network, None

        networks = get_all_pretrained_models(whitelist, whitelist_datasets, problem_type,
                self.pretrained_network_path
                )
        self.toolset['networks'] = dict()  # networks
        if last_stage_source_network is not None:
            networks[last_stage_source_network.target_dataset][
                last_stage_source_network.model_name] = last_stage_source_network
            log.info(f'Switch to {last_stage_source_network.model_name} as feature extractor '
                         f'for choosing network {self.config["find_source_data_network"]} to ensure'
                         f'first stage network can be used as embedding for chosing source domain')
            self.config['find_source_data_network'] = last_stage_source_network.model_name

        dataset_name = self.get_source_dataset(whitelist, whitelist_datasets, target_dataset, networks,
                                                problem_type)
        if dataset_name is not None:
            source_dataset = whitelist_datasets[f'{dataset_name}_train'] 
            pretrained_network = self.get_best_pretrained_network(dataset_name, 
                target_dataset, networks, problem_type)
        else:
            source_dataset = None 
            if problem_type != 'video_classification':
                raise ValueError('No source dataset found')
            pretrained_network = self.get_best_pretrained_network('Kinetics400', 
                target_dataset, networks, problem_type)

        if last_stage_source_network is not None and self.config['always_use_first_stage_network_and_dataset']:
            pretrained_network = last_stage_source_network
            source_dataset = whitelist_datasets[f'{last_stage_source_network.target_dataset}_train']
        return pretrained_network, source_dataset

    def get_source_dataset(self, whitelist, whitelist_datasets, target_dataset, networks, problem_type):
        """ Return the name of the source dataset which has the lowest proxy-A distance to the target dataset.


        Args:
            whitelist (list[str]): list of white datasets to iterate through
            whitelist_datasets (dict[str,ImageClassificationDataset]): Actual dataset
                objects to extract features.
            target_dataset (ImageClassificationDataset): target dataset
            networks (dict[str,dict[str, ImageClassificationNetwork]]): dict
                of add datasets with dictionaries for each pretrained network.
            problem_type (str): Can be either image_classification or object_detection.

        Returns:

        """
        if problem_type == 'video_classification':
            if len(whitelist) == 0:
                # no whitelist datasets loaded on computer
                log.info(f'Not using any source since whitelist is empty')
                if WANDB_INFO.run_wandb:
                    WANDB_INFO.add_runtime_info({'source dataset': None})
                return None 
            elif len(whitelist) == 1:
                # only 1whitelist datasets loaded on computer
                dataset_name = whitelist[0]
                log.info(f'Only have 1 choice of source dataset - {dataset_name}')
                if WANDB_INFO.run_wandb:
                    WANDB_INFO.add_runtime_info({'source dataset': dataset_name})
                return dataset_name

        dataset_name = self.config['find_source_data_method']
        if dataset_name == 'auto':
            similarities = dict()
            network_name = self.config['find_source_data_network']
            for d in whitelist:
                if problem_type == "image_classification":
                    if 'imagenet_1k' not in whitelist:
                        if network_name not in networks[d]:
                            log.info(f'No pretrained network of type {network_name} for {d} so skipping')
                            continue
                        net = networks[d][network_name]
                    else:
                        net = networks['imagenet_1k'][network_name]
                elif problem_type == "object_detection":
                    if 'coco2014' not in whitelist:
                        net = networks[d][network_name]
                        if network_name not in networks[d]:
                            log.info(f'No pretrained network of type {network_name} for {d} so skipping')
                            continue
                    else:
                        net = networks['coco2014'][network_name]
                elif problem_type == 'video_classification':
                    #if len(whitelist) > 1:
                    #    raise NotImplementedError
                    if network_name in networks[d]:
                        net = networks[d][network_name]
                    else:
                        log.info(f'No pretrained network of type {network_name} for {d} so skipping')
                        continue
                else:
                    raise NotImplementedError
                dataset_train_name = f'{d}_test'
                metric = 'proxy_a_w_iou'
                similarities[d] = self.rate_similarity(net,
                                                       whitelist_datasets[dataset_train_name],
                                                       target_dataset,
                                                       metric=metric)

                garbage_collection_cuda()
            df = pd.DataFrame(similarities).T
            log.info(f"{df}")
            if WANDB_INFO.run_wandb:
                WANDB_INFO.log_df(df.reset_index(), f"source_dataset_selection_{metric}")
            #dataset_name = df.index[df.proxy_a.argmin()]
            dataset_name = df.index[df.proxy_a_w_iou.argmax()]

        if dataset_name not in whitelist:
            raise AttributeError(f'"{dataset_name}" is not in the whitelist:\n {whitelist}')

        log.info(f'Using {dataset_name} as source dataset')
        if WANDB_INFO.run_wandb:
            WANDB_INFO.add_runtime_info({'source dataset': dataset_name})

        return dataset_name

    def get_best_pretrained_network(self, source_dataset_name, target_dataset, networks, problem_type):
        """ Get the best pretrained network based on contrast accuracy.

        Args:
            source_dataset_name (str): Name of the source dataset - will override for pretraining in
                'auto' select mode and look for netowrks from all pretrained systems.
            target_dataset (Union(ImageClassificationDataset, ObjectDetectionDataset)): Target dataset to extract features
                on.
            networks (dict[str,dict[str, Union(ImageClassificationNetwork, DetectronObjectDetector)]]): dict of
                all pretrained networks for the source dataset.
            problem_type (str): Can be either ImageClassification or ObjectDetection.

        Returns:
            str: name of best network

        TODO:
            Judge accuracy about classes after a certain threshold

        """
        network_name = self.config['find_pretrained_method']
        if len(target_dataset.get_labeled_indices()) \
           and problem_type == "image_classification":
            metric = 'labeled_loss'
        else:
            metric = 'entropy'
            
        if network_name == 'auto':
            similarities = dict()
            for dataset_name, dateset_networks in networks.items():
                if problem_type == "image_classification" and dataset_name != 'imagenet_1k':
                    continue
                log.info(f'Looking at networks from {dataset_name}')
                for net_name, net in dateset_networks.items():
                    similarities[(dataset_name, net_name)] = self.rate_similarity(net,
                                                                                  None,
                                                                                  target_dataset,
                                                                                  metric=metric)
                    garbage_collection_cuda()
            df = pd.DataFrame(similarities).T
            log.info(f"{df}")
            if WANDB_INFO.run_wandb:
                WANDB_INFO.log_df(df.reset_index(), f"source_network_selection_{metric}")
            source_dataset_name, network_name = df.index[df[metric].argmin()]

        if self.config['set_source_data_network'] is not None:
            if self.config['set_source_data_network'] == 'use_defaults':
                # defaults: ic --> imagenet, od: --> coco204
                if problem_type == 'image_classification':
                    source_dataset_name = "imagenet_1k"
                elif problem_type == 'object_detection':
                    source_dataset_name = 'coco2014'
                elif problem_type == 'video_classification':
                    source_dataset_name = 'Kinetics400'
            else:
                source_dataset_name = self.config['set_source_data_network']

        if network_name not in networks[source_dataset_name].keys():
            err_str = (f'No pretrained network for the "{source_dataset_name}".  Only '
                       f'{networks[source_dataset_name].keys()} available for this dataset.  '
                       f'Defaulting to Imagenet.  (If pretraining, this is expected). ')
            log.warning(err_str)
            if problem_type == "image_classification":
                network = ImageClassificationNetwork(source_dataset_name, '', network_name, True)
            elif problem_type == "object_detection":
                cfg_file = Path(os.path.join(hydra_utils.get_original_cwd(),
                    f'configs/detectron_config/{source_dataset_name}_{network_name}.yaml'))
                model = Path(os.path.join(self.pretrained_network_path,
                    f'{source_dataset_name}_{network_name}.pth'))
                # Use default coco config
                if not cfg_file.exists():
                    cfg_file = Path(os.path.join(hydra_utils.get_original_cwd(),
                        f'configs/detectron_config/coco2014_{network_name}.yaml'))
                if not model.exists():
                    model = Path(os.path.join(self.pretrained_network_path,
                        f'coco2014_{network_name}.pkl'))
                num_cls = len(target_dataset.categories)
                network = DetectronObjectDetector(source_dataset_name, str(model),
                                                  cfg_file, network_name, True, num_cls)
            elif problem_type == "video_classification":
                network = VideoClassificationNetwork(source_dataset_name, '', network_name, True)
            else:
                raise NotImplementedError(f"Invalid problem type {problem_type} specified")
            log.warning(f'Using "{source_dataset_name}" as pretraining dataset and {network_name} as the '
                            f'network but imagenet weights')
            network_name = f'selected {network_name} but used imagenet weights'
        else:
            network = networks[source_dataset_name][network_name]
            log.info(f'Using "{source_dataset_name}" as pretraining dataset and '
                         f'{network_name} as the network')
        
        if WANDB_INFO.run_wandb:
            WANDB_INFO.add_runtime_info({'source network architecture': network_name,
                'source network weights' : source_dataset_name})
        return network

    def rate_similarity(self, network, source_dataset, target_dataset, metric):
        """ Return two values.  Similarity based on proxy-a and similarity based on contrastive problem
        Similarity value is 1 when very similar, 0 when dissimilar

        Proxy-A defined https://arxiv.org/pdf/1412.4446.pdf Eq. 3

        Contrastive is the accuracy of the algorithm to be more similar to self with augmentations rather
        than other items in the same dataset

        Todo:
            Add in similarity based on supervised classification for if we are recalculating with more labels
                or if overlap in source and target domain

        Args:
            network (learn.utils.pretrained_networks.ImageClassificationNetwork):
            source_dataset (ImageClassificationDataset): dataset of measure from
            target_dataset (ImageClassificationDataset): dataset to measure to
            metric (str): either 'proxy_a' or 'contrastive' for metric

        Returns:
            dict[str, metric] : dictionary of metrics.
        """

        target_features = self.extract_features(network, target_dataset, augmentation_style='test')
        out = dict()
        if metric == 'proxy_a':
            source_features = self.extract_features(network, source_dataset, augmentation_style='test')
            proxy_a = self.get_proxy_a(source_features, target_features)
            log.info(f'Proxy a distance between {source_dataset.name} and {target_dataset.name} is '
                         f': {proxy_a:.03f}')
            out['proxy_a'] = proxy_a
        elif metric == 'entropy':
            entropy = self.get_entropy(target_features)
            log.info(f'Entropy on {target_dataset.name} with {network.model_name} is '
                         f'{entropy:.03f}')
            out['entropy'] = entropy
        elif metric == 'contrastive':
            target_features_aug = self.extract_features(network, target_dataset, augmentation_style='train')
            contrast_acc = self.get_contrast_acc(target_features, target_features_aug)
            log.info(f'Contrastive accuracy on {target_dataset.name} with {network.model_name} is '
                         f'{contrast_acc:.03f}')
            out['contrast_acc'] = contrast_acc
        elif metric == 'labeled_loss':
            labeled_loss, entropy_all = self.get_labeled_criterion(network, target_features, target_dataset)
            log.info(f'labeled loss on {target_dataset.name} with {network.model_name} is '
                         f'{labeled_loss:.03f}')
            log.info(f'entropy for all target samples on {target_dataset.name} with {network.model_name} is '
                         f'{entropy_all:.03f}')
            out['labeled_loss'] = labeled_loss
            out['entropy_all'] = entropy_all
        elif metric == 'proxy_a_w_iou':
            source_features = self.extract_features(network, source_dataset, augmentation_style='test')
            proxy_a = self.get_proxy_a(source_features, target_features)
            out['raw_proxy_a'] = proxy_a
            # clip proxy a to between 0 and 2
            if proxy_a < 0:
                proxy_a = 0
            elif proxy_a > 2: # shouldn't be possible
                proxy_a = 2
            # normalize to be between 0 and 1
            proxy_a /= 2
            # negate so 0 is worst and 1 is best
            proxy_a = abs(1 - proxy_a)
            out['normalized_proxy_a'] = proxy_a
            # get iou of classes between source and target
            source_classes = source_dataset.categories.tolist()
            target_classes = target_dataset.categories.tolist()
            source_classes = set(list(map(lambda l: str(l).lower().replace('_', ' ').replace('-', ' '),
                 source_classes)))
            target_classes = set(list(map(lambda l: str(l).lower().replace('_', ' ').replace('-', ' '),
                 target_classes)))
            common_categories = source_classes & target_classes
            #target_only_cats = target_classes - source_classes
            total_categories = list(source_classes | target_classes)
            class_iou = len(common_categories) / len(total_categories)
            out['class_iou'] = class_iou
            # do weighted add of the two
            weight_proxy_a = self.config['weight_proxy_a']
            proxy_a = weight_proxy_a * proxy_a + (1 - weight_proxy_a) * class_iou
            log.info(f'Normalized proxy a distance - IOU between {source_dataset.name} and {target_dataset.name} is '
                         f': {proxy_a:.03f}')
            out['proxy_a_w_iou'] = proxy_a
        else:
            raise NotImplementedError(f'"{metric}" not a valid metric. Please use "proxy_a", "proxy_a_w_iou", "contrastive", "entropy", or "labeled_loss"')

        return out


    def get_labeled_criterion(self, network, target_features, target_dataset):
        """ Returns the Contrast accuracy self similarity for target to target aug features

        Args:
            target_features (np.array): 2D array of target features.  obs,features dim order
            target_dataset target dataset

        Returns:
            accuracy calculated by labeled samples (cross validation only for labeled samples).

        """
        log.info('calculating label acc distance')

        if not len(target_dataset.get_labeled_indices()):
            raise NotImplementedError(f'No labeled target samples')
        dim_feat = target_features.shape[1]
        weight_class = self.get_weight_each_class(network, target_dataset, dim_feat)
        labeled_features, labeled_labels = \
            self.extract_labeled_features(network, target_dataset, augmentation_style='test')
        weight_class = self.to_torch_and_norm(weight_class).float()
        with torch.no_grad():
            target_pt_all = self.to_torch_and_norm(target_features)
            target_pt_labeled = self.to_torch_and_norm(labeled_features)
            tgt_scores_all = torch.matmul(target_pt_all, weight_class.t())
            tgt_scores_labeled = torch.matmul(target_pt_labeled, weight_class.t())
            tgt_scores_all = torch.nn.Softmax(dim=1)(tgt_scores_all) + 1e-8
            criterion = torch.nn.CrossEntropyLoss()
            cross_entropy_labeled = criterion(tgt_scores_labeled,
                                              labeled_labels.long().to(self.device)).cpu().item()
            entropy_all = -(tgt_scores_all * torch.log(tgt_scores_all+1e-8)).sum(1).cpu().mean().item()
        return cross_entropy_labeled, entropy_all

    def get_entropy(self, target_features):
        """ Returns the mean entropy on the target features

        Args:
            target_features (np.array): 2D array of target features.  obs,features dim order

        Returns:
            Entropy of the target dataset for these features

        """
        tgt_scores = torch.nn.Softmax(dim=1)(torch.tensor(target_features)) + 1e-8
        return -(tgt_scores * torch.log(tgt_scores)).sum(1).cpu().mean().item()

    def get_contrast_acc(self, target_features, target_features_aug):
        """ Returns the Contrast accuracy self similarity for target to target aug features

        Args:
            target_features (np.array): 2D array of target features.  obs,features dim order
            target_features_aug (np.array): 2D array of target features.  obs,features dim order

        Returns:
            Similarity score between target and source dataset.

        """
        log.info('calculating contrast acc distance')
        with torch.no_grad():
            target_pt = self.to_torch_and_norm(target_features)
            aug_pt = self.to_torch_and_norm(target_features_aug)
            max_idxs = torch.tensordot(target_pt, aug_pt, dims=([1], [1])).argmax(dim=1)
            diag_idex = torch.arange(target_pt.shape[0], device=self.device)

        return (max_idxs == diag_idex).to(float).mean().item()

    def to_torch_and_norm(self, x):
        """ Small helper function which takes a 2D matrix.  Casts to pytorch device and normalizes the rows

        Args:
            x (np.array): 2D array where the first dim is the obs and the second is the features

        Returns:
            (torch.tensor): normed pytorch tensor
        """
        x = torch.tensor(x, device=self.device)
        return x / x.norm(dim=1, p=2, keepdim=True)

    @staticmethod
    def get_proxy_a(source_features, target_features):
        """ Return Proxy A similarity value.  We run 10 SVMs on random splits to get the accuracy of telling
        the domains apart

        Proxy-A defined https://arxiv.org/pdf/1412.4446.pdf Eq. 3

        Args:
            source_features (np.array): 2D array of features for source dataset
            target_features (np.array): 2D array of features for target dataset

        Returns:
            float: proxy a distace between the source and target datasets

        """
        log.info('calculating proxy d distance')
        X = np.concatenate((source_features, target_features), axis=0)
        n = X.shape[0]
        y = np.ones(n)
        y[:source_features.shape[0]] = 0

        # take accuracy over 5 random splits using domain as labels.
        acc_all = []
        # test size max between total-5000 samples or 20% of dataset.  So if small dataset, just use 20%.
        test_size = max((n - 5000), int(n * .2))
        log.info(f'Train size: {n - test_size} | Test size: {test_size}')
        for i in ub.ProgIter(range(5), desc='Calculating Proxy A'):
            x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=i)
            # SVC is too slow for datasets like imagenet.
            # clf = make_pipeline(StandardScaler(), SVC(gamma='auto', verbose=True))
            clf = make_pipeline(StandardScaler(), LinearSVC(random_state=i, tol=1e-5, verbose=False))
            clf.fit(x_train, y_train)
            acc_all.append(clf.score(x_test, y_test))
            log.info(f'Accuracy: {acc_all[-1]}')
        acc = np.mean(acc_all)

        # Not sure what this outside 2 is doing since makes more sense without since it just makes it between
        # 1 and -1 to 2 and -2.  A small helper function to show what the scores are:
        # (list(map(lambda acc_: 2 * (2 * acc_ - 1), [0.0, 0.5, 1.0])))
        proxy_a = 2 * (2 * acc - 1)

        return proxy_a

    def extract_features(self, network, dataset, augmentation_style='test'):
        """ Extract Features for network

        Args:
            network (ImageClassificationNetwork):
            dataset (ImageClassificationDataset):
            augmentation_style (str): 'test' or 'train for what type of augmentations you want

        Returns:
            np.array: 2D array for features with obs, feats dim order

        """
        log.info(f'Extracting features on {dataset.name} using network {network.model_name}')

        crop_size = 224
        if augmentation_style == 'test':
            dataset.transform = transforms.Compose([
                transforms.Resize(256),
                transforms.CenterCrop(crop_size),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        elif augmentation_style == 'train':
            dataset.transform = transforms.Compose([
                transforms.Resize(256),
                transforms.RandomHorizontalFlip(),
                transforms.RandomCrop(crop_size),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        else:
            raise NotImplementedError(f'"{augmentation_style}" augmentation style not implemented')

        dataloader = DataLoader(dataset,
                                sampler=torch.utils.data.SequentialSampler(dataset),
                                batch_size=self.config['batch_size'],
                                num_workers=int(self.config['num_workers']),
                                collate_fn=dataset.collate_batch,
                                drop_last=False)

        network = network.eval().to(self.device)
        features = []
        with torch.no_grad():
            for im, _, _ in ub.ProgIter(dataloader):
                im = im.to(self.device)
                feat = network(im)
                features.append(feat.cpu().detach().numpy())

        # Deallocate GPU space so other networks can use it
        network = network.cpu()
        del dataloader
        garbage_collection_cuda()

        return np.concatenate(features)

    def get_weight_each_class(self, network, dataset, dim_feat, augmentation_style='test'):
        """ Extract Features for network

    Args:
        network (ImageClassificationNetwork):
        dataset (ImageClassificationDataset):
        augmentation_style (str): 'test' or 'train for what type of augmentations you want

    Returns:
        np.array: 2D array for weights, (number of class, dimension of features).

    """

        log.info(f'Extracting features on {dataset.name} using network {network.model_name}')

        crop_size = 224
        if augmentation_style == 'test':
            dataset.transform = transforms.Compose([
                transforms.Resize(256),
                transforms.CenterCrop(crop_size),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        elif augmentation_style == 'train':
            dataset.transform = transforms.Compose([
                transforms.Resize(256),
                transforms.RandomHorizontalFlip(),
                transforms.RandomCrop(crop_size),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        else:
            raise NotImplementedError(f'"{augmentation_style}" augmentation style not implemented')
        print("number of labeled instance %s" %len(dataset.get_labeled_indices()))
        #log.info(f
        #    'number of labeled instance {dataset.name} using network {network.model_name}')
        labeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
            dataset.get_labeled_indices()
        )
        dataloader = DataLoader(dataset,
                                sampler=labeled_sampler,
                                batch_size=self.config['batch_size'],
                                num_workers=int(self.config['num_workers']),
                                collate_fn=dataset.collate_batch,
                                drop_last=False)

        network = network.eval().to(self.device)
        num_class = len(dataset.categories)
        weight = np.zeros([num_class, dim_feat])
        sample_perclass = np.zeros((num_class,1)) + 1e-5

        with torch.no_grad():
            for im, label, _ in ub.ProgIter(dataloader):
                im = im.to(self.device)

                feat = network(im)
                feat = feat / feat.norm(dim=1, p=2, keepdim=True)
                feat = feat.cpu().detach().numpy()
                for i, each_label in enumerate(label):
                    weight[each_label] += feat[i]
                    sample_perclass[each_label, 0] += 1
        weight = weight / sample_perclass
        # Deallocate GPU space so other networks can use it
        network = network.cpu()

        garbage_collection_cuda()

        return weight

    def extract_labeled_features(self, network, dataset, augmentation_style='test'):
        """ Extract Features for network

        Args:
            network (ImageClassificationNetwork):
            dataset (ImageClassificationDataset):
            augmentation_style (str): 'test' or 'train for what type of augmentations you want

        Returns:
            np.array: 2D arrays for features with obs, feats dim order
            torch tensor: label of features

        """

        log.info(f'Extracting features on {dataset.name} using network {network.model_name}')

        crop_size = 224
        if augmentation_style == 'test':
            dataset.transform = transforms.Compose([
                transforms.Resize(256),
                transforms.CenterCrop(crop_size),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        elif augmentation_style == 'train':
            dataset.transform = transforms.Compose([
                transforms.Resize(256),
                transforms.RandomHorizontalFlip(),
                transforms.RandomCrop(crop_size),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        else:
            raise NotImplementedError(f'"{augmentation_style}" augmentation style not implemented')
        labeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
            dataset.get_labeled_indices()
        )
        dataloader = DataLoader(dataset,
                                sampler=labeled_sampler,
                                batch_size=self.config['batch_size'],
                                num_workers=int(self.config['num_workers']),
                                collate_fn=dataset.collate_batch,
                                drop_last=False)

        network = network.eval().to(self.device)

        features = []
        labels = []
        with torch.no_grad():
            for im, lab, _ in ub.ProgIter(dataloader):
                im = im.to(self.device)
                feat = network(im)
                feat = feat / feat.norm(dim=1, p=2, keepdim=True)
                features.append(feat.cpu().detach().numpy())
                labels.append(lab)

        # Deallocate GPU space so other networks can use it
        network = network.cpu()
        del dataloader
        garbage_collection_cuda()

        return np.concatenate(features), torch.cat(labels)
