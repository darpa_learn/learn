import scriptconfig as scfg


class Config(scfg.Config):
    """
    Default configuration for your algorithm

    Please add all of your default configuration parameters for your algorithm below
    with a description (help) on
        - what your parameter does
        - what changes when you use your parameter

    This is so that someone else can come and alter there parameters and know
    how this will affect the run of the algorithm

    """
    default = {
        'name': scfg.Value('domain_network_selection',
                           help='name of the proposed method'),

        'batch_size': scfg.Value(128,
                                 help='batch size of network while running.  '
                                      'Increasing will do X and decrease will do Y'),

        'num_workers': scfg.Value(16,
                                 help='number of workers in the dataloader'),

        'device': scfg.Value(0,
                           help='what device to use (eg. 0 means gpu 0'),

        'find_pretrained_method': scfg.Value("resnet34",
                           help='Either name of a torchvision model or "auto" for automatic finding'),

        'find_source_data_network': scfg.Value("resnet34",
                                             help='torchvision model computer features for source dataset'),

        'find_source_data_method': scfg.Value("imagenet_1k",
                                             help='Either dataset name or "auto" for automatic detection'
                                                  'Will raise error if dataset not in whitelist'),

    }