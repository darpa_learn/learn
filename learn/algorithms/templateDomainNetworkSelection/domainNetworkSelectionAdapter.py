import abc
from learn.deprecated_tinker.basealgorithm import BaseAlgorithm


class DomainNetworkSelectionAdapter(BaseAlgorithm):
    """ Adapt the DomainNetworkSelection class for use with the
        framework.

    """

    def __init__(self, toolset):
        BaseAlgorithm.__init__(self, toolset)

    def execute(self, toolset, step_descriptor):
        """ Redirect the execute call to the correct subfunction name
            and break out the toolset for simpler use

        Arguments:
            toolset: dict containing named resources for the
                functions to use
            step_descriptor: string describing which step of
                processing should be done

        Returns: the information returned by the subfunction
        """
        if step_descriptor == "SelectNetworkAndDataset":
            last_stage_source_network = None
            if 'last_stage_pretrained_network' in self.toolset:
                last_stage_source_network = toolset['last_stage_pretrained_network']

            return self.select_network_and_dataset(toolset["whitelist"],
                                                   toolset["whitelist_datasets"],
                                                   toolset["target_dataset"],
                                                   toolset["problem_type"],
                                                   last_stage_source_network)
        else:
            raise NotImplementedError

    @abc.abstractmethod
    def select_network_and_dataset(self, whitelist, whitelist_datasets, target_dataset, problem_type,
                                   last_stage_source_network):
        """ a subfunction dispatched by execute. This function must
            implement a part of the algorithm itself.
        """
        raise NotImplementedError
