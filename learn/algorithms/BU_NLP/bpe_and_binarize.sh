# Copyright (c) 2019-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the
# NOTICE FILE in the root directory of this source tree.
#

set -e


#
# Data preprocessing configuration
#
N_MONO=10000000  # number of monolingual sentences for each language
CODES=60000     # number of BPE codes
N_THREADS=16    # number of threads in data preprocessing


#
# Read arguments
#
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
  --src)
    SRC="$2"; shift 2;;
  --tgt)
    TGT="$2"; shift 2;;
  --dataset_src)
    DT_SRC="$2"; shift 2;;
  --split)
    SPLIT="$2"; shift 2;;
  --lg)
    LG="$2"; shift 2;;
  *)
  POSITIONAL+=("$1")
  shift
  ;;
esac
done
set -- "${POSITIONAL[@]}"


#
# Initialize tools and data paths
#

# main paths
MAIN_PATH=$PWD/learn/algorithms/BU_NLP
DATA_PATH=$MAIN_PATH/data/$SRC-$TGT-pth

# create paths
mkdir -p $DATA_PATH

# fastBPE
FASTBPE_DIR=$MAIN_PATH/fastBPE
FASTBPE=$MAIN_PATH/fastBPE/fast

# BPE / vocab files
BPE_CODES=$DATA_PATH/codes
FULL_VOCAB=$DATA_PATH/vocab.$SRC-$TGT

#if [[ $DT_SRC == *"test"* ]]; then
#final_dataset_src=valid.$SRC-$TGT.$SRC
#else

final_dataset_src=$SPLIT.$SRC-$TGT.$LG
#fi

# valid / test parallel BPE data
PARA_SRC_BPE=$DATA_PATH/$final_dataset_src


$FASTBPE applybpe $PARA_SRC_BPE $DT_SRC $BPE_CODES $FULL_VOCAB

echo "Binarizing data..."
rm -f $PARA_SRC_BPE.pth
$MAIN_PATH/preprocess_isidora.py $FULL_VOCAB $PARA_SRC_BPE


#
# Summary
#
echo ""
echo "===== Data summary"
echo "Parallel test data:"
echo "    $SRC: $PARA_SRC_BPE.pth"
echo ""
