# framework -a learn/algorithms -i JPLInterface -p configs/mt_test_feyza.yaml learn/protocol/learn_protocol.py

import os
import ubelt as ub
import logging
import torch
import pandas as pd
from typing import Any, Tuple, List, Dict
from shutil import copyfile

from learn.utils.memory import garbage_collection_cuda
from learn.algorithms.BU_NLP.config import Config
from learn.algorithms.BU_NLP.machineTranslationAdapter import MachineTranslationAdapter

import scriptconfig as scfg
from pathlib import Path

log = logging.getLogger(__name__)

import json
import random
import pdb
from src.slurm import init_signal_handler, init_distributed_mode
from src.data.loader import check_data_params, load_data
from src.utils import bool_flag, initialize_exp, set_sampling_probs, shuf_order
from src.model import check_model_params, build_model
from src.model.memory import HashingMemory
from src.trainer import SingleTrainer, EncDecTrainer
from src.evaluation.evaluator import SingleEvaluator, EncDecEvaluator
import fastBPE
import preprocess
from collections import OrderedDict
from learn.algorithms.BU_NLP.distributed_launch import torch_distributed_launch


class MachineTranslationAlgorithm(MachineTranslationAdapter):
    """

    """

    def __init__(self, toolset: Dict[str, Any]) -> None:
        MachineTranslationAdapter.__init__(self, toolset)

        # Load the default configuration from config.py for your algorithm
        self.config = Config()
        self.update_parameters(toolset['protocol_config'])
        
        #generate parser / parse parameters 
        params = Bunch(self.config)
        self.params = params
        
        #debug mode
        if params.debug:
            params.exp_name = 'debug'
            params.exp_id = 'debug_%08i' % random.randint(0, 100000000)
            params.debug_slurm = True
            params.debug_train = True

        # This sends the configuration back to the toolset
        self.toolset['classification_config'] = self.config

        device = self.config['device']
        if ub.iterable(device):
            self.device = device[0]
        else:
            self.device = device

    def update_parameters(self, protocol_config: scfg) -> None:
        """  Update parameters for algorithm based on protocol config

        Args:
            protocol_config (dict):  Dictionary of parameters.

        Returns:
            None
        """
        common_keyword = set(protocol_config.keys()) & set(self.config.keys())
        for ck in common_keyword:
            self.config[ck] = protocol_config[ck]

    def initialize(self, source_network: torch.nn.Module,
                   target_dataset: pd.DataFrame,
                   eval_dataset: pd.DataFrame,
                   query_fn: Any) -> None:
        """  Initialize your algorithm and save any parameters or objects you want to be persistent between
        budget checkpoints.  DomainNetworkSelection will choose a potential source datasets `source_dataset`
        and provide a trained network for that dataset `source_dataset.` Please do not modify the datasets
        beyond overriding the transforms (self.transform and self.transform_target are fine to change).



        Args:
            source_network (pytorch.nn.Module):  Pretrained model from previous stage or None
            target_dataset: (pandas.DataFrame): The target dataset for training with indices
            eval_dataset (pandas.DataFrame): The evaluation dataset for testing with indices
            query_fn (function): python function for querying labels.  Given indices, returns answers

        """
        log.info('Initializing NLP algorithm')
        
        # clean dumped path
        os.path.join(self.params.dump_path, self.params.exp_name, self.params.exp_id,  )
        
        # remove the eval files (valid/test) if exists
        eval_files = [os.path.join(self.params.data_path,'test.ar-en.ar'),
                      os.path.join(self.params.data_path,'test.ar-en.ar.pth'),
                      os.path.join(self.params.data_path,'valid.ar-en.ar'),
                      os.path.join(self.params.data_path,'valid.ar-en.ar.pth')]
        
        for filename in eval_files:
            try:
                os.remove(filename)
            except OSError:
                pass

        # bpe and binarize eval data
        bpe = fastBPE.fastBPE(self.params.codes_path, self.params.vocab_path)
        bped = bpe.apply(eval_dataset.iloc[:,1]) #TODO: Ask Chris if we can assume the same format for datasets all the time
        bped_path_test = os.path.join(self.params.data_path,'test.ar-en.ar')
        with open(bped_path_test, 'w') as f:
            for item in bped:
                f.write("%s\n" % item)
                
        preprocess.binarize(self.params.vocab_path, bped_path_test, self.params.max_len)
        
        # override/create the valid file
        copyfile(bped_path_test+'.pth', os.path.join(self.params.data_path, "valid.ar-en.ar.pth"))
        
        
        
#         # check model params
#         check_model_params(self.params)
        
#         #check parameters
#         check_data_params(self.params)
        
#         # initialize the multi-GPU / multi-node training
#         init_distributed_mode(self.params)
        
#         # initialize the experiment
#         logger = initialize_exp(self.params)
        
        
#         # initialize SLURM signal handler for time limit / pre-emption
#         init_signal_handler()
        
        
        
#         # load data
#         data = load_data(self.params)

#         # build model
#         self.network = source_network
        
#         if self.params.encoder_only:
#             model = build_model(self.params, data['dico'])
#             if source_network is None:
#                 self.network = model
#                 self.toolset['source_network'] = self.network  # Save source network for future use
#         else:
#             encoder, decoder = build_model(self.params, data['dico'])
#             if source_network is None:
#                 self.network = (encoder, decoder)
#                 self.toolset['source_network'] = self.network  # Save source network for future use
                
                
#         # build trainer, reload potential checkpoints / build evaluator
#         self.trainer = None
#         self.evaluator = None
        
#         if self.params.encoder_only:
#             self.trainer = SingleTrainer(model, data, self.params)
#             self.evaluator = SingleEvaluator(self.trainer, data, self.params)
#         else:
#             self.trainer = EncDecTrainer(encoder, decoder, data, self.params)
#             self.evaluator = EncDecEvaluator(self.trainer, data, self.params)

        self.target_dataset = target_dataset
        self.target_labels = None
        self.eval_dataset = eval_dataset
        self.query_fn = query_fn

    
    def select_and_label_data(self, budget: int) -> None:
        """ This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
        the dataloaders below and feel free to override the transformers in the datasets to use whatever
        pretraining you desire. Please do not modify the datasets though.

        You can call multiple times query_fn with any number of labels up until the budget has run out and
        then it will return nothing.  However, if you already have something labeled, it will relabel it and
        count against you.

        Args:
            budget (int): the number of sentences you may request

        """
        
        ##
        # This method will load the encoder and decoder, do a forward pass in single gpu, compute scores
        # query the server get the labels.
        ##
        config = self.config
        log.info(f'Querying for New Labels for {config["name"]}')
        if self.target_labels is None:
            query = self.target_dataset['id'].sample(budget).tolist()
        else:
            # Get Queries for unlabeled samples.
            unlabeled = pd.Series(
                pd.concat((self.target_labels['id'], self.target_dataset['id'])).unique()
            )
            budget = min(len(unlabeled), budget)

            log.info(f'Number unlabeled sentences is {len(unlabeled)}')
            query = unlabeled.sample(budget).tolist()

        log.info(f'Asking for {len(query)} possible sentences (will only get {budget} characters)')

        # This requests new labels, can be called as many times as you want and will just stop giving
        # more labels when the budget is up.
        new_labels = self._chunk_request(query)

        log.info(f'Adding {len(new_labels)} new sentences')
        if self.target_labels is None:
            self.target_labels = new_labels
        else:
            self.target_labels = pd.concat((self.target_labels, new_labels))

        garbage_collection_cuda()

    def _chunk_request(self, query: List[int]):
        """ This function will chunk the request into multiple different calls to prevent timeout of the
        JPL network.  Since not sure how many characters

        Args:
            query (list[int]): The list of indices for labeling

        Returns:

        """
        chuck_size = 500
        n = len(query)
        output = []
        for i in range(0, n, chuck_size):
            new_labels = self.query_fn(query[i:min(i+chuck_size, n)])
            if len(new_labels) > 0:
                output += new_labels
            else:
                break
        return pd.DataFrame(output)

    def domain_adapt_training(self) -> None:
        """ This is where you will fine-tune/train your algorithm.  You should make sure your network is saved
        in the self.network to be able to use it in the second stage.

        """
        ##
        # This will basically call python torch.distributed.launch with my good old train.py
        ##
        
        config = self.config
        log.info(f'Domain Adaption / Learning Step for {config["name"]}')
        
        # start distributed training
        torch_distributed_launch(self.params.n_gpus, self.params.training_script)

        garbage_collection_cuda()

    def inference(self) -> Dict[str, List[int]]:
        """
        Inference is during the evaluation stage.  Here we are just returning the prediction in `pred` below
        for every item in the test set.  Hopefully your approach will do better.  This is from the ipython
        notebook given out by JPL.

        Returns:
            Dict[str, List[int]]: the return from creating the pandas with the id list and the text
                predictions

        """
        config = self.config
        log.info(f'Evaluation Step for {config["name"]}')
        
#         scores = OrderedDict({'epoch': self.trainer.epoch})
#         hypothesis = self.evaluator.evaluate_mt(scores, "test", "ar", "en", 
#                                                 eval_bleu=True, return_hypothesis=True)

#         # print / JSON log
#         for k, v in scores.items():
#             print("SCORES: %s -> %.6f" % (k, v))
# #             logger.info("%s -> %.6f" % (k, v))
#         if self.params.is_master:
#             print("__log__:%s" % json.dumps(scores))
#             logger.info()
#         pred = 'The quick brown fox jumps over the lazy dog'
#         pred_list = [pred for _ in range(len(self.eval_dataset))]
#         pred = 'The quick brown fox jumps over the lazy dog'
#         pred_list = [pred for _ in range(33)]

        # read the path for the best hypothesis file
        path = os.path.join(self.params.dump_path, self.params.exp_name, self.params.exp_id, 'which_hyp_is_best.txt')
        f = open(path, "r")
        best_hyp_path = f.read()
        f.close()
        
        # now read the hypothesis to a list
        with open(best_hyp_path) as f:
            pred_list = f.readlines()

        ret = pd.DataFrame({'id': self.eval_dataset['id'].tolist(), 'text': pred_list}).to_dict()

        garbage_collection_cuda()
        return ret


class Bunch(object):
    def __init__(self, adict):
        self.__dict__.update(adict)
        
        
if __name__ == '__main__':
    initialize()
    