# framework -a learn/algorithms -i JPLInterface -p configs/mt_test_feyza.yaml learn/protocol/learn_protocol.py

import os
import ubelt as ub
import logging
log = logging.getLogger(__name__)

import torch
import pandas as pd
from typing import Any, Tuple, List, Dict
import shutil
import pickle

import numpy as np

from learn.utils.memory import garbage_collection_cuda
from learn.algorithms.BU_NLP.config import Config, EPOCHS, TOKENS_PER_BATCH, EPOCH_SIZE, MT_STEPS, PARAM_FILE, AVG_SENT_LEN #, BT_STEPS
from learn.algorithms.BU_NLP.machineTranslationAdapter import MachineTranslationAdapter

import scriptconfig as scfg
from pathlib import Path

#import subprocess



import json
import random
import pdb
from src.slurm import init_signal_handler, init_distributed_mode
from src.data.loader import check_data_params, load_data
from src.utils import bool_flag, initialize_exp, set_sampling_probs, shuf_order
from src.model import check_model_params, build_model
from src.model.memory import HashingMemory
from src.trainer import SingleTrainer, EncDecTrainer
from src.evaluation.evaluator import SingleEvaluator, EncDecEvaluator
import fastBPE
import preprocess
from collections import OrderedDict
from learn.algorithms.BU_NLP.distributed_launch import torch_distributed_launch




class MachineTranslationAlgorithm(MachineTranslationAdapter):
    """

    """

    def __init__(self, toolset: Dict[str, Any]) -> None:
        MachineTranslationAdapter.__init__(self, toolset)

        # Load the default configuration from config.py for your algorithm
        self.config = Config()
        config = self.config
        self.update_parameters(toolset['protocol_config'])

        # This sends the configuration back to the toolset
        self.toolset['classification_config'] = config

        device = config['device']
        if ub.iterable(device):
            self.device = device[0]
        else:
            self.device = device

    def update_parameters(self, protocol_config: scfg) -> None:
        """  Update parameters for algorithm based on protocol config

        Args:
            protocol_config (dict):  Dictionary of parameters.

        Returns:
            None
        """
        common_keyword = set(protocol_config.keys()) & set(self.config.keys())
        for ck in common_keyword:
            self.config[ck] = protocol_config[ck]

    def initialize(self, source_network: torch.nn.Module,
                   target_dataset: pd.DataFrame,
                   eval_dataset: pd.DataFrame,
                   query_fn: Any) -> None:
        """  Initialize your algorithm and save any parameters or objects you want to be persistent between
        budget checkpoints.  DomainNetworkSelection will choose a potential source datasets `source_dataset`
        and provide a trained network for that dataset `source_dataset.` Please do not modify the datasets
        beyond overriding the transforms (self.transform and self.transform_target are fine to change).



        Args:
            source_network (pytorch.nn.Module):  Pretrained model from previous stage or None
            target_dataset: (pandas.DataFrame): The target dataset for training with indices
            eval_dataset (pandas.DataFrame): The evaluation dataset for testing with indices
            query_fn (function): python function for querying labels.  Given indices, returns answers

        """
        log.info('Initializing NLP algorithm')
        config = self.config
        
        # Save datasets
        self.target_dataset = target_dataset
        self.target_labels = None
        self.eval_dataset = eval_dataset
        self.query_fn = query_fn
        
        
        # Report empty sequences found in eval dataset
        self.empty_index = self.record_if_empty(self.eval_dataset)
        if len(self.empty_index) > 0:
            log.warn(f'Detected empty sequences in eval dataset at {self.empty_index}')


        # Report empty sequences found in eval dataset
        self.empty_index_target = self.record_if_empty(self.target_dataset)
        if len(self.empty_index_target) > 0:
            log.warn(f"Detected empty sequences in target dataset at {self.empty_index_target}")

        
        # Fill in the originally dummy character lines in target_dataset
        for ind in self.empty_index_target:
            print("!!!!! Adding a dummy character at index {0}".format(ind))
            dummy = "جملة وهمية.."
            self.target_dataset.at[ind, 'source'] = dummy

        # Replace newline and tab chars in datasets' sequences with spaces
        log.info("Reporting whitespaces for eval_dataset:")
        self.check_and_report_and_remove_whitespace(self.eval_dataset)
        
        # Replace newline and tab chars in datasets' sequences with spaces
        log.info("Reporting whitespaces for eval_dataset:")
        self.check_and_report_and_remove_whitespace(self.target_dataset)
  
        # Set checkpoint 
        self.checkpoint = 0
        if source_network is None:
            """ Base stage. """
            
            # Set stage
            self.stage='base'
            
            # Clean dump path (one reason is to not load an old checkpoint.pth)
            pth = os.path.join(config['dump_path'], config['exp_name'], config['exp_id'])
            try:
                shutil.rmtree(pth)
            except FileNotFoundError:
                pass
        else:
            """ Adaption stage. """
            self.stage='adapt'
            
            # Update checkpoint (first checkpoint of adapt stage)
            self.checkpoint = 8
            
            # Remove the base checkpoint and relocate the adapt checkpoint
            log.info("***** Replacing the base checkpoint with adapted checkpoint. *****")
            pth = os.path.join(config['dump_path'], config['exp_name'], config['exp_id'], "checkpoint.pth")
            shutil.copyfile(config["adapt_checkpoint"], pth)
            
        # Save eval dataset to csv based on stage
        self.eval_dataset.to_csv(os.path.join(config['data_path'],"eval_dataset_{0}.csv".format(self.stage)))

        # Remove the eval and train files if exists, all will be re-created
        files = [os.path.join(config['data_path'],'test.ar-en.ar'),
                 os.path.join(config['data_path'],'test.ar-en.ar.pth'),
                 os.path.join(config['data_path'],'valid.ar-en.ar'),
                 os.path.join(config['data_path'],'valid.ar-en.ar.pth'),
                 ]
        
        remove_files(files)
        
        # FEYZA- START
        # Preprocess the target dataset and save it as test/valid which will be used
        # in forward pass for score calculation. ##### !!!!! TODO CHANGE BACK TO TARGET
        bpe = fastBPE.fastBPE(config['codes_path'], config['vocab_path'])
        bped = bpe.apply(self.target_dataset['source'])
        self.target_bped_lengths = [len(seq.split(" ")) for seq in bped]
        bped_path_test = os.path.join(config['data_path'],'test.ar-en.ar')
        
        # Save
        with open(bped_path_test, 'w') as f:
            for item in bped:
                f.write("%s\n" % item)
                
        # Create test.ar-en.ar.pth
        preprocess.binarize(config['vocab_path'], bped_path_test, config['max_len'])
        
        # Create others required by copying
        files = ["valid.ar-en.ar.pth", "valid.ar-en.en.pth", "test.ar-en.en.pth"]
        for f in files:
            shutil.copyfile(bped_path_test+'.pth', os.path.join(config['data_path'], f))
        # FEYZA- END
        
        
        """
        #ISIDORA - START
        bped_path_test = os.path.join(self.params.data_path,'target.ar-en.ar')
        with open(bped_path_test, 'w') as f:
            for item in target_dataset.iloc[:,1]:
                f.write("%s\n" % item)
        subprocess.call("./learn/algorithms/BU_NLP/bpe_and_binarize.sh --src %s --tgt %s --dataset_src %s --split %s --lg %s" % ('ar', 'en', bped_path_test, 'test','ar'), shell=True)
        
        bped_path_valid= os.path.join(self.params.data_path,'test.ar-en.ar')
        # override/create the valid file
        shutil.copyfile(bped_path_valid+'.pth', os.path.join(self.params.data_path, "valid.ar-en.ar.pth"))
        shutil.copyfile(bped_path_valid+'.pth', os.path.join(self.params.data_path, "valid.ar-en.en.pth"))
        shutil.copyfile(bped_path_valid+'.pth', os.path.join(self.params.data_path, "test.ar-en.en.pth"))
        #ISIDORA - END
        """
        
        
        # Save the source network for adapt stage
        pth = os.path.join(config['dump_path'], config['exp_name'], config['exp_id'])
        self.toolset['source_network'] = os.path.join(pth, "checkpoint.pth") 
        
    def random_select_and_label_data(self, budget:int) -> None:
        """ This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
        the dataloaders below and feel free to override the transformers in the datasets to use whatever
        pretraining you desire. Please do not modify the datasets though.

        You can call multiple times query_fn with any number of labels up until the budget has run out and
        then it will return nothing.  However, if you already have something labeled, it will relabel it and
        count against you.

        Args:
            budget (int): the number of sentences you may request

        """
        config = self.config
        log.info(f'Querying for New Labels for {config["name"]}')

        budget_approx_sent = int(budget / AVG_SENT_LEN)

        # clean training files if exists
        files = [os.path.join(config['data_path'],'train.ar-en.ar'),
                os.path.join(config['data_path'],'train.ar-en.ar.pth'),
                os.path.join(config['data_path'],'train.ar-en.en'),
                os.path.join(config['data_path'],'train.ar-en.en.pth')]

        remove_files(files)

        # Querying

        # If label available so far
        if self.target_labels is None:
            query = self.target_dataset['id'].sample(budget_approx_sent).tolist()
        else:
            # Get Queries for unlabeled samples.
            unlabeled = pd.Series(
                pd.concat((self.target_labels['id'], self.target_dataset['id'])).unique()
            )
            budget_approx_sent = min(len(unlabeled), budget_approx_sent)

            log.info(f'Number unlabeled sentences is {len(unlabeled)}')
            query = unlabeled.sample(budget_approx_sent).tolist()


        # This requests new labels, can be called as many times as you want and will just stop giving
        # more labels when the budget is up.
        log.info(f'Asking for {len(query)} possible sentences (will only get {budget_approx_sent})')
        new_labels = self._chunk_request(query)
        log.info(f'Adding {len(new_labels)} new sentences')
        if self.target_labels is None:
            self.target_labels = new_labels
        else:
            self.target_labels = pd.concat((self.target_labels, new_labels))


        # Assert ar and er of train to be of equal length, check for other mishaps
        ar_train = self.target_dataset.set_index(['id']).loc[self.target_labels['id'], 'source']
        en_train = self.target_labels['text']
        ar_train, en_train = self.check_queried_labels(ar_train.reset_index()['source'],
                                                       en_train.reset_index()['text'])

        # Save the train files
        self.bpe_and_binarize_train_files(ar_train, en_train)

        garbage_collection_cuda()
        
    def select_and_label_data(self, budget: int) -> None:
        """ This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
        the dataloaders below and feel free to override the transformers in the datasets to use whatever
        pretraining you desire. Please do not modify the datasets though.

        You can call multiple times query_fn with any number of labels up until the budget has run out and
        then it will return nothing.  However, if you already have something labeled, it will relabel it and
        count against you.

        Args:
            budget (int): the number of sentences you may request

        """
        config = self.config
        log.info(f'Querying for New Labels for {config["name"]}')
        budget_approx_sent = int(budget / AVG_SENT_LEN)
        
        # Clean training files if exists
        files = [os.path.join(config['data_path'],'train.ar-en.ar'),
                os.path.join(config['data_path'],'train.ar-en.ar.pth'),
                os.path.join(config['data_path'],'train.ar-en.en'),
                os.path.join(config['data_path'],'train.ar-en.en.pth')]
        
        remove_files(files)
   
        ####### Active learning part #######
    
        new_budget = 0
        # If label available so far
        if self.target_labels is None:
            log.info(f'self.target_labels is None')
            #query = self.target_dataset['id'].sample(budget).tolist()
            
            if config['easy_run']:
                scores_path = os.path.join(config['data_path'],"scores_{0}.txt".format(self.stage))
                
            else:
                scores_filelist = []
                if self.stage == 'base':
                    log.info('"scoring sentences in base stage"')
                    torch_distributed_launch(config['n_gpus'], config['scoring_script'])
                    if config['n_gpus'] >1 and config['split_data']:
                        for i in range(config['n_gpus']):
                            scores_filelist.append(os.path.join(config['data_path'],"scores_base"+str(i)+".txt"))
                    else:
                        scores_filelist.append(os.path.join(config['data_path'],"scores_base0.txt"))
                    scores_path = os.path.join(config['data_path'],"scores_base.txt")

                else:
                    log.info('"scoring sentences in adapt stage"')
                    torch_distributed_launch(config['n_gpus'], config['scoring_script'])
                    if config['n_gpus'] >1 and config['split_data']:
                        for i in range(config['n_gpus']):
                            scores_filelist.append(os.path.join(config['data_path'],"scores_adapt"+str(i)+".txt"))
                    else:
                        scores_filelist.append(os.path.join(config['data_path'],"scores_adapt0.txt"))
                    scores_path = os.path.join(config['data_path'],"scores_adapt.txt")

                # Concat the different score files into one
                with open(scores_path, 'w') as outfile:
                    for fname in scores_filelist:
                        with open(fname) as infile:
                            for line in infile:
                                outfile.write(line)

            # Reading the scores to a list
            with open(scores_path) as f:
                scores = f.readlines()
            scores = [float(i.strip("\n")) for i in scores]
            assert len(scores) == len(self.target_dataset)

            # Fill in the originally dummy character lines in target_dataset
            for ind in self.empty_index_target:
                print("!!!!! Adding a low score at index {0}".format(ind))
                scores[ind]=-1e100

            # Boosting sentences with 100<=length<=250
            assert len(self.target_bped_lengths) == len(scores)

            for i in range(len(self.target_bped_lengths)):
                if self.target_bped_lengths[i]>=100 and self.target_bped_lengths[i]<=250:
                    scores[i]+= self.target_bped_lengths[i]

#             self.target_dataset['id'] = pd.to_numeric(self.target_dataset['id'],downcast='integer')
            sorterIndex = dict(zip(range(len(scores)),scores))
            self.target_dataset['scores_id'] = self.target_dataset.reset_index().index.map(sorterIndex)
            self.target_dataset['scores_id'] = pd.to_numeric(self.target_dataset['scores_id'],downcast='float')
            self.target_dataset = self.target_dataset.sort_values(by= 'scores_id',ascending=False)
            self.target_dataset = self.target_dataset.reset_index(drop=True)
            query = self.target_dataset['id'].head(budget_approx_sent).tolist()
            query=[int(i) for i in query]
            
            # FEYZA-START
            # BPE and binarize eval data
            bpe = fastBPE.fastBPE(config['codes_path'], config['vocab_path'])
            bped = bpe.apply(self.eval_dataset['source'].drop(self.empty_index))
            bped_path_test = os.path.join(config['data_path'],'test.ar-en.ar')
            with open(bped_path_test, 'w') as f:
                for item in bped:
                    f.write("%s\n" % item)
            # Create test.ar-en.ar.pth
            preprocess.binarize(config['vocab_path'], bped_path_test, config['max_len'])
            # Create others required by copying
            files = ["valid.ar-en.ar.pth", "valid.ar-en.en.pth", "test.ar-en.en.pth"]
            for f in files:
                shutil.copyfile(bped_path_test+'.pth', os.path.join(config['data_path'], f))
            #FEYZA-END

            """
            #ISIDORA - START
            bped_path_test = os.path.join(self.params.data_path,'target.ar-en.ar')
            with open(bped_path_test, 'w') as f:
                for item in self.eval_dataset.iloc[:,1]:
                    f.write("%s\n" % item)
            subprocess.call("./learn/algorithms/BU_NLP/bpe_and_binarize.sh --src %s --tgt %s --dataset_src %s --split %s --lg %s" % ('ar', 'en', bped_path_test, 'test','ar'), shell=True)
            
            bped_path_valid= os.path.join(self.params.data_path,'test.ar-en.ar')
            # override/create the valid file
            shutil.copyfile(bped_path_valid+'.pth', os.path.join(self.params.data_path, "valid.ar-en.ar.pth"))
            shutil.copyfile(bped_path_valid+'.pth', os.path.join(self.params.data_path, "valid.ar-en.en.pth"))
            shutil.copyfile(bped_path_valid+'.pth', os.path.join(self.params.data_path, "test.ar-en.en.pth"))
            #ISIDORA - END
            """

        else:
            log.info(f'self.target_labels is of length= {len(self.target_labels)}')
            
            # Get Queries for unlabeled samples.
            # Keeping only the values in target dataset but not in target labels,
            # the ones in target labels have already been labeled
            tlist = self.target_labels['id']
            self.target_dataset = self.target_dataset.set_index(['id'])
            unlabeled = self.target_dataset.loc[~self.target_dataset.index.isin(tlist)].index
            self.target_dataset = self.target_dataset.reset_index()
            log.info(f'Number of unlabeled sentences is {len(unlabeled)}')

            query = unlabeled.tolist()[:budget_approx_sent]
            query = [int(i) for i in query]
        
        log.info(f'Asking for {len(query)} possible sentences (will only get {budget} characters)')

  
        # This requests new labels, can be called as many times as you want and will just stop giving
        # more labels when the budget is up.
        log.info(f'Asking for {len(query)} possible sentences (will only get {budget_approx_sent})')
        new_labels = self._chunk_request(query)
        log.info(f'Adding {len(new_labels)} new sentences')
        if self.target_labels is None:
            self.target_labels = new_labels
        else:
            self.target_labels = pd.concat((self.target_labels, new_labels))

#         lst_target = self.target_labels['id'].tolist()
#         lst_target = [int(i) for i in lst_target]

        # FEYZA- START
        # Assert ar and er of train to be of equal length, check for other mishaps
        ar_train = self.target_dataset.set_index(['id']).loc[self.target_labels['id'], 'source']
#         ar_train = self.target_dataset.loc[self.target_dataset['id'].isin(lst_target),'source']
        en_train = self.target_labels['text']
        ar_train, en_train = self.check_queried_labels(ar_train.reset_index()['source'], 
                                                       en_train.reset_index()['text'])
        
        # Save the train files
        self.bpe_and_binarize_train_files(ar_train, en_train)
        #FEYZA- END
        
        """
        #ISIDORA - START
        bped_path_train_ar = os.path.join(self.params.data_path,'train.ar-en.ar_')
        with open(bped_path_train_ar, 'w') as f:
            for item in self.target_dataset.iloc[lst_target,1]:
                f.write("%s\n" % item)
        subprocess.call("./learn/algorithms/BU_NLP/bpe_and_binarize.sh --src %s --tgt %s --dataset_src %s --split %s --lg %s" % ('ar', 'en', bped_path_train_ar, 'train','ar'), shell=True)
    
        
        # Save the train files
        self.bpe_and_binarize_train_files(ar_train, en_train)
        """
        garbage_collection_cuda()


    def _chunk_request(self, query: List[int]):
        """ This function will chunk the request into multiple different calls to prevent timeout of the
        JPL network.  Since not sure how many characters

        Args:
            query (list[int]): The list of indices for labeling

        Returns:

        """
        chuck_size = 500
        n = len(query)
        output = []
        for i in range(0, n, chuck_size):
            new_labels = self.query_fn(query[i:min(i+chuck_size, n)])
            if len(new_labels) > 0:
                output += new_labels
            else:
                break
        return pd.DataFrame(output)

    def domain_adapt_training(self) -> None:
        """ This is where you will fine-tune/train your algorithm.  You should make sure your network is saved
        in the self.network to be able to use it in the second stage.
        
        The train.py script (training_script) automatically loads the latest checkpoint if the same dump path /
        exp name and exp id are used.
        
        So for every checkpoint, once this is called, if a checkpoint.pth is found, it'll load it and continue to
        train with the newest training data retrieved and saved by select_and_label_data.

        """

        config = self.config
        log.info(f'Domain Adaption / Learning Step for {config["name"]}')
        
        # Do not reload the initial model unless the first checkpoint of base stage
        if self.stage == "adapt" or self.checkpoint > 0:
            config['reload_model'] = ""
        
        # Update the pickle file for parameters with the corresponding parameters to the checkpoint
        if config["easy_run"]:
            config['tokens_per_batch'] = 1000
            config['epoch_size'] = 1000
            config['max_epoch'] = 1
            config['mt_steps'] = MT_STEPS[self.checkpoint]
        else:
            config['tokens_per_batch'] = TOKENS_PER_BATCH[self.checkpoint] 
            config['epoch_size'] = EPOCH_SIZE[self.checkpoint]
            config['max_epoch'] = EPOCHS[self.checkpoint]
            config['mt_steps'] = MT_STEPS[self.checkpoint]  
        
        
        log.info(f"UPDATED TRAINING PARAMETERS FOR CHECKPOINT {self.checkpoint} AS FOLLOWS: MAX_EPOCH={config['max_epoch']}, EPOCH_SIZE={config['epoch_size']}, TOKENS_PER_BATCH={config['tokens_per_batch']}")

        # Save the config to be used in train.py
        with open(PARAM_FILE, 'wb') as handle:
            pickle.dump(config, handle)

        # Start distributed training
        if config['easy_run']:
            if self.checkpoint in [0,8]:
                torch_distributed_launch(config['n_gpus'], config['training_script'])
        else:
            torch_distributed_launch(config['n_gpus'], config['training_script'])
        self.checkpoint = self.checkpoint + 1
        garbage_collection_cuda()

        
    def inference(self) -> Dict[str, List[int]]:
        """
        Inference is during the evaluation stage.  Here we are just returning the prediction in `pred` below
        for every item in the test set.  Hopefully your approach will do better.  This is from the ipython
        notebook given out by JPL.

        Returns:
            Dict[str, List[int]]: the return from creating the pandas with the id list and the text
                predictions

        """
        config = self.config
        log.info(f'Evaluation Step for {config["name"]}')

        # Latest hypohtesis to load
        epoch = int(config['max_epoch'])-1
        path = os.path.join(config['dump_path'], config['exp_name'], config['exp_id'], "hypotheses", 'hyp{0}.ar-en.test.txt'.format(epoch))
        
        # Now read the hypothesis to a list
        with open(path) as f:
            pred_list = f.readlines()
            
        # Fill in the originally empty lines with empty predictions
        for ind in self.empty_index:
            print("!!!!! Adding an empty line at index {0}".format(ind))
            pred_list.insert(ind, " ")
        print("Size of eval_dataset {} and size of pred_list {}".format(len(self.eval_dataset['id']),
                                                                        len(pred_list)))
        
        # Return predictions
        ret = pd.DataFrame({'id': self.eval_dataset['id'].tolist(), 'text': pred_list}).to_dict()
        garbage_collection_cuda()
        return ret
    
    
    def check_and_report_and_remove_whitespace(self, dataset):
        n = dataset['source'].str.contains("\n", regex=False)
        t = dataset['source'].str.contains("\t", regex=False)

        log.info(f"Dataset contains {n.sum()} and {t.sum()} newline and tab characters within sequences.")
        log.info("Index of sequences containing newline: {0}".format(n[n].index))
        log.info("Index of sequences containing tab: {0}".format(t[t].index))
        
        # Remove tabs and spaces
        log.info("Replaced these chars with empty spaces.")
        dataset['source'] = dataset['source'].apply(lambda x: " ".join(x.split()))
        
        dataset['source'] = dataset['source'].str.replace("\t"," ").str.replace("\n", " ")
        
    def record_if_empty(self, dataset):
        # Save if a query is empty or white space only
        x = dataset.reset_index()['source'].str.strip() == ""
        empty_index = x.index[x].tolist()
        return empty_index
    
    def check_queried_labels(self, ar_train, en_train):
        config = self.config
        assert len(ar_train) == len(en_train)
        
        # Replace any newline and tab characters with spaces
        log.info("Reporting for ar_train:")
        temp_ar_train = pd.DataFrame({'source':ar_train})
        self.check_and_report_and_remove_whitespace(temp_ar_train)
        temp_ar_train.to_csv(os.path.join(config['data_path'], "train.ar-en.ar.csv"))
        ar_train = temp_ar_train['source']
        
        log.info("Reporting for en_train:")
        temp_en_train = pd.DataFrame({'source':en_train})
        self.check_and_report_and_remove_whitespace(temp_en_train)
        temp_en_train.to_csv(os.path.join(config['data_path'], "train.ar-en.en.csv"))
        en_train = temp_en_train['source']
        
        assert len(ar_train) == len(en_train)
        
        # Check if any <empty> - <non-empty> sequence pairs are present, if so remove the pair
        inconsistent_indices = []
        for i, seq in enumerate(ar_train):
            
            if seq.strip() == "" or en_train.iloc[i].strip() == "":
                log.warn("Inconsistent (or empty) pair detected in train set. Removing the pair.")
                log.info(f"Arabic: {seq}  English: {en_train.iloc[i]}")
                series_ind_ar = ar_train.index[i]
                series_ind_en = en_train.index[i]
                assert series_ind_ar == series_ind_en
                inconsistent_indices.append(series_ind_ar)
            
        for pair_ind in inconsistent_indices:
            ar_train.drop(pair_ind, inplace=True)
            en_train.drop(pair_ind, inplace=True)
        log.info(f"Total number of inconsistent pairs that are removed from train set is {len(inconsistent_indices)}.")
        
        assert len(ar_train) == len(en_train)
        return ar_train, en_train
        
    def bpe_and_binarize_train_files(self, ar_train, en_train):
        config = self.config
        #FEYZA- START
        # BPE and binarize arabic
        bpe = fastBPE.fastBPE(config['codes_path'], config['vocab_path'])
        bped_ar = bpe.apply(ar_train)
        bped_en = bpe.apply(en_train) 
        assert len(bped_ar) == len(bped_en)
        
        # Check if any elements are empty again. Sometimes BPE operation leaves some sequences empty.
        # Check if any <empty> - <non-empty> sequence pairs are present, if so remove the pair.
        inconsistent_indices = []
        for i, seq in enumerate(bped_ar):
            if seq.strip() == "" or bped_en[i].strip() == "":
                log.warn("Inconsistent (or empty) pair detected in train set. Removing the pair.")
                log.info(f"Arabic: {seq}  English: {bped_en[i]}")
                inconsistent_indices.append(i)
        
        for pair_ind in inconsistent_indices:
            del bped_ar[pair_ind]
            del bped_en[pair_ind]
            
        log.info(f"Total number of inconsistent pairs that are removed from train set is {len(inconsistent_indices)}.")
                
        assert "" not in bped_ar
        assert "" not in bped_en
        
        # Save BPE'd files
        bped_path_train_ar = os.path.join(config['data_path'],'train.ar-en.ar')
        bped_path_train_en = os.path.join(config['data_path'],'train.ar-en.en')
        
        assert len(bped_ar) == len(bped_en)
        
        len_of_train = len(bped_ar)
        print("Length of train set is {}.".format(len_of_train))
        
        with open(bped_path_train_ar, 'w') as f1:
            for ind, item in enumerate(bped_ar):
                f1.write("%s\n" % (item))
        preprocess.binarize(config['vocab_path'], bped_path_train_ar, config['max_len'])
        
        with open(bped_path_train_en, 'w') as f2:
            for ind, item in enumerate(bped_en):
                f2.write("%s\n" % (item))
        preprocess.binarize(config['vocab_path'], bped_path_train_en, config['max_len'])
        
        # Read back to make sure
        with open(bped_path_train_ar) as f:
            lines_ar = f.readlines()
            
        with open(bped_path_train_en) as f:
            lines_en = f.readlines()
        
        assert len(lines_ar) == len_of_train
        assert len(lines_en) == len_of_train
            
        #FEYZA- END

def remove_files(files):
    for filename in files:
        try:
            os.remove(filename)
        except OSError:
            pass

class Bunch(object):
    def __init__(self, adict):
        self.__dict__.update(adict)