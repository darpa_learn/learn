# reload encoder
import os
import torch
import torch.nn.functional as F

#model_path ='/project/multilm/isidora/XLM/mlm_ende_1024.pth'
#model_path = '/project/multilm/isidora/XLM/dumped/unsupMT_ende/ejiyczt0vd/checkpoint.pth'

def reload_encoder(model_path):
    enc_path = model_path
    if enc_path != '':
        print("Reloading encoder from %s ..." % enc_path)
        #enc_reload = torch.load(enc_path, map_location=lambda storage, loc: storage.cuda(params.local_rank))
        enc_reload = torch.load(enc_path, map_location=lambda storage, loc: storage.cuda(0))
        enc_reload = enc_reload['model' if 'model' in enc_reload else 'encoder']
        print("encoder loaded")
        enc_reload = {(k[len('module.'):] if k.startswith('module.') else k): v for k, v in enc_reload.items()}
        print(enc_reload.keys())
        #print("----------")
        for i in range(6):
            weight_str = 'attentions.'+str(i)+'.out_lin.weight'
            #print(weight_str," = ", enc_reload[weight_str])
            weight_str_qlin = 'attentions.'+str(i)+'.q_lin.weight'
            #print(weight_str_qlin," = ", enc_reload[weight_str_qlin])
            weight_str_qbias = 'attentions.'+str(i)+'.q_lin.bias'
            #print(weight_str_qbias," = ", enc_reload[weight_str_qbias])
            #print(enc_reload[weight_str].size())
            #print(enc_reload[weight_str_qlin].size())
            #print(enc_reload[weight_str_qbias].size())
        print()
    return enc_reload

"""
    if all([k.startswith('module.') for k in enc_reload.keys()]):
        enc_reload = {k[len('module.'):]: v for k, v in enc_reload.items()}
    encoder.load_state_dict(enc_reload)
"""
def reload_decoder(model_path):
# reload decoder
    dec_path = model_path
    if dec_path != '':
        print("Reloading decoder from %s ..." % dec_path)
        #dec_reload = torch.load(dec_path, map_location=lambda storage, loc: storage.cuda(params.local_rank))
        dec_reload = torch.load(dec_path, map_location=lambda storage, loc: storage.cuda(0))
        dec_reload = dec_reload['model' if 'model' in dec_reload else 'decoder']
        print("decoder loaded")
        dec_reload = {(k[len('module.'):] if k.startswith('module.') else k): v for k, v in dec_reload.items()}
        print(dec_reload.keys())
        print("----------")
        for i in range(6):
            weight_str = 'attentions.'+str(i)+'.out_lin.weight'
            print(weight_str," = ", dec_reload[weight_str])
            print(dec_reload[weight_str].size())
            weight_str_qlin = 'attentions.'+str(i)+'.q_lin.weight'
            print(weight_str_qlin," = ", dec_reload[weight_str_qlin])
            weight_str_qbias = 'attentions.'+str(i)+'.q_lin.bias'
            print(weight_str_qbias," = ", dec_reload[weight_str_qbias])
            print(dec_reload[weight_str_qlin].size())
            print(dec_reload[weight_str_qbias].size())
            print(dec_reload['attentions.'+str(i)+'.k_lin.weight'].size())
            print(dec_reload['attentions.'+str(i)+'.k_lin.bias'].size())
            print(dec_reload['attentions.'+str(i)+'.v_lin.weight'].size())
            print(dec_reload['attentions.'+str(i)+'.v_lin.bias'].size())
            #print(dec_reload[weight_str].size())
            #print(dec_reload['attentions.'+str(i)+'.out_lin.bias'].size())
        #print(dec_reload['position_embeddings.weight'].size())
        #print(dec_reload['lang_embeddings.weight'].size())
        #print(dec_reload['embeddings.weight'].size())
        print()
    return dec_reload
"""
Q= A*dec_reload[weight_str_qlin]+dec_reload[weight_str_qbias]
K= A*dec_reload[weight_str_klin]+dec_reload[weight_str_kbias]
V= A*dec_reload[weight_str_vlin]+dec_reload[weight_str_vbias]
dk=8
attentions=F.softmax(scores.float(), dim=-1).type_as(scores) 
"""
"""
if all([k.startswith('module.') for k in dec_reload.keys()]):
    dec_reload = {k[len('module.'):]: v for k, v in dec_reload.items()}
for i in range(params.n_layers):
    for name in DECODER_ONLY_PARAMS:
        if name % i not in dec_reload:
            logger.warning("Parameter %s not found." % (name % i))
            dec_reload[name % i] = decoder.state_dict()[name % i]
decoder.load_state_dict(dec_reload) 
"""
