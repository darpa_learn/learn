# Copyright (c) 2019-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.
#

from logging import getLogger
import os
import subprocess
from collections import OrderedDict
import numpy as np
import torch
import io


from ..utils import to_cuda, restore_segmentation, concat_batches
from ..model.memory import HashingMemory


logger = getLogger()
torch.set_printoptions(profile="full")


def load_sentences(bpe_path):
    """
    - Load the sentences from the file located in bpe_path
    - Return a list of bpe-d sentences
    """
    sentences_bpe=[]
    with io.open(bpe_path,'rb') as f:
        for line in f:
            sentences_bpe.append(line)
            #sentences_bpe.append(line.encode('bytes'))
    # add </s> sentence delimiters
    sentences_bpe = [(('</s> %s </s>' % sent.strip()).split()) for sent in sentences_bpe]
    return sentences_bpe

def coverage_penalty(n_heads,n_layers,bs,slen,dec_attention,tlength):

    #dec_attention:(n_layers,bs,n_heads,slen,slen)
    
    t_ones = torch.ones([n_layers,bs,n_heads,slen],dtype=torch.float32).cuda()
    t_sum = torch.sum(dec_attention,dim=3).cuda()
    t_min = torch.min(t_sum,t_ones)
    
    t_min=torch.add(t_min,1e-12)

    t_log = torch.log(t_min)
    t_sum_log = torch.sum(t_log,dim=3)
    t_inv_x = torch.mean(dec_attention,dim=3) #(n_layers,bs,n_heads,slen)
    tst = torch.ones(t_sum_log.size(),dtype=torch.float32).cuda()

    tlen = tlength[:,:,:,0]     #view of the tlength tensor with sentence lengths
    covp = torch.div(t_sum_log,tlen)#!!!!divide by |x| not by slen

    del tst,t_sum,t_ones
    torch.cuda.empty_cache()

    return covp

def attention_distraction(n_heads,n_layers,bs,slen,dec_attention,tlength):
    #dec_attention:(n_layers,bs,n_heads,slen,slen)
    kurt_all = torch.zeros(n_layers,bs,n_heads,slen).cuda()
    tlen = tlength[:,:,:,0]
   

    for i in range(bs):
        tsub=torch.sub(dec_attention[:,:,:,i,:],tlength)
        t_pow2 = torch.pow(tsub,2)
        t_pow4 = torch.pow(t_pow2,2)
        tsum4 = torch.sum(t_pow4, dim =3)
        tsum2 = torch.sum(t_pow2, dim =3)
        tden = torch.pow(tsum2,2)
        tdiv = torch.div(tsum4,tden)
        krt = tdiv*tlen
        kurt_all[:,:,:,i] = krt
    cad_nom = -torch.sum(kurt_all,dim=3)
    cad = torch.div(cad_nom,tlen)

    return cad
        


def kl_score(x):
    # assert np.abs(np.sum(x) - 1) < 1e-5
    _x = x.copy()
    _x[x == 0] = 1
    return np.log(len(x)) + (x * np.log(_x)).sum()


def gini_score(x):
    # assert np.abs(np.sum(x) - 1) < 1e-5
    B = np.cumsum(np.sort(x)).mean()
    return 1 - 2 * B


def tops(x):
    # assert np.abs(np.sum(x) - 1) < 1e-5
    y = np.cumsum(np.sort(x))
    top50, top90, top99 = y.shape[0] - np.searchsorted(y, [0.5, 0.1, 0.01])
    return top50, top90, top99


class Evaluator(object):

    def __init__(self, trainer, data, params):
        """
        Initialize evaluator.
        """
        self.trainer = trainer
        self.data = data
        self.dico = data['dico']
        self.params = params
        self.memory_list = trainer.memory_list


    def get_iterator(self, data_set, lang1, lang2=None, stream=False,lst=None):
        """
        Create a new iterator for a dataset.
        """
        assert data_set in ['valid']
        assert lang1 in self.params.langs
        assert lang2 is None or lang2 in self.params.langs
        assert stream is False or lang2 is None

        # hacks to reduce evaluation time when using many languages
        if len(self.params.langs) > 30:
            eval_lgs = set(["ar", "bg", "de", "el", "en", "es", "fr", "hi", "ru", "sw", "th", "tr", "ur", "vi", "zh", "ab", "ay", "bug", "ha", "ko", "ln", "min", "nds", "pap", "pt", "tg", "to", "udm", "uk", "zh_classical"])
            eval_lgs = set(["ar", "bg", "de", "el", "en", "es", "fr", "hi", "ru", "sw", "th", "tr", "ur", "vi", "zh"])
            subsample = 10 if (data_set == 'test' or lang1 not in eval_lgs) else 5
            n_sentences = 600 if (data_set == 'test' or lang1 not in eval_lgs) else 1500
        elif len(self.params.langs) > 5:
            subsample = 10 if data_set == 'test' else 5
            n_sentences = 300 if data_set == 'test' else 1500
        else:
            #n_sentences = -1 if data_set == 'test' else 100
            n_sentences = -1
            subsample = 1

        if lang2 is None:
            if stream:
                iterator = self.data['mono_stream'][lang1][data_set].get_iterator(shuffle=False, subsample=subsample)
            else:
                iterator = self.data['mono'][lang1][data_set].get_iterator(
                    shuffle=False,
                    group_by_size=False,
                    n_sentences=n_sentences,
                    lst=lst
                )
        else:
            assert stream is False
            _lang1, _lang2 = (lang1, lang2) if lang1 < lang2 else (lang2, lang1)

            iterator = self.data['para'][(_lang1, _lang2)][data_set].get_iterator(
                shuffle=False,
                group_by_size=False,
                n_sentences=n_sentences,
                lst=lst
            )

        for batch in iterator:
            yield batch if lang2 is None or lang1 < lang2 else batch[::-1]
    
    def run_all_evals(self, trainer):
        """
        Run all evaluations.
        """
        params = self.params
        scores = OrderedDict({'epoch': trainer.epoch})
        scores1 = []
        scores2 = []

        with torch.no_grad():
            for data_set in ['valid']:
                # machine translation task (evaluate perplexity and accuracy)
                lang1=params.langs[0]
                lang2=params.langs[1]
                scores = self.evaluate_mt(scores, data_set, lang1,lang2)
            if params.n_gpu_per_node > 1 and params.split_data:
                scores_path_adapt = os.path.join(self.params.data_path,"scores_adapt"+str(params.local_rank)+".txt")
                scores_path_base = os.path.join(self.params.data_path,"scores_base"+str(params.local_rank)+".txt")
            else:
                if params.is_master:
                    scores_path_adapt = os.path.join(self.params.data_path,"scores_adapt0.txt")
                    scores_path_base = os.path.join(self.params.data_path,"scores_base0.txt")
                else:
                    torch.cuda.empty_cache()
                    return
            if os.path.isfile(scores_path_base):
                np.savetxt(scores_path_adapt,scores,fmt='%d')
            else:
                np.savetxt(scores_path_base,scores,fmt='%d')

        torch.cuda.empty_cache()
        return


class SingleEvaluator(Evaluator):

    def __init__(self, trainer, data, params):
        """
        Build language model evaluator.
        """
        super().__init__(trainer, data, params)
        self.model = trainer.model


class EncDecEvaluator(Evaluator):

    def __init__(self, trainer, data, params):
        """
        Build encoder / decoder evaluator.
        """
        super().__init__(trainer, data, params)
        self.encoder = trainer.encoder
        self.decoder = trainer.decoder

    def evaluate_mt(self, scores, data_set, lang1, lang2):
        """
        Evaluate perplexity and next word prediction accuracy.
        """
        params = self.params
        assert data_set in ['valid']
        assert lang1 in params.langs
        assert lang2 in params.langs

        self.encoder.eval()
        self.decoder.eval()
        encoder = self.encoder.module if params.multi_gpu else self.encoder
        decoder = self.decoder.module if params.multi_gpu else self.decoder

        params = params
        lang1_id = params.lang2id[lang1]
        lang2_id = params.lang2id[lang2]

        torch.set_printoptions(profile="full")
        iteration = 0
        n_sentences = len(self.data['para'][(lang1,lang2)][data_set])
        
        #cp_all: coverage penalty tensot
        cp_all = torch.cuda.FloatTensor(6,n_sentences,8).fill_(0)

        #krt_all: 
        #krt_all = torch.cuda.FloatTensor(6,n_sentences,8).fill_(0)

        for batch in self.get_iterator(data_set, lang1, lang2,lst=None):


            print("iteration = ",iteration)
            # generate batch
            (x1, len1), (x2, len2) = batch
            bs = x1.size()[1] #number of sentences in the batch
            slengths=len1 #lengths of all sentences in the batch
            slen_prev = max(slengths)    #maximum sentence length
            slen = 500
 
            found=False
            
            x2 = torch.LongTensor(slen, bs).fill_(0)
            for i in range(bs):
                if slen_prev.item()>=slen:
                    found=True
                    break
                x2[:slen_prev,i] = x1[:,i]
            if found:
                continue

            # target words to predict
            #alen = torch.arange(len2.max(), dtype=torch.long, device=len2.device)
            #pred_mask = alen[:, None] < len2[None] - 1  # do not predict anything given the last target word
            #y = x2[1:].masked_select(pred_mask[:-1])
            #assert len(y) == (len2 - 1).sum().item()

            # cuda
            #x1, len1, langs1, x2, len2, langs2, y = to_cuda(x1, len1, langs1, x2, len2, langs2, y)
            langs1 = x2.clone().fill_(lang1_id)
            x2,len1,langs1= to_cuda(x2, len1, langs1)
            # encode source sentence
            enc1 = encoder('fwd', x=x2, lengths=len1, langs=langs1, causal=False)
            #enc1 = enc1.transpose(0, 1)
            #enc1 = enc1.half() if params.fp16 else enc1

            # decode target sentence
            tensor,dec_attention=decoder('fwd', x=x2, src_enc=enc1, src_len=len1, lengths=len1, langs=langs1, causal=True)
            tr_tensor=torch.transpose(tensor,0,1)

            tlength=torch.zeros(params.n_layers,bs,params.n_heads,slen).cuda()
            
            for i in range(bs):
                tlength[:,i,:,:]=len1[i]
            
            if iteration ==0:
                prev_bs=0
            dec_attention=dec_attention.cuda()

            
            #COVERAGE PENALTY CALCULATIONS
            cp = coverage_penalty(params.n_heads,params.n_layers,bs,slen,dec_attention,tlength) #(n_layers,bs,n_heads)
            new_bs=prev_bs+bs
            cp_all[:,prev_bs:new_bs,:] = cp
            prev_bs=new_bs
            iteration+=1
                        
            """
            #KURTOSIS CALCULATIONS
            krt = attention_distraction(params.n_heads,params.n_layers,bs,slen,dec_attention,tlength)
            new_bs=prev_bs+bs
            krt_all[:,prev_bs:new_bs,:] = krt
            prev_bs=new_bs
            iteration+=1
            #print("krt ad = ", krt_all[:,:new_bs,:].size())
            """

        
        #SAVING ALL COVERAGE PENALTIES
        #torch.save(cp_all, 'coverage_penalty.pt')
        cp_mean_over_heads = torch.mean(cp_all.float(),dim=2)
        cp_last_layer = cp_mean_over_heads[5,:]
        #sorted_indices = torch.argsort(cp_last_layer,descending=True)
        lst = cp_last_layer.tolist()
        #lst = sorted_indices.tolist()
        #self.data['para'][(lang1,lang2)][data_set].sids = lst
        
        #SAVING ALL ATTENTION DISTRACTION SCORES
        """
        torch.save(krt_all, 'attention_distraction.pt')
        krt_mean_over_heads = torch.mean(krt_all.float(),dim=2)
        krt_last_layer = krt_mean_over_heads[5,:]
        sorted_indices = torch.argsort(krt_last_layer,descending=True)
        lst = sorted_indices.tolist()
        self.data['para'][(lang1,lang2)][data_set].sids = lst
        """

        """
        # define data paths
        score = 'ad'
        lang1_path = 'results'+score+'.ar-en.ar'

        # text sentences
        lang1_txt = []
        # convert to text


        for bts in self.get_iterator(data_set, lang1, lang2, lst=lst):
            (sent1, l1),(sent2, l2) = bts
            lang1_txt.extend(convert_to_text(sent1, l1, self.dico, params))

        # replace <unk> by <<unk>> as these tokens cannot be counted in BLEU
        #lang1_txt = [x.replace('<unk>', '<<unk>>') for x in lang1_txt]
        #lang2_txt = [x.replace('<unk>', '<<unk>>') for x in lang2_txt]

        # export hypothesis
        with open(lang1_path, 'w', encoding='utf-8') as f:
            f.write('\n'.join(lang1_txt) + '\n')

        # restore original segmentation
        restore_segmentation(lang1_path)
        """
        del encoder, decoder, tensor,dec_attention,cp_all,x2,len1,langs1,tlength
        torch.cuda.empty_cache()
        return lst




def convert_to_text(batch, lengths, dico, params):
    """
    Convert a batch of sentences to a list of text sentences.
    """
    batch = batch.cpu().numpy()
    lengths = lengths.cpu().numpy()

    slen, bs = batch.shape
    assert lengths.max() == slen and lengths.shape[0] == bs
    assert (batch[0] == params.eos_index).sum() == bs
    assert (batch == params.eos_index).sum() == 2 * bs
    sentences = []

    for j in range(bs):
        words = []
        for k in range(1, lengths[j]):
            if batch[k, j] == params.eos_index:
                break
            words.append(dico[batch[k, j]])
        sentences.append(" ".join(words))
    return sentences


