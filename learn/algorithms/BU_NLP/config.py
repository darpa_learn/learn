import scriptconfig as scfg


EPOCHS = [1,1,1,1,1,5,7,10,
          1,1,1,1,1,5,7,10]
TOKENS_PER_BATCH = [3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 
                    3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000]
EPOCH_SIZE = [30000, 30000, 30000, 60000, 60000, 60000, 100000, 200000, 
              30000, 30000, 30000, 60000, 60000, 60000, 100000, 200000]
MT_STEPS = [""]*1 + ["ar-en,en-ar"]*7 + ["ar-en,en-ar"]*8

AVG_SENT_LEN = 20
PARAM_FILE = "learn/algorithms/BU_NLP/params.pickle"


class Config(scfg.Config):
    """
    Default configuration for your algorithm

    Please add all of your default configuration parameters for your algorithm below
    with a description (help), on
        - what your parameter does
        - what changes when you use your parameter

    This is so that someone else can come and alter there parameters and know
    how this will affect the run of the algorithm

    """
    default = {
        'name': scfg.Value('NLP eXtreme',
                           help='name of the proposed method'),

        'batch_size': scfg.Value(32,
                                 help='number of sentences per batch.  '
                                      'Increasing will improve performace and decrease will make it worse.'),

        'num_workers': scfg.Value(16,
                                 help='number of workers in the dataloader'),

        'device': scfg.Value(-1,
                           help='what device to use (eg. 0 means gpu 0 and -1 means all available'),
        
        "dump_path": scfg.Value("./learn/algorithms/BU_NLP/dumped/",
                        help="Experiment dump path"),
        "exp_name": scfg.Value("unsupMT_aren",
                            help="Experiment name"),
        "save_periodic": scfg.Value(0,
                            help="Save the model periodically (0 to disable)"),
        "exp_id": scfg.Value("lwll",
                            help="Experiment ID"),

        # float16 / AMP API
        "fp16": scfg.Value(False,
                            help="Run model with float16"),
        "amp": scfg.Value(-1,
                            help="Use AMP wrapper for float16 / distributed / gradient accumulation. Level of optimization. -1 to disable."),

        # only use an encoder (use a specific decoder for machine translation),
        "encoder_only": scfg.Value(False,
                            help="Only use an encoder"),

        # model parameters
        "emb_dim": scfg.Value(1024,
                            help="Embedding layer size"),
        "n_layers": scfg.Value(6,
                            help="Number of Transformer layers"),
        "n_heads": scfg.Value(8,
                            help="Number of Transformer heads"),
        "dropout": scfg.Value(0.1,
                            help="Dropout"),
        "attention_dropout": scfg.Value(0.1,
                            help="Dropout in the attention layer"),
        "gelu_activation": scfg.Value(True,
                            help="Use a GELU activation instead of ReLU"),
        "share_inout_emb": scfg.Value(True,
                            help="Share input and output embeddings"),
        "sinusoidal_embeddings": scfg.Value(False,
                            help="Use sinusoidal embeddings"),
        "use_lang_emb": scfg.Value(True,
                            help="Use language embedding"),

        # memory parameters
        "use_memory": scfg.Value(False,
                            help="Use an external memory"),
#         if parser.parse_known_args()[0].use_memory:
#             HashingMemory.register_args(parser),
#             parser.add_argument("--mem_enc_positions": scfg.Value("",
#                                 help="Memory positions in the encoder ('4' for inside layer 4, '7,10+' for inside layer 7 and after layer 10)"),
#             parser.add_argument("--mem_dec_positions": scfg.Value("",
#                                 help="Memory positions in the decoder. Same syntax as `mem_enc_positions`."),

        # adaptive softmax
        "asm": scfg.Value(False,
                            help="Use adaptive softmax"),
#         if parser.parse_known_args()[0].asm:
#             parser.add_argument("--asm_cutoffs": scfg.Value("8000,20000",
#                                 help="Adaptive softmax cutoffs"),
#             parser.add_argument("--asm_div_value": scfg.Value(4,
#                                 help="Adaptive softmax cluster sizes ratio"),

        # causal language modeling task parameters
        "context_size": scfg.Value(0,
                            help="Context size (0 means that the first elements in sequences won't have any context),"),

        # masked language modeling task parameters
        "word_pred": scfg.Value(0.15,
                            help="Fraction of words for which we need to make a prediction"),
        "sample_alpha": scfg.Value(0,
                            help="Exponent for transforming word counts to probabilities (~word2vec sampling)"),
        "word_mask_keep_rand": scfg.Value("0.8,0.1,0.1", type=str,
                            help="Fraction of words to mask out / keep / randomize, among the words to predict"),

        # input sentence noise
        "word_shuffle": scfg.Value(0,
                            help="Randomly shuffle input words (0 to disable)"),
        "word_dropout": scfg.Value(0,
                            help="Randomly dropout input words (0 to disable)"),
        "word_blank": scfg.Value(0,
                            help="Randomly blank input words (0 to disable)"),

        # codes and vocab paths
        "vocab_path": scfg.Value("./learn/algorithms/BU_NLP/data/ar-en-pth/vocab.ar-en",
                            help="Vocab path"),
        "codes_path": scfg.Value("./learn/algorithms/BU_NLP/data/ar-en-pth/codes",
                            help="Codes path"),
        
        # data
        "data_path": scfg.Value("./learn/algorithms/BU_NLP/data/ar-en-pth/",
                            help="Data path"),
        "lgs": scfg.Value("ar-en",
                            help="Languages (lg1-lg2-lg3 .. ex: en-fr-es-de)"),
        "max_vocab": scfg.Value(200000,
                            help="Maximum vocabulary size (-1 to disable)"),
        "min_count": scfg.Value(0,
                            help="Minimum vocabulary count"),
        "lg_sampling_factor": scfg.Value(-1,
                            help="Language sampling factor"),

        # batch parameters
        "bptt": scfg.Value(256,
                            help="Sequence length"),
        "max_len": scfg.Value(250,
                            help="Maximum length of sentences (after BPE)"),
        "group_by_size": scfg.Value(True,
                            help="Sort sentences by size during the training"),
        "max_batch_size": scfg.Value(0,
                            help="Maximum number of sentences per batch (used in combination with tokens_per_batch, 0 to disable),"),
        "tokens_per_batch": scfg.Value(2000,
                            help="Number of tokens per batch"),

        # training parameters
        "split_data": scfg.Value(False,
                            help="Split data across workers of a same node"),
        "easy_run": scfg.Value(False,
                            help="Model skips scoring overall and trains only the first checkpoints."),
        "optimizer": scfg.Value("adam_inverse_sqrt,beta1=0.9,beta2=0.98,lr=0.0001", type=str,
                            help="Optimizer (SGD / RMSprop / Adam, etc.)"),
        "clip_grad_norm": scfg.Value(5,
                            help="Clip gradients norm (0 to disable)"),
        "epoch_size": scfg.Value(2000,
                            help="Epoch size / evaluation frequency (-1 for parallel data size)"),
        "max_epoch": scfg.Value(1,
                            help="Maximum epoch size"),
        "stopping_criterion": scfg.Value("", type=str,
                            help="Stopping criterion, and number of non-increase before stopping the experiment"),
        "validation_metrics": scfg.Value("test_ar-en_mt_ppl",
                            help="Validation metrics"),
        "accumulate_gradients": scfg.Value(1,
                            help="Accumulate model gradients over N iterations (N times larger batch sizes)"),

        # training coefficients
        "lambda_mlm": scfg.Value("1", type=str,
                            help="Prediction coefficient (MLM)"),
        "lambda_clm": scfg.Value("1", type=str,
                            help="Causal coefficient (LM)"),
        "lambda_pc": scfg.Value("1", type=str,
                            help="PC coefficient"),
        "lambda_ae": scfg.Value("1", type=str,
                            help="AE coefficient"),
        "lambda_mt": scfg.Value("1", type=str,
                            help="MT coefficient"),
        "lambda_bt": scfg.Value("1", type=str,
                            help="BT coefficient"),

        # training steps
        "clm_steps": scfg.Value("",
                            help="Causal prediction steps (CLM)"),
        "mlm_steps": scfg.Value("",
                            help="Masked prediction steps (MLM / TLM)"),
        "mt_steps": scfg.Value("ar-en,en-ar", type=str,
                            help="Machine translation steps"),
        "ae_steps": scfg.Value("",
                            help="Denoising auto-encoder steps"),
        "bt_steps": scfg.Value("ar-en-ar,en-ar-en", type=str,
                            help="Back-translation steps"),
        "pc_steps": scfg.Value("",
                            help="Parallel classification steps"),

        # reload pretrained embeddings / pretrained model / checkpoint
        "reload_emb": scfg.Value("",
                            help="Reload pretrained word embeddings"),
        "reload_model": scfg.Value("learn/algorithms/BU_NLP/pretrained/unsupMT_aren/6734835/best-valid_ar-en_mt_bleu.pth,learn/algorithms/BU_NLP/pretrained/unsupMT_aren/6734835/best-valid_ar-en_mt_bleu.pth", type=str,
                            help="Reload a pretrained model"),
        "reload_checkpoint": scfg.Value("",
                            help="Reload a checkpoint"),
        "adapt_checkpoint": scfg.Value("learn/algorithms/BU_NLP/pretrained/adapt/checkpoint_ep0_adapt.pth",
                            help="Reload a checkpoint for adaptation."),

        # beam search (for MT only),
        "beam_size": scfg.Value(1,
                            help="Beam size, default = 1 (greedy decoding)"),
        "length_penalty": scfg.Value(1,
                            help="Length penalty, values < 1.0 favor shorter sentences, while values > 1.0 favor longer ones."),
        "early_stopping": scfg.Value(False,
                            help="Early stopping, stop as soon as we have `beam_size` hypotheses, although longer ones may have better scores."),

        # evaluation
        "eval_bleu": scfg.Value(True,
                            help="Evaluate BLEU score during MT training"),
        "eval_only": scfg.Value(False,
                            help="Only run evaluations"),
        
        "eval_mt_only_ar_en": scfg.Value(True,
                            help="Only run evaluations"),
        "eval_single_set": scfg.Value("test",
                            help="Only run evaluations"),
        
        # debug
        "debug_train": scfg.Value(False,
                            help="Use valid sets for train sets (faster loading)"),
        "debug_slurm": scfg.Value(False,
                            help="Debug multi-GPU / multi-node within a SLURM job"),
        "debug": scfg.Value(False, 
                            help="Enable all debug flags"),

        # training script
        "training_script": scfg.Value("learn/algorithms/BU_NLP/train.py", type=str,
                            help="Number of gpus."),
        #scoring script

        "scoring_script": scfg.Value("learn/algorithms/BU_NLP/train_al.py", type=str,
                            help="Number of gpus."),  
        # multi-gpu / multi-node
        "n_gpus": scfg.Value(8,
                            help="Number of gpus."),
        "local_rank": scfg.Value(-1,
                            help="Multi-GPU - Local rank"),
        "master_port": scfg.Value(-1,
                            help="Master port (for multi-node SLURM jobs)")

    }
