from learn.algorithms.CLUE.query_adapter import QueryAdapter
import logging
import numpy as np
import ubelt as ub

import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from torch.utils.data import SequentialSampler, Subset
from torchvision import transforms

from learn.utils.dataset import ImageClassificationDataset
from learn.utils.memory import garbage_collection_cuda
from learn.utils.pretrained_networks import ImageClassificationNetwork


class QueryAlgo(QueryAdapter):
    def __init__(self, toolset):
        QueryAdapter.__init__(self, toolset)
        self.config = toolset['protocol_config']['active_learner']['params']
        
        device = self.config['device']
        if ub.iterable(device):
            self.device = device[0]
        else:
            self.device = device

    def initialize(self, source_network, source_dataset, target_dataset):
        """
        Initialize your algorithm and save any parameters or objects you want
        to be persistent between budget checkpoints.

        Args:
            source_network (ImageClassificationNetwork): Pytorch network used for data extraction and could
                be fine-tuned using the image classification algorithm (as with MME)
            source_dataset (ImageClassificationDataset): Pytorch dataset for source domain/task
            target_dataset: (ImageClassificationDataset): Pytorch dataset for target domain/task
        """
        logging.info('Initializing CLUE algorithm')

        self.source_dataset = source_dataset
        self.target_dataset = target_dataset
        self.source_network = source_network

    def select_and_label_data(self, budget):
        """
        Select the data to be labelled by the active learning algorithm
        """
        logging.info(f'Running CLUE algorithm with labeling budget of {budget}')
        # Only run if any unlabeled images left.
        if self.target_dataset.unlabeled_size > 0:
            #  ########### Query for labels ################

            # Extract out feature from target dataset for unlabeled indices
            unlabeled_indices = self.target_dataset.get_unlabeled_indices()
            """ Note: For CLUE we usually extract features from the penultimate layer of the network
                but in practice I have not observed a significant difference with using last layer features
                so have not modified that.
            """
            # Compute embeddings
            from sklearn.cluster import KMeans
            from sklearn.metrics.pairwise import euclidean_distances

            extracted_features, unlabeled_indices = self.extract_features(self.source_network,
                                                                          self.target_dataset,
                                                                          unlabeled_indices)

            # Compute entropy
            tgt_scores = nn.Softmax(dim=1)(torch.tensor(extracted_features)) + 1e-8
            sample_weights = -(tgt_scores*torch.log(tgt_scores)).sum(1).cpu().numpy()

            # Run weighted K-means over embeddings
            km = KMeans(min(budget, len(extracted_features)))
            km.fit(extracted_features, sample_weight=sample_weights)

            # Find nearest neighbors to inferred centroids
            dists = euclidean_distances(km.cluster_centers_, extracted_features)
            sort_idxs = dists.argsort(axis=1)
            indices_to_query = []
            ax, rem = 0, budget
            while rem > 0 and ax<sort_idxs.shape[1]:
                indices_to_query.extend(list(sort_idxs[:, ax][:rem]))
                indices_to_query = list(set(indices_to_query))
                rem = budget-len(indices_to_query)
                ax += 1

            indices_to_query = np.array(indices_to_query)

            # Here is where it will get you more labels.  The target dataset will be automatically labeled
            # so no need to do anything more.
            self.target_dataset.get_more_labels(indices_to_query)

        logging.info(f'Done running Clue algorithm for this budget level')

    def extract_features(self, network, dataset, subset_to_run=None, augmentation_style='test'):
        """ Extract Features for network

        Args:
            network (ImageClassificationNetwork): network to extract layers from
            dataset (ImageClassificationDataset): image dataset to extract images
            subset_to_run (list[int]): indices to run.  If None, run all.
            augmentation_style (str): 'test' or 'train for what type of augmentations you want

        Returns:
            tuple(np.array, np.array): 2D array for features with obs, feats dim order and another one for
                the corresponding indices

        """

        logging.info(f'Extracting features on {dataset.name} using network {network.model_name}')

        crop_size = 224
        if augmentation_style == 'test':
            dataset.transform = transforms.Compose([
                transforms.Resize(256),
                transforms.CenterCrop(crop_size),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        elif augmentation_style == 'train':
            dataset.transform = transforms.Compose([
                transforms.Resize(256),
                transforms.RandomHorizontalFlip(),
                transforms.RandomCrop(crop_size),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        else:
            raise NotImplementedError(f'"{augmentation_style}" augmentation style not implemented')
        dataset_to_run = dataset
        if subset_to_run is not None:
            dataset_to_run = Subset(dataset, subset_to_run)

        dataloader = DataLoader(dataset_to_run,
                                sampler=SequentialSampler(dataset_to_run),
                                batch_size=self.config['batch_size'],
                                num_workers=self.config['num_workers'],
                                #collate_fn=dataset.collate_batch,
                                drop_last=False)

        network.set_as_feature_extractor()
        network = network.eval().to(self.device)

        features = []
        indices = []
        with torch.no_grad():
            for im, _, ind in ub.ProgIter(dataloader):
                im = im.to(self.device)
                feat = network(im)
                features.append(feat.cpu().detach().numpy())
                indices.append(ind.cpu().detach().numpy())

        # Deallocate GPU space so other networks can use it
        network = network.cpu()

        garbage_collection_cuda()
        del dataloader
        return np.concatenate(features), np.concatenate(indices)
