import torch
from .imageClassificationAdapter import ImageClassifierAdapter
import torch.utils.data as data
from .config import Config
import logging


class ImageClassifierAlgorithm(ImageClassifierAdapter):

    def __init__(self, toolset):
        # def __init__(self, problem, base_dataset, adapt_dataset, arguments):
        ImageClassifierAdapter.__init__(self, toolset)

        # Load the default configuration from config.py for your algorithm
        self.config = Config()
        self.update_parameters(toolset['protocol_config'])

        # This sends the configuration back to the toolset
        self.toolset['classification_config'] = self.config

    def update_parameters(self, protocol_config):
        """  Update parameters for algorithm based on protocol config

        Args:
            protocol_config (dict):  Dictionary of parameters.

        Returns:
            None
        """
        common_keyword = set(protocol_config.keys()) & set(self.config.keys())
        for ck in common_keyword:
            self.config[ck] = protocol_config[ck]

    def initialize(self, source_network, source_dataset, whitelist_datasets):
        """  Initialize your algorithm and save any parameters or objects you want to be persistent between
        budget checkpoints.  DomainNetworkSelection will choose a potential source datasets `source_dataset`
        and provide a trained network for that dataset `source_dataset.` Please do not modify the datasets
        beyond overriding the transforms (self.transform and self.transform_target are fine to change).

        Args:
            source_network (pytorch.nn.Module):  Pretrained network on source task
            source_dataset (framework.dataset): Pytorch dataset for source task
            whitelist_datasets (dict[framework.dataset]): Dictionary of all possible external datasets which
                can be used on this task.  You may use this if you want to select a different dataset than
                the one selected as source.

        """

        self.source_model = source_network
        self.source_dataset = source_dataset
        logging.info(f"{whitelist_datasets.keys()}")

    def domain_adapt_training(self, target_dataset, eval_dataset):
        """ This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
        the dataloaders below and feel free to override the transformers in the datasets to use whatever
        pretraining you desire. Please do not modify the datasets though.

        Args:
            target_dataset (object): pytorch dataset for the target dataset used for training
            eval_dataset (object): eval dataset used for evaluation.  Included here in case you want to do
                transductive learning.
        """

        # ###################  Creating the Labeled DataLoader ###############
        # Create Dataloaders for labeled data by getting the labeled indices and
        # creating a random sub-sampler.  Then create the dataloader using this
        # sampler
        #
        #  Note: To change the transformer in the JPLDataset dataset, edit
        #      current_dataset.transform.  You can also edit the labels during
        #      loading by editing the  current_dataset.target_transform
        #
        #  Note: Another approach could be to inherit the JPLDataset class and
        #      override the __get_item__ function
        #
        #  Check out dataset.py for more information
        #     Notes: There could be NO unlabeled data and the algorithm has to be
        #         able to handle that case!
        #
        #         The min batch size here is so that the batch size isn't
        #         larger than the labeled/unlabeled dataset which causes the
        #         dataloader to hang

        labeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
            self.toolset["target_dataset"].get_labeled_indices()
        )

        labeled_dataloader = torch.utils.data.DataLoader(
            self.toolset["target_dataset"],
            sampler=labeled_sampler,
            batch_size=min(self.toolset["target_dataset"].labeled_size,
                           int(self.config["batch_size"])
                           ),
            num_workers=int(self.config['num_workers']),
            collate_fn=self.toolset["target_dataset"].collate_batch,
            drop_last=True,
        )

        # ###################  Creating the Unlabeled DataLoader ###############
        #  Same as labeled dataaset but for unlabeled indices
        #   Except skip if no unlabeled
        if self.toolset["target_dataset"].unlabeled_size > 0:
            unlabeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
                self.toolset["target_dataset"].get_unlabeled_indices()
            )
            unlabeled_dataloader = torch.utils.data.DataLoader(
                self.toolset["target_dataset"],
                sampler=unlabeled_sampler,
                batch_size=min(self.toolset["target_dataset"].unlabeled_size,
                               int(self.config["batch_size"])
                               ),
                num_workers=int(self.config['num_workers']),
                drop_last=False,
            )

        # ##################### fine-tune/train your model ######################
        #  Here is where you will actual finetune the model
        #  You can use the pretrained model with self.model
        #  Make sure to set any persistent tensors to cpu with `tensor.cpu` command

    def inference(self, eval_dataset):
        """
        Inference is during the evaluation stage.  For this example, the
        task_network is trained in the train and adapt stage's code and
        is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to
        JPL

        Args:
            eval_dataset (framework.dataset): unlabeled evaluation framework.  We need to predict the classes
                for each item in the dataset.  Example below.

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        # self.task_model.eval()
        # eval_dataloader = data.DataLoader(
        #     toolset["Dataset"],
        #     batch_size=int(self.arguments["batch_size"]),
        #     drop_last=False
        # )
        # preds = []
        # indices = []
        #
        # for imgs, inds in eval_dataloader:
        #     if self.cuda:
        #         imgs = imgs.cuda()
        #
        #     with torch.no_grad():
        #         preds_ = self.task_model(imgs)
        #
        #     preds += torch.argmax(preds_, dim=1).cpu().numpy().tolist()
        #     indices += inds.numpy().tolist()

        preds, indices = self.toolset["eval_dataset"].dummy_data(
            'image_classification')
        return preds, indices
