import torch
import torch.nn as nn
import torch.nn.functional as F

from .utils import compute_logits


class Classifier(nn.Module):
    
    def __init__(self, encoder, feat_dim, n_classes):
        super().__init__()
        self.encoder = encoder
        self.classifier = nn.Linear(feat_dim, n_classes)

    def forward(self, x):
        x = self.encoder(x)
        x = self.classifier(x)
        return x


class MetaBaseline(nn.Module):

    def __init__(self, encoder, method='cos',
                 temp=10., temp_learnable=True):
        super().__init__()
        self.encoder = encoder
        self.method = method

        if temp_learnable:
            self.temp = nn.Parameter(torch.tensor(temp))
        else:
            self.temp = temp

    def forward(self, x_shot, x_query):
        # x_shot: (bs, n, k, hwc)
        # x_query: (bs, n * k, hwc)
        
        shot_shape = x_shot.shape[:-3]
        query_shape = x_query.shape[:-3]
        img_shape = x_shot.shape[-3:]

        x_shot = x_shot.view(-1, *img_shape)
        x_query = x_query.view(-1, *img_shape)
        x_tot = self.encoder(torch.cat([x_shot, x_query], dim=0))
        x_shot, x_query = x_tot[:len(x_shot)], x_tot[-len(x_query):]
        x_shot = x_shot.view(*shot_shape, -1)
        x_query = x_query.view(*query_shape, -1)

        if self.method == 'cos':
            x_shot = x_shot.mean(dim=-2)
            x_shot = F.normalize(x_shot, dim=-1)
            x_query = F.normalize(x_query, dim=-1)
            metric = 'dot'
        elif self.method == 'sqr':
            x_shot = x_shot.mean(dim=-2)
            metric = 'sqr'

        logits = compute_logits(
                x_query, x_shot, metric=metric, temp=self.temp)
        return logits
