import os

import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import transforms
from torch.utils.data import DataLoader
import gc
import ubelt as ub
import numpy as np

import learn.algorithms.MetaBaseline.utils as utils
import learn.algorithms.MetaBaseline.utils.few_shot as fs
from learn.algorithms.MetaBaseline.imageClassificationAdapter import ImageClassifierAdapter
from learn.algorithms.MetaBaseline.datasets import WrapDataset, CategoriesSampler
from learn.algorithms.MetaBaseline.models import Classifier, MetaBaseline
from learn.utils.dataset import ImageClassificationDataset
import logging


class ImageClassifierAlgorithm(ImageClassifierAdapter):
    """
    Note: I updated for device rather than cuda quickly, need to test to make sure this still works.
    """

    def __init__(self, toolset):
        # def __init__(self, problem, base_dataset, adapt_dataset, arguments):
        ImageClassifierAdapter.__init__(self, toolset)

        self.config = toolset['protocol_config']['image_classifier']['params']

        device = self.config['device']
        if ub.iterable(device):
            self.device = device[0]
        else:
            self.device = device

    @staticmethod
    def _get_dataset_labels(dataset):
        ret = []
        for i in range(len(dataset)):
            ret.append(int(dataset[i][1]))
        return ret

    def initialize(self, source_network, source_dataset, whitelist_datasets, target_dataset):
        """  Initialize your algorithm and save any parameters or objects you want to be persistent between
        budget checkpoints.  DomainNetworkSelection will choose a potential source datasets `source_dataset`
        and provide a trained network for that dataset `source_dataset.` Please do not modify the datasets
        beyond overriding the transforms (self.transform and self.transform_target are fine to change).

        Args:
            source_network (pytorch.nn.Module):  Pretrained network on source task
            source_dataset (ImageClassificationDataset): Pytorch dataset for source domain/task
            whitelist_datasets (dict[ImageClassificationDataset]): Dictionary of all possible external datasets which
                can be used on this task.  You may use this if you want to select a different dataset than
                the one selected as source.
            target_dataset: (ImageClassificationDataset): Pytorch dataset for target domain/task

        """
        logging.info('Initializing Metabaseline Algorithm')
        self.encoder = source_network
        self.encoder.set_as_feature_extractor()

        _gpus = self.device
        self._parallel = ub.iterable(_gpus) and (len(_gpus) > 1)

        self.n_base_classes = len(source_dataset.categories)

        # Took out pretraining since source network is already pretrained on source dataset
        trans = transforms.Compose([
            transforms.RandomResizedCrop(224),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])
        source_dataset.transform = trans

        #self.classification_pretraining(source_dataset)

        if self.config['meta_data_aug']:
            logging.info('meta_aug is on')
            trans = transforms.Compose([
                transforms.RandomResizedCrop(224),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        else:
            trans = transforms.Compose([
                transforms.Resize(224),
                transforms.CenterCrop(224),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        source_dataset.transform = trans

        self.meta_learning(source_dataset)

    def classification_pretraining(self, source_dataset):
        #### Dataset ####
        labeled_dataset = WrapDataset(
            source_dataset,
            indices=source_dataset.get_labeled_indices(),
        )
        labeled_dataloader = DataLoader(
            labeled_dataset,
            batch_size=256,
            shuffle=True,
            num_workers=self.config['num_workers'],
            collate_fn=source_dataset.collate_batch,
            drop_last=False,
        )

        ########
        feat_dim = self.encoder.num_feature_dims
        n_classes = self.n_base_classes
        model = Classifier(self.encoder, feat_dim, n_classes).to(self.device)
        if self._parallel:
            model = nn.DataParallel(model)

        optimizer = torch.optim.SGD(model.parameters(),
            lr=0.1, weight_decay=1.e-4, momentum=0.9)
        lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min')

        ########

        train_loader = labeled_dataloader

        logging.info('start classification pre-training ...')

        epoch = 0
        while True:
            cur_lr = optimizer.param_groups[0]['lr']
            if cur_lr < 0.001 - 1e-9:
                break

            epoch += 1

            tl, ta = utils.Averager(), utils.Averager()

            # train
            model.train()
            for data, label in train_loader:
                data, label = data.to(self.device), label.to(self.device)
                logits = model(data)
                loss = F.cross_entropy(logits, label)
                acc = utils.compute_acc(logits, label)
                tl.add(loss.item())
                ta.add(acc)

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                logits = None
                loss = None

            lr_scheduler.step(tl.item())

            logging.info('epoch {}, train: loss={:.4f} acc={:.4f}, lr={}'.format(
                epoch, tl.item(), ta.item(), cur_lr))

        if self._parallel:
            model = model.module
        self.encoder = model.encoder.cpu()

    def meta_learning(self, source_dataset):
        #### Dataset ####

        labeled_dataset = WrapDataset(
            source_dataset,
            indices=source_dataset.get_labeled_indices(),
        )

        label_list = np.array(source_dataset.targets)[source_dataset.get_labeled_indices()].tolist()
        n_way, n_shot = 5, 5  ##
        n_query = 15  ##
        # use the mini
        #_, counts = np.unique(label_list, return_counts=True)
        #n_query = min(counts.min()-n_shot, n_query)

        if n_query < 1:
            raise ValueError('There is a category with too few labels to run this algorithm.  Need at '
                             f'least {n_way+1} labels in source categories')
        logging.info(f'Using n_query: {n_query} and n_way: {n_way}')

        n_train_way = n_way
        n_train_shot = n_shot
        ep_per_batch = self.config['meta_batch_size']

        train_dataset = labeled_dataset
        train_sampler = CategoriesSampler(
            label_list, self.config['meta_epoch_size'],
            n_train_way, n_train_shot + n_query,
            ep_per_batch=ep_per_batch)
        train_loader = DataLoader(train_dataset, batch_sampler=train_sampler,
                                  num_workers=self.config['num_workers'])
        ########

        #### Model and optimizer ####

        model = MetaBaseline(self.encoder).to(self.device)
        if self._parallel:
            model = nn.DataParallel(model)

        optimizer, lr_scheduler = utils.make_optimizer(
            model.parameters(), 'sgd', **{
                'lr': self.config['meta_lr'],
                'weight_decay': 1.e-4
            }
        )

        ########

        max_epoch = self.config['meta_max_epoch']

        logging.info('start meta-learning ...')

        for epoch in range(1, max_epoch + 1):
            tl, ta = utils.Averager(), utils.Averager()

            # train
            model.train()
            if self.config['meta_freeze_bn']:
                utils.freeze_bn(model)

            for data, _ in train_loader:
                data = data.to(self.device)
                x_shot, x_query = fs.split_shot_query(
                    data, n_train_way, n_train_shot, n_query,
                    ep_per_batch=ep_per_batch)
                label = fs.make_nk_label(n_train_way, n_query,
                                         ep_per_batch=ep_per_batch)
                label = label.to(self.device)

                logits = model(x_shot, x_query).view(-1, n_train_way)
                loss = F.cross_entropy(logits, label)
                acc = utils.compute_acc(logits, label)
                tl.add(loss.item())
                ta.add(acc)

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                logits = None
                loss = None

            logging.info('epoch {}, meta-train: loss={:.4f} acc={:.4f}'.format(
                epoch, tl.item(), ta.item()))

        if self._parallel:
            model = model.module
        self.encoder = model.encoder.cpu()

    def compute_protos(self, target_dataset):
        labeled_dataset = WrapDataset(
            target_dataset,
            indices=target_dataset.get_labeled_indices(),
        )
        labeled_dataloader = DataLoader(
            labeled_dataset,
            batch_size=self.config['eval_batch_size'],
            num_workers=self.config['num_workers'],
            collate_fn=target_dataset.collate_batch,
            drop_last=False,
        )

        encoder = self.encoder
        encoder.eval()
        if self._parallel:
            encoder = nn.DataParallel(encoder)

        protos = dict()
        cnt = dict()
        n_classes = -1
        encoder = encoder.to(self.device)

        with torch.no_grad():

            for data, label in labeled_dataloader:
                data = data.to(self.device)
                feat = encoder(data)
                for i, lb in enumerate(label):
                    lb = int(lb)
                    if protos.get(lb) is None:
                        protos[lb] = feat[i]
                        cnt[lb] = 1.0
                    else:
                        protos[lb] += feat[i]
                        cnt[lb] += 1.0
                    if lb + 1 > n_classes:
                        n_classes = lb + 1

            _protos = []
            for c in range(n_classes):
                _protos.append(protos[c] / cnt[c])
            self.protos = torch.stack(_protos)
            self.protos = F.normalize(self.protos, dim=-1)

        self.encoder = encoder.cpu()

    def domain_adapt_training(self, target_dataset, eval_dataset):
        """ This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
        the dataloaders below and feel free to override the transformers in the datasets to use whatever
        pretraining you desire. Please do not modify the datasets though.

        Args:
            target_dataset (ImageClassificationDataset): pytorch dataset for the target dataset used for training
            eval_dataset (ImageClassificationDataset): eval dataset used for evaluation.  Included here in case you want to do
                transductive learning.
        """
        logging.info('Few-shot Adaption with Metabaseline Algorithm')
        trans = transforms.Compose([
            transforms.Resize(224),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])
        target_dataset.transform = trans
        eval_dataset.transform = trans
        self.compute_protos(target_dataset)
        logging.info('Few-shot Adaption with Metabaseline Algorithm (done)')

        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()

    def inference(self, eval_dataset):
        """
        Inference is during the evaluation stage.  For this example, the task_network is trained in the
        train and adapt stage's code and is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to JPL

        Args:
            eval_dataset (ImageClassificationDataset): unlabeled evaluation framework.
                We need to predict the classes for each item in the dataset.

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        logging.info('MetaBaseline inference')

        encoder = self.encoder

        trans = transforms.Compose([
            transforms.Resize(224),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])
        eval_dataset.transform = trans

        encoder = encoder.to(self.device)
        protos = self.protos.to(self.device)
        encoder.eval()
        if self._parallel:
            encoder = nn.DataParallel(encoder)

        eval_dataset_wrapped = WrapDataset(
            eval_dataset,
            is_eval=True
        )
        eval_dataloader = DataLoader(
            eval_dataset_wrapped,
            batch_size=self.config['eval_batch_size'],
            num_workers=self.config['num_workers'],
            collate_fn=eval_dataset.collate_batch,
            drop_last=False
        )

        preds = []
        indices = []
        with torch.no_grad():
            for data, inds in eval_dataloader:
                data = data.to(self.device)
                feat = self.encoder(data)
                feat = F.normalize(feat, dim=-1)
                logits = torch.mm(feat, protos.t())
                preds += torch.argmax(logits, dim=-1).cpu().tolist()
                indices += inds.tolist()

        logging.info('MetaBaseline inference (done)')

        self.encoder.to('cpu')
        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()

        return preds, indices
