import scriptconfig as scfg


class Config(scfg.Config):
    """
    Default configuration for meta-baseline
    """
    default = {
        'name': scfg.Value('meta-baseline',
                help='name of the proposed method'),

        'meta_data_aug': scfg.Value(False,
                help='augment data in meta-learning stage'),
        'meta_batch_size': scfg.Value(1,
                help='batch size in meta-learning stage'),
        'meta_epoch_size': scfg.Value(100,
                help='num of batches in each epoch in meta-learning stage'),
        'meta_lr': scfg.Value(0.001,
                help='lr in meta-learning stage'),
        'meta_max_epoch': scfg.Value(10,
                help='max epoch in meta-learning stage'),
        'meta_freeze_bn': scfg.Value(True,
                help='freezing the BN layers in meta-learning stage'),

        'eval_batch_size': scfg.Value(512,
                help='batch size in evaluation'),

        'device': scfg.Value(0,
                help='what device to use (eg. 0 means gpu 0'),
        'num_workers': scfg.Value(8,
                help='number of workers in the dataloader'),
    }
