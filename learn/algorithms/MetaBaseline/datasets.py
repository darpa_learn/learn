import torch
import numpy as np
from torch.utils.data.dataset import Dataset
from torchvision import transforms


class WrapDataset(Dataset):

    def __init__(self, dataset, indices=None, is_eval=False):
        self.dataset = dataset
        if indices is None:
            indices = list(range(len(dataset)))
        self.indices = indices
        #self.transform = transform
        self.is_eval = is_eval

    def __getitem__(self, i):
        """ Get item command for the wrapped dataset.  Returns label if self.is_eval is false and index of
        self.is_eval is true.

        Args:
            i: index of image in wrapped dataset.

        Returns:

        """
        if self.is_eval:
            data, _, label = self.dataset[self.indices[i]]
        else:
            data, label, _ = self.dataset[self.indices[i]]
        #if self.transform is not None:
        #    data = transforms.ToPILImage()(data)
        #    data = self.transform(data)
        return data, label

    def __len__(self):
        return len(self.indices)


class CategoriesSampler:
    def __init__(self, label, n_batch, n_cls, n_per, ep_per_batch=1):
        self.n_batch = n_batch
        self.n_cls = n_cls
        self.n_per = n_per
        self.ep_per_batch = ep_per_batch

        label = np.array(label)
        self.catlocs = []
        for c in range(max(label) + 1):
            ls = np.argwhere(label == c).reshape(-1)
            if len(ls) >= n_per:
                self.catlocs.append(ls)

    def __len__(self):
        return self.n_batch
    
    def __iter__(self):
        for _ in range(self.n_batch):
            batch = []
            for _ in range(self.ep_per_batch):
                episode = []
                classes = np.random.choice(len(self.catlocs), self.n_cls,
                                           replace=False)
                for c in classes:
                    l = np.random.choice(self.catlocs[c], self.n_per,
                                         replace=False)
                    episode.append(torch.from_numpy(l))
                episode = torch.stack(episode)
                batch.append(episode)
            batch = torch.stack(batch) # bs * n_cls * n_per
            yield batch.view(-1)

