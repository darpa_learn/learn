import torch
import logging
from datetime import datetime
import time
import torch.nn as nn
import torchvision.utils
import torch.utils.data as data
import torchvision.transforms as transforms
from pathlib import Path
from argparse import Namespace

from learn.algorithms.pretrainEfficientnet.imageClassificationAdapter import ImageClassifierAdapter
from learn.algorithms.pretrainEfficientnet.config import Config
from learn.utils.memory import garbage_collection_cuda

try:
    from apex import amp
    from apex.parallel import DistributedDataParallel as DDP
    from apex.parallel import convert_syncbn_model
    has_apex = True
except ImportError:
    from torch.nn.parallel import DistributedDataParallel as DDP
    has_apex = False

from timm.data import create_loader, resolve_data_config, FastCollateMixup, Mixup, AugMixDataset
from timm.models import create_model, resume_checkpoint, convert_splitbn_model
from timm.utils import *
from timm.loss import LabelSmoothingCrossEntropy, SoftTargetCrossEntropy, JsdCrossEntropy
from timm.optim import create_optimizer
from timm.scheduler import create_scheduler


class ImageClassifierAlgorithm(ImageClassifierAdapter):

    def __init__(self, toolset):
        # def __init__(self, problem, base_dataset, adapt_dataset, arguments):
        ImageClassifierAdapter.__init__(self, toolset)

        # Load the default configuration from config.py for your algorithm
        self.config = Config()
        self.update_parameters(toolset['protocol_config'])

        # This sends the configuration back to the toolset
        self.toolset['classification_config'] = self.config

    def update_parameters(self, protocol_config):
        """  Update parameters for algorithm based on protocol config

        Args:
            protocol_config (dict):  Dictionary of parameters.

        Returns:
            None
        """
        common_keyword = set(protocol_config.keys()) & set(self.config.keys())
        for ck in common_keyword:
            self.config[ck] = protocol_config[ck]

    def initialize(self, source_network, source_dataset, whitelist_datasets):
        """  Initialize your algorithm and save any parameters or objects you want to be persistent between
        budget checkpoints.  DomainNetworkSelection will choose a potential source datasets `source_dataset`
        and provide a trained network for that dataset `source_dataset.` Please do not modify the datasets
        beyond overriding the transforms (self.transform and self.transform_target are fine to change).

        Args:
            source_network (torch.nn.Module):  Pretrained network on source task
            source_dataset (framework.datasets.ImageClassificationDataset): Pytorch dataset for source
                domain/task
            whitelist_datasets (dict[str, framework.datasets.ImageClassificationDataset]): Dictionary of all
                possible external datasets which can be used on this task.  You may use this if you want to
                select a different dataset than the one selected as source.
        """
        train_dataset = source_dataset
        num_classes = len(train_dataset.categories)
        self.config["num_classes"] = num_classes
        self.prefetcher = not self.config["no_prefetcher"]
        # pre-trained network on source task (the closest task)
        #  You are not required to use it but strongly suggested to use it
        #  These are in the style of torchvision models
        torch.manual_seed(self.config['seed'])
        model = create_model(
                            self.config["model"],
                            pretrained=self.config["pretrained"],
                            num_classes=num_classes,
                            drop_rate=self.config["drop"],
                            drop_connect_rate=self.config["drop_connect"],  # DEPRECATED, use drop_path
                            drop_path_rate=self.config["drop_path"],
                            drop_block_rate=self.config["drop_block"],
                            global_pool=self.config["gp"],
                            bn_tf=self.config["bn_tf"],
                            bn_momentum=self.config["bn_momentum"],
                            bn_eps=self.config["bn_eps"])

        data_config = resolve_data_config(self.config.to_dict(), model=model,
                                          verbose=self.config["local_rank"] == 0)
        num_aug_splits = 0
        if self.config["aug_splits"] > 0:
            assert self.config["aug_splits"] > 1, 'A split of 1 makes no sense'
            num_aug_splits = self.config["aug_splits"]

        if self.config["split_bn"]:
            assert num_aug_splits > 1 or self.config["resplit"]
            model = convert_splitbn_model(model, max(num_aug_splits, 2))

        if self.config["num_gpu"] > 1:
            if self.config["amp"]:
                logging.warning(
                    'AMP does not work well with nn.DataParallel, disabling. Use distributed mode for multi-GPU AMP.')
                self.config["amp"] = False
            model = nn.DataParallel(model, device_ids=list(range(self.config["num_gpu"]))).cuda()
        else:
            model.cuda()

        # Create a namespace from scriptconfig objects since create optimizer
        # uses attributes to get the parameters
        # Refer to https://stackoverflow.com/a/24017468 for info about conversion
        self.config_attr = Namespace(**self.config)
        self.config_attr.categories = source_dataset.categories
        self.config_attr.prefetcher = self.prefetcher
        optimizer = create_optimizer(self.config_attr, model)

        use_amp = False
        if has_apex and self.config["amp"]:
            model, optimizer = amp.initialize(model, optimizer, opt_level='O1')
            use_amp = True
        if self.config["local_rank"] == 0:
            logging.info('NVIDIA APEX {}. AMP {}.'.format(
                'installed' if has_apex else 'not installed', 'on' if use_amp else 'off'))

        # optionally resume from a checkpoint
        resume_state = {}
        resume_epoch = None
        if self.config["resume"]:
            resume_state, resume_epoch = resume_checkpoint(model, self.config["resume"])
        if resume_state and not self.config["no_resume_opt"]:
            if 'optimizer' in resume_state:
                if self.config['local_rank'] == 0:
                    logging.info('Restoring Optimizer state from checkpoint')
                optimizer.load_state_dict(resume_state['optimizer'])
            if use_amp and 'amp' in resume_state and 'load_state_dict' in amp.__dict__:
                if self.config["local_rank"] == 0:
                    logging.info('Restoring NVIDIA AMP state from checkpoint')
                amp.load_state_dict(resume_state['amp'])
        del resume_state

        model_ema = None
        if self.config["model_ema"]:
            # Important to create EMA model after cuda(), DP wrapper, and AMP but before SyncBN and DDP wrapper
            model_ema = ModelEma(
                model,
                decay=self.config["model_ema_decay"],
                device='cpu' if self.config["model_ema_force_cpu"] else '',
                resume=self.config["resume"])

        if self.config["distributed"]:
            if self.config["sync_bn"]:
                assert not self.config["split_bn"]
                try:
                    if has_apex:
                        model = convert_syncbn_model(model)
                    else:
                        model = torch.nn.SyncBatchNorm.convert_sync_batchnorm(model)
                    if self.config["local_rank"] == 0:
                        logging.info(
                            'Converted model to use Synchronized BatchNorm. WARNING: You may have issues if using '
                            'zero initialized BN layers (enabled by default for ResNets) while sync-bn enabled.')
                except Exception as e:
                    logging.error('Failed to enable Synchronized BatchNorm. Install Apex or Torch >= 1.1')
            if has_apex:
                model = DDP(model, delay_allreduce=True)
            else:
                if self.config["local_rank"] == 0:
                    logging.info("Using torch DistributedDataParallel. Install NVIDIA Apex for Apex DDP.")
                model = DDP(model, device_ids=[self.config["local_rank"]])  # can use device str in Torch >= 1.1
            # NOTE: EMA model does not need to be wrapped by DDP

        lr_scheduler, num_epochs = create_scheduler(self.config_attr, optimizer)
        start_epoch = 0
        if self.config["start_epoch"] is not None:
            # a specified start_epoch will always override the resume epoch
            start_epoch = self.config["start_epoch"]
        elif resume_epoch is not None:
            start_epoch = resume_epoch
        if lr_scheduler is not None and start_epoch > 0:
            lr_scheduler.step(start_epoch)

        if self.config["local_rank"] == 0:
            logging.info('Scheduled epochs: {}'.format(num_epochs))

        collate_fn = None
        if self.prefetcher and self.config["mixup"] > 0:
            assert not num_aug_splits  # collate conflict (need to support deinterleaving in collate mixup)
            collate_fn = FastCollateMixup(self.config["mixup"],
                                          self.config["smoothing"],
                                          num_classes)

        if num_aug_splits > 1:
            source_dataset = AugMixDataset(source_dataset, num_splits=num_aug_splits)
        train_interpolation = self.config["train_interpolation"]
        if self.config["no_aug"] or not train_interpolation:
            train_interpolation = data_config['interpolation']
        loader_train = create_loader(
            source_dataset,
            input_size=data_config['input_size'],
            batch_size=self.config["batch_size"],
            is_training=True,
            use_prefetcher=self.prefetcher,
            re_prob=self.config["reprob"],
            re_mode=self.config["remode"],
            re_count=self.config["recount"],
            re_split=self.config["resplit"],
            color_jitter=self.config["color_jitter"],
            auto_augment=self.config["aa"],
            num_aug_splits=num_aug_splits,
            interpolation=train_interpolation,
            mean=data_config['mean'],
            std=data_config['std'],
            num_workers=self.config["workers"],
            distributed=self.config["distributed"],
            collate_fn=collate_fn,
            pin_memory=self.config["pin_mem"],
            use_multi_epochs_loader=self.config["use_multi_epochs_loader"]
        )

        dataset_eval = whitelist_datasets[f"{train_dataset.name}_test"]

        loader_eval = create_loader(
            dataset_eval,
            input_size=data_config['input_size'],
            batch_size=self.config["validation_batch_size_multiplier"] * self.config["batch_size"],
            is_training=False,
            use_prefetcher=self.prefetcher,
            interpolation=data_config['interpolation'],
            mean=data_config['mean'],
            std=data_config['std'],
            num_workers=self.config["workers"],
            distributed=self.config["distributed"],
            crop_pct=data_config['crop_pct'],
            pin_memory=self.config["pin_mem"],
        )

        if self.config["jsd"]:
            assert num_aug_splits > 1  # JSD only valid with aug splits set
            train_loss_fn = JsdCrossEntropy(num_splits=num_aug_splits,
                                            smoothing=self.config["smoothing"]).cuda()
            validate_loss_fn = nn.CrossEntropyLoss().cuda()
        elif self.config["mixup"] > 0.:
            # smoothing is handled with mixup label transform
            train_loss_fn = SoftTargetCrossEntropy().cuda()
            validate_loss_fn = nn.CrossEntropyLoss().cuda()
        elif self.config["smoothing"]:
            train_loss_fn = LabelSmoothingCrossEntropy(smoothing=self.config["smoothing"]).cuda()
            validate_loss_fn = nn.CrossEntropyLoss().cuda()
        else:
            train_loss_fn = nn.CrossEntropyLoss().cuda()
            validate_loss_fn = train_loss_fn

        eval_metric = self.config["eval_metric"]
        best_metric = None
        best_epoch = None
        saver = None
        output_dir = source_dataset.name
        if self.config["local_rank"] == 0:
            output_base = self.config["output"] if self.config["output"] else f"./{source_dataset.name}"
            exp_name = '-'.join([
                datetime.now().strftime("%Y%m%d-%H%M%S"),
                self.config["model"],
                str(data_config['input_size'][-1])
            ])
            output_dir = get_outdir(output_base, 'train', exp_name)
            decreasing = True if eval_metric == 'loss' else False
            saver = CheckpointSaver(checkpoint_dir=output_dir, decreasing=decreasing)
            with open(os.path.join(output_dir, 'args.yaml'), 'w') as f:
                f.write(self.config.dumps())

        try:
            for epoch in range(start_epoch, num_epochs):
                if self.config["distributed"]:
                    loader_train.sampler.set_epoch(epoch)

                train_metrics = self.train_epoch(
                    epoch, model, loader_train, optimizer, train_loss_fn, self.config_attr,
                    lr_scheduler=lr_scheduler, saver=saver, output_dir=output_dir,
                    use_amp=use_amp, model_ema=model_ema)

                if self.config["distributed"] and self.config["dist_bn"] in ('broadcast', 'reduce'):
                    if self.config["local_rank"] == 0:
                        logging.info("Distributing BatchNorm running means and vars")
                    distribute_bn(model, self.config["world_size"], self.config["dist_bn"] == 'reduce')

                eval_metrics = self.validate(model, loader_eval, validate_loss_fn, self.config_attr)

                if model_ema is not None and not self.config["model_ema_force_cpu"]:
                    if self.config["distributed"] and self.config["dist_bn"] in ('broadcast', 'reduce'):
                        distribute_bn(model_ema, self.config["world_size"], self.config["dist_bn"] == 'reduce')

                    ema_eval_metrics = self.validate(
                        model_ema.ema, loader_eval, validate_loss_fn, self.config_attr, log_suffix=' (EMA)')
                    eval_metrics = ema_eval_metrics

                if lr_scheduler is not None:
                    # step LR for next epoch
                    lr_scheduler.step(epoch + 1, eval_metrics[eval_metric])

                update_summary(
                    epoch, train_metrics, eval_metrics, os.path.join(output_dir, 'summary.csv'),
                    write_header=best_metric is None)
                if saver is not None:
                    # save proper checkpoint with eval metric
                    save_metric = eval_metrics[eval_metric]
                    best_metric, best_epoch = saver.save_checkpoint(
                        model, optimizer, self.config_attr,
                        epoch=epoch, model_ema=model_ema, metric=save_metric,
                        use_amp=use_amp)
        except KeyboardInterrupt:
            pass
        if best_metric is not None:
            logging.info('*** Best metric: {0} (epoch {1})'.format(best_metric, best_epoch))


    def train_epoch(self, epoch, model, loader, optimizer, loss_fn, args,
                    lr_scheduler=None, saver=None, output_dir='', use_amp=False,
                    model_ema=None):

        if args.prefetcher and args.mixup > 0 and loader.mixup_enabled:
            if args.mixup_off_epoch and epoch >= args.mixup_off_epoch:
                loader.mixup_enabled = False

        batch_time_m = AverageMeter()
        data_time_m = AverageMeter()
        losses_m = AverageMeter()

        model.train()

        end = time.time()
        last_idx = len(loader) - 1
        num_updates = epoch * len(loader)
        mixup_batch = Mixup(
            mixup_alpha=args.mixup,
            label_smoothing=args.smoothing,
            num_classes=args.num_classes,
        )
        mixup_batch.mixup_enabled = not (args.mixup_off_epoch and epoch >= args.mixup_off_epoch)

        for batch_idx, (input, target) in enumerate(loader):
            last_batch = batch_idx == last_idx
            data_time_m.update(time.time() - end)
            if not args.prefetcher:
                input, target = input.cuda(), target.cuda()
                if args.mixup > 0.:
                    input, target = mixup_batch(input, target)

            output = model(input)

            loss = loss_fn(output, target)
            if not args.distributed:
                losses_m.update(loss.item(), input.size(0))

            optimizer.zero_grad()
            if use_amp:
                with amp.scale_loss(loss, optimizer) as scaled_loss:
                    scaled_loss.backward()
            else:
                loss.backward()
            optimizer.step()

            torch.cuda.synchronize()
            if model_ema is not None:
                model_ema.update(model)
            num_updates += 1

            batch_time_m.update(time.time() - end)
            if last_batch or batch_idx % args.log_interval == 0:
                lrl = [param_group['lr'] for param_group in optimizer.param_groups]
                lr = sum(lrl) / len(lrl)

                if args.distributed:
                    reduced_loss = reduce_tensor(loss.data, args.world_size)
                    losses_m.update(reduced_loss.item(), input.size(0))

                if args.local_rank == 0:
                    logging.info(
                        'Train: {} [{:>4d}/{} ({:>3.0f}%)]  '
                        'Loss: {loss.val:>9.6f} ({loss.avg:>6.4f})  '
                        'Time: {batch_time.val:.3f}s, {rate:>7.2f}/s  '
                        '({batch_time.avg:.3f}s, {rate_avg:>7.2f}/s)  '
                        'LR: {lr:.3e}  '
                        'Data: {data_time.val:.3f} ({data_time.avg:.3f})'.format(
                            epoch,
                            batch_idx, len(loader),
                            100. * batch_idx / last_idx,
                            loss=losses_m,
                            batch_time=batch_time_m,
                            rate=input.size(0) * args.world_size / batch_time_m.val,
                            rate_avg=input.size(0) * args.world_size / batch_time_m.avg,
                            lr=lr,
                            data_time=data_time_m))

                    if args.save_images and output_dir:
                        torchvision.utils.save_image(
                            input,
                            os.path.join(output_dir, 'train-batch-%d.jpg' % batch_idx),
                            padding=0,
                            normalize=True)

            if saver is not None and args.recovery_interval and (
                    last_batch or (batch_idx + 1) % args.recovery_interval == 0):
                saver.save_recovery(
                    model, optimizer, args, epoch, model_ema=model_ema, use_amp=use_amp, batch_idx=batch_idx)

            if lr_scheduler is not None:
                lr_scheduler.step_update(num_updates=num_updates, metric=losses_m.avg)

            end = time.time()
            # end for

        if hasattr(optimizer, 'sync_lookahead'):
            optimizer.sync_lookahead()

        return OrderedDict([('loss', losses_m.avg)])


    def validate(self, model, loader, loss_fn, args, log_suffix=''):
        batch_time_m = AverageMeter()
        losses_m = AverageMeter()
        top1_m = AverageMeter()
        top5_m = AverageMeter()

        model.eval()

        end = time.time()
        last_idx = len(loader) - 1
        with torch.no_grad():
            for batch_idx, (input, target) in enumerate(loader):
                last_batch = batch_idx == last_idx
                if not args.prefetcher:
                    input = input.cuda()
                    target = target.cuda()

                output = model(input)
                if isinstance(output, (tuple, list)):
                    output = output[0]

                # augmentation reduction
                reduce_factor = args.tta
                if reduce_factor > 1:
                    output = output.unfold(0, reduce_factor, reduce_factor).mean(dim=2)
                    target = target[0:target.size(0):reduce_factor]

                loss = loss_fn(output, target)
                acc1, acc5 = accuracy(output, target, topk=(1, 5))

                if args.distributed:
                    reduced_loss = reduce_tensor(loss.data, args.world_size)
                    acc1 = reduce_tensor(acc1, args.world_size)
                    acc5 = reduce_tensor(acc5, args.world_size)
                else:
                    reduced_loss = loss.data

                torch.cuda.synchronize()

                losses_m.update(reduced_loss.item(), input.size(0))
                top1_m.update(acc1.item(), output.size(0))
                top5_m.update(acc5.item(), output.size(0))

                batch_time_m.update(time.time() - end)
                end = time.time()
                if args.local_rank == 0 and (last_batch or batch_idx % args.log_interval == 0):
                    log_name = 'Test' + log_suffix
                    logging.info(
                        '{0}: [{1:>4d}/{2}]  '
                        'Time: {batch_time.val:.3f} ({batch_time.avg:.3f})  '
                        'Loss: {loss.val:>7.4f} ({loss.avg:>6.4f})  '
                        'Acc@1: {top1.val:>7.4f} ({top1.avg:>7.4f})  '
                        'Acc@5: {top5.val:>7.4f} ({top5.avg:>7.4f})'.format(
                            log_name, batch_idx, last_idx, batch_time=batch_time_m,
                            loss=losses_m, top1=top1_m, top5=top5_m))

        metrics = OrderedDict([('loss', losses_m.avg), ('top1', top1_m.avg), ('top5', top5_m.avg)])
        return metrics

    def domain_adapt_training(self, target_dataset, eval_dataset):
        """ This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
        the dataloaders below and feel free to override the transformers in the datasets to use whatever
        pretraining you desire. Please do not modify the datasets though.

        Args:
            target_dataset (object): pytorch dataset for the target dataset used for training
            eval_dataset (object): eval dataset used for evaluation.  Included here in case you want to do
                transductive learning.
        """
        pass

    def inference(self, eval_dataset):
        """
        Inference is during the evaluation stage.  For this example, the task_network is trained in the
        train and adapt stage's code and is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to JPL

        Args:
            eval_dataset (framework.datasets.ImageClassificationDataset): unlabeled evaluation framework.
                We need to predict the classes for each item in the dataset.

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        pass
