from torch.utils.data import Dataset
from learn.algorithms.FixMatch.randaugment import RandAugment
#from learn.algorithms.FixMatch.fixmatch_utils import get_onehot
from torchvision import datasets, transforms

from PIL import Image
import numpy as np
import copy

def get_transforms(mean=[0.485, 0.456, 0.406], 
        std=[0.229, 0.224, 0.225], train=True):
    if train:
        return transforms.Compose([transforms.RandomHorizontalFlip(),
                                      transforms.Resize(256),
                                      transforms.RandomCrop(224),
                                      transforms.ToTensor(), 
                                      transforms.Normalize(mean, std)])
    else:
        return transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(size=224),
            transforms.ToTensor(), 
            transforms.Normalize(mean, std)])

class FixMatchDataset(Dataset):
    """
    FixMatchDataset returns a pair of image and labels (targets).
    If targets are not given, FixMatchDataset returns None as the label.
    This class supports strong augmentation for Fixmatch,
    and return both weakly and strongly augmented images.
    """
    def __init__(self,
                 wrapped_dataset,
                 #num_classes=None,
                 transform=None,
                 use_strong_transform=False,
                 strong_transform=None,
                 #onehot=False,
                 *args, **kwargs):
        """
        Args
            data: x_data
            targets: y_data (if not exist, None)
            num_classes: number of label classes
            transform: basic transformation of data
            use_strong_transform: If True, this dataset returns both weakly and strongly augmented images.
            strong_transform: list of transformation functions for strong augmentation
            onehot: If True, label is converted into onehot vector.
        """
        super(FixMatchDataset, self).__init__()
        self.wrapped_dataset = wrapped_dataset
        #self.original_transforms = wrapped_dataset.transforms
        self.transforms = transform
        self.use_strong_transform = use_strong_transform
        self.set_transform_to_identity() # so doesn't use native transforms
        if use_strong_transform:
            if strong_transform is None:
                self.strong_transform = copy.deepcopy(self.transforms)
                self.strong_transform.transforms.insert(0, RandAugment(3,5))
        else:
            self.strong_transform = strong_transform

    def set_transform_to_identity(self):
        """ Sets wrapped dataset transform to identity so it doesn't mess with the transforms 
        this dataset will apply. To undo, call set_transform_to_original()"""
        self.wrapped_dataset.transform = transforms.Lambda(lambda x: x)

    '''def set_transform_to_original(self):
        self.wrapped_dataset.transform = self.original_transforms'''
    
    def __getitem__(self, idx):
        """
        If strong augmentation is not used,
            return weak_augment_image, target
        else:
            return weak_augment_image, strong_augment_image, target
        """
        img, target, idx = self.wrapped_dataset[idx]

        if self.transforms is None:
            return transforms.ToTensor()(img), target
        else:
            if isinstance(img, np.ndarray):
                img = Image.fromarray(img)
            img_w = self.transforms(img)
            if not self.use_strong_transform:
                return img_w, target
            else:
                return img_w, self.strong_transform(img), target


    def __len__(self):
        return len(self.wrapped_dataset)
