import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torch.backends.cudnn as cudnn
import torch.utils.data as data
import ubelt as ub
import random
from torchvision import transforms
import math

from learn.algorithms.FixMatch.fixmatch import FixMatch as FixMatchClass
from learn.algorithms.FixMatch.train_utils import get_SGD, get_cosine_schedule_with_warmup
from learn.algorithms.FixMatch.fixmatch_dataset import FixMatchDataset, get_transforms
from learn.algorithms.FixMatch.imageClassificationAdapter import ImageClassifierAdapter
from learn.utils.dataset import ImageClassificationDataset
import logging
import gc
import copy

class ImageClassifierAlgorithm(ImageClassifierAdapter):
    """
    This implementation follows FixMatch (https://arxiv.org/abs/2001.07685) and
    is based on https://github.com/LeeDoYup/FixMatch-pytorch
    """

    def __init__(self, toolset):
        ImageClassifierAdapter.__init__(self, toolset)
        self.config = toolset['protocol_config']['image_classifier']['params']

        device = self.config['device']
        if ub.iterable(device):
            self.device = device[0]
        else:
            self.device = device

    def initialize(self, source_network, source_dataset, whitelist_datasets, target_dataset, networks):
        """  Initialize your algorithm and save any parameters or objects you want to be persistent between
        budget checkpoints.  DomainNetworkSelection will choose a potential source datasets `source_dataset`
        and provide a trained network for that dataset `source_dataset.` Please do not modify the datasets
        beyond overriding the transforms (self.transform and self.transform_target are fine to change).

        Args:
            source_network (pytorch.nn.Module):  Pretrained network on source task
            source_dataset (ImageClassificationDataset): Pytorch dataset for source domain/task
            whitelist_datasets (dict[framework.dataset]): Dictionary of all possible external datasets which
                can be used on this task.  You may use this if you want to select a different dataset than
                the one selected as source.
            target_dataset: (ImageClassificationDataset): Pytorch dataset for target domain/task
            networks: (Dict[str, Dict[str, nn.module]]): Two leveled dict with the dataset as the first key
                and the network structure name as the second key.  e.g. networks['imagenet_1k']['densenet169']



        """
        logging.info('Initializing FixMatch algorithm')
        # Note: whitelist is contained here in case you want the change the source dataset
        random.seed(self.config['seed'])
        torch.manual_seed(self.config['seed'])
        torch.cuda.manual_seed(self.config['seed'])
        np.random.seed(self.config['seed'])
        cudnn.deterministic = True
        #cudnn.benchmark = True   

        self.source_dataset = source_dataset
        
        class_list = target_dataset.categories
        self.source_network = copy.deepcopy(source_network)
        self.source_network.set_as_feature_extractor()
        self.fixmatch = FixMatchClass(
            model = self.source_network,
            num_classes = len(class_list),
            ema_m = self.config['ema_m'],
            T = self.config['T'],
            p_cutoff = self.config['p_cutoff'],
            lambda_u = self.config['ulb_loss_ratio'],
            device = self.device,
            hard_label = self.config['hard_label'])
            #num_eval_iter = config['num_eval_iter']) 
        
        '''source_cats = set(source_dataset.categories.tolist())
        target_cats = set(target_dataset.categories.tolist())
        common_categories = source_cats & target_cats
        target_only_cats = target_cats - source_cats
        logging.info(f'Number of categories in common: {len(common_categories)}')
        logging.info(f'Number of Categories only in target: {len(target_only_cats)}')'''

    '''
    @staticmethod
    def get_weights_for_sampler(dataset, useful_cats):
        """ get weight the dataset to give to pytorch's weightedrandomsampler.
        This is instead of subset selector so that we can weight each element.

        Args:
            dataset (ImageClassificationDataset): dataset which you are weighting
            useful_cats (list[str]): categories to weight, usually for removing
                categories from source dataset

        Returns:
            list[float]: weights for random sampler
        """
        # get counts for each class
        targets = np.array(dataset.targets)
        n = len(targets)
        cls, counts = np.unique(targets[targets!=None], return_counts=True)

        count_dict = dict(zip(cls, counts))

        # Set counts to 0 if unused
        cats = dataset.categories
        cats_to_drop = list(set(cats) - set(useful_cats))
        drop_idxs = dataset._category_name_to_category_index(cats_to_drop)

        weight_dict = dict({None: 0})
        for k, v in count_dict.items():
            if k in drop_idxs:
                weight_dict[k] = 0
            else:
                weight_dict[k] = float(n-v)/n

        # Set weights
        return [weight_dict[t] for t in targets]'''

    def get_l_and_unl_datasets(self, source_dataset, target_dataset):
        """ Get labeled and unlabeled datasets

        Args:
            source_dataset (ImageClassificationDataset): source dataset
            target_dataset (ImageClassificationDataset): target dataset

        Returns:
            unlabeled dataset (FixMatchDataset), labeled dataset (FixMatchDataset)
        """
        # Note: the unlabeled dataset includes the labeled examples, as shown
        # here: https://github.com/google-research/fixmatch/issues/53
        if self.config["train_on_source_target_strategy"] == 'source_and_target':
            unlabeled_dset = torch.utils.data.ConcatDataset([source_dataset, target_dataset])
            labeled_inds = list(np.range(0,len(source_dataset))) \
                + list(target_dataset.get_labeled_indices())
        else: # just use target
            unlabeled_dset = target_dataset
            labeled_inds = list(target_dataset.get_labeled_indices())
        labeled_dset = torch.utils.data.Subset(unlabeled_dset, labeled_inds)
        unlabeled_dset = FixMatchDataset(
            wrapped_dataset = unlabeled_dset,
            use_strong_transform = True,
            transform=get_transforms(train=True),
            strong_transform = None # will just do RandAugment
        ) 
        labeled_dset = FixMatchDataset(
            wrapped_dataset= labeled_dset,
            use_strong_transform = False,
            transform=get_transforms(train=True),
            strong_transform=None
        )
        return unlabeled_dset, labeled_dset
   
    def get_dataloaders(self, labeled_dset, unlabeled_dset):
        """Return labeled and unlabeled dataloader.

        Note that they will be of the same length. So the labeled examples will 
        be iterated multiple times for each epoch (as specified in the FixMatch paper)
        """
        
        # Unlabeled
        unlabeled_dataloader = torch.utils.data.DataLoader(
            unlabeled_dset,
            batch_size=int(self.config["batch_size"] * self.config['uratio']),
            num_workers=int(self.config['num_workers']),
            drop_last=False,
            pin_memory=True
        )
        
        # Labeled
        labeled_data_sampler = torch.utils.data.RandomSampler(
            labeled_dset, replacement = True, 
            # this should set the number of iterations between the labeled 
            # and unlabeled dataloader to be equal
            num_samples = math.ceil(len(unlabeled_dset) / self.config['uratio'])
        )

        labeled_batch_sampler = torch.utils.data.sampler.BatchSampler(labeled_data_sampler,
                batch_size = int(self.config['batch_size']), 
                drop_last = False)
        labeled_dataloader = torch.utils.data.DataLoader(labeled_dset,
                batch_sampler = labeled_batch_sampler,
                num_workers = int(self.config['num_workers']),
                pin_memory=True)

        return labeled_dataloader, unlabeled_dataloader

    def domain_adapt_training(self, target_dataset, eval_dataset):
        """ This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
        the dataloaders below and feel free to override the transformers in the datasets to use whatever
        pretraining you desire. Please do not modify the datasets though.

        Args:
            target_dataset (ImageClassificationDataset): pytorch dataset for the target dataset used for training
            eval_dataset (ImageClassificationDataset): eval dataset used for evaluation.  Included here in case you want to do
                transductive learning.
        """
        logging.info('Few-shot Adaption with FixMatch Algorithm')
        unlabeled_dset, labeled_dset = self.get_l_and_unl_datasets(self.source_dataset, target_dataset) 
        #unlabeled_dset.set_transform_to_identity() # may not be necessary

        logging.info('Creating dataloaders')
        labeled_dataloader, unlabeled_dataloader = self.get_dataloaders(labeled_dset, unlabeled_dset)
        
        self.fixmatch.move_to_gpu()
        
        # SET Optimizer & LR Scheduler
        ## construct SGD and cosine lr scheduler
       
        num_iter = self.config['epoch'] * len(labeled_dataloader)
        optimizer = get_SGD([self.fixmatch.train_model, self.fixmatch.train_classifier], self.config['optimizer_name'], 
            self.config['lr'], self.config['momentum'], self.config['weight_decay'])
        scheduler = get_cosine_schedule_with_warmup(optimizer,
            num_iter,#self.config['epoch'],
            num_warmup_steps=self.config['warmup_epoch'] * len(labeled_dataloader))
        ## set SGD and cosine lr on FixMatch
        self.fixmatch.set_optimizer(optimizer, scheduler)
        self.fixmatch.set_data_loader(train_ulb = unlabeled_dataloader,
            train_lb = labeled_dataloader
        )

        prog = ub.ProgIter(range(self.config['epoch']), desc="Training FixMatch")
        # START TRAINING of FixMatch
        trainer = self.fixmatch.train
        for epoch in range(self.config['epoch']):
            trainer(amp = self.config['amp'])

        self.fixmatch.move_to_cpu()

        #unlabeled_dset.set_transform_to_original() # may not be necessary
        
        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()
        

    def inference(self, eval_dataset):
        """
        Inference is during the evaluation stage.  For this example, the task_network is trained in the
        train and adapt stage's code and is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to JPL

        Args:
            eval_dataset (framework.datasets.ImageClassificationDataset): unlabeled evaluation framework.
                We need to predict the classes for each item in the dataset.

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        self.fixmatch.move_to_gpu(just_eval=True)
        self.fixmatch.eval_model.eval()
        self.fixmatch.eval_classifier.eval()

        eval_dataset.transforms = get_transforms(train=False)
        eval_dataloader = data.DataLoader(eval_dataset,
            batch_size=int(self.config["batch_size"]),
            drop_last=False
        )
        # TODO: check if need to overwrite eval_dataset transforms
        preds = []
        indices = []
        with torch.no_grad():
            for imgs, _, inds in eval_dataloader:
                imgs = imgs.to(self.device)
                output = self.fixmatch.eval_model(imgs)
                preds_ = self.fixmatch.eval_classifier(output)

                preds += torch.argmax(preds_, dim=1).cpu().numpy().tolist()
                indices += inds.numpy().tolist()

        self.fixmatch.move_to_cpu(just_eval=True)
        
        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()

        return preds, indices
