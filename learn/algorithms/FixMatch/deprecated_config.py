import scriptconfig as scfg


class Config(scfg.Config):
    """
    Default configuration for your algorithm

    Please add all of your default configuration parameters for your algorithm below
    with a description (help) on
        - what your parameter does
        - what changes when you use your parameter

    This is so that someone else can come and alter there parameters and know
    how this will affect the run of the algorithm

    """
    default = {
        'name': scfg.Value('FixMatch',
                           help='name of the proposed method'),

        'batch_size': scfg.Value(64,
                                 help='batch size of network while running.  '
                                      'Increasing will do X and decrease will do Y'),

        'num_workers': scfg.Value(4,
                                  help='number of workers in the dataloader'),
        
        'epoch': scfg.Value(100,
            help='number of epochs to train on'),

        'lr': scfg.Value(0.03,
                         help='learning rate starting value'),

        'device': scfg.Value(0,
                             help='what device to use (eg. 0 means gpu 0'),

        'seed': scfg.Value(1,
                           help='random seed (default: 1)'),

        'ema_m': scfg.Value(0.999,
                           help='ema momentum for eval_model'),

        'T': scfg.Value(0.5,
                           help='Temperature scaling parameter for output sharpening'),

        'p_cutoff': scfg.Value(0.95,
                           help='confidence cutoff parameters for loss masking'),

        'ulb_loss_ratio': scfg.Value(1.0,
                           help='ratio of unsupervised loss to supervised loss'),

        'hard_label': scfg.Value(True,
                           help='If should use hard labels (for unsupervised training)'),

        'train_on_source_target_strategy': scfg.Value('target',
                           help='If should use just target or source_and_target'),

        'optimizer_name': scfg.Value('SGD',
                           help='Which optimizer to use'),

        'momentum': scfg.Value(0.9,
                           help='Momentum for optimizer'),

        'weight_decay': scfg.Value(5e-4),

        'warmup_epoch': scfg.Value(5,
                           help='Num epochs to warmup for'),

        'amp': scfg.Value(True,
                           help='If should use mixed precision'),
        
        'uratio': scfg.Value(7,
                           help='ratio of unlabeled to labeled data in each mini-batch')
        }
