import abc
from learn.deprecated_tinker.basealgorithm import BaseAlgorithm


class AlgorithmSelectionAdapter(BaseAlgorithm):
    def __init__(self, toolset):
        BaseAlgorithm.__init__(self, toolset)

    def execute(self, toolset: dict, step_descriptor: str):
        """
        Execute function associated with the adapter

        Args:
            toolset (dict): A dictionary with experiment parameters
            step_descriptor (str): The name of different steps supported by adapter

        Returns:

        """
        self.toolset = toolset
        if step_descriptor == 'SelectAlgorithms':
            return self.select_algorithms(
                protocol_config=self.toolset["protocol_config"],
                source_dataset=self.toolset["source_dataset"],
                target_dataset=self.toolset["target_dataset"],
                problem_type=self.toolset["problem_type"]
            )
        else:
            raise NotImplementedError(f'Step {step_descriptor} not implemented')

    @abc.abstractmethod
    def select_algorithms(self, protocol_config, source_dataset, target_dataset, problem_type):
        raise NotImplementedError('Select Algorithms no implemented for algorithm selection')

