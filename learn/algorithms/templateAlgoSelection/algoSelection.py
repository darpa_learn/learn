from learn.algorithms.templateAlgoSelection.algoSelectionAdapter import AlgorithmSelectionAdapter
from learn.utils.dataset import (
    ImageClassificationDataset, ObjectDetectionDataset#,
#    VideoClassificationDataset
)
from typing import Union, Dict, Tuple
import logging
from learn.algorithms.CoMix.dataset_slowfast import VideoClassificationDataset_SlowFast as VideoClassificationDataset

class AlgorithmSelection(AlgorithmSelectionAdapter):
    def __init__(self, toolset):
        AlgorithmSelectionAdapter.__init__(self, toolset)

    def select_algorithms(self,
                          protocol_config: Dict[str, str],
                          source_dataset: Union[ImageClassificationDataset, ObjectDetectionDataset, VideoClassificationDataset],
                          target_dataset: Union[ImageClassificationDataset, ObjectDetectionDataset, VideoClassificationDataset],
                          problem_type: str) -> Tuple[str, str]:
        """  Function to select which datasets to use for the problem.  This can be run for each
        budget level or for each stage.  This one here is designed for each stage (since only depends
        on category overlap).

        Args:
            protocol_config: config for the entire problem defining which algorithms to use
            source_dataset: pytorch dataset for selected source dataset
            target_dataset: pytorch dataset for target dataset
            problem_type: either 'image_classification', 'object_detection', 'machine_translation' defined by
                problem metadata

        Returns: name of query algorithm

        """

        choice_dict = {
            'image_classification': self._choose_image_classification,
            'object_detection': self._choose_object_detection,
            'machine_translation': self._choose_machine_translation,
            'video_classification': self._choose_video_classification
        }

        query_algo = protocol_config['active_learner']['name']
        if query_algo == 'auto':
            query_algo = 'templateQueryAlgo/query_algo_rand.py'
        logging.info(f'Using {query_algo} algorithm for query algorithm')
        if problem_type in choice_dict:
            classifier = choice_dict[problem_type](protocol_config, source_dataset, target_dataset)
        else:
            raise NotImplementedError(f"{problem_type} not implemented")
        return query_algo, classifier

    @staticmethod
    def _choose_image_classification(protocol_config, source_dataset, target_dataset):
        classifier = protocol_config['image_classifier']['name']
        if classifier == 'auto':
            classifier = 'templateImageClassification/image_classification.py'
            # source_cats = set(source_dataset.categories.tolist())
            # target_cats = set(target_dataset.categories.tolist())
            # common_categories = source_cats & target_cats
            # if len(common_categories) < 1:
            #     classifier = 'MetaBaseline/image_classification.py'
            #     logging.info(f'Switching from MME to MetaBaseline since no categories in common between '
            #                  f'source_dataset: "{source_dataset.name}" and target dataset: '
            #                  f'"{target_dataset.name}"')

        logging.info(f'Using {classifier} algorithm for image classification')
        return classifier

    @staticmethod
    def _choose_object_detection(protocol_config, source_dataset, target_dataset):
        classifier = protocol_config['object_detector']['name']
        if classifier == 'auto':
            classifier = 'templateObjectDetection/object_detection.py'
        logging.info(f'Using {classifier} algorithm for object detection')
        return classifier

    @staticmethod
    def _choose_machine_translation(protocol_config, source_dataset, target_dataset):
        classifier = protocol_config['machine_translation']['name']
        if classifier == 'auto':
            classifier = 'templateNLP/machine_translation.py'
        logging.info(f'Using {classifier} algorithm for machine translation')
        return classifier

    @staticmethod
    def _choose_video_classification(protocol_config, source_dataset, target_dataset):
        classifier = protocol_config['video_classifier']['name']
        if classifier == 'auto':
            classifier = 'templateVideoClassification/video_classification.py'
        logging.info(f'Using {classifier} algorithm for video classification')
        return classifier
