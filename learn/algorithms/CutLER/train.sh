# run the following commands under learn's home directory
### CutLER
## Cut-and-Learning's cut stage
# navigate to the directory where the MaskCut is located.
cd learn/algorithms/CutLER/CutLER-main/maskcut
# structure of dataset should be:
# maskcutDataset
#   train
#       images
#           *.jpg/png/JPEG
#   annotations
mkdir maskcutDataset
mkdir maskcutDataset/train
mkdir maskcutDataset/annotations
# NOTE: /path/to/train/folder contains all images in the training set
ln -s /path/to/train/folder maskcutDataset/train/images
# run the following commands to start the pseudo-mask generation process.
python maskcut.py \
--vit-arch base --patch-size 8 \
--tau 0.15 \
--num-folder 1 --job-index 0 \
--dataset-path maskcutDataset/train \
--out-dir maskcutDataset/annotations

## Cut-and-Learning's learning stage
# navigate to the directory where the CutLER pretraining process is located.
cd ../cutler/
# run the following commands to start the CutLER pretraining process.
# OUTPUT_DIR is the path to save checkpoints and training log
export DETECTRON2_DATASETS="../maskcut/"
CUDA_VISIBLE_DEVICES=1,2,3,4 python train_net.py --num-gpus 4 \
  --config-file model_zoo/configs/CutLER-ImageNet/cascade_mask_rcnn_R_50_FPN_poolcar.yaml \
  OUTPUT_DIR output/

### FSDet stage for few-shot learning
# load model initializations from CutLER and load the training process
# NOTE:
# WEIGHTS in "configs/detectron_config/coco2014_cascade_mask_rcnn_R_50_FPN_3x.yaml" should be updated to load 
# CutLER's pretraining weights
cd ../../../../../
CUDA_VISIBLE_DEVICES=5,6,7,8 python hydra_launcher.py problem=quick_od_test \
domain_network_selector=od_auto object_detector=FsDet \
problem.dataset_dir=/home/xwang4/shaan/learn/lwll_datasets \
domain_network_selector.params.find_pretrained_method=cascade_mask_rcnn_R_50_FPN_3x domain_network_selector.params.find_source_data_method=coco2014 problem.add_dataset_to_whitelist=[coco2014] \
domain_network_selector.params.pretrained_network_dir=None \
self_supervision_pretrain=CutLER harness=local "+wandb_params.name=CutLER poolcar" \
problem.tasks_to_run=poolcar wandb_params.run_wandb=False \
object_detector.params.fsnet_maximum_iter=10000 \
object_detector.params.learning_rate=0.0025 object_detector.params.batch_size=8 | tee terminal_bs16_poolcar.txt