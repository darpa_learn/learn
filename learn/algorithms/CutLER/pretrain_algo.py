from __future__ import absolute_import, division, print_function, unicode_literals
import json
import logging
logger = logging.getLogger(__name__)
import os
import shutil
import tempfile
import pickle

import numpy as np
import torch
import torch.distributed as dist
import torch.nn as nn
import ubelt as ub
# from learn.algorithms.BU_NLP.distributed_launch import torch_distributed_launch
from learn.algorithms.CutLER.pretrain_adapter import PretrainAdapter
from learn.utils.dataset import ImageClassificationDataset
from learn.utils.memory import garbage_collection_cuda
from learn.utils.pretrained_networks import ImageClassificationNetwork
from torch.utils.data import DataLoader, SequentialSampler, Subset
from torchvision import transforms
from hydra.utils import get_original_cwd
# Retinanet imports
from typing import Dict, List, Tuple
from detectron2.structures import Boxes, ImageList, Instances, pairwise_iou
from torch import Tensor, nn
from detectron2.config import CfgNode
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.engine import DefaultTrainer, launch
from detectron2.config import get_cfg
from detectron2 import model_zoo
from detectron2.checkpoint import DetectionCheckpointer, Checkpointer
from detectron2.model_zoo import get_checkpoint_url
import gc
# torch_distributed_launch imports
import sys
import subprocess
import os
from argparse import ArgumentParser, REMAINDER
import pdb
import yaml
from subprocess import PIPE, STDOUT, Popen
from learn.algorithms.CutLER.distributed import trainer_func

import mmcv
from mmcv.runner import load_checkpoint, save_checkpoint
from mmcv import Config
import mmdet
from mmdet.datasets import build_dataset, build_dataloader
from mmdet.models import build_detector
from mmdet.apis import train_detector, inference_detector # , init_random_seed, set_random_seed
from mmdet.models.builder import LOSSES
from mmdet.datasets.builder import DATASETS
from mmdet.datasets.custom import CustomDataset
from mmdet.utils import collect_env

import learn.algorithms.MMDET.register_modules
from functools import partial
import random



def get_config(orig_cfg, train_dataset_name, num_classes, model_directory, config):
    """ Configuration for detectron.  This takes the configuration parameters
    from the config.py and sets them to the appropriate values in the detectron
    config.

    More info: https://detectron2.readthedocs.io/modules/config.html

    Args:
        cfg (CfgNode): DetectronConfig obtained from source network
        train_dataset_name (str): name of the train dataset (used for identifying
            it later in the code
        test_dataset_name (str): name of the test dataset (used for identifying
            it later in the code
        num_classes (int): number of classes for the problem you are solving
        model_directory (TemporaryDirectory): TemporaryDirectory for saving/loading models
        config (scriptconfig.Config): list of configuration values for the
            detectron system.  Defined in config.py

    Returns:
         detectron2 CfgNode instance

    """
    cfg = orig_cfg.clone() # This is done to use a copy of config that is not frozen
    cfg.defrost()
    cfg.DATASETS.TRAIN = (train_dataset_name,)
    cfg.TEST.EVAL_PERIOD = 0
    cfg.DATALOADER.NUM_WORKERS = config['num_workers']
    cfg.SOLVER.IMS_PER_BATCH = config['batch_size']
    cfg.SOLVER.BASE_LR = config['learning_rate']  # pick a good LR
    cfg.SOLVER.MAX_ITER = config['maximum_iter'] 
    cfg.SOLVER.BASE_LR_MULTIPLIER = config["BASE_LR_MULTIPLIER"]
    cfg.SOLVER.BASE_LR_MULTIPLIER_NAMES = config["BASE_LR_MULTIPLIER_NAMES"]
    cfg.MODEL.ROI_HEADS.NUM_CLASSES = num_classes
    cfg.MODEL.RETINANET.NUM_CLASSES = num_classes
    # specify the trained model weights, here use the default weights from
    # detectron2 model zoo
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = config[
        'score_thresh_test']  # set the testing threshold for this model
    cfg.OUTPUT_DIR = model_directory 
    
    return cfg



class PretrainAlgo(PretrainAdapter):
    def __init__(self, toolset):
        PretrainAdapter.__init__(self, toolset)
        print("Here is the Toolset")
        print(toolset)
        print("end")
        self.config = toolset['protocol_config']['self_supervision_pretrain']['params'] # configs/hydra_config/self_supervision_pretrain/CutLER.yaml
        print(self.config)
        print('end of self.config')


    def initialize(self, initial_model, datasets, stage):
        """
        Initialize your algorithm and save any parameters or objects you want
        to be persistent between budget checkpoints.

        Args:
            initial_model: Pytorch network used as the initial model that the pretraining takes place on top of.
            datasets [ImageClassificationDataset]: Array of Pytorch dataset for the pretraining.
        """
        logger.info('Initializing CutLER algorithm')
        print("PRINTING THE INITIAL MODEL ", initial_model)
        print("self.toolset is: ", self.toolset)
        self.initial_model = initial_model
        ### datasets will be a list of either the  target dataset, source dataset, or both
        ### you can configure this in the yaml config by setting pretrain_on_source_target_strategy
        ### see pretrain_adapter.py file for settings details
        ### see learn/utils/dataset.py for information about the dataset format
        self.datasets = datasets

        device = self.config['device']
        if ub.iterable(device):
            self.device = device
        else:
            if device == -1:
                self.device = list(range(torch.cuda.device_count()))
            else:
                self.device = [device]
        if len(self.device) > torch.cuda.device_count():
            self.device = self.device[:torch.cuda.device_count()]
        self.ddp_num_gpus = len(self.device)
        
        ### directory where you can save model checkpoints and other info
        if self.config["work_dir"] is not None:
            self.work_dir = self.config["work_dir"]
        elif "work_dir" in self.toolset["protocol_config"]["object_detector"]["params"] and \
                self.toolset["protocol_config"]["object_detector"]["params"]["work_dir"] is not None:
            self.work_dir = self.toolset["protocol_config"]["object_detector"]["params"]["work_dir"]
            self.config["work_dir"] = self.work_dir
        else:
            self.work_dir = self.toolset["temp_model_directory"]
            self.config["work_dir"] = self.toolset["temp_model_directory"]
        os.makedirs(self.work_dir, exist_ok=True)
                
        self.target_dataset = train_dataset = self.datasets[0]
        self.target_dataset_name = f'pretrain_{train_dataset.name}_train'
        # print("self.target_dataset_name: ", self.target_dataset_name)
        if self.target_dataset_name in MetadataCatalog.list():
            self.target_dataset_name = f'pretrain1_{train_dataset.name}_train'

        DatasetCatalog.register(self.target_dataset_name, lambda d=train_dataset: self.transform_dataset(d))
        print("registered datasets ", DatasetCatalog.list())
        train_cats = [str(cat) for cat in train_dataset.categories.tolist()]
        MetadataCatalog.get(self.target_dataset_name).thing_classes = train_cats
        
        num_cats = len(train_dataset.categories)

        self.cfg = get_config(self.toolset["source_network"].model_cfg,
                                    self.target_dataset_name,
                                    num_cats,
                                    self.work_dir,
                                    self.config)


    def pretrain(self):
        # self.cfg.MODEL.WEIGHTS = self.toolset["source_network"].model_cfg.MODEL.WEIGHTS
        # DetectionCheckpointer(self.initial_model)._load_file(self.cfg.MODEL.WEIGHTS)
        # DetectionCheckpointer(self.cfg.MODEL)._load_file(self.cfg.MODEL.WEIGHTS)
        # print("SET BOTH INITIAL MODEL AND CFG MODEL WEIGHTS TO ", self.cfg.MODEL.WEIGHTS)

        logger = logging.getLogger(__name__)

        ##### Generate the maskcut annotations
        top_data_dir = os.path.join(self.work_dir, "imagenet")
        os.makedirs(top_data_dir, exist_ok=True)
        os.makedirs(os.path.join(top_data_dir, "train"), exist_ok=True)
        os.makedirs(os.path.join(top_data_dir, "annotations"), exist_ok=True)
        train_image_dir = self.toolset["target_dataset"].root # /data/datasets/lwll_datasets/external/pool_car_detection/pool_car_detection_full/train

        lst_train_files = os.listdir(train_image_dir)
        logger.info(f"Running maskcut on {len(lst_train_files)} images")

        # delete the symlink if it exists. if you don't do this, the adapt stage
        # will fail since you can't overwrite the existing link
        if os.path.exists(f"{top_data_dir}/train/images"):
            os.unlink(f"{top_data_dir}/train/images")

        os.system(f"ln -s {train_image_dir} {top_data_dir}/train/images")
        
        args = ["--vit-arch", "base",
            "--patch-size", "8",
            "--tau", "0.15",
            "--img-limit", str(self.config["img_limit"]),
            "--num-folder", "1",
            "--job-index", "0",
            "--dataset-path", f"{top_data_dir}/train",
            "--actual-img-path", train_image_dir,
            "--out-dir", f"{top_data_dir}/annotations",
            "--stage", self.toolset["stage"]] # set these directories to use the temp directory

        maskcut_path = os.path.join(get_original_cwd(), "learn/algorithms/CutLER/CutLER_main/maskcut/maskcut.py")
        torch_distributed_launch(nproc_per_node=1, training_script=maskcut_path, training_script_args=args)


        # ##### Train the model on the maskcut annotations
        # os.environ["DETECTRON2_DATASETS"] = str(os.path.join(top_data_dir))
        # train_net_path = os.path.join(get_original_cwd(), "learn/algorithms/CutLER/CutLER_main/cutler/train_net.py")
        # config_path = os.path.join(get_original_cwd(), "learn/algorithms/CutLER/CutLER_main/cutler/model_zoo/configs/CutLER-ImageNet/cascade_mask_rcnn_R_50_FPN_poolcar.yaml")
        # args = ["--dataset_file", os.path.join(os.path.join(top_data_dir, "annotations"), "imagenet_train_fixsize480_tau0.15_N3_0_1.json"),
        #     "--images_path", os.path.join(top_data_dir, "train"),
        #     "--num-gpus", "2", # f"{len(self.device)}",
        #     "--config-file", config_path,
        #     "OUTPUT_DIR", self.work_dir]

        # training_script = train_net_path
        # training_args = args
        # torch_distributed_launch(nproc_per_node=2, training_script=training_script, training_script_args=training_args)



        ##### Train an MMDetection model rather than detectron
        self.register_new_losses()

        mmdet_config_path = os.path.join(get_original_cwd(), './learn/algorithms/MMDET/configs/'+self.config["mmdet_model_config_file"])
        coco_data_path = os.path.join(top_data_dir, f"annotations/{self.toolset['stage']}_maskcut_annotations.json")

        self.mmdet_config = self.set_config(mmdet_config_path, coco_data_path, train_image_dir)
        self.mmdet_config.work_dir = self.work_dir

        print(f'Config:\n{self.mmdet_config.pretty_text}')

        original_chkpt_file = str(os.path.join(self.toolset['protocol_config']['domain_network_selector']['params']["pretrained_network_dir"],
            self.config["model_checkpoint_file"]))

        seed = self.config["seed"]
        random.seed(seed)
        np.random.seed(seed)
        torch.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)
        self.mmdet_config.seed = seed

        def replace(conf, depth):
            if depth <= 0:
                return
            try:
                for k,v in conf.items():
                    if isinstance(v, dict):
                        replace(v, depth-1)
                    elif isinstance(v, list):
                        for element in v:
                            replace(element, depth-1)
                    else:
                        # print(k,v)
                        if k == 'num_classes':
                            conf[k] = 1
                        if k == 'CLASSES':
                            conf[k] = ("fg",)
            except:
                pass

        replace(self.mmdet_config, 500)

        if self.config["use_ddp"] and torch.cuda.device_count() > 1 and self.ddp_num_gpus > 1:

            logger.info("Using DDP")
            self.mmdet_config.dump(os.path.join(self.work_dir, 'mmdet_config.py'))
            training_args = ['--work_dir', self.work_dir, '--seed', str(seed), '--gpus', str(len(self.device)), \
                '--model_checkpoint_file', original_chkpt_file, '--freeze', f"{self.config['freeze_backbone']}"]
            training_script = os.path.join(get_original_cwd(), 'learn/algorithms/MMDET/distributed.py')
            torch_distributed_launch(nproc_per_node=len(self.device), training_script=training_script, training_script_args=training_args)
            logger.info("Returned from distributed training")

            # distributed model should be saved in the work dir
            model = build_detector(
                self.mmdet_config.model, train_cfg=self.mmdet_config.get('train_cfg'), test_cfg=self.mmdet_config.get('test_cfg')
            )
            checkpoint_file = os.path.join(self.work_dir, 'latest.pth')
            chkpt = load_checkpoint(model, checkpoint_file)
            self.model = model
            save_checkpoint(model, os.path.join(self.work_dir, "pretrain.pth"))

        else:

            meta = dict()
            env_info_dict = collect_env()
            env_info = '\n'.join([(f'{k}: {v}') for k, v in env_info_dict.items()])
            dash_line = '-' * 60 + '\n'
            logger.info('Environment info:\n' + dash_line + env_info + '\n' +
                        dash_line)
            meta['env_info'] = env_info
            meta['config'] = self.mmdet_config.pretty_text
            meta['seed'] = seed

            logger.info("building dataset")
            datasets = [build_dataset(self.mmdet_config.data.train)]

            logger.info("building detector")
            model = build_detector(
                self.mmdet_config.model, train_cfg=self.mmdet_config.get('train_cfg'), test_cfg=self.mmdet_config.get('test_cfg')
            )

            checkpoint_file = original_chkpt_file
            chkpt = load_checkpoint(model, checkpoint_file)

            logger.info("training model")

            # the fix below will log multiple mmdet things for training so set this to error
            # for now to avoid that
            log = logging.getLogger()
            log.handlers[0].level = logging.ERROR

            model.train()
            train_detector(model, datasets, self.mmdet_config, distributed=False, validate=False, meta=meta)

            # somehow the root logger's stream handler is getting set to ERROR, so set it back to INFO manually
            # this will then set __name__'s log level to INFO again
            log = logging.getLogger()
            log.handlers[0].level = logging.INFO
            logger = logging.getLogger(__name__)

            logger.info("finished training")
            self.model = model
            save_checkpoint(model, os.path.join(self.work_dir, "pretrain.pth"))


        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()
        return


    def register_new_losses(self):
        train_dataset = self.toolset["target_dataset"]

        LOSSES._module_dict.pop('ReducedFocalLoss', None)
        LOSSES._module_dict.pop('EQLv2', None)

        # https://github.com/facebookresearch/fvcore/blob/main/fvcore/nn/focal_loss.py
        def reduced_focal_loss(inputs, targets, threshold=0.5, alpha=0.25, gamma=2.0):
            
            targets = F.one_hot(targets, num_classes=inputs.shape[-1]).type_as(inputs)
            p = torch.sigmoid(inputs)
            ce_loss = F.binary_cross_entropy_with_logits(
                inputs, targets, reduction="none"
            )
            p_t = p * targets + (1 - p) * (1 - targets)
            ones_mask = p_t < threshold
            other_mask = p_t >= threshold
            other_mask = other_mask * (((1 - p_t) ** gamma) / threshold ** gamma)
            f_x = ones_mask + other_mask
            
            loss = ce_loss * f_x

            if alpha >= 0:
                alpha_t = alpha * targets + (1 - alpha) * (1 - targets)
                loss = alpha_t * loss

            return loss

        @LOSSES.register_module()
        class ReducedFocalLoss(torch.nn.Module):
            def __init__(self,
                        threshold=0.5,
                        gamma=2.0,
                        alpha=0.25,
                        reduction='mean',
                        loss_weight=1.0):
                super(ReducedFocalLoss, self).__init__()
                self.alpha = alpha
                self.gamma = gamma
                self.reduction = reduction
                self.loss_weight = loss_weight
                self.threshold = threshold


            def forward(self,
                        pred,
                        target,
                        weight=None,
                        avg_factor=None,
                        reduction_override=None):
                """Forward function.
                Args:
                    pred (torch.Tensor): The prediction.
                    target (torch.Tensor): The learning target of the prediction
                        in gaussian distribution.
                    weight (torch.Tensor, optional): The weight of loss for each
                        prediction. Defaults to None.
                    avg_factor (int, optional): Average factor that is used to average
                        the loss. Defaults to None.
                    reduction_override (str, optional): The reduction method used to
                        override the original reduction method of the loss.
                        Defaults to None.
                """
                assert reduction_override in (None, 'none', 'mean', 'sum')
                reduction = (
                    reduction_override if reduction_override else self.reduction)
                loss_reg = self.loss_weight * reduced_focal_loss(
                    pred,
                    target,
                    threshold=self.threshold,
                    alpha=self.alpha,
                    gamma=self.gamma)
                
                if reduction == 'mean':
                    return loss_reg.mean()
                if reduction == 'sum':
                    return loss_reg.sum()

                return loss_reg


        @LOSSES.register_module()
        class EQLv2(torch.nn.Module):
            def __init__(self,
                    use_sigmoid=True,
                    reduction='mean',
                    class_weight=None,
                    loss_weight=1.0,
                    num_classes=len(train_dataset.categories),  # 1203 for lvis v1.0, 1230 for lvis v0.5
                    gamma=12,
                    mu=0.8,
                    alpha=4.0,
                    vis_grad=False):
                super().__init__()
                self.use_sigmoid = True
                self.reduction = reduction
                self.loss_weight = loss_weight
                self.class_weight = class_weight
                self.num_classes = num_classes
                self.group = True

                # cfg for eqlv2
                self.vis_grad = vis_grad
                self.gamma = gamma
                self.mu = mu
                self.alpha = alpha

                # initial variables
                self._pos_grad = None
                self._neg_grad = None
                self.pos_neg = None

                def _func(x, gamma, mu):
                    return 1 / (1 + torch.exp(-gamma * (x - mu)))
                self.map_func = partial(_func, gamma=self.gamma, mu=self.mu)
                # logger = get_root_logger()
                # logger.info(f"build EQL v2, gamma: {gamma}, mu: {mu}, alpha: {alpha}")

            def forward(self,
                        cls_score,
                        label,
                        weight=None,
                        avg_factor=None,
                        reduction_override=None,
                        **kwargs):
                self.n_i, self.n_c = cls_score.size()

                self.gt_classes = label
                self.pred_class_logits = cls_score

                def expand_label(pred, gt_classes):
                    target = pred.new_zeros(self.n_i, self.n_c)
                    target[torch.arange(self.n_i), gt_classes] = 1
                    return target

                target = expand_label(cls_score, label)
                pos_w, neg_w = self.get_weight(cls_score)
                weight = pos_w * target + neg_w * (1 - target)
                cls_loss = F.binary_cross_entropy_with_logits(cls_score, target,
                                                            reduction='none')
                cls_loss = torch.sum(cls_loss * weight) / self.n_i
                self.collect_grad(cls_score.detach(), target.detach(), weight.detach())
                return self.loss_weight * cls_loss

            def get_channel_num(self, num_classes):
                num_channel = num_classes + 1
                return num_channel

            def get_activation(self, cls_score):
                cls_score = torch.sigmoid(cls_score)
                n_i, n_c = cls_score.size()
                bg_score = cls_score[:, -1].view(n_i, 1)
                cls_score[:, :-1] *= (1 - bg_score)
                return cls_score

            def collect_grad(self, cls_score, target, weight):
                prob = torch.sigmoid(cls_score)
                grad = target * (prob - 1) + (1 - target) * prob
                grad = torch.abs(grad)

                # do not collect grad for objectiveness branch [:-1]
                pos_grad = torch.sum(grad * target * weight, dim=0)[:-1]
                neg_grad = torch.sum(grad * (1 - target) * weight, dim=0)[:-1]

                # dist.all_reduce(pos_grad)
                # dist.all_reduce(neg_grad)

                self._pos_grad += pos_grad
                self._neg_grad += neg_grad
                self.pos_neg = self._pos_grad / (self._neg_grad + 1e-10)

            def get_weight(self, cls_score):
                # we do not have information about pos grad and neg grad at beginning
                if self._pos_grad is None:
                    self._pos_grad = cls_score.new_zeros(self.num_classes)
                    self._neg_grad = cls_score.new_zeros(self.num_classes)
                    neg_w = cls_score.new_ones((self.n_i, self.n_c))
                    pos_w = cls_score.new_ones((self.n_i, self.n_c))
                else:
                    # the negative weight for objectiveness is always 1
                    neg_w = torch.cat([self.map_func(self.pos_neg), cls_score.new_ones(1)])
                    pos_w = 1 + self.alpha * (1 - neg_w)
                    neg_w = neg_w.view(1, -1).expand(self.n_i, self.n_c)
                    pos_w = pos_w.view(1, -1).expand(self.n_i, self.n_c)
                return pos_w, neg_w



    def set_config(self, path, coco_data_path, images_path):
        """
        Load an MMDET config for training. There is the MMDET config which
        contains the low-level model/training details. 
        """

        # train_dataset =  self.toolset['target_dataset']
        # test_dataset = self.toolset['eval_dataset']
    
        mmdet_config = Config.fromfile(path)
        mmdet_config.dataset_type = 'CocoDataset'
        mmdet_config.data_root = images_path
        mmdet_config.data.ann_file = coco_data_path
        train_pipeline = mmdet_config.train_pipeline
        test_pipeline = mmdet_config.test_pipeline
        mmdet_config.classes = ("fg",)
        # print(type(mmdet_config)) # <class 'mmcv.utils.config.Config'>

        # if using RepeatDataset
        if mmdet_config.data.train.type == "RepeatDataset":
            if self.config["use_class_balanced"] and mmdet_config.data.train.dataset.type != 'ClassBalancedDataset':
                mmdet_config.data.train.dataset.type = 'CocoDataset'
                mmdet_config.data.train.dataset.data_root = images_path
                mmdet_config.data.train.dataset.ann_file = coco_data_path
                mmdet_config.data.train.dataset.img_prefix = images_path
                mmdet_config.data.train.dataset.classes = ("fg",)

                data = copy.deepcopy(mmdet_config.data.train.dataset)
                mmdet_config.data.train.dataset = dict(
                    type='ClassBalancedDataset',
                    oversample_thr=self.config["oversample_thr"],
                    dataset=data)
            elif self.config["use_class_balanced"]:
                mmdet_config.data.train.dataset.dataset.type = 'CocoDataset'
                mmdet_config.data.train.dataset.dataset.data_root = images_path
                mmdet_config.data.train.dataset.dataset.ann_file = coco_data_path
                mmdet_config.data.train.dataset.dataset.img_prefix = images_path
                mmdet_config.data.train.dataset.dataset.classes = ("fg",)
            else:
                mmdet_config.data.train.dataset.type = 'CocoDataset'
                mmdet_config.data.train.dataset.data_root = images_path
                mmdet_config.data.train.dataset.ann_file = coco_data_path
                mmdet_config.data.train.dataset.img_prefix = images_path
                mmdet_config.data.train.dataset.classes = ("fg",)

        elif mmdet_config.data.train.type == 'ClassBalancedDataset' and self.config["use_class_balanced"]:
            mmdet_config.data.train.oversample_thr = self.config["oversample_thr"]
            mmdet_config.data.train.dataset.type = 'CocoDataset'
            mmdet_config.data.train.dataset.data_root = images_path
            mmdet_config.data.train.dataset.ann_file = coco_data_path
            mmdet_config.data.train.dataset.img_prefix = images_path
            mmdet_config.data.train.dataset.classes = ("fg",)
        
        else:
            mmdet_config.data.train.type = 'CocoDataset'
            mmdet_config.data.train.data_root = images_path
            mmdet_config.data.train.ann_file = coco_data_path
            mmdet_config.data.train.img_prefix = images_path
            mmdet_config.data.train.classes = ("fg",) # tuple(train_dataset.category_to_category_index.values())

            if self.config["use_class_balanced"]:
                data = copy.deepcopy(mmdet_config.data.train)
                mmdet_config.data.train = dict(
                    type='ClassBalancedDataset',
                    oversample_thr=self.config["oversample_thr"],
                    dataset=data)

        mmdet_config.data.val.type = 'CocoDataset'
        mmdet_config.data.val.data_root = images_path
        mmdet_config.data.val.ann_file = coco_data_path
        mmdet_config.data.val.img_prefix = images_path
        mmdet_config.data.val.classes = ("fg",) # tuple(train_dataset.category_to_category_index.values())

        mmdet_config.data.test.type = 'CocoDataset'
        mmdet_config.data.test.data_root = images_path  # change back to test??
        mmdet_config.data.test.ann_file = coco_data_path
        mmdet_config.data.test.img_prefix = images_path
        mmdet_config.data.test.classes = ("fg",) # tuple(train_dataset.category_to_category_index.values()) # is this issue in model?

        mmdet_config.log_config.interval = self.config["log_interval"]
        mmdet_config.checkpoint_config.interval = self.config["checkpoint_interval"]
        mmdet_config.data.samples_per_gpu = self.config["batch_size"]  # Batch size
        mmdet_config.gpu_ids = self.device
        mmdet_config.device = 'cuda'
        mmdet_config.work_dir = self.config["work_dir"]

        num_iter = self.config["max_iters"]

        mmdet_config.lr_config.warmup_iters = 1 if self.config["warmup_iters"] >= num_iter else self.config["warmup_iters"]
        mmdet_config.lr_config.step = [step for step in self.config["lr_steps"] if step < num_iter]
        mmdet_config.runner = {'type': 'IterBasedRunner', 'max_iters': num_iter}
        mmdet_config.optimizer.lr = self.config["lr"]

        # print(f'Config:\n{mmdet_config.pretty_text}')

        # mmdet_config.dump(os.path.join(get_original_cwd(), './learn/algorithms/MMDET/configs/convnext_extra_large_config.py'))
        # exit()

        return mmdet_config



    def load_model(self):
        print("Shaan - Loaded weights ")
        output_dir = self.cfg.OUTPUT_DIR
        model_weights = self.toolset["source_network"].model_cfg.MODEL.WEIGHTS
        self.cfg.MODEL.WEIGHTS = model_weights
        DetectionCheckpointer(self.trainer.model, save_dir=output_dir)._load_file(model_weights)



    ### this is the transform_dataset from learn/algorithms/FsDet/object_detection.py for use with 
    ### detectron2. You'll need to modify (or maybe not even use it) for your purposes.
    @staticmethod
    def transform_dataset(dataset):
        """ Transform our datasets into the coco format which can be then be registered
        into the dectectron system.  For now we are only training it but we might
        want to create a validation dataset to test on as well

        Args:
            dataset (framework.dataset):  Dataset from framework

        Returns
            dict in coco format.
        """

        # ######################### Get the code ready for detectron
        # Convert our dataset
        logger.info('Converting target_dataset to coco dataset')
        from detectron2.structures import BoxMode
        from detectron2.data.detection_utils import read_image
        from PIL import Image

        num_images = dataset.num_images
        dataset_dict = []
        num_records = 0
        for index in range(num_images):
            record = dict()
            record['file_name'] = dataset.root + '/' + dataset.image_fnames[index]
            img = Image.fromarray(read_image(record['file_name']))
            record['width'], record['height'] = img.size
            record['image_id'] = index
            record['annotations'] = list()
            targets = dataset.targets[index]
            if targets is not None:
                for target in targets:
                    record['annotations'].append(dict({
                        'bbox': target['bbox'].tolist(),
                        'bbox_mode': BoxMode.XYXY_ABS,
                        'category_id': target['category']
                    }))
                num_records += 1
            dataset_dict.append(record)

        logger.info(f"Transformed the dataset into COCO style. "
                     f"Num Images {len(dataset_dict)} and Num Annotations: {num_records}")

        return dataset_dict


def torch_distributed_launch(nproc_per_node, training_script, training_script_args=""):
    args = {}
    
    args["nnodes"] =  1
    args["node_rank"] = 0
    args["nproc_per_node"] = nproc_per_node
    args["master_addr"] = "127.0.0.2"
    args["master_port"] = 29501
    args["use_env"] = False
    args["module"] = False
    args["no_python"] = False
    args["training_script"] = training_script
    args["training_script_args"] = training_script_args
    

    # world size in terms of number of processes
    dist_world_size = args["nproc_per_node"] * args["nnodes"]

    # set PyTorch distributed related environmental variables
    current_env = os.environ.copy()
    current_env["MASTER_ADDR"] = args["master_addr"]
    current_env["MASTER_PORT"] = str(args["master_port"])
    current_env["WORLD_SIZE"] = str(dist_world_size)
    current_env["NGPU"] = str(args["nproc_per_node"])
    processes = []

    if 'OMP_NUM_THREADS' not in os.environ and args["nproc_per_node"] > 1:
        current_env["OMP_NUM_THREADS"] = str(1)
        print("*****************************************\n"
            "Setting OMP_NUM_THREADS environment variable for each process "
            "to be {} in default, to avoid your system being overloaded, "
            "please further tune the variable for optimal performance in "
            "your application as needed. \n"
            "*****************************************".format(current_env["OMP_NUM_THREADS"]))

    for local_rank in range(0, args["nproc_per_node"]):
        # each process's rank
        dist_rank = args["nproc_per_node"] * args["node_rank"] + local_rank
        current_env["RANK"] = str(dist_rank)
        current_env["LOCAL_RANK"] = str(local_rank)

        # spawn the processes
        with_python = not args["no_python"]
        cmd = []
        if with_python:
            cmd = [sys.executable, "-u"]
            if args["module"]:
                cmd.append("-m")
        else:
            if not args["use_env"]:
                raise ValueError("When using the '--no_python' flag, you must also set the '--use_env' flag.")
            if args["module"]:
                raise ValueError("Don't use both the '--no_python' flag and the '--module' flag at the same time.")

        cmd.append(args["training_script"])
        cmd.extend(args["training_script_args"])
        cmd.extend(["--local_rank", f"{local_rank}"])

        process = subprocess.Popen(cmd, env=current_env)
        processes.append(process)

    def log_subprocess_output(pipe):
        for line in iter(pipe.readline, b''): # b'\n'-separated lines
            logging.info('got line from subprocess: %r', line)
        
    for process in processes:
        process.wait()
        if process.returncode != 0:
            raise subprocess.CalledProcessError(returncode=process.returncode,
                                                cmd=[cmd])
