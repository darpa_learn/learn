import os
import logging
logger = logging.getLogger(__name__)

import time
import datetime

from learn.algorithms.FsDet.objectDetectionAdapter import ObjectDetectionAdapter

import torch
import numpy as np
from contextlib import contextmanager
import torch
torch.multiprocessing.set_sharing_strategy('file_system')
import gc
import ubelt as ub
import copy
from learn.utils.metrics import mean_average_precision, mAP
import pandas as pd
import yaml
import json
from hydra.utils import get_original_cwd
import argparse
import gc
import psutil

import torch.distributed as dist
from torch.nn.parallel import DistributedDataParallel as DDP
import torchvision

from detectron2.data.build import get_detection_dataset_dicts
from detectron2.data.build import build_detection_train_loader
from detectron2.data.samplers import TrainingSampler   # this sampler causes distributed training to hang
from detectron2.data.dataset_mapper import DatasetMapper
from detectron2.utils.events import get_event_storage
from detectron2.engine.hooks import HookBase
from detectron2.utils import comm
from detectron2.config import CfgNode
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.engine import DefaultTrainer, launch
from detectron2.config import get_cfg
from detectron2 import model_zoo
from detectron2.utils.comm import get_world_size
from detectron2.utils.logger import log_every_n_seconds
from detectron2.checkpoint import DetectionCheckpointer, Checkpointer
from detectron2.model_zoo import get_checkpoint_url
from tempfile import TemporaryDirectory


class NewSampler(torch.utils.data.Sampler):
    def __init__(self, len_dataset, num_overall_samples, num_replicas=None, rank=None):
        self.len_dataset = len_dataset
        self.num_overall_samples = num_overall_samples
        self.num_replicas = num_replicas
        self.rank = rank

    def __iter__(self):
        weights = torch.ones((self.len_dataset,)) / self.len_dataset
        sub_inds = torch.multinomial(weights, self.num_overall_samples, replacement=True).tolist()
        sub_inds = sub_inds[self.rank:len(sub_inds):self.num_replicas]
        return iter(sub_inds)

    def __len__(self):
        return self.num_overall_samples


# just need an object that has fields for the transform_dataset function
class ObjectDetectionDataset(torchvision.datasets.VisionDataset):
    def __init__(self, cfg):
        self.name = cfg["name"]
        self.root = cfg["root"]
        self.categories = cfg["categories"]
        self.image_fnames = cfg["image_fnames"]
        self.num_images = cfg["num_images"]
        self.labeled_size = cfg["labeled_size"]
        self.unlabeled_size = cfg["unlabeled_size"]
        self.targets = cfg["targets"]
        self.labeled_indices = cfg["labeled_indices"]
        self.unlabeled_indices = cfg["unlabeled_indices"]
        self.transform = None
        self.target_transform = None
        self.null_target = [{'category': -1, 'bbox': torch.tensor([-1, -1, -1, -1])}]

    def __len__(self) -> int:
        """
        Get length of unlabeled and labeled data.

        Returns:
            int: number of unlabeled and labeled data
        """
        return self.num_images



def transform_dataset(dataset):
    """ Transform our datasets into the coco format which can be then be registered
    into the dectectron system.  For now we are only training it but we might
    want to create a validation dataset to test on as well

    Args:
        dataset (framework.dataset):  Dataset from framework

    Returns
        dict in coco format.
    """

    # ######################### Get the code ready for detectron
    # Convert our dataset
    logger.info('Converting target_dataset to coco dataset')
    from detectron2.structures import BoxMode
    from detectron2.data.detection_utils import read_image
    from PIL import Image

    num_images = dataset.num_images
    dataset_dict = []
    num_records = 0
    for index in range(num_images):
        record = dict()
        record['file_name'] = dataset.root + '/' + dataset.image_fnames[index]
        img = Image.fromarray(read_image(record['file_name']))
        record['width'], record['height'] = img.size
        record['image_id'] = index
        record['annotations'] = list()
        targets = dataset.targets[index]
        if targets is not None:
            for target in targets:
                record['annotations'].append(dict({
                    'bbox': target['bbox'],   # .tolist(),
                    'bbox_mode': BoxMode.XYXY_ABS,
                    'category_id': target['category']
                }))
            num_records += 1
        dataset_dict.append(record)

    logger.info(f"Transformed the dataset into COCO style. "
                    f"Num Images {len(dataset_dict)} and Num Annotations: {num_records}")

    return dataset_dict



# main distributed training function
def trainer_func(filepath):

    #print(f"reached trainer func with world size {dist.get_world_size()} and rank {dist.get_rank()}")

    with open(os.path.join(filepath, 'ddp_config.json'), 'r') as jsonfile:
        ddp_config = json.load(jsonfile)

    train_dataset = ObjectDetectionDataset(ddp_config["target_dataset"])



    target_dataset_name = f'pretrain_{train_dataset.name}_train'
    if target_dataset_name in MetadataCatalog.list():
        target_dataset_name = f'pretrain1_{train_dataset.name}_train_adapt'
    #target_dataset_name = f'{train_dataset.name}_train'
    DatasetCatalog.register(target_dataset_name, lambda d=train_dataset: transform_dataset(d))
    train_cats = [str(cat) for cat in train_dataset.categories]
    MetadataCatalog.get(target_dataset_name).thing_classes = train_cats


    with open(os.path.join(filepath, "original_config.yaml"), 'r') as yamlfile:
        dct = yaml.load(yamlfile,  Loader=yaml.FullLoader)

    cfg = CfgNode(dct)
    print("Loaded config for trainer")

    cfg.MODEL.WEIGHTS = os.path.join(filepath, "initial_model.pth")

    trainer = DefaultTrainer(cfg)
    print("Created DefaultTrainer")
    trainer.resume_or_load(resume=False)  # loads weights from cfg.MODEL.WEIGHTS
    print("Loaded weights for distributed training")
    dist.barrier()

    
    train_dataset = get_detection_dataset_dicts(target_dataset_name)
    # train_sampler = TrainingSampler(len(train_dataset))   # this causes the hang
    
    ### Distributed training does not work with TrainingSampler from detectron2
    ### The processes will not join with the main process after training
    ### You must use a different sampler

    # class NewSampler(torch.utils.data.Sampler):
    #     def __init__(self, len_dataset, num_overall_samples, num_replicas=None, rank=None):
    #         self.len_dataset = len_dataset
    #         self.num_overall_samples = num_overall_samples
    #         self.num_replicas = num_replicas
    #         self.rank = rank

    #     def __iter__(self):
    #         weights = torch.ones((self.len_dataset,)) / self.len_dataset
    #         sub_inds = torch.multinomial(weights, self.num_overall_samples, replacement=True).tolist()
    #         sub_inds = sub_inds[self.rank:len(sub_inds):self.num_replicas]
    #         return iter(sub_inds)

    #     def __len__(self):
    #         return self.num_overall_samples

    a_lot = cfg.SOLVER.IMS_PER_BATCH * cfg.SOLVER.MAX_ITER * 100

    sampler = NewSampler(len(train_dataset), a_lot, dist.get_world_size(), dist.get_rank())
    loader = build_detection_train_loader(train_dataset, mapper=DatasetMapper(cfg, is_train=True), sampler=sampler, 
        total_batch_size=cfg.SOLVER.IMS_PER_BATCH, aspect_ratio_grouping=True, num_workers=cfg.DATALOADER.NUM_WORKERS)

    trainer._trainer.data_loader = loader
    trainer._trainer._data_loader_iter = iter(loader)

    trainer.train()

    dist.barrier()
    print("finished training, saving model")

    if int(dist.get_rank() == 0):
        chkptr = DetectionCheckpointer(trainer.model.module, save_dir=filepath)
        chkptr.save(name='final_model') 

    dist.barrier()

    print("Finished saving checkpoint, returning to pretrain")
    #print(dist.get_world_size(), dist.get_rank())
    del trainer
    gc.collect() 
    torch.cuda.empty_cache()
    return



# main function if using torch.distributed.launch
# not used if using detectron2's launch function
if __name__=="__main__":

    trainer_func()
    print("reached main in distributed.py after trainer_func")


