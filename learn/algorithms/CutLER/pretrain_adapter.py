import abc
from learn.deprecated_tinker.basealgorithm import BaseAlgorithm


class PretrainAdapter(BaseAlgorithm):

    def __init__(self, toolset):
        BaseAlgorithm.__init__(self, toolset)

    def execute(self, toolset, step_descriptor):
        """
        stage is a string that is passed in according to the protocol. It
        identifies which stage of the task is being requested (e.g. 'train',
        'adapt' ) execution does not return anything, it is purely for the sake of
        altering the internal model. Available resources for training can be
        retrieved using the BaseAlgorithm functions.
        """
        self.toolset = toolset
        strategy = toolset['protocol_config']['self_supervision_pretrain'] \
            ['params']['pretrain_on_source_target_strategy']
        if step_descriptor == 'Initialize':
            if strategy == 'source':
                datasets = [self.toolset['source_dataset']]
            elif strategy == 'target':
                datasets = [self.toolset['target_dataset']]
            elif strategy == 'source_then_target':
                raise NotImplementedError(f'CutLER strategy "{strategy}" not yet implemented')
            elif strategy == 'source_and_target':
                datasets = [self.toolset['source_dataset'], self.toolset['target_dataset']]
            else:
                raise ValueError(f'CutLER strategy "{strategy}" does not exist')

            # we always pretrain on top of the "source model"
            return self.initialize(toolset['source_network'], datasets, toolset['stage'])

        elif step_descriptor == 'Pretrain':
            if strategy is None:
                return
            else:
                return self.pretrain()
        else:
            raise NotImplementedError(f'Step {step_descriptor} not implemented')

    @abc.abstractmethod
    def initialize(self, initial_model, datasets):
        raise NotImplementedError

    @abc.abstractmethod
    def pretrain(self):
        raise NotImplementedError
