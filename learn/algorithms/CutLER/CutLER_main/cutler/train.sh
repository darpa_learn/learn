export DETECTRON2_DATASETS="../../"
CUDA_VISIBLE_DEVICES=1,2,3,4 python train_net.py --num-gpus 4 \
  --config-file model_zoo/configs/CutLER-ImageNet/cascade_mask_rcnn_R_50_FPN_poolcar.yaml \
  OUTPUT_DIR output/