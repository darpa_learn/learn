""" 
File to be executed by MMDET's object_detection.py for distributed training
"""

import os
import sys
import logging
logger = logging.getLogger(__name__)

import time
import datetime
import random

from learn.algorithms.MMDET.objectDetectionAdapter import ObjectDetectionAdapter
from learn.algorithms.FsDet.distributed import trainer_func
from learn.utils.wandb_utils import WANDB_INFO
from learn.utils.metrics import mean_average_precision, mAP

import learn.algorithms.MMDET.register_modules

import torch
import torch.nn.functional as F
import torch.distributed as dist
import numpy as np
from contextlib import contextmanager
import gc
import ubelt as ub
import copy
import pandas as pd
import yaml
import json
from hydra.utils import get_original_cwd
# from IPython import embed
from tempfile import TemporaryDirectory
import argparse
import psutil

import mmcv
from mmcv.runner import load_checkpoint, get_dist_info, init_dist
from mmcv import Config
import mmcls
from mmcls.datasets import build_dataset
from mmcls.models import build_classifier
from mmcls.utils import collect_env
from mmcls.apis import init_random_seed, set_random_seed, train_model


torch.multiprocessing.set_start_method('spawn', force=True)
os.environ['OMP_NUM_THREADS'] = "1"


def parse_args():
    """
    Parse arguments for torch.distributed.launch function
    """
    parser = argparse.ArgumentParser(description='Train MMCLS with DDP')
    # add more arguments here

    parser.add_argument(
        '--work_dir',
        type=str,
        default=None,
        help='the dir to save logs and models')
    parser.add_argument('--seed', type=int, default=None, help='random seed')
    parser.add_argument(
        '--gpus',
        type=int,
        default=1,
        help='number of gpus to use')
    parser.add_argument(
        '--model_checkpoint_file',
        type=str,
        default=None,
        help='file containing models checkpoint to load')
    parser.add_argument('--local_rank', type=int, default=0)
    parser.add_argument('--freeze', type=bool, default=False)
    args = parser.parse_args()
    if 'LOCAL_RANK' not in os.environ:
        os.environ['LOCAL_RANK'] = str(args.local_rank)

    return args


def trainer_func():
    args = parse_args()
    dist.init_process_group(backend="nccl", init_method='env://', rank=int(os.getenv("LOCAL_RANK")), world_size=args.gpus)
    filepath = args.work_dir
    torch.cuda.set_device(args.local_rank)

    # set random seed so both processes have the same model
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    torch.cuda.manual_seed_all(args.seed)

    print(f"reached trainer func with world size {dist.get_world_size()} and rank {dist.get_rank()}")

    cfg_pth = os.path.join(filepath, 'mmcls_config.py')
    mmcls_config = Config.fromfile(cfg_pth)

    meta = dict()
    env_info_dict = collect_env()
    env_info = '\n'.join([(f'{k}: {v}') for k, v in env_info_dict.items()])
    dash_line = '-' * 60 + '\n'
    logger.info('Environment info:\n' + dash_line + env_info + '\n' +
                dash_line)
    meta['env_info'] = env_info
    meta['config'] = mmcls_config.pretty_text
    meta['seed'] = args.seed

    num_classes = len(mmcls_config.classes)

    mmcls_config.seed = int(args.seed)
    mmcls_config.work_dir = filepath

    # print(torch.cuda.device_count(), os.getenv('CUDA_VISIBLE_DEVICES'), args.local_rank, type(args.local_rank))
    # torch.cuda.set_device(int(mmcls_config.gpu_ids[args.local_rank]))

    mmcls_config.gpu_ids = list([int(x) for x in range(dist.get_world_size())])
    datasets = [build_dataset(mmcls_config.data.train)]
    model = build_classifier(mmcls_config.model)

    if args.model_checkpoint_file is not None:
        chkpt = load_checkpoint(model, args.model_checkpoint_file)

    model.CLASSES = datasets[0].CLASSES
    model.train()

    print("mmcls config in distributed:")
    print(f'Config:\n{mmcls_config.pretty_text}')

    if args.freeze:
        for n,p in model.named_parameters():
            if "backbone" in n:
                p.requires_grad = False

    # train_detector(model, datasets, mmcls_config, distributed=True, validate=False, meta=meta)
    train_model(model, datasets, mmcls_config, distributed=True, validate=False, device=mmcls_config.gpu_ids, meta=meta)

    print(f'finished training rank {dist.get_rank()}')
    gc.collect()
    torch.cuda.empty_cache()
    return 0


if __name__=="__main__":
    print("reached main in distributed.py")
    trainer_func()
    print("finished trainer_func in main")
