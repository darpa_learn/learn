import abc
from learn.deprecated_tinker.basealgorithm import BaseAlgorithm


class ImageClassifierAdapter(BaseAlgorithm):

    def __init__(self, toolset):
        BaseAlgorithm.__init__(self, toolset)

    def execute(self, toolset, step_descriptor):
        """
        stage is a string that is passed in according to the protocol. It
        identifies which stage of the task is being requested (e.g. "train",
        "adapt" ) execution does not return anything, it is purely for the sake of
        altering the internal model. Available resources for training can be
        retrieved using the BaseAlgorithm functions.
        """
        self.toolset = toolset

        if step_descriptor == 'Initialize':
            return self.initialize(
                source_network=self.toolset["source_network"],
                source_dataset=self.toolset["source_dataset"],
                whitelist_datasets=self.toolset["whitelist_datasets"],
                target_dataset=self.toolset["target_dataset"],
                networks=self.toolset["networks"]
            )
        elif step_descriptor == 'DomainAdaptTraining':
            return self.domain_adapt_training(
                target_dataset=self.toolset["target_dataset"],
                eval_dataset=self.toolset["eval_dataset"]
            )
        elif step_descriptor == 'EvaluateOnTestDataSet':
            return self.inference(eval_dataset=self.toolset["eval_dataset"])
        else:
            raise NotImplementedError(f'Step {step_descriptor} not implemented')

    @abc.abstractmethod
    def initialize(self, source_network, source_dataset, whitelist_datasets, target_dataset, networks):
        raise NotImplementedError

    @abc.abstractmethod
    def domain_adapt_training(self, target_dataset, eval_dataset):
        raise NotImplementedError

    @abc.abstractmethod
    def inference(self, eval_dataset):
        raise NotImplementedError

