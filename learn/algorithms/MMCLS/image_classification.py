import random
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.utils.data as data
import copy
import ubelt as ub
from torchvision import transforms
import pandas as pd
import os
import time

# import learn.algorithms.MMCLS.register_modules
from learn.utils.dataset import ImageClassificationDataset
from learn.algorithms.MMCLS.imageClassificationAdapter import ImageClassifierAdapter
import logging
from learn.utils.wandb_utils import WANDB_INFO
from hydra.utils import get_original_cwd
import math
import sys
from typing import Iterable
import gc
import subprocess

import mmcv
from mmcv.runner import load_checkpoint
from mmcv import Config
import mmcls
from mmcls.datasets import build_dataset
from mmcls.models import build_classifier
from mmcls.utils import collect_env
from mmcls.apis import init_random_seed, set_random_seed, train_model
from mmcls.apis.inference import inference_model
from mmcv.parallel import collate, scatter
from mmcls.datasets.pipelines import Compose

from sklearn.metrics import confusion_matrix


logger = logging.getLogger(__name__)


class ImageClassifierAlgorithm(ImageClassifierAdapter):
    """
    A wrapper for interfacing with MMClassification: https://github.com/open-mmlab/mmclassification

    """

    def __init__(self, toolset):
        # def __init__(self, problem, base_dataset, adapt_dataset, arguments):
        ImageClassifierAdapter.__init__(self, toolset)

        self.config = toolset["protocol_config"]["image_classifier"][
            "params"
        ]  # configs/hydra_config/image_classifier/MMCLS.yaml

        device = self.config['device']
        if ub.iterable(device):
            self.device = device
        else:
            if device == -1:
                self.device = list(range(torch.cuda.device_count()))
            else:
                self.device = [device]
        if len(self.device) > torch.cuda.device_count():
            self.device = self.device[:torch.cuda.device_count()]

        self.ddp_num_gpus = len(self.device)

        if self.config["work_dir"] is not None:
            self.work_dir = self.config["work_dir"]
        else: 
            self.work_dir = self.toolset["temp_model_directory"]
        os.makedirs(self.work_dir, exist_ok=True)


    def initialize(
        self,
        source_network,
        source_dataset,
        whitelist_datasets,
        target_dataset,
        networks,
    ):
        """Initialize your algorithm and save any parameters or objects you want to be persistent between
        budget checkpoints.  DomainNetworkSelection will choose a potential source datasets `source_dataset`
        and provide a trained network for that dataset `source_dataset.` Please do not modify the datasets
        beyond overriding the transforms (self.transform and self.transform_target are fine to change).

        Args:
            source_network (pytorch.nn.Module):  Pretrained network on source task
            source_dataset (ImageClassificationDataset): Pytorch dataset for source domain/task
            whitelist_datasets (dict[framework.dataset]): Dictionary of all possible external datasets which
                can be used on this task.  You may use this if you want to select a different dataset than
                the one selected as source.
            target_dataset: (ImageClassificationDataset): Pytorch dataset for target domain/task
            networks: (Dict[str, Dict[str, nn.module]]): Two leveled dict with the dataset as the first key
                and the network structure name as the second key.  e.g. networks['imagenet_1k']['densenet169']
                Note: empty for now for memory savings
        """

        logger.info("Running MMCLS initialize")

        train_dataset = self.toolset['target_dataset']
        test_dataset = self.toolset['eval_dataset']
        self.target_dataset_name = f'{train_dataset.name}_train'
        self.eval_dataset_name = f'{test_dataset.name}_test'


    def domain_adapt_training(self, target_dataset, eval_dataset):
        """This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
        the dataloaders below and feel free to override the transformers in the datasets to use whatever
        pretraining you desire. Please do not modify the datasets though.

        Args:
            target_dataset (ImageClassificationDataset): pytorch dataset for the target dataset used for
                training
            eval_dataset (ImageClassificationDataset): eval dataset used for evaluation.  Included here in
                case you want to do transductive learning.
        """
        logger = logging.getLogger(__name__)
        logger.info("Running MMCLS domain_adapt_training")
        config = self.config

        # put the datasets in mmcls format
        train_dataset = self.toolset['target_dataset']
        self.transform_dataset(train_dataset, train=True)

        # load the config file and overwrite the necessary fields
        config_path = os.path.join(get_original_cwd(), './learn/algorithms/MMCLS/configs/'+self.config["model_config_file"])
        self.mmcls_config = self.set_config(config_path)
        self.mmcls_config.work_dir = self.work_dir

        original_chkpt_file = os.path.join(self.toolset['protocol_config']['domain_network_selector']['params']["pretrained_network_dir"], 
            config["model_checkpoint_file"])

        # loop over config, and if there are any num_classes, replace it
        # there might be problems with this (i.e. won't work with nested lists)
        # but I think it's fine for mmdet's config structure
        def replace(conf, depth):
            if depth <= 0:
                return
            try:
                for k,v in conf.items():
                    if isinstance(v, dict):
                        replace(v, depth-1)
                    elif isinstance(v, list):
                        for element in v:
                            replace(element, depth-1)
                    else:
                        # print(k,v)
                        if k == 'num_classes':
                            conf[k] = len(self.toolset["target_dataset"].categories)
                        if k == 'CLASSES':
                            conf[k] = self.toolset['target_dataset'].categories
            except:
                pass

        replace(self.mmcls_config, 500)
        self.mmcls_config.CLASSES = tuple(self.toolset['target_dataset'].categories)
        # self.mmcls_config.model.CLASSES = tuple(self.toolset['target_dataset'].categories)

        print(f'Config:\n{self.mmcls_config.pretty_text}')

        seed = self.config["seed"]
        random.seed(seed)
        np.random.seed(seed)
        torch.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)
        self.mmcls_config.seed = seed


        if self.config["use_ddp"] and torch.cuda.device_count() > 1 and self.ddp_num_gpus > 1 and len(self.device) > 1:

            logger.info("Using DDP")
            self.mmcls_config.dump(os.path.join(self.work_dir, 'mmcls_config.py'))
            training_args = ['--work_dir', self.work_dir, '--seed', str(seed), '--gpus', str(len(self.device)), \
                '--model_checkpoint_file', original_chkpt_file, '--freeze', f"{self.config['freeze_backbone']}"]
            training_script = os.path.join(get_original_cwd(), './learn/algorithms/MMCLS/distributed.py')
            self.torch_distributed_launch(nproc_per_node=len(self.device), training_script=training_script, training_script_args=training_args)
            logger.info("Returned from distributed training")

            # distributed model should be saved in the work dir
            model = build_classifier(self.mmcls_config.model)
            checkpoint_file = os.path.join(self.work_dir, 'latest.pth')
            chkpt = load_checkpoint(model, checkpoint_file)
            self.model = model

        else:

            meta = dict()
            env_info_dict = collect_env()
            env_info = '\n'.join([(f'{k}: {v}') for k, v in env_info_dict.items()])
            dash_line = '-' * 60 + '\n'
            logger.info('Environment info:\n' + dash_line + env_info + '\n' +
                        dash_line)
            meta['env_info'] = env_info
            meta['config'] = self.mmcls_config.pretty_text
            meta['seed'] = seed

            logger.info("building dataset")
            datasets = [build_dataset(self.mmcls_config.data.train)]

            logger.info("building classifier")
            model = build_classifier(self.mmcls_config.model)

            checkpoint_file = original_chkpt_file
            chkpt = load_checkpoint(model, checkpoint_file)

            logger.info("training model")

            # the fix below will log multiple mmdet things for training so set this to error
            # for now to avoid that
            log = logging.getLogger()
            log.handlers[0].level = logging.ERROR

            model.train()

            if self.config["freeze_backbone"]:
                for n,p in model.named_parameters():
                    if "backbone" in n:
                        p.requires_grad = False

            # train_detector(model, datasets, self.mmcls_config, distributed=False, validate=False, meta=meta)
            train_model(model, datasets, self.mmcls_config, distributed=False, validate=False, device=self.device, meta=meta)

            # somehow the root logger's stream handler is getting set to ERROR, so set it back to INFO manually
            # this will then set __name__'s log level to INFO again
            log = logging.getLogger()
            log.handlers[0].level = logging.INFO
            logger = logging.getLogger(__name__)

            logger.info("finished training")

            self.model = model


        gc.collect()  # Make sure all object have been deallocated if not used
        torch.cuda.empty_cache()


    def inference(self, eval_dataset):
        """
        Inference is during the evaluation stage.  For this example, the task_network is trained in the
        train and adapt stage's code and is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to JPL

        Args:
            eval_dataset (framework.datasets.ImageClassificationDataset): unlabeled evaluation framework.
                We need to predict the classes for each item in the dataset.

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        torch.cuda.set_device(self.device[0]) # inference after ddp is slow without this
        self.model.cpu()
        self.model.cuda()

        self.mmcls_config.data.test.test_mode = True
        # test_dataset = build_dataset(self.mmcls_config.data.test)
        # test_loader = build_dataloader(test_dataset,
        #     samples_per_gpu=1, # batch size of each gpu
        #     workers_per_gpu=1,
        #     num_gpus=1,
        #     dist=False,
        #     shuffle=False)
        self.model.cfg = self.mmcls_config

        self.model.CLASSES = tuple(self.toolset['target_dataset'].categories)

        test_pipeline = Compose(self.mmcls_config.data.test.pipeline)

        test_loader = torch.utils.data.DataLoader(self.toolset["eval_dataset"], batch_size=1) # self.config["batch_size"])
        self.model.eval()
        logger.info(f"Starting inference for {len(test_loader)} batches")
        start_time = time.time()
        last_log_time = time.time()

        preds = []
        indices = []
        # confidences = []

        for inference_itr, (img, _, inds) in enumerate(test_loader):
            if time.time() - last_log_time > 30:
                print(f"Completed {inference_itr}/{len(test_loader)} batches, {(time.time() - start_time):.2f} seconds elapsed")
                last_log_time = time.time()

            filename = os.path.join(self.toolset["eval_dataset"].root, self.toolset["eval_dataset"].index_to_image_fname[inds.cpu().item()])
            # results = inference_model(self.model, filename)

            data = dict(img_info=dict(filename=filename), img_prefix=None)
            data = test_pipeline(data)
            data = collate([data], samples_per_gpu=1)
            if next(self.model.parameters()).is_cuda:
                # scatter to specified GPU
                data = scatter(data, [self.device[0]])[0]

            with torch.no_grad():
                scores = self.model(return_loss=False, **data)
                scores = scores[0]

            if self.config["probabilistic_predictions"]:
                preds.append(scores)
            else:
                class_pred = np.argmax(scores)
                preds.append(class_pred)

            # {'pred_label': 15, 'pred_score': 0.028634032234549522, 'pred_class': 'camel'}
            indices.append(inds.cpu().item())
            # preds.append(results["pred_label"])
            # confidences.append(results["pred_score"])

        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()

        if self.config["probabilistic_predictions"]:
            predictions = pd.DataFrame(preds, columns=np.arange(len(preds[0])))
            predictions["id"] = indices
            return predictions
        else:
            return preds, indices


    def set_config(self, path):
        """
        Load an MMCLS config for training.
        """

        train_dataset =  self.toolset['target_dataset']
        test_dataset = self.toolset['eval_dataset']
    
        mmcls_config = Config.fromfile(path)

        mmcls_config.dataset_type = 'CustomDataset'
        mmcls_config.data_prefix = train_dataset.root
        train_pipeline = mmcls_config.train_pipeline
        test_pipeline = mmcls_config.test_pipeline
        mmcls_config.classes = tuple(train_dataset.categories) # tuple(train_dataset.category_to_category_index.values())

        mmcls_config.data.train.type = 'CustomDataset'
        mmcls_config.data.train.ann_file = str(os.path.join(self.work_dir, 'train_annotations.txt'))
        mmcls_config.data.train.data_prefix = train_dataset.root
        mmcls_config.data.train.classes =  tuple(train_dataset.categories) # tuple(train_dataset.categories)

        mmcls_config.data.val.type = 'CustomDataset'
        mmcls_config.data.val.ann_file = str(os.path.join(self.work_dir, 'train_annotations.txt'))
        mmcls_config.data.val.data_prefix = train_dataset.root
        mmcls_config.data.val.classes =  tuple(train_dataset.categories) # tuple(train_dataset.categories)

        mmcls_config.data.test.type = 'CustomDataset'
        mmcls_config.data.test.ann_file = str(os.path.join(self.work_dir, 'train_annotations.txt'))
        mmcls_config.data.test.data_prefix = train_dataset.root
        mmcls_config.data.test.classes =  tuple(train_dataset.categories) # tuple(train_dataset.categories)

        mmcls_config.log_config.interval = self.config["log_interval"]
        mmcls_config.checkpoint_config.interval = self.config["checkpoint_interval"]
        mmcls_config.data.samples_per_gpu = self.config["batch_size"]
        mmcls_config.gpu_ids = self.device
        mmcls_config.device = 'cuda'
        mmcls_config.work_dir = self.config["work_dir"]

        ckpt = int(self.toolset["ckpt"])
        if len(self.config["iters_per_ckpt"]) > 0 and ckpt < len(self.config["iters_per_ckpt"]):
            num_iter = self.config["iters_per_ckpt"][ckpt]
        else:
            num_iter = self.config["max_iters"]
            
        mmcls_config.lr_config.warmup_iters = 1 if self.config["warmup_iters"] >= num_iter else self.config["warmup_iters"]
        if "step" in mmcls_config.lr_config:
            mmcls_config.lr_config.step = [step for step in self.config["lr_steps"] if step < num_iter]
        mmcls_config.runner = {'type': 'IterBasedRunner', 'max_iters': num_iter}
        mmcls_config.optimizer.lr = self.config["lr"]

        return mmcls_config



    def transform_dataset(self, dataset, train=False):
        """ Transform our datasets into the coco format which can be then be registered
        into the dectectron system.  For now we are only training it but we might
        want to create a validation dataset to test on as well

        Args:
            dataset (framework.dataset):  Dataset from framework

        Returns
            dict in coco format.
        """
        logger.info('Converting target_dataset to mmcls dataset')

       # need to write annotation.txt file where each row is image name and then class index separated by a space

        # dataset.category_to_category_index

        categories = []
        images = []
        
        ##### I think the categories are already indices?

        with open(os.path.join(self.work_dir, 'train_annotations.txt'), "w") as f:
            for index in dataset.get_labeled_indices():
                filename = dataset.image_fnames[index]
                targ = dataset.targets[index]
                f.write(f"{filename} {targ}\n")
                categories.append(targ)
                images.append(filename)

        all_labels = set(categories)
        print(f"Set of all potential categories: {all_labels} \n")

        logger.info(f"Transformed the dataset into MMCLS style. "
                     f"Num Images {len(images)}")


    # based on learn.algorithms.BU_NLP.distributed_launch.torch_distributed_launch
    def torch_distributed_launch(self, nproc_per_node, training_script, training_script_args):
        args = {}

        # os.environ['CUDA_VISIBLE_DEVICES'] = ','.join([str(x) for x in self.device])
    
        os.environ['OMP_NUM_THREADS'] = "1"

        args["nnodes"] =  1
        args["node_rank"] = 0
        args["nproc_per_node"] = nproc_per_node
        args["master_addr"] = "127.0.0.2"
        args["master_port"] = self.config["port"]
        args["use_env"] = False
        args["module"] = False
        args["no_python"] = False
        args["training_script"] = training_script
        args["training_script_args"] = training_script_args
        
        # world size in terms of number of processes
        dist_world_size = args["nproc_per_node"] * args["nnodes"]

        # set PyTorch distributed related environmental variables
        current_env = os.environ.copy()
        current_env["MASTER_ADDR"] = args["master_addr"]
        current_env["MASTER_PORT"] = str(args["master_port"])
        current_env["WORLD_SIZE"] = str(dist_world_size)
        current_env["NGPU"] = str(args["nproc_per_node"])
        processes = []

        if 'OMP_NUM_THREADS' not in os.environ and args["nproc_per_node"] > 1:
            current_env["OMP_NUM_THREADS"] = str(1)
            print("*****************************************\n"
                "Setting OMP_NUM_THREADS environment variable for each process "
                "to be {} in default, to avoid your system being overloaded, "
                "please further tune the variable for optimal performance in "
                "your application as needed. \n"
                "*****************************************".format(current_env["OMP_NUM_THREADS"]))

        for local_rank in range(0, args["nproc_per_node"]):
            # each process's rank
            dist_rank = args["nproc_per_node"] * args["node_rank"] + local_rank
            current_env["RANK"] = str(dist_rank)
            current_env["LOCAL_RANK"] = str(local_rank)

            # spawn the processes
            with_python = not args["no_python"]
            cmd = []
            if with_python:
                cmd = [sys.executable, "-u"]
                if args["module"]:
                    cmd.append("-m")
            else:
                if not args["use_env"]:
                    raise ValueError("When using the '--no_python' flag, you must also set the '--use_env' flag.")
                if args["module"]:
                    raise ValueError("Don't use both the '--no_python' flag and the '--module' flag at the same time.")

            cmd.append(args["training_script"])

            if not args["use_env"]:
                cmd.append("--local_rank={}".format(local_rank))

            cmd.extend(args["training_script_args"])

            process = subprocess.Popen(cmd, env=current_env)
            processes.append(process)

            
        for process in processes:
            process.wait()
            if process.returncode != 0:
                raise subprocess.CalledProcessError(returncode=process.returncode,
                                                    cmd=[cmd])