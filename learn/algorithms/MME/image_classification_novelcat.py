import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.utils.data as data
import copy
import ubelt as ub
from torchvision import transforms
import pandas as pd

import os
from learn.algorithms.MME.imageClassificationAdapter import ImageClassifierAdapter
from learn.algorithms.MME.model.basenet import Predictor, Predictor_deep, Predictor_baseline, Predictor_deep_baseline, Predictor_baselinepp, Predictor_deep_basepp
from learn.algorithms.MME.utils.loss import entropy, adentropy
from learn.algorithms.MME.utils.lr_schedule import inv_lr_scheduler
from learn.utils.dataset import ImageClassificationDataset
import logging
from learn.utils.wandb_utils import WANDB_INFO
from sklearn.model_selection import train_test_split
import wandb

log = logging.getLogger(__name__)


class ImageClassifierAlgorithm(ImageClassifierAdapter):
    """
    This implementation follows MME (https://arxiv.org/pdf/1904.06487.pdf) except this allows for
    non-perfect overlap between the source and target domain labels.

    """

    def __init__(self, toolset):
        # def __init__(self, problem, base_dataset, adapt_dataset, arguments):
        ImageClassifierAdapter.__init__(self, toolset)

        self.config = toolset['protocol_config']['image_classifier']['params']
        self.hyperparam_config = toolset['protocol_config']['image_classifier']['hyperparams']

        device = self.config['device']
        if ub.iterable(device):
            self.device = device[0]
        else:
            self.device = device

    def initialize(self, source_network, source_dataset, whitelist_datasets, target_dataset, networks):
        """  Initialize your algorithm and save any parameters or objects you want to be persistent between
        budget checkpoints.  DomainNetworkSelection will choose a potential source datasets `source_dataset`
        and provide a trained network for that dataset `source_dataset.` Please do not modify the datasets
        beyond overriding the transforms (self.transform and self.transform_target are fine to change).

        Args:
            source_network (pytorch.nn.Module):  Pretrained network on source task
            source_dataset (ImageClassificationDataset): Pytorch dataset for source domain/task
            whitelist_datasets (dict[framework.dataset]): Dictionary of all possible external datasets which
                can be used on this task.  You may use this if you want to select a different dataset than
                the one selected as source.
            target_dataset: (ImageClassificationDataset): Pytorch dataset for target domain/task
            networks: (Dict[str, Dict[str, nn.module]]): Two leveled dict with the dataset as the first key
                and the network structure name as the second key.  e.g. networks['imagenet_1k']['densenet169']
                Note: empty for now for memory savings



        """
        log.info('Initializing MME algorithm')
        # Note: whitelist is contained here in case you want the change the source dataset
        log.debug(f"{whitelist_datasets.keys()}")
        self.source_dataset = source_dataset
        torch.cuda.manual_seed(self.config['seed'])
        self.original_source_network = copy.deepcopy(source_network)
        G = source_network  # Note: source network only imagenet for now
        self.source_network = source_network
        G.set_as_feature_extractor()
        self.config['net'] = G.model_name
        inc = G.num_feature_dims
        self.target_categories = target_dataset.categories.tolist()
        self.target_categories = list(map(lambda l: str(l).lower().replace('_', ' ').replace('-', ' '),
                 self.target_categories))
        self.source_categories = source_dataset.categories.tolist()
        self.source_categories = list(map(lambda l: str(l).lower().replace('_', ' ').replace('-', ' '),
                 self.source_categories))

        source_cats = set(self.source_categories)
        target_cats = set(self.target_categories)
        common_categories = source_cats & target_cats
        target_only_cats = target_cats - source_cats
        self.total_categories = list(source_cats | target_cats)

        # get mapping list to consider class overlap. 
        self.map_source2total = [self.total_categories.index(i) for i in self.source_categories]
        self.map_target2total = [self.total_categories.index(i) for i in self.target_categories]

        self.hyperparam_prev_inds = []

        logging.info(f'Number of categories in common: {len(common_categories)}')
        logging.info(f'Number of Categories only in target: {len(target_only_cats)}')

        if self.config['mme_auto_off'] and len(target_only_cats) > 0:
            log.info('Only run baseline')
            self.config['only_target_mme'] = True
            self.config['rate_threshold'] = 1000000000

        # If UDA, use previous classification network, otherwise, make a random one
        if not (isinstance(self.original_source_network.classifier, (Predictor_deep_baseline, Predictor_deep_basepp, Predictor_deep, Predictor_baseline, Predictor_baselinepp, Predictor)) and len(common_categories) == len(target_cats)):
            if "resnet" in self.config['net']:
                if self.config['mme_baseline_classifier']:
                    F1 = Predictor_deep_baseline(num_class=len(self.total_categories),
                                inc=inc)
                elif self.config['mme_baseline_plusplus']:
                    F1 = Predictor_deep_basepp(num_class=len(self.total_categories), inc=inc,
                                               temp=self.config['T'])
                else:
                    F1 = Predictor_deep(num_class=len(self.total_categories),
                                inc=inc)
            else:
                if self.config['mme_baseline_classifier']:
                    F1 = Predictor_baseline(num_class=len(self.total_categories), inc=inc)
                elif self.config['mme_baseline_plusplus']:
                    F1 = Predictor_baselinepp(num_class=len(self.total_categories), inc=inc,
                               temp=self.config['T'])
                else:
                    F1 = Predictor(num_class=len(self.total_categories), inc=inc,
                               temp=self.config['T'])
            self.F1 = F1
            self.original_source_network.classifier = self.F1
            G.classifier = self.F1
        else:
            self.F1 = source_network.classifier
        self.G = G
        return


    @staticmethod
    def get_weights_for_sampler(dataset):
        """ get weight the dataset to give to pytorch's weightedrandomsampler.
        This is instead of subset selector so that we can weight each element.

        Args:
            dataset (ImageClassificationDataset): dataset which you are weighting
            useful_cats (list[str]): categories to weight, usually for removing
                categories from source dataset

        Returns:
            list[float]: weights for random sampler
        """
        # get counts for each class
        targets = np.array(dataset.targets)
        n = len(targets)
        cls, counts = np.unique(targets[targets!=None], return_counts=True)


        ## set weights to zero for those indices that are not in labeled_indices and not in unlabeled_indices
        # get difference between union of labeled and unlabeled and all indices
        union_lab_unlab = np.union1d(dataset.get_unlabeled_indices(), dataset.get_labeled_indices())
        diff = np.setdiff1d(np.arange(len(dataset)), union_lab_unlab)
        print('len lab in weights: ', len(dataset.get_labeled_indices()))
        print('len unlab in weights: ', len(dataset.get_unlabeled_indices()))
        print('len total dataset in weights: ', len(dataset))
        print('len diff: ', len(diff))  # 92
        print(diff)
        

        count_dict = dict(zip(cls, counts))

        # # Set counts to 0 if unused
        # cats = dataset.categories
        # cats_to_drop = list(set(cats) - set(useful_cats))
        # drop_idxs = dataset._category_name_to_category_index(cats_to_drop)

        weight_dict = dict({None: 0})
        for k, v in count_dict.items():
            weight_dict[k] = float(n/v)
        # Set weights

        weights = [weight_dict[t] for t in targets]
        weights = np.array(weights)
        weights[diff] = 0.0
        weights = weights.tolist()

        return weights
        # return [weight_dict[t] for t in targets]

    def get_dataloaders(self, source_dataset, target_dataset):
        """ Get dataloaders for source labeled for only target classes, target labeled,
        and target unlabeled

        Args:
            source_dataset (ImageClassificationDataset): source dataset
            target_dataset (ImageClassificationDataset): target dataset

        Returns:
            dataloaders for source labeled for only target classes, target labeled,
                and target unlabeled

        """
        class_names = target_dataset.categories

        a_lot = int(1e6)
        logging.info('Creating dataloaders')


        print('len target lab inds in get_dataloaders: ', len(target_dataset.get_labeled_indices()))




        # Source
        source_weights = self.get_weights_for_sampler(source_dataset)
        source_labeled_dataloader = torch.utils.data.DataLoader(
            source_dataset,
            sampler=torch.utils.data.sampler.WeightedRandomSampler(
                source_weights,
                a_lot),
            batch_size=min(source_dataset.labeled_size,
                           int(self.config[
                                   "batch_size"])),
            num_workers=int(self.config['num_workers']/3),
            collate_fn=source_dataset.collate_batch,
            drop_last=False,
        )

        target_labeled_dataloader = None
        if target_dataset.labeled_size > 0:
            target_weights = self.get_weights_for_sampler(target_dataset)
            # Source
            target_labeled_dataloader = torch.utils.data.DataLoader(
                target_dataset,
                sampler=torch.utils.data.sampler.WeightedRandomSampler(
                    target_weights,
                    a_lot),
                batch_size=min(target_dataset.labeled_size,
                               int(self.config[
                                       "batch_size"])),
                num_workers=int(self.config['num_workers']/3),
                collate_fn=target_dataset.collate_batch,
                drop_last=False,
        )

        target_unlabeled_dataloader = None
        if target_dataset.unlabeled_size > 0:
            unlabeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
                target_dataset.get_unlabeled_indices()
            )
            target_unlabeled_dataloader = torch.utils.data.DataLoader(
                target_dataset,
                sampler=unlabeled_sampler,
                batch_size=min(target_dataset.unlabeled_size,
                               int(self.config["batch_size"])
                               ),
                num_workers=int(self.config['num_workers']/3),
                drop_last=False,
            )

        return source_labeled_dataloader, target_labeled_dataloader, target_unlabeled_dataloader, class_names

    def mapping_to_total(self, gt_labels, map_list):
        return torch.LongTensor([map_list[i] for i in gt_labels.squeeze().tolist()]).to(self.device)

    def domain_adapt_training(self, target_dataset, eval_dataset):
        """ This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
        the dataloaders below and feel free to override the transformers in the datasets to use whatever
        pretraining you desire. Please do not modify the datasets though.

        Args:
            target_dataset (ImageClassificationDataset): pytorch dataset for the target dataset used for
                training
            eval_dataset (ImageClassificationDataset): eval dataset used for evaluation.  Included here in
                case you want to do transductive learning.
        """
        log.info('Few-shot Adaption with MME Algorithm')
        config = self.config

        # override transforms
        crop_size = 227
        if config['net'] != 'alexnet':
            crop_size = 224

        trans = transforms.Compose([
            transforms.Resize(256),
            transforms.RandomHorizontalFlip(),
            transforms.RandomCrop(crop_size),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])

        self.source_dataset.transform = trans
        target_dataset.transform = trans

        # Get dataloaders for source and target
        source_loader, target_loader, target_loader_unl, class_list = self.get_dataloaders(
            self.source_dataset, target_dataset
        )


        print('len target labeled inds in domain_adapt_training: ', len(target_dataset.get_labeled_indices()))

        self.target_category_ind = target_dataset._category_name_to_category_index(target_dataset.categories)
        method = config['method']

        # If not warm start, use original source weights
        if not config['warm_start']:
            log.info('Using Pre-training Parameters rather than using last budget level')
            self.G = copy.deepcopy(self.original_source_network)
            self.G.set_as_feature_extractor()
            self.F1 = self.G.classifier
            self.toolset['source_network'] = self.G

        else:
            log.info('Using last budget level if available')

        G = self.G.train().to(self.device)
        F1 = self.F1.train().to(self.device)

        # for name, param in G.named_parameters():
        #     # assert self.before_hyperparam_G[name].cpu() == param.clone().cpu()
        #     assert torch.equal(self.before_hyperparam_G[name].cpu(), param.clone().cpu())

        # for name, param in F1.named_parameters():
        #     # assert self.before_hyperparam_F[name].cpu() == param.clone().cpu()
        #     assert torch.equal(self.before_hyperparam_F[name].cpu(), param.clone().cpu())

        # assert len(set(self.before_hyperparam_target_labels).difference(set(target_dataset.get_labeled_indices()))) == 0
        # assert len(set(target_dataset.get_labeled_indices()).difference(set(self.before_hyperparam_target_labels))) == 0


        params = []
        for key, value in dict(G.named_parameters()).items():
            if value.requires_grad:
                if 'classifier' not in key:
                    params += [{'params': [value], 'lr': self.config['multi'],
                                'weight_decay': 0.0005}]
                else:
                    params += [{'params': [value], 'lr': self.config['multi'] * 10,
                                'weight_decay': 0.0005}]

        # ##################### fine-tune/train your model ######################
        #  Here is where you will actual finetune the model
        #  You can use the pretrained model with self.model
        #  Make sure to set any persistent tensors to cpu with `tensor.cpu` command

        optimizer_g = optim.SGD(params, momentum=0.9,
                                weight_decay=0.0005, nesterov=True)
        optimizer_f = optim.SGD(list(F1.parameters()), lr=1.0, momentum=0.9,
                                weight_decay=0.0005, nesterov=True)

        def zero_grad_all():
            optimizer_g.zero_grad()
            optimizer_f.zero_grad()

        param_lr_g = []
        for param_group in optimizer_g.param_groups:
            param_lr_g.append(param_group["lr"])
        param_lr_f = []
        for param_group in optimizer_f.param_groups:
            param_lr_f.append(param_group["lr"])
        criterion = nn.CrossEntropyLoss()

        all_step = config['steps']

        if not config['only_target_mme']:
            data_iter_s = iter(source_loader)
        if target_loader is not None:
            data_iter_t = iter(target_loader)

        if target_loader_unl is not None:
            data_iter_t_unl = iter(target_loader_unl)
        len_train_source = len(source_loader)
        if target_loader is not None:
            len_train_target = len(target_loader)
        else:
            # So high it should never trigger
            len_train_target = 1e9999

        if target_loader_unl is not None:
            len_train_target_semi = len(target_loader_unl)
        else:
            # So high it should never trigger
            len_train_target_semi = 1e9999
        best_loss = 1e10
        counter = 0
        loss_avg = 0
        loss_t_avg = 0
        prog = ub.ProgIter(range(all_step), desc="Training MME")
        if target_loader_unl is not None and target_loader is not None:
            rate_labeled = float(max(len(target_loader_unl.dataset.get_unlabeled_indices()), 1)
                                 / len(target_loader.dataset.get_labeled_indices()))
            flag_unl_training = rate_labeled < self.config["rate_threshold"]
        elif target_loader_unl is not None and target_loader is None:
            flag_unl_training = False
            rate_labeled = 0
        else:
            flag_unl_training = True

        if flag_unl_training:
            log.info("disable MME training")
        else:
            log.info("unlabeled / labeled %s" % (rate_labeled))


        print('init lr: ', config['lr'])
        if WANDB_INFO.run_wandb:
            WANDB_INFO.log_metrics({'lr': config["lr"]})

        for step in prog:
            optimizer_g = inv_lr_scheduler(param_lr_g, optimizer_g, step,
                                           init_lr=config['lr'])
            optimizer_f = inv_lr_scheduler(param_lr_f, optimizer_f, step,
                                           init_lr=config['lr'])
            lr = optimizer_f.param_groups[0]['lr']
            if step % len_train_target == 0 and target_loader is not None:
                data_iter_t = iter(target_loader)
            if step % len_train_target_semi == 0 and target_loader_unl is not None:
                data_iter_t_unl = iter(target_loader_unl)
            if not config['only_target_mme'] and step % len_train_source == 0:
                data_iter_s = iter(source_loader)

            zero_grad_all()
            if target_loader is not None:
                im_data_t, gt_labels_t, _ = next(data_iter_t)
                # need to map the labels considering class overlap
                gt_labels_t = self.mapping_to_total(gt_labels_t, self.map_target2total)
            if not config['only_target_mme']:
                im_data_s, gt_labels_s, _ = next(data_iter_s)
                gt_labels_s = self.mapping_to_total(gt_labels_s, self.map_source2total)
                if target_loader is not None: # If not UDA (no target labeled)
                    data = torch.cat((im_data_s, im_data_t), 0).to(self.device)
                    target = torch.cat((gt_labels_s, gt_labels_t), 0).to(self.device)
                else:
                    data = im_data_s.to(self.device)
                    target = gt_labels_s.to(self.device)
            else:
                data = im_data_t.to(self.device)
                target = gt_labels_t.to(self.device)

            ## freeze backbone networks completely. It will also freeze batch norm.
            if config['mme_freeze_backbone']:
                G.eval()
                with torch.no_grad():
                    output = G(data).detach()
            else:
                output = G(data)

            out1 = F1(output)
            loss = criterion(out1, target)
            loss_avg += loss.cpu().item()
            loss.backward()
            G.train()
            if not config['mme_freeze_backbone']:
                optimizer_g.step()
            optimizer_f.step()
            zero_grad_all()

            if WANDB_INFO.run_wandb:
                WANDB_INFO.log_metrics({'loss': loss})

            if not config["only_target_mme"] and not method == 'S+T' and not flag_unl_training and target_loader_unl is not None:
                im_data_tu, _, _ = next(data_iter_t_unl)
                output = G(im_data_tu.to(self.device))
                steps_here = max(step % config['log_interval'], 1)
                if method == 'ENT':
                    loss_t = entropy(F1, output, config['lambda'])
                    loss_t_avg += loss_t.cpu().item()
                    loss_t.backward()
                    optimizer_f.step()
                    optimizer_g.step()
                    prog.set_extra(
                        f'lr{lr:.02g} Loss CE:{loss_avg/steps_here:.04f} E:{loss_t_avg/steps_here:.04f}')
                elif method == 'MME':
                    # Apply MME only for target classes
                    out_t1 = F1(output, reverse=True, eta=1.0)[:, self.map_target2total]
                    out_t1 = F.softmax(out_t1)
                    loss_t = config['lambda'] * torch.mean(torch.sum(out_t1 *
                                                            (torch.log(out_t1 + 1e-5)), 1))
                    loss_t.backward()
                    loss_t_avg += loss_t.cpu().item()
                    optimizer_f.step()
                    optimizer_g.step()
                    prog.set_extra(
                        f'lr{lr:.02g} Loss CE:{loss_avg/steps_here:.04f} E:{loss_t_avg/steps_here:.04f}')
                else:
                    raise ValueError('Method cannot be recognized.')
            if (step % config['log_interval'] == 0 or step % config['save_interval'] == 0) and step > 0:
                loss_avg /= config['log_interval']
                loss_t_avg /= config['log_interval']

                if not method == 'S+T' and not config["only_target_mme"]:
                    log_train = (f'\nTrain Ep: {step} lr: {lr} \t '
                                 f'Loss Classification: {loss_avg:.6f} Loss T {-loss_t_avg:.6f} '
                                 f'Method {method}\n')
                else:
                    log_train = (f'\nTrain Ep: {step} lr: {lr} \t '
                                 f'Loss Classification: {loss_avg:.6f} Method {method}\n')
                log.debug(f"{log_train}")
                print(f"{log_train}")

            if step % config['save_interval'] == 0 and step > 0:
                if loss_avg < best_loss:
                    log.debug(f'\nNew best loss: {best_loss:04f}->{loss_avg:04f}')
                    best_loss = loss_avg
                    counter = 0
                else:
                    counter += 1
                    if counter >= config['patience']:
                        break

        logging.debug("Finished training MME")
        self.hyperparam_prev_inds += target_dataset.get_labeled_indices()

        del source_loader, target_loader, target_loader_unl
        self.G = G.cpu()
        self.F1 = F1.cpu()
        if self.config['model_save_path'] not in [None, "None", ""]:
            if not os.path.exists(self.config['model_save_path']):
                os.makedirs(self.config['model_save_path'])
            self.save_model(self.G, os.path.join(self.config['model_save_path'], "mme_G"))
            self.save_model(self.F1, os.path.join(self.config['model_save_path'], "mme_F1"))

            #torch.save(self.G.state_dict(),
            #       os.path.join(self.config['model_save_path'],f"mme_G_{}.pth"))
            #torch.save(self.F1.state_dict(),
            #       os.path.join(self.config['model_save_path'],f"mme_F1_{}.pth"))
        import gc
        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()

        from learn.algorithms.MME.utils.utils import CombinedFeatureExtractorClassifier
        return CombinedFeatureExtractorClassifier(G, F1, self.map_target2total)





    # method for doing hyperparameter search prior to domain_adapt_training
    def hyperparameter_search(self, target_dataset):
        """ This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
        the dataloaders below and feel free to override the transformers in the datasets to use whatever
        pretraining you desire. Please do not modify the datasets though.

        Args:
            target_dataset (ImageClassificationDataset): pytorch dataset for the target dataset used for
                training
            eval_dataset (ImageClassificationDataset): eval dataset used for evaluation.  Included here in
                case you want to do transductive learning.
        """

        config = self.config
        hyperparam_config = self.hyperparam_config

        ############################## start old eval hyperparameter code
        # self.before_hyperparam_G = {}
        # for name, param in self.G.named_parameters():
        #     self.before_hyperparam_G[name] = param.clone()

        # self.before_hyperparam_F = {}
        # for name, param in self.F1.named_parameters():
        #     self.before_hyperparam_F[name] = param.clone()


        # self.before_hyperparam_target_labels = target_dataset.get_labeled_indices()
        
        logging.info('Hyperparameter tuning for MME')
        # config = self.config
        # hyperparam_config = self.hyperparam_config

        # print('og target unlab num: ', len(target_dataset.get_unlabeled_indices()))
        # print('og target lab num: ', len(target_dataset.get_labeled_indices()))

        # original_labeled_indices = copy.deepcopy(target_dataset.get_labeled_indices())

        # # split the target set into train and test sets
        # val_ratio = hyperparam_config["val_ratio"]
        # lab_inds = np.array(target_dataset.get_labeled_indices())
        # lab_targets = np.array(target_dataset.targets)[lab_inds]

        # # # might be better to sample a fixed percentage of the images in each class
        # targs = np.array(target_dataset.targets)
        # class_, counts = np.unique(targs[targs!=None], return_counts=True)
        # min_count = min(counts)

        # print('min count of images in each class: ', min_count)
        # print('proposed val images per class: ', int(val_ratio * min_count))

        # # can't do hyperparameter tuning if val split would be zero
        # if int(val_ratio * min_count) < 1:
        #     return

        # # target_inds = []
        # train_inds = []
        # val_inds = []

        # for t in class_:
        #     locs = np.argwhere(target_dataset.targets == t)
        #     count = locs.shape[0]
        #     flat_locs = np.squeeze(locs)

        #     # shuffle   PUT THIS BACK
        #     flat_locs = flat_locs[torch.randperm(len(flat_locs))]



        #     # # count shouldn't be 1 for hyperparameter tuning
        #     # if count == 1:
        #     #     indices = [flat_locs.tolist()]
        #     # else:
        #     #     indices = flat_locs.tolist()
            
        #     indices = flat_locs.tolist()

        #     # vals = [ind for ind in indices if ind not in self.hyperparam_prev_inds]
        #     vals = np.setdiff1d(indices, self.hyperparam_prev_inds).tolist()
        #     trains = np.setdiff1d(indices, vals).tolist()

        #     num_val = int(len(indices) * val_ratio)

        #     if len(vals) <= num_val:
        #         append_num = num_val - len(vals) 
        #         vals += trains[:append_num]
        #         trains = trains[append_num:]
        #     else:
        #         trains = trains + vals[num_val:]
        #         vals = vals[:num_val]

        #         # hardcode which image is being chosen
        #         # vals_ = [vals[2]]
        #         # trains = trains + vals[:2] + [vals[3]]
        #         # vals = vals_



        #     # vals = indices[:num_val]
        #     # trains = indices[num_val:]

        #     train_inds.append(trains)
        #     val_inds.append(vals)
        
        # train_inds = [x for lst in train_inds for x in lst]
        # val_inds = [x for lst in val_inds for x in lst]

        # # idx_train = train_inds + target_dataset.get_unlabeled_indices()
        # # idx_train.sort()

        # print('intersection between train and test: ', np.intersect1d(train_inds, val_inds))
        # print('intersection between val inds and prev train inds: ', np.intersect1d(self.hyperparam_prev_inds, val_inds))



        # # val_num = int(max(len(target_dataset.categories), int(len(lab_inds) * val_ratio)))
        # # idx_train, idx_valid = train_test_split(lab_inds, test_size=val_num, stratify=lab_targets, random_state=2390)

        # # idx_train = idx_train.tolist() + target_dataset.get_unlabeled_indices()  # sort this?
        # # idx_train.sort()
        # # idx_train = np.array(idx_train)

        # # idx_train = idx_train.astype('int')
        # # idx_valid = idx_valid.astype('int')

        # # target_dataset = SubsetWrapperTargetTrain(target_dataset, idx_train)




        # eval_dataset = torch.utils.data.Subset(target_dataset, val_inds)
        # # target_dataset.labeled_indices = set(train_inds)  # maybe this will cause problems idk

        
        # print('new target unlab num: ', len(target_dataset.get_unlabeled_indices()))
        # print('new target lab num: ', len(target_dataset.get_labeled_indices()))
        # print('len val inds: ', len(val_inds))

        ######################## end old eval hyperparameter code


        # override transforms
        crop_size = 227
        if self.config['net'] != 'alexnet':
            crop_size = 224

        trans = transforms.Compose([
            transforms.Resize(256),
            transforms.RandomHorizontalFlip(),
            transforms.RandomCrop(crop_size),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])

        self.source_dataset.transform = trans
        target_dataset.transform = trans

        eval_trans = transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(crop_size),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])

        # ##################### fine-tune/train your model ######################
        #  Here is where you will actual finetune the model
        #  You can use the pretrained model with self.model
        #  Make sure to set any persistent tensors to cpu with `tensor.cpu` command



        # loop over hyperparameters such as lr

        lrs = hyperparam_config["lr"]
        # print('lrs: ', lrs)

        lr_dict = {}





        ########### start optimal LR finder

        # Get dataloaders for source and target
        source_loader, target_loader, target_loader_unl, class_list = self.get_dataloaders(
            self.source_dataset, target_dataset
        )
        self.target_category_ind = target_dataset._category_name_to_category_index(target_dataset.categories)
        method = config['method']

        # eval_dataset.transform = eval_trans
        # eval_dataloader = torch.utils.data.DataLoader(eval_dataset,
        #     batch_size=min(len(eval_dataset), int(self.config["batch_size"])),
        #     drop_last=False, num_workers=self.config["num_workers"]
        # )

        # If not warm start, use original source weights
        if not config['warm_start']:
            logging.info('Using Pre-training Parameters rather than using last budget level')
            G = copy.deepcopy(self.original_source_network)
            G.set_as_feature_extractor()
            G = copy.deepcopy(G).train().to(self.device)
            F1 = copy.deepcopy(G.classifier).train().to(self.device)
        else:
            logging.info('Using last budget level if available')
            G = copy.deepcopy(self.G).train().to(self.device)
            F1 = copy.deepcopy(self.F1).train().to(self.device)

        params = []
        for key, value in dict(G.named_parameters()).items():
            if value.requires_grad:
                if 'classifier' not in key:
                    params += [{'params': [value], 'lr': self.config['multi'],
                                'weight_decay': 0.0005}]
                else:
                    params += [{'params': [value], 'lr': self.config['multi'] * 10,
                                'weight_decay': 0.0005}]


        optimizer_g = optim.SGD(params, momentum=0.9,
                                weight_decay=0.0005, nesterov=True)
        optimizer_f = optim.SGD(list(F1.parameters()), lr=1.0, momentum=0.9,
                                weight_decay=0.0005, nesterov=True)

        def zero_grad_all():
            optimizer_g.zero_grad()
            optimizer_f.zero_grad()

        param_lr_g = []
        for param_group in optimizer_g.param_groups:
            param_lr_g.append(param_group["lr"])
        param_lr_f = []
        for param_group in optimizer_f.param_groups:
            param_lr_f.append(param_group["lr"])
        criterion = nn.CrossEntropyLoss()

        all_step = hyperparam_config["val_step"]

        if not config['only_target_mme']:
            data_iter_s = iter(source_loader)
        if target_loader is not None:
            data_iter_t = iter(target_loader)

        if target_loader_unl is not None:
            data_iter_t_unl = iter(target_loader_unl)
        len_train_source = len(source_loader)
        if target_loader is not None:
            len_train_target = len(target_loader)
        else:
            # So high it should never trigger
            len_train_target = 1e9999

        if target_loader_unl is not None:
            len_train_target_semi = len(target_loader_unl)
        else:
            # So high it should never trigger
            len_train_target_semi = 1e9999
        best_loss = 1e10
        counter = 0
        loss_avg = 0
        loss_t_avg = 0
        prog = ub.ProgIter(range(all_step), desc="Training MME")
        if target_loader_unl is not None and target_loader is not None:
            rate_labeled = float(max(len(target_loader_unl.dataset.get_unlabeled_indices()), 1)
                                / len(target_loader.dataset.get_labeled_indices()))
            flag_unl_training = rate_labeled < self.config["rate_threshold"]
        else:
            flag_unl_training = True

        if flag_unl_training:
            logging.info("disable MME training")
        else:
            logging.info("unlabeled / labeled %s" % (rate_labeled))



        # low_lr = 0.00001
        # high_lr = 0.9
        # num_iter = 200 # number of batches

        low_lr = hyperparam_config["low_lr"]
        high_lr = hyperparam_config["high_lr"]
        num_iter = hyperparam_config["num_iterations"]

        losses = []
        lrs = []

        # print(f'tuning LR from {low_lr} to {high_lr} for {num_iter} iterations')
        logging.info(f'tuning LR from {low_lr} to {high_lr} for {num_iter} iterations')

        for step in range(num_iter):

            if step % 10 == 0:
                print('step ', step)

            # calculate learning rate
            if step > 0:
                learning_rate = low_lr * ((high_lr / low_lr) ** (step / num_iter))
            else:
                learning_rate = low_lr

            lrs.append(learning_rate)

            # print('step: ', step, ' lr: ', learning_rate)
            
                
            optimizer_g = inv_lr_scheduler(param_lr_g, optimizer_g, step,
                                        init_lr=learning_rate)
            optimizer_f = inv_lr_scheduler(param_lr_f, optimizer_f, step,
                                        init_lr=learning_rate)


            # print('lrs: ', optimizer_f.param_groups[0]['lr'], optimizer_g.param_groups[0]['lr'])

            lr = optimizer_f.param_groups[0]['lr']

            # print('lr: ', lr)

            if step % len_train_target == 0 and target_loader is not None:
                data_iter_t = iter(target_loader)
            if step % len_train_target_semi == 0 and target_loader_unl is not None:
                data_iter_t_unl = iter(target_loader_unl)
            if not config['only_target_mme'] and step % len_train_source == 0:
                data_iter_s = iter(source_loader)

            zero_grad_all()
            if target_loader is not None:    
                im_data_t, gt_labels_t, _ = next(data_iter_t)
                # need to map the labels considering class overlap
                gt_labels_t = self.mapping_to_total(gt_labels_t, self.map_target2total)
            if not config['only_target_mme']:
                im_data_s, gt_labels_s, _ = next(data_iter_s)
                gt_labels_s = self.mapping_to_total(gt_labels_s, self.map_source2total)
                if target_loader is not None: # If not UDA (no target labeled)
                    data = torch.cat((im_data_s, im_data_t), 0).to(self.device)
                    target = torch.cat((gt_labels_s, gt_labels_t), 0).to(self.device)
                else:
                    data = im_data_s.to(self.device)
                    target = gt_labels_s.to(self.device)
            else:
                data = im_data_t.to(self.device)
                target = gt_labels_t.to(self.device)

            ## freeze backbone networks completely. It will also freeze batch norm.
            if config['mme_freeze_backbone']:
                G.eval()
                with torch.no_grad():
                    output = G(data).detach()
            else:
                output = G(data)

            out1 = F1(output)
            loss = criterion(out1, target)
            loss_avg += loss.cpu().item()
            loss.backward()
            G.train()
            if not config['mme_freeze_backbone']:
                optimizer_g.step()
            optimizer_f.step()
            zero_grad_all()

            losses.append(loss.detach().cpu().item())


            if WANDB_INFO.run_wandb:
                wandb.log({
                    "hyperparam/lr_loss" : loss, "hyperparam/lr": learning_rate})

            if not config["only_target_mme"] and not method == 'S+T' and not flag_unl_training and target_loader_unl is not None:
                im_data_tu, _, _ = next(data_iter_t_unl)
                output = G(im_data_tu.to(self.device))
                steps_here = max(step % config['log_interval'], 1)
                if method == 'ENT':
                    loss_t = entropy(F1, output, config['lambda'])
                    loss_t_avg += loss_t.cpu().item()
                    loss_t.backward()
                    optimizer_f.step()
                    optimizer_g.step()
                    prog.set_extra(
                        f'lr{lr:.02g} Loss CE:{loss_avg/steps_here:.04f} E:{loss_t_avg/steps_here:.04f}')
                elif method == 'MME':
                    # Apply MME only for target classes
                    out_t1 = F1(output, reverse=True, eta=1.0)[:, self.map_target2total]
                    out_t1 = F.softmax(out_t1)
                    loss_t = config['lambda'] * torch.mean(torch.sum(out_t1 *
                                                            (torch.log(out_t1 + 1e-5)), 1))
                    loss_t.backward()
                    loss_t_avg += loss_t.cpu().item()
                    optimizer_f.step()
                    optimizer_g.step()
                    prog.set_extra(
                        f'lr{lr:.02g} Loss CE:{loss_avg/steps_here:.04f} E:{loss_t_avg/steps_here:.04f}')
                else:
                    raise ValueError('Method cannot be recognized.')

       

        # best LR is one with steepest negative gradient

        lrs = np.array(lrs)
        losses = np.array(losses)

        losses = losses[np.isfinite(losses)]
        lrs = lrs[np.isfinite(losses)]

        # smooth out the losses
        smoothed_losses = []
        numAccum = 0
        for itr, loss_ in enumerate(losses):
            if itr == 0:
                smoothed_losses.append(loss_)
                last = loss_
                continue

            last = losses[itr-1] * 0.13 + (1 - 0.13) * last
            # numAccum += 1
            # debiasWeight = 1.0 - ((1 - 0.13) ** numAccum)
            # smoothed_losses.append(last / debiasWeight)
            smoothed_losses.append(last)


        for loss_ in smoothed_losses:
            if WANDB_INFO.run_wandb:
                wandb.log({
                    "hyperparam/smoothed_lr_loss" : loss_})

        min_grad = np.gradient(smoothed_losses).argmin()
        best_lr = lrs[min_grad]
        print('best LR with smoothing: ', best_lr)
        self.config["lr"] = best_lr


        min_grad_no_smooth = np.gradient(losses).argmin()
        best_lr_no_smooth = lrs[min_grad_no_smooth]
        print('best LR without smoothing: ', best_lr_no_smooth)

        ##################### end optimal LR finder









        ############################ old loop over set number of LRs
        # for learning_rate in lrs:

        #     # Get dataloaders for source and target
        #     source_loader, target_loader, target_loader_unl, class_list = self.get_dataloaders(
        #         self.source_dataset, target_dataset
        #     )
        #     self.target_category_ind = target_dataset._category_name_to_category_index(target_dataset.categories)
        #     method = config['method']

        #     eval_dataset.transform = eval_trans
        #     eval_dataloader = torch.utils.data.DataLoader(eval_dataset,
        #         batch_size=min(len(eval_dataset), int(self.config["batch_size"])),
        #         drop_last=False, num_workers=self.config["num_workers"]
        #     )

        #     # If not warm start, use original source weights
        #     if not config['warm_start']:
        #         logging.info('Using Pre-training Parameters rather than using last budget level')
        #         G = copy.deepcopy(self.original_source_network)
        #         G.set_as_feature_extractor()
        #         G = copy.deepcopy(G).train().to(self.device)
        #         F1 = copy.deepcopy(G.classifier).train().to(self.device)
        #     else:
        #         logging.info('Using last budget level if available')
        #         G = copy.deepcopy(self.G).train().to(self.device)
        #         F1 = copy.deepcopy(self.F1).train().to(self.device)

        #     params = []
        #     for key, value in dict(G.named_parameters()).items():
        #         if value.requires_grad:
        #             if 'classifier' not in key:
        #                 params += [{'params': [value], 'lr': self.config['multi'],
        #                             'weight_decay': 0.0005}]
        #             else:
        #                 params += [{'params': [value], 'lr': self.config['multi'] * 10,
        #                             'weight_decay': 0.0005}]


        #     optimizer_g = optim.SGD(params, momentum=0.9,
        #                             weight_decay=0.0005, nesterov=True)
        #     optimizer_f = optim.SGD(list(F1.parameters()), lr=1.0, momentum=0.9,
        #                             weight_decay=0.0005, nesterov=True)

        #     def zero_grad_all():
        #         optimizer_g.zero_grad()
        #         optimizer_f.zero_grad()

        #     param_lr_g = []
        #     for param_group in optimizer_g.param_groups:
        #         param_lr_g.append(param_group["lr"])
        #     param_lr_f = []
        #     for param_group in optimizer_f.param_groups:
        #         param_lr_f.append(param_group["lr"])
        #     criterion = nn.CrossEntropyLoss()

        #     all_step = hyperparam_config["val_step"]

        #     if not config['only_target_mme']:
        #         data_iter_s = iter(source_loader)
        #     data_iter_t = iter(target_loader)

        #     if target_loader_unl is not None:
        #         data_iter_t_unl = iter(target_loader_unl)
        #     len_train_source = len(source_loader)
        #     len_train_target = len(target_loader)

        #     if target_loader_unl is not None:
        #         len_train_target_semi = len(target_loader_unl)
        #     else:
        #         # So high it should never trigger
        #         len_train_target_semi = 1e9999
        #     best_loss = 1e10
        #     counter = 0
        #     loss_avg = 0
        #     loss_t_avg = 0
        #     prog = ub.ProgIter(range(all_step), desc="Training MME")
        #     if target_loader_unl is not None:
        #         rate_labeled = float(max(len(target_loader_unl.dataset.get_unlabeled_indices()), 1)
        #                             / len(target_loader.dataset.get_labeled_indices()))
        #         flag_unl_training = rate_labeled < self.config["rate_threshold"]
        #     else:
        #         flag_unl_training = True

        #     if flag_unl_training:
        #         logging.info("disable MME training")
        #     else:
        #         logging.info("unlabeled / labeled %s" % (rate_labeled))
            
        #     for step in prog:
                
        #         optimizer_g = inv_lr_scheduler(param_lr_g, optimizer_g, step,
        #                                     init_lr=learning_rate)
        #         optimizer_f = inv_lr_scheduler(param_lr_f, optimizer_f, step,
        #                                     init_lr=learning_rate)


        #         # print('lrs: ', optimizer_f.param_groups[0]['lr'], optimizer_g.param_groups[0]['lr'])

        #         lr = optimizer_f.param_groups[0]['lr']

        #         # print('lr: ', lr)

        #         if step % len_train_target == 0:
        #             data_iter_t = iter(target_loader)
        #         if step % len_train_target_semi == 0 and target_loader_unl is not None:
        #             data_iter_t_unl = iter(target_loader_unl)
        #         if not config['only_target_mme'] and step % len_train_source == 0:
        #             data_iter_s = iter(source_loader)

        #         im_data_t, gt_labels_t, _ = next(data_iter_t)
        #         zero_grad_all()
        #         # need to map the labels considering class overlap
        #         gt_labels_t = self.mapping_to_total(gt_labels_t, self.map_target2total)
        #         if not config['only_target_mme']:
        #             im_data_s, gt_labels_s, _ = next(data_iter_s)
        #             gt_labels_s = self.mapping_to_total(gt_labels_s, self.map_source2total)
        #             data = torch.cat((im_data_s, im_data_t), 0).to(self.device)
        #             target = torch.cat((gt_labels_s, gt_labels_t), 0).to(self.device)
        #         else:
        #             data = im_data_t.to(self.device)
        #             target = gt_labels_t.to(self.device)

        #         ## freeze backbone networks completely. It will also freeze batch norm.
        #         if config['mme_freeze_backbone']:
        #             G.eval()
        #             with torch.no_grad():
        #                 output = G(data).detach()
        #         else:
        #             output = G(data)

        #         out1 = F1(output)
        #         loss = criterion(out1, target)
        #         loss_avg += loss.cpu().item()
        #         loss.backward()
        #         G.train()
        #         if not config['mme_freeze_backbone']:
        #             optimizer_g.step()
        #         optimizer_f.step()
        #         zero_grad_all()


        #         if WANDB_INFO.run_wandb:
        #             wandb.log({
        #                 f"hyperparam/lr_{learning_rate}_loss" : loss})

        #         if not config["only_target_mme"] and not method == 'S+T' and not flag_unl_training and target_loader_unl is not None:
        #             im_data_tu, _, _ = next(data_iter_t_unl)
        #             output = G(im_data_tu.to(self.device))
        #             steps_here = max(step % config['log_interval'], 1)
        #             if method == 'ENT':
        #                 loss_t = entropy(F1, output, config['lambda'])
        #                 loss_t_avg += loss_t.cpu().item()
        #                 loss_t.backward()
        #                 optimizer_f.step()
        #                 optimizer_g.step()
        #                 prog.set_extra(
        #                     f'lr{lr:.02g} Loss CE:{loss_avg/steps_here:.04f} E:{loss_t_avg/steps_here:.04f}')
        #             elif method == 'MME':
        #                 # Apply MME only for target classes
        #                 out_t1 = F1(output, reverse=True, eta=1.0)[:, self.map_target2total]
        #                 out_t1 = F.softmax(out_t1)
        #                 loss_t = config['lambda'] * torch.mean(torch.sum(out_t1 *
        #                                                         (torch.log(out_t1 + 1e-5)), 1))
        #                 loss_t.backward()
        #                 loss_t_avg += loss_t.cpu().item()
        #                 optimizer_f.step()
        #                 optimizer_g.step()
        #                 prog.set_extra(
        #                     f'lr{lr:.02g} Loss CE:{loss_avg/steps_here:.04f} E:{loss_t_avg/steps_here:.04f}')
        #             else:
        #                 raise ValueError('Method cannot be recognized.')

        #     # test results on eval_dataset (held-out portion of target dataset)
        #     preds = []
        #     labels = []

        #     with torch.no_grad():
        #         for imgs, labs, _ in eval_dataloader:

        #             for ind in _:
        #                 assert ind not in target_dataset.get_labeled_indices()

        #             imgs = imgs.to(self.device)
        #             output = G(imgs)
        #             preds_ = F1(output)[:, self.map_target2total]
        #             preds.append(torch.argmax(preds_, dim=1))
        #             labels.append(labs)

        #     preds = torch.cat(preds, dim=0).cpu()
        #     labels = torch.cat(labels, dim=0)


        #     top1_num_correct = float(preds.eq(labels).sum())
        #     top1_acc = top1_num_correct / len(preds)


        #     if WANDB_INFO.run_wandb:
        #         wandb.log({
        #             f"hyperparam/lr_{learning_rate}_num_correct" : top1_num_correct,
        #             f"hyperparam/lr_{learning_rate}_top1_acc" : top1_acc
        #         })
        #     print('number correct: ', top1_num_correct, '/', len(preds))
        #     print('top1_acc for lr ', learning_rate, ': ', top1_acc)

        #     lr_dict[learning_rate] = top1_acc

        #     del source_loader, target_loader, target_loader_unl, eval_dataloader


        # best_lr = max(lr_dict, key=lambda key: lr_dict[key])

        # # do we want to do this? or create a new instance variable?
        # self.config["lr"] = best_lr

        # print(lr_dict)
        # print('best lr: ', best_lr)
        # # exit()


        # # put the original labeled indices back
        # target_dataset.labeled_indices = set(original_labeled_indices)

        ############################ old loop over set number of LRs










        # self.G = G.cpu()
        # self.F1 = F1.cpu()

        import gc
        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()

    def save_model(self,model, path):
        # hacky way to deal with checkpoints
        i = 0
        while True:
            model_path = path + f"{i}.pth"
            if os.path.exists(model_path):
                i += 1
            else:
                torch.save(model.state_dict(), model_path)
                break

    def only_train_bn(self, dset, eps=1e-5, momentum=0.1, reset_stats=False):
        logging.info("Training Batch Norm")
        G = self.G.eval().to(self.device)
        F1 = self.F1.eval().to(self.device)
        
        dataloader = data.DataLoader(dset,
            batch_size=int(self.config["batch_size"]),
            shuffle=True,
            drop_last=False  ##### remove num_workers here
        )

        for n,p in G.named_modules():
            if not isinstance(p, torch.nn.modules.batchnorm._BatchNorm):
                p.requires_grad_(False)
            else:
                p.requires_grad_(True)
                p.train()
                # configure epsilon for stability, and momentum for updates
                p.eps = eps
                p.momentum = momentum
                if reset_stats:
                    p.reset_running_stats()

        for n,p in F1.named_modules():
            if not isinstance(p, torch.nn.modules.batchnorm._BatchNorm):
                p.requires_grad_(False)
            else:
                p.requires_grad_(True)
                p.train()
                # configure epsilon for stability, and momentum for updates
                p.eps = eps
                p.momentum = momentum

                if reset_stats:
                    p.reset_running_stats()

        with torch.no_grad():
            for imgs, _, inds in dataloader:
                F1(G(imgs.to(self.device)))
        self.G = G.cpu()
        self.F1 = F1.cpu()

    def inference(self, eval_dataset):
        """
        Inference is during the evaluation stage.  For this example, the task_network is trained in the
        train and adapt stage's code and is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to JPL

        Args:
            eval_dataset (framework.datasets.ImageClassificationDataset): unlabeled evaluation framework.
                We need to predict the classes for each item in the dataset.

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        config = self.config

        # override transforms
        crop_size = 227
        if config['net'] != 'alexnet':
            crop_size = 224

        trans = transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(crop_size),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])

        eval_dataset.transform = trans

        eval_dataloader = data.DataLoader(eval_dataset,
            batch_size=int(self.config["batch_size"]),
            drop_last=False  ##### remove num_workers here
        )
        if config['train_bn']:
            self.only_train_bn(eval_dataset, 
                    reset_stats=config['bn_reset_stats'])

        G = self.G.eval().to(self.device)
        F1 = self.F1.eval().to(self.device)
        softmax = nn.Softmax(dim=1).to(self.device)
        
        preds = []
        indices = []
        with torch.no_grad():
            for imgs, _, inds in eval_dataloader:
                imgs = imgs.to(self.device)
                output = G(imgs)
                preds_ = F1(output)[:, self.map_target2total]
                if config['probabilistic_predictions']:
                    preds += softmax(preds_).cpu().numpy().tolist()
                else:
                    preds += torch.argmax(preds_, dim=1).cpu().numpy().tolist()

                indices += inds.numpy().tolist()

        self.G = G.cpu()
        self.F1 = F1.cpu()

        del eval_dataloader
        import gc
        gc.collect()  # Make sure all object have ben deallocated if not used
        torch.cuda.empty_cache()


        if config['probabilistic_predictions']:
            predictions = pd.DataFrame(preds, columns=np.arange(len(preds[0])))
            predictions['id'] = indices
            return predictions
        else:
            return preds, indices
