import os
import torch
import torch.nn as nn
import shutil

class CombinedFeatureExtractorClassifier(nn.Module):
    """
    Given a feature extraction model and a classifier of features, 
    construct a classifier model by combining the feature extraction model and classifier

    :param extractor: A feature extraction model
    :param classifier: function mapping numpy vectors to an iterable of probabilities 
        signifying how likely they are to be in the target (positive) class.
    """

    def __init__(self, extractor, classifier, map_target2total=None):
        super(CombinedFeatureExtractorClassifier, self).__init__()
        self.extractor = extractor
        self.classifier = classifier
        self.map_target2total = map_target2total

    def forward(self, x):
        x = self.extractor(x)
        # note - this needs to be run with torch.no_grad,
        # otherwise this won't detach
        #x = torch.flatten(x, 1)
        x = self.classifier(x)
        if self.map_target2total:
            x = x[:, self.map_target2total]
        return x


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.1)
    elif classname.find('Linear') != -1:
        nn.init.xavier_normal_(m.weight)
        nn.init.zeros_(m.bias)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.1)
        m.bias.data.fill_(0)


def save_checkpoint(state, is_best, checkpoint='checkpoint',
                    filename='checkpoint.pth.tar'):
    filepath = os.path.join(checkpoint, filename)
    torch.save(state, filepath)
    if is_best:
        shutil.copyfile(filepath, os.path.join(checkpoint,
                                               'model_best.pth.tar'))
