import sys
import torch
import torch.utils.data as data
from learn.algorithms.CloserLookFewShot.imageClassificationAdapter import ImageClassifierAdapter
import learn.algorithms.CloserLookFewShot.model as model
import learn.algorithms.CloserLookFewShot.solver as solver
from learn.algorithms.CloserLookFewShot.config import Config
from learn.utils.memory import garbage_collection_cuda
import ubelt as ub


class ImageClassifierAlgorithm(ImageClassifierAdapter):

    def __init__(self, toolset):
        # def __init__(self, problem, base_dataset, adapt_dataset, arguments):
        ImageClassifierAdapter.__init__(self, toolset)

        # Load the default configuration from config.py for your algorithm
        self.config = Config()
        self.update_parameters(toolset['protocol_config'])

        # This sends the configuration back to the toolset
        self.toolset['classification_config'] = self.config

        device = self.config['device']
        if ub.iterable(device):
            self.device = device[0]
        else:
            self.device = device

    def update_parameters(self, protocol_config):
        """  Update parameters for algorithm based on protocol config

        Args:
            protocol_config (dict):  Dictionary of parameters.

        Returns:
            None
        """
        common_keyword = set(protocol_config.keys()) & set(self.config.keys())
        for ck in common_keyword:
            self.config[ck] = protocol_config[ck]

    def initialize(self):
        # ############## initialize the algorithm ##############
        # set the CloserLookFewShot configuration options

        # Load the config for this algorithm
        self.config = Config()


        # model = solver.get_model(self.arguments["backbone"])

        pretrained_model = self.toolset["source_network"]
        # TODO: Generalize the feature extraction network input
        num_classes = len(self.toolset["target_dataset"].categories)
        self.task_model = model.BaselineTrain(pretrained_model,
                                              num_classes,
                                              self.device,
                                              loss_type='dist')

        # This sends the configuration back to the toolset
        self.toolset['classification_config'] = self.config
        self.task_model.cpu()
        garbage_collection_cuda()

    def domain_adapt_training(self):
        # ###################  Creating the Labeled DataLoader ###############
        # Create Dataloaders for labeled data by getting the labeled indices and
        # creating a random sub-sampler.  Then create the dataloader using this
        # sampler
        #
        #  Note: To change the transformer in the JPLDataset dataset, edit
        #      current_dataset.transform.  You can also edit the labels during
        #      loading by editing the  current_dataset.target_transform
        #
        #  Note: Another approach could be to inherit the JPLDataset class and
        #      override the __get_item__ function
        #
        #  Check out dataset.py for more information
        #     Notes: There could be NO unlabeled data and the algorithm has to be
        #         able to handle that case!
        #
        #         The min batch size here is so that the batch size isn't
        #         larger than the labeled/unlabeled dataset which causes the
        #         dataloader to hang

        labeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
            self.toolset["target_dataset"].get_labeled_indices()
        )

        labeled_dataloader = torch.utils.data.DataLoader(
            self.toolset["target_dataset"],
            sampler=labeled_sampler,
            batch_size=min(self.toolset["target_dataset"].labeled_size,
                           int(self.config["batch_size"])
                           ),
            num_workers=int(self.config['num_workers']),
            collate_fn=self.toolset["target_dataset"].collate_batch,
            drop_last=True,
        )

        # ###################  Creating the Unlabeled DataLoader ###############
        #  Same as labeled dataaset but for unlabeled indices
        unlabeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
            self.toolset["target_dataset"].get_unlabeled_indices()
        )
        unlabeled_dataloader = torch.utils.data.DataLoader(
            self.toolset["target_dataset"],
            sampler=unlabeled_sampler,
            batch_size=min(self.toolset["target_dataset"].unlabeled_size,
                           int(self.config["batch_size"])
                           ),
            num_workers=int(self.config['num_workers']),
            drop_last=False,
        )
        self.task_model.to(self.device)
        acc, self.task_model = solver.train(self.config,
                                            labeled_dataloader,
                                            self.task_model)
        self.task_model.cpu()
        garbage_collection_cuda()

    def inference(self):
        """
        Inference is during the evaluation stage.  For this example, the
        task_network is trained in the train and adapt stage's code and
        is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to
        JPL

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        self.task_model.eval().to(self.device)
        eval_dataloader = data.DataLoader(
            self.toolset["eval_dataset"],
            batch_size=int(self.config["batch_size"]),
            drop_last=False
        )

        preds = []
        indices = []

        for imgs, _, inds in eval_dataloader:
            imgs = imgs.to(self.device)

            with torch.no_grad():
                preds_ = self.task_model(imgs)

            preds += torch.argmax(preds_, dim=1).cpu().numpy().tolist()
            indices += inds.numpy().tolist()

        # preds, indices = self.toolset["eval_dataset"].dummy_data(
        #                                                'image_classification')
        self.task_model.cpu()
        garbage_collection_cuda()
        return preds, indices



