import scriptconfig as scfg

class Config(scfg.Config):
    """
    Default configuration for baseline++
    """
    default = {
        'name': scfg.Value('baseline++',
                           help='name of the proposed method'),

        'batch_size': scfg.Value(128,
                                 help='batch size of network while running.  '
                                      'Increasing will do X and decrease will do Y'),

        'num_workers': scfg.Value(8,
                                 help='number of workers in the dataloader'),

        'start_epoch': scfg.Value(0,
                                 help='epoch to start counting from during training'),

        'num_classes': scfg.Value(0,
                                 help='epoch to start counting from during training'),

        'end_epoch': scfg.Value(25,
                                help='epoch to stop at during training'),

        'device': scfg.Value([0],
                             help='Device to use during training'),

        'checkpoint_dir': scfg.Path("./checkpoints",
                                    help='Example on how to setup a path.  '
                                         'You dont need to save checkpoints'),

    }