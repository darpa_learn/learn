import os
import copy
from PIL import Image
import torch
from RandAugment import RandAugment

def inv_lr_scheduler(param_lr, optimizer, iter_num, gamma=0.0001, power=0.75):
	"""
	Decay learning rate
	"""	
	for i, param_group in enumerate(optimizer.param_groups):
		lr = param_lr[i] * (1 + gamma * iter_num) ** (- power)
		param_group['lr'] = lr
	return optimizer

def get_pseudolabels(model, loader, device, num_classes):
	model.eval()
	embedding = torch.zeros([len(loader.dataset), num_classes])
	preds = torch.zeros(len(loader.dataset)).long()
	with torch.no_grad():
		for batch_idx, ((data, _, _), target, indices) in enumerate(loader):
			data = data.to(device)
			e1 = model(data)
			embedding[indices, :] = e1.cpu()
			preds[indices] = e1.argmax(dim=1, keepdim=True).squeeze().cpu()
	
	nz_idxs = torch.arange(len(loader.dataset))[embedding.sum(dim=1) != 0]
	preds = preds[nz_idxs]
	
	return preds

def default_loader(path):
	# open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
	with open(path, 'rb') as f:
		with Image.open(f) as img:
			return img.convert('RGB')

class DatasetWithIndicesWrapper(torch.utils.data.Dataset):

	def __init__(self, data, targets, root_dir, transforms, base_transforms):
		self.data = data
		self.targets = targets
		self.transforms = transforms
		self.root_dir = root_dir
		self.base_transforms = base_transforms
		self.rand_aug_transforms = copy.deepcopy(self.base_transforms)
		self.committee_size = 1
		self.ra_obj = RandAugment(1, 2.0)
		self.rand_aug_transforms.transforms.insert(0, self.ra_obj)

	def __len__(self):
		return len(self.targets)

	def __getitem__(self, index):

		data, target = self.data[index], self.targets[index]
		if target is None: target = -1
		data = default_loader(os.path.join(self.root_dir, self.data[index]))

		rand_aug_lst = [self.rand_aug_transforms(data) for _ in range(self.committee_size)]			
		return (self.transforms(data), self.base_transforms(data), rand_aug_lst), int(target), int(index)