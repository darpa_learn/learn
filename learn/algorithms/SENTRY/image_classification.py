import copy
import numpy as np
from collections import Counter
from sklearn.metrics import confusion_matrix

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import ubelt as ub
import random

from torchvision import transforms
from learn.algorithms.SENTRY.imageClassificationAdapter import ImageClassifierAdapter
import logging
import utils
from learn.algorithms.SENTRY.utils import DatasetWithIndicesWrapper
from learn.algorithms.SENTRY.model import ResNet50FS
import os
from learn.utils.wandb_utils import WANDB_INFO

class ImageClassifierAlgorithm(ImageClassifierAdapter):
        """
        This implementation implements unsupervised domain adaptation via SENTRY (https://arxiv.org/abs/2012.11460).
        """

        def __init__(self, toolset):
                # def __init__(self, problem, base_dataset, adapt_dataset, arguments):
                ImageClassifierAdapter.__init__(self, toolset)
                self.config = toolset["protocol_config"]["image_classifier"]["params"]

                device = self.config["device"]
                if ub.iterable(device):
                        self.device = device[0]
                else:
                        self.device = device

        def initialize(
                self,
                source_network,
                source_dataset,
                whitelist_datasets,
                target_dataset,
                networks,
        ):
                """  Initialize your algorithm and save any parameters or objects you want to be persistent between
                budget checkpoints.  DomainNetworkSelection will choose a potential source datasets `source_dataset`
                and provide a trained network for that dataset `source_dataset.` Please do not modify the datasets
                beyond overriding the transforms (self.transform and self.transform_target are fine to change).

                Args:
                        source_network (pytorch.nn.Module):  Pretrained network on source task
                        source_dataset (framework.dataset): Pytorch dataset for source task
                        whitelist_datasets (dict[framework.dataset]): Dictionary of all possible external datasets which
                                can be used on this task.  You may use this if you want to select a different dataset than
                                the one selected as source.
                        target_dataset: (ImageClassificationDataset): Pytorch dataset for target domain/task
                        networks: (Dict[str, Dict[str, nn.module]]): Two leveled dict with the dataset as the first key
                                and the network structure name as the second key.  e.g. networks['imagenet_1k']['densenet169']
                """
                config = self.config
                self.source_model = source_network
                self.source_model.set_as_classifier()
                self.source_dataset = source_dataset

                self.target_categories = target_dataset.categories.tolist()
                self.target_categories = list(map(lambda l: str(l).lower().replace('_', ' ').replace('-', ' '),
                                                                 self.target_categories))
                self.source_categories = source_dataset.categories.tolist()
                self.source_categories = list(map(lambda l: str(l).lower().replace('_', ' ').replace('-', ' '),
                                                                 self.source_categories))

                source_cats = set(self.source_categories)
                target_cats = set(self.target_categories)        

                self.perfect_label_overlap = True if (source_cats == target_cats) else False

                logging.info(f"{whitelist_datasets.keys()}")

                random.seed(config["seed"])
                torch.manual_seed(config["seed"])
                np.random.seed(config["seed"])
                torch.cuda.manual_seed(config["seed"])

                self.num_classes = len(target_dataset.categories)
                self.model = self.source_model

        def get_weights_for_sampler(self, dataset):
                """ get weight the dataset to give to pytorch's weightedrandomsampler.
                This is instead of subset selector so that we can weight each element.

                Args:
                        dataset (framework.dataset.ImageDataset): dataset which you are weighting            

                Returns:
                        list[float]: weights for random sampler
                """

                targets = copy.deepcopy(dataset.targets)
                if isinstance(targets, torch.Tensor): targets = targets.numpy()
                count_dict = Counter(targets)

                count_dict_full = {lbl: 0 for lbl in range(self.num_classes)}
                for k, v in count_dict.items():
                        count_dict_full[k] = v
                count_dict_sorted = {
                        k: v for k, v in sorted(count_dict_full.items(), key=lambda item: item[0])
                }
                class_sample_count = np.array(list(count_dict_sorted.values()))
                class_sample_count = class_sample_count / class_sample_count.max()
                class_sample_count += 1e-8

                weights = 1 / torch.Tensor(class_sample_count)
                sample_weights = [weights[l] for l in targets]
                sample_weights = torch.DoubleTensor(np.array(sample_weights))

                # Set weights
                return sample_weights

        def get_dataloader(self, dataset, class_balance=False):
                """ Get dataloaders for given dataset

                Args:
                        dataset (framework.datasets.ImageClassificationDataset): dataset (source or target)

                Returns:
                        dataloaders for given dataset

                """
                if class_balance:
                        weights = self.get_weights_for_sampler(dataset)
                else:
                        weights = torch.DoubleTensor(np.ones(len(dataset)))

                batch_size = int(self.config["batch_size"])#min(dataset.labeled_size, int(self.config["batch_size"]))
                num_iter = self.config['num_iter'] * batch_size
                dataloader = torch.utils.data.DataLoader(
                        dataset,
                        sampler=torch.utils.data.sampler.WeightedRandomSampler(weights, num_iter),
                        batch_size=batch_size,
                        num_workers=int(self.config["num_workers"]),
                        collate_fn=dataset.collate_batch,
                        drop_last=False,
                )
                return dataloader

        def lr_step(self):
                """
                Learning rate scheduler to decay learning rate
                """
                self.tgt_opt = utils.inv_lr_scheduler(
                        self.param_lr_c,
                        self.tgt_opt,
                        self.current_step,
                )

        def domain_adapt_training(self, target_dataset, eval_dataset):
                """ This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
                the dataloaders below and feel free to override the transformers in the datasets to use whatever
                pretraining you desire. Please do not modify the datasets though.

                Args:
                        target_dataset (object): pytorch dataset for the target dataset used for training
                        eval_dataset (object): eval dataset used for evaluation.  Included here in case you want to do
                                transductive learning.
                """
                config = self.config

                if self.config['sentry_auto_off'] and not self.perfect_label_overlap:
                        logging.info('Disabling SENTRY since label overlap is not 100%')
                        return

                # ##################### fine-tune/train your model ######################
                #  Here is where you will actual finetune the model
                full_transforms = transforms.Compose(
                        [
                                transforms.Resize((256, 256)),
                                transforms.RandomCrop((224, 224)),
                                transforms.RandomHorizontalFlip(),
                                transforms.ToTensor(),
                                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
                        ]
                )
                base_transforms = transforms.Compose(
                        [
                                transforms.Resize((224, 224)),
                                transforms.ToTensor(),
                                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
                        ]
                )

                source_dataset_wrapped = DatasetWithIndicesWrapper(
                        self.source_dataset.image_fnames,
                        self.source_dataset.targets,
                        self.source_dataset.root,
                        full_transforms,
                        base_transforms,
                )
                target_dataset_wrapped = DatasetWithIndicesWrapper(
                        target_dataset.image_fnames,
                        target_dataset.targets,
                        target_dataset.root,
                        full_transforms,
                        base_transforms,
                )
                source_dataset_wrapped.labeled_size = self.source_dataset.labeled_size
                source_dataset_wrapped.collate_batch = self.source_dataset.collate_batch
                target_dataset_wrapped.labeled_size = target_dataset.labeled_size
                target_dataset_wrapped.collate_batch = target_dataset.collate_batch

                source_loader = self.get_dataloader(source_dataset_wrapped, class_balance=True)
                target_loader = self.get_dataloader(target_dataset_wrapped)

                lambda_src, lambda_infoent, lambda_ent = (
                        config["lambda_src"],
                        config["lambda_infoent"],
                        config["lambda_ent"],
                )
                positive_threshold, negative_threshold = (
                        (config["committee_size"] // 2) + 1,
                        (config["committee_size"] // 2) + 1,
                )  # Use majority voting scheme
                # Pass in hyperparams to dataset
                target_loader.dataset.committee_size = config["committee_size"]
                target_loader.dataset.ra_obj.n = config["randaug_n"]
                target_loader.dataset.ra_obj.m = config["randaug_m"]

                param_list = []
                for key, value in dict(self.model.named_parameters()).items():
                        if value.requires_grad:
                                if "classifier" not in key:
                                        param_list += [
                                                {
                                                        "params": [value],
                                                        "lr": config["da_lr"] * 0.1,
                                                        "weight_decay": config["da_wd"],
                                                }
                                        ]
                                else:
                                        param_list += [
                                                {
                                                        "params": [value],
                                                        "lr": config["da_lr"],
                                                        "weight_decay": config["da_wd"],
                                                }
                                        ]

                self.tgt_opt = optim.SGD(
                        param_list, momentum=0.9, weight_decay=config["da_wd"], nesterov=True
                )

                self.current_step = 0
                self.param_lr_c = []
                for param_group in self.tgt_opt.param_groups:
                        self.param_lr_c.append(param_group["lr"])

                self.model.train()
                self.model.to(self.device)

                queue = torch.zeros(config["queue_length"]).to(self.device)
                pointer = 0
                joint_loader = zip(source_loader, target_loader)
                joint_len = min(len(source_loader), len(target_loader))

                for epoch in range(config["da_num_epochs"]):
                        for (
                                batch_idx,
                                (
                                        ((data_s, _, _), label_s, _),
                                        ((_, data_t_og, data_t_raug), _, indices_t),
                                ),
                        ) in enumerate(joint_loader):
                                self.current_step += 1
                                data_s, label_s = data_s.to(self.device), label_s.to(self.device)
                                data_t_og = data_t_og.to(self.device)

                                # Train with target labels
                                score_s = self.model(data_s)
                                xeloss_src = lambda_src * nn.CrossEntropyLoss()(score_s, label_s)
                                loss = xeloss_src

                                info_str = "\n[Train SENTRY] Epoch: {}, it: {}/{}".format(epoch, batch_idx, joint_len)
                                info_str += " Source XE loss: {:.3f}".format(xeloss_src.item())

                                score_t_og = self.model(data_t_og)
                                batch_sz = data_t_og.shape[0]
                                tgt_preds = score_t_og.max(dim=1)[1].reshape(-1)

                                if (
                                        pointer + batch_sz > config["queue_length"]
                                ):  # Deal with wrap around when ql % batchsize != 0
                                        rem_space = config["queue_length"] - pointer
                                        queue[pointer : config["queue_length"]] = (
                                                tgt_preds[:rem_space].detach() + 1
                                        )
                                        queue[0 : batch_sz - rem_space] = tgt_preds[rem_space:].detach() + 1
                                else:
                                        queue[pointer : pointer + batch_sz] = tgt_preds.detach() + 1
                                pointer = (pointer + batch_sz) % config["queue_length"]

                                bincounts = (
                                        torch.bincount(queue.long(), minlength=self.num_classes + 1).float()
                                        / config["queue_length"]
                                )
                                bincounts = bincounts[1:]

                                log_q = torch.log(bincounts + 1e-12).detach()
                                loss_infoent = lambda_infoent * torch.mean(
                                        torch.sum(
                                                score_t_og.softmax(dim=1) * log_q.reshape(1, self.num_classes),
                                                dim=1,
                                        )
                                )
                                loss += loss_infoent
                                info_str += " Infoent loss: {:.3f}".format(loss_infoent.item())

                                score_t_og = self.model(data_t_og).detach()
                                tgt_preds = score_t_og.max(dim=1)[1].reshape(-1)

                                correct_mask, incorrect_mask = (
                                        torch.zeros_like(tgt_preds).to(self.device),
                                        torch.zeros_like(tgt_preds).to(self.device),
                                )

                                score_t_aug_pos, score_t_aug_neg = (
                                        torch.zeros_like(score_t_og),
                                        torch.zeros_like(score_t_og),
                                )
                                
                                for ix in range(config["committee_size"]):
                                        data_t_aug_curr = torch.cat([data_t_raug[el][ix].unsqueeze(0) \
                                                                                                 for el in range(len(data_t_og))]).to(self.device)
                                        score_t_aug_curr = self.model(data_t_aug_curr)
                                        tgt_preds_aug = score_t_aug_curr.max(dim=1)[1].reshape(-1)
                                        consistent_idxs = (tgt_preds == tgt_preds_aug).detach()
                                        inconsistent_idxs = (tgt_preds != tgt_preds_aug).detach()
                                        correct_mask = correct_mask + consistent_idxs.type(torch.uint8)
                                        incorrect_mask = incorrect_mask + inconsistent_idxs.type(torch.uint8)

                                        score_t_aug_pos[consistent_idxs, :] = score_t_aug_curr[consistent_idxs, :]
                                        score_t_aug_neg[inconsistent_idxs, :] = score_t_aug_curr[inconsistent_idxs, :]

                                correct_mask = correct_mask >= positive_threshold
                                incorrect_mask = incorrect_mask >= negative_threshold

                                # Compute some stats
                                correct_ratio = (correct_mask).sum().item() / data_t_og.shape[0]
                                incorrect_ratio = (incorrect_mask).sum().item() / data_t_og.shape[0]                            
                                info_str += "\n {:d} / {:d} consistent ({:.2f})".format(
                                        correct_mask.sum(),
                                        data_t_og.shape[0],
                                        correct_ratio,
                                )
                                
                                loss_cent_correct = torch.Tensor([-1])
                                if correct_ratio > 0.0:
                                        probs_t_pos = F.softmax(score_t_aug_pos, dim=1)
                                        loss_cent_correct = (
                                                lambda_ent
                                                * correct_ratio
                                                * -torch.mean(
                                                        torch.sum(
                                                                probs_t_pos[correct_mask]
                                                                * (torch.log(probs_t_pos[correct_mask] + 1e-12)),
                                                                1,
                                                        )
                                                )
                                        )
                                        loss += loss_cent_correct
                                        info_str += " SENTRY loss (consistent): {:.3f}".format(
                                                loss_cent_correct.item()
                                        )
                                
                                loss_cent_incorrect = torch.Tensor([-1])
                                if config["use_entmax"] and (incorrect_ratio > 0.0):
                                        probs_t_neg = F.softmax(score_t_aug_neg, dim=1)
                                        loss_cent_incorrect = (
                                                lambda_ent
                                                * incorrect_ratio
                                                * torch.mean(
                                                        torch.sum(
                                                                probs_t_neg[incorrect_mask]
                                                                * (torch.log(probs_t_neg[incorrect_mask] + 1e-12)),
                                                                1,
                                                        )
                                                )
                                        )
                                        loss += loss_cent_incorrect
                                        info_str += " SENTRY loss (inconsistent): {:.3f}".format(
                                                loss_cent_incorrect.item()
                                        )

                                # Backprop
                                self.tgt_opt.zero_grad()
                                loss.backward()
                                self.tgt_opt.step()

                                # Learning rate update (if using SGD)
                                self.lr_step()
                                
                                if WANDB_INFO.run_wandb:
                                    wb_tag = "IC"
                                    WANDB_INFO.log_metrics({
                                        f"SENTRY/loss_inconsistent" : loss_cent_incorrect.item(),
                                        f"SENTRY/loss_consistent" : loss_cent_correct.item(),
                                        f"SENTRY/consistent_ratio" : correct_ratio,
                                        f"SENTRY/infoent_loss" : loss_infoent.item(),
                                        f"SENTRY/xeloss_src" : xeloss_src.item()
                                    })

                                if batch_idx % 1000 == 0:
                                        logging.info(info_str)

                self.model.cpu()
                import gc

                gc.collect()  # Make sure all object have ben deallocated if not used
                torch.cuda.empty_cache()

                if self.config['model_save_path'] is not None:
                    save_path = self.config['model_save_path']
                    if not os.path.exists(save_path):
                        os.mkdir(save_path)
                    save_path = os.path.join(save_path, self.toolset['stage'])
                    if not os.path.exists(save_path):
                        os.mkdir(save_path)
                    
                    ckpt = self.toolset['ckpt']
                    torch.save(self.model.state_dict(), os.path.join(
                        save_path, f"model_{ckpt}.pth"
                    ))



        def inference(self, eval_dataset):
                """
                Inference is during the evaluation stage.  For this example, the
                task_network is trained in the train and adapt stage's code and
                is used here to create the predictions.  The indices are used to
                track the images/filenames for submitting the predictions back to
                JPL

                Args:
                        eval_dataset (framework.dataset): unlabeled evaluation framework.  We need to predict the classes
                                for each item in the dataset.  Example below.

                Returns:
                        tuple(list[int],list[int]): preds and indices
                                predicted category indices and image indices

                """
                self.model.eval()
                self.model.to(self.device)
                test_transforms = transforms.Compose(
                        [
                                transforms.Resize((224, 224)),
                                transforms.ToTensor(),
                                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
                        ]
                )
                eval_dataset.transform = test_transforms
                eval_dataloader = torch.utils.data.DataLoader(
                        eval_dataset, batch_size=int(self.config["batch_size"]), drop_last=False
                )
                preds = []
                indices = []
                with torch.no_grad():
                        for imgs,  _, inds in eval_dataloader:
                                imgs = imgs.to(self.device)
                                preds_ = self.model(imgs)
                                preds += torch.argmax(preds_, dim=1).cpu().numpy().tolist()
                                indices += inds.numpy().tolist()

                self.model.cpu()
                import gc

                gc.collect()  # Make sure all object have ben deallocated if not used
                torch.cuda.empty_cache()

                return preds, indices
