import scriptconfig as scfg


class Config(scfg.Config):
    """
    Default configuration for your algorithm

    Please add all of your default configuration parameters for your algorithm below
    with a description (help) on
        - what your parameter does
        - what changes when you use your parameter

    This is so that someone else can come and alter there parameters and know
    how this will affect the run of the algorithm

    """
    default = {
        'name': scfg.Value('SENTRY', help='name of the proposed method'),
        'seed': scfg.Value(1234, help='Random seed for reproducibility'),
        # CNN parameters
        'cnn': scfg.Value('ResNet50FS', help='ResNet50 with few-shot modifications (matching MME)'),
        'l2_normalize': scfg.Value(True, help='L2-normalize penultimate activations?'),
        'temperature': scfg.Value(8, help='CNN softmax temperature'),
        # Optimizer parameters
        'batch_size': scfg.Value(16, help='batch size of network while running.  '
                                          'Increasing will do X and decrease will do Y'),
        'num_workers': scfg.Value(8, help='number of workers in the dataloader'),                
        'da_lr': scfg.Value(0.001, help='Unsupervised DA Learning rate'),
        'da_wd': scfg.Value(0.0005, help='Unsupervised DA weight decay'),
        'da_num_epochs': scfg.Value(40, help='DA Number of epochs'),
        # Loss weights
        'lambda_src': scfg.Value(1.0, help='Source supervised XE loss weight'),
        'lambda_unsup': scfg.Value(0.1, help='Target unsupervised loss weight'),
        'lambda_ent': scfg.Value(1.0, help='Target entropy minimization loss weight'),
        # Miscellaneous SENTRY parameters
        'queue_length': scfg.Value(256, help='Queue length for computing target information entropy loss'),
        'randaug_n': scfg.Value(3, help='RandAugment number of consecutive transformations'),
        'randaug_m': scfg.Value(2.0, help='RandAugment severity'),
        'committee_size': scfg.Value(3, help='Committee size')   
    }