import torch
import torch.nn as nn
from torch.nn import init
import torch.nn.functional as F
import torchvision
from torchvision import models

class TaskNet(nn.Module):
    "Basic class which does classification."
    def __init__(self, num_classes, l2_normalize=False, temperature=1.0):
        super(TaskNet, self).__init__()
        self.num_classes = num_classes
        self.l2_normalize = l2_normalize
        self.temperature = temperature

    def forward(self, x):
        # Extract features
        x = self.conv_params(x)
        x = x.view(x.size(0), -1)
        x = x.clone()
        emb = self.fc_params(x)
        
        # Classify
        if isinstance(self.classifier, nn.Sequential): # LeNet   
            emb = self.classifier[:-1](emb)
            if self.l2_normalize: emb = F.normalize(emb)
            score = self.classifier[-1](emb) / self.temperature
        else:                                          # ResNet
            if self.l2_normalize: emb = F.normalize(emb)
            score = self.classifier(emb) / self.temperature
        
        return score

    def setup_net(self):
        """Method to be implemented in each class."""
        pass

    def load(self, init_path):
        net_init_dict = torch.load(init_path, map_location=torch.device('cpu'))
        self.load_state_dict(net_init_dict)

    def save(self, out_path):
        torch.save(self.state_dict(), out_path)

class ResNet50FS(TaskNet):    
    def __init__(self, num_classes, l2_normalize=False, temperature=1.0):
        super(ResNet50FS, self).__init__(num_classes, l2_normalize=False, temperature=1.0)
        model = models.resnet50(pretrained=True)
        model.fc = nn.Identity()
        self.conv_params = model
        self.fc_params = nn.Identity()        
        self.classifier = nn.Linear(2048, num_classes, bias=False)        
        init.xavier_normal_(self.classifier.weight)