from learn.algorithms.templateQueryAlgo.query_adapter import QueryAdapter
from learn.utils.dataset import (
    ImageClassificationDataset, ObjectDetectionDataset,
    MachineTranslationDataset
)
from typing import Union
import numpy as np
import logging
log = logging.getLogger(__name__)


class QueryAlgo(QueryAdapter):
    def __init__(self, toolset):
        QueryAdapter.__init__(self, toolset)
        # no config passed here

    def initialize(self,
                   source_dataset: Union[ImageClassificationDataset, ObjectDetectionDataset],
                   target_dataset: Union[ImageClassificationDataset, ObjectDetectionDataset]):
        """
        Initialize your algorithm and save any parameters or objects you want
        to be persistent between budget checkpoints.

        Args:
            source_dataset (ImageClassificationDataset): Pytorch dataset for source domain/task
            target_dataset: (ImageClassificationDataset): Pytorch dataset for target domain/task
        """
        log.info('Initializing Random Query algorithm')
        self.target_dataset = target_dataset

    def select_and_label_data(self, budget: int):
        """
        Select the data to be labelled by the active learning algorithm

        Args:
            budget:  number of samples we can ask for in the budget level

        Returns: None

        """
        log.info('Doing Random Sampling instead of Active Learning')
        target_dataset = self.target_dataset
        if budget > 0 and target_dataset.unlabeled_size > 0:
            if isinstance(target_dataset, MachineTranslationDataset):
                # budget here is # of characters rather than # of translation
                # records. Will approximate budget by asking for number of
                # characters in source text
                randomly_shuffled_indices = np.random.permutation(
                    target_dataset.get_unlabeled_indices())
                sampled_indices = []
                for i in randomly_shuffled_indices:
                    size = target_dataset.index_to_characters(i)
                    sampled_indices.append(i)
                    budget -= size
                    if budget < 0:
                        break
            else:
                sampled_indices = np.random.choice(target_dataset.get_unlabeled_indices(),
                                                   min(budget, target_dataset.unlabeled_size),
                                                   replace=False)
            target_dataset.get_more_labels(sampled_indices)

