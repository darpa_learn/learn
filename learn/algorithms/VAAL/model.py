from typing import List, Tuple
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init as init


class View(nn.Module):
    def __init__(self, size):
        super(View, self).__init__()
        self.size = size

    def forward(self, tensor):
        return tensor.view(self.size)


class VAE(nn.Module):
    """Encoder-Decoder architecture for both WAE-MMD and WAE-GAN."""

    def __init__(self, z_dim=32, nc=3, flatten_dim=14, device=torch.device(0)):
        super(VAE, self).__init__()
        self.z_dim = z_dim
        self.nc = nc
        self.encoder = nn.Sequential(
            nn.Conv2d(nc, 128, 4, 2, 1, bias=False),  # B,  128, 32, 32
            nn.BatchNorm2d(128),
            nn.ReLU(True),
            nn.Conv2d(128, 256, 4, 2, 1, bias=False),  # B,  256, 16, 16
            nn.BatchNorm2d(256),
            nn.ReLU(True),
            nn.Conv2d(256, 512, 4, 2, 1, bias=False),  # B,  512,  8,  8
            nn.BatchNorm2d(512),
            nn.ReLU(True),
            nn.Conv2d(512, 1024, 4, 2, 1, bias=False),  # B, 1024,  4,  4
            nn.BatchNorm2d(1024),
            nn.ReLU(True),
            View((-1, 1024 * flatten_dim * flatten_dim)),  # B, 1024*4*4
        )

        self.fc_mu = nn.Linear(1024 * flatten_dim * flatten_dim, z_dim)  # B, z_dim
        self.fc_logvar = nn.Linear(1024 * flatten_dim * flatten_dim, z_dim)  # B, z_dim
        self.decoder = nn.Sequential(
                nn.Linear(z_dim, 1024 * flatten_dim * flatten_dim),  # B, 1024*8*8
                View((-1, 1024, flatten_dim, flatten_dim)),  # B, 1024,  8,  8
            nn.ConvTranspose2d(
                1024, 512, 4, 2, 1, bias=False
            ),  # B,  512, 16, 16
            nn.BatchNorm2d(512),
            nn.ReLU(True),
            nn.ConvTranspose2d(
                512, 256, 4, 2, 1, bias=False
            ),  # B,  256, 32, 32
            nn.BatchNorm2d(256),
            nn.ReLU(True),
            nn.ConvTranspose2d(
                256, 128, 4, 2, 1, bias=False
            ),  # B,  128, 64, 64
            nn.BatchNorm2d(128),
            nn.ReLU(True),
            nn.ConvTranspose2d(128, nc, 4, 2, 1),  # B,   nc, 64, 64
        )
        self.weight_init()
        self.device=device

    def weight_init(self):
        for block in self._modules:
            try:
                for m in self._modules[block]:
                    kaiming_init(m)
            except:
                kaiming_init(block)

    def forward(self, x):
        z = self._encode(x)
        mu, logvar = self.fc_mu(z), self.fc_logvar(z)
        z = self.reparameterize(mu, logvar)
        x_recon = self._decode(z)
        return x_recon, z, mu, logvar

    def reparameterize(self, mu, logvar):
        stds = (0.5 * logvar).exp()
        epsilon = torch.randn(*mu.size())
        if mu.is_cuda:
            stds, epsilon = stds.cuda(self.device), epsilon.cuda(self.device)
        latents = epsilon * stds + mu
        return latents

    def _encode(self, x):
        return self.encoder(x)

    def _decode(self, z):
        return self.decoder(z)


class VAEBase(nn.Module):
    """
    Encoder-Decoder architecture for both WAE-MMD and WAE-GAN.
    Arguments:
        z_dim: dim of latent space
        nc: number of channels for input images
    Notes:
        Looks like original implementation of this encoder/decoder architecture
        came from: https://github.com/1Konny/WAE-pytorch/blob/master/model.py
    """

    def __init__(self, z_dim: int = 32, nc: int = 3, input_size: int = 32, device=torch.device(0)):
        super().__init__()
        self.z_dim: int = z_dim
        self.nc: int = nc
        self.input_size: int = input_size
        (
            self.encoder,
            self.decoder,
            self.flattened_encoder_output_size,
        ) = self.build_encoder_decoder()
        self.fc_mu = nn.Linear(self.flattened_encoder_output_size, z_dim)  # B, z_dim
        self.fc_logvar = nn.Linear(
            self.flattened_encoder_output_size, z_dim
        )  # B, z_dim

        self.weight_init()
        self.device = device

    def get_conv_output_size(
        self, input_size: int, kernel_size: int, stride=1, padding=0, dilation=1,
    ) -> int:
        """
        Calculate and return size of output dimension of nn.Conv2d for given conv.
        params.
        """
        return int(
            ((input_size + 2 * padding - dilation * (kernel_size - 1) - 1) / stride) + 1
        )

    def get_convtranspose_output_size(
        self,
        input_size: int,
        kernel_size: int,
        stride=1,
        padding=0,
        output_padding=0,
        dilation=1,
    ) -> int:
        """
        Calculate and return size of output dimension of nn.ConvTranspose2d for given
        conv. params.
        """
        return int(
            (kernel_size - 1) * stride
            - 2 * padding
            + dilation * (kernel_size - 1)
            + output_padding
            + 1
        )

    def get_network_output_size(
        self, input_size: int, layer_params: List[Tuple[int]], type="conv"
    ) -> int:
        """
        Given list of convolutional params, and input dim size, calculate dim size
        of network output.
        """
        output_size = input_size
        tensor_sizes = []
        if type == "conv":
            for kernel_size, stride, padding, dilation in layer_params:
                output_size = self.get_conv_output_size(
                    output_size, kernel_size, stride, padding, dilation
                )
                tensor_sizes.append(output_size)
                # print(f"output_size: {output_size}")
        else:
            for kernel_size, stride, padding, output_padding, dilation in layer_params:
                output_size = self.get_convtranspose_output_size(
                    output_size, kernel_size, stride, padding, output_padding, dilation
                )
                tensor_sizes.append(output_size)
        return output_size, tensor_sizes

    def build_encoder_decoder(self):
        # Calculate the size of the flattened vector after passing it through the
        # encoder:
        layer_params = [
            (4, 2, 1, 1),
            (4, 2, 1, 1),
            (4, 2, 1, 1),
            (4, 2, 1, 1),
        ]
        self.encoder_output_size, tensor_sizes = self.get_network_output_size(
            self.input_size, layer_params, type="conv"
        )
        flattened_encoder_output_size = (
            1024 * self.encoder_output_size * self.encoder_output_size
        )
        # print(f"input_size: {self.input_size}")
        # print(f"encoder_output_size: {self.encoder_output_size}")
        # print("encoder intermediate dims: ", tensor_sizes)

        encoder = nn.Sequential(
            nn.Conv2d(self.nc, 128, 4, 2, 1, bias=False),  # B,  128, 32, 32
            nn.BatchNorm2d(128),
            nn.ReLU(True),
            nn.Conv2d(128, 256, 4, 2, 1, bias=False),  # B,  256, 16, 16
            nn.BatchNorm2d(256),
            nn.ReLU(True),
            nn.Conv2d(256, 512, 4, 2, 1, bias=False),  # B,  512,  8,  8
            nn.BatchNorm2d(512),
            nn.ReLU(True),
            nn.Conv2d(512, 1024, 4, 2, 1, bias=False),  # B, 1024,  4,  4
            nn.BatchNorm2d(1024),
            nn.ReLU(True),
            View((-1, flattened_encoder_output_size)),  # B, 1024*4*4
        )
        second_to_last_layer_size = tensor_sizes[-2]
        # print(f"second_to_last_layer_size: {second_to_last_layer_size}")
        decoder = nn.Sequential(
            nn.Linear(
                self.z_dim, 1024 * second_to_last_layer_size * second_to_last_layer_size
            ),  # B, 1024*8*8
            View(
                (-1, 1024, second_to_last_layer_size, second_to_last_layer_size)
            ),  # B, 1024,  8,  8
            nn.ConvTranspose2d(1024, 512, 4, 2, 1, bias=False),  # B,  512, 16, 16
            nn.BatchNorm2d(512),
            nn.ReLU(True),
            nn.ConvTranspose2d(512, 256, 4, 2, 1, bias=False),  # B,  256, 32, 32
            nn.BatchNorm2d(256),
            nn.ReLU(True),
            nn.ConvTranspose2d(256, 128, 4, 2, 1, bias=False),  # B,  128, 64, 64
            nn.BatchNorm2d(128),
            nn.ReLU(True),
            nn.ConvTranspose2d(128, self.nc, 1),  # B,   nc, 64, 64
        )
        return encoder, decoder, flattened_encoder_output_size

    def weight_init(self):
        for block in self._modules:
            try:
                for m in self._modules[block]:
                    kaiming_init(m)
            except:
                kaiming_init(block)

    def forward(self, x, include_reconstruction=True):
        z = self._encode(x)
        mu, logvar = self.fc_mu(z), self.fc_logvar(z)
        z = self.reparameterize(mu, logvar)

        if include_reconstruction:
            x_recon = self._decode(z)
            return x_recon, z, mu, logvar
        else:
            return None, z, mu, logvar

    def reparameterize(self, mu, logvar):
        stds = (0.5 * logvar).exp()
        epsilon = torch.randn(*mu.size(), device=mu.device)
        latents = epsilon * stds + mu
        return latents

    def _encode(self, x):
        return self.encoder(x)

    def _decode(self, z):
        return self.decoder(z)


# Note: If you sub-class VAE, all you have to do is define a build_encoder_decoder(self)
# method, as in this example:
class BetaVAE(VAEBase):
    """
    Model proposed in original beta-VAE paper(Higgins et al, ICLR, 2017).
    Based on: https://github.com/1Konny/Beta-VAE/blob/master/model.py.
    This model accepts inputs of any size.
    If input size is smaller than 64, it will remove one of the encoder's convolutional
    layers (as well as the corresponding layer in the decoder) to avoid trying to output
    a tensor with HxW dim's less than zero.
    """

    def __init__(self, z_dim: int = 32, nc: int = 3, input_size: int = 32, device=torch.device(0)):
        super().__init__(z_dim, nc, input_size, device)

    def build_encoder_decoder(self):
        # Calculate the size of the flattened vector after passing it through the
        # encoder:
        layer_params = [
            (4, 2, 1, 1),
            (4, 2, 1, 1),
            (4, 2, 1, 1),
            (4, 2, 1, 1),
            (4, 1, 0, 1),
        ]
        if self.input_size < 64:
            del layer_params[1]
        self.encoder_output_size, tensor_sizes = self.get_network_output_size(
            self.input_size, layer_params, type="conv"
        )
        flattened_encoder_output_size = (
            256 * self.encoder_output_size * self.encoder_output_size
        )

        encoder_layers = [
            nn.Conv2d(self.nc, 32, 4, 2, 1),
            nn.ReLU(True),
            nn.Conv2d(32, 32, 4, 2, 1),  # B,  32, _, _           # ***del if input<32
            nn.ReLU(True),  # ***del if input<32
            nn.Conv2d(32, 64, 4, 2, 1),  # B,  64,  _,  _
            nn.ReLU(True),
            nn.Conv2d(64, 64, 4, 2, 1),  # B,  64,  _,  _
            nn.ReLU(True),
            nn.Conv2d(64, 256, 4, 1),  # B, 256,  _,  _
            nn.ReLU(True),
            View((-1, flattened_encoder_output_size)),  # B, 256
        ]
        last_layer_size = tensor_sizes[-1]
        # print(f"last_layer_size: {last_layer_size}")
        decoder_layers = [
            nn.Linear(self.z_dim, 256 * last_layer_size * last_layer_size),  # B, 256
            View((-1, 256, last_layer_size, last_layer_size)),  # B, 256, _, _
            nn.ReLU(True),
            nn.ConvTranspose2d(256, 64, 4),  # B,  64, _, _
            nn.ReLU(True),
            nn.ConvTranspose2d(64, 64, 4, 2, 1),  # B,  64, _, _
            nn.ReLU(True),
            nn.ConvTranspose2d(64, 32, 4, 2, 1),  # B,  32, _, _
            nn.ReLU(True),
            nn.ConvTranspose2d(32, 32, 4, 2, 1),  # B,  32, _, _  # ***del if input<32
            nn.ReLU(True),  # ***del if input<32
            nn.ConvTranspose2d(32, self.nc, 4, 2, 1),  # B, nc, _, _
        ]
        if self.input_size < 64:
            del encoder_layers[3]
            del encoder_layers[2]
            del decoder_layers[10]
            del decoder_layers[9]
        encoder = nn.Sequential(*encoder_layers)
        decoder = nn.Sequential(*decoder_layers)
        return encoder, decoder, flattened_encoder_output_size


class Discriminator(nn.Module):
    """Adversary architecture(Discriminator) for WAE-GAN."""

    def __init__(self, z_dim=10):
        super(Discriminator, self).__init__()
        self.z_dim = z_dim
        self.net = nn.Sequential(
            nn.Linear(z_dim, 512),
            nn.ReLU(True),
            nn.Linear(512, 512),
            nn.ReLU(True),
            nn.Linear(512, 512),
            nn.ReLU(True),
            nn.Linear(512, 512),
            nn.ReLU(True),
            nn.Linear(512, 1),
            nn.Sigmoid(),
        )
        self.weight_init()

    def weight_init(self):
        for block in self._modules:
            for m in self._modules[block]:
                kaiming_init(m)

    def forward(self, z):
        return torch.squeeze(self.net(z))


class DiscriminatorZDim(nn.Module):
    """Smaller version of Adversary architecture(Discriminator) for WAE-GAN. Channels =
    z-dim, at each layer"""

    def __init__(self, z_dim=10):
        super().__init__()
        self.z_dim = z_dim
        self.net = nn.Sequential(
            nn.Linear(z_dim, z_dim),
            nn.ReLU(True),
            nn.Linear(z_dim, z_dim),
            nn.ReLU(True),
            nn.Linear(z_dim, z_dim),
            nn.ReLU(True),
            nn.Linear(z_dim, z_dim),
            nn.ReLU(True),
            nn.Linear(z_dim, 1),
            nn.Sigmoid(),
        )
        self.weight_init()

    def weight_init(self):
        for block in self._modules:
            for m in self._modules[block]:
                kaiming_init(m)

    def forward(self, z):
        return self.net(z)


def kaiming_init(m):
    if isinstance(m, (nn.Linear, nn.Conv2d)):
        init.kaiming_normal_(m.weight)
        if m.bias is not None:
            m.bias.data.fill_(0)
    elif isinstance(m, (nn.BatchNorm1d, nn.BatchNorm2d)):
        m.weight.data.fill_(1)
        if m.bias is not None:
            m.bias.data.fill_(0)


def normal_init(m, mean, std):
    if isinstance(m, (nn.Linear, nn.Conv2d)):
        m.weight.data.normal_(mean, std)
        if m.bias.data is not None:
            m.bias.data.zero_()
    elif isinstance(m, (nn.BatchNorm2d, nn.BatchNorm1d)):
        m.weight.data.fill_(1)
        if m.bias.data is not None:
            m.bias.data.zero_()
