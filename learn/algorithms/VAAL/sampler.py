import torch
import torch.nn as nn
from typing import List, Tuple
# https://github.com/pytorch/pytorch/issues/973
torch.multiprocessing.set_sharing_strategy('file_system')
import ubelt as ub
import numpy as np
import resource


class AdversarySampler:
    def __init__(self, budget):
        self.budget = budget

    def sample(self, vae, discriminator, data, cuda, device):
        all_preds = []
        all_indices = []
        for images, _, indices in ub.ProgIter(data, desc='VAE sampling'):
            if cuda:
                images = images.cuda(device)
            with torch.no_grad():
                _, _, mu, _ = vae(images)
                preds = discriminator(mu)

            preds = preds.cpu().data
            # If only 1 image is present in a batch
            if len(preds.size())==0:
                preds = preds.unsqueeze(dim=0)
            all_preds.extend(preds)
            all_indices.extend(indices)

        all_preds = torch.stack(all_preds)
        all_preds = all_preds.view(-1)
        # need to multiply by -1 to be able to use torch.topk
        all_preds *= -1

        # Select the points which the discriminator things are the most
        # likely to be unlabeled
        _, querry_indices = torch.topk(all_preds, int(self.budget))
        querry_pool_indices = np.asarray(all_indices)[querry_indices]

        return querry_pool_indices

    def get_predictions_and_indices(
        self,
        vae: nn.Module,
        discriminator: nn.Module,
        data: torch.utils.data.DataLoader,
        device: torch.device = None,
    ) -> Tuple[List[float], List[int]]:
        """
        Returns discriminator score for every item in the dataloader
        """
        all_preds: List[float] = []
        all_indices: List[int] = []
        vae.eval()
        discriminator.eval()

        with torch.no_grad():
            for images, _, indices in ub.ProgIter(data, desc='VAE sampling'):
                if device:
                    images = images.to(device)
                # VAE fwd pass returns: (x_recon, z, mu, logvar)
                # We only  need mu, so disable img reconstruction for a huge speedup:
                vae_result = vae.forward(images, include_reconstruction=False)
                mu = vae_result[2]
                preds = discriminator(mu)
                preds = preds.cpu().data.squeeze()
                if len(preds.shape) == 0:
                    all_preds.append(preds.item())
                else:
                    all_preds.extend(preds.tolist())
                all_indices.extend(indices.tolist())
                del images, indices, mu, vae_result, preds
        vae.train()
        discriminator.train()
        return all_preds, all_indices
