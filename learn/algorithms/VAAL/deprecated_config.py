import scriptconfig as scfg


class Config(scfg.Config):
    """
    Default configuration for your algorithm

    Please add all of your default configuration parameters for your algorithm below
    with a description (help) on
        - what your parameter does
        - what changes when you use your parameter

    This is so that someone else can come and alter there parameters and know
    how this will affect the run of the algorithm

    """
    default = {
        'name': scfg.Value('VAAL',
                           help='Name of the Domain Selection Algorithm'),
        "batch_size": scfg.Value(384,
                                 help='Batch size of the network'),
        "num_workers": scfg.Value(16,
                                  help='Number of workers needed for the dataloader'),
        "latent_dim": scfg.Value(128,
                                 help='Latent dimension used by VAE'),
        "vae_model": scfg.Value("betavae",
                                help="VAE model to use"),
        "discriminator_model": scfg.Value("discriminator_smaller",
                                          help="Discriminator model to use"),
        "input_size": scfg.Value(224,
                                 help="size of images input into VAAL (after transforms)"
                                      "only needed for some vae models"),
        "flatten_dim": scfg.Value(14,
                                  help="Size of data after the data has been encoded."
                                       "This parameter depends on the input image size"
                                       "Not used if BetaVAE is used (value gets derrived based on img size)"),
        "cuda": scfg.Value(True,
                           help='Flag to determine if GPU is used by the active learner'),
        "device": scfg.Value([0],
                             help='CUDA Device used during training'),
        "train_iterations": scfg.Value(8800,
                                       help='Number of training iterations'),
        "num_vae_steps": scfg.Value(1,
                                    help='Number of steps VAE is trained'),
        "num_adv_steps": scfg.Value(1,
                                    help='Number of adversarial steps'),
        "adversary_param": scfg.Value(1,
                                      help='Adversary parameter'),
        "beta": scfg.Value(1,
                           help='Beta associated with VAE'),
        'seed': scfg.Value(1,
                           help='random seed (default: 1)'),
        'learning_rate': scfg.Value(5e-4,
                                    help='learning rate for algorithms (default: 5e-4)'),
        'print_iter': scfg.Value(25,
                                 help='iteration for printing relevant information'),
        'vaal_reduction_in_mse_loss': scfg.Value('sum',
                                                 help='Switching between old and new loss in VAAL.  This is '
                                                      'sum or mean where sum makes better reconsturcitons '
                                                      'and mean normalizes between image sizes.'),
    }
