import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

import numpy as np
from sklearn.metrics import accuracy_score

import ubelt as ub
import logging


class Solver:
    def __init__(self, args, test_dataloader):
        self.args = args
        self.test_dataloader = test_dataloader

        self.bce_loss = nn.BCELoss()
        self.mse_loss = nn.MSELoss()
        self.ce_loss = nn.CrossEntropyLoss()

        self.sampler = None

    def read_data(self, dataloader, labels=True):
        if labels:
            while True:
                for img, label, _ in dataloader:
                    yield img, label
        else:
            while True:
                for img, _, _ in dataloader:
                    yield img

    def train(
        self,
        querry_dataloader,
        task_model,
        vae,
        discriminator,
        unlabeled_dataloader,
    ):
        labeled_data = self.read_data(querry_dataloader)
        unlabeled_data = self.read_data(unlabeled_dataloader, labels=False)

        optim_vae = optim.Adam(vae.parameters(),
                               lr=self.args["learning_rate"])

        if task_model:
            optim_task_model = optim.Adam(task_model.parameters(),
                                          lr=self.args["learning_rate"])
        optim_discriminator = optim.Adam(discriminator.parameters(),
                lr=self.args["learning_rate"])

        vae.train()
        discriminator.train()
        if task_model:
            task_model.train()
        loss_reduction = self.args['vaal_reduction_in_mse_loss']
        if self.args["cuda"]:
            vae = vae.cuda(torch.device(int(self.args["device"])))
            discriminator = discriminator.cuda(torch.device(int(self.args["device"])))
            if task_model:
                task_model = task_model.cuda(torch.device(int(self.args["device"])))

        change_lr_iter = int(self.args["train_iterations"]) // 25
        train_prog = ub.ProgIter(range(int(self.args["train_iterations"])),
                                 desc='Training on Data')
        for iter_count in train_prog:
            display_this_iter = iter_count % self.args["print_iter"] == 0
            if iter_count != 0 and iter_count % change_lr_iter == 0:
                for param in optim_vae.param_groups:
                    param["lr"] = param["lr"] * 0.9
                if task_model:
                    for param in optim_task_model.param_groups:
                        param["lr"] = param["lr"] * 0.9

                for param in optim_discriminator.param_groups:
                    param["lr"] = param["lr"] * 0.9
            labeled_imgs, labels = next(labeled_data)
            unlabeled_imgs = next(unlabeled_data)
            if self.args["cuda"]:
                labeled_imgs = labeled_imgs.cuda(torch.device(int(self.args["device"])))
                unlabeled_imgs = unlabeled_imgs.cuda(torch.device(int(self.args["device"])))

            if task_model:
                # task_model step
                if self.args["cuda"]:
                    labels = labels.cuda(torch.device(int(self.args["device"])))
                preds = np.squeeze(task_model(labeled_imgs))
                task_loss = self.ce_loss(preds, labels)
                optim_task_model.zero_grad()
                task_loss.backward()
                optim_task_model.step()

            # VAE step
            for count in range(int(self.args["num_vae_steps"])):
                recon, z, mu, logvar = vae(labeled_imgs)
                unsup_loss = self.vae_loss(
                    labeled_imgs,
                    recon,
                    mu,
                    logvar,
                    self.args["beta"],
                    loss_reduction,
                    display_this_iter,
                    prefix='unsup_loss'
                )
                unlab_recon, unlab_z, unlab_mu, unlab_logvar = vae(
                    unlabeled_imgs
                )
                transductive_loss = self.vae_loss(
                    unlabeled_imgs,
                    unlab_recon,
                    unlab_mu,
                    unlab_logvar,
                    self.args["beta"],
                    loss_reduction,
                    display_this_iter,
                    prefix='transductive_loss'
                )

                labeled_preds = discriminator(mu)
                unlabeled_preds = discriminator(unlab_mu)

                lab_real_preds = torch.ones(labeled_imgs.size(0))
                unlab_real_preds = torch.ones(unlabeled_imgs.size(0))

                if self.args["cuda"]:
                    lab_real_preds = lab_real_preds.cuda(torch.device(int(self.args["device"])))
                    unlab_real_preds = unlab_real_preds.cuda(torch.device(int(self.args["device"])))

                dsc_loss = self.bce_loss(
                    labeled_preds, lab_real_preds
                ) + self.bce_loss(unlabeled_preds, unlab_real_preds)
                total_vae_loss = (
                    unsup_loss
                    + transductive_loss
                    + self.args["adversary_param"] * dsc_loss
                )
                if display_this_iter:
                    logging.info(f'VAE dsc_loss: {dsc_loss.item():0.04g}')
                optim_vae.zero_grad()
                total_vae_loss.backward()
                optim_vae.step()

                # sample new batch if needed to train the adversarial network
                if count < int(self.args["num_vae_steps"] - 1):
                    labeled_imgs, _ = next(labeled_data)
                    unlabeled_imgs = next(unlabeled_data)

                    if self.args["cuda"]:
                        labeled_imgs = labeled_imgs.cuda(torch.device(int(self.args["device"])))
                        unlabeled_imgs = unlabeled_imgs.cuda(torch.device(int(self.args["device"])))

            # Discriminator step
            for count in range(int(self.args["num_adv_steps"])):
                with torch.no_grad():
                    _, _, mu, _ = vae(labeled_imgs)
                    _, _, unlab_mu, _ = vae(unlabeled_imgs)

                labeled_preds = discriminator(mu)
                unlabeled_preds = discriminator(unlab_mu)

                lab_real_preds = torch.ones(labeled_imgs.size(0))
                unlab_fake_preds = torch.zeros(unlabeled_imgs.size(0))

                if self.args["cuda"]:
                    lab_real_preds = lab_real_preds.cuda(torch.device(int(self.args["device"])))
                    unlab_fake_preds = unlab_fake_preds.cuda(torch.device(int(self.args["device"])))

                dsc_loss = self.bce_loss(
                    labeled_preds, lab_real_preds
                ) + self.bce_loss(unlabeled_preds, unlab_fake_preds)
                if display_this_iter:
                    logging.info(f'Discriminator dsc_loss: {dsc_loss.item():0.04g}')

                optim_discriminator.zero_grad()
                dsc_loss.backward()
                optim_discriminator.step()

                # sample new batch if needed to train the adversarial network
                if count < int(self.args["num_adv_steps"] - 1):
                    labeled_imgs, _ = next(labeled_data)
                    unlabeled_imgs = next(unlabeled_data)

                    if self.args["cuda"]:
                        labeled_imgs = labeled_imgs.cuda(torch.device(int(self.args["device"])))
                        unlabeled_imgs = unlabeled_imgs.cuda(torch.device(int(self.args["device"])))

            if display_this_iter:
                train_prog.ensure_newline()
                logging.info("Current training iteration: {}".format(iter_count))

                if task_model:
                    logging.info(
                        "Current task model loss: {:.4f}".format(task_loss.item())
                    )
                logging.info(
                    "Current vae model loss: {:.4f}".format(
                        total_vae_loss.item()
                    )
                )
            if iter_count % (self.args["print_iter"]*10) == 0:
                train_prog.ensure_newline()
                pred_mean_lab, pred_mean_unlab = \
                    self.check_dsc_preds(vae, discriminator, querry_dataloader, unlabeled_dataloader)
                logging.info(f"pred_mean_lab: {pred_mean_lab}, pred_mean_unlab: {pred_mean_unlab}")

        if task_model:
            final_accuracy = self.test(task_model, querry_dataloader)
        else:
            final_accuracy = 0

        return final_accuracy, task_model, vae, discriminator

    def sample_for_labeling(self, vae, discriminator, unlabeled_dataloader):
        querry_indices = self.sampler.sample(
            vae, discriminator, unlabeled_dataloader, self.args["cuda"],
            torch.device(int(self.args["device"]))
        )

        return querry_indices

    def check_dsc_preds(self, vae, discriminator, labelled_loader, unlabelled_loader) -> None:
        """
        Get mean dsc prediction on labelled and unlabelled sets
        """

        def get_pred_mean(data_loader):
            preds, indices = self.sampler.get_predictions_and_indices(
                vae, discriminator, data_loader, torch.device(int(self.args["device"]))
            )
            return np.mean(preds), np.std(preds)

        pred_mean_labelled, pred_std_labelled = get_pred_mean(labelled_loader)
        pred_mean_unlabelled, pred_std_unlabelled = get_pred_mean(unlabelled_loader)

        vae.train()
        discriminator.train()
        return pred_mean_labelled, pred_mean_unlabelled

    def test(self, task_model, test_dataloader):
        task_model.eval()
        total, correct = 0, 0
        for imgs, labels, _ in test_dataloader:
            if self.args["cuda"]:
                imgs = imgs.cuda(torch.device(int(self.args["device"])))

            with torch.no_grad():
                preds = task_model(imgs)

            preds = torch.argmax(preds, dim=1).cpu().numpy()
            correct += accuracy_score(labels, preds, normalize=False)
            total += imgs.size(0)
        return correct / total * 100

    def vae_loss(self, x, recon, mu, logvar, beta, reduction, display=False, prefix='loss'):
        """
        Loss from https://github.com/1Konny/Beta-VAE/blob/master/solver.py
        The differences between this and VAELoss are:
        (1) This loss uses reduction="sum" for MSE reconstruction loss, while VAE loss
        uses default of reduction="mean". So VAELoss uses average MSE each items in the
        batch while this loss function uses the MSE of the entire batch and normalizes
        by batch size.
        total_items = recon.shape[0]*recon.shape[1]*recon.shape[2]*recon.shape[3]
        VAELoss.reconstruction_loss         -> torch.sum((recon-x)**2) / total_items
        KonnyVAELoss.reconstruction_loss    -> torch.sum((recon-x)**2) / batch_size
        (2) This loss takes the average of the KLD for each z vector in the batch, while
        VAELoss just sums the KLD for all items in the batch. In other words:
        VAE.kld  -> -0.5*torch.sum(1 + logvar - mu.pow(2) - logvar.exp())
        Konny.kld->(-0.5*torch.sum(1 + logvar - mu.pow(2) - logvar.exp())) / batch_size
        Notice that the only real difference between the two loss functions is the
        relative scale of the reconstruction and KLD terms. So we can fix the VAELoss by
        dividing VAE.kld above by total_items (gbiamby: I verified this change
        successfully generates reconstructions)
        """
        batch_size = x.size(0)
        # E[log P(X|z)]
        # Reconstruction loss
        # print(f"Loss, x.shape: {x.shape}, recon.shape: {recon.shape}")
        recon = torch.sigmoid(recon)
        recon_loss = F.mse_loss(recon, x, reduction=reduction).div(batch_size)

        # D_KL(Q(z|X) || P(z)):
        # logvar and mu have shape: [B, z_dim]
        klds = -0.5 * (1 + logvar - mu.pow(2) - logvar.exp())
        total_kld = klds.sum(1).mean(0, True)
        vae_loss = recon_loss + beta * total_kld
        if display:
            logging.info(f'\nCurrent {prefix} loss: {recon_loss.item()}\n'
                         f'\tRecon: {recon_loss.item()}\n'
                         f'\tTotal_kld: {total_kld.item()}')
        return vae_loss
