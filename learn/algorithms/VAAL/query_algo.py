from learn.algorithms.VAAL.query_adapter import QueryAdapter
from learn.algorithms.VAAL import sampler, solver, model
import torch
import ubelt as ub
import logging
from torchvision import transforms
from learn.utils.dataset import ImageClassificationDataset


class QueryAlgo(QueryAdapter):
    def __init__(self, toolset):
        QueryAdapter.__init__(self, toolset)
        self.config = toolset['protocol_config']['active_learner']['params']

        self.device = self.config['device']
        if ub.iterable(self.device):
            self.device = self.device[0]
            self.config['device'] = self.device

    def initialize(self, source_dataset, target_dataset):
        """
        Initialize your algorithm and save any parameters or objects you want
        to be persistent between budget checkpoints.

        Args:
            source_dataset (ImageClassificationDataset): Pytorch dataset for source domain/task
            target_dataset: (ImageClassificationDataset): Pytorch dataset for target domain/task
        """
        logging.info('Initializing VAAL algorithm')

        self.vaal = solver.Solver(self.config, None)
        self.source_dataset = source_dataset
        self.target_dataset = target_dataset
        self.set_augmentations(self.source_dataset)
        self.set_augmentations(self.target_dataset)
        self.cuda = self.config["cuda"]
        torch.cuda.manual_seed(self.config['seed'])
        self.train_accuracies = []

    @staticmethod
    def set_augmentations(dataset):
        """ Set the augmentations for a dataset

        Args:
            dataset (ImageClassificationDataset): dataset

        Returns:
            None

        """
        crop_size = 224
        dataset.transform = transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(crop_size),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])

    def get_dataloaders(self, source_dataset, target_dataset):
        """
        Create Dataloaders for labeled data by getting the labeled indices and
        creating a random sub-sampler.  Then create the dataloader using this
        sampler

        Note: To change the transformer in the JPLDataset dataset, edit
             current_dataset.transform.  You can also edit the labels during
             loading by editing the  current_dataset.target_transform

        Note: Another approach could be to inherit the JPLDataset class and
             override the __get_item__ function

        Check out dataset.py for more information
        Notes: There could be NO unlabeled data and the algorithm has to be
            able to handle that case!

            The min batch size here is so that the batch size isn't
            larger than the labeled/unlabeled dataset which causes the
            dataloader to hang


        Args:
            source_dataset (framework.dataset): Pytorch dataset for source domain/task
            target_dataset: (framework.dataset): Pytorch dataset for target domain/task

        Returns:
            A tuple of dataloaders associated with labelled and unlabelled data
        """
        labeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
            target_dataset.get_labeled_indices()
        )
        labeled_dataloader = torch.utils.data.DataLoader(
            target_dataset,
            sampler=labeled_sampler,
            batch_size=min(target_dataset.labeled_size,
                           int(self.config["batch_size"])),
            num_workers=int(self.config["num_workers"])//2,
            collate_fn=target_dataset.collate_batch,
            drop_last=True,
        )

        # ###################  Creating the Unlabeled DataLoader ###############
        #  Same as labeled dataaset but for unlabeled indices
        unlabeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
            target_dataset.get_unlabeled_indices()
        )
        unlabeled_dataloader = torch.utils.data.DataLoader(
            target_dataset,
            sampler=unlabeled_sampler,
            batch_size=min(target_dataset.unlabeled_size,
                           int(self.config["batch_size"])
                           ),
            num_workers=int(self.config["num_workers"])//2,
            drop_last=False,
        )

        return labeled_dataloader, unlabeled_dataloader

    def select_and_label_data(self, budget):
        """
        Select the data to be labelled by the active learning algorithm
        """
        labeled_dataloader, unlabeled_dataloader = self.get_dataloaders(
            self.source_dataset, self.target_dataset)

        # Initialize/Re-Initialize models
        if self.target_dataset.unlabeled_size > 0:
            if self.config["vae_model"].lower() == "vae":
                logging.info("Building VAE")
                vae = model.VAE(int(self.config["latent_dim"]),
                                flatten_dim=int(self.config["flatten_dim"]),
                                device=torch.device(self.device))
            else:
                logging.info("Building BetaVAE")
                vae = model.BetaVAE(int(self.config["latent_dim"]),
                                input_size=int(self.config["input_size"]),
                                device=torch.device(self.device))
            if self.config["discriminator_model"].lower() == "discriminator_smaller":
                discriminator = model.DiscriminatorZDim(int(self.config["latent_dim"]))
            else:
                discriminator = model.Discriminator(int(self.config["latent_dim"]))

            #  This approach sets the budget for how many images that they want labeled.
            self.vaal.sampler = sampler.AdversarySampler(budget)

            # train the models on the current data
            acc, task_model, vae, discriminator = self.vaal.train(
                labeled_dataloader,
                None,  # Setting task model to None so it doesn't train
                vae,
                discriminator,
                unlabeled_dataloader,
            )

            # You pick which indices that you want labeled.  Here, it is using
            # dataset to ensure the correct indices and tracking inside their
            # function (the dataset getitem returns the index)
            sampled_indices = self.vaal.sample_for_labeling(
                vae, discriminator, unlabeled_dataloader)

            #  ########### Query for labels -- Kitware managed ################
            #  This function is handled by Kitware and takes the indices from the
            #  algorithm and queries for new labels. The new labels are added to
            #  the dataset and the labeled/unlabeled indices are updated

            self.target_dataset.get_more_labels(sampled_indices)

        del labeled_dataloader, unlabeled_dataloader
        #        #  Note: you don't have to request the entire budget, but
        #        #      you shouldn't end the function until the budget is exhausted
        #        #      since the budget is lost after evaluation.

        #
        # ##################  End of ACTIVE LEARNING #####################
