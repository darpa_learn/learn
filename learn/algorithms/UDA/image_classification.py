import torch
from .imageClassificationAdapter import ImageClassifierAdapter
import torch.utils.data as data
import torch.nn as nn
from .config import Config

from .utils.parse_tasks import parse_tasks
import torch.optim as optim
from .utils.train import train
import logging


class ImageClassifierAlgorithm(ImageClassifierAdapter):

    def __init__(self, toolset):
        # def __init__(self, problem, base_dataset, adapt_dataset, arguments):

        # Load the default configuration from config.py for your algorithm
        self.config = Config()
        self.update_parameters(toolset['protocol_config'])

        # This sends the configuration back to the toolset
        self.toolset['classification_config'] = self.config

    def update_parameters(self, protocol_config):
        """  Update parameters for algorithm based on protocol config

        Args:
            protocol_config (dict):  Dictionary of parameters.

        Returns:
            None
        """
        common_keyword = set(protocol_config.keys()) & set(self.config.keys())
        for ck in common_keyword:
            self.config[ck] = protocol_config[ck]

    def initialize(self, source_network, source_dataset, whitelist_datasets):
        """  Initialize your algorithm and save any parameters or objects you want to be persistent between
        budget checkpoints.  DomainNetworkSelection will choose a potential source datasets `source_dataset`
        and provide a trained network for that dataset `source_dataset.` Please do not modify the datasets
        beyond overriding the transforms (self.transform and self.transform_target are fine to change).

        Args:
            source_network (pytorch.nn.Module):  Pretrained network on source task
            source_dataset (framework.dataset): Pytorch dataset for source task
            whitelist_datasets (dict[framework.dataset]): Dictionary of all possible external datasets which
                can be used on this task.  You may use this if you want to select a different dataset than
                the one selected as source.

        """
        # TODO: finish implementation
        raise NotImplementedError
        self.net = source_network
        # TODO: create generalized extractor
        self.ext = extractor_from_layer3(net)
        self.source_dataset_train = source_dataset
        # TODO: go through whitelist to get source datasets
        self.source_dataset_test = source_dataset
        logging.info(f"{whitelist_datasets.keys()}")

    def domain_adapt_training(self, target_dataset, eval_dataset):
        """ This is where you will fine-tune/train your few-shot or domain adaption algorithm.  You may use
        the dataloaders below and feel free to override the transformers in the datasets to use whatever
        pretraining you desire. Please do not modify the datasets though.

        Args:
            target_dataset (framework.dataset): pytorch dataset for the target dataset used for training
            eval_dataset (framework.dataset): eval dataset used for evaluation.  Included here in case you
                want to do transductive learning.
        """

        # ###################  Creating the Labeled DataLoader ###############
        # Create Dataloaders for labeled data by getting the labeled indices and
        # creating a random sub-sampler.  Then create the dataloader using this
        # sampler
        #
        #  Note: To change the transformer in the JPLDataset dataset, edit
        #      current_dataset.transform.  You can also edit the labels during
        #      loading by editing the  current_dataset.target_transform
        #
        #  Note: Another approach could be to inherit the JPLDataset class and
        #      override the __get_item__ function
        #
        #  Check out dataset.py for more information
        #     Notes: There could be NO unlabeled data and the algorithm has to be
        #         able to handle that case!
        #
        #         The min batch size here is so that the batch size isn't
        #         larger than the labeled/unlabeled dataset which causes the
        #         dataloader to hang

        labeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
            self.toolset["target_dataset"].get_labeled_indices()
        )

        labeled_dataloader = torch.utils.data.DataLoader(
            self.toolset["target_dataset"],
            sampler=labeled_sampler,
            batch_size=min(target_dataset.labeled_size,
                           int(self.config["batch_size"])
                           ),
            num_workers=int(self.config['num_workers']),
            collate_fn=target_dataset.collate_batch,
            drop_last=True,
        )

        # ###################  Creating the Unlabeled DataLoader ###############
        #  Same as labeled dataaset but for unlabeled indices
        #   Except skip if no unlabeled
        if self.toolset["target_dataset"].unlabeled_size > 0:
            unlabeled_sampler = torch.utils.data.sampler.SubsetRandomSampler(
                target_dataset.get_unlabeled_indices()
            )
            unlabeled_dataloader = torch.utils.data.DataLoader(
                target_dataset,
                sampler=unlabeled_sampler,
                batch_size=min(target_dataset.unlabeled_size,
                               int(self.config["batch_size"])
                               ),
                num_workers=int(self.config['num_workers']),
                drop_last=False,
            )


        # ##################### fine-tune/train your model ######################
        #  Here is where you will actual finetune the model
        #  You can use the pretrained model with self.model
        #  Make sure to set any persistent tensors to cpu with `tensor.cpu` command


        # sc_tr_dataset, sc_te_dataset = prepare_dataset(args.source, image_size, channels,
        #                                                path=args.data_root)
        sc_tr_dataset = self.source_dataset_train
        sc_te_dataset = self.source_dataset_test
        args = self.config

        # Todo: combine training dataset between source and target for labeled data
        sc_tr_loader = data.DataLoader(sc_tr_dataset, batch_size=args.batch_size, shuffle=True,
                                       num_workers=4)
        sc_te_loader = data.DataLoader(sc_te_dataset, batch_size=args.batch_size, shuffle=False,
                                       num_workers=4)
        tg_tr_dataset = target_dataset
        tg_te_dataset = self.eval_dataset

        tg_te_loader = data.DataLoader(tg_te_dataset, batch_size=args.batch_size, shuffle=False,
                                       num_workers=4)
        sstasks = parse_tasks(args, self.ext, sc_tr_dataset, sc_te_dataset, tg_tr_dataset, tg_te_dataset)

        criterion = nn.CrossEntropyLoss().cuda()
        parameters = list(self.net.parameters())
        for sstask in sstasks:
            parameters += list(sstask.head.parameters())
        optimizer = optim.SGD(parameters, lr=args.lr, momentum=0.9, weight_decay=5e-4)
        scheduler = torch.optim.lr_scheduler.MultiStepLR(
            optimizer, [args.milestone_1, args.milestone_2], gamma=0.1, last_epoch=-1)

        all_epoch_stats = []
        logging.info('==> Running..')
        for epoch in range(1, args.nepoch + 1):
            logging.debug(f"Source epoch {epoch}{args.nepoch} lr={optimizer.param_groups[0]['lr']}")
            logging.debug('Error (%)\t\tmmd\ttarget test\tsource test\tunsupervised test')

            epoch_stats = train(args, self.net, self.ext, sstasks,
                                criterion, optimizer, scheduler, sc_tr_loader, sc_te_loader, tg_te_loader)
            all_epoch_stats.append(epoch_stats)

    def inference(self, eval_dataset):
        """
        Inference is during the evaluation stage.  For this example, the
        task_network is trained in the train and adapt stage's code and
        is used here to create the predictions.  The indices are used to
        track the images/filenames for submitting the predictions back to
        JPL

        Args:
            eval_dataset (framework.dataset): unlabeled evaluation framework.  We need to predict the classes
                for each item in the dataset.  Example below.

        Returns:
            tuple(list[int],list[int]): preds and indices
                predicted category indices and image indices

        """
        # self.task_model.eval()
        # eval_dataloader = data.DataLoader(
        #     toolset["Dataset"],
        #     batch_size=int(self.arguments["batch_size"]),
        #     drop_last=False
        # )
        # preds = []
        # indices = []
        #
        # for imgs, inds in eval_dataloader:
        #     if self.cuda:
        #         imgs = imgs.cuda()
        #
        #     with torch.no_grad():
        #         preds_ = self.task_model(imgs)
        #
        #     preds += torch.argmax(preds_, dim=1).cpu().numpy().tolist()
        #     indices += inds.numpy().tolist()

        preds, indices = self.toolset["eval_dataset"].dummy_data(
            'image_classification')
        return preds, indices

    def get_dataloaders(self):
        # TODO:  Should we combine together unsupervised and supervised way
        pass
