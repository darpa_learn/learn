import scriptconfig as scfg


class Config(scfg.Config):
    """
    Default configuration for your algorithm

    Please add all of your default configuration parameters for your algorithm below
    with a description (help) on
        - what your parameter does
        - what changes when you use your parameter

    This is so that someone else can come and alter there parameters and know
    how this will affect the run of the algorithm

    """
    default = {
        'name': scfg.Value('uda',
                           help='name of the proposed method'),

        'batch_size': scfg.Value(128,
                                 help='batch size of network while running.  '
                                      'Increasing will do X and decrease will do Y'),

        'nepoch': scfg.Value(128,
                                 help='Total number of epochs for training'
                                      'This is the total amount of epochs'),

        'lr': scfg.Value(128,
                                 help='learning rate for   '
                                      'Increasing will do X and decrease will do Y'),

        'milestone_1': scfg.Value(5,
                                 help='The epoch number for the first time the lr will decrease  '
                                      'Increasing will delay the learning rate drop'),

        'milestone_2': scfg.Value(10,
                                 help='The epoch number for the second time the lr will decrease  '
                                      'Increasing will delay the learning rate drop'),

        'num_batches_per_test': scfg.Value(128,
                                 help='The size of the batch for testing'),

        'num_workers': scfg.Value(8,
                                 help='number of workers in the dataloader'),

        ############Self-Supervised tasks ####################################################

        'rotation': scfg.Value(True,
                               help='whether or not to do the rotation task'),

        'lr_rotation': scfg.Value(0.1,
                               help='learning rate for the rotation task'),

        'quadrant': scfg.Value(True,
                               help='whether or not to do the quadrant task'),

        'lr_quadrant': scfg.Value(0.1,
                               help='learning rate for the quadrant task'),

        'flip': scfg.Value(True,
                               help='whether or not to do the flip task'),

        'lr_flip': scfg.Value(0.1,
                               help='learning rate for the flip task'),
        ################################################################
        # Note, width and depth have been taken out of here since this should either be defined by the
        #     source model or in the code if it doesn't change.

    }
