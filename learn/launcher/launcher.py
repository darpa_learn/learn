"""Meta-configuration demonstration."""

import logging
import json
import os
from datetime import datetime
from pathlib import Path
from typing import Dict, Any, Tuple
from pkg_resources import iter_entry_points, DistributionNotFound
import torch

from learn.protocol.learn_protocol import Learn

from learn.protocol.localinterface import LocalInterface
from learn.protocol.jplinterface import JPLInterface
import learn.protocol as protocol_folder
import learn.algorithms as algorithms_folder
from learn.utils.wandb_utils import setup_wandb_run

import ubelt as ub

from omegaconf import OmegaConf

log = logging.getLogger(__name__)


def discoverable_plugins(entry_point: str = 'tinker') -> Dict[str, Any]:
    """ Fixture to replicate plugin discovery from framework.
    Looks for plugins within the environment that use the keyterm tinker
    """
    discovered_plugins = {}
    for entry_point in iter_entry_points(entry_point):
        log.debug(f"Loading Plugin {entry_point.name}")
        try:
            ep = entry_point.load()
            discovered_plugins[entry_point.name] = ep
        except (DistributionNotFound, ImportError):
            log.exception(f"Plugin {entry_point.name} not found")
    return discovered_plugins


def add_gpu_to_config(cfg: dict):
    """
    Adds the gpu device to the cfg.

    If the environmental variable 'LWLL_TA1_GPUS' is specified, use that.
    Else, if there is a value in the cfg, keep that.
    Otherwise, just take the available devices from torch
    """
    gpu_env_val = os.environ.get("LWLL_TA1_GPUS")
    # if env variable is set, use this
    if gpu_env_val is not None:
        gpu_env_val = gpu_env_val.strip()
        if gpu_env_val == "all":
            cfg['problem']['device'] = list(range(torch.cuda.device_count()))
        else:
            cfg['problem']['device'] = list(map(int, gpu_env_val.split(" ")))
    # if no env var, then use the cfg
    elif 'device' in cfg['problem']:            
        if cfg['problem']['device'] == "all":
            cfg['problem']['device'] = list(range(torch.cuda.device_count()))
    # no value set, just get all environemntal variables
    else:
        cfg['problem']['device'] = list(range(torch.cuda.device_count()))

class LaunchProtocol(object):
    """A protocol demonstrating how meta-configurations work."""

    def run_protocol(self, config: Dict[str, Any], extra_plugins: Dict = dict()) -> None:
        """Run the protocol by printout out the config.

        Args:
            config: passed in uses 3 parameters to control the launching of the protocols
            - protocol: either 'ond' or 'condda' to define which protocol to run
            - harness:  either 'local' or 'par' to define which harness to use
            - workdir: a directory to save all the information from the run including
                - Config
                - Output of algorithm
            extra_plugins:  Dict of plugins to append to the plugins detected so that you can pass an
                algorithm direct in the function call like for ipython where you defined it in the
                code before calling this function.

        """
        config = OmegaConf.to_container(config, resolve = True)
        harness = config['harness']
        del config['harness']

        # add gpu to config
        add_gpu_to_config(config)
        
        log.info(f'Config: \n{json.dumps(config, indent=4)}')

        harnness_config_path = Path(protocol_folder.__file__).parent
        if harness == 'local':
            log.info('Loading Local Harness')
            harness = LocalInterface('configuration.json', harnness_config_path)
            #harness.result_directory = config['detector_config']['csv_folder']
            #harness.file_provider.results_folder = config['detector_config']['csv_folder']

            # Setup W&B for local harness only
            setup_wandb_run(config)

        elif harness == 'jpl':
            log.info('Loading JPL Harness')
            harness = JPLInterface('configuration.json', harnness_config_path)
            #harness.folder = config['detector_config']['csv_folder']

            setup_wandb_run(config)
        else:
            raise AttributeError(f'Valid harnesses "local" or "jpl".  '
                                 f'Given harness "{harness}" ')

        # Get the plugins
        plugins = ub.dict_union(discoverable_plugins('tinker'), extra_plugins)

        log.debug('Plugins found:')
        log.debug(plugins)

        # Load the protocol

        run_protocol = Learn(discovered_plugins=plugins,
                           algorithms_directory=str(Path(algorithms_folder.__file__).parent),
                           harness=harness,
                           config_file=config)#config_filepath)

        # Run the protocol
        print("launcher ",os.getcwd())
        run_protocol.run_protocol()
        log.info('Protocol Finished')
