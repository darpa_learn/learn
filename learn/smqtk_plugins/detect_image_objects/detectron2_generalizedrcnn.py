import detectron2.data.transforms as T

from learn.smqtk_plugins.detect_image_objects.detectron2_base import Detectron2Base


class Detectron2GeneralizedRCNN(Detectron2Base):
    """
    Concrete plugin intended to be used be models that extend the
    `detectron2.modeling.GeneralizedRCNN` base architecture.

    This defines basic image preprocessing that will resize the image into the
    configured range. This follows in line with detectron2's `DefaultPredictor`
    [1] functionality.

    [1] https://detectron2.readthedocs.io/en/latest/_modules/detectron2/engine/defaults.html#DefaultPredictor
    """

    def _get_augmentation(self) -> T.Augmentation:
        cfg = self._cfg
        return T.AugmentationList([
            T.ResizeShortestEdge(
                [cfg.INPUT.MIN_SIZE_TEST, cfg.INPUT.MIN_SIZE_TEST],
                cfg.INPUT.MAX_SIZE_TEST
            )
            # GeneralizedRCNN incorporates pixel mean/stddev normalization
            # in the forward pass.
        ])
