import abc
import logging
from typing import cast
from typing import Any, Dict, Generic, Hashable, Iterable, Iterator, List
from typing import Optional, Tuple, TypeVar, Union
from typing_extensions import Protocol, runtime_checkable
import warnings

from detectron2.checkpoint import DetectionCheckpointer     # type: ignore
from detectron2.config import get_cfg                       # type: ignore
from detectron2.data import MetadataCatalog                 # type: ignore
import detectron2.data.transforms as T                      # type: ignore
from detectron2.data.samplers import InferenceSampler       # type: ignore
from detectron2.modeling import build_model                 # type: ignore
import numpy as np
import torch.nn
from torch.utils.data import DataLoader, Dataset, IterableDataset

from smqtk_detection import DetectImageObjects
from smqtk_detection.utils.bbox import AxisAlignedBoundingBox


LOG = logging.getLogger(__name__)
T_co = TypeVar("T_co", covariant=True)


@runtime_checkable
class SequenceLike (Iterable[T_co], Protocol):
    """ Simple protocol for size-bounded random-access sequences. """
    def __len__(self) -> int: ...
    def __getitem__(self, index: int) -> T_co: ...


class Detectron2Base(DetectImageObjects):
    """
    Plugin base wrapping the loading and application of detectron2 models
    on images to yield object detections and classifications.

    It is expected that classes will be derived from this base that concretely
    defines the data augmentation to appropriate transform input imagery for
    the configured network.

    This plugin expects input image matrices to be in the dimension format
    ``[H x W]`` or ``[H x W x C]``. It is *not* the case that all input imagery
    must have matching shape values.

    This plugin attempts to be intelligent in how it handles different kinds of
    iterable inputs. When given a ``Dataset`` or countable sequence (has
    ``__len__`` and ``__getitem__``), any valid value may be provided to
    ``num_workers`` as ``DataLoader`` might accept.
    However, when the input to ``detect_objects`` is an uncountable iterable,
    like a generic generator or stream source, the ``num_workers`` value should
    usually be either 0 or 1.
    This is due to the input iterable being copied for each worker, which may
    not result in desired behavior.
    For example:
      * when the input iterable involves non-trivial operations per yield,
        these operations are duplicated for each copy of the iterable as
        traversed on each worker, probably resulting in excessive use of
        resources. E.g. if the iterable is loading images from disk, each
        worker is loading every image as it traverses their copy of the
        iterable, even though each worker may only operate on a minority of
        elements traversed.
      * when the input iterable yields real-time data or is otherwise **not**
        idempotent, like an iterable that yields images from a webcam stream,
        each traversal of a copy of that iterable will produce different values
        for equivalent "indices" since what is returned is conditional on when
        ``next()`` is requested. Since iterators are copied to N separate
        workers, each making independent next requests, the e.g. 64th
        ``next()`` for each worker might yield a different image matrix.

    :param detectron_config: Filesystem path to the detectron2 model
        configuration to use for model initialization.
    :param load_device: The device to load the model onto for execution.
    :param batch_size: Optionally provide prediction batch size override. If
        set, we will override the configuration's ``SOLVER.IMS_PER_BATCH``
        parameter to this integer value. Otherwise, we will use the batch size
        value set to that parameter.
    :param weights_uri: Optional reference to the model weights file to use
        instead of that referenced in the detectron configuration file.
        If not provided, we will
    :param model_lazy_load: If the model should be lazy-loaded on the first
        inference request (``True`` value), or if we should load the model up-
        front (``False`` value).
    :param num_workers: The number of workers to use for data loading. When set
        to ``None`` (the default) we will pull from the detectron config,
        otherwise we will obey this value. See torch ``DataLoader`` for
        ``num_workers`` value meanings.
    """

    def __init__(
        self,
        detectron_config: str,
        load_device: Union[int, str] = "cpu",
        batch_size: Optional[int] = None,
        weights_uri: Optional[str] = None,
        model_lazy_load: bool = True,
        num_workers: Optional[int] = None,
    ):
        self._detectron_config = detectron_config
        self._load_device_prim = load_device  # int/str reference only
        self._batch_size = batch_size
        self._weights_uri = weights_uri
        self._model_lazy_load = model_lazy_load
        self._num_workers = num_workers

        # Load detectron config on top of global default structure
        cfg = get_cfg()
        cfg.merge_from_file(detectron_config)
        if batch_size is not None:
            LOG.info(f"Overriding config to use a batch size of: "
                     f"{batch_size}")
            cfg.SOLVER.IMS_PER_BATCH = batch_size
        if weights_uri is not None:
            LOG.info(f"Overriding config to use model weights at: "
                     f"{weights_uri}")
            cfg.MODEL.WEIGHTS = weights_uri
        if num_workers is not None:
            LOG.info(f"Overriding config to use data-loader workers: "
                     f"{num_workers}")
            cfg.DATALOADER.NUM_WORKERS = num_workers
        self._cfg = cfg

        # Model prediction class index-to-label transformation is dictated by
        # MetadataCatalog mappings. We can draw on the `cfg.DATASETS.TEST`
        # configuration property. However, not sure how to handle if there are
        # more than one train dataset, or if the dataset name does not resolve
        # to anything. We warn here if there are >1 and also let the
        # AttributeError that is raised in that case to pass through.
        test_datasets = cfg.DATASETS.TEST
        if len(test_datasets) > 1:
            warnings.warn(
                "Input detectron model configuration specifies multiple test "
                "datasets. We will only use the first to assign class labels "
                "to model predictions.",
                RuntimeWarning
            )
        self._label_vec = MetadataCatalog.get(test_datasets[0]).thing_classes

        self._model_device = torch.device(load_device)

        self._model: Optional[torch.nn.Module] = None
        if not model_lazy_load:
            self._lazy_load_model()

    @abc.abstractmethod
    def _get_augmentation(self) -> T.Augmentation:
        """
        Create and return the augmentation instance that will appropriately
        transform an input image for this network.
        """

    def _lazy_load_model(self) -> torch.nn.Module:
        """
        Actually initialize the model and set the weights, storing on the
        requested device. If the model is already initialized, we simply return
        it. This method is idempotent and should always return the same model
        instance once loaded.

        If this fails to initialize the model, then nothing is set to the class
        and ``None`` is returned (reflective of the set model state).
        """
        if self._model is None:
            # Based on model building by `detectron2.engine.defaults.DefaultPredictor`
            # We don't actually use that here due to it in-lining input
            # preprocessing with module invocation as well as it not being
            # vectorized.
            cfg = self._cfg
            model = build_model(cfg)
            model.to(self._model_device).eval()
            checkpointer = DetectionCheckpointer(model)
            checkpointer.load(cfg.MODEL.WEIGHTS)
            self._model = model
        return self._model

    def detect_objects(
        self,
        img_iter: Iterable[np.ndarray]
    ) -> Iterable[Iterable[Tuple[AxisAlignedBoundingBox, Dict[Hashable, float]]]]:
        # Wrap image array iterable into a torch dataset-like
        # ?? detectron2.data.build.build_detection_test_loader
        cfg = self._cfg
        model = self._lazy_load_model()
        augmentation = self._get_augmentation()
        batch_size = cfg.SOLVER.IMS_PER_BATCH

        # Sampler logic pulled from `build_detection_test_loader` but without
        # the batch-size hard-coding.
        dataset: Union[AugmentImageSequence, AugmentImageIterable]
        if isinstance(img_iter, SequenceLike):
            # The input is effectively a "Sequence" type that we can wrap in
            # `AugmentImageSequence`.
            dataset = AugmentImageSequence(img_iter, augmentation)
        else:
            # Have to treat as real, general iterable/iterator
            LOG.debug("Given an uncountable iterable, using "
                      "AugmentImageIterable.")
            dataset = AugmentImageIterable(img_iter, augmentation, batch_size)

        data_loader = make_d2_dataloader(dataset, batch_size, cfg.DATALOADER.NUM_WORKERS)

        with torch.no_grad():
            for batch_input in data_loader:
                batch_output = self._forward(model, batch_input)
                # For each output, yield an iteration converting outputs into
                # the interface-defined data-types
                for output_dict in batch_output:
                    yield self._iterate_output(output_dict)

    def _forward(self, model: torch.nn.Module, batch_inputs: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
        """
        Method encapsulating running a forward pass on a model given some batch
        inputs.

        This is a separate method to allow for potential subclasses to override
        this.

        :param model: Torch module as loaded by detectron2 to perform forward
            passes with.
        :param batch_inputs: Detectron2 formatted batch inputs. It can be
            expected that this will follow the format described by [1] and
            contain the "image", "height" and "width" keys.

        Returns:
            Sequence of outputs for each batch input. Each item in this output
            is expected to be interpreted by ``_iterate_output``.

        [1]: https://detectron2.readthedocs.io/en/latest/tutorials/models.html#model-input-format
        """
        return model(batch_inputs)

    def _iterate_output(
        self, single_output: Dict[str, Any]
    ) -> Iterable[Tuple[AxisAlignedBoundingBox, Dict[Hashable, float]]]:
        """
        Given the model's output for a single image's input, yield out a number
        of ``(AxisAlignedBoundingBox, dict)`` pairs representing detections.

        :param single_output: Detectron2 formatted output dictionary. It can be
            expected that this will follow the format described by [1].

        [1]: https://detectron2.readthedocs.io/en/latest/tutorials/models.html#model-output-format
        """
        cpu_instances = single_output['instances'].to('cpu')
        for box, cls_idx, score in zip(
            cpu_instances.get('pred_boxes'),
            cpu_instances.get('pred_classes'),
            cpu_instances.get('scores')
        ):
            yield (
                AxisAlignedBoundingBox(box[:2], box[2:]),
                {self._label_vec[cls_idx]: float(score)}
            )

    def get_config(self) -> Dict[str, Any]:
        return {
            "detectron_config": self._detectron_config,
            "load_device": self._load_device_prim,
            "batch_size": self._batch_size,
            "weights_uri": self._weights_uri,
            "model_lazy_load": self._model_lazy_load,
            "num_workers": self._num_workers,
        }


def make_d2_dataloader(
    dataset: Union[Dataset, IterableDataset],
    batch_size: int,
    num_workers: int = 0
) -> DataLoader:
    """
    Common logic for dataloader creation given a dataset that will yield
    detectron2 dictionary inputs for inference.

    This is a simple version of ``detectron2.data.build.build_detection_test_loader``
    but with the ability to actually set the batch size to something other than
    one (1). A future version will allow the setting of ``batch_size``, at which
    time the use of this function may be deprecated in favor of the detectron2
    provided function.

    Args:
        dataset: Dataset-like input for the dataloader.
        batch_size: batch size.
        num_workers: number of workers.

    Returns:
        New ``DataLoader`` instance.
    """
    # We know that upstream detectron2 has an updated parameterization for
    # `build_detection_test_loader` that would deprecate the need for this.
    #   https://detectron2.readthedocs.io/en/latest/_modules/detectron2/data/build.html#build_detection_test_loader
    import inspect
    from detectron2.data.build import build_detection_test_loader  # type: ignore
    if 'batch_size' in inspect.signature(build_detection_test_loader).parameters:
        warnings.warn(
            "`batch_size` now a parameter of `build_detection_test_loader`. "
            "That function should now be use instead of this.",
            DeprecationWarning
        )

    # from detectron2.data.build import build_detection_test_loader
    sampler: Optional[InferenceSampler]
    if isinstance(dataset, IterableDataset):
        sampler = None
    else:
        # Assuming that datasets have __len__ defined, which I think is
        # effectively required...
        sampler = InferenceSampler(len(dataset))
    return DataLoader(
        dataset,
        batch_size=batch_size,
        sampler=sampler,
        num_workers=num_workers,
        drop_last=False,
        collate_fn=_trivial_batch_collator
    )


def _trivial_batch_collator(batch: Any) -> Any:
    """
    A batch collator that does nothing.
    """
    return batch


def _to_tensor(np_image: np.ndarray) -> torch.Tensor:
    """
    Common transform to go from ``[H x W [x C]]`` numpy image matrix to a
    ``[[C x] x H x W]`` torch float32 tensor image. Image pixel scale is left
    alone.
    """
    aug_image = np_image.astype(np.float32)
    if aug_image.ndim == 3:
        aug_image = aug_image.transpose([2, 0, 1])
    return torch.as_tensor(aug_image)


def _aug_one_image(image: np.ndarray, aug: T.Augmentation) -> Dict[str, Union[torch.Tensor, int]]:
    """
    Common augmentation operation for detectron2 inference passes, performed by
    datasets defined below.

    Args:
        image: Image matrix to be augmented
        aug: Augmentation to be performed on the input image.

    Returns:
        Detectron 2 input dictionary with the augmented image tensor and
        original image height and width attributes.
    """
    # sorta replicating detectron2.engine.defaults.DefaultPredictor use of
    # input formatting, which passes along original image height and width
    height, width = image.shape[:2]

    # apply aug. `aug_input` will now contain the changed image matrix after
    # `aug` call.
    aug_input = T.AugInput(image=image)
    aug(aug_input)

    # convert from numpy-common format to torch.Tensor-common format.
    aug_image = _to_tensor(aug_input.image)

    return {
        "image": aug_image,
        "height": height,
        "width": width,
    }


class AugmentImageSequence(Dataset):
    """
    Simple dataset for a sequence of images.

    The given augmentation instance will be applied to imagery before they are
    returned when this dataset is accessed by index.

    :param sequence: Sequence of image matrices to yield via index access.
    :param augmentation: The augmentation to apply when accessing an item.
    """

    def __init__(self, sequence: SequenceLike[np.ndarray], augmentation: T.Augmentation):
        self._sequence = sequence
        self._aug = augmentation

    def __len__(self) -> int:
        return len(self._sequence)

    def __getitem__(self, idx: int) -> Dict[str, Any]:
        image = self._sequence[idx]
        return _aug_one_image(image, self._aug)


# noinspection PyAbstractClass
class AugmentImageIterable(IterableDataset):
    """
    Simple dataset for an arbitrary sized iterable of image matrices.

    Pytorch ``DataLoader``, when given an ``IterableDataset``, does *not*
    request work in a round-robin style, but instead requests ``batch_size``
    outputs from a worker before requesting the same quantity from the next
    worker, as empirically observed.
    When in "parallel" mode, this iterator, for each worker, will try to yield
    batch-size consecutive items. This should align with the ``DataLoader``
    logic such that yielded batches, when concatenated together, result in an
    input order equivalent to the source iterable.
    This requires knowing the batch size that will be use with the
    ``DataLoader``.

    :param iterable: Iterable of image matrices.
    :param augmentation: The augmentation to apply when accessing an item.
    :param batch_size: Batch size this data-set will be loaded with. This is
        very important for when ``DataLoader`` num_workers is ``>1``.
    """

    def __init__(self, iterable: Iterable[np.ndarray],
                 augmentation: T.Augmentation,
                 batch_size: int):
        self._iterable = iterable
        self._iterator = iter(iterable)
        self._aug = augmentation
        self._batch_size = batch_size
        self._next_i = 0  # For multiple workers, see exception below.

    def __iter__(self) -> Iterator:
        # return self
        aug = self._aug
        batch_size = self._batch_size

        worker_info = torch.utils.data.get_worker_info()
        worker_id = worker_info.id if worker_info else None
        worker_count = worker_info.num_workers if worker_info else None
        if worker_info is not None and worker_count > 1:
            warnings.warn(
                f"Using an iterable dataset and more than 1 worker "
                f"(num_workers={worker_info.num_workers}). "
                f"Double-check that it is OK for the iterable to be copied "
                f"{worker_info.num_workers} times (one for each worker), may "
                f"be iterated independently and does not incur undue "
                f"processing cost when skipping past iterated elements.",
                UserWarning
            )

        for i, image in enumerate(self._iterable):
            # worker things -- "my" batch when
            # `i // batch_size) % num_workers == worker_id`, otherwise skip
            # that `i`.
            if (
                worker_info is None or
                (i // batch_size) % worker_count == worker_id
            ):
                LOG.debug(f"i={i}, batch_size={batch_size}, worker={worker_info}")
                item = _aug_one_image(image, aug)
                yield item
