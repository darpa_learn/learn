from learn.deprecated_tinker.baseprotocol import BaseProtocol
import logging

log = logging.getLogger(__name__)
import traceback
import tempfile
import os
import ubelt
import json

import torch

torch.multiprocessing.set_sharing_strategy("file_system")

# Note: doing full include to allow for documentation.  This isn't necessary
#     for just running.
from learn.protocol.lwll_env_var import update_config_with_env_var
from learn.utils.memory import garbage_collection_cuda
from learn.utils.wandb_utils import WANDB_INFO
from learn.utils.jpl_logger import jpl_log, JPLHandler

from hydra import utils as hydra_utils
import wandb

class Learn(BaseProtocol):
    """
    The protocol class should derive from two source classes. The first is the
    BaseProtocol. This provides the self.get_algorithm function for locating and
    creating an algorithm object for you given only the filename of the file
    containing the algorithm. To use the functions of that algorithm, call the
    execute() function. This call performs two critical actions that are not
    available by directly calling the algorithms functions. First, it establishes
    an indirect call path that allows the automated tools to generate and
    annotate the adapter and template files for use when creating brand new
    algorithms.  Second, it allows the deconstruction of the toolset dict(), and
    automatically stores the toolset for local use within the algorithm itself.

    The second class that the protocol class should derive from is the desired
    back-end data and reporting structure. One such back-end is the
    JPLInterface which provides access to the JPL T&E server for the learn
    project (and hopefully sail-on as well). Another useful back-end is the
    LocalInterface which provides similar access as the JPLInterface using
    local data only. The interface mimics the JPLInterface, but works entirely
    within the framework, and requires no external server to function.

    Only Kitware should be modifying this function.

    """

    def __init__(self, discovered_plugins, algorithms_directory, harness, config_file):
        BaseProtocol.__init__(
            self, discovered_plugins, algorithms_directory, harness, config_file
        )

        self.config = config_file
        self.config = update_config_with_env_var(self.config)
        # make this path absolute, since working directory is moved when
        # working in hydra
        self.config["problem"]["local_task_folder_path"] = os.path.join(
            hydra_utils.get_original_cwd(),
            self.config["problem"]["local_task_folder_path"],
        )
        # log.debug(self.config.dumps('yaml'))
        self.toolset["protocol_config"] = self.config
        self.test_harness.update_parameters(self.config["problem"])
        self.base_dataset = None

    def run_protocol(self):
        """run_protocol is called from the framework and represents that initial
        execution point of the protocol.
        """

        task_ids = self.test_harness.get_task_ids()
        tasks_to_run = self.config["problem"]["tasks_to_run"]
        log.info("List of Potential Tasks:")

        log.info(f"{task_ids}\n")

        for task in task_ids:
            log.info(f"\n{task}")
            log.info(self.test_harness.get_problem_metadata(task))

        if len(tasks_to_run) < 1:
            log.info("Running All Tasks")
            tasks_to_run = task_ids
        else:
            log.info("Running following tasks:")
        log.info(tasks_to_run)

        if not isinstance(tasks_to_run, list):
            assert isinstance(tasks_to_run, str)
            tasks_to_run = [tasks_to_run]

        for task in tasks_to_run:
            try:
                if WANDB_INFO.run_wandb:
                    wandb.config.update(
                        {"task_info": self.test_harness.get_problem_metadata(task)}
                    )
                self.run_task(task)
            except Exception as e:
                log.exception(f"test: {traceback.format_exc()}")
                raise e
            finally:
                self.test_harness.terminate_session()

    def run_task(self, task_id: str) -> None:
        """

        Args:
            task_id (str): task id for which you want to run.

        Returns:

        """
        # Getting rid of all the self here to clean up the code.
        config = self.config
        toolset = self.toolset
        interface = self.test_harness
        get_algorithm = self.get_algorithm
        interface.initialize_session(task_id, config)

        if self.config['problem']['use_JPL_logger']:
            log.info('Setting Up JPL Logger')
            hdlr_JPL = JPLHandler(checkpoint=0)
            log.addHandler(hdlr_JPL)

            log.info('Config')
            log.info(json.dumps(config, indent=4))

            log.info('Problem Metadata')
            log.info(interface.get_problem_metadata())

            log.info('Current Status')
            log.info(interface.get_current_status())
            
        print("learn_proto ",os.getcwd())
        toolset["problem_type"] = interface.metadata["problem_type"]
        (
            toolset["whitelist_datasets"],
            toolset["whitelist"],
        ) = interface.get_whitelist_datasets_jpl(
            config["problem"]["dataset_dir"],
            config["problem"]["add_dataset_to_whitelist"],
        )
        domain_select_algo = get_algorithm(
            config["domain_network_selector"]["name"], toolset
        )
        algo_select_algo = get_algorithm(config["algo_selector"]["name"], toolset)
        if config["self_supervision_pretrain"]["name"]:
            pretrain_src_tgt_algo = get_algorithm(
                config["self_supervision_pretrain"]["name"], toolset
            )
        else:
            pretrain_src_tgt_algo = None

        if config['test_time_adaptation']['name']:
            test_time_adapt_algo = get_algorithm(
                config['test_time_adaptation']['name'], toolset)
        else:
            test_time_adapt_algo = None
        stages = interface.get_stages()
        with tempfile.TemporaryDirectory() as temp_dir:

            for stage in stages:
                if self.config['problem']['use_JPL_logger']:
                    log.removeHandler(hdlr_JPL)
                    hdlr_JPL = JPLHandler(checkpoint=0)
                    log.addHandler(hdlr_JPL)

                log.info(f"\n\n*********** Starting Stage {stage} ***************\n")
                interface.start_stage(stage)
                toolset["target_dataset"] = interface.get_dataset_jpl(stage,
                    "train", seed_filepath=config['problem']['seed_filepath'])
                toolset["eval_dataset"] = interface.get_dataset_jpl(stage, "eval")
                toolset["stage"] = stage

                # Get network and source dataset for the stage.
                source_network, source_dataset = domain_select_algo.execute(
                    toolset, "SelectNetworkAndDataset"
                )

                target_equals_source = False
                if stage == "adapt" and self.toolset["protocol_config"]["domain_network_selector"]["params"]["use_first_stage_dataset"]:
                    # use first stage dataset as source dataset for adapt stage
                    
                    if self.base_dataset is not None:
                        source_cats = set(list(self.base_dataset.categories))
                        target_cats = set(list(toolset["target_dataset"].categories))
                        target_equals_source = set(target_cats) == set(source_cats)   # susbet or do they have to be equal????

                        if target_equals_source:
                            source_dataset = self.base_dataset
                            # source_cats = set(list(source_dataset.categories))
                            # target_cats = set(list(toolset["target_dataset"].categories))
                            # num_target_only_cats = len(target_cats) - len(target_cats.intersection(source_cats))

                toolset["source_dataset"] = source_dataset
                toolset["source_network"] = source_network
                toolset["temp_model_directory"] = os.path.join(temp_dir, stage)
                ubelt.ensuredir(toolset["temp_model_directory"])

                if stage == "base":
                    self.base_dataset = toolset["target_dataset"]

                checkpoints, is_uda = interface.get_budget_checkpoints(stage, toolset["target_dataset"])
                if is_uda:
                    if "do_uda" in toolset["protocol_config"]["problem"]:
                        is_uda = toolset["protocol_config"]["problem"]["do_uda"]
                        if not is_uda:
                            checkpoints = checkpoints[1:]

                # if specified by `pretrain_on_source_target_strategy` config setting
                # perform additional [self-supervised] pretraining using
                # source, source+target, source then target, or target
                # if pretrain_on_source_target_strategy=None, then
                # these two steps will do nothing
                # for now, don't do HPT if source dataset is imagenet
                if pretrain_src_tgt_algo and not is_uda:
                    skip_pretrain = False
                    if (
                        "HPT/pretrain_algo.py"
                        == config["self_supervision_pretrain"]["name"]
                    ):
                        # is hpt, check if should use with imagenet
                        if "imagenet" in source_dataset.name:
                            if not config["self_supervision_pretrain"]["params"][
                                "use_with_imagenet"
                            ]:
                                skip_pretrain = True
                    if not skip_pretrain:
                        pretrain_src_tgt_algo.execute(toolset, "Initialize")
                        pretrain_src_tgt_algo.execute(toolset, "Pretrain")

                #### hacky way of dealing with PACMAC / MMCLS tradeoff
                if toolset["protocol_config"]['image_classifier']["name"] == "MMCLS/image_classification.py" or toolset["protocol_config"]['image_classifier']["name"] == "PACMAC/image_classification_pacmac.py":
                    if not target_equals_source:
                        toolset["protocol_config"]['image_classifier']["name"] = "MMCLS/image_classification.py"
                        toolset["protocol_config"]['image_classifier']["params"] = toolset["protocol_config"]['image_classifier']["mmcls_params"]["params"]
                    else:
                        toolset["protocol_config"]['image_classifier']["name"] = "PACMAC/image_classification_pacmac.py"
                        toolset["protocol_config"]['image_classifier']["params"] = toolset["protocol_config"]['image_classifier']["pacmac_params"]["params"]

                # Select which algorithms to use
                query_algo_id, adapt_algo_id = algo_select_algo.execute(
                    toolset, "SelectAlgorithms"
                )

                query_algo = get_algorithm(query_algo_id, toolset)
                toolset["query_fn"] = interface.get_more_labels
                query_algo.execute(toolset, "Initialize")

                # Random init too for last checkpoint or skipping
                random_query_algo = get_algorithm('templateQueryAlgo/query_algo_rand.py', toolset)
                random_query_algo.execute(toolset, "Initialize")

                # check if we are supposed to use the same source for both
                # query and adapt. if not, then init the adapt separately.
                if adapt_algo_id == query_algo_id:
                    adapt_algo = query_algo
                else:
                    adapt_algo = get_algorithm(adapt_algo_id, toolset)
                    adapt_algo.execute(toolset, "Initialize")

                if test_time_adapt_algo:
                    test_time_adapt_algo.execute(toolset, "Initialize")

                for it_cp, cp in enumerate(checkpoints):
                    # start at -1 when UDA and negative checkpoint iteration means UDA
                    if is_uda:
                        it_cp -= 1
                    if self.config['problem']['use_JPL_logger']:
                        log.removeHandler(hdlr_JPL)
                        hdlr_JPL = JPLHandler(checkpoint=it_cp)
                        log.addHandler(hdlr_JPL)

                    log.info(
                        f"\n\n*********** Starting Checkpoing {it_cp}: for {cp} labels  ***************\n"
                    )
                    skipping_checkpoint = interface.start_checkpoint(stage, toolset["target_dataset"], it_cp,
                                                      config['problem']['checkpoints_to_skip'])
                    toolset["budget"] = interface.get_remaining_budget()
                    toolset['ckpt'] = it_cp

                    if stage == "base":
                        if "skip_base" in toolset["protocol_config"]["problem"] and toolset["protocol_config"]["problem"]["skip_base"]:
                            if it_cp < 7:
                                skipping_checkpoint = True

                    if not skipping_checkpoint:
                        # call the query_algo to update the dataset with new labels
                        if toolset["budget"] > 0 and 7 > it_cp >= 0:
                            query_algo.execute(toolset, "SelectAndLabelData")
                        elif toolset["budget"] > 0 and 7 == it_cp:
                            log.info("Doing Random Query algorithm/Active learning since last checkpoint and "
                                     "no choice")
                            random_query_algo.execute(toolset, "SelectAndLabelData")
                        else:
                            log.info("Skipping Query/Active Learning algorithm")
                        
                        if it_cp == 7 and toolset["protocol_config"]['image_classifier']["name"] == "PACMAC/image_classification_pacmac.py":
                            # switch to mmcls
                            toolset["protocol_config"]['image_classifier']["name"] = "MMCLS/image_classification.py"
                            toolset["protocol_config"]['image_classifier']["params"] = toolset["protocol_config"]['image_classifier']["mmcls_params"]["params"]
                            query_algo_id, adapt_algo_id = algo_select_algo.execute(
                                toolset, "SelectAlgorithms"
                            )
                            adapt_algo = get_algorithm(adapt_algo_id, toolset)
                            adapt_algo.execute(toolset, "Initialize")

                        # this is probably not a good way to do this - image_classifier and
                        # object_detector are always present
                        if "image_classifier" in toolset["protocol_config"]:
                            if "hyperparams" in toolset["protocol_config"]["image_classifier"]:
                                if toolset["protocol_config"]["image_classifier"]["hyperparams"]["tune"]:
                                    adapt_algo.execute(toolset, "HyperparameterSearch")
                        
                        if "object_detector" in toolset['protocol_config']:
                            if "hyperparams" in toolset['protocol_config']['object_detector']:
                                if toolset['protocol_config']['object_detector']["hyperparams"]["tune"]:
                                    adapt_algo.execute(toolset, "HyperparameterSearch")



                        # call the adapt_algo to update the model to incorporate the new labels
                        # (TODO): if using MAL as active learning algorithm, can optionally initialize
                        # the network backbone during adaptation with MAL's feature encoder
                        # (can be accessed with query_algo.mal.F)
                        # if use_mal_encoder:
                        #     pass
                        toolset['pred_network'] = adapt_algo.execute(toolset, "DomainAdaptTraining")

                        # call test time adaptation if have it
                        if test_time_adapt_algo:
                            test_time_adapt_algo.execute(toolset, "DomainAdaptTraining")
                            results = test_time_adapt_algo.execute(toolset, "EvaluateOnTestDataSet")
                        else:
                            results = adapt_algo.execute(toolset, "EvaluateOnTestDataSet")
                            print("GOT RESULTS")
                        print("****** POSTING RESULTS")
                        interface.post_results(stage, toolset["eval_dataset"], results, it_cp)
                    else:
                        log.warning(f'Skipping Checkpoint {it_cp} and doing random querying/active learning')
                        if it_cp >= 4:
                            random_query_algo.execute(toolset, "SelectAndLabelData")
                        if interface.set_skip_checkpoint():
                            log.warning(f'Skipping Checkpoint {it_cp}')
                        else:
                            log.error(f'Error Skipping Checkpoint {it_cp}')

                interface.finish_stage(stage, toolset)
                garbage_collection_cuda()
            interface.terminate_session()
