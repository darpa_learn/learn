"""JPL Interface."""
import requests
import json
from learn.deprecated_tinker.harness import Harness
from learn.utils.dataset import (
    ImageClassificationDataset, 
    ObjectDetectionDataset,
#    VideoClassificationDataset,
    MachineTranslationDataset
)
from learn.algorithms.CoMix.dataset_slowfast import VideoClassificationDataset_SlowFast as VideoClassificationDataset
from pathlib import Path
import pandas as pd
import scriptconfig as scfg
import logging
log = logging.getLogger(__name__)
from typing import List, Union
from tenacity import retry, stop_after_attempt, wait_fixed, retry_if_exception_type, after_log, before_sleep_log
from requests.exceptions import HTTPError
import ubelt as ub
import os
from learn.utils.wandb_utils import WANDB_INFO
import wandb
import copy

class JPLInterface(Harness):
    """
    JPL Interface.

    This interface handles the backend for integration with the JPL test
    harness.  It handles all the rest calls.
    """

    def __init__(self, json_configuration_file: str, interface_config_path: str):
        """
        Construct the JPL problem and get list of tasks.

        For reference, these are the keys for Berkeley Team:
            apikey = "adab5090-39a9-47f3-9fc2-3d65e0fee9a2"
            url = "https://api-staging.lollllz.com/"

        Args will be moved to config.
        """
        super().__init__(json_configuration_file, interface_config_path)

        self.data_type = None
        self.api_key = None
        self.task_id = None
        self.stage_id = None
        self.sessiontoken = None
        self.status = {}
        self.metadata = {}

        # Change url during evaluation on DMC servers (changes paths to eval datasets)
        self.evaluate = False

        self.stagenames = ["base", "adapt"]
        self.checkpoint_num = 0
        self.dataset_dir = None
        self.api_key = None
        self.url = None

        self.current_dataset = None
        self.categories = None

    def update_parameters(self, protocol_config):
        log.info('Config File Parameters:')
        log.info(protocol_config)
        self.dataset_dir = protocol_config['dataset_dir']
        self.api_key = protocol_config['apikey']
        self.url = protocol_config['url']
        if self.url[-1] == "/":
            self.url = self.url[:-1]
        self.data_type = protocol_config['data_type']
        self.govteam_secret = protocol_config['govteam_secret']
        self.dataset_type = protocol_config['dataset_type']
        self.headers = {"user_secret": self.api_key,
                        "govteam_secret": self.govteam_secret}

        # specify which GPU(s) to be used
        if isinstance(protocol_config['device'], int):
            os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
            os.environ["CUDA_VISIBLE_DEVICES"] = str(protocol_config['device'])
            protocol_config['device'] = 0
        elif ub.iterable(protocol_config['device']):
            os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
            os.environ["CUDA_VISIBLE_DEVICES"] = ','.join(map(str, protocol_config['device']))
            protocol_config['device'] = list(range(0, len(protocol_config['device'])))

    @retry(stop=stop_after_attempt(5), wait=wait_fixed(2),
            reraise=True, before_sleep=before_sleep_log(logging.getLogger(__name__), logging.INFO))
    def initialize_session(self, task_id: str, protocol_config: scfg.Config) -> None:
        """
        Get the session token from JPL's server.

        This should only be run once per task. The session token defines the
        instance of the problem being run.

        Args:
            task_id (str): Name of the task you would like to start
            protocol_config (scfg.Config): Protocol Config

        Returns:
            none

        """
        self.task_id = task_id

        session_name = 'testing'
        if WANDB_INFO.run_wandb:
            if WANDB_INFO.run_name is not None:
                session_name = WANDB_INFO.run_name

        _json = {
            "session_name": session_name,
            "data_type": self.data_type,
            "task_id": task_id,
        }
        r = None
        try:
            r = requests.post(f"{self.url}/auth/create_session", headers=self.headers, json=_json)
            r.raise_for_status()
            self.sessiontoken = r.json()["session_token"]
            log.info(f'Using Session Token: {self.sessiontoken}')
            # prep the headers for easier use in the future.
            self.headers["session_token"] = self.sessiontoken

            # load and store the session status and metadata.
            self.status = self.get_current_status()

            self.metadata = self.get_problem_metadata()
            
            self.problem_type = self.metadata["problem_type"]

            self.current_dataset = self.status["current_dataset"]["name"]

            if WANDB_INFO.run_wandb:
                WANDB_INFO.current_stage = self.status['pair_stage']

            # if pretrainin and dataaset isn't there, add it
            if (protocol_config['image_classifier']['name'] \
                == 'pretrainimageclassification/image_classification.py' or \
                protocol_config['object_detector']['name'] \
                == 'pretrainobjectdetection/object_detection.py' or \
                protocol_config['image_classifier']['name'] \
                == 'pretrainefficientnet/image_classification.py') \
                and (protocol_config['domain_network_selector']['params']['find_source_data_method'] not in self.metadata["whitelist"]):
                protocol_config['problem']['add_dataset_to_whitelist'] = [protocol_config['domain_network_selector']['params']]

            '''savepath = Path(protocol_config['output_path'])
            savepath.mkdir(parents=True, exist_ok=True)
            savepath /= 'protocol.yaml'
            with savepath.open('w') as f:
                protocol_config.dump(f, 'yaml')'''
        except HTTPError as e:
            raise Exception(r.json()["trace"])
        except Exception as e:
            raise e

    def get_whitelist_datasets_jpl(self, dataset_dir, whitelist=None):
        """
        Get all the whitelisted datasets requested in the task from disk.

        Warn if they aren't on disk.

        Args:
            dataset_dir (str): dataset root folder for all datasets (usually starts with `lwll`)
            whitelist (list[str]): list of whitelists datasets.

        Returns:
            dict[str, Union[ImageClassificationDataset,ObjectDetectionDataset]]: dictionary of datasets
        """
        if 'whitelist' not in self.metadata:
            log.info('No whitelist found... if not MT, there might be an issue')
            return None, None

        # If whitelist is None, assume it's from itself
        if whitelist is None:
            whitelist = self.metadata["whitelist"]
        log.info(f"Get whitelist datasets: {whitelist}")
        external_dataset_root = f"{dataset_dir}/external/"
        p = Path(external_dataset_root)
        # TODO: Iter whitelist rather than ones on disk.  This is fine for now.
        external_datasets = {}
        whitelist_found = set()
        whitelist_loaded = set()
        for e in [x for x in p.iterdir() if x.is_dir()]:
            name = e.parts[-1]
            # If not on whitelist (check cases)
            if name not in whitelist:# and name.upper() not in whitelist and \
                    #name.lower() not in whitelist:
                log.info(f"Skipping {name}, not on whitelist")
                continue

            log.info(f"Loading {name}")
            whitelist_found.add(name)
            for split in ["train", "test"]:
                labels = pd.read_feather(e / f"labels_full" / f"labels_{split}.feather")
                e_root = e / f"{name}_full" / split
                if "bbox" in labels.columns:
                    if self.problem_type == 'object_detection':
                        whitelist_loaded.add(name)
                        external_datasets[f"{name}_{split}"] = ObjectDetectionDataset(
                            self, dataset_name=name, dataset_root=e_root, seed_labels=labels)
                else:
                    if self.problem_type == 'image_classification':
                        whitelist_loaded.add(name)
                        external_datasets[f"{name}_{split}"] = ImageClassificationDataset(
                            self, dataset_name=name, dataset_root=e_root, seed_labels=labels)
                    elif self.problem_type == 'video_classification':
                        try:
                            dataset_to_add = VideoClassificationDataset(
                                self, dataset_name=name, dataset_root=e_root, dataset_split=split,
                                data_type=self.data_type, seed_labels=labels)
                        except FileNotFoundError: # this is a hack for if the incoming dataset is an image dataset
                            log.info(f"Not including {name}_{split} since not video dataset")
                            dataset_to_add = None
                        if dataset_to_add is not None:
                            whitelist_loaded.add(name)
                            external_datasets[f"{name}_{split}"] = dataset_to_add

        whitelist_not_found = set(whitelist) - whitelist_found
        if len(whitelist_not_found) > 0:
            log.warning(
                f"Warning: The following items are on the whitelist but not found on the computer: "
                f"{whitelist_not_found}"
            )
        if WANDB_INFO.run_wandb:
            wandb.config.update({"whitelist_dataset" : list(whitelist_found)})

        return external_datasets, list(whitelist_loaded)

    def get_whitelist_datasets(self, dataset_dir, whitelist=None):
        """Get datasets."""
        self.get_whitelist_datasets_jpl(dataset_dir, whitelist)

    def get_budget_checkpoints(self, stage, target_dataset):
        """
        Find and return the budget checkpoints from the previously loaded metadata.

        Args:
            stage (str): current stage
            target_dataset (Union[ImageClassificationDataset,ObjectDetectionDataset]): dataset from which you
                want the budget (only for local)
        """
        if stage == "base":
            para = f"base_label_budget_{self.data_type}"
        elif stage == "adapt":
            para = f"adaptation_label_budget_{self.data_type}"
        else:
            raise NotImplementedError("{} not implemented".format(self.stage_id))

        # No idea what this was for... taking out and seeing if anything breaks
        # total_available_labels = target_dataset.unlabeled_size
        # for index, budget in enumerate(self.metadata[para]):
        #     if budget > total_available_labels:
        #         self.metadata[para][index] = total_available_labels
        #     total_available_labels -= self.metadata[para][index]
        checkpoints = self.metadata[para]

        # check if UDA is applicable
        is_uda = False
        try:
            if (stage == 'adapt'
                    and abs(self.metadata['uda_adapt_to_base_overlap_ratio'] - 1.0) < 1e-4
                    and self.problem_type != 'object_detection'):
                checkpoints = [0] + checkpoints
                is_uda = True
        except KeyError:
            pass

        return checkpoints, is_uda

    def start_stage(self, stage_name: str):
        """ Update Stage Name for local interface
        Args:
            stage_name: Name of the stage starting... will update the current stage.
        """
        log.info(f'Starting Stage {stage_name}')
        if WANDB_INFO.run_wandb:
            WANDB_INFO.current_stage = stage_name

    def start_checkpoint(self, stage, target_dataset, checkpoint_num, skip_checkpoints):
        """
        Update status and get second seed labels if on 2nd checkpoint (counting from 1).

        Args:
            stage (str): name of stage (not used here)
            target_dataset: target dataset for this checkpoint
                (used to get secondary seed labels)
            checkpoint_num (int): the current number of the checkpoint
                Only calls secondary seed labels
            skip_checkpoints (List[int]): A list of checkpoints to skip
            is_uda (bool): if there is a UDA checkpoint

        Return:
             bool: if skipping this checkpoint
        """
        if 0 <= checkpoint_num < 4 and self.problem_type != 'machine_translation':
            log.info('Getting seed labels')
            target_dataset.get_seed_labels(None, checkpoint_num)
        # Update status.
        self.status = self.get_current_status()
        skip = False
        if checkpoint_num in skip_checkpoints:
            skip = True
        return skip

    def get_remaining_budget(self) -> int:
        """
        Update the status and return the current budget.

        Returns:
            Current budget

        """
        # Make sure it's up to date
        status = self.get_current_status()
        return status["budget_left_until_checkpoint"]

    def post_results(self, stage_id, dataset, predictions, checkpoint_num):
        """
        Submit prediction back to JPL for evaluation.

        Args:
            stage_id (str): unused here.
            dataset (Union[ImageClassificationDataset,ObjectDetectionDataset]): the dataset that you are
                predicting on. Necessary to format the predictions correctly
            predictions (dict): predictions to submit in a dictionary format

        """
        return self.submit_predictions(predictions, dataset, checkpoint_num)

    def terminate_session(self):
        """
        Clean up session and close if not closed yet.

        Wipe the session id so that it can't be inadvertently used again.

        TODO:
            Save all protocol files in this command.

        Returns:
            None
        """

        if self.sessiontoken is not None:
            self.deactivate_current_session()
            self.sessiontoken = None
            del self.headers["session_token"]
        self.toolset = {}

    @retry(stop=stop_after_attempt(5), wait=wait_fixed(2),
           reraise=True, before_sleep=before_sleep_log(logging.getLogger(__name__), logging.INFO))
    def get_current_status(self):
        """
        Get the current status of the session.

        This will tell you things like the number of images you can query
        before evaluation, location of the dataset, number of classes, etc.

        An example session: ::

            { 'active': 'In Progress',
              'budget_left_until_checkpoint': 10,
              'budget_used': 0,
              'current_dataset': {'classes': ['0',
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7',
                '8',
                '9'],
               'data_url': '/datasets/lwll_datasets/mnist/mnist_sample/train',
               'dataset_type': 'image_classification',
               'name': 'mnist',
               'number_of_channels': 1,
               'number_of_classes': 10,
               'number_of_samples_test': 1000,
               'number_of_samples_train': 5000,
               'uid': 'mnist'},
              'current_label_budget_stages': [10, 30, 70, 165, 387, 909, 2131, 5000],
              'date_created': 1588221182000,
              'date_last_interacted': 1588221182000,
              'pair_stage': 'base',
              'session_name': 'testing',
              'task_id': 'problem_test_image_classification',
              'uid': 'k6tE4Vo2oFEuSHo7MhUE',
              'user_name': 'JPL',
              'using_sample_datasets': True}

        Returns:
            dict[str, str]: status of problem/task
        """
        r = None
        try:
            r = requests.get(f"{self.url}/session_status", headers=self.headers)
            r.raise_for_status()
            status = r.json()["Session_Status"]
            self.status = status
        except HTTPError as e:
            raise Exception(r.json()["trace"])
        except Exception as e:
            raise e

        return status

    @retry(stop=stop_after_attempt(5), wait=wait_fixed(2),
           reraise=True, before_sleep=before_sleep_log(logging.getLogger(__name__), logging.INFO))
    def get_problem_metadata(self, task_id=None):
        """
        Get the task metadata from JPL's server.

        Args:
            task_id (str): task id for which you want the metadata for.  If none
                given, assume you want the current one

        An example: ::

              "task_metadata": {
                "adaptation_dataset": "mnist",
                "adaptation_evaluation_metrics": [
                  "accuracy"
                ],
                "adaptation_label_budget_full": [
                  10,
                  110,
                  314,
                  899,
                  2569,
                  7343,
                  20991,
                  60000
                ],
                "adaptation_label_budget_sample": [
                  10,
                  30,
                  70,
                  165,
                  387,
                  909,
                  2131,
                  5000
                ],
                "base_dataset": "cifar100",
                "base_evaluation_metrics": [
                  "accuracy"
                ],
                "base_label_budget_full": [
                  100,
                  1100,
                  2078,
                  3926,
                  7416,
                  14010,
                  26467,
                  50000
                ],
                "base_label_budget_sample": [
                  100,
                  300,
                  479,
                  766,
                  1225,
                  1957,
                  3128,
                  5000
                ],
                "problem_type": "image_classification",
                "task_id": "6d5e1f85-5d8f-4cc9-8184-299db03713f4",
                "whitelist": [
                  "imagenet1k",
                  "domain_net-painting",
                  "coco2014"
                ]
              }
            }

        """
        r = None
        if task_id is None:
            task_id = self.task_id
        try:
            #if task_id == '3159d8ef-dfef-48fc-9325-829258be5f28':
            #    return None
            r = requests.get(f"{self.url}/task_metadata/{task_id}", headers=self.headers)
            r.raise_for_status()
            metadata = r.json()["task_metadata"]
        except HTTPError as e:
            raise Exception(r.json()["trace"])
        except Exception as e:
            raise e
        return metadata

    def get_dataset_jpl(self, stage_name, dataset_split, seed_filepath=None):
        """
        Return a JPL dataset.

        Gets a dataset which has been downloaded from the JPL Repo and
        return a Union[ImageClassificationDataset,ObjectDetectionDataset] with that data.

        Args:
            stage_name (str): unused here
            dataset_split (str): whether get train or test data
            categories (list[str]): if test data, list of catagories.
                Otherwise None
            seed_filepath: added to match local interface, but not used at all
                here
        Returns:
            (Union[ImageClassificationDataset,ObjectDetectionDataset]) requested dataset
        """
        current_dataset = self.status["current_dataset"]["name"]
        self.categories = sorted(self.status["current_dataset"]["classes"])
        
        if dataset_split == "eval":
            dataset_split = "test"

    
        dataset_root = Path(self.dataset_dir, self.dataset_type, current_dataset,
                            f'{current_dataset}_{self.data_type}', dataset_split)
        name = current_dataset + "_" + dataset_split

        if self.metadata["problem_type"] == "image_classification":
            dataset = ImageClassificationDataset(self, dataset_root=str(dataset_root), dataset_name=name,
                                                 categories=self.categories)
        elif self.metadata["problem_type"] == "object_detection":
            dataset = ObjectDetectionDataset(self, dataset_root=str(dataset_root), dataset_name=name,
                                             categories=self.categories)
        elif self.metadata["problem_type"] == "video_classification":
            dataset = VideoClassificationDataset(self, dataset_root=str(dataset_root), dataset_name=name,
                    dataset_split=dataset_split, data_type=self.data_type, categories=self.categories)
        elif self.metadata["problem_type"] == 'machine_translation':
            dataset = MachineTranslationDataset(self, dataset_name = name, 
                dataset_split = dataset_split,
                dataset_root = dataset_root)
            #dataset = pd.read_feather(dataset_root.with_name(f'{dataset_split}_data.feather'))
        else:
            raise NotImplementedError(f'Problem type: {self.metadata["problem_type"]} not implemented yet')

        if self.metadata["problem_type"] != 'machine_translation':
            self.categories = sorted(self.status["current_dataset"]["classes"])

        return dataset

    @retry(stop=stop_after_attempt(5), wait=wait_fixed(2),
           reraise=True, before_sleep=before_sleep_log(logging.getLogger(__name__), logging.INFO))
    def get_seed_labels(self, dataset_name, seed_level):
        """
        Get the seed labels for the dataset from JPL's server.

        Args:
            dataset_name (str): Not used here
            seed_level (int): number of seed labeled level (either 0 or 1)
                necessitated by the secondary_seed_labels in the second checkpoint

        Returns:
            list[tuple[str, str]]: the initial seed labels
                a list of [filename, label] elements
        """
        r = None
        log.debug('Requesting Seed Labels')
        try:
            r = requests.get(f"{self.url}/seed_labels", headers=self.headers)
            r.raise_for_status()
            seed_labels = r.json()
        except HTTPError as e:
            raise Exception(r.json()["trace"])
        except Exception as e:
            raise e

        log.debug(f'Received {len(seed_labels["Labels"])} seed labels')
        return seed_labels["Labels"]

    @retry(stop=stop_after_attempt(5), wait=wait_fixed(2),
           reraise=True, before_sleep=before_sleep_log(logging.getLogger(__name__), logging.INFO))
    def get_more_labels(self, fnames, dataset_name=None):
        """
        Query JPL's API for more labels (the active learning component).

        Danger:
            Neither JPL nor this function protects against
            relabeling images that are already labeled.
            These relabeling requests count against your budget.

        Args:
            fnames (list[str]): filenames for which you want the labels
            dataset_name (str): dataset name (used in local for lookup)

        Return:
            list: (list[tuple(str,str)]): newly labeled image filenames and classes
        """

        log.debug(f'Requesting {len(fnames)} new datums requested Seed Labels')
        try:

            r = requests.post(f"{self.url}/query_labels", json={"example_ids": fnames}, headers=self.headers)
            r.raise_for_status()
            new_data = r.json()

        except HTTPError as e:
            raise Exception(r.json()["trace"])
        except Exception as e:
            raise e
        self.status = new_data["Session_Status"]

        log.debug(f'Got {len(new_data["Labels"])} new labels')
        return new_data["Labels"]

    @retry(stop=stop_after_attempt(5), wait=wait_fixed(2),
            reraise=True, before_sleep=before_sleep_log(logging.getLogger(__name__), logging.INFO))
    def submit_predictions(self, predictions: dict,
                           dataset: Union[ImageClassificationDataset,ObjectDetectionDataset],
                           checkpoint_num: int) -> dict:
        """
        Submit prediction back to JPL for evaluation.

        Look at
        ..:meth:ImageClassificationDataset.format_predictions for formatting info

        Args:
            predictions (dict): predictions to submit in a dictionary format
            dataset (Union[ImageClassificationDataset,ObjectDetectionDataset]) for which you are submitting
                the labels

        Returns:
            dict for status
        """

        if self.problem_type != 'machine_translation':
            log.debug(f'Submitting {len(predictions[0])} predictions.')
            if isinstance(predictions, pd.DataFrame):
                predictions_formatted = dataset.format_prob_predictions(
                    copy.deepcopy(predictions))
            else:
                predictions_formatted = dataset.format_predictions(predictions[0], predictions[1])

        r = None
        url_target = 'submit_predictions'
        if checkpoint_num < 0:
            url_target = 'submit_UDA_predictions'
        try:
            r = requests.post(f"{self.url}/{url_target}",
                              json={"predictions": predictions_formatted},
                              headers=self.headers)
            r.raise_for_status()
            self.status = r.json()["Session_Status"]
        except HTTPError as e:
            if "trace" in r.json():
                raise Exception(r.json()["trace"])
            else:
                Exception(r.json()["message"])
        except Exception as e:
            raise e
        return self.status

    def format_status(self, update=False):
        """
        Return formatted string with the current status of the problem/task.

        Args:
            update (bool): should the status be updated

        Returns:
              str: Formatted String of Status
        """
        if update:
            info = json.dumps(self.get_current_status(), indent=4)
        else:
            info = json.dumps(self.status, indent=4)
        #if WANDB_INFO.run_wandb:
        #    if 'checkpoint_scores' in info:
                # has scores
        #        scores = info['checkpoint_scores']
        return "\n".join(["Problem/Task Status:", info, ""])

    def format_task_metadata(self):
        """
        Return formatted string of the task/problem metadata.

        Returns:
              str: Formatted String of Metadata
        """
        self.metadata = self.get_problem_metadata()
        info = json.dumps(self.metadata, indent=4)
        return "\n".join(["Problem/Task Metadata:", info, ""])

    def __repr__(self):
        """
        Format self as a string.

        Returns:
            str: Formatted String of metadata and status
        """

        return self.format_task_metadata() + "\n" + self.format_status()

    @retry(stop=stop_after_attempt(5), wait=wait_fixed(2),
           reraise=True, before_sleep=before_sleep_log(logging.getLogger(__name__), logging.INFO))
    def deactivate_all_session(self):
        """
        Clean up by deactivating all active sessions.

        Only do this by and if necessary to clean up.

        Returns:
            None
        """
        r = None
        try:
            r = requests.get(f"{self.url}/list_active_sessions", headers=self.headers)
            r.raise_for_status()
            active_sessions = r.json()["active_sessions"]
            for act_sess in active_sessions:
                r = requests.post(f"{self.url}/deactivate_session",
                                  json={"session_token": act_sess},
                                  headers=self.headers,)
                r.raise_for_status()
                log.info(f"{r.json()}")
        except HTTPError as e:
            raise Exception(r.json()["trace"])
        except Exception as e:
            raise e

    @retry(stop=stop_after_attempt(5), wait=wait_fixed(2),
            reraise=True, before_sleep=before_sleep_log(logging.getLogger(__name__), logging.INFO))
    def deactivate_current_session(self):
        """
        Clean up by deactivating a session that was canceled early or do nothing.

        Returns:
            None
        """
        r = None
        try:
            r = requests.get(f"{self.url}/list_active_sessions", headers=self.headers)
            r.raise_for_status()
            active_sessions = r.json()["active_sessions"]
            if self.sessiontoken in active_sessions:
                r = requests.post(
                    f"{self.url}/deactivate_session", json={"session_token": self.sessiontoken}, headers=self.headers,
                )
                r.raise_for_status()
                log.warning("Deactivated improperly ended session")
            else:
                log.info("Session Properly Deactivated")
        except HTTPError as e:
            raise Exception(r.json()["trace"])
        except Exception as e:
            raise e

    def get_stages(self):
        """
        Return a list of the stages in the current task.

        Args:
            none
        """
        return self.stagenames

    def finish_stage(self, stage, toolset):
        """
        Cleanup for end of each stage

        Args:
            stage (str): current stage of evaluation
            toolset (dict[str,any]): toolset for protocol
        """
        # Add stage dataset to the whitelist

        toolset['last_stage_pretrained_network'] = toolset["source_network"]
        log.info(f'Finishing Stage "{stage}" for dataset {self.current_dataset}')
        if self.problem_type != 'machine_translation':
            train_name = f'{stage}_{toolset["target_dataset"].name}'
            toolset["whitelist_datasets"][train_name] = toolset["target_dataset"]
            test_name = f'{stage}_{toolset["eval_dataset"].name}'
            toolset["whitelist_datasets"][test_name] = toolset["eval_dataset"]
            name = train_name.replace('_train', '')
            toolset['whitelist'].append(name)
            if toolset["source_network"] is not None:
                toolset["source_network"].target_dataset = name
        log.info(self.format_status(False))

    @retry(stop=stop_after_attempt(5), wait=wait_fixed(2),
            reraise=True, before_sleep=before_sleep_log(logging.getLogger(__name__), logging.INFO))
    def get_task_ids(self):
        """
        Return a list of tasks.

        Args:
            none
        """
        r = None
        try:
            log.info(f"Using this URL: {self.url}")
            r = requests.get(f"{self.url}/list_tasks", headers=self.headers)
            r.raise_for_status()
            self.task_ids = r.json()["tasks"]
        except HTTPError as e:
            raise Exception(r.json()["trace"])
        except Exception as e:
            raise e
        return self.task_ids

    @staticmethod
    def get_task_subset_by_type(secret: str, govteam_secret: str, url: str,
                                subset_type: str) -> List[str]:
        """
        Helper function that returns the task ids in a list that match a specified
        problem type

        Args:
            secret (str): Key used for authenticating
            govteam_secret (str): Second key used for authenticating
            url (str): URL of the server
            subset_type (str): The task_type subset you want to get back

        Returns:
            list of tasks for a problem type
        """
        r = None
        try:
            headers = {"user_secret": secret,
                       "govteam_secret": govteam_secret}
            tasks = requests.get(f"{url}/list_tasks", headers=headers)
            task_list = tasks.json()['tasks']
            subset_tasks = []
            for _task in task_list:
                r = requests.get(f"{url}/task_metadata/{_task}", headers=headers)
                task_metadata = r.json()
                if task_metadata['task_metadata']['problem_type'] == subset_type:
                    subset_tasks.append(_task)
        except HTTPError as e:
            raise Exception(r.json()["trace"])
        except Exception as e:
            raise e
        return subset_tasks

    @retry(stop=stop_after_attempt(5), wait=wait_fixed(2),
            reraise=True, before_sleep=before_sleep_log(logging.getLogger(__name__), logging.INFO))
    def set_skip_checkpoint(self):
        """
        Tell JPL interface that we are skipping the checkpoint

        Args:
            None
        """
        r = None
        try:
            r = requests.get(f"{self.url}/skip_checkpoint", headers=self.headers)
            r.raise_for_status()
            ret = True
        except HTTPError as e:
            raise Exception(r.json()["trace"])
            ret = False
        except Exception as e:
            raise e
            ret = False
        return ret
