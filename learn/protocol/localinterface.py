"""Local Interface."""

import json
from pathlib import Path
import pandas as pd
from learn.deprecated_tinker.harness import Harness
from learn.utils.dataset import (
    ImageClassificationDataset, ObjectDetectionDataset,
    #VideoClassificationDataset, 
    MachineTranslationDataset
)
from learn.algorithms.CoMix.dataset_slowfast import VideoClassificationDataset_SlowFast as VideoClassificationDataset
from learn.utils.wandb_utils import WANDB_INFO
import logging
log = logging.getLogger(__name__)
import ubelt as ub
import os
import scriptconfig as scfg
from typing import List, Union, Any
import numpy as np
import wandb
from learn.utils import metrics

class LocalInterface(Harness):
    """Local interface."""

    def __init__(self, json_configuration_file, interface_config_path):
        """
        Initialize the object by loading the given configuration file.

        The configuration file is expected to be located relative to the
        interface_config_path.

        Args:
            json_configuration_file:
        """

        Harness.__init__(self, json_configuration_file, interface_config_path)

        self.task_ids = dict()

        self.data_type = None
        self.task_id = None
        self.stage_id = None
        self.status = {}
        self.status['results'] = []

        self.metadata = {}

        # Change url during evaluation on DMC servers (changes paths to eval datasets)
        self.evaluate = False

        self.stagenames = []
        self.checkpoint_num = 0
        self.dataset_dir = None

        self.categories = None

        self.current_stage = None
        self.seed_labels = {}
        self.label_sets = {}
        self.label_sets_pd = {}

    def update_parameters(self,  protocol_config: dict):
        """ Update class with protocol information and search for local tasks

        """
        log.info('Protocol Config File Parameters:')
        log.info(protocol_config)
        self.dataset_dir = protocol_config['dataset_dir']
        self.api_key = protocol_config['apikey']
        self.url = protocol_config['url']
        self.data_type = protocol_config['data_type']
        self.dataset_type = protocol_config['dataset_type']

        # specify which GPU(s) to be used
        if isinstance(protocol_config['device'], int):
            os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
            os.environ["CUDA_VISIBLE_DEVICES"] = str(protocol_config['device'])
            protocol_config['device'] = 0
        elif ub.iterable(protocol_config['device']):
            os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
            os.environ["CUDA_VISIBLE_DEVICES"] = ','.join(map(str, protocol_config['device']))
            protocol_config['device'] = list(range(0, len(protocol_config['device'])))

        # Load local configs
        task_folder = Path(protocol_config['local_task_folder_path'])
        task_files = list(task_folder.glob('*.json'))
        log.info(f'Found {len(task_files)} task files.')
        for tf in task_files:
            with open(tf, 'r') as f:
                tf_json = json.load(f)
                self.task_ids[tf_json['name']] = tf_json

    def initialize_session(self, task_id, protocol_config: scfg.Config):
        """
        Open a new session for the given task id.

        This will reset old information from any previous sessions. Do not call
        this before posting results from an existing session or the results will
        not post properly.

        Args:
            task_id: task_id
            protocol_config: protocol configuration

        """
        # clear any old session data, and prepare for the next task
        self.metadata = self.task_ids[task_id]
        # prep the stagenames from the metadata for easier searching later.
        self.stagenames = []
        for stage in self.metadata["stages"]:
            self.stagenames.append(stage["name"])
        self.current_stage = self.stagenames[0]
        if WANDB_INFO.run_wandb:
            WANDB_INFO.current_stage = self.current_stage

        self.status['current_dataset'] = dict()
        self.status['current_dataset']['name'] = self.get_stage_metadata(self.current_stage)['dataset']

        self.problem_type = self.metadata["problem_type"]

        '''savepath = Path(protocol_config['output_path'])
        savepath.mkdir(parents=True, exist_ok=True)
        savepath /= 'protocol.yaml'
        with savepath.open('w') as f:
            protocol_config.dump(f, 'yaml')'''

    def get_whitelist_datasets_jpl(self, dataset_dir, whitelist=None):
        """
        Get all the whitelisted datasets requested in the task from disk.

        Warn if they aren't on disk.

        Args:
            dataset_dir (str): dataset root folder for all datasets (usually starts with `lwll`)
            whitelist (list[str]): list of whitelists datasets.

        Returns:
            dict[str, Union[ImageClassificationDataset,ObjectDetectionDataset]]: dictionary of datasets
        """
        if 'whitelist' not in self.metadata:
            log.info('No whitelist found... if not MT, there might be an issue')
            return None, None

        # If whitelist is None, assume it's from itself
        if whitelist is None:
            whitelist = self.metadata["whitelist"]
        log.info(f"Get whitelist datasets: {whitelist}")
        external_dataset_root = f"{dataset_dir}/external/"
        p = Path(external_dataset_root)
        # TODO: Iter whitelist rather than ones on disk.  This is fine for now.
        external_datasets = {}
        whitelist_found = set()
        whitelist_loaded = set()
        print("localinterface ",os.path.abspath(p))
        for e in [x for x in p.iterdir() if x.is_dir()]:
            name = e.parts[-1]
            # If not on whitelist
            if name not in whitelist:# and name.upper() not in whitelist and \
                    #name.lower() not in whitelist:
                log.info(f"Skipping {name}, not on whitelist")
                continue

            log.info(f"Loading {name}")
            whitelist_found.add(name)
            for split in ["train", "test"]:
                labels = pd.read_feather(e / f"labels_full" / f"labels_{split}.feather")
                e_root = e / f"{name}_full" / split
                if "bbox" in labels.columns:
                    if self.problem_type == 'object_detection':
                        whitelist_loaded.add(name)
                        external_datasets[f"{name}_{split}"] = ObjectDetectionDataset(
                            self, dataset_name=name, dataset_root=e_root, seed_labels=labels)
                else:
                    if self.problem_type == 'image_classification':
                        whitelist_loaded.add(name)
                        external_datasets[f"{name}_{split}"] = ImageClassificationDataset(
                            self, dataset_name=name, dataset_root=e_root, seed_labels=labels)
                    elif self.problem_type == 'video_classification':
                        whitelist_loaded.add(name)
                        external_datasets[f"{name}_{split}"] = VideoClassificationDataset(
                            self, dataset_name=name, dataset_root=e_root, dataset_split=split,
                            data_type=self.data_type, seed_labels=labels)

        whitelist_not_found = set(whitelist) - whitelist_found
        if len(whitelist_not_found) > 0:
            log.warning(
                f"Warning: The following items are on the whitelist but not found on the computer: "
                f"{whitelist_not_found}"
            )
        if WANDB_INFO.run_wandb:
            wandb.config.update({"whitelist_dataset" : list(whitelist_found)})
        return external_datasets, list(whitelist_loaded)

    def get_whitelist_datasets(self, dataset_dir, whitelist=None):
        """Get datasets."""
        self.get_whitelist_datasets_jpl(dataset_dir, whitelist)

    def get_budget_checkpoints(self, stage, target_dataset):
        """
        Find and return the budget checkpoints from the previously loaded metadata.

        This uses the configuration option: "label_budget"

        Args:
            stage (str): name of stage to collect the metadata
            target_dataset (str): the naeme of the database
        """
        stage_metadata = self.get_stage_metadata(stage)
        if stage_metadata:
            seed_total = 0
            # Create the checkpoints cumlative
            self.checkpoints = []
            for it in range(len(stage_metadata["seed_budgets"])):
                seed_total += len(self.seed_labels[f'{stage_metadata["dataset"]}_train'][it]['id'].unique())
                self.checkpoints.append(seed_total)
            self.checkpoints += stage_metadata["label_budget"]

            if self.checkpoints[-1] > len(target_dataset):
                log.warning('Warning: label budgets exceed size of training dataset.')

            self.budgets = [self.checkpoints[0]] + np.diff(self.checkpoints).tolist()
            # Set seed label budgets to 0
            for it in range(len(stage_metadata["seed_budgets"])):
                self.budgets[it] = 0

            is_uda = False
            try:
                if (stage == 'adapt'
                        and abs(self.metadata['uda_adapt_to_base_overlap_ratio'] - 1.0) < 1e-4
                        and self.problem_type != 'object_detection'):
                    self.checkpoints = [0] + self.checkpoints
                    is_uda = True
            except KeyError:
                pass
            return self.checkpoints, is_uda

        else:
            log.error(f"Missing stage metadata for {stage}")
            exit(1)

    def start_stage(self, stage_name: str):
        """ Update Stage Name for local interface

        Args:
            stage_name: Name of the stage starting... will update the current stage.
        """
        log.info(f'Starting Stage {stage_name}')
        if not self.current_stage == stage_name:
            # this is a new stage, so reset to use the budgets for the new stage
            self.current_stage = stage_name
            if WANDB_INFO.run_wandb:
                WANDB_INFO.current_stage = self.current_stage

    def start_checkpoint(self, stage_name, target_dataset, checkpoint_num, skip_checkpoints):
        """
        Cycle through the checkpoints for all stages in order.

        Report an error if we try to start a checkpoint after the last
        stage is complete. A checkpoint is ended when post_results is called.

        Args:
            stage_name (str): name of current stage
            target_dataset (Union[ImageClassificationDataset,ObjectDetectionDataset]): dataset class
            checkpoint_num (int): checkpoint number currently at
            skip_checkpoints (List[int]): A list of checkpoints to skip
        """

        stage_metadata = self.get_stage_metadata(stage_name)

        log.info(f'Starting Budget Level {checkpoint_num} with a budget of '
                     f'{self.checkpoints[checkpoint_num]}.')

        if not self.stagenames:
            log.error("Can't start a checkpoint without initializing a sesssion")
            exit(1)

        if 0 <= checkpoint_num < len(stage_metadata["seed_budgets"]) \
                and self.problem_type != 'machine_translation':
            target_dataset.get_seed_labels(None, checkpoint_num)

        # move to the next checkpoint and move its budget into the current budget.
        self.current_checkpoint_index = checkpoint_num
        if self.current_checkpoint_index >= len(self.checkpoints):
            log.error("Out of checkpoints, cant start a new checkpoint")
            exit(1)

        self.current_budget = self.budgets[self.current_checkpoint_index]
        if target_dataset.unlabeled_size < self.current_budget:
            self.current_budget = target_dataset.unlabeled_size
            
        skipping = False
        if checkpoint_num in skip_checkpoints:
            skipping = True
        return skipping

    def set_skip_checkpoint(self):
        return True

    def get_remaining_budget(self):
        """
        Return the amount of budget remaining on the current checkpoint.

        The budget reflects and extra labels that have already been used, but is
        not affected by seed labels. The budget indicates the total number of
        files that are permitted to be labeled, and the actual number of labels
        may be higher if there are multiple labels per file.

        Returns:
                The current number of files that can be labeled. Multiple labels may
                be returned per file requested.
        """
        if self.current_budget is not None:
            return self.current_budget
        else:
            log.error("Must start a checkpoint before requesting a budget")
            exit(1)

    def download_dataset(self, dataset_name, dataset_path):
        """
        Download the data using torchvision.

        Args:
            dataset_name (str): name of dataset to download
            dataset_path (Path): path to folder to download into

        """
        import torchvision

        if dataset_name == "mnist":
            dataset = torchvision.datasets.MNIST(
                dataset_path, train=True, transform=None, target_transform=None, download=True)
            self.create_dataset(dataset, dataset_name, dataset_path)

            dataset = torchvision.datasets.MNIST(
                dataset_path, train=False, transform=None, target_transform=None, download=True)

            self.create_dataset(dataset, dataset_name, dataset_path)

    @staticmethod
    def create_dataset(dataset, dataset_name, dataset_path):
        """
        Convert torchvision dataset to a coco dataset object.

        This will download the zip, create and save the iamges as a png, and
        then create a coco dataset object.

        Args:
            dataset (torchvision.datasets.mnist): dataset from torchvision which
                will be remade into our glorious dataset
            dataset_name (str): name of dataset being created
            dataset_path (Path): path to root dataset folder where all the
                images will be saved.

        """
        dataset_coco = {}
        dataset_root = dataset_path / "images"
        dataset_coco["root"] = str(dataset_root)
        dataset_coco["classes"] = dataset.classes
        dataset_coco["class_to_idx"] = dataset.class_to_idx
        # using prefix to seperate out the images
        split = "test"
        if dataset.train:
            split = "train"
        images = []

        for itx, data in enumerate(dataset):
            img, label = data
            img_name = dataset_root / f"{split}_{itx}.png"
            img.save(img_name)

            # Add record for coco
            record = {}
            record["file_name"] = str(img_name)
            record["height"] = img.height
            record["width"] = img.width
            record["image_id"] = itx
            record["category_id"] = label
            images.append(record)

        dataset_coco["images"] = images
        coco_filename = dataset_path / f"{dataset_name}_{split}.coco"
        json_obj = json.dumps(dataset_coco, indent=4)
        with open(coco_filename, "w") as json_file:
            json_file.write(json_obj)

    def get_dataset_jpl(self, stage_name: str, dataset_split: str, categories: List=None,
            seed_filepath: str =None):
        """
        Construct a JPL dataset object from a path in the configuration.

        Args:
            stage_name: name of stage
            dataset_split: either train or eval
            categories: list of category names.
        """

        stage_metadata = self.get_stage_metadata(stage_name)
        current_dataset = stage_metadata['dataset']
        log.info(f"current dataset: {current_dataset}" )
        if dataset_split == "eval":
            dataset_split = "test"

        dataset_root = Path(self.dataset_dir, self.dataset_type, current_dataset,
                            f'{current_dataset}_{self.data_type}', dataset_split)
        label_file = Path(self.dataset_dir, self.dataset_type, current_dataset,
                          f'labels_{self.data_type}', f'labels_{dataset_split}.feather')

        labels = pd.read_feather(label_file)

        if dataset_split == "eval":
            dataset_split = "test"

        name = current_dataset + "_" + dataset_split

        self.label_sets_pd[name] = labels
        # translate the labels provided by pandas into a dict keyed on the filename
        # while we are at it, build the seed labels as well.

        # select one label for each class, using the first label we find for that
        # class.

        self.seed_labels[name] = dict()
        labels_ = labels.copy()
        seed_current = 0
        if seed_filepath and os.path.exists(seed_filepath):
            with open(seed_filepath) as seed_filepath_f:
                seed_file = json.load(seed_filepath_f)
        else:
            seed_file = None

        for it, k in enumerate(stage_metadata['seed_budgets']):
            seed_current = k - seed_current  # Subtract previous layer (so make sure it's x-shot)
            if seed_file and str(k) in seed_file:
                seed_labels = labels_[labels_['id'].isin(seed_file[str(k)])]
            else:
                if self.problem_type == 'machine_translation':
                    # TODO: check to make sure behavior matches (for characters)
                    seed_labels = labels_.groupby('id').tail(seed_current)
                else:
                    seed_labels = labels_.groupby('class').tail(seed_current)

            # match weird behavior of JPL code to pad out to match their static budget levels
            if self.problem_type in ['image_classification',
                    'video_classification']:
                labels_ = labels_.drop(seed_labels.index)
            if self.problem_type == 'object_detection':
                # find any other labels on this image
                extra_seed_labels = labels_[labels_["id"].isin(seed_labels['id'].unique())]
                seed_labels = seed_labels.append(extra_seed_labels)
                labels_ = labels_.drop(seed_labels.index)
                num_images_chosen = len(seed_labels['id'].unique())
                num_classes = len(seed_labels['class'].unique())
                remaining = num_classes*seed_current - num_images_chosen
                extra_seed_ids = labels_['id'].sample(remaining, random_state=0)
                extra_seed_labels = labels_[labels_["id"].isin(extra_seed_ids)]
                seed_labels = seed_labels.append(extra_seed_labels)
                labels_ = labels_.drop(extra_seed_labels.index)
            # TODO: add checks for MT and video
            self.seed_labels[name][it] = seed_labels.copy()
            log.debug(seed_labels)

        name = current_dataset + "_" + dataset_split
        if self.metadata["problem_type"] == "image_classification":
            dataset = ImageClassificationDataset(self, dataset_root=str(dataset_root), dataset_name=name,
                                                 categories=self.categories)
            if dataset_split == 'train':
                self.categories = dataset.categories
        elif self.metadata["problem_type"] == "object_detection":
            dataset = ObjectDetectionDataset(self, dataset_root=str(dataset_root), dataset_name=name,
                                             categories=self.categories)
            if dataset_split == 'train':
                self.categories = dataset.categories
        elif self.metadata["problem_type"] == 'machine_translation':
            dataset = MachineTranslationDataset(self, dataset_name = name,
                dataset_split = dataset_split,
                dataset_root = dataset_root)
            #dataset = pd.read_feather(dataset_root.with_name(f'{dataset_split}_data.feather'))

        elif self.metadata["problem_type"] == "video_classification":
            dataset = VideoClassificationDataset(self, dataset_root=str(dataset_root), dataset_name=name,
                    dataset_split=dataset_split, data_type=self.data_type, categories=self.categories)
            if dataset_split == 'train':
                self.categories = dataset.categories
        else:
            raise NotImplementedError(f'Problem type: {self.metadata["problem_type"]} not implemented yet')

        if dataset_split == 'test':
            self.categories = None

        return dataset

    def get_more_labels(self, fnames, dataset_name):
        """
        Request labels for a given set of filenames.

        This will deduct the number of files requested from the budget, and
        return all labels for the files requested. This can return more labels
        than files requested if individual files have multiple labels per file.
        If not enough budget is available for the requested list, this function
        will produce an error.

        Args:
            fnames (list[str]): name of file names
            dataset_name (str): lookup for dataset name in label_set_pd
        """
        if self.current_budget is None:
            log.error("Can't get labels before checkpoint is started")
            exit(1)

        if len(fnames) > self.current_budget:
            # the request is for too many labels
            log.error("Too many labels requested, not enough budget.")
            exit(1)

        mask = self.label_sets_pd[dataset_name]["id"].isin(fnames)
        new_labels = self.label_sets_pd[dataset_name][mask]
        return new_labels.to_dict()

    def get_seed_labels(self, dataset_name, seed_level):
        """
        Retrieve seed labels.

        Seed labels do not count against the budgets.

        Args:
            dataset_name (str): name of dataset to get the seed labels from
            seed_level: (int): number of seed label level
        """

        return self.seed_labels[dataset_name][seed_level]

    def post_results(self, stage_id, dataset, predictions, checkpoint_num):
        """
        Submit predictions for analysis and recording.

        This function will report on the accuracy of the submitted predictions.

        Args:
            dataset (Union[ImageClassificationDataset,ObjectDetectionDataset]):  Dataset
            predictions:
        """
        # this will need to do more processing in the future.
        self.current_budget = None

        if self.metadata["problem_type"] == "machine_translation":
            # TODO: fix quick hack
            log.info("No metric for mt yet")
            return

        if isinstance(predictions, pd.DataFrame):
            predictions_formatted = dataset.format_prob_predictions(predictions)
            probabilistic = True
        else:
            predictions_formatted = dataset.format_predictions(predictions[0], predictions[1])
            probabilistic = False

        predicitons_filename = f'{self.metadata["results_file"]}_' \
                               f'{stage_id}_{self.current_checkpoint_index}.json'

        json_obj = json.dumps(predictions_formatted, indent=4)
        with open(predicitons_filename, "w") as json_file:
            json_file.write(json_obj)

        gt = self.label_sets_pd[dataset.name].sort_values("id")

        # Ensure ground truth class labels are strings.
        gt["class"] = gt["class"].astype(str)
        if self.metadata["problem_type"] == "video_classification":
            gt = gt.drop(columns=["video_id", "end_frame", "start_frame"])

        pred = pd.DataFrame(predictions_formatted).sort_values("id")

        # pred = gt.copy()
        # pred['confidence'] = 0.95
        metric_dict = {}
        if self.metadata["problem_type"] in ["image_classification",
                "video_classification"]:
            # Ensure that this is true and all classes are aligned.
            assert (gt["id"].values != pred["id"].values).sum() == 0

            metrics._validate_input_ids_one_to_one(pred, gt)
            classes = list(set(gt['class']))# can instead get this directly from the dataset
            if probabilistic:
                pred = metrics._probabilistic_to_hard_assignment(pred)
            y_true_oh, y_pred_oh = metrics._preprocess_df_to_array(pred, gt, classes)

            y_true, y_pred = metrics._align_ids(pred, gt)
            metric_dict['accuracy'] = metrics.accuracy(y_true, y_pred)
            metric_dict['weighted_acc'] = metrics.weighted_accuracy(y_true, y_pred)
            # need else statement here, otherwise will error out if probabilistic is true
            if probabilistic:
                metric_dict['top_5_accuracy'] = metrics.top_5_accuracy(gt['class'].values, y_pred_oh)
                metric_dict['roc_auc'] = metrics.roc_auc(y_true_oh, y_pred_oh)
                metric_dict["cross_entropy_logloss"] = metrics.cross_entropy_logloss(y_true_oh, y_pred_oh)
                metric_dict["brier_score"] = metrics.brier_score(y_true_oh, y_pred_oh)

            metric_info = ''
            for k,v in metric_dict.items():
                metric_info += '\t{} : {}\n'.format(k,v)
            log.info(
                f"\nAccuracy for Stage:{stage_id} "
                f"Checkpoint: {self.current_checkpoint_index} is "
                f"{metric_info}"
            )
        elif self.metadata["problem_type"] == "object_detection":
            from learn.utils.metrics import mAP
            from learn.utils.metrics import tide

            acc = mAP(pred, gt)
            log.info(
                f"\nAccuracy for Stage:{stage_id} "
                f"Checkpoint: {self.current_checkpoint_index} is "
                f"mAP: {100 * acc:.02f}"
            )
            tide(pred, gt)

            # temporary hack for OD
            metric_dict['accuracy'] = acc
        else:
            raise ValueError("Unknown problem type for accuracy: " + 
                "{}".format(self.metadata["problem_type"]))

        if WANDB_INFO.run_wandb:
            metric_dict.update({"stage_id" : stage_id, 
                "checkpoint": self.current_checkpoint_index,
                'num_labels' : self.checkpoints[self.current_checkpoint_index]})    
            WANDB_INFO.log_metrics(metric_dict)
        self.status['results'].append([stage_id, self.current_checkpoint_index, metric_dict['accuracy']])

    def get_problem_metadata(self, task_id):
        """
        Return the metadata for the given task id.

        This data will provide information about the test configuration for the
        task.

        Args:
            task_id:
        """
        self.metadata = self.task_ids[task_id]
        return self.metadata

    def terminate_session(self):
        """
        End the current session.

        This should be called after all results have been posted, and before a
        new session is started.
        """
        self.toolset = {}
        self.metadata = None

    def get_stage_metadata(self, stagename):
        """
        Find and return the metadata for the given stage name.

        If no matching stage is found, this will return None.
        """

        # search through the list of stages for one that has a matching name.
        for stage in self.metadata["stages"]:
            if stage["name"] == stagename:
                return stage
        return None

    def format_status(self, update: bool) -> str:
        """
        Update and return formatted string with the current status of the problem/task.

        Also should return accuracy if available

        Args:
            update (bool): not used

        Returns:
              str: Formatted String of Status
        """
        info = json.dumps(self.status, indent=4)
        return "\n".join(["Problem/Task Status:", info, ""])

    def format_task_metadata(self):
        """
        Return formatted string of the task/problem metadata.

        Returns:
              str: Formatted String of Metadata
        """
        info = json.dumps(self.metadata, indent=4)
        return "\n".join(["Problem/Task Metadata:", info, ""])

    def get_stages(self):
        """
        Return a list of the stages in the current task.

        Args:
            none
        """
        return self.stagenames

    def get_task_ids(self):
        """
        Return a list of tasks.

        Args:
            none
        """
        return self.task_ids

    @staticmethod
    def deactivate_current_session():
        """
        Clean up by deactivating a session that was canceled early or do nothing.

        Returns:
            None
        """
        log.warning("Deactivated improperly ended session")

    def finish_stage(self, stage, toolset):
        """
        Cleanup for end of each stage

        Args:
            stage (str): current stage of evaluation
            toolset (dict[str,any]): toolset for protocol
        """
        # Add stage dataset to the whitelist

        toolset['last_stage_pretrained_network'] = toolset["source_network"]
        stage_metadata = self.get_stage_metadata(stage)
        log.info(f'Finishing Stage "{stage}" for dataset {stage_metadata["dataset"]}')
        if self.problem_type != 'machine_translation':
            train_name = f'{stage}_{toolset["target_dataset"].name}'
            toolset["whitelist_datasets"][train_name] = toolset["target_dataset"]
            test_name = f'{stage}_{toolset["eval_dataset"].name}'
            toolset["whitelist_datasets"][test_name] = toolset["eval_dataset"]
            name = train_name.replace('_train', '')
            toolset['whitelist'].append(name)
            if toolset["source_network"] is not None:
                toolset["source_network"].target_dataset = name
        log.info(self.format_status(False))
        log.info(self.format_task_metadata())

    def get_task_subset_by_type(self, secret: str, govteam_secret: str, url: str,
                                subset_type: str) -> List[str]:
        """
        Helper function that returns the task ids in a list that match a specified
        problem type

        Args:
            secret (str): Only for JPL
            govteam_secret (str): Only for JPL
            url (str): Only for JPL
            subset_type (str): The task_type subset you want to get back

        Returns:
            list of tasks for a problem type
        """
        task_list = self.get_task_ids()
        for _task in task_list:
            if task_list[_task]['problem_type'] == subset_type:
                pass

        #             subset_tasks.append(_task)
        # r = None
        # try:
        #     headers = {"user_secret": secret,
        #                "govteam_secret": govteam_secret}
        #     tasks = requests.get(f"{url}/list_tasks", headers=headers)
        #     task_list = tasks.json()['tasks']
        #     subset_tasks = []
        #     for _task in task_list:
        #         r = requests.get(f"{url}/task_metadata/{_task}", headers=headers)
        #         task_metadata = r.json()
        #         if task_metadata['task_metadata']['problem_type'] == subset_type:
        #             subset_tasks.append(_task)
        # except HTTPError as e:
        #     raise Exception(r.json()["trace"])
        # except Exception as e:
        #     raise e
