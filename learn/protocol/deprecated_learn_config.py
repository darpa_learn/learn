# DUE TO SHIFT TO HYDRA, THIS IS NO LONGER NEEDED
#import scriptconfig as scfg
#
#
#class LearnConfig(scfg.Config):
#    """
#    Default configuration for Learn task
#
#    Example:
#        >>> from learn.protocol.learn_config import LearnConfig
#        >>> config = LearnConfig()
#        >>> print('config = {!r}'.format(config))
#    """
#    default = {
#        'name': scfg.Value('cool_name',
#                           help='Name of the method'),
#        'dataset_dir': scfg.Path("~/lwll_datasets/",
#                                 help='location of dataset (usually called lwll_datasets'),
#        'dataset_type': scfg.Path("development",
#                                  help="Type of dataset ( Choice: development, evaluation or external )"),
#        'tasks_to_run': scfg.Value(['bbfadb2c-c7c3-4596-b548-3dd01a6d1d2c'],
#                                   help='Array or Tuple with the tasks to Run'),
#
#        'output_path': scfg.Path('./output/fsdet',
#                                 help='Where some algorithms will output their files'),
#
#        'data_type': scfg.Value('full',
#                                help='Whether to use sample or full datasets.  '
#                                     'Values are "full" or "sample"'),
#
#        'domain_network_selection_algo': scfg.Path(
#            "templateDomainNetworkSelection/domainNetworkSelection.py",
#            help='Algorithm to select the source domain and network for the task'),
#
#        'algoSelection_algo': scfg.Path(
#            "algoSelection.py",
#            help='Algorithm for Selection of algorithms for query and classifications'),
#
#        'pretrain_on_source_target_algo': scfg.Value(
#            None,
#            help="Algorithm for doing joint/sequential (self-supervised) pretraining on the source and/or target"
#                 "eg. 'HPT/pretrain_algo.py'"),
#
#        'query_algo': scfg.Path(
#            'templateQueryAlgo/query_algo.py',
#            help='path from algorithm folder to the query algorithm '
#                 '(Use "auto" to have the algorithm select'),
#
#        'image_classification_algo': scfg.Path(
#            'MME/image_classification.py',
#            help='path from algorithm folder to the image classification algorithm'
#                 '(Use "auto" to have the algorithm select'),
#
#        'object_detection_algo': scfg.Path(
#            'FsDet/object_detection.py',
#            help='path from algorithm folder to the object detection algorithm'
#                 '(Use "auto" to have the algorithm select'),
#
#        'machine_translation_algo': scfg.Path(
#            'templateNLP/machine_translation.py',
#            help='path from algorithm folder to the NLP algorithm'
#                 '(Use "auto" to have the algorithm select'),
#
#        'video_classification_algo': scfg.Path(
#            'templateVideoClassification/video_classification.py',
#            help='path from algorithm folder to the video classification algorithm'
#                 '(Use "auto" to have the algorithm select'),
#        
#        'cuda': scfg.Value(True),
#
#        'device': scfg.Value([0],
#                             help='Which device to run the experiment on.  A number specifies a GPU'
#                                  'Use "cpu" for the cpu'),
#
#        'url': scfg.Value("https://api-staging.lollllz.com/",
#                          help='The URL for the JPL API'),
#
#        'apikey': scfg.Value("adab5090-39a9-47f3-9fc2-3d65e0fee9a2",
#                             help="Key for Berkeley team.  Also known as secret even"
#                                  "though it isn't really secret"),
#
#        'govteam_secret': scfg.Value(None,
#                                     help="Key used by government to add an extra layer of security"
#                                          " and prevent information leak"),
#        'add_dataset_to_whitelist': scfg.Value(None,
#                                               help="Use dataset to whitelist (only for testing or debugging)"),
#
#        'find_pretrained_method': scfg.Value("auto",
#                                             help='Either name of a torchvision model or "auto" for '
#                                                  'automatic finding'),
#
#        'find_source_data_network': scfg.Value("resnet34",
#                                               help='torchvision model computer features for source dataset'),
#
#        'find_source_data_method': scfg.Value("auto",
#                                              help='Either dataset name or "auto" for automatic detection'
#                                                   'Will raise error if dataset not in whitelist'),
#
#        'model_config_file': scfg.Value("COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x.yaml",
#                                        help='object detection Default model for training and '
#                                             'parameters.  This defines the  pretrained model '
#                                             'parameter and setting'),
#
#        'mixed_precision': scfg.Value(False,
#                                      help='Train with Mixed precision?'),
#
#        'num_workers': scfg.Value(16,
#                                  help='Number of cpu works for the task'),
#
#        'pretrain_on_source_target_strategy': scfg.Value(None,
#                                                         help='What data to use for [self-sup] pretraining: '
#                                                         '{None, "source", "target", "source_then_target", "source_and_target"}'),
#
#        'pretrain_on_source_target_train_steps': scfg.Value(50000,
#                                                         help='How many training steps (minibatch updates) to run HPT'),
#
#        'rate_threshold': scfg.Value(0.1,
#                                     help='Rate at ratio between |unlabeled data| / |labeled data| for MME '
#                                          'to start switch from MME to just normal fine-tuning.'),
#
#        'warm_start': scfg.Value(True,
#                                 help='Whether to save start at the last budget level or start for '
#                                      'pretrained weights'),
#
#        'only_target_mme': scfg.Value(False,
#                                 help='Only use target data for MME algorithm'),
#
#        'mme_freeze_backbone': scfg.Value(False,
#                                      help='freeze the backbone network for mme if true'),
#
#        'mme_baseline_classifier': scfg.Value(False,
#                                      help='Use a classifier with no normalization (standard linear classifier)'),
#        
#        'mme_baseline_plusplus': scfg.Value(False,
#                                          help='Use a classifier of baseline_plusplus'),
#        
#        'vaal_reduction_in_mse_loss': scfg.Value('sum',
#                                 help='Switching between old and new loss in VAAL.  This is sum or mean'
#                                      'where sum makes better reconsturcitons and mean normalizes between'
#                                      'image sizes.'),
#
#        'local_task_folder_path': scfg.Path('configs/local_task_configs'),
#
#        'fsnet_freeze_backbone':  scfg.Value(False,
#                help='Whether or not to freeze the backbone for fsnet'),
#
#        'mme_auto_off': scfg.Value(False,
#                                          help='Automatically turn off MME if not all target classes are in '
#                                               'the source datasets'),
#
#        'fsnet_maximum_iter': scfg.Value(10000,
#                                  help='maximum number of iterations for training '
#                                       'Higher will take longer but might be more '
#                                       'accurate'),
#
#    }
#
