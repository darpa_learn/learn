"""
Environment Variables Provided by DMC during evaluation

Refer to [DMC Environment Variables](https://lollllz.com/wiki/x/8QDp) for details
"""
# from learn.protocol.learn_config import LearnConfig
import logging
import os
import torch

log = logging.getLogger(__name__)

# None signifies no default value specified by DMC
LWLL_ENV_VARS = {
                 "LWLL_TA1_DATASET_TYPE": ["sample", "full", "all"],
                 "LWLL_TA1_PROB_TYPE": ["image_classification",
                                        "object_detection",
                                        "machine_translation",
                                        "video_classification",
                                        "all"],
                 "LWLL_TA1_API_ENDPOINT": ["http://localhost:5000",
                                           "https://api-dev.lollllz.com/",
                                           "https://api-staging.lollllz.com/",
                                           "https://api-prod.lollllz.com/"],
                 "LWLL_TA1_TEAM_SECRET": None,
                 "LWLL_TA1_GOVTEAM_SECRET": None,
                 "LWLL_TA1_PROB_TASK": None,
                 "LWLL_TA1_GPUS": None,
                 "LWLL_TA1_HOURS": None,
                 "LWLL_TA1_DATA_PATH": None
                }

ENV_TO_CONFIG = {"LWLL_TA1_PROB_TYPE" : "problem_type"}

PROBLEM_ENV_TO_CONFIG = {
                 "LWLL_TA1_PROB_TYPE": "problem_type",
                 "LWLL_TA1_API_ENDPOINT": "url",
                 "LWLL_TA1_DATASET_TYPE": "data_type",
                 "LWLL_TA1_TEAM_SECRET": "apikey",
                 "LWLL_TA1_PROB_TASK": "tasks_to_run",
                 "LWLL_TA1_GOVTEAM_SECRET": "govteam_secret",
                 "LWLL_TA1_GPUS": "device",
                 "LWLL_TA1_DATA_PATH": "dataset_dir"
                 }


def check_env_var():
    """
    Check that lwll environment are set

    Returns:
        A bool indicating if any environment variables are present

    Example:
        >>> # Mocking the environment variable state to have values to check.
        >>> import unittest.mock as mock
        >>> with mock.patch.dict(os.environ, {
        ...     'LWLL_TA1_DATASET_TYPE': 'sample'
        ... }):
        ...     check_env_var()
        True
    """
    for LWLL_ENV_VAR in LWLL_ENV_VARS.keys():
        if os.environ.get(LWLL_ENV_VAR):
            return True
    return False


def validate_with_plausible_values():
    """
    Check that environment variables take appropriate values

    Returns:
        A bool indicating that the env variables have a valid value

    Example:
        >>> # Mocking the environment variable state to have values to validate.
        >>> import unittest.mock as mock
        >>> with mock.patch.dict(os.environ, {
        ...     "LWLL_TA1_DATASET_TYPE": "sample",
        ...     "LWLL_TA1_PROB_TYPE": "image_classification",
        ...     "LWLL_TA1_API_ENDPOINT": "https://api-staging.lollllz.com/",
        ...     "LWLL_TA1_TEAM_SECRET": "adab5090-39a9-47f3-9fc2-3d65e0fee9a2",
        ...     "LWLL_GPUS": "0",
        ...     "LWLL_TA1_HOURS": "0.4",
        ...     "LWLL_TA1_DATA_PATH": "/lwll",
        ... }):
        ...     validate_with_plausible_values()
        True
    """
    for LWLL_ENV_VAR in LWLL_ENV_VARS.keys():
        if LWLL_ENV_VARS[LWLL_ENV_VAR] is not None \
                and os.environ.get(LWLL_ENV_VAR) not in LWLL_ENV_VARS[LWLL_ENV_VAR]:
            log.info(LWLL_ENV_VAR)
            return False
    return True


def update_all_devices(config, device):
    """
    For any key "device" that recursively exists in the given `config`, set the
    value to `device.

    Args:
        config: Dictionary-like instance to recursively scan and update
            "device" values of.
        device: New value to set to "device" keys.

    Returns:
        The input `config` instance that was input, post update pass.

    Example:
        >>> test_config = {
        ...     "device": "cpu",
        ...     "nested-key": {
        ...         "irrelevant_key": "foobar",
        ...         "device": "cloud",
        ...     }
        ... }
        >>> updated_config = update_all_devices(test_config, "cuda:0")
        >>> assert updated_config is test_config
        >>> updated_config['device']
        'cuda:0'
        >>> updated_config['nested-key']['device']
        'cuda:0'
    """
    for k, v in config.items():
        if isinstance(v, dict):
            update_all_devices(v, device)
        else:
            if k == 'device':
                config[k] = device
    return config


def merge_config(config: dict, env_mapping: dict, sub_key=None) -> dict:
    """
    Merge learn configuration with environment variables based on a mapping from
    environment to learn configuration

    Args:
        config : LEARN configuration as dictionary
        env_mapping : A dictionary containing mapping from env variables to learn config
        sub_key : An optional key to merge into instead of the root level of
            `config`. If this key does not exist yet in `config` it will be
            added as a new dictionary before being updated.

    Returns:
        An overriden learn config where the overriden values are from environment

    Example:
        >>> learn_config = {}
        >>> # Mocking the environment variable state to have values to merge
        >>> import unittest.mock as mock
        >>> with mock.patch.dict(os.environ, {
        ...     "LWLL_TA1_DATASET_TYPE": "sample",
        ...     "LWLL_TA1_PROB_TYPE": "image_classification",
        ...     "LWLL_TA1_API_ENDPOINT": "https://api-staging.lollllz.com/",
        ...     "LWLL_TA1_TEAM_SECRET": "adab5090-39a9-47f3-9fc2-3d65e0fee9a2",
        ...     "LWLL_GPUS": "0",
        ...     "LWLL_TA1_HOURS": "0.4",
        ...     "LWLL_TA1_DATA_PATH": "/lwll",
        ... }):
        ...     result_config = merge_config(learn_config, PROBLEM_ENV_TO_CONFIG)
        >>> result_config["url"]
        'https://api-staging.lollllz.com/'
        >>> result_config["problem_type"]
        'image_classification'
    """
    for env_key, config_key in env_mapping.items():
        if os.environ.get(env_key):
            env_val = os.environ.get(env_key)
            if env_key == "LWLL_TA1_GPUS":
                if env_val == "all":
                    env_val = list(range(torch.cuda.device_count()))
                else:
                    env_val = list(map(int, env_val.split(" ")))
                config = update_all_devices(config, env_val)
            elif (env_key == "LWLL_TA1_API_ENDPOINT" and
                  env_val == "https://api-prod.lollllz.com/"):
                config["dataset_type"] = "evaluation"
            elif env_key == "LWLL_TA1_HOURS":
                env_val = float(env_val)
            elif env_key == "LWLL_TA1_PROB_TASK":
                # When prob type is set to all we would be using a script that would
                # schedule the tasks on different gpus. For every task provide
                # a config would be created with the task id. Thus in this case this
                # env variable is ignored
                if env_val == "all":
                    continue
                else:
                    env_val = [env_val]
            if sub_key:
                config.setdefault(sub_key, {})[config_key] = env_val
            else:
                config[config_key] = env_val
        else:
            log.warning(f"{env_key} not provided in the environment")
    return config


def update_config_with_env_var(config: dict) -> dict:
    """
    Update learn configuration with environment variables

    Args:
        config (dict): LEARN configuration as dictionary

    Returns:
        An overriden dictionary config where the overriden values are from environment

    Example:
        >>> learn_config = {}
        >>> # Mocking the environment variable state to have values to merge
        >>> import unittest.mock as mock
        >>> with mock.patch.dict(os.environ, {
        ...     "LWLL_TA1_DATASET_TYPE": "sample",
        ...     "LWLL_TA1_PROB_TYPE": "image_classification",
        ...     "LWLL_TA1_API_ENDPOINT": "https://api-staging.lollllz.com/",
        ...     "LWLL_TA1_TEAM_SECRET": "adab5090-39a9-47f3-9fc2-3d65e0fee9a2",
        ...     "LWLL_TA1_GOVTEAM_SECRET": "adab5090-39a9-47f3-9fc2-3d65e0fee9a2",
        ...     "LWLL_GPUS": "0",
        ...     "LWLL_TA1_HOURS": "0.4",
        ...     "LWLL_TA1_DATA_PATH": "/lwll",
        ... }):
        ...     updated_config = update_config_with_env_var(learn_config)
        >>> updated_config["problem_type"]
        'image_classification'
        >>> updated_config["problem"]["url"]
        'https://api-staging.lollllz.com/'
    """
    if check_env_var():
        if validate_with_plausible_values():
            config = merge_config(config, ENV_TO_CONFIG)
            config = merge_config(config, PROBLEM_ENV_TO_CONFIG, sub_key='problem')
            log.info(f"Merged config:")
            log.info(f"{config}")
            return config
        else:
            log.warning("Using the original config without any modifications from the environment")
            return config
    else:
        logging.info("No environment variables found")
        return config
