# type: ignore  # noqa: E800

"""
File to house metric calculation code.

Copied from JPL's code here:
https://gitlab.lollllz.com/lwll/lwll_api/-/blob/devel/lwll_api/classes/metrics.py
"""
from collections import defaultdict
from typing import List, Tuple, Optional
import pandas as pd  # type: ignore
import math
import numpy as np  # type: ignore
import logging
log = logging.getLogger(__name__)

from typing import Tuple
from nltk.translate.bleu_score import corpus_bleu
import sacrebleu
from sklearn.metrics import roc_auc_score, top_k_accuracy_score, log_loss, balanced_accuracy_score
from learn.utils.wandb_utils import WANDB_INFO
import wandb


def accuracy(y_true: pd.DataFrame, y_pred: pd.DataFrame) -> float:
    """
    Args:
        df: pd.DataFrame, The performer's predictions
        actuals: pd.DataFrame, actual labels
    Return:
        float
    """
    acc = float((y_pred.flatten() == y_true).mean())
    return acc


def top_5_accuracy(y_true: np.array, preds: np.array) -> float:
    """
    Args:
        y_true: {np.array}, must be 1-D (Actually this is an array-like type
        preds: must be 1-D
    Return:
        float
    """
    top_5 = top_k_accuracy_score(y_true, preds, k=5)
    return float(top_5)


def roc_auc(y_true: np.array, preds: np.array) -> float:
    """
    One-vs-rest computes the AUC of each class against the rest. Sensitive to class imbalance

    Args:
        y_true: {np.array}, must be 1-D (Actually this is an array-like type
        preds: must be 1-D
    Return:
        float
    """
    roc = roc_auc_score(y_true, preds, multi_class='ovr', average="macro")
    return float(roc)


def mAP(df: pd.DataFrame, actuals: pd.DataFrame) -> float:
    """
    Args:
        df: pd.DataFrame, The performer's predictions
        actuals: pd.DataFrame, actual labels
    Return:
        float
    """
    _validate_input_ids_many_to_many(df, actuals)
    _df = format_obj_detection_data(df)
    _actuals = format_obj_detection_data(actuals)
    mAP = mean_average_precision(_actuals, _df)
    return mAP


def cross_entropy_logloss(y_true: np.array, preds: np.array) -> float:
    """
    Args:
        y_true: {np.array}, must be 1-D (Actually this is an array-like type
        preds: must be 1-D
    Return:
        float
    """
    return float(log_loss(y_true, preds))


def weighted_accuracy(y_true: np.array, y_pred: np.array) -> float:
    """
    Computes class weighted accuracy.
    TODO update type hinting to ArrayLike
    Args:
        y_true: {np.array}, must be 1-D (Actually this is an array-like type
        y_pred: must be 1-D
    Return:
        float
    """
    return float(balanced_accuracy_score(y_true, y_pred))


def brier_score(y_true: np.array, preds: np.array) -> float:
    """
    Args:
        y_true: {np.array}, must be 1-D (Actually this is an array-like type
        preds: must be 1-D
    Return:
        float
    """
    return float(np.mean(np.sum((preds - y_true)**2, axis=1)))


def _probabilistic_to_hard_assignment(df: pd.DataFrame) -> pd.DataFrame:
    """
    Args:
        df: pd.DataFrame, The performer's predictions
    Return:
        pd.DataFrame
    """
    # change df and actuals to be like old format
    class_cols = df.drop(columns='id').columns
    if "video_id" in df.columns:
        class_cols = df.drop(columns=["id", "video_id", "end_frame", "start_frame"]).columns
    class_col = pd.Series(df.loc[:, class_cols].idxmax(axis=1), name="class")
    reformat_df = pd.merge(class_col, df['id'], left_index=True, right_index=True)
    # print(f"reformat_df looks like: {reformat_df.head()}")
    return reformat_df


def bleu(df: pd.DataFrame, actuals: pd.DataFrame) -> List[float]:
    """
    Old implementation of using nltk bleu score. This has been deprecated since it has been
    advised sacrebleu is much better: https://gitlab.lollllz.com/lwll/lwll_api/-/issues/80#3-bleu
    """
    _validate_input_ids_one_to_one(df, actuals)
    actual_series = actuals['text'].apply(lambda x: [x.split()])
    predicted_series = df['text'].apply(lambda x: x.split())
    bleu_1 = corpus_bleu(actual_series, predicted_series, weights=(1.0, 0, 0, 0))
    bleu_2 = corpus_bleu(actual_series, predicted_series, weights=(0.5, 0.5, 0, 0))
    bleu_3 = corpus_bleu(actual_series, predicted_series, weights=(0.3, 0.3, 0.3, 0))
    bleu_4 = corpus_bleu(actual_series, predicted_series, weights=(0.25, 0.25, 0.25, 0.25))
    log.info('BLEU-1: %f' % bleu_1)
    log.info('BLEU-2: %f' % bleu_2)
    log.info('BLEU-3: %f' % bleu_3)
    log.info('BLEU-4: %f' % bleu_4)
    return [bleu_1, bleu_2, bleu_3, bleu_4]

def bleu_sacre(df: pd.DataFrame, actuals: pd.DataFrame) -> Tuple[float, str]:
    """
    Implemention of bleu using sacrebleu as it is more accepted in MT than the nltk version
    https://gitlab.lollllz.com/lwll/lwll_api/-/issues/80#3-bleu

    Example 'refs' and 'sys' for sacrebleu calculation
    refs = [['The dog bit the man.', 'It was not unexpected.', 'The man bit him first.']]
    sys = ['The dog bit the man.', "It wasn't surprising.", 'The man had just bitten him.']
    """

    _validate_input_ids_one_to_one(df, actuals)
    df['_id'] = df['id'].apply(lambda x: int(x))
    actuals['_id'] = actuals['id'].apply(lambda x: int(x))
    _combined = df.merge(actuals, left_on='_id', right_on='_id')

    sys = _combined['text_x'].tolist()
    refs = [_combined['text_y'].tolist()]
    # log.info(f"sys:: {sys[:5]}")
    # log.info(f"refs::{refs[0][:5]}")
    bleu = sacrebleu.corpus_bleu(sys, refs)
    score: float = bleu.score
    bleu_str: str = str(bleu)
    return score, bleu_str

def _align_ids(df: pd.DataFrame, actuals: pd.DataFrame) -> Tuple[np.array, np.array]:
    """
    Args:
        df: pd.DataFrame, The performer's predictions
        actuals: pd.DataFrame, actual labels
    Return:
        Tuple[np.array, np.array]
    """
    df['id'] = df['id'].astype(str)
    df_sort = df.sort_values(by=['id'])
    actual_sort = actuals.sort_values(by=['id'])
    _df = df_sort.set_index('id')
    _actuals = actual_sort.set_index('id')

    preds = _df.to_numpy()
    y_true = _actuals['class'].to_numpy()
    return y_true, preds

def _preprocess_df_to_array(df: pd.DataFrame, actuals: pd.DataFrame, classes: Optional[List[str]]) -> Tuple[np.array, np.array]:
    """
    Args:
        df: pd.DataFrame, The performer's predictions
        actuals: pd.DataFrame, actual labels
        classes: List, list of classes
    Return:
        Tuple[np.array, np.array]
    """
    df['id'] = df['id'].astype(str)
    df_sort = df.sort_values(by=['id'])
    actual_sort = actuals.sort_values(by=['id'])

    # One-hot encode the classes
    if "class" in df.columns:
        df_sort = pd.get_dummies(df_sort, columns=['class'], prefix='', prefix_sep='')
    actual_sort = pd.get_dummies(actual_sort, columns=['class'], prefix='', prefix_sep='')

    _df = df_sort.set_index('id')
    _actuals = actual_sort.set_index('id')

    # Add missing columns to predictions and actuals for test set
    classes = set(classes)
    df_cols = set(_df.columns)
    actuals_cols = set(_actuals.columns)
    if classes:
        for col in classes.difference(df_cols):
            _df[col] = 0.0

        for col in classes.difference(actuals_cols):
            _actuals[col] = 0.0

    for col in df_cols.difference(actuals_cols):
        _actuals[col] = 0.0

    for col in actuals_cols.difference(df_cols):
        _df[col] = 0.0

    # Sort the columns and make sure that they are all the same
    _df = _df.reindex(sorted(_df.columns), axis=1)
    _actuals = _actuals.reindex(sorted(_actuals.columns), axis=1)
    assert (_df.columns == _actuals.columns).all()

    # Convert to numpy and check that all rows sum to 1
    preds = _df.to_numpy()
    y_true = _actuals.to_numpy()

    assert np.allclose(preds.sum(axis=1), np.ones(preds.shape[0]))
    assert np.array_equal(y_true.sum(axis=1), np.ones(y_true.shape[0]))

    return y_true, preds

def _validate_input_ids_one_to_one(df: pd.DataFrame, actuals: pd.DataFrame) -> None:
    """
    Args:
        df: pd.DataFrame, The performer's predictions
        actuals: pd.DataFrame, actual labels
    """
    # Check lengths of dataframes are the same
    log.debug('Validating input ids one to one relationship...')
    pred_ids = df['id'].tolist()
    actual_ids = actuals['id'].tolist()
    set_difference = set(pred_ids).difference(set(actual_ids))
    log.debug(f"set difference is: {set_difference}")
    log.debug(f"{len(set_difference)} ids are different")
    if len(df['id']) != len(actuals['id']):
        raise Exception(f"Hitting condition: `len(df[{'id'}]) != len(actuals[{'id'}])`\nThis probably means that you are missing some test ids")
    # Check all test labels are accounted for in one to one relationship
    if len(set_difference) != 0:
        raise Exception(
            f"Hitting condition: `len(set(df[{'id'}].tolist()).difference(set(actuals[{'id'}].tolist()))) != 0`\nThis probably means that you are \
            missing some test ids")
    return

def _validate_input_ids_many_to_many(df: pd.DataFrame, actuals: pd.DataFrame) -> None:
    """
    Args:
        df: pd.DataFrame, The performer's predictions
        actuals: pd.DataFrame, actual labels
    """
    # Check all test labels are accounted for in a possible many to many relationship
    if len(set(df['id'].unique().tolist()).difference(set(actuals['id'].unique().tolist()))) != 0:
        raise Exception(
            f"Hitting condition: `len(set(df['id'].unique().tolist()).difference(set(actuals['id'].unique().tolist()))) != 0`\nThis \
            probably means that you are missing some test ids")
    return

"""
This code was largely taken from: https://github.com/Cartucho/mAP and modified
in order to work in memory and not use temporary files saving to disk throughout the process.

The initial pass through this code was to remove a lot of unecessary functionality related to plotting
as well as trying to group logical pieces of initial script. I think the person who initially wrote this
is a Matlap person hence why the code style is not consistent with the rest of this code base. - MH 12/15/19
"""

def log_average_miss_rate(precision: np.array, fp_cumsum: np.array, num_images: int) -> Tuple[float, float, float]:
    """
        log-average miss rate:
            Calculated by averaging miss rates at 9 evenly spaced FPPI points
            between 10e-2 and 10e0, in log-space.
        output:
                lamr | log-average miss rate
                mr | miss rate
                fppi | false positives per image
        references:
            [1] Dollar, Piotr, et al. "Pedestrian Detection: An Evaluation of the
               State of the Art." Pattern Analysis and Machine Intelligence, IEEE
               Transactions on 34.4 (2012): 743 - 761.
    """

    # if there were no detections of that class
    if precision.size == 0:
        lamr = 0.0
        mr = 1
        fppi = 0
        return lamr, mr, fppi

    fppi = fp_cumsum / float(num_images)
    mr = (1 - precision)

    fppi_tmp = np.insert(fppi, 0, -1.0)
    mr_tmp = np.insert(mr, 0, 1.0)

    # Use 9 evenly spaced reference points in log-space
    ref = np.logspace(-2.0, 0.0, num=9)
    for i, ref_i in enumerate(ref):
        # np.where() will always find at least 1 index, since min(ref) = 0.01 and min(fppi_tmp) = -1.0
        j = np.where(fppi_tmp <= ref_i)[-1][-1]
        ref[i] = mr_tmp[j]

    # log(0) is undefined, so we use the np.maximum(1e-10, ref)
    lamr = math.exp(np.mean(np.log(np.maximum(1e-10, ref))))

    return lamr, mr, fppi

def voc_ap(rec: list, prec: list) -> Tuple[float, list, list]:
    rec.insert(0, 0.0)  # insert 0.0 at begining of list
    rec.append(1.0)  # insert 1.0 at end of list
    mrec = rec[:]
    prec.insert(0, 0.0)  # insert 0.0 at begining of list
    prec.append(0.0)  # insert 0.0 at end of list
    mpre = prec[:]
    # This part makes the precision monotonically decreasing

    for i in range(len(mpre)-2, -1, -1):
        mpre[i] = max(mpre[i], mpre[i+1])
    """
     This part creates a list of indexes where the recall changes
        matlab: i=find(mrec(2:end)~=mrec(1:end-1))+1;
    """
    i_list = []
    for i in range(1, len(mrec)):
        if mrec[i] != mrec[i-1]:
            i_list.append(i)  # if it was matlab would be i + 1
    """
     The Average Precision (AP) is the area under the curve
        (numerical integration)
        matlab: ap=sum((mrec(i)-mrec(i-1)).*mpre(i));
    """
    ap = 0.0
    for i in i_list:
        ap += ((mrec[i]-mrec[i-1])*mpre[i])
    return ap, mrec, mpre

def populate_gt_stats(ground_truth_labels: list) -> Tuple[dict, dict, dict, list, int]:
    out_dict = {}

    gt_counter_per_class: defaultdict = defaultdict(int)
    counter_images_per_class: defaultdict = defaultdict(int)

    unique_images = sorted(set([line[0] for line in ground_truth_labels]))
    for file_id in unique_images:
        lines_list = [
            line for line in ground_truth_labels if line[0] == file_id]

        # create ground-truth dictionary
        bounding_boxes = []
        already_seen_classes: list = []
        for line in lines_list:
            class_name, left, top, right, bottom = line[1], line[2], line[3], line[4], line[5]
            bbox = f"{left} {top} {right} {bottom}"
            bounding_boxes.append(
                {"class_name": class_name, "bbox": bbox})
            # count that object
            gt_counter_per_class[class_name] += 1

            if class_name not in already_seen_classes:
                counter_images_per_class[class_name] += 1
                already_seen_classes.append(class_name)

        out_dict[file_id] = bounding_boxes

        gt_classes = list(gt_counter_per_class.keys())
        # let's sort the classes alphabetically
        gt_classes = sorted(gt_classes)
        n_classes = len(gt_classes)

    return out_dict, gt_counter_per_class, counter_images_per_class, gt_classes, n_classes

def prep_preds(preds_list: list, gt_classes: list) -> dict:
    out_preds: dict = {class_name: [] for class_name in gt_classes}
    for line in preds_list:
        file_id, class_name, confidence, left, top, right, bottom = \
            line[0], line[1], line[2], line[3], line[4], line[5], line[6]

        if class_name in gt_classes:
            out_preds[class_name].append({
                "confidence": confidence,
                "file_id": file_id,
                "bbox": f"{left} {top} {right} {bottom}",
            })

    for class_name in out_preds.keys():
        out_preds[class_name].sort(key=lambda x: float(x['confidence']), reverse=True)

    return out_preds

def _add_used_var_to_bboxes(out_dict: dict) -> dict:
    _to_return = {}
    for key, val in out_dict.items():
        new_list = []
        for row in val:
            row['used'] = False
            new_list.append(row)
        _to_return[key] = new_list
    return _to_return

def calculate_mAP(out_dict: dict, out_preds: dict, gt_classes: list, counter_images_per_class: dict,
                  gt_counter_per_class: dict, n_classes: int, min_overlap: float = 0.5) -> float:
    """
    Calculate the AP for each class
    """
    out_dict = _add_used_var_to_bboxes(out_dict)
    sum_AP = 0.0
    ap_dictionary = {}
    lamr_dictionary = {}
    # open file to store the results
    # print("# AP and precision/recall per class\n")
    count_true_positives = {}
    for class_index, class_name in enumerate(gt_classes):
        count_true_positives[class_name] = 0
        """
            Load detection-results of that class
        """
        dr_data = out_preds[class_name]

        """
            Assign detection-results to ground-truth objects
        """
        nd = len(dr_data)
        tp = [0] * nd  # creates an array of zeros of size nd
        fp = [0] * nd
        for idx, detection in enumerate(dr_data):
            file_id = detection["file_id"]
            # assign detection-results to ground truth object if any
            ground_truth_data = out_dict[file_id]
            ovmax = -1
            gt_match = -1
            # load detected object bounding-box
            bb = [float(x) for x in detection["bbox"].split()]
            for obj in ground_truth_data:
                # look for a class_name match
                if obj["class_name"] == class_name:
                    bbgt = [float(x) for x in obj["bbox"].split()]
                    bi = [max(bb[0], bbgt[0]), max(bb[1], bbgt[1]),
                          min(bb[2], bbgt[2]), min(bb[3], bbgt[3])]
                    iw = bi[2] - bi[0] + 1
                    ih = bi[3] - bi[1] + 1
                    if iw > 0 and ih > 0:
                        # compute overlap (IoU) = area of intersection / area of union
                        ua = (bb[2] - bb[0] + 1) * (bb[3] - bb[1] + 1) + (bbgt[2] - bbgt[0] + 1) * (bbgt[3] - bbgt[1] + 1) - iw * ih
                        ov = iw * ih / ua
                        # print(f"Overlap: {ov}")
                        if ov > ovmax:
                            ovmax = ov  # type: ignore
                            gt_match = obj

            # assign detection as true positive/don't care/false positive
            if ovmax >= min_overlap:
                if not bool(gt_match["used"]):  # type: ignore
                    # true positive
                    tp[idx] = 1
                    gt_match["used"] = True  # type: ignore
                    count_true_positives[class_name] += 1
                else:
                    # false positive (multiple detection)
                    fp[idx] = 1
            else:
                # false positive
                fp[idx] = 1
                if ovmax > 0:
                    # status = "INSUFFICIENT OVERLAP"
                    pass

        # compute precision/recall
        cumsum = 0
        for idx, val in enumerate(fp):
            fp[idx] += cumsum
            cumsum += val
        cumsum = 0
        for idx, val in enumerate(tp):
            tp[idx] += cumsum
            cumsum += val
        rec = tp[:]
        for idx, val in enumerate(tp):
            rec[idx] = float(tp[idx]) / gt_counter_per_class[class_name]
        prec = tp[:]
        for idx, val in enumerate(tp):
            prec[idx] = float(tp[idx]) / (fp[idx] + tp[idx])  # type: ignore

        ap, mrec, mprec = voc_ap(rec[:], prec[:])
        sum_AP += ap
        ap_dictionary[class_name] = ap

        n_images = counter_images_per_class[class_name]
        lamr, mr, fppi = log_average_miss_rate(
            np.array(rec), np.array(fp), n_images)
        lamr_dictionary[class_name] = lamr


    log.info("AP per class:")
    log.info(ap_dictionary)
    ap_log = {k + '_AP': v for k,v in ap_dictionary.items()}
    if WANDB_INFO.run_wandb:
        WANDB_INFO.log_metrics(ap_log)


    if WANDB_INFO.run_wandb:
        sorted_classes = {k: v for k, v in sorted(gt_counter_per_class.items(), key=lambda item: item[1])}
        sorted_class_counts = list(sorted_classes.values())
        sorted_aps = [ap_dictionary[class_] for class_ in sorted_classes.keys()]

        data = [[x, y] for (x, y) in zip(sorted_class_counts, sorted_aps)]
        table = wandb.Table(data=data, columns = ["class counts", "AP"])
        WANDB_INFO.log_metrics({"class counts vs. class AP" : table})

        import matplotlib
        import matplotlib.pyplot as plt
        matplotlib.rc('font', size=6)
        # plt.rcParams["figure.figsize"]=(30, 6) # width, height

        plt.plot(np.arange(len(sorted_classes)), sorted_aps)
        plt.ylabel('Average Precision')
        plt.xlabel('Class counts')
        locs, labs = plt.xticks()
        plt.grid(False)
        plt.grid(True)
        plt.xticks(np.arange(len(sorted_classes)), [str(round(v,2))+"_"+k for k,v in sorted_classes.items()], rotation=90)
        plt.ylim(0,1.0)
        plt.yticks([0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0])
        plt.tight_layout()
        image = wandb.Image(plt, caption="AP per class counts")
        WANDB_INFO.log_metrics({'AP per class counts': image})

    mAP = round(sum_AP / n_classes, 2)
    return mAP


def tide(df: pd.DataFrame, actuals: pd.DataFrame):
    """
    Uses pip version of https://github.com/dbolya/tide
    """
    from tidecv.data import Data 
    from tidecv.quantify import TIDE

    _validate_input_ids_many_to_many(df, actuals)
    _df = format_obj_detection_data(df)  # list
    _actuals = format_obj_detection_data(actuals)  # list

    preds = Data('preds', max_dets=100000)
    gt = Data('gt', max_dets=100000)

    for entry in _actuals:
        gt.add_ground_truth(entry[0], entry[1], entry[2:])

    for entry in _df:
        preds.add_detection(entry[0], entry[1], entry[2], entry[3:])

    tide = TIDE()
    tide.evaluate_range(gt, preds, mode=TIDE.BOX) 
    tide.summarize()
    err = tide.get_main_errors()['preds']
    spec = tide.get_special_errors()['preds']

    if WANDB_INFO.run_wandb:
        # get each AP for every threshold
        ap_table = wandb.Table(columns=['Thresh', '50', '55', '60', '65', '70', '75', '80', '85', '90', '95'])
        aps = []
        for run_name in tide.run_thresholds:
            thresh_runs = tide.run_thresholds[run_name]
            aps = [trun.ap for trun in thresh_runs]
            aps = [round(x,2) for x in aps]
            ap_table.add_data('AP', *aps)

        # get the impact on AP for each error type
        err_table = wandb.Table(columns=['Type', 'Cls', 'Loc', 'Both', 'Dupe', 'Bkg', 'Miss'])
        list_err = ['dAP']
        for col_name in ['Cls', 'Loc', 'Both', 'Dupe', 'Bkg', 'Miss']:
            list_err.append(round(err[col_name], 2))
        err_table.add_data(*list_err)

        spec_err_table = wandb.Table(columns=['Type', 'FalsePos', 'FalseNeg'])
        list_spec_err = ['dAP']
        for col_name in ['FalsePos', 'FalseNeg']:
            list_spec_err.append(round(spec[col_name], 2))
        spec_err_table.add_data(*list_spec_err)

        
        WANDB_INFO.log_metrics({"AP thresholds" : ap_table})
        WANDB_INFO.log_metrics({"Error types" : err_table})
        WANDB_INFO.log_metrics({"Special error types" : spec_err_table})  


def mean_average_precision(ground_truth: list, predictions: list, overlap: float = 0.5) -> float:
    """
    :param ground_truth:
    :param predictions:
    :param overlap:
    :param classes: list of classes from DatasetMetadata
    :return:
    """
    out_dict, gt_counter_per_class, counter_images_per_class, gt_classes, n_classes = populate_gt_stats(
        ground_truth)
    out_preds = prep_preds(predictions, gt_classes)
    mAP = calculate_mAP(out_dict, out_preds, gt_classes, counter_images_per_class, gt_counter_per_class, n_classes, min_overlap=overlap)

    return mAP

def format_obj_detection_data(inp: pd.DataFrame) -> list:
    """
    Takes our DataFrame format input and converts to the appropriate list of lists format
    """
    new = inp['bbox'].str.split(",", n=4, expand=True)
    new.columns = ['xmin', 'ymin', 'xmax', 'ymax']
    inp = inp.merge(new, left_index=True, right_index=True)

    if 'confidence' in inp.columns:
        inp = inp[['id', 'class', 'confidence', 'xmin', 'ymin', 'xmax', 'ymax']]
    else:
        inp = inp[['id', 'class', 'xmin', 'ymin', 'xmax', 'ymax']]

    inp.loc[:, 'xmin'] = inp['xmin'].apply(lambda x: int(float(x)))
    inp.loc[:, 'ymin'] = inp['ymin'].apply(lambda x: int(float(x)))
    inp.loc[:, 'xmax'] = inp['xmax'].apply(lambda x: int(float(x)))
    inp.loc[:, 'ymax'] = inp['ymax'].apply(lambda x: int(float(x)))
    outp: list = inp.to_numpy().tolist()
    return outp
