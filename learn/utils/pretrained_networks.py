"""
========================
Pretrained Network Class
========================

Code for defining the pretrained classes

"""

import torch
from torch import tensor
import torch.nn as nn
torch.multiprocessing.set_sharing_strategy('file_system')
import torchvision.models as models
from pathlib import Path
import copy
from typing import List, Dict
import ubelt as ub
import logging
import os
from detectron2.modeling import build_model
from detectron2.config import get_cfg
from detectron2.config import CfgNode
from detectron2.checkpoint.detection_checkpoint import DetectionCheckpointer
from timm import create_model
from timm.models import load_checkpoint
from learn.utils.dataset import (
    ImageClassificationDataset, 
    ObjectDetectionDataset,
    VideoClassificationDataset
)
from hydra import utils as hydra_utils

log = logging.getLogger(__name__)


def l2_norm(input: torch.Tensor) -> torch.Tensor:
    """ Return the 2d tensor normalized

    Args:
        input (torch.Tensor): 2D array with obs, features dim order

    Returns:
        torch.Tensor: 2D array with each row normalized
    """
    input_size = input.size()
    buffer = torch.pow(input, 2)

    normp = torch.sum(buffer, 1).add_(1e-10)
    norm = torch.sqrt(normp)

    _output = torch.div(input, norm.view(-1, 1).expand_as(input))

    output = _output.view(input_size)

    return output


def get_model(model_name: str) -> nn.Module:
    """ Return model from torchvision pretrained on imagenet.  Download and save models

    Args:
        model_name (str): name of the model.

    Returns:
        nn.Module: network with pretrained weights.

    Example:
        >>> from learn.utils.pretrained_networks import get_model
        >>> get_model('resnet18')  # doctest: +ELLIPSIS
        ResNet(...)
    """
    return getattr(models, model_name)(pretrained=True)


def get_video_model(model_name: str) -> nn.Module:
    """ Return model from torchvision pretrained on Kinetics400.  Download and save models

    Args:
        model_name (str): name of the model.

    Returns:
        nn.Module: network with pretrained weights.

    Example:
        >>> from learn.utils.pretrained_networks import get_video_model
        >>> get_video_model('r3d_18')  # doctest: +ELLIPSIS
        VideoResNet(...)
    """
    return getattr(models.video, model_name)(pretrained=True)


def get_pretrained_network_weights_for_imagenet():
    """ Get imagenet trained weights for all these models from torchvision repo
    Needs to run in docker code to ensure not downloading from the internet during
    evaluation

    Example:
        >>> from learn.utils.pretrained_networks import get_pretrained_network_weights_for_imagenet
        >>> get_pretrained_network_weights_for_imagenet()

    """
    for model_name in get_model_types():
        get_model(model_name)


def get_all_pretrained_models_image_classification(
        whitelist: List[str],
        whitelist_datasets: Dict[str, ImageClassificationDataset],
        pretrained_network_path):
    """ Return the pretrained model classes which have not been initialized

    Args:
        whitelist (list[str]): List of datasets which are allowed for this program.
        whitelist_datasets (list[ImageClassificationNetwork]: List of datasets from which
            to get pretrained network.

    Returns:
        dict[list[str]] of the pretrained networks available.  In the hierarchy of [dataset][model_type]
    """
    found_pretrained_networks = ub.AutoOrderedDict()
    for d in whitelist:
        found_pretrained_networks[d] = dict()
        num_cls = len(whitelist_datasets[f'{d}_train'].categories)
        for mt in get_model_types():
            file = Path(os.path.join(pretrained_network_path,
                f'{d}_{mt}.pth'))
            if file.exists() or d == 'imagenet_1k':
                network = ImageClassificationNetwork(d, file, mt, True, num_cls)
                found_pretrained_networks[d][mt] = network
                log.info(f"{file} Loaded")
    return found_pretrained_networks


def get_all_pretrained_models_object_detection(
        whitelist: List[str],
        whitelist_datasets: Dict[str, ObjectDetectionDataset],
        pretrained_network_path):
    """
    Return the pretrained model classes which have not been initialized

    Args:
        whitelist (list[str]): List of datasets which are allowed for this program.
        whitelist_datasets (list[ObjectDetectionDataset]: List of datasets from which
            to get pretrained network.

    Returns:
        dict[list[str]] of the pretrained networks available.  In the hierarchy of [dataset][model_type]


    Example:
        >>> from learn.utils.pretrained_networks import *
    """
    found_pretrained_networks = ub.AutoOrderedDict()
    for d in whitelist:
        found_pretrained_networks[d] = dict()
        num_cls = len(whitelist_datasets[f'{d}_train'].categories)
        for mt in get_od_model_cfg():
            # Google open images has been added here since we don't have a pretrained
            # model
            if d == 'coco2014' and 'resim' not in mt:
                file = Path(os.path.join(pretrained_network_path,
                    f'{d}_{mt}.pkl'))
            else:
                file = Path(os.path.join(pretrained_network_path,
                    f'{d}_{mt}.pth'))
            cfg_file = Path(os.path.join(hydra_utils.get_original_cwd(),
                f'configs/detectron_config/coco2014_{mt}.yaml'))
            if file.exists() or d == 'coco2014':
                network = DetectronObjectDetector(d, str(file), cfg_file, mt, True, num_cls)
                found_pretrained_networks[d][mt] = network
                log.info(f"Found model: {file} and cfg_file: {cfg_file}")
    return found_pretrained_networks


def get_all_pretrained_models_video_classification(
        whitelist: List[str],
        whitelist_datasets: Dict[str, VideoClassificationDataset],
        pretrained_network_path):
    """ Return the pretrained model classes which have not been initialized

    Args:
        whitelist (list[str]): List of datasets which are allowed for this program.
        whitelist_datasets (list[VideoClassificationNetwork]: List of datasets from which
            to get pretrained network.

    Returns:
        dict[list[str]] of the pretrained networks available.  In the hierarchy of [dataset][model_type]


    Example:
        >>> from learn.utils.pretrained_networks import *
    """
    found_pretrained_networks = ub.AutoOrderedDict()
    if len(whitelist) == 0:
        # whitelist is empty, no dataset is on computer. Just use Kinetics400
        # model
        whitelist = ['Kinetics400']
        num_cls = 400
    else:
        num_cls = None

    for d in whitelist:
        found_pretrained_networks[d] = dict()
        if num_cls is None:
            num_cls = len(whitelist_datasets[f'{d}_train'].categories)
        for mt in get_vid_model_types():
            file = Path(os.path.join(pretrained_network_path,
                f'{d}_{mt}.pth'))
            if file.exists() or d == 'Kinetics400':
                network = VideoClassificationNetwork(d, file, mt, True, num_cls)
                found_pretrained_networks[d][mt] = network
                log.info(f"{file} Loaded")
    return found_pretrained_networks


def get_all_pretrained_models(whitelist: List[str],
                              whitelist_datasets: Dict[str, ImageClassificationDataset],
                              problem_type: str,
                              pretrained_network_path):
    """ Return the pretrained model classes which have not been initialized

    Args:
        whitelist (list[str]): List of datasets which are allowed for this program.
        whitelist_datasets (list[ImageClassificationNetwork]: List of datasets from which
            to get pretrained network.
        problem_type (str): The problem type of the networks

    Returns:
        dict[list[str]] of the pretrained networks available.  In the hierarchy of [dataset][model_type]


    Example:
        >>> from learn.utils.pretrained_networks import *
    """
    if problem_type == 'image_classification':
        return get_all_pretrained_models_image_classification(whitelist, whitelist_datasets,
                pretrained_network_path)
    elif problem_type == 'object_detection':
        return get_all_pretrained_models_object_detection(whitelist, whitelist_datasets,
                pretrained_network_path) 
    elif problem_type == 'video_classification':
        return get_all_pretrained_models_video_classification(whitelist, whitelist_datasets,
                pretrained_network_path)
    else:
        raise ValueError(f'Problem type {problem_type} not implemented')


def get_model_types(amount_of_networks: str = 'large'):
    """  Get all the model type names that are used in this example.

    Args:
        amount_of_networks (str): either 'large' for lots of networks or 'small' for only a few

    Returns:
        list[str]: Model type names
    """
    if amount_of_networks == 'large':
        return ['resnet18', 'resnet34', 'resnet50', 'resnet152', 'wide_resnet101_2',
                'densenet169', 'mobilenet_v2', 'vgg16', 'efficientnet_b2']
    elif amount_of_networks == 'small':
        return ['vgg16', 'resnet34', 'densenet169', 'mobilenet_v2']


def get_vid_model_types(amount_of_networks: str = 'large'):
    """  Get all the model type names that are used in this example for video.

    Args:
        amount_of_networks (str): either 'large' for lots of networks or 'small' for only a few

    Returns:
        list[str]: Model type names
    """
    if amount_of_networks == 'large':
        return ['r3d_18', 'mc3_18', 'r2plus1d_18']
    elif amount_of_networks == 'small':
        return ['r3d_18', 'mc3_18', 'r2plus1d_18']


def get_od_model_cfg(amount_of_networks: str = 'large'):
    """  Get all the model configs used for object detection

    Args:
        amount_of_networks (str): either 'large' for lots of networks or 'small' for only a few

    Returns:
        list[str]: Model type names
    """
    if amount_of_networks == 'large':
        return ['faster_rcnn_R_101_FPN_3x',
                'faster_rcnn_X_101_32x8d_FPN_3x',
                'faster_rcnn_R_50_FPN_3x',
                'retinanet_R_101_FPN_3x',
                'fpn-resim-resnet101',
                'colorado-resim-retinanet',
                'tete-resim-retinanet']
    elif amount_of_networks == 'small':
        return ['faster_rcnn_X_101_32x8d_FPN_3x',
                'retinanet_R_101_FPN_3x']


class ImageClassificationNetwork(nn.Module):
    """
    Wrapper class for
        example tested networks list:
            'resnet18', 'resnet34', 'resnet152', 'wide_resnet101_2', 'densenet169', 'mobilenet_v2', 'vgg16'

    Example:
        >>> from learn.utils.pretrained_networks import *
        >>> pretrained_path = 'data/pretrained_networks/domain_net-clipart_resnet34.pth'
        >>> ImageClassificationNetwork('domainNet', pretrained_path, 'resnet34', True)  # doctest: +ELLIPSIS
        ImageClassificationNetwork(...)
        >>> ImageClassificationNetwork('imagenet_1k', '', 'densenet169', True)  # doctest: +ELLIPSIS
        ImageClassificationNetwork(...)
    """

    def __init__(self, pretraining_dataset: str, pretrained_path: str, model_name: str,
                 feature_extractor_only: bool, num_cls: int = 345) -> None:
        """
            Load pretrained network

        Args:
            pretraining_dataset (str): the name of the dataset for the network
            pretrained_path (str): the name for the folder (or pth file) with the pretrained network
                if folder, assuming model is named `{pretraining_dataset}_{model_name}.pth`.
                Ignored if using imagenet_1k.
            model_name (str): name of the model
            feature_extractor_only (bool): only feature extraction or also classification
            num_cls (int): Number of classes

        """
        super().__init__()
        log.info(f'Loading network trained on {pretraining_dataset} on a {model_name} from {pretrained_path}')
        self.pretraining_dataset = pretraining_dataset
        self.pretrained_path = pretrained_path
        self.model_name = model_name
        self.num_channels = 3
        self.feature_extractor_only = feature_extractor_only
        self.num_cls = num_cls
        self.type = 'image_classification'
        if feature_extractor_only:
            self.set_as_feature_extractor()

        self.conv_params = None
        self.classifier = None
        self.num_feature_dims = None
        self.load_pretrained_network()

    def load_pretrained_network(self) -> None:
        """ Load pretrained network based on values in class for pretraining_dataset, model_name,
        and pretrained_path.  Will load state_dict into self if it finds the network weights give warning
        of not being able to find it.

        Returns:
            None
        """
        self.setup_net()
        if self.pretraining_dataset != 'imagenet_1k':
            pp = Path(self.pretrained_path)
            if pp.is_dir():
                pp /= f'{self.pretraining_dataset}_{self.model_name}.pth'

            if pp.exists():
                #TODO: Remove the if statement in model loading and use existing
                # load state dict to load efficientnet models
                if "efficientnet" in self.model_name:
                    load_checkpoint(self.classifier, str(pp), use_ema=True, strict=False)
                    load_checkpoint(self.conv_params, str(pp), use_ema=True, strict=False)
                else:
                        log.info(f'Pretrained model loaded from {pp}')
                        self.load_state_dict(torch.load(pp, map_location='cpu'))
            else:
                log.warning(f'No file found at pretrained network not found at {pp}.  '
                                f'Sticking with imagenet pretrained model and random last fc layer')
        else:
            log.info(f'Pretained model loaded from {self.pretraining_dataset}')

    def set_as_feature_extractor(self):
        log.info('Only using model as a feature extractor')
        self.forward = self.forward_feat_extractor

    def set_as_classifier(self):
        log.info('Using model as a classifier')
        self.forward = self.forward_classifier

    def setup_net(self) -> None:
        """ For setting up network as pretrained network.

        Returns:
            None
        """
        log.info(f'Setting up {self.model_name} with weights')
        if "efficientnet" in self.model_name:
            self.classifier = create_model(self.model_name,
                                                   pretrained=True,
                                                   num_classes=self.num_cls,
                                                   in_chans=3).classifier
            self.conv_params = create_model(self.model_name,
                                            pretrained=True,
                                            in_chans=3)
            self.conv_params.reset_classifier(0)
            self.num_feature_dims = self.conv_params.num_features
        else:
            model = get_model(self.model_name)

            # Set last layer to identity.  Save imagenet weights otherwise.
            if hasattr(model, 'fc'):
                last_layer_name = 'fc'
            elif hasattr(model, 'classifier'):
                last_layer_name = 'classifier'
            else:
                import ipdb
                ipdb.set_trace()  # find last layer name to make identity

            fc = getattr(model, last_layer_name)
            second_level = False
            if ub.iterable(fc):
                fc = fc[-1]  # assume last one is classifier if iterable...like mobilenet_v2
                second_level = True

            self.num_feature_dims = fc.in_features
            if self.pretraining_dataset == 'imagenet_1k':
                self.classifier = copy.deepcopy(fc)

            if second_level:
                getattr(model, last_layer_name)[-1] = nn.Identity()
            else:
                setattr(model, last_layer_name, nn.Identity())

            if self.pretraining_dataset != 'imagenet_1k':
                self.classifier = nn.Linear(self.num_feature_dims, self.num_cls)
                nn.init.xavier_normal_(self.classifier.weight)
                self.classifier.bias.data.zero_()

            self.conv_params = model
            self.pretext_classifier = nn.Linear(self.num_feature_dims, 4)
            nn.init.xavier_normal_(self.pretext_classifier.weight)
            self.pretext_classifier.bias.data.zero_()

    def forward_feat_extractor(self, x: tensor) -> tensor:
        """ Forward

        Args:
            x (torch.Tensor): input to the network

        Returns:
            torch.Tensor() output of the network
        """
        return self.conv_params(x)

    def forward_classifier(self, x: tensor) -> tensor:
        """
        Args:
            x: input tensor for the network.  Runs from the input to the output.

        Returns:

        """
        return self.classifier(self.forward_feat_extractor(x))

    def forward(self, x: tensor) -> tensor:
        self.classifier(self.forward_feat_extractor(x))
        raise NotImplementedError('You need to override this function in init.  Ensure that is happening!')


class DetectronObjectDetector(nn.Module):
    """
    Wrapper class for ObjectDetector present in detectron 2
        example tested networks list:

    Example:
        >>> from learn.utils.pretrained_networks import *
    """

    def _override_cfg(self, model_cfg: str) -> CfgNode:
        """
        Private function for overriding default detectron configuration

        Args:
            model_cfg (str): A yaml file that contains parameters to override the default values

        Returns:
            Overriden CfgNode
        """
        cfg = get_cfg()
        cfg.merge_from_file(model_cfg)
        return cfg

    def __init__(self, pretraining_dataset: str, pretrained_path: str, model_cfg: str,
                 model_name: str, feature_extractor_only: bool, num_cls: int = 345) -> None:
        """
            Load pretrained network

        Args:
            pretraining_dataset (str): the name of the dataset for the network
            pretrained_path (str): the name for the folder (or pth file) with the pretrained network
                if folder, assuming model is named `{pretraining_dataset}_{model_name}.pth`.
                Ignored if using imagenet_1k.
            model_cfg (str): Model configuration
            model_name (str): Name of the model
            feature_extractor_only (bool): only feature extraction or also classification
            num_cls (int): Number of classes

        """
        super().__init__()
        log.info(f'Loading network trained on {pretraining_dataset} from {model_cfg} using {pretrained_path}')
        self.pretraining_dataset = pretraining_dataset
        self.pretrained_path = pretrained_path
        self.num_cls = num_cls
        self.model_cfg = self._override_cfg(model_cfg)
        self.model_cfg["MODEL"]["ROI_HEADS"]["NUM_CLASSES"] = self.num_cls
        self.model_cfg["MODEL"]["RETINANET"]["NUM_CLASSES"] = self.num_cls
        if os.path.exists(self.pretrained_path):
            self.model_cfg["MODEL"]["WEIGHTS"] = self.pretrained_path
        #print("ORDER ", os.environ["CUDA_DEVICE_ORDER"])
        #print("CUDADEVICES ", os.environ["CUDA_VISIBLE_DEVICES"])
        os.environ["CUDA_VISIBLE_DEVICES"] = "1,2,3,4,5,6,7,8"
        print("CUDADEVICES NOW ", os.environ["CUDA_VISIBLE_DEVICES"])
        self.object_detector = build_model(self.model_cfg)
        self.feature_extractor_only = feature_extractor_only
        self.model_name = model_name
        self.type = 'object_detection'
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        if feature_extractor_only:
            self.set_as_feature_extractor()
        else:
            self.set_as_classifier()

        self.load_pretrained_network()

    def load_pretrained_network(self) -> None:
        """ Load pretrained network based on values in class for pretraining_dataset, model_name,
        and pretrained_path.  Will load state_dict into self if it finds the network weights give warning
        of not being able to find it.

        Returns:
            None
        """
        pp = Path(self.pretrained_path)
        if pp.exists():
            log.info(f'Pretrained model loaded from {pp}')
            DetectionCheckpointer(self.object_detector).load(str(pp))
        else:
            log.warning(
                f'No file found at pretrained network not found at {pp}.  Sticking with coco model'
            )

    def set_as_feature_extractor(self):
        log.info('Only using model as a feature extractor')
        self.forward = self.forward_feat_extractor

    def set_as_detector(self):
        log.info('Using model as a classifier')
        self.forward = self.forward_classifier

    def forward_feat_extractor(self, x: tensor) -> tensor:
        """ Forward pass as a feature extractor

        Args:
            x (torch.Tensor): input to the network

        Returns:
            torch.Tensor() output of the network
        """
        return torch.flatten(self.avgpool(self.object_detector.backbone(x)['p6']), 1)

    def forward_detection(self, x: tensor) -> tensor:
        """ Forward pass as an object detector
        Args:
            x: input tensor for the network.  Runs from the input to the output.

        Returns:

        """
        return self.object_detector(x)

    def forward(self, x: tensor) -> tensor:
        self.classifier(self.forward_feat_extractor(x))
        raise NotImplementedError('You need to override this function in init.  Ensure that is happening!')


class VideoClassificationNetwork(nn.Module):
    """
    Wrapper class for
        example tested networks list:
    Example:
        >>> from learn.utils.pretrained_networks import *
        >>> pretrained_path = 'data/pretrained_networks/ava_dataset_r3d_18.pth'
        >>> VideoClassificationNetwork('AVA', pretrained_path, 'r3d_18', True)  # doctest: +ELLIPSIS
        VideoClassificationNetwork(...)
        >>> VideoClassificationNetwork('Kinetics400', '', 'r3d_18', True)  # doctest: +ELLIPSIS
        VideoClassificationNetwork(...)
    """

    def __init__(self, pretraining_dataset: str, pretrained_path: str, model_name: str,
                 feature_extractor_only: bool, num_cls: int = 345) -> None:
        """
            Load pretrained network

        Args:
            pretraining_dataset (str): the name of the dataset for the network
            pretrained_path (str): the name for the folder (or pth file) with the pretrained network
                if folder, assuming model is named `{pretraining_dataset}_{model_name}.pth`.
                Ignored if using imagenet_1k.
            model_name (str): name of the model
            feature_extractor_only (bool): only feature extraction or also classification
            num_cls (int): Number of classes

        """
        super().__init__()
        log.info(f'Loading network trained on {pretraining_dataset} on a {model_name} from {pretrained_path}')
        self.pretraining_dataset = pretraining_dataset
        self.pretrained_path = pretrained_path
        self.model_name = model_name
        #self.num_channels = 4
        self.feature_extractor_only = feature_extractor_only
        self.num_cls = num_cls
        self.type = 'video_classification'
        if feature_extractor_only:
            self.set_as_feature_extractor()
        else:
            self.set_as_classifier()

        self.conv_params = None
        self.classifier = None
        self.num_feature_dims = None
        self.load_pretrained_network()

    def load_pretrained_network(self) -> None:
        """ Load pretrained network based on values in class for pretraining_dataset, model_name,
        and pretrained_path.  Will load state_dict into self if it finds the network weights give warning
        of not being able to find it.

        Returns:
            None
        """
        self.setup_net()
        if self.pretraining_dataset != 'Kinetics400':
            pp = Path(self.pretrained_path)
            if pp.is_dir():
                pp /= f'{self.pretraining_dataset}_{self.model_name}.pth'

            if pp.exists():
                log.info(f'Pretrained model loaded from {pp}')
                self.load_state_dict(torch.load(pp, map_location='cpu'))
            else:
                log.warning(f'No file found at pretrained network not found at {pp}.  '
                                f'Sticking with imagenet pretrained model and random last fc layer')
        else:
            log.info(f'Pretained model loaded from {self.pretraining_dataset}')

    def set_as_feature_extractor(self):
        log.info('Only using model as a feature extractor')
        self.forward = self.forward_feat_extractor

    def set_as_detector(self):
        log.info('Using model as a classifier')
        self.forward = self.forward_classifier

    def forward_feat_extractor(self, x: tensor) -> tensor:
        """ Forward

        Args:
            x (torch.Tensor): input to the network

        Returns:
            torch.Tensor() output of the network
        """
        return self.conv_params(x)

    def forward_classifier(self, x: tensor) -> tensor:
        """
        Args:
            x: input tensor for the network.  Runs from the input to the output.

        Returns:

        """
        return self.classifier(self.forward_feat_extractor(x))

    def forward(self, x: tensor) -> tensor:
        self.classifier(self.forward_feat_extractor(x))
        raise NotImplementedError('You need to override this function in init.  Ensure that is happening!')

    def setup_net(self) -> None:
        """ For setting up network as pretrained network.

        Returns:
            None
        """
        log.info(f'Setting up {self.model_name} with weights')
        model = get_video_model(self.model_name)
        # Set last layer to identity.  Save imagenet weights otherwise.
        if hasattr(model, 'fc'):
            last_layer_name = 'fc'
        elif hasattr(model, 'classifier'):
            last_layer_name = 'classifier'
        else:
            raise ValueError("Model has no known last classifier layer")

        fc = getattr(model, last_layer_name)
        second_level = False
        if ub.iterable(fc):
            fc = fc[-1]  # assume last one is classifier if iterable...like mobilenet_v2
            second_level = True

        self.num_feature_dims = fc.in_features
        if self.pretraining_dataset == 'Kinetics400':
            self.classifier = copy.deepcopy(fc)

        if second_level:
            getattr(model, last_layer_name)[-1] = nn.Identity()
        else:
            setattr(model, last_layer_name, nn.Identity())

        if self.pretraining_dataset != 'Kinetics400':
            self.classifier = nn.Linear(self.num_feature_dims, self.num_cls)
            nn.init.xavier_normal_(self.classifier.weight)
            self.classifier.bias.data.zero_()

        self.conv_params = model
        self.pretext_classifier = nn.Linear(self.num_feature_dims, 4)
        nn.init.xavier_normal_(self.pretext_classifier.weight)
        self.pretext_classifier.bias.data.zero_()
