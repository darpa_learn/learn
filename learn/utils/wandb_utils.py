import wandb
from typing import Dict
import json
import hashlib

class WANDB_INFO:
    # this class contains some global wandb info. In particular, it contains
    # whether wandb should be run
    run_wandb : bool = False # defaults to true
    current_stage : str = None
    run_name : str = None

    def log_metrics(metric_dict):
        if WANDB_INFO.current_stage is not None:
            new_metric_dict = {}
            for k,v in metric_dict.items():
                new_metric_dict['{}_{}'.format(k,WANDB_INFO.current_stage)] = v
            metric_dict = new_metric_dict
        wandb.log(metric_dict)

    def add_runtime_info(info_to_add):
        # will add any runtime info to summary 
        # info_to_add should be dictionary
        if WANDB_INFO.current_stage is not None:
            new_info = {}
            for k,v in info_to_add.items():
                new_info['{}_{}'.format(k,WANDB_INFO.current_stage)] = v
            info_to_add = new_info
        for k,v in info_to_add.items():
            wandb.run.summary[k] = v

        # the below code instead adds the info to the config, but the summary
        # seems like a better silution
        '''run_info_key = 'info_added_while_running'
        if run_info_key not in wandb.config:
            wandb.config.update({run_info_key:{}})
        existing_run_info = wandb.config[run_info_key]
        existing_run_info.update(info_to_add)
        wandb.config.update({run_info_key: existing_run_info})'''

    def log_df(df, table_key):
        table = wandb.Table(dataframe = df)
        wandb.run.log({table_key : table})

def setup_wandb_run(cfg: Dict):
    # TODO: add environment variable updates back to config and to init call
    wandb_cfg = cfg['wandb_params'].copy()
    if not wandb_cfg['run_wandb']:
        WANDB_INFO.run_wandb = False
        return None # not initializing wandb session
    else:
        WANDB_INFO.run_wandb = True
        del wandb_cfg['run_wandb']

    if 'config' in wandb_cfg:
        # update config values
        wandb_cfg.update(wandb_cfg['config'])
        del wandb_cfg['config']

    if 'tags' not in wandb_cfg:
        wandb_cfg.update({'tags' : []})
    
    task_to_run = cfg['problem']['tasks_to_run']
    if isinstance(task_to_run, list):
        assert len(task_to_run) 
        task_to_run = task_to_run[0]

    wandb_cfg['tags'].extend([
        cfg['problem']['problem_type'],
        task_to_run
    ])
    
    
    if 'group' not in wandb_cfg:
        # for now set group to be hash of config
        # may revisit this later
        wandb_cfg['group'] = hashlib.md5(
            json.dumps(cfg, sort_keys = True).encode()
        ).hexdigest()
    
    if 'name' in wandb_cfg:
        WANDB_INFO.run_name = wandb_cfg['name']

    return wandb.init(
        config = cfg,
        **wandb_cfg
    )

