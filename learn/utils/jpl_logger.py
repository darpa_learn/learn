import os
import requests
import socket
import logging

API_URL = os.environ.get("LOG_API_URL", "http://lwll-govteam-elasticsearch.lollllz.com/api")
log_team = 'berkeley'


class JPLHandler(logging.Handler):
    def __init__(self, level=logging.NOTSET, checkpoint=0):
        logging.Handler.__init__(self, level)
        self.checkpoint = checkpoint
        self.setFormatter(
                    fmt=logging.Formatter(
                        '[%(asctime)s][%(name)s][%(levelname)s] - %(message)s'
                    )
                )

    def emit(self, record):
        msg = self.format(record)
        output = jpl_log(msg, checkpoint=self.checkpoint, local=False)


def jpl_log(error_message: str, checkpoint: int = 0, local: bool = False) -> dict:
    try:
        hostname = socket.gethostbyname(socket.gethostname())
    except:
        hostname = socket.gethostname()
    data = {
        "error_message": error_message,
        "team": log_team,
        "checkpoint": checkpoint,
        "reported_hostname": hostname
    }
    if local:
        return data
    else:
        return requests.post(f"{API_URL}/log", json=data).json()


class JPLLogger:
    def __init__(self, team: str, checkpoint: int = 0, print_stdout: bool = False, local: bool = False):
        self.team = team
        self.print_stdout = print_stdout
        self.checkpoint = checkpoint
        self.local = local

    def log(self, error_message: str, checkpoint: int = None) -> dict:
        try:
            hostname = socket.gethostbyname(socket.gethostname())
        except:
            hostname = socket.gethostname()
        data = {
            "error_message": error_message,
            "team": self.team,
            "checkpoint": checkpoint if checkpoint else self.checkpoint,
            "reported_hostname": hostname
        }

        if self.print_stdout:
            print(error_message)

        if self.local:
            return data
        else:
            return requests.post(f"{API_URL}/log", json=data).json()
