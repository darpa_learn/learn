FROM nvidia/cuda:11.1-cudnn8-devel-ubuntu18.04

#ARG FRAMEWORK_UNAME=learn-august-2020-eval
ARG LEARN_TAG=master_2023
ARG LEARN_UNAME=learn-august-2020-eval
#ARG FRAMEWORK_TOKEN
ARG LEARN_TOKEN
#ARG FRAMEWORK_REPO=gitlab.kitware.com/darpa_learn/framework.git
ARG LEARN_REPO=gitlab.kitware.com/darpa_learn/learn.git
#ARG FRAMEWORK_SHA=1b6ccd7f8e54bb977530534d0767cd5acebb0a90
#ARG LEARN_SHA=0f7c3a57bfef6f0969fdf9262bb26936947f3221

ENV TORCH_CUDA_ARCH_LIST="5.2 6.0 6.1 7.0 7.5 8.0 8.6+PTX"
ENV TORCH_NVCC_FLAGS="-Xfatbin -compress-all"

# Update NVIDIA signing keys
#RUN wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-keyring_1.0-1_all.deb && \
#    dpkg -i cuda-keyring_1.0-1_all.deb
RUN apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/3bf863cc.pub
RUN apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64/7fa2af80.pub

# Install dependencies
RUN apt-get update && \
    apt install -y ca-certificates \
                   git \
                   git-lfs \
                   gcc \
                   libgl1-mesa-glx \
                   python3.7 \
                   python3.7-dev \
                   python3-pip \
    && rm -rf /var/lib/apt/lists/*

# Update pip
RUN python3.7 -m pip install --upgrade pip

# Symlink for convenience
RUN ln -s /usr/bin/python3.7 /usr/bin/python

# Symlink cuda stubs so that they can be used by apex
RUN ln -sf /usr/local/cuda/lib64/stubs/libcuda.so /usr/local/cuda/lib64/libcuda.so

WORKDIR /app

# Clone the framework and learn repo
#RUN git clone https://${FRAMEWORK_UNAME}:${FRAMEWORK_TOKEN}@${FRAMEWORK_REPO} /app/framework

RUN git clone -b ${LEARN_TAG} https://${LEARN_UNAME}:${LEARN_TOKEN}@${LEARN_REPO} /app/learn

#WORKDIR /app/framework

# build framework
#RUN git checkout ${FRAMEWORK_SHA} && \
#    pip install -r requirements.txt && \
#    pip install .

WORKDIR /app/learn

# build learn
#RUN git lfs install && \
#    git lfs pull && \
#    git checkout ${LEARN_SHA} && \
RUN    pip install -r requirements.txt && \
    pip install .

RUN pip install -r learn/algorithms/PACMAC/requirements.txt
RUN pip install git+https://github.com/lucasb-eyer/pydensecrf.git

RUN pip install git+https://github.com/cocodataset/panopticapi.git

RUN pip install git+https://github.com/mcordts/cityscapesScripts.git

RUN pip install -r learn/algorithms/CutLER/CutLER_main/requirements.txt


# install apex
RUN git clone https://github.com/NVIDIA/apex /app/apex && \
    cd /app/apex && \
	git reset --hard a651e2c24ecf97cbf367fd3f330df36760e1c597 && \
    pip install -v --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" ./

# install individual algo requirements
# RUN cd /app/learn/learn/algorithms/HPT && \
#     pip install -r requirements.txt
# hack to fix slight issue with dependency (hopefully fixed in the lib in next release)
#RUN sed -i 's/workflow, tuple/workflow, list/' /usr/local/lib/python3.7/dist-packages/mmcv/runner/epoch_based_runner.py

# RUN git clone https://github.com/open-mmlab/mmdetection /app/mmdetection && \
#     cd /app/mmdetection && \
#     pip install -r requirements/build.txt && \
#     pip install "git+https://github.com/open-mmlab/cocoapi.git#subdirectory=pycocotools" && \
#     pip install -v -e .


RUN MMCV_WITH_OPS=1 FORCE_CUDA=1 pip install mmcv-full==1.5.3 -f https://download.openmmlab.com/mmcv/dist/cu111/torch1.9.0/index.html

RUN pip install mmdet==2.25.0

RUN pip install mmcls==0.25.0
RUN pip install -r learn/algorithms/ClipVideo/requirements.txt

# relink the newly installed algos
RUN pip install .

# Move again to learn directory
WORKDIR /app/learn


RUN mkdir -p outputs

#RUN mkdir -p /scratch

#RUN useradd 65534

RUN chown -R 65534:65534 /app/learn

RUN chmod -R a+rwx /app

USER 65534:65534

ENV HOME=/app/learn
# run agent
# # PACMAC+MMCLS
# ENTRYPOINT ["python", "hydra_launcher.py", "wandb_params.run_wandb=False", "problem.do_uda=False", "problem.skip_base=True", "problem.dataset_type=evaluation", "domain_network_selector.params.pretrained_network_dir=/lwll/extra/pretrained_networks", "problem.use_JPL_logger=False", "self_supervision_pretrain=none", "image_classifier=PACMAC_MMCLS", "image_classifier.pacmac_params.params.finetune_epochs=8", "image_classifier.pacmac_params.params.adapt_epochs=4", "image_classifier.mmcls_params.params.max_iters=0", "image_classifier.mmcls_params.params.batch_size=16", "image_classifier.mmcls_params.params.iters_per_ckpt=[6000,8000,10000,12000,14000,16000,18000,20000]", "image_classifier.mmcls_params.params.lr=0.00001", "image_classifier.mmcls_params.params.device=[0,1,2,3]", "image_classifier.mmcls_params.params.use_ddp=True", "image_classifier.mmcls_params.params.freeze_backbone=True"]
# # ClipVideo
# ENTRYPOINT ["python", "hydra_launcher.py", "problem=quick_vid_test", "problem.do_uda=False", "problem.skip_base=True", "wandb_params.run_wandb=False", "domain_network_selector.params.pretrained_network_dir=/lwll/extra/pretrained_networks", "problem.use_JPL_logger=False", "problem.dataset_type=evaluation", "self_supervision_pretrain=none", "domain_network_selector=simple_vid", "video_classifier=ClipVideo", "video_classifier.params.num_workers=4", "video_classifier.params.device_ids=[0,1,2,3,4,5,6,7]", "video_classifier.params.num_test_views=1", "video_classifier.params.num_test_crops=1", "video_classifier.params.batch_size=4"]
# MMDET / CutLER
# ENTRYPOINT ["python", "hydra_launcher.py", "problem=quick_od_test", "problem.skip_base=True", "problem.dataset_type=evaluation", "domain_network_selector=od_auto", "object_detector=MMDET", "domain_network_selector.params.find_pretrained_method=retinanet_R_101_FPN_3x", "domain_network_selector.params.find_source_data_method=coco2014", "problem.add_dataset_to_whitelist=[coco2014]", "domain_network_selector.params.pretrained_network_dir=/lwll/extra/pretrained_networks", "self_supervision_pretrain=CutLER", "wandb_params.run_wandb=False", "self_supervision_pretrain.params.device=[0,1,2,3]", "self_supervision_pretrain.params.use_ddp=True", "self_supervision_pretrain.params.ddp_num_gpus=4", "self_supervision_pretrain.params.max_iters=1000", "self_supervision_pretrain.params.freeze_backbone=True", "object_detector.params.use_ddp=True", "object_detector.params.ddp_num_gpus=4", "object_detector.params.max_iters=0", "object_detector.params.iters_per_ckpt=[4000,6000,8000,10000,14000,20000,24000,30000]", "object_detector.params.iters_per_ckpt_adapt=[4000,6000,8000,10000,14000,20000,24000,30000]", "object_detector.params.batch_size=1", "object_detector.params.device=[0,1,2,3]"]
